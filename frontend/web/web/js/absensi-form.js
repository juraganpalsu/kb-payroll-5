/**
 * Created by jiwa on 8/28/17.
 */

$(function () {
    $('#laporan-absensi-form').on('beforeSubmit', function () {
        var form = $(this);
        var btn = $('#laporan_absensi-btn');
        btn.html('loading...').prop('disabled', true);
        if (form.find('.form-group.has-error').length) {
            btn.html('Submit').prop('disabled', false);
            return false;
        }
        $.post(form.attr('action'), form.serialize())
            .done(function (data) {
                btn.html('Submit').prop('disabled', false);
                if (data.status === true) {
                    var win = window.open(data.url, '_blank');
                    win.focus();
                }
            }).fail(function () {
            console.log('Error menghampiri.')
        });
        return false;
    });
});
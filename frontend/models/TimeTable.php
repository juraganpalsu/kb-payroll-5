<?php

namespace frontend\models;

use common\models\TimeTable as BaseTimeTable;

/**
 * This is the model class for table "time_table".
 */
class TimeTable extends BaseTimeTable
{

    /**
     * @param null $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function listSelect2($q = null)
    {
        $query = self::find()->select(['id', 'concat(`nama`,\'-\',`masuk`,\'-\',`pulang`) as text'])
            ->andWhere(['like', 'concat(nama, masuk, pulang)', $q])->limit(20)->asArray()->all();
        return $query;
    }
}

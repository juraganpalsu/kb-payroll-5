<?php

namespace frontend\models;

use common\models\LiburPegawai as BaseLiburPegawai;

/**
 * This is the model class for table "libur_pegawai".
 * @property mixed monthString
 * @property array $ids
 */
class LiburPegawai extends BaseLiburPegawai
{

    public $ids;

    /**
     * @param array $pegawaiids
     */
    public function prosesData(array $pegawaiids)
    {
        $modelAbsensi = new Absensi();
        try {
            $modelAbsensi->prosesData($this->tanggal, $this->tanggal, null, $pegawaiids);
        } catch (\Exception $e) {
            \Yii::info($e->getMessage(), 'exception');
        }
    }
}

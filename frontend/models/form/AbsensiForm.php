<?php

namespace frontend\models\form;

use common\components\Status;
use common\models\SistemKerja;
use common\models\Spl;
use common\models\TimeTable;
use console\models\AntrianLaporanAbsensi;
use DateInterval;
use DateTime;
use Exception;
use frontend\models\Absensi;
use frontend\models\Attendance;
use frontend\models\Pegawai;
use frontend\models\SplTemplate;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\masterdata\models\PUangMakan;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Signup form
 * @property mixed sistemKerja_
 * @property mixed statusKehadiran_
 *
 * @property integer $unit
 * @property integer $departemen
 * @property integer $divisi
 * @property integer $golongan_id
 * @property integer $sistem_kerja
 * @property integer $status_kehadiran
 * @property integer $time_table_id
 * @property string $pegawai_id
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property array $pegawai_ids
 * @property string $tanggal
 * @property integer $bisnis_unit_id
 * @property integer $jenis_kontrak_id
 *
 *
 * @property array $_singkatanShift
 * @property string $_singkatanLemburKhusus
 * @property string $_singkatanLibur
 * @property string $_singkatanAlfa
 * @property string $_singkatanError
 * @property array $_singkatanIjin
 * @property array $_singkatanDetailError
 * @property  array $_singkatanCuti
 *
 */
class AbsensiForm extends Model
{
    public $unit;
    public $departemen;
    public $divisi;
    public $sistem_kerja;

    /************ properti yg dipakai untuk report terbaru **********/
    public $golongan_id;
    public $bisnis_unit_id;
    public $jenis_kontrak_id;
    public $status_kehadiran;
    public $tanggal_awal;
    public $tanggal_akhir;
    /****************************************************************/

    public $time_table_id;
    public $pegawai_id;
    public $pegawai_ids;
    public $tanggal;

    //singkatan khusus laporan jika ada SPL
    const SPB = 1; //surat perintah lembur belum diapprove
    const SPT = 2; //surat perintah lembur tidak sesuai

    /**
     * @var array Untuk menampilkan data sesuai laporan yang dinginkan
     */
    public $_singkatanShift = [TimeTable::NON_SHIFT => 'H', TimeTable::SHIFT_SATU => 'H1', TimeTable::SHIFT_DUA => 'H2', TimeTable::SHIFT_TIGA => 'H3'];

    public $_singkatanLemburKhusus = 'LH';

    public $_singkatanLibur = 'L';

    public $_singkatanAlfa = 'A';

    public $_singkatanError = 'UFN';

    public $_singkatanCuti = 'CT';

    public $_singkatanDetailError = [self::SPB => 'SPB', self::SPT => 'SPT'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_awal', 'tanggal_akhir', 'tanggal', 'golongan_id', 'pegawai_ids'], 'required'],
            [['departemen', 'divisi', 'golongan_id', 'jenis_kontrak_id', 'bisnis_unit_id', 'sistem_kerja', 'status_kehadiran', 'time_table_id', 'pegawai_id', 'tanggal_awal', 'tanggal_akhir', 'pegawai_ids'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['proses-data'] = ['tanggal', 'pegawai_ids'];
        $scenarios['laporan'] = ['golongan_id', 'jenis_kontrak_id', 'bisnis_unit_id', 'status_kehadiran', 'tanggal_awal', 'tanggal_akhir'];
        $scenarios['proses-data-per-golongan'] = ['tanggal', 'golongan_id', 'bisnis_unit_id'];
        return $scenarios;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $this->tanggal_awal = date('Y-m-d', strtotime($tanggal[0]));
            $this->tanggal_akhir = date('Y-m-d', strtotime($tanggal[1]));
        }
        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public function getSistemKerja()
    {
        return ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama');
    }

    /**
     * @param bool $allowAll
     * @return array
     */
    public function getStatusKehadiran($allowAll = true)
    {
        $modelTt = new Absensi();
        if ($allowAll) {
            return array_merge(['Semua'], $modelTt->_status_kehadiran);
        }
        return $modelTt->_status_kehadiran;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'time_table_id' => Yii::t('app', 'Time Table'),
            'pegawai_id' => Yii::t('app', 'Pegawai'),
            'pegawai_ids' => Yii::t('app', 'Pegawai'),
            'golongan_id' => Yii::t('app', 'Golongan'),
            'jenis_kontrak_id' => Yii::t('app', 'Jenis Kontrak'),
            'bisnis_unit_id' => Yii::t('app', 'Bisnis Unit')
        ];
    }

    /**
     * @return PerjanjianKerja
     */
    public static function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @param Pegawai $pegawai
     * @param string $tanggal
     * @return array
     */
    public function getAttendances(Pegawai $pegawai, string $tanggal)
    {
        $modelAttendance = new Attendance();
        $dari = strtotime($tanggal . '00:00:00');
        $sampai = strtotime($tanggal . '23:59:59');
        $attendances = $modelAttendance->getAttendances($pegawai, $dari, $sampai);
        return array_map(
            function ($d) {
                /** @var Attendance $d */
                return $d->check_time;
            }, $attendances);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function laporan()
    {
        $queryAbsensi = Absensi::find();
        $queryAbsensi->andWhere(['BETWEEN', 'tanggal', $this->tanggal_awal, $this->tanggal_akhir]);

        $query = Pegawai::find();
        $query->joinWith(['absensis' => function (ActiveQuery $query) {
            $query->andWhere(['BETWEEN', 'tanggal', $this->tanggal_awal, $this->tanggal_akhir]);
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $query) {
                if (!empty($this->jenis_kontrak_id)) {
                    $query->andWhere(['perjanjian_kerja.jenis_perjanjian' => $this->jenis_kontrak_id]);
                }
                if (!empty($this->bisnis_unit_id)) {
                    $query->andWhere(['perjanjian_kerja.kontrak' => $this->bisnis_unit_id]);
                }
            }]);
            $query->joinWith(['pegawaiGolongan' => function (ActiveQuery $query) {
                $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);
            }]);
            $query->orderBy('absensi.tanggal ASC');
        }]);

        $query->orderBy('pegawai.nama_lengkap ASC');

        $pegawais = $query->all();

        $datas = [];
        /** @var Pegawai $pegawai */
        foreach ($pegawais as $pegawai) {
            foreach ($pegawai->absensis as $absensi) {
                $datas[$pegawai->id]['pegawai'] = [
                    'id' => $pegawai->id,
                    'nama' => $pegawai->nama,
                    'id_pegawai' => $absensi->perjanjianKerja->id_pegawai,
                    'bisnis_unit' => $absensi->perjanjianKerja->namaKontrak,
                    'jenis_perjanjian' => $absensi->perjanjianKerja->jenis->nama,
                    'golongan' => $absensi->pegawaiGolongan->golongan->nama,

                ];
                $datas[$pegawai->id]['absensis'][$absensi->tanggal] = [
                    'sistem_kerja' => $absensi->sistemKerja ? $absensi->sistemKerja->nama : '',
                    'masuk' => $absensi->masuk,
                    'pulang' => $absensi->pulang,
                    'status_kehadiran' => $absensi->getStatusKehadiran_(),
                    'status_absensi' => $absensi->status_kehadiran,
                    'time_table' => $absensi->timeTable ? $absensi->timeTable->shift_ : '',
                    'time_table_id' => $absensi->timeTable ? $absensi->timeTable->id : 0,
                    'shift' => $absensi->timeTable ? $absensi->timeTable->shift : 0,
                    'spl_id' => $absensi->spl_id,
                    'spl_shift' => $absensi->splRel ? $absensi->splRel->splTemplate->tunjangan_shift : 0,
                    'jam_lembur' => $absensi->jamLembur(),
                    'attendances' => $absensi->status_kehadiran == Absensi::ERROR ? $this->getAttendances($pegawai, $absensi->tanggal) : [],
                ];
                $datas[$pegawai->id]['status_kehadirans'][] = $absensi->status_kehadiran;
            }
        }
        return $datas;
    }


    /**
     * @param AntrianLaporanAbsensi $laporanAbsensi
     * @return bool
     */
    public function downloadReport(AntrianLaporanAbsensi $laporanAbsensi)
    {
        try {
            $spreadsheet = new Spreadsheet();
            $headerStyle = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
                'fill' => [
                    'fillType' => Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => 'FFA0A0A0',
                    ],
                    'endColor' => [
                        'argb' => 'FFFFFFFF',
                    ],
                ],
            ];

            $center = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ]
            ];

            $outLineLeftBorder = [
                'borders' => array(
                    'left' => array(
                        'borderStyle' => Border::BORDER_THIN,
                    ),
                    'bottom' => array(
                        'borderStyle' => Border::BORDER_DOTTED,
                    ),
                ),
            ];

            $outLineRightBorder = [
                'borders' => array(
                    'right' => array(
                        'borderStyle' => Border::BORDER_THIN,
                    ),
                    'bottom' => array(
                        'borderStyle' => Border::BORDER_DOTTED,
                    ),
                ),
            ];

            $outLineBottomBorder = [
                'borders' => array(
                    'bottom' => array(
                        'borderStyle' => Border::BORDER_THIN,
                    ),
                ),
            ];

            $borderInside = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_DOTTED,
                    ],
                ],
            ];

            $font = array(
                'font' => array(
                    'size' => 9,
                    'name' => 'Calibri'
                ));

            $spreadsheet->setActiveSheetIndex(0);
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(5);
            $activeSheet->getColumnDimension('B')->setWidth(15);
            $activeSheet->getColumnDimension('C')->setWidth(30);
            $activeSheet->getColumnDimension('D')->setWidth(12);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
//        $activeSheet->getColumnDimension('G')->setWidth(30);


            $activeSheet->mergeCells('A1:A3')->setCellValue('A1', 'NO')->getStyle('A1:A3')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('B1:B3')->setCellValue('B1', 'Id Pegawai')->getStyle('B1:B3')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('C1:C3')->setCellValue('C1', 'Nama')->getStyle('C1:C3')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('D1:D3')->setCellValue('D1', 'Bisnis unit')->getStyle('D1:D3')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('E1:E3')->setCellValue('E1', 'Jenis Perjanjian')->getStyle('E1:E3')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('F1:F3')->setCellValue('F1', 'Golongan')->getStyle('F1:F3')->applyFromArray($headerStyle);
//        $activeSheet->mergeCells('G1:G3')->setCellValue('G1', '')->getStyle('G1:G3')->applyFromArray($headerStyle);

            $x = 'G';
            $j = 'I';
            $tanggal_awal = new DateTime($this->tanggal_awal);
            $tanggal_akhir = new DateTime($this->tanggal_akhir);
            while ($tanggal_awal <= $tanggal_akhir) {
                $activeSheet->mergeCells($x . '2:' . $j . '2')->setCellValue($x . '2', $tanggal_awal->format('D'))->getStyle($x . '2:' . $j . '2')->applyFromArray($headerStyle);
                $activeSheet->mergeCells($x . '3:' . $j . '3')->setCellValue($x . '3', $tanggal_awal->format('d-m-Y'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
                for ($r = 0; $r < 3; $r++) {
                    $x++;
                    $j++;
                }
                $tanggal_awal->add(new DateInterval('P1D'));
            }

            $tempJ = $j;
            $tempJ++;
            $tempJ++;
            $tempJ++;
            $tempJ++;
            $tempJ++;
            $tempJ++;
            $tempJ++;
            $tempJ++;
            $tempJ++;
            $activeSheet->mergeCells($x . '2:' . $tempJ . '2')->setCellValue($x . '2', Yii::t('frontend', 'Jumlah'))->getStyle($x . '2:' . $tempJ . '2')->applyFromArray($headerStyle);

            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'Masuk'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'Alfa'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'Ijin'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'Libur'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'Undefined'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'Cuti'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'Jam Lembur'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'S2+S3'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'Uang Lembur'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);
            $x++;
            $j++;
            $activeSheet->setCellValue($x . '3', Yii::t('frontend', 'UML'))->getStyle($x . '3:' . $j . '3')->applyFromArray($headerStyle);

            $highestColumn = $activeSheet->getHighestColumn('3');

            $highestColumn--;
            $header = Yii::t('frontend', 'REKAP ABSENSI');
            $periode = date('d-m-Y', strtotime($this->tanggal_awal)) . ' s/d ' . date('d-m-Y', strtotime($this->tanggal_akhir));
            $activeSheet->mergeCells('H1:' . $highestColumn . '1')->setCellValue('H1', $header . '  ' . $periode)->getStyle('H1:' . $highestColumn . '1')->applyFromArray($headerStyle);


            $row = 4;
            $no = 1;

            foreach ($this->laporan() as $pegawai) {
                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $pegawai['pegawai']['id_pegawai'])->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('C' . $row, $pegawai['pegawai']['nama'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $pegawai['pegawai']['bisnis_unit'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $pegawai['pegawai']['jenis_perjanjian'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $pegawai['pegawai']['golongan'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
//            $activeSheet->setCellValue('G' . $row, 's')->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                $dayColumn = 'G';
                $tanggal_awal = new DateTime($this->tanggal_awal);
                $tanggal_akhir = new DateTime($this->tanggal_akhir);
                $hitungShift23 = 0;
                $hitungJumlahLembur = 0;
                $modelMasterData = new MasterDataDefault();
                $modelMasterData->pegawai_id = $pegawai['pegawai']['id'];
                $pegawaiId = $pegawai['pegawai']['id'];
//
//            $modelGapok = new PGajiPokok();
//            $modelGapok->pegawai_id = $pegawaiId;
//
//            $modelAdminBank = new PAdminBank();
//            $modelAdminBank->pegawai_id = $pegawaiId;

                $upahLembur = 0;
                $uml = 0;
                while ($tanggal_awal <= $tanggal_akhir) {
                    if (isset($pegawai['absensis'][$tanggal_awal->format('Y-m-d')])) {

                        $keyTanggal = $tanggal_awal->format('Y-m-d');
                        $absensi = $pegawai['absensis'][$keyTanggal];
                        if ($absensi['status_absensi'] == Absensi::MASUK && ($absensi['shift'] == 2 || $absensi['shift'] == 3 || $absensi['spl_shift'] == 2 || $absensi['spl_shift'] == 3)) {
                            $hitungShift23++;
                        }
                        $statusAbsensi = $absensi['status_absensi'] ? $this->getStatusAbsensi($absensi['status_absensi'], $absensi['time_table_id'], $keyTanggal, $pegawai['pegawai']['id'], $absensi['spl_id']) : '-';
                        $activeSheet->getColumnDimension($dayColumn)->setWidth(5);
                        $activeSheet->setCellValue($dayColumn . $row, $statusAbsensi)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineLeftBorder));
                        $dayColumn++;

                        $jamLembur = $absensi['status_absensi'] ? $absensi['jam_lembur'] : 0;
                        $hitungJumlahLembur += (float)$jamLembur;
                        $activeSheet->getColumnDimension($dayColumn)->setWidth(5);
                        $activeSheet->setCellValue($dayColumn . $row, $jamLembur)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineLeftBorder));
                        //hitung uang lembur
                        $upahLembur += MasterDataDefault::_hitungUpahLembur($pegawaiId, (float)$jamLembur, $keyTanggal);
                        $dayColumn++;

                        $hitungUML = $absensi['jam_lembur'] ? $this->hitungUML($pegawai['pegawai']['id'], $keyTanggal) : '-';
                        $activeSheet->getColumnDimension($dayColumn)->setWidth(5);
                        $activeSheet->setCellValue($dayColumn . $row, $hitungUML)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                        //hitung UML
                        $uml += PUangMakan::_hitungUangMakanLembur($pegawaiId, $keyTanggal);
                        $dayColumn++;
                    } else {
                        $activeSheet->getColumnDimension($dayColumn)->setWidth(5);
                        $activeSheet->setCellValue($dayColumn . $row, '')->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineLeftBorder));
                        $dayColumn++;
                        $activeSheet->getColumnDimension($dayColumn)->setWidth(5);
                        $activeSheet->setCellValue($dayColumn . $row, '')->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineLeftBorder));
                        $dayColumn++;
                        $activeSheet->getColumnDimension($dayColumn)->setWidth(5);
                        $activeSheet->setCellValue($dayColumn . $row, '')->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                        $dayColumn++;
                    }
                    $tanggal_awal->add(new DateInterval('P1D'));
                }

                $hitungStatusKehadiran = array_count_values($pegawai['status_kehadirans']);
                $activeSheet->setCellValue($dayColumn . $row, $hitungStatusKehadiran[Absensi::MASUK] ?? 0)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                $dayColumn++;
                $activeSheet->setCellValue($dayColumn . $row, $hitungStatusKehadiran[Absensi::ALFA] ?? 0)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                $dayColumn++;
                $activeSheet->setCellValue($dayColumn . $row, $hitungStatusKehadiran[Absensi::IJIN] ?? 0)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                $dayColumn++;
                $activeSheet->setCellValue($dayColumn . $row, $hitungStatusKehadiran[Absensi::LIBUR] ?? 0)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                $dayColumn++;
                $activeSheet->setCellValue($dayColumn . $row, $hitungStatusKehadiran[Absensi::ERROR] ?? 0)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                $dayColumn++;
                $activeSheet->setCellValue($dayColumn . $row, $hitungStatusKehadiran[Absensi::CUTI] ?? 0)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                $dayColumn++;
                $activeSheet->setCellValue($dayColumn . $row, $hitungJumlahLembur)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
                $dayColumn++;
                $activeSheet->setCellValue($dayColumn . $row, $hitungShift23)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder));
//            $dayColumn++;
//            $activeSheet->setCellValue($dayColumn . $row, $upahLembur)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder))->getNumberFormat()->setFormatCode('#,##0.00');
//            $dayColumn++;
//            $activeSheet->setCellValue($dayColumn . $row, $uml)->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder))->getNumberFormat()->setFormatCode('#,##0');
//            $dayColumn++;
//            $activeSheet->setCellValue($dayColumn . $row, $pegawai['pegawai']['gaji_pokok'])->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder))->getNumberFormat()->setFormatCode('#,##0');
//            $dayColumn++;
//            $activeSheet->setCellValue($dayColumn . $row, $pegawai['pegawai']['admin_bank'])->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder))->getNumberFormat()->setFormatCode('#,##0');
//            $dayColumn++;
//            $activeSheet->setCellValue($dayColumn . $row, $pegawai['pegawai']['tunj_pulsa'])->getStyle($dayColumn . $row)->applyFromArray(array_merge($center, $outLineRightBorder))->getNumberFormat()->setFormatCode('#,##0');

                $row++;
                $no++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($font);

            $writer = new Xlsx($spreadsheet);
            $writer->save(Yii::getAlias('@frontend') . $laporanAbsensi::filePathLaporan . $laporanAbsensi->id . '.xlsx');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }


    /**
     * @param int $statusAbsensi
     * @param int $timeTableId
     * @param string $tanggal
     * @param string $pegawaiId
     * @param int $splId
     * @return mixed|string
     */
    private function getStatusAbsensi(int $statusAbsensi, int $timeTableId = 0, string $tanggal = '', string $pegawaiId = '', $splId = 0)
    {
        if ($statusAbsensi == Absensi::ALFA) {
            return $this->_singkatanAlfa;
        }

        if ($statusAbsensi == Absensi::MASUK) {
            $modelTimeTable = TimeTable::findOne($timeTableId);
            if ($modelTimeTable) {
                return $this->_singkatanShift[$modelTimeTable->shift];
            } else {
                $modelSpl = Spl::findOne($splId);
                if ($modelSpl) {
                    if ($modelSpl->splTemplate->kategori == SplTemplate::LEMBUR_KHUSUS) {
                        return $this->_singkatanLemburKhusus . $modelSpl->splTemplate->tunjangan_shift;
                    }
                }
            }
        }

        if ($statusAbsensi == Absensi::IJIN) {
            $modelAbsensi = Absensi::findOne(['pegawai_id' => $pegawaiId, 'tanggal' => $tanggal]);
            $izinTanpaHadir = $modelAbsensi->getIzinTanpaHadir();
            if ($izinTanpaHadir) {
                $data = explode(" -> ", $izinTanpaHadir);
                return $data[1];
            }
            $absensiTidakLengkap = $modelAbsensi->getIzinAbsensiTidakLengkap();
            if ($absensiTidakLengkap) {
                $data = explode(" -> ", $absensiTidakLengkap);
                return $data[1];
            }
            $setengahHari = $modelAbsensi->getIzinSetengahHari();
            if ($setengahHari) {
                $data = explode(" -> ", $setengahHari);
                return $data[1];
            }
            //untuk testing error
//            if (!isset($modelAbsensi->_status_kehadiran[$this->status_kehadiran])){
//                return 'testing';
//            }
            return $modelAbsensi->_status_kehadiran[$this->status_kehadiran];
        }

        if ($statusAbsensi == Absensi::LIBUR) {
            return $this->_singkatanLibur;
        }

        if ($statusAbsensi == Absensi::ERROR) {
            $modelPegawai = Pegawai::findOne($pegawaiId);
            $modelSpl = new Spl();
            $splDefault = $modelSpl->getSplByPegawai($tanggal, $modelPegawai);
            if ($splDefault) {
                return $this->_singkatanDetailError[self::SPB];
            }
            $splApprove = $modelSpl->getSplByPegawai($tanggal, $modelPegawai, [Status::APPROVED]);
            if ($splApprove) {
                return $this->_singkatanDetailError[self::SPT];
            }
            return $this->_singkatanError;
        }


        if ($statusAbsensi == Absensi::CUTI) {

            return $this->_singkatanCuti;
        }

        return '--';
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggal
     * @return string
     */
    public function hitungUML(string $pegawaiId, string $tanggal)
    {
        $modelAbsensi = Absensi::findOne(['pegawai_id' => $pegawaiId, 'tanggal' => $tanggal]);
        if ($modelAbsensi) {
            $prefix = 'UML';
            $pengali = $modelAbsensi->hitungPengaliUangMakanLembur();
            return $pengali ? $prefix . $pengali : '-';
        }
        return '-';

    }

    /**
     * @return array
     */
    public function prosesData()
    {
        $modelAbsensi = new Absensi();
        $queryPegawai = Pegawai::find()->select('id');
        $filterTree = false;
        if ($filterTree) {
            $dataPegawai = $queryPegawai->asArray()->all();
            $dataPegawai = array_map(function ($a) {
                return $a['id'];
            }, $dataPegawai);
            $dataPegawai = array_merge($dataPegawai, $this->pegawai_ids);
        } else {
            $dataPegawai = $this->pegawai_ids;
        }

        $info = [];
        try {
            $info = $modelAbsensi->prosesData($this->tanggal_awal, $this->tanggal_akhir, null, $dataPegawai);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $info;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 05/02/2019
 * Time: 20:09
 */

namespace frontend\models\form;


use common\models\Divisi;
use common\models\Unit;
use frontend\models\Spl;
use mdm\admin\components\Helper;
use yii\base\Model;

/**
 * Class SplForm
 * @package frontend\models\form
 *
 * @property int $unit
 * @property int $divisi
 * @property string $date
 *
 * @property string $unit_
 * @property string $divisi_
 */
class SplForm extends Model
{

    public $unit;
    public $divisi;
    public $date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['unit', 'divisi'], 'safe']
        ];
    }

    /**
     * @return string
     */
    public function getUnit_()
    {
        if ($this->unit) {
            return Unit::findOne($this->unit)->nama;
        }
        return '-';
    }

    /**
     * @return string
     */
    public function getDivisi_()
    {
        if ($this->divisi) {
            return Divisi::findOne($this->divisi)->nama;
        }
        return '-';
    }


    /**
     * @param array $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public function dailyReport(array $params)
    {
        $model = new self();
        $model->load($params);

        $spls = [];
        if ($model->validate()) {
            $querySpl = Spl::find();
            $querySpl->andWhere(['tanggal' => $model->date]);
            if (!empty($model->unit)) {
                $querySpl->andWhere(['unit' => $model->unit]);
            }
            if (!empty($model->divisi)) {
                $querySpl->andWhere(['divisi' => $model->divisi]);
            }
            // jika bukan user yg dapat melakukan approve, maka dhanya dapat melihat SPL yg dibuat sendiri
            if (!Helper::checkRoute('/spl/approval-form') && !Helper::checkRoute('/spl/view-all-spl')) {
                $querySpl->andWhere(['created_by' => \Yii::$app->user->id]);
            }

            $spls = $querySpl->all();
        }
        return $spls;

    }
}
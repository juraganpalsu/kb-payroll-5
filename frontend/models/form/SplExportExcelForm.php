<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 05/02/2019
 * Time: 20:09
 */

namespace frontend\models\form;


use common\components\Hari;
use common\components\Status;
use common\models\Golongan;
use common\models\SistemKerja;
use Exception;
use frontend\models\Spl;
use frontend\models\SplDetail;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class SplForm
 * @property string $tanggal
 * @property integer $sistem_kerja
 * @property integer $golongan_id
 * @property integer $bisnis_unit
 * @property integer $kategori
 * @property string $tanggalAwal
 * @property string $tanggalAKhir
 *
 * @property string $sistemKerja
 * @property string $golongan
 * @property string $bisnisUnit
 * @property array $_kategori
 * @property string $spl_alasan_id
 *
 */
class SplExportExcelForm extends Model
{
    public $sistem_kerja;
    public $golongan_id;
    public $tanggal;
    public $bisnis_unit;
    public $kategori;
    public $spl_alasan_id;

    const LEMBUR_BIASA = 1;
    const LEMBUR_INSENTIF = 2;

    public $_kategori = [self::LEMBUR_BIASA => 'LEMBUR BIASA', self::LEMBUR_INSENTIF => 'LEMBUR INSENTIF'];


    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            [['tanggal', 'kategori'], 'required'],
            [['sistem_kerja', 'golongan_id', 'tanggal', 'bisnis_unit', 'kategori', 'spl_alasan_id'], 'safe']
        ];
    }


    /**
     * @return string
     */
    public function getSistemKerja(): string
    {
        if ($this->sistem_kerja) {
            return SistemKerja::findOne($this->sistem_kerja)->nama;
        }
        return '-';
    }

    /**
     * @return string
     */
    public function getGolongan(): string
    {
        if ($this->golongan_id) {
            return Golongan::findOne($this->golongan_id)->nama;
        }
        return '-';
    }

    /**
     * @return PerjanjianKerja
     */
    public static function modelPerjanjianKerja(): PerjanjianKerja
    {
        return new PerjanjianKerja();
    }

    /**
     * @param array $statuses
     * @return array|ActiveRecord[]
     */
    public function getSpl(array $statuses = [Status::SUBMITED, Status::APPROVED, Status::FINISHED]): array
    {
        $querySpl = Spl::find();

        if (!empty($this->kategori)) {
            $querySpl->andWhere(['kategori' => $this->kategori]);
        }

        if (!empty($this->spl_alasan_id)) {
            $querySpl->andWhere(['spl_alasan_id' => $this->spl_alasan_id]);
        }

        $tanggal = explode('s/d', $this->tanggal);
        $tanggalAwal = date('Y-m-d', strtotime($tanggal[0]));
        $tanggalAkhir = date('Y-m-d', strtotime($tanggal[1]));
        $querySpl->andWhere(['BETWEEN', 'spl.tanggal', $tanggalAwal, $tanggalAkhir]);
        $querySpl->andWhere(['IN', 'spl.last_status', $statuses]);

        if (!empty($this->sistem_kerja)) {
            $querySpl->joinWith(['splDetails' =>
                function (ActiveQuery $q) {
                    $q->joinWith(['pegawai' => function (ActiveQuery $q) {
                        $q->joinWith(['pegawaiSistemKerjas' => function (ActiveQuery $x) {
                            return $x->andWhere(['pegawai_sistem_kerja.sistem_kerja_id' => $this->sistem_kerja]);
                        }]);
                    }]);
                }
            ]);
        }

        if (!empty($this->bisnis_unit)) {
            $querySpl->joinWith(['splDetails' =>
                function (ActiveQuery $q) {
                    $q->joinWith(['pegawai' => function (ActiveQuery $q) {
                        $q->joinWith(['perjanjianKerjas' => function (ActiveQuery $x) {
                            return $x->andWhere(['perjanjian_kerja.kontrak' => $this->bisnis_unit]);
                        }]);
                    }]);
                }
            ]);
        }

        if (!empty($this->golongan_id)) {
            $querySpl->joinWith(['splDetails' =>
                function (ActiveQuery $q) {
                    $q->joinWith(['pegawai' => function (ActiveQuery $q) {
                        $q->joinWith(['pegawaiGolongans' => function (ActiveQuery $x) {
                            return $x->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);
                        }]);
                    }]);
                }
            ]);
        }


        return $querySpl->all();
    }


    /**
     * @param array $spls
     * @param bool $saveAsFile
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function downloadExcel(array $spls, bool $saveAsFile = false): bool
    {
        if ($spls) {
            $spreadsheet = new Spreadsheet();
            $Excel_writer = new Xls($spreadsheet);
            $headerStyle = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
                'fill' => [
                    'fillType' => Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => 'FFA0A0A0',
                    ],
                    'endColor' => [
                        'argb' => 'FFFFFFFF',
                    ],
                ],
            ];

            $center = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ]
            ];

            $outLineBottomBorder = [
                'borders' => array(
                    'bottom' => array(
                        'borderStyle' => Border::BORDER_THIN,
                    ),
                ),
            ];

            $borderInside = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_DOTTED,
                    ],
                ],
            ];

            $font = array(
                'font' => array(
                    'size' => 9,
                    'name' => 'Calibri'
                ));

            $spreadsheet->setActiveSheetIndex(0);
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(10);
            $activeSheet->getColumnDimension('B')->setWidth(15);
            $activeSheet->getColumnDimension('C')->setWidth(15);
            $activeSheet->getColumnDimension('D')->setWidth(10);
            $activeSheet->getColumnDimension('E')->setWidth(10);
            $activeSheet->getColumnDimension('F')->setWidth(30);
            $activeSheet->getColumnDimension('G')->setWidth(10);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(10);
            $activeSheet->getColumnDimension('J')->setWidth(8);
            $activeSheet->getColumnDimension('K')->setWidth(10);
            $activeSheet->getColumnDimension('L')->setWidth(10);
            $activeSheet->getColumnDimension('M')->setWidth(20);
            $activeSheet->getColumnDimension('N')->setWidth(30);
            $activeSheet->getColumnDimension('O')->setWidth(50);
            $activeSheet->getColumnDimension('P')->setWidth(12);
            $activeSheet->getColumnDimension('Q')->setWidth(9);
            $activeSheet->getColumnDimension('R')->setWidth(12);
            $activeSheet->getColumnDimension('S')->setWidth(12);


            $activeSheet->setCellValue('A1', Yii::t('frontend', 'Seq Code'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Departemen'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Id Pegawai'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Nama'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Sistem Kerja'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Golongan'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Hari '))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', Yii::t('frontend', 'Tanggal'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', Yii::t('frontend', 'Jumlah Jam'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', Yii::t('frontend', 'SPL Template'))->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('N1', Yii::t('frontend', 'Kategori Alasan'))->getStyle('N1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('O1', Yii::t('frontend', 'Keterangan'))->getStyle('O1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('P1', Yii::t('frontend', 'Klaim Quaker'))->getStyle('P1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('Q1', Yii::t('frontend', 'Last Status'))->getStyle('Q1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('R1', Yii::t('frontend', 'Kategori'))->getStyle('R1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('S1', Yii::t('frontend', 'Gedung'))->getStyle('S1')->applyFromArray($headerStyle);

            $row = 2;
            $no = 1;

            /** @var Spl $spl */
            foreach ($spls as $spl) {
                /** @var SplDetail $splDetail */
                foreach ($spl->splDetails as $splDetail) {
                    $pegawai = $splDetail->pegawai;

                    if (!$pegawai) {
                        continue;
                    }
                    $sistemKerja = '';
                    $departemen = '';
                    $jabatan = '';
                    $fungsiJabatan = '';
                    $kategori = $spl->_kategori[$spl->kategori];

                    if ($pegawai->getPegawaiSistemKerjaUntukTanggal($splDetail->spl->tanggal)) {
                        if ($pegawaiSistemKerja = $pegawai->getPegawaiSistemKerjaUntukTanggal($splDetail->spl->tanggal)) {
                            $sistemKerja = $pegawaiSistemKerja->sistemKerja->nama;
                        }
                    }
                    if ($pegawai->getPegawaiStrukturUntukTanggal($splDetail->spl->tanggal)) {
                        if ($pegawaiStruktur = $pegawai->getStrukturTerakhir()) {
                            $departemen = $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen->nama : '' : '';
                            $jabatan = $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->jabatan->nama : '' : '';
                            $fungsiJabatan = $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->fungsiJabatan->nama : '' : '';
                        }
                    }

                    $activeSheet->setCellValue('A' . $row, $splDetail->spl->seq_code)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('B' . $row, $departemen)->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('C' . $row, $jabatan)->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('D' . $row, $fungsiJabatan)->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('E' . $row, $pegawai->getPerjanjianKerjaTerakhir()->id_pegawai)->getStyle('E' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('F' . $row, $pegawai->nama)->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('G' . $row, $sistemKerja)->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('H' . $row, $pegawai->getPerjanjianKerjaTerakhir()->namaKontrak)->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('I' . $row, $pegawai->getGolonganTerakhir()->golongan->nama)->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('J' . $row, Hari::getHariIndonesia(date('D', strtotime($splDetail->spl->tanggal))))->getStyle('J' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('K' . $row, date('d-m-Y', strtotime($splDetail->spl->tanggal)))->getStyle('K' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('L' . $row, $splDetail->spl->splTemplate->jumlahKonversi)->getStyle('L' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('M' . $row, $splDetail->spl->splTemplate->nama)->getStyle('M' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('N' . $row, $splDetail->spl->splAlasan->nama)->getStyle('N' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('O' . $row, $splDetail->spl->keterangan)->getStyle('O' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('P' . $row, Status::activeInactive($splDetail->spl->klaim_quaker))->getStyle('P' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('Q' . $row, Status::statuses($splDetail->spl->last_status))->getStyle('Q' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('R' . $row, $kategori)->getStyle('R' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('S' . $row, $pegawai->getAreaKerjaTerakhir()->areaGedung())->getStyle('S' . $row)->applyFromArray(array_merge($borderInside));

                    $row++;
                    $no++;
                }
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'S' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'S' . $row)->applyFromArray($font);

            if ($saveAsFile) {
                $Excel_writer->save(Yii::getAlias('@console/runtime/spl/') . 'data-spl-' . date('d-m-Y') . '.xls');
            } else {
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename=rekap-spl' . date('YmdHis') . '.xls');
                header('Cache-Control: max-age=0');
                try {
                    ob_end_clean();
                    $Excel_writer->save('php://output');
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        }
        return true;
    }
}
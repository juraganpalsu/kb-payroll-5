<?php

namespace frontend\models\status;

use common\components\Status;
use frontend\models\status\base\SplDetailApproval as BaseSplDetailApproval;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "spl_detail_approval".
 *
 * @property SplDetailApproval $detailApproval
 */
class SplDetailApproval extends BaseSplDetailApproval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'spl_detail_id'], 'required'],
            [['tipe', 'level', 'urutan', 'notifikasi', 'created_by', 'created_by_struktur'], 'integer'],
            [['tanggal', 'created_at'], 'safe'],
            [['komentar'], 'string'],
            [['spl_detail_approval_id'], 'default', 'value' => '-'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'spl_detail_id', 'spl_detail_approval_id', 'created_by_pk'], 'string', 'max' => 32]
        ];
    }


    /**
     * @return mixed
     */
    public function getApproval()
    {
        return Status::approvalStatus($this->tipe);
    }


    /**
     * Melakukan approval,
     * Jika yg melakukan approval mempunyai childs, maka semua childs yg belum melakukan approval akan di tandai SKIPPED
     * @param int $approval_type tipe approval
     * @param string $comment komentar jika detail ditolak
     */
    public function approval(int $approval_type = Status::APPROVE, string $comment = '')
    {
        $model = self::findOne($this->id);
        if ($model->tipe == Status::DEFLT) {
            $model->tanggal = $model->tipe == Status::DEFLT ? date('Y-m-d H:i:s') : $model->tanggal;
            $model->tipe = $model->tipe == Status::DEFLT ? $approval_type : $model->tipe;
            $model->komentar = !empty($comment) && $model->tipe == Status::REJECT ? $comment : $model->komentar;
            if ($model->save()) {
                if ($parent = $model->splDetailApproval) {
                    if ($parent->tipe == Status::DEFLT && ($model->tipe == Status::REJECT || $model->tipe == Status::SKIP)) {
                        $model->splDetailApproval->approval(Status::SKIP);
                    }
                }
                if ($child = $model->detailApproval) {
                    if ($child->tipe == Status::DEFLT) {
                        $model->detailApproval->approval(Status::SKIP);
                    }
                }
                $model->createStatusDetail();
                $model->createApprovalSpl();
            }
        }
    }

    /**
     * Membuat status untuk 'detail'
     *
     * Jika yg melakukan approval ada di puncak tree approval, maka status 'detail' akan di update sesuai
     * dengan approval nya
     *
     */
    public function createStatusDetail()
    {
        if (!$this->spl_detail_approval_id) {
            $approvalType = $this->tipe == Status::APPROVE ? Status::APPROVED : Status::REJECTED;
            $this->splDetail->createStatus($approvalType);
            $this->createStatusSpl();
        }
    }

    /**
     *
     * Create Approval SPL
     */
    public function createApprovalSpl()
    {
        $spl = $this->splDetail->spl;
        if (!$spl->checkStatusApprovalByUser()) {
            if ($spl->checkStatusApprovalByUser(Status::APPROVE)) {
                $spl->createApproval();
            } else {
                $spl->createApproval();
            }
        }
    }

    /**
     * Membuat status untuk Spl
     *
     * Jika semua detail telah dilakukan aksi approval, maka buat status untuk Spl
     * Jika ada satu item saja yg di approve, maka Spl berstatus APPROVE, tetapi jika tidak ada , maka statusnya REJECT
     *
     */
    public function createStatusSpl()
    {
        $spl = $this->splDetail->spl;
        if (!$spl->checkStatusApprovalByUser()) {
            if ($spl->checkStatusApprovalByUser(Status::APPROVE)) {
                $spl->createStatus(Status::APPROVED);
            } else {
                $spl->createStatus(Status::REJECTED);
            }
        }
    }

    /**
     * Relasi has one untuk nested untuk mengambil anak2 nya
     *
     * @return ActiveQuery
     */
    public function getDetailApproval()
    {
        return $this->hasOne(SplDetailApproval::class, ['spl_detail_approval_id' => 'id']);
    }

}

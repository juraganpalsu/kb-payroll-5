<?php

namespace frontend\models\status;

use Yii;
use \frontend\models\status\base\SplDetailStatus as BaseSplDetailStatus;

/**
 * This is the model class for table "spl_detail_status".
 */
class SplDetailStatus extends BaseSplDetailStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'created_by_struktur'], 'integer'],
            [['spl_detail_id'], 'required'],
            [['created_at'], 'safe'],
            [['spl_detail_id', 'created_by_pk'], 'string', 'max' => 32]
        ];
    }

}

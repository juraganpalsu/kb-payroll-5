<?php

namespace frontend\models\status;

use common\components\Status;
use frontend\models\status\base\SplApproval as BaseSplApproval;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "spl_approval".
 *
 * @property SplApproval $approval
 */
class SplApproval extends BaseSplApproval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'spl_id'], 'required'],
            [['tipe', 'level', 'urutan', 'notifikasi', 'created_by', 'created_by_struktur'], 'integer'],
            [['tanggal', 'created_at'], 'safe'],
            [['komentar'], 'string'],
            [['tipe'], 'default', 'value' => 0],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'spl_id', 'spl_approval_id', 'created_by_pk'], 'string', 'max' => 32]
        ];
    }

    /**
     * Melakukan approval,
     * Jika yg melakukan approval mempunyai childs, maka semua childs yg belum melakukan approval akan di tandai SKIPPED
     * @param int $approval_type tipe approval
     * @param string $comment komentar jika detail ditolak
     */
    public function createApproval(int $approval_type = Status::APPROVE, string $comment = '')
    {
        $model = self::findOne($this->id);
        if ($model->tipe == Status::DEFLT) {
            $model->tanggal = $model->tipe == Status::DEFLT ? date('Y-m-d H:i:s') : $model->tanggal;
            $model->tipe = $model->tipe == Status::DEFLT ? $approval_type : $model->tipe;
            $model->komentar = !empty($comment) && $model->tipe == Status::REJECT ? $comment : $model->komentar;
            if ($model->save()) {
                if ($parent = $model->splApproval) {
                    if ($parent->tipe == Status::DEFLT && ($model->tipe == Status::REJECT || $model->tipe == Status::SKIP)) {
                        $model->splApproval->createApproval(Status::SKIP);
                    }
                }
                if ($child = $model->approval) {
                    if ($child->tipe == Status::DEFLT) {
                        $model->approval->createApproval(Status::SKIP);
                    }
                }
            }
        }
    }

    /**
     * Relasi has one untuk nested untuk mengambil anak2 nya
     *
     * @return ActiveQuery
     */
    public function getApproval()
    {
        return $this->hasOne(SplApproval::class, ['spl_approval_id' => 'id']);
    }
}

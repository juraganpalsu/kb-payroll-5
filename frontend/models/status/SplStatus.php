<?php

namespace frontend\models\status;

use Yii;
use \frontend\models\status\base\SplStatus as BaseSplStatus;

/**
 * This is the model class for table "spl_status".
 */
class SplStatus extends BaseSplStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'created_by_struktur'], 'integer'],
            [['spl_id'], 'required'],
            [['created_at'], 'safe'],
            [['spl_id', 'created_by_pk'], 'string', 'max' => 32],
        ];
    }

}

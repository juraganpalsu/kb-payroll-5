<?php

namespace frontend\models\status\base;

use common\components\CreatedByBehavior;
use common\models\Spl;
use frontend\models\Pegawai;
use frontend\models\User;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\struktur\models\Struktur;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "spl_approval".
 *
 * @property string $id
 * @property integer $tipe
 * @property string $tanggal
 * @property string $komentar
 * @property integer $level
 * @property integer $urutan
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_struktur_id
 * @property string $spl_id
 * @property string $spl_approval_id
 * @property integer $notifikasi
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 *
 * @property \frontend\models\status\Spl $spl
 * @property \frontend\models\status\SplApproval $splApproval
 * @property \frontend\models\status\SplApproval[] $splApprovals
 * @property Pegawai $pegawai
 * @property PerjanjianKerja $perjanjianKerja
 * @property PegawaiStruktur $pegawaiStruktur
 */
class SplApproval extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'spl_id'], 'required'],
            [['tipe', 'level', 'urutan', 'notifikasi', 'created_by', 'created_by_struktur'], 'integer'],
            [['tanggal', 'created_at'], 'safe'],
            [['komentar'], 'string'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'spl_id', 'spl_approval_id', 'created_by_pk'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spl_approval';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tipe' => Yii::t('frontend', 'Tipe'),
            'tanggal' => Yii::t('frontend', 'Tanggal'),
            'komentar' => Yii::t('frontend', 'Komentar'),
            'level' => Yii::t('frontend', 'Level'),
            'urutan' => Yii::t('frontend', 'Level'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'spl_id' => Yii::t('frontend', 'Spl ID'),
            'spl_approval_id' => Yii::t('frontend', 'Spl Approval ID'),
            'notifikasi' => Yii::t('frontend', 'Notifikasi'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSpl()
    {
        return $this->hasOne(Spl::class, ['id' => 'spl_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplApproval()
    {
        return $this->hasOne(\frontend\models\status\SplApproval::class, ['id' => 'spl_approval_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplApprovals()
    {
        return $this->hasMany(\frontend\models\status\SplApproval::class, ['spl_approval_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getCreatedByPk()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'created_by_pk']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedByStruktur()
    {
        return $this->hasOne(Struktur::class, ['id' => 'created_by_struktur']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
        ];
    }
}

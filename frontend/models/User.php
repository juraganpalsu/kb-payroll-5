<?php

/**
 * Created with love at Sep 1, 2018
 * @author jiwa
 */

namespace frontend\models;

use frontend\modules\pegawai\models\UserPegawai;
use frontend\modules\struktur\models\Struktur;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Description of User
 *
 * @property string $initialName
 * @property UserPegawai $userPegawai
 * @property \frontend\modules\pegawai\models\Pegawai $pegawai
 *
 */
class User extends \dektrium\user\models\User
{

    /**
     *
     * @return string acronym
     */
    public function getInitialName()
    {
        $profileName = $this->profile->name;
        $acronym = '';
        if (!is_null($profileName)) {
            $words = explode(' ', $profileName);
            foreach ($words as $w) {
                $acronym .= $w[0];
            }
        } else {
            $acronym = $this->username;
        }
        return substr($acronym, 0, 2);
    }

    /**
     * @return bool|\frontend\modules\pegawai\models\Pegawai
     */
    public function getPegawai()
    {
        if ($userPegawai = $this->userPegawai) {
            if ($userPegawai->pegawai) {
                return $userPegawai->pegawai;
            }
        }
        return false;
    }

    /**
     *
     * Memanggil object user secara static, jika pegawai tidak ditemukan , maka return 0
     *
     * @return \frontend\modules\pegawai\models\Pegawai|int
     */
    public static function pegawai()
    {
        $user = new User();
        $user->id = Yii::$app->user->id;
        return $user->pegawai ?? 0;
    }

    /**
     * @return ActiveQuery
     */
    public function getUserPegawai()
    {
        return $this->hasOne(UserPegawai::class, ['user_id' => 'id']);
    }

    /**
     * Add default role untuk unser baru
     *
     * @param string $insert
     * @param string $changedAttributes
     * @return boolean
     */
//    public function afterSave($insert, $changedAttributes) {
//        parent::afterSave($insert, $changedAttributes);
//        if ($insert) {
//            $auth = \Yii::$app->authManager;
//            $authorRole = $auth->getRole('Pelamar');
//            $auth->assign($authorRole, $this->getId());
//        }
//        return true;
//    }

    /**
     *
     * Validasi user yg login, karena kedepannya harus mempunyai data yg valid untuk keperluan logging
     *
     * @return bool
     */
    public function validasiKelengkapanAkun()
    {
        $tervalidasi = false;
        if ($pegawai = $this->pegawai) {
            if ($pegawai->perjanjianKerja && $pegawai->pegawaiStruktur) {
                $tervalidasi = true;
            }
        }
        return $tervalidasi;
    }

    /**
     *
     * Mengambil data struktur yg aktif pada user yg sedang login
     *
     * @param bool $sebagaiObjek
     * @return bool|Struktur|string
     */
    public function getStrukturSaya(bool $sebagaiObjek = true)
    {
        if ($this->validasiKelengkapanAkun()) {
            $struktur = $this->pegawai->pegawaiStruktur->struktur;
            if ($sebagaiObjek) {
                return $struktur;
            } else {
                return $struktur->id;
            }
        }
        return false;
    }

    /**
     *
     * Mengambil struktur bawahan dari user yg sedang login
     *
     * @param bool $hanyaId
     * @param bool $masukkanStrukturSaya
     * @return array|ActiveRecord[]
     */
    public function getStrukturBawahan(bool $hanyaId = true, bool $masukkanStrukturSaya = true)
    {
        $strukturSaya = $this->getStrukturSaya();
        $bawahanIds = [];
        if (is_object($strukturSaya)) {
            $bawahans = $strukturSaya->children()->all();
            if (!$hanyaId) {
                return $bawahans;
            }
            /** @var Struktur $bawahan */
            foreach ($bawahans as $bawahan) {
                $bawahanIds[] = $bawahan->id;
            }
        }
        if ($masukkanStrukturSaya) {
            return array_merge($bawahanIds, [$this->getStrukturSaya(false)]);
        }
        return $bawahanIds;
    }

    /**
     * @param bool $hanyaId
     * @param bool $masukkanStrukturSaya
     * @return array|ActiveRecord[]
     */
    public function getStrukturAtasan(bool $hanyaId = true, bool $masukkanStrukturSaya = true)
    {
        $strukturSaya = $this->getStrukturSaya();
        $atasanIds = [];
        if (is_object($strukturSaya)) {
            $atasans = $strukturSaya->parents()->all();
            if (!$hanyaId) {
                return $atasans;
            }
            /** @var Struktur $atasan */
            foreach ($atasans as $atasan) {
                $atasanIds[] = $atasan->id;
            }
        }
        if ($masukkanStrukturSaya) {
            return array_merge($atasanIds, [$this->getStrukturSaya(false)]);
        }
        return $atasanIds;
    }

    public function getGudang()
    {

    }

}

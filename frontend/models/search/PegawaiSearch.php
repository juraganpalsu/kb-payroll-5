<?php

namespace frontend\models\search;

use common\components\Status;
use common\models\PegawaiSistemKerja;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\models\search\PegawaiSearch represents the model behind the search form about `frontend\models\Pegawai`.
 * @property string $sistemKerja_
 * @property integer $sistem_kerja
 * @property integer $id_pegawai
 * @property integer $busines_unit
 * @property integer $jenis_kontrak
 * @property integer $golongan
 * @property integer $struktur
 * @property string $departemen
 * @property integer $area_kerja
 *
 */
class PegawaiSearch extends Pegawai
{

    public $sistem_kerja;

    public $struktur;

    public $departemen;

    public $sistemKerja_;

    public $id_pegawai;

    public $busines_unit;

    public $jenis_kontrak;

    public $golongan;

    public $area_kerja;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'area_kerja', 'nama_lengkap', 'id_pegawai', 'busines_unit', 'is_resign', 'tempat_lahir', 'created_at', 'updated_at', 'deleted_at', 'struktur', 'departemen'], 'safe'],
            [['sistem_kerja', 'id_pegawai', 'busines_unit', 'created_by', 'updated_by', 'deleted_by', 'lock', 'sistemKerja_', 'status_pernikahan', 'jenis_kontrak', 'golongan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, $filterResign = false): ActiveDataProvider
    {
        $query = Pegawai::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($filterResign) {
            $query->andWhere(['pegawai.is_resign' => Status::TIDAK]);
        }

        if (!empty($this->sistem_kerja)) {
            $query->joinWith(['pegawaiSistemKerja' => function (ActiveQuery $x) {
                return $x->andWhere(['pegawai_sistem_kerja.sistem_kerja_id' => $this->sistem_kerja]);
            }]);
        }


        if (!empty($this->id_pegawai)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['LIKE', 'perjanjian_kerja.id_pegawai', $this->id_pegawai]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if (!empty($this->jenis_kontrak)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.jenis_perjanjian' => $this->jenis_kontrak]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        if (!empty($this->struktur)) {
            $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) {
                $query->joinWith(['struktur' => function (ActiveQuery $query) {
                    $query->andWhere(['like', 'struktur.name', $this->struktur]);
                }]);
            }]);
        }

        if (!empty($this->departemen)) {
            $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) {
                $query->joinWith(['struktur' => function (ActiveQuery $query) {
                    $query->joinWith(['departemen' => function (ActiveQuery $query) {
                        $query->andWhere(['like', 'departemen.nama', $this->departemen]);
                    }]);
                }]);
            }]);
        }

        if (!empty($this->area_kerja)) {
            $query->joinWith(['areaKerja' => function (ActiveQuery $query) {
                $query->joinWith(['refAreaKerja' => function (ActiveQuery $query) {
                    $query->andWhere(['like', 'ref_area_kerja.name', $this->area_kerja]);
                }]);
            }]);
        }

        if (!empty($this->updated_at)) {
            $tanggal = explode('s/d', $this->updated_at);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'pegawai.updated_at', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'pegawai.id', $this->id])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'pegawai.tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['pegawai.status_pernikahan' => $this->status_pernikahan])
            ->andFilterWhere(['pegawai.is_resign' => $this->is_resign]);

        $query->orderBy('pegawai.created_at DESC');

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchBca(array $params)
    {
        $query = Pegawai::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->sistem_kerja)) {
            $query->joinWith(['pegawaiSistemKerja' => function (ActiveQuery $x) {
                return $x->andWhere(['pegawai_sistem_kerja.sistem_kerja_id' => $this->sistem_kerja]);
            }]);
        }

        if (!empty($this->id_pegawai)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['LIKE', 'perjanjian_kerja.id_pegawai', $this->id_pegawai]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        $subQueryPerjanjianKerja = PerjanjianKerja::find()->select('pegawai_id')
            ->andWhere(['IN', 'kontrak', [PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA]])
            ->andWhere('IF(`kontrak` IN (' . PerjanjianKerja::KBU . ',' . PerjanjianKerja::ADA . '), `dasar_masa_kerja` = 0,`dasar_masa_kerja` = 1)')
            ->groupBy('pegawai_id')->having('COUNT(*) > 0');
        $query->andWhere(['IN', 'pegawai.id', $subQueryPerjanjianKerja]);

        if (!empty($this->jenis_kontrak)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.jenis_perjanjian' => $this->jenis_kontrak]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        if (!empty($this->struktur)) {
            $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) {
                $query->joinWith(['struktur' => function (ActiveQuery $query) {
                    $query->andWhere(['like', 'struktur.name', $this->struktur]);
                }]);
            }]);
        }

        if (!empty($this->area_kerja)) {
            $query->joinWith(['areaKerja' => function (ActiveQuery $query) {
                $query->joinWith(['refAreaKerja' => function (ActiveQuery $query) {
                    $query->andWhere(['like', 'ref_area_kerja.name', $this->area_kerja]);
                }]);
            }]);
        }

        if (!empty($this->updated_at)) {
            $tanggal = explode('s/d', $this->updated_at);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'pegawai.updated_at', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'pegawai.id', $this->id])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'pegawai.tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['pegawai.status_pernikahan' => $this->status_pernikahan])
            ->andFilterWhere(['pegawai.is_resign' => $this->is_resign]);

        $query->orderBy('pegawai.created_at DESC');

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchPegawaiAktifTanpaDasarMasaKerja(array $params = []): ActiveDataProvider
    {
        $queryPerjanjianKerjaAktif = PerjanjianKerja::find();
        $queryPerjanjianKerjaAktif->select('perjanjian_kerja.pegawai_id');
        $queryPerjanjianKerjaAktif->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarTanggal(date('Y-m-d')));
        $queryPerjanjianKerjaAktif->andWhere(['IN', 'kontrak', [PerjanjianKerja::KBU, PerjanjianKerja::ADA, PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA]]);

        $queryTanpaDasarMasaKerja = PerjanjianKerja::find()
            ->select('pegawai_id, SUM(dasar_masa_kerja) as jumlahDmk')->andWhere(['IN', 'pegawai_id', $queryPerjanjianKerjaAktif])->having(['=', 'jumlahDmk', 0])->groupBy('pegawai_id')->asArray()->all();

        $pegawaiIds = array_map(function ($x) {
            return $x['pegawai_id'];
        }, $queryTanpaDasarMasaKerja);

        $queryPegawai = Pegawai::find()->andWhere(['IN', 'id', $pegawaiIds])->andWhere(['is_resign' => Status::TIDAK]);


        $dataProvider = new ActiveDataProvider([
            'query' => $queryPegawai,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $queryPegawai->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->nama_lengkap]);

        $queryPegawai->orderBy('pegawai.nama_lengkap DESC');

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchPegawaiAktifTanpaMasterData(array $params = []): ActiveDataProvider
    {
        $queryPerjanjianKerjaAktif = PerjanjianKerja::find();
        $queryPerjanjianKerjaAktif->select('perjanjian_kerja.pegawai_id');
        $queryPerjanjianKerjaAktif->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarTanggal(date('Y-m-d')));
        $queryPerjanjianKerjaAktif->andWhere(['IN', 'kontrak', [PerjanjianKerja::KBU, PerjanjianKerja::ADA, PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA]]);

        $queryMasterData = MasterDataDefault::find()->select('pegawai_id');

        $queryPegawai = Pegawai::find()->andWhere(['IN', 'id', $queryPerjanjianKerjaAktif])
            ->andWhere(['NOT IN', 'pegawai.id', $queryMasterData])
            ->andWhere(['is_resign' => Status::TIDAK]);

        $dataProvider = new ActiveDataProvider([
            'query' => $queryPegawai,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $queryPegawai->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->nama_lengkap]);

        $queryPegawai->orderBy('pegawai.nama_lengkap DESC');

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchPegawaiAktifSistemKerja(array $params = []): ActiveDataProvider
    {
        $queryPerjanjianKerjaAktif = PerjanjianKerja::find();
        $queryPerjanjianKerjaAktif->select('perjanjian_kerja.pegawai_id');
        $queryPerjanjianKerjaAktif->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarTanggal(date('Y-m-d')));
        $queryPerjanjianKerjaAktif->andWhere(['IN', 'kontrak', [PerjanjianKerja::KBU, PerjanjianKerja::ADA, PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA]]);

        $queryPegawaiSistemKerja = PegawaiSistemKerja::find()->select('pegawai_id');

        $queryPegawai = Pegawai::find()->andWhere(['IN', 'id', $queryPerjanjianKerjaAktif])
            ->andWhere(['NOT IN', 'pegawai.id', $queryPegawaiSistemKerja])
            ->andWhere(['is_resign' => Status::TIDAK]);

        $dataProvider = new ActiveDataProvider([
            'query' => $queryPegawai,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $queryPegawai->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->nama_lengkap]);

        $queryPegawai->orderBy('pegawai.nama_lengkap DESC');

        return $dataProvider;
    }
}

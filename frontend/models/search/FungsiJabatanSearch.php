<?php

namespace frontend\models\search;

use frontend\modules\struktur\models\FungsiJabatan;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\models\search\FungsiJabatanSearch represents the model behind the search form about `frontend\modules\struktur\models\FungsiJabatan`.
 */
 class FungsiJabatanSearch extends FungsiJabatan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama', 'kode', 'keterangan', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_org_chart', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FungsiJabatan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}

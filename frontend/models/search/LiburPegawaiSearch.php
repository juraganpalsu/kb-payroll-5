<?php

namespace frontend\models\search;

use frontend\models\LiburPegawai;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\models\search\LiburPegawaiSearch represents the model behind the search form about `frontend\models\LiburPegawai`.
 * @property mixed $monthDropdown
 *
 * @property integer $unit
 * @property integer $divisi
 * @property string $idfp
 *
 */
class LiburPegawaiSearch extends LiburPegawai
{
    public $unit;
    public $divisi;
    public $idfp;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit', 'divisi'], 'integer'],
            [['tanggal', 'idfp', 'pegawai_id', 'unit', 'divisi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiburPegawai::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('pegawai');


        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }

        if (!empty($this->idfp)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                $query->joinWith(['perjanjianKerja' => function (ActiveQuery $query) {
                    $query->andWhere(['like', 'perjanjian_kerja.id_pegawai', $this->idfp]);
                }]);
            }]);
        }
        if (!empty($this->unit)) {
            $query->joinWith(['pegawai' =>
                function (ActiveQuery $x) {
                    $x->andWhere(['pegawai.unit' => $this->unit]);
                }
            ]);
        }

        if (!empty($this->divisi)) {
            $query->joinWith(['pegawai' =>
                function (ActiveQuery $x) {
                    $x->andWhere(['pegawai.divisi' => $this->divisi]);
                }
            ]);
        }

        $query->andFilterWhere(['like', 'pegawai_id', $this->pegawai_id]);
        $query->orderBy('tanggal DESC');

        return $dataProvider;
    }
}

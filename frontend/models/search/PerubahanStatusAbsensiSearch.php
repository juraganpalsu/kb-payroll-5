<?php

namespace frontend\models\search;

use common\models\PerubahanStatusAbsensi;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\models\search\PerubahanStatusAbsensiSearch represents the model behind the search form about `common\models\PerubahanStatusAbsensi`.
 *
 *
 * @property string $nama
 * @property string $idfp
 */
class PerubahanStatusAbsensiSearch extends PerubahanStatusAbsensi
{

    /**
     * @var string
     */
    public $nama;

    public $idfp;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'idfp', 'id', 'tanggal', 'tanggal_pulang', 'masuk', 'pulang', 'jumlah_jam_lembur', 'keterangan', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['idfp', 'urut', 'status_kehadiran', 'time_table_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PerubahanStatusAbsensi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'urut' => $this->urut,
            'masuk' => $this->masuk,
            'pulang' => $this->pulang,
            'status_kehadiran' => $this->status_kehadiran,
            'time_table_id' => $this->time_table_id
        ]);


        if (!empty($this->tanggal)) {
            $tanggal = explode('-', $this->tanggal);
            $query->andWhere(['BETWEEN', 'tanggal', $tanggal[0], $tanggal[1]]);
        }


        if (!empty($this->tanggal_pulang)) {
            $tanggal = explode('-', $this->tanggal_pulang);
            $query->andWhere(['BETWEEN', 'tanggal_pulang', $tanggal[0], $tanggal[1]]);
        }


        if (!empty($this->nama)) {
            $query->joinWith(['perubahanStatusAbsensiDetails' =>
            /**
             * @param ActiveQuery $q
             */
                function (ActiveQuery $q) {
                    $q->joinWith(['pegawai' =>

                    /**
                     * @param ActiveQuery $x
                     */
                        function (ActiveQuery $x) {
                            $x->andWhere(['like', 'pegawai.nama_lengkap', $this->nama]);
                        }
                    ]);
                }
            ]);
        }
        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->idfp)) {
            $query->joinWith(['perubahanStatusAbsensiDetails' => function (ActiveQuery $q) {
                $q->joinWith(['pegawai' => function (ActiveQuery $q) {
                    $q->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                        $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->idfp]);
                    }]);
                }
                ]);
            }
            ]);
        }


        $query->andFilterWhere(['like', 'jumlah_jam_lembur', $this->jumlah_jam_lembur])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        $query->orderBy('perubahan_status_absensi.urut DESC');

        return $dataProvider;
    }
}

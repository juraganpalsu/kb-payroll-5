<?php

namespace frontend\models\search;

use common\models\Divisi;
use common\models\GrupKerja;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\models\search\GrupKerjaSearch represents the model behind the search form about `common\models\GrupKerja`.
 *
 * @property integer $unit_id
 */
class GrupKerjaSearch extends GrupKerja
{
    public $unit_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'divisi_id', 'unit_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['nama', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GrupKerja::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->unit_id)) {
            $query->joinWith(['divisi' => function ($divisi) {
                /** @var ActiveQuery $divisi */
                $divisi->andWhere(['unit_id' => $this->unit_id]);
            }]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'divisi_id' => $this->divisi_id,
            'unit_id' => $this->unit_id,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama]);

        return $dataProvider;
    }
}

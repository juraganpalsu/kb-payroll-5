<?php

namespace frontend\models\search;

use frontend\models\Absensi;
use frontend\models\TimeTable;
use frontend\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\models\search\AbsensiSearch represents the model behind the search form about `frontend\models\Absensi`.
 * @property int $shift_search
 * @property int $id_pegawai
 *
 *
 * @property integer $unit
 * @property integer $divisi
 * @property integer $sistem_kerja
 * @property integer $golongan_id
 * @property integer $busines_unit
 *
 */
class AbsensiSearch extends Absensi
{

    /**
     * @var int
     */
    public $id_pegawai;

    /**
     * @var int untuk drop down shift
     */
    public $shift_search;


    public $unit;
    public $divisi;
    public $sistem_kerja;
    public $golongan_id;
    public $nama_lengkap;
    public $busines_unit;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pegawai', 'status_kehadiran', 'unit', 'sistem_kerja', 'busines_unit', 'golongan_id', 'divisi', 'created_by', 'updated_by', 'deleted_by', 'lock', 'shift_search'], 'integer'],
            [['tanggal', 'nama_lengkap', 'pegawai_id', 'id_pegawai', 'time_table_id', 'created_at', 'updated_at', 'deleted_at', 'jumlah_jam_lembur'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getShift_()
    {
        $modelTimeTable = new TimeTable();
        return $modelTimeTable->_shift;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, bool $filterAbsensi = true)
    {
        $query = Absensi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('pegawai');

        $query->andFilterWhere([
            'absensi.masuk' => $this->masuk,
            'absensi.pulang' => $this->pulang,
            'absensi.status_kehadiran' => $this->status_kehadiran,
            'absensi.jumlah_jam_lembur' => $this->jumlah_jam_lembur,
        ]);

        if ($filterAbsensi) {
            $awal = date('Y-m-t', strtotime('- 2 months'));
            $akhir = date('Y-m-d');
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }


        if (!empty($this->sistem_kerja)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                $q->joinWith(['pegawaiSistemKerja' => function (ActiveQuery $x) {
                    return $x->andWhere(['pegawai_sistem_kerja.sistem_kerja_id' => $this->sistem_kerja]);
                }]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if (!empty($this->golongan_id)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);

        }


        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }


        if (!empty($this->shift_search)) {
            $query->joinWith(['timeTable' => function (ActiveQuery $q) {
                $q->andWhere(['time_table.shift' => $this->shift_search]);
            }]);
        }

        $query->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['absensi.time_table_id' => $this->time_table_id]);


        $query->orderBy('absensi.tanggal ASC');

        return $dataProvider;
    }

    /**
     * @param array $params
     * @param bool $absenTeam
     * @return ActiveDataProvider
     */
    public function searchEss(array $params, bool $absenTeam = false)
    {
        $query = Absensi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if ($absenTeam) {
            $query->joinWith('pegawai');
            $query->joinWith('pegawaiStruktur');
        }
        if (!$absenTeam) {
            $query->joinWith('pegawai');
            $query->andWhere(['pegawai.id' => $user->pegawai->id]);
        }

        $query->andFilterWhere([
            'absensi.masuk' => $this->masuk,
            'absensi.pulang' => $this->pulang,
            'absensi.status_kehadiran' => $this->status_kehadiran,
            'absensi.jumlah_jam_lembur' => $this->jumlah_jam_lembur,
        ]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }


        if (!empty($this->sistem_kerja)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                $q->joinWith(['pegawaiSistemKerja' => function (ActiveQuery $x) {
                    return $x->andWhere(['pegawai_sistem_kerja.sistem_kerja_id' => $this->sistem_kerja]);
                }]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if (!empty($this->golongan_id)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);

        }


        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }


        if (!empty($this->shift_search)) {
            $query->joinWith(['timeTable' => function (ActiveQuery $q) {
                $q->andWhere(['time_table.shift' => $this->shift_search]);
            }]);
        }

        $query->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['absensi.time_table_id' => $this->time_table_id]);
        if ($absenTeam) {
        $query->andWhere(['IN', 'pegawai_struktur.struktur_id', $user->getStrukturBawahan()]);
        }

        $awal = date('Y-m-t', strtotime('- 2 months'));
        if ($absenTeam) {
            $awal = date('Y-m-t', strtotime('- 1 months'));
        }
        $akhir = date('Y-m-d');
        $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);

        $query->orderBy('absensi.tanggal DESC');

        return $dataProvider;
    }
}

<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Libur;

/**
 * frontend\models\search\LiburSearch represents the model behind the search form about `frontend\models\Libur`.
 */
class LiburSearch extends Libur
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jenis', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal', 'keterangan', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Libur::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jenis' => $this->jenis,
        ]);


        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }


        $query->andFilterWhere(['like', 'keterangan', $this->keterangan]);
        $query->orderBy('tanggal DESC');

        return $dataProvider;
    }
}

<?php

namespace frontend\models\search;

use common\models\AttendanceBeatroute;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\models\search\AttendanceBeatrouteSearch represents the model behind the search form about `common\models\AttendanceBeatroute`.
 */
class AttendanceBeatrouteSearch extends AttendanceBeatroute
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'emp_role_id', 'user_br_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['date', 'duration', 'status', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AttendanceBeatroute::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->date)) {
            $tanggal = explode('s/d', $this->date);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'date', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'emp_role_id', $this->emp_role_id]);
        $query->andFilterWhere(['like', 'user_br_id', $this->user_br_id]);
        $query->andFilterWhere(['like', 'status', $this->status]);
        $query->andFilterWhere(['like', 'duration', $this->duration]);

        return $dataProvider;
    }
}

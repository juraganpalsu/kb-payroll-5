<?php

namespace frontend\models\search;

use frontend\models\Ijin;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\models\search\IjinSearch represents the model behind the search form about `frontend\models\Ijin`.
 */
class IjinSearch extends Ijin
{

    public $tipe_;

    public $unit;
    public $divisi;
    public $idfp;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipe', 'tipe_', 'unit', 'divisi', 'shift'], 'integer'],
            [['tanggal', 'pegawai_id', 'keterangan', 'idfp', 'unit', 'divisi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ijin::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tipe' => $this->tipe_,
            'shift' => $this->shift
        ]);


        $query->joinWith('pegawai');

        if (!empty($this->idfp)) {
            $query->andWhere(['like', 'pegawai.idfp', $this->idfp]);

        }
        if (!empty($this->unit)) {
            $query->andWhere(['pegawai.unit' => $this->unit]);
        }

        if (!empty($this->divisi)) {
            $query->andWhere(['pegawai.divisi' => $this->divisi]);
        }

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }


        $query->andFilterWhere(['like', 'pegawai_id', $this->pegawai_id])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);


        return $dataProvider;
    }
}

<?php

namespace frontend\models\search;
use frontend\models\User;
use frontend\models\Attendance;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use Yii;


/**
 * frontend\models\search\AttendanceSearch represents the model behind the search form about `frontend\models\Attendance`.
 *
 * @property integer $unit
 * @property integer $divisi
 * @property integer $sistem_kerja
 * @property integer $golongan_id
 * @property string $tanggal
 * @property string $jam
 */
class AttendanceSearch extends Attendance
{

    public $unit;
    public $divisi;
    public $sistem_kerja;
    public $golongan_id;
    public $busines_unit;

    public $tanggal;
    public $jam;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idfp', 'unit', 'sistem_kerja', 'golongan_id', 'divisi', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['pegawai_id','busines_unit', 'device_id', 'check_time', 'tanggal', 'jam', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attendance::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        $query->andFilterWhere([
            'attendance.check_time' => $this->check_time,
            'attendance.idfp' => $this->idfp,
        ]);

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'date(check_time)', $awal, $akhir]);
        }

        if (!empty($this->jam)) {
            $query->andWhere('time(check_time) =:jam', [':jam' => date('H:i:s', strtotime($this->jam))]);
        }

        if (!empty($this->unit)) {
            $query->joinWith(['pegawai' =>
                function (ActiveQuery $x) {
                    $x->andWhere(['pegawai.unit' => $this->unit]);
                }
            ]);
        }

        if (!empty($this->divisi)) {
            $query->joinWith(['pegawai' =>
                function (ActiveQuery $x) {
                    $x->andWhere(['pegawai.divisi' => $this->divisi]);
                }
            ]);
        }

        if (!empty($this->sistem_kerja)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                $q->joinWith(['pegawaiSistemKerja' => function (ActiveQuery $x) {
                    return $x->andWhere(['pegawai_sistem_kerja.sistem_kerja_id' => $this->sistem_kerja]);
                }]);
            }]);
        }

        if (!empty($this->golongan_id)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                $q->andWhere(['pegawai.golongan_id' => $this->golongan_id]);
            }]);
        }

        $query->andFilterWhere(['device_id' => $this->device_id])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);

        $query->orderBy('check_time DESC');

        return $dataProvider;
    }

    /**
     * @param array $params
     * @param bool $absenTeam
     * @return ActiveDataProvider|\yii\web\Response
     */
    public function searchEss(array $params, bool $absenTeam = false)
    {
        $query = Attendance::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user) {
            return Yii::$app->controller->redirect(['/']);
        }

        if ($absenTeam) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) use ($user) {
                $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) use ($user) {
                    $query->andWhere(['IN', 'struktur_id', $user->getStrukturBawahan()]);
                }]);
            }]);
        }
        if (!$absenTeam) {
            $query->joinWith('pegawai');
            $query->andWhere(['pegawai.id' => $user->pegawai->id]);
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $query) use ($user) {
            $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) use ($user) {
                $query->andWhere(['IN', 'struktur_id', $user->getStrukturBawahan()]);
            }]);
        }]);

        $query->andFilterWhere([
            'attendance.check_time' => $this->check_time,
            'attendance.idfp' => $this->idfp,
        ]);


        if (!empty($this->pegawai_id)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                $q->andWhere(['pegawai.id' => $this->pegawai_id]);
            }]);
        }

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'date(check_time)', $awal, $akhir]);
        }

        if (!empty($this->jam)) {
            $query->andWhere('time(check_time) =:jam', [':jam' => date('H:i:s', strtotime($this->jam))]);
        }

        if (!empty($this->unit)) {
            $query->joinWith(['pegawai' =>
                function (ActiveQuery $x) {
                    $x->andWhere(['pegawai.unit' => $this->unit]);
                }
            ]);
        }

        if (!empty($this->divisi)) {
            $query->joinWith(['pegawai' =>
                function (ActiveQuery $x) {
                    $x->andWhere(['pegawai.divisi' => $this->divisi]);
                }
            ]);
        }

        if (!empty($this->sistem_kerja)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                $q->joinWith(['pegawaiSistemKerja' => function (ActiveQuery $x) {
                    return $x->andWhere(['pegawai_sistem_kerja.sistem_kerja_id' => $this->sistem_kerja]);
                }]);
            }]);
        }

        if (!empty($this->golongan_id)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                $q->andWhere(['pegawai.golongan_id' => $this->golongan_id]);
            }]);
        }

        $query->andFilterWhere(['device_id' => $this->device_id]);

        $query->orderBy('check_time DESC');

        return $dataProvider;
    }
}

<?php

namespace frontend\models\search;

use frontend\models\TimeTable;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @property int $sistemKerja_
 * @property int $shift_search
 *
 * frontend\models\search\TimeTableSearch represents the model behind the search form about `frontend\models\TimeTable`.
 */
class TimeTableSearch extends TimeTable
{

    public $sistemKerja_;
    public $shift_search;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'shift', 'sistem_kerja', 'created_by', 'updated_by', 'deleted_by', 'lock', 'sistemKerja_', 'shift_search'], 'integer'],
            [['nama', 'untuk_hari', 'masuk_minimum', 'masuk', 'masuk_maksimum', 'pulang_minimum', 'pulang', 'pulang_maksimum', 'created_at', 'updated_at', 'deleted_at'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeTable::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            var_dump($this->errors);
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'shift' => $this->shift_search,
            'masuk_minimum' => $this->masuk_minimum,
            'masuk' => $this->masuk,
            'masuk_maksimum' => $this->masuk_maksimum,
            'pulang_minimum' => $this->pulang_minimum,
            'pulang' => $this->pulang,
            'pulang_maksimum' => $this->pulang_maksimum,
            'sistem_kerja' => $this->sistemKerja_,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama]);

        return $dataProvider;
    }
}

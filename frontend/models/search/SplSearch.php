<?php

namespace frontend\models\search;

use common\components\Status;
use frontend\models\Spl;
use frontend\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\models\search\SplSearch represents the model behind the search form about `frontend\models\Spl`.
 *
 */

/**
 * @property string $_nama_detail
 * @property string $nama
 * @property string $id_pegawai
 * @property integer $sistem_kerja
 * @property integer $golongan_id
 * @property integer $busines_unit
 */
class SplSearch extends Spl
{

    public $nama;

    public $id_pegawai;

    public $sistem_kerja;

    public $golongan_id;

    public $busines_unit;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'seq_code', 'busines_unit', 'spl_template_id', 'is_planing', 'klaim_quaker', 'sistem_kerja', 'golongan_id', 'updated_by', 'deleted_by', 'lock', 'unit', 'divisi', 'last_status', 'kategori'], 'integer'],
            [['nama', 'id_pegawai', 'cost_center_id', 'spl_alasan_id', 'seq_code', 'tanggal', 'keterangan', 'created_at', 'updated_at', 'deleted_at', 'created_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param bool $filterStruktur
     * @param bool $filterLastStatus
     * @param array $status
     * @return ActiveDataProvider
     */
    public function search(array $params, $kategori = [], bool $filterStruktur = true, bool $filterLastStatus = false, array $status = [])
    {
        $query = Spl::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($filterStruktur) {
            /** @var User $modelUser */
            $modelUser = Yii::$app->user->identity;

            $strukturs = array_merge($modelUser->getStrukturBawahan(), $modelUser->getStrukturAtasan());

            $query->andWhere(['IN', 'spl.created_by_struktur', $strukturs]);
        }
        $query->joinWith(['splTemplate' => function (ActiveQuery $q) {
        }]);

        $query->andFilterWhere([
            'spl.spl_template_id' => $this->spl_template_id,
            'spl.cost_center_id' => $this->cost_center_id,
            'spl.spl_alasan_id' => $this->spl_alasan_id,
            'spl.kategori' => $this->kategori,
        ]);

        if (!empty($this->nama)) {
            $query->joinWith(['splDetails' =>
                function (ActiveQuery $q) {
                    $q->joinWith(['pegawai' =>
                        function (ActiveQuery $x) {
                            $x->andWhere(['like', 'pegawai.nama_lengkap', $this->nama]);
                        }
                    ]);
                }
            ]);
        }

        if (!empty($kategori)) {
            $query->andWhere(['IN', 'spl.kategori', $kategori]);
        }

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['splDetails' =>
                function (ActiveQuery $q) {
                    $q->joinWith(['perjanjianKerja' =>
                        function (ActiveQuery $x) {
                            $x->andWhere(['like', 'perjanjian_kerja.id_pegawai', $this->id_pegawai]);
                        }
                    ]);
                }
            ]);
        }

        if (!empty($this->created_by)) {
            $query->joinWith(['createdBy' => function (ActiveQuery $query) {
                $query->joinWith(['userPegawai' => function (ActiveQuery $query) {
                    $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                        $query->andWhere(['like', 'pegawai.nama_lengkap', $this->created_by]);
                    }]);
                }]);
            }]);
        }

        //

        if (!empty($this->golongan_id)) {
            $query->joinWith(['splDetails' => function (ActiveQuery $y) {
                $y->joinWith(['pegawai' => function (ActiveQuery $q) {
                    $q->joinWith(['pegawaiGolongan' => function (ActiveQuery $x) {
                        return $x->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);
                    }]);
                }]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith(['splDetails' => function (ActiveQuery $q) {
                $q->joinWith(['perjanjianKerja' => function (ActiveQuery $x) {
                    return $x->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
                }]);
            }]);
        }

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'spl.tanggal', $awal, $akhir]);
        }


        $query->andFilterWhere([
            'spl.unit' => $this->unit,
            'spl.divisi' => $this->divisi,
            'spl.is_planing' => $this->is_planing,
            'spl.klaim_quaker' => $this->klaim_quaker
        ]);

        if ($filterLastStatus && !empty($status)) {
            $query->andWhere(['IN', 'spl.last_status', $status]);
        }

        $query->andFilterWhere(['spl.last_status' => $this->last_status]);
        $query->andFilterWhere(['like', 'spl.keterangan', $this->keterangan]);
        $query->andFilterWhere(['like', 'spl.seq_code', $this->seq_code]);
        $query->orderBy('spl.seq_code DESC');

        return $dataProvider;
    }

    /**
     * @param array $params
     * @param bool $filterStruktur
     * @param bool $filterLastStatus
     * @param array $status
     * @return ActiveDataProvider
     */
    public function searchEss(array $params, bool $filterStruktur = true, bool $filterLastStatus = false, array $status = [Status::FINISHED, Status::APPROVED])
    {
        $query = Spl::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($filterStruktur) {
            /** @var User $modelUser */
            $modelUser = Yii::$app->user->identity;

            $strukturs = array_merge($modelUser->getStrukturBawahan(), $modelUser->getStrukturAtasan());

            $query->andWhere(['IN', 'spl.created_by_struktur', $strukturs]);
        }

        $query->joinWith('splTemplate');
        $query->joinWith(['splDetails' =>
            function (ActiveQuery $q) {
                $q->joinWith(['pegawai' =>
                    function (ActiveQuery $x) {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        $x->andWhere(['pegawai.id' => $user->pegawai->id]);
                    }
                ]);
            }
        ]);


        $query->andFilterWhere([
            'spl.spl_template_id' => $this->spl_template_id,
            'spl.cost_center_id' => $this->cost_center_id,
            'spl.spl_alasan_id' => $this->spl_alasan_id,
            'spl.kategori' => $this->kategori,
        ]);

        if (!empty($this->nama)) {
            $query->joinWith(['splDetails' =>
                function (ActiveQuery $q) {
                    $q->joinWith(['pegawai' =>
                        function (ActiveQuery $x) {
                            $x->andWhere(['like', 'pegawai.nama_lengkap', $this->nama]);
                        }
                    ]);
                }
            ]);
        }

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['splDetails' =>
                function (ActiveQuery $q) {
                    $q->joinWith(['perjanjianKerja' =>
                        function (ActiveQuery $x) {
                            $x->andWhere(['like', 'perjanjian_kerja.id_pegawai', $this->id_pegawai]);
                        }
                    ]);
                }
            ]);
        }

        if (!empty($this->created_by)) {
            $query->joinWith(['createdBy' => function (ActiveQuery $query) {
                $query->joinWith(['userPegawai' => function (ActiveQuery $query) {
                    $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                        $query->andWhere(['like', 'pegawai.nama_lengkap', $this->created_by]);
                    }]);
                }]);
            }]);
        }

        if (!empty($this->golongan_id)) {
            $query->joinWith(['splDetails' =>
                function (ActiveQuery $q) {
                    $q->joinWith(['pegawai' => function (ActiveQuery $q) {
                        $q->andWhere(['pegawai.golongan_id' => $this->golongan_id]);
                    }]);
                }
            ]);
        }

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'spl.tanggal', $awal, $akhir]);
        }


        $query->andFilterWhere([
            'spl.unit' => $this->unit,
            'spl.divisi' => $this->divisi,
            'spl.is_planing' => $this->is_planing,
            'spl.klaim_quaker' => $this->klaim_quaker
        ]);

        if ($filterLastStatus && !empty($status)) {
            $query->andWhere(['IN', 'spl.last_status', $status]);
        }

        $query->andFilterWhere(['spl.last_status' => $this->last_status]);
        $query->andFilterWhere(['like', 'spl.keterangan', $this->keterangan]);
        $query->andFilterWhere(['like', 'spl.seq_code', $this->seq_code]);
        $query->orderBy('spl.seq_code DESC');

        return $dataProvider;
    }

    /**
     * Mencari data yg belum dilakukan approval bedasarkan user login
     *
     * @param bool $asActiveDataProvider
     * @return ActiveDataProvider|ActiveQuery
     */
    public function searchApproval(bool $asActiveDataProvider = true)
    {
        $query = Spl::find();

        $query->alias('spl');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andWhere(['spl.last_status' => Status::SUBMITED]);

        $query->joinWith(['splApproval' => function (ActiveQuery $q) {
            $modelPegawai = User::pegawai();
            $q->alias('spl_approval')->andWhere(['spl_approval.tipe' => Status::DEFLT, 'pegawai_id' => $modelPegawai->id]);
        }]);

        $query->orderBy('spl.tanggal DESC');

        if ($asActiveDataProvider) {
            return $dataProvider;
        }
        return $query;
    }
}

<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\SplTemplate;

/**
 * frontend\models\search\SplTemplateSearch represents the model behind the search form about `frontend\models\SplTemplate`.
 */
class SplTemplateSearch extends SplTemplate
{
    public $sistemKerja_;
    public $kategori_;
    public $shift_;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'toleransi_masuk', 'toleransi_pulang', 'sistem_kerja', 'sebagai_header', 'kategori', 'created_by', 'updated_by', 'deleted_by', 'lock', 'sistemKerja_', 'kategori_', 'shift_', 'istirahat'], 'integer'],
            [['nama', 'masuk_minimum', 'masuk', 'masuk_maksimum', 'pulang_minimum', 'pulang', 'pulang_maksimum', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['jumlah_jam_lembur'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SplTemplate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sistem_kerja' => $this->sistemKerja_,
            'sebagai_header' => $this->sebagai_header,
            'kategori' => $this->kategori_,
            'tunjangan_shift' => $this->shift_,
            'istirahat' => $this->istirahat,
            'jumlah_jam_lembur' => $this->jumlah_jam_lembur,
            'masuk_minimum' => $this->masuk_minimum,
            'masuk' => $this->masuk,
            'toleransi_masuk' => $this->toleransi_masuk,
            'toleransi_pulang' => $this->toleransi_pulang,
            'masuk_maksimum' => $this->masuk_maksimum,
            'pulang_minimum' => $this->pulang_minimum,
            'pulang' => $this->pulang,
            'pulang_maksimum' => $this->pulang_maksimum,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama]);

        return $dataProvider;
    }
}

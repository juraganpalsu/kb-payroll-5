<?php

namespace frontend\models\search;

use common\models\SistemKerja;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\models\search\SistemKerjaSearch represents the model behind the search form about `common\models\SistemKerja`.
 */
 class SistemKerjaSearch extends SistemKerja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['nama', 'hari_keenam_setengah', 'jenis_libur', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SistemKerja::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'hari_keenam_setengah' => $this->hari_keenam_setengah,
            'jenis_libur' => $this->jenis_libur,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama]);

        $query->orderBy('id ASC');

        return $dataProvider;
    }
}

<?php

namespace frontend\models;

use common\models\Absensi as BaseAbsensi;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "absensi".
 * @property mixed ijin
 * @property mixed $statusKehadiran_
 */
class Absensi extends BaseAbsensi
{

    /*
     * Mengambil data attendance dari kemarin sampai besok, untuk mempermudah pengecekan
     *
     * @param bool $asDataProvider
     * @return array|ActiveDataProvider|\yii\db\ActiveRecord[]
     */
    public function dataProviderAttendance(bool $asDataProvider = true)
    {
        $perjanjianKerja = $this->perjanjianKerja;
        if (!$perjanjianKerja) {
            return [];
        }
        $query = Attendance::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $query->where([
            'idfp' => $this->perjanjianKerja->id_pegawai,
        ]);

        $kemarin = date('Y-m-d', strtotime($this->tanggal . '-1 day'));
        $besok = date('Y-m-d', strtotime($this->tanggal . '+1 day'));

        $query->andWhere(['BETWEEN', 'date(check_time)', $kemarin, $besok]);

        $query->orderBy('check_time ASC');

        if ($asDataProvider) {
            return $dataProvider;
        }
        return $query->all();
    }

    /**
     * Menghitung telah masuk dalam hitungan menit
     *
     * @return float|string
     */
    public function hitungTelatMasuk()
    {
        $telat = '';
        if ($this->masuk) {
            $jamAbsenMasuk = strtotime($this->masuk);
            $toleransi = strtotime($this->tanggal . ' ' . $this->timeTable->masuk . ' +' . $this->timeTable->toleransi_masuk . ' minute');
            if ($jamAbsenMasuk > $toleransi) {
                $jamMasuk = strtotime($this->tanggal . ' ' . $this->timeTable->masuk);
                $telat = round(($jamAbsenMasuk - $jamMasuk) / 60);
            }
        }
        return $telat;
    }

    /**
     * Menghitung pulang cepat dalam hitungan menit
     *
     * @return float|string
     */
    public function hitungPulangCepat()
    {
        $cepat = '';
        if ($this->pulang) {
            $tanggal = $this->tanggal;
            if (strtotime($tanggal) < strtotime(date('Y-m-d', strtotime($this->pulang)))) {
                $tanggal = date('Y-m-d', strtotime($this->tanggal . ' +1 day'));
            }
            $jamAbsenPulang = strtotime($this->pulang);
            $toleransi = strtotime($tanggal . ' ' . $this->timeTable->pulang . ' -' . $this->timeTable->toleransi_pulang . ' minute');
            if ($jamAbsenPulang < $toleransi) {
                $jamPulang = strtotime($tanggal . ' ' . $this->timeTable->pulang);
                $cepat = round(($jamPulang - $jamAbsenPulang) / 60);
            }
        }
        return $cepat;
    }

}

<?php

namespace frontend\controllers;

use common\models\Golongan;
use DateTime;
use frontend\models\Absensi;
use frontend\models\search\PegawaiSearch;
use frontend\modules\dashboard\models\DsAbsensi;
use frontend\modules\dashboard\models\DsAkhirKontrak;
use frontend\modules\dashboard\models\DsAkhirKontrakDetail;
use frontend\modules\dashboard\models\DsTurnOver;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\PerjanjianKerjaJenis;
use frontend\modules\personalia\models\SuratPeringatan;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $modelPegawai = new Pegawai();
        return Yii::$app->user->isGuest ? $this->redirect(['/user/login']) : $this->render('index', ['modelPegawai' => $modelPegawai]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        return Yii::$app->user->isGuest ? $this->redirect(['/user/login']) : $this->render('index');
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string
     */
    public function actionDashboardHr()
    {
        $tglObj = new DateTime();

        $pegawaiPerempuan = Pegawai::find()->joinWith('perjanjianKerja')->andWhere(['jenis_kelamin' => Pegawai::PEREMPUAN])->count();
        $pegawaiLakiLaki = Pegawai::find()->joinWith('perjanjianKerja')->andWhere(['jenis_kelamin' => Pegawai::LAKI_LAKI])->count();
        $jenisKelamin = [Pegawai::PEREMPUAN => $pegawaiPerempuan, Pegawai::LAKI_LAKI => $pegawaiLakiLaki];

        $golongans = Golongan::find()->all();
        $pegawaiPerGolongan = [];
        /** @var Golongan $golongan */
        foreach ($golongans as $golongan) {
            $jumlah = Pegawai::find()->joinWith('pegawaiGolongan')->andWhere(['golongan_id' => $golongan->id])->count();
            if ($jumlah > 0) {
                $pegawaiPerGolongan[] = ['name' => $golongan->nama, 'y' => (int)$jumlah];
            }
        }
        $queryPerjanjianKerja = PerjanjianKerjaJenis::find()->all();
        $pegawaiPerBusinesUnit = [];
        /** @var PerjanjianKerjaJenis $jenisPerjanjianKerja */
        foreach ($queryPerjanjianKerja as $jenisPerjanjianKerja) {
            $jumlahPerBusinesUnit = Pegawai::find()->joinWith('perjanjianKerja')->andWhere(['jenis_perjanjian' => $jenisPerjanjianKerja->id])->count();
            if ($jumlahPerBusinesUnit > 0) {
                $pegawaiPerBusinesUnit[] = ['name' => $jenisPerjanjianKerja->nama, 'y' => (int)$jumlahPerBusinesUnit];
            }
        }

        $pegawaiPerJenisKontrak = [];
        $modelPerjanjianKerja = new PerjanjianKerja();
        foreach ($modelPerjanjianKerja->_kontrak as $key => $kontrak) {
            $jumlahPerJenisKontrak = Pegawai::find()->joinWith('perjanjianKerja')->andWhere(['kontrak' => $key])->count();
            if ($jumlahPerJenisKontrak > 0) {
                $pegawaiPerJenisKontrak[] = ['name' => $kontrak, 'y' => (int)$jumlahPerJenisKontrak];
            }
        }
        $akhirKontraks = [];
        $bulan = date('m', strtotime('+ 2 months'));
        $tahun = date('Y', strtotime('+ 2 months'));
        $queryDsAkhirKontrak = DsAkhirKontrak::find()->where(['month(tanggal)' => $bulan, 'year(tanggal)' => $tahun])->orderBy('tanggal ASC')->all();
        if ($queryDsAkhirKontrak) {
            $akhirKontrak = [];
            foreach ($queryDsAkhirKontrak as $dsAkhirKontrak) {
                /** @var DsAkhirKontrakDetail $dsAkhirKontrakDetail */
                foreach ($dsAkhirKontrak->dsAkhirKontrakDetails as $dsAkhirKontrakDetail) {
                    $akhirKontrak[$dsAkhirKontrakDetail->kontrak][] = $dsAkhirKontrakDetail->jumlah;
                }
            }
            if ($akhirKontrak) {
                foreach ($akhirKontrak as $jenisKontrak => $dataAkhirKontrak) {
                    $jenisKontrak = $modelPerjanjianKerja->_kontrak[$jenisKontrak];
                    $akhirKontraks[] = ['name' => $jenisKontrak, 'y' => array_sum($dataAkhirKontrak)];
                }
            }
        }

        $dsAbsensi = new DsAbsensi();
        $dataForDashboardAbsensi = $dsAbsensi->getDataForDashboard();
        $dashboardAbsensi = ['departemen' => [], 'status_kehadiran' => []];
        if (!empty($dataForDashboardAbsensi)) {
            $kemarin = $tglObj->modify('-1 day');
            if (isset($dataForDashboardAbsensi[$kemarin->format('Y-m-d')])) {
                $dashboardAbsensi = $dataForDashboardAbsensi[$kemarin->format('Y-m-d')];
            }
        }

        $modelSuratPeringatan = new SuratPeringatan();
        $dsSuratPeringatan = $modelSuratPeringatan->formatingDataDashboard();

        $dataJumlahPegawaiPerdepartemens = Pegawai::getDataForDashboardPerDepartement();
        $dsJumlahPegawaiPerdepartemen = [];
        foreach ($dataJumlahPegawaiPerdepartemens as $dataJumlahPegawaiPerdepartemen) {
            $dsJumlahPegawaiPerdepartemen[] = ['name' => $dataJumlahPegawaiPerdepartemen['name'], 'y' => count($dataJumlahPegawaiPerdepartemen['y'])];
        }

        $dsTurnOver = DsTurnOver::getDashboardData($tglObj->format('Y'));
        $dsInOut = DsTurnOver::getDashboardInOut($tglObj->format('Y'));

        $modelPegawai = new Pegawai();
        $dsRangeAge = $modelPegawai->formatingDashboardRangeAge();

        return $this->render('dashboard-hr', [
            'jenisKelamin' => $jenisKelamin, 'pegawaiPerGolongan' => $pegawaiPerGolongan,
            'pegawaiPerBusinesUnit' => $pegawaiPerBusinesUnit, 'pegawaiPerJenisKontrak' => $pegawaiPerJenisKontrak,
            'dsAkhirKontrak' => $akhirKontraks, 'dashboardAbsensi' => $dashboardAbsensi, 'dsSuratPeringatan' => $dsSuratPeringatan, 'dsJumlahPegawaiPerdepartemen' => $dsJumlahPegawaiPerdepartemen,
            'dsTurnOver' => $dsTurnOver, 'dsInOut' => $dsInOut, 'dsRangeAge' => $dsRangeAge, 'modelPegawai' => $modelPegawai
        ]);
    }

    /**
     * Lists all IzinTanpaHadir models.
     * @return mixed
     */
    public function actionDashboardControlData()
    {
        $searchModel = new PegawaiSearch();
        $pegawaiTanpaMasaKerja = $searchModel->searchPegawaiAktifTanpaDasarMasaKerja(Yii::$app->request->queryParams);
        $pegawaiTanpaMasterData = $searchModel->searchPegawaiAktifTanpaMasterData(Yii::$app->request->queryParams);
        $pegawaiTanpaSistemKerja = $searchModel->searchPegawaiAktifSistemKerja(Yii::$app->request->queryParams);
        $model = new \frontend\modules\pegawai\models\search\PegawaiSearch();
        $pegawaiHabisKontrak = $model->searchHabisKontrak();

        return $this->render('dashboard-control-data', [
            'pegawaiTanpaMasaKerja' => $pegawaiTanpaMasaKerja,
            'pegawaiTanpaMasterData' => $pegawaiTanpaMasterData,
            'pegawaiHabisKontrak' => $pegawaiHabisKontrak,
            'pegawaiTanpaSistemKerja' => $pegawaiTanpaSistemKerja,
        ]);
    }

    /**
     * @param string $param
     * @return string|Response
     */
    public function actionTampilkanNama(string $param)
    {
        $params = explode('*', $param);

        $tglObj = new DateTime();
        $modify = $tglObj->modify('-1 day');
        $kemarin = $modify->format('Y-m-d');

        $modelAbsensi = new Absensi();
        $statusKehadiranFlip = array_flip($modelAbsensi->_status_kehadiran);
        $statusKehadiran = $statusKehadiranFlip[$params[2]];

        $query = DsAbsensi::find()->andWhere(['departemen_string' => $params[0], 'jumlah' => $params['1'], 'status_kehadiran' => $statusKehadiran])
            ->joinWith(['dsPerTanggal' => function (ActiveQuery $query) use ($kemarin) {
                $query->andWhere(['tanggal' => $kemarin]);
            }])->one();

        $id = 0;
        if ($query) {
            $id = $query['id'];
        }

        $modelDsAbsensi = DsAbsensi::findOne($id);
        if ($modelDsAbsensi) {
            $pegawaiIds = explode('*', $modelDsAbsensi->pegawai_ids);
            if (!empty($pegawaiIds)) {
                $queryPegawai = Pegawai::find()->andWhere(['IN', 'id', $pegawaiIds])->orderBy('nama_lengkap ASC');
                $dataProvider = new ActiveDataProvider([
                    'query' => $queryPegawai,
                ]);
                return $this->renderAjax('tampilkan-nama-ds-absensi', ['modelDsAbsensi' => $modelDsAbsensi, 'dataProvider' => $dataProvider]);
            }
        }
        return $this->redirect(Url::to(['/site/dashboard-hr']));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 8/26/17
 * Time: 3:03 PM
 */

namespace frontend\controllers;


use frontend\models\Pegawai;
use frontend\models\TimeTable;
use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;

class HelperController extends Controller
{

    /**
     *
     */
    public function actionGetTimeTableDepDrop()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     *
     */
    public function actionGetPegawaiDepDrop()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     * @param null $q
     * @param int $sistemKerjaId
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function actionGetTimeTableSelect2($q = null, $sistemKerjaId = 0, $id = 0)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, nama AS text')
                ->from('time_table')
                ->where(['like', 'nama', $q])
                ->andWhere('sistem_kerja =:skid', [':skid' => $sistemKerjaId])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => TimeTable::findOne($id)->nama];
        }
        return $out;
    }

    /**
     * @param null $q
     * @param int $sistemKerjaId
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function actionGetPegawaiSelect2($q = null, $sistemKerjaId = 0, $id = 0)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, nama AS text')
                ->from('pegawai')
                ->where(['like', 'nama', $q])
                ->andWhere('sistem_kerja =:skid', [':skid' => $sistemKerjaId])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Pegawai::findOne($id)->nama];
        }
        return $out;
    }


    /**
     * @param null $q
     * @return array
     * @throws Exception
     */
    public function actionGetUserSelect2($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, username AS text')
                ->from('user')
                ->where(['LIKE', 'username', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }
}
<?php

namespace frontend\controllers;

use frontend\models\GantiHari;
use frontend\models\Pegawai;
use frontend\models\search\GantiHariSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * GantiHariController implements the CRUD actions for GantiHari model.
 */
class GantiHariController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GantiHari models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GantiHariSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GantiHari model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerGantiHariDetail = new ArrayDataProvider([
            'allModels' => $model->gantiHariDetails,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerGantiHariDetail' => $providerGantiHariDetail,
        ]);
    }

    /**
     * Creates a new GantiHari model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GantiHari();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                foreach ($model->pegawai_ids as $pegawai_id) {
                    $model->createDetail($pegawai_id);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing GantiHari model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $jumlahOld = $model->jumlah_jam;

        $pegawais = array_unique(array_map(function ($dt) {
            return $dt->pegawai_id;
        }, $model->gantiHariDetails));

        if (!empty($pegawais)) {
            foreach ($pegawais as $pegawai) {
                $model->pegawai_exist[$pegawai] = Pegawai::findOne($pegawai)->nama;
            }
            $model->pegawai_ids = array_keys($model->pegawai_exist);
        }
        $exist = $model->pegawai_ids;
        if ($model->load(Yii::$app->request->post())) {
            $jumlahNew = $model->jumlah_jam;
            $submit = $model->pegawai_ids;
            $removePegawais = array_diff($exist, $submit);
            $addPegawais = array_diff($submit, $exist);
            if ($model->save()) {
                if (!empty($removePegawais)) {
                    array_map(function ($d) use ($model) {
                        return $model->deleteDetail($d);
                    }, $removePegawais);
                }
                if (!empty($addPegawais)) {
                    array_map(function ($d) use ($model) {
                        return $model->createDetail($d);
                    }, $addPegawais);
                }
                if ($jumlahOld != $jumlahNew) {
                    $diff = $jumlahOld - $jumlahNew;
                }
//                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing GantiHari model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GantiHari model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GantiHari the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GantiHari::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}

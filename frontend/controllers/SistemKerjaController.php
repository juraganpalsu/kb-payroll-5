<?php

namespace frontend\controllers;

use common\components\Hari;
use common\models\PolaJadwalLibur;
use common\models\SistemKerja;
use common\models\SistemKerjaTimeTable;
use frontend\models\search\SistemKerjaSearch;
use kartik\widgets\ActiveForm;
use Throwable;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * SistemKerjaController implements the CRUD actions for SistemKerja model.
 */
class SistemKerjaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'delete-pola' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SistemKerja models.
     * @param string $click
     *
     * @return string|mixed
     */
    public function actionIndex(string $click = '')
    {
        $searchModel = new SistemKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'click' => $click
        ]);
    }

    /**
     * Displays a single SistemKerja model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerPolaJadwalLibur = new ArrayDataProvider([
            'allModels' => $model->polaJadwalLiburs,
        ]);
        $providerSistemKerjaTimeTable = new ArrayDataProvider([
            'allModels' => $model->sistemKerjaTimeTables,
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerPolaJadwalLibur' => $providerPolaJadwalLibur,
            'providerSistemKerjaTimeTable' => $providerSistemKerjaTimeTable,
        ]);
    }

    /**
     * Creates a new SistemKerja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SistemKerja();

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-sistem-kerja-row-' . $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing SistemKerja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-sistem-kerja-row-' . $model->id])], 'message' => Yii::t('frontend', 'Berhasil update data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing SistemKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            $model->delete();
        }

        return $this->redirect(['index']);
    }


    /**
     * Finds the SistemKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SistemKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SistemKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the SistemKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PolaJadwalLibur the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelPolaJadwalLibur($id)
    {
        if (($model = PolaJadwalLibur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the SistemKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SistemKerjaTimeTable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelSistemKerjaTimeTable($id)
    {
        if (($model = SistemKerjaTimeTable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     *
     * @return mixed
     * @throws NotAcceptableHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new SistemKerja();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * @return string|mixed
     */
    public function actionShowConfiguration()
    {
        $expand = Yii::$app->request->post('expandRowKey');
        if (isset($expand)) {
            try {
                $model = $this->findModel($expand);
                return $this->renderAjax('configuration', ['model' => $model]);
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        return '<div class="alert alert-danger">No data found</div>';
    }

    /**
     * @param int $skid
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionBuatPola(int $skid)
    {
        $modelSistemKerja = $this->findModel($skid);

        $model = new PolaJadwalLibur();
        $model->sistem_kerja_id = $modelSistemKerja->id;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-sistem-kerja-row-' . $model->sistem_kerja_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('buat-pola', [
            'model' => $model,
        ]);

    }


    /**
     * @param int $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePola(int $id)
    {
        $model = $this->findModelPolaJadwalLibur($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-sistem-kerja-row-' . $model->sistem_kerja_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        if (!$model->isNewRecord && !empty($model->hari) && $model->sistemKerja->jenis_libur == SistemKerja::HARI) {
            $model->hari = array_keys(Hari::getHarisIndonesia(Json::decode($model->hari)));
        }

        return $this->renderAjax('buat-pola', [
            'model' => $model,
        ]);

    }


    /**
     *
     * @param int $skid
     * @return mixed
     * @throws NotAcceptableHttpException
     * @throws NotFoundHttpException
     */
    public function actionBuatPolaValidation(int $skid)
    {
        $modelSistemKerja = $this->findModel($skid);

        $model = new PolaJadwalLibur();
        $model->sistem_kerja_id = $modelSistemKerja->id;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * @param int $id
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeletePola(int $id)
    {
        $model = $this->findModelPolaJadwalLibur($id);

        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost && $model) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-sistem-kerja-row-' . $model->sistem_kerja_id])], 'message' => Yii::t('frontend', 'Berhasil delete data.'), 'status' => true];

        }

        return $response->data;
    }


    /**
     * @param int $skid
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionTambahTimeTable(int $skid)
    {
        $modelSistemKerja = $this->findModel($skid);

        $model = new SistemKerjaTimeTable();
        $model->sistem_kerja_id = $modelSistemKerja->id;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-sistem-kerja-row-' . $model->sistem_kerja_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('tambah-time-table', [
            'model' => $model,
        ]);

    }

    /**
     * @param int $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateTimeTable(int $id)
    {
        $model = $this->findModelSistemKerjaTimeTable($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-sistem-kerja-row-' . $model->sistem_kerja_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('tambah-time-table', [
            'model' => $model,
        ]);

    }


    /**
     *
     * @return mixed
     * @throws NotAcceptableHttpException
     * @throws NotFoundHttpException
     */
    public function actionFormTimeTableValidation(int $skid)
    {
        $modelSistemKerja = $this->findModel($skid);

        $model = new SistemKerjaTimeTable();
        $model->sistem_kerja_id = $modelSistemKerja->id;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * @param int $id
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteTimeTable(int $id)
    {
        $model = $this->findModelSistemKerjaTimeTable($id);

        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost && $model) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-sistem-kerja-row-' . $model->sistem_kerja_id])], 'message' => Yii::t('frontend', 'Berhasil delete data.'), 'status' => true];

        }

        return $response->data;
    }


    /**
     * @param null $q
     * @return array
     */
    public function actionListSelect2($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = new SistemKerja();
            $out['results'] = array_values($model->listSelect2($q));
        }
        return $out;
    }
}

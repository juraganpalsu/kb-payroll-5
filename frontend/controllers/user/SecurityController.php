<?php

/**
 * Created with love at Aug 31, 2018
 * @author jiwa
 */

namespace frontend\controllers\user;

use dektrium\user\controllers\SecurityController as BaseSecurityController;
use dektrium\user\models\LoginForm;
use dektrium\user\Module;
use frontend\models\User;
use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\web\Response;

/**
 * Controller that manages user authentication process.
 *
 * @property Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SecurityController extends BaseSecurityController
{

    /**
     * Displays the login page.
     *
     * @return string|Response
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionLogin()
    {

        $this->layout = '@frontend/views/layouts/main-login';

        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::class);
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
            'module' => $this->module,
        ]);
    }

    /**
     * Logs the user out and then redirects to the homepage.
     *
     * @return Response
     * @throws InvalidConfigException
     */
    public function actionLogout()
    {
        /** @var User $user */
        $user = Yii::$app->user;
        $event = $this->getUserEvent($user->identity);

        $this->trigger(self::EVENT_BEFORE_LOGOUT, $event);

        Yii::$app->getUser()->logout();

        $this->trigger(self::EVENT_AFTER_LOGOUT, $event);

        return $this->goHome();
    }


}

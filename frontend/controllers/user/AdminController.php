<?php

namespace frontend\controllers\user;

use dektrium\user\controllers\AdminController as BaseAdminController;
use dektrium\user\models\UserSearch;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Url;

/**
 * UserController implements the CRUD actions for User model.
 */
class AdminController extends BaseAdminController
{

    /**
     * @return mixed|string
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        Url::remember('', 'actions-redirect');
        $searchModel = Yii::createObject(UserSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('@frontend/views/user/admin/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

}

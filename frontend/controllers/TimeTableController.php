<?php

namespace frontend\controllers;


use common\components\Hari;
use frontend\models\search\TimeTableSearch;
use frontend\models\TimeTable;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TimeTableController implements the CRUD actions for TimeTable model.
 */
class TimeTableController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TimeTable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimeTableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TimeTable model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        try {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } catch (NotFoundHttpException $e) {
            return $e;
        }
    }

    /**
     * Creates a new TimeTable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TimeTable();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'dataHari' => Hari::_hari,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TimeTable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        try {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
//                if (!$model->isNewRecord && !empty($model->untuk_hari)) {
//                    $model->untuk_hari = array_keys(Hari::getHarisIndonesia(Json::decode($model->untuk_hari)));
//                }
                return $this->render('update', [
                    'dataHari' => Hari::_hari,
                    'model' => $model,
                ]);
            }
        } catch (NotFoundHttpException $e) {
            return $e;
        }


    }

    /**
     * Deletes an existing TimeTable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (\Exception $e) {
            return $e;
        }

    }

    /**
     * Finds the TimeTable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TimeTable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TimeTable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param null $q
     * @return array
     */
    public function actionListSelect2($q = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = new TimeTable();
            $out['results'] = array_values($model->listSelect2($q));
        }
        return $out;
    }
}

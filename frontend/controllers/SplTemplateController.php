<?php

namespace frontend\controllers;

use common\models\SplTemplateDetail;
use frontend\models\search\SplTemplateSearch;
use frontend\models\SplTemplate;
use kartik\widgets\ActiveForm;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * SplTemplateController implements the CRUD actions for SplTemplate model.
 */
class SplTemplateController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SplTemplate models.
     *
     * @param string $click
     *
     * @return mixed
     */
    public function actionIndex(string $click = '')
    {
        $searchModel = new SplTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'click' => $click
        ]);
    }

    /**
     * Displays a single SplTemplate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        try {
            $model = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            return $e;
        }
        $providerSpl = new ArrayDataProvider([
            'allModels' => $model->spls,
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerSpl' => $providerSpl,
        ]);
    }

    /**
     * Creates a new SplTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SplTemplate();
        $arrSistemKerja = $model->getSistemKerja_();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'sistem_kerja' => $arrSistemKerja
            ]);
        }
    }

    /**
     * Updates an existing SplTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing SplTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        if ($model = $this->findModel($id)) {
            $model->delete();
        };

        return $this->redirect(['index']);
    }

    /**
     * Finds the SplTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SplTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SplTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return SplTemplateDetail|null
     * @throws NotFoundHttpException
     */
    protected function findModelDetailTemplate(string $id)
    {
        if (($model = SplTemplateDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * @return string
     */
    public function actionShowDetail()
    {
        $expand = Yii::$app->request->post('expandRowKey');
        if (isset($expand)) {
            try {
                $queryDetail = SplTemplateDetail::find();
                $model = $this->findModel($expand);
                $dataProvider = new ActiveDataProvider([
                    'query' => $queryDetail,
                ]);
                if ($model) {
                    $queryDetail->andWhere(['spl_template_id' => $model->id]);
                }
                return $this->renderAjax('index-detail', ['model' => $model, 'dataProvider' => $dataProvider]);
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        return '<div class="alert alert-danger">No data found</div>';
    }

    /**
     * @param int $stid
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionTambahkanTemplate(int $stid)
    {
        $splTemplate = $this->findModel($stid);

        $model = new SplTemplateDetail();
        $model->spl_template_id = $splTemplate->id;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-spl-template-row-' . $model->spl_template_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-tambah-template', [
            'model' => $model,
        ]);

    }

    /**
     * @return array
     * @throws NotAcceptableHttpException
     */
    public function actionTambahkanTemplateValidation()
    {
        $model = new SplTemplateDetail();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }


    /**
     * @param null $q
     * @param string $status
     * @return array
     */
    public function actionListSelect2($q = null, $status = '*')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = new SplTemplate();
            $out['results'] = array_values($model->listSelect2($q, $status));
        }
        return $out;
    }


    /**
     * @param string $id
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionRemoveDetailTemplate(string $id)
    {
        $model = $this->findModelDetailTemplate($id);

        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal hapus data.'), 'status' => false];
        if ($request->isAjax && $request->isPost && $model) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['index', 'click' => '#kv-grid-spl-template-row-' . $model->spl_template_id])], 'message' => Yii::t('frontend', 'Berhasil hapus data.'), 'status' => true];
        }
        return $response->data;
    }
}

<?php

namespace frontend\controllers;

use common\models\Absensi;
use common\models\PerubahanStatusAbsensi;
use common\models\PerubahanStatusAbsensiDetail;
use frontend\models\search\PerubahanStatusAbsensiSearch;
use kartik\widgets\ActiveForm;
use Throwable;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * PerubahanStatusAbsensiController implements the CRUD actions for PerubahanStatusAbsensi model.
 */
class PerubahanStatusAbsensiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PerubahanStatusAbsensi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PerubahanStatusAbsensiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelAbsensi = new Absensi();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelAbsensi' => $modelAbsensi,
            'click' => ''
        ]);
    }

    /**
     * Displays a single PerubahanStatusAbsensi model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerPerubahanStatusAbsensiDetail = new ArrayDataProvider([
            'allModels' => $model->perubahanStatusAbsensiDetails,
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerPerubahanStatusAbsensiDetail' => $providerPerubahanStatusAbsensiDetail,
        ]);
    }

    /**
     * Creates a new PerubahanStatusAbsensi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PerubahanStatusAbsensi();
        $modelAbsensi = new Absensi();

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $details = [];
            if ($model->load($request->post())) {
                if (!empty($model->pegawai_ids)) {
                    if ($model->save()) {
                        $details = $model->createDetail();
                    }
                }
                $response->data = ['data' => ['detail' => $details, 'url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'modelAbsensi' => $modelAbsensi
        ]);

    }

    /**
     * Updates an existing PerubahanStatusAbsensi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelAbsensi = new Absensi();

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $details = [];
            if ($model->load($request->post())) {
                $model->cekPegawaiExist();
                if (!empty($model->pegawai_ids)) {
                    if ($model->save()) {
                        $details = $model->createDetail();
                    }
                }
                $response->data = ['data' => ['detail' => $details, 'url' => Url::to(['index', 'click' => '#kv-grid-perubahan-status-absensi-row-' . $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'modelAbsensi' => $modelAbsensi
        ]);

    }

    /**
     * Deletes an existing PerubahanStatusAbsensi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            $model->delete();
            $pegawaiIds = [];
            if ($detail = $model->perubahanStatusAbsensiDetails) {
                $pegawaiIds = array_map(function (PerubahanStatusAbsensiDetail $detail) {
                    $detail->delete();
                    return $detail->pegawai_id;
                }, $detail);
            }
            if (!empty($pegawaiIds)) {
                $modelAbsensi = new Absensi();
                $modelAbsensi->prosesData($model->tanggal, $model->tanggal, null, $pegawaiIds);
            }
        }

        return $this->redirect(['index']);
    }


    /**
     * Finds the PerubahanStatusAbsensi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PerubahanStatusAbsensi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerubahanStatusAbsensi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     *
     * @return mixed
     * @throws NotAcceptableHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new PerubahanStatusAbsensi();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }
}

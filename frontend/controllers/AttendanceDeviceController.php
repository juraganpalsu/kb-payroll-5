<?php

namespace frontend\controllers;

use common\models\AttendanceDevice;
use frontend\models\search\AttendanceDeviceSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * AttendanceDeviceController implements the CRUD actions for AttendanceDevice model.
 */
class AttendanceDeviceController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AttendanceDevice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttendanceDeviceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the AttendanceDevice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AttendanceDevice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AttendanceDevice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @return array|mixed
     * @throws \yii\httpclient\Exception
     */
    public function actionGetDevice()
    {
        $model = new AttendanceDevice();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        $counter = 0;
        if ($request->isAjax && $request->isGet) {
            $getDevice = $model->getDevice();
            if (isset($getDevice['status']) && $getDevice['status']) {
                if (!empty($getDevice['data'])) {
                    foreach ($getDevice['data'] as $device) {
//                        print_r($device);
                        if ($model->saveDevice($device)) {
                            $counter++;
                        }
                    }
                }
                Yii::$app->getSession()->setFlash('berhasil', $counter . ' data berhasil disimpan.');
            } else {
                Yii::$app->getSession()->setFlash('gagal', Yii::t('app', 'No new data found!'));
            }
            $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
        }
        return $response->data;
    }
}

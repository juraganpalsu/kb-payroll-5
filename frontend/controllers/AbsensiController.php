<?php

namespace frontend\controllers;

use Exception;
use frontend\models\Absensi;
use frontend\models\form\AbsensiForm;
use frontend\models\Pegawai;
use frontend\models\search\AbsensiSearch;
use frontend\models\User;
use frontend\modules\dashboard\models\DsAbsensi;
use frontend\modules\queue\models\AntrianLaporanAbsensi;
use frontend\modules\queue\models\AntrianProsesAbsensi;
use frontend\modules\struktur\models\search\StrukturSearch;
use frontend\modules\struktur\models\Struktur;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * AbsensiController implements the CRUD actions for Absensi model.
 */
class AbsensiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Absensi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AbsensiSearch();
        ini_set('memory_limit', '6144M');
        ini_set('max_execution_time', 600);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $user = Yii::$app->user->identity;
        if (!$user) {
            return Yii::$app->controller->redirect(['/']);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexAll()
    {
        $searchModel = new AbsensiSearch();
        ini_set('memory_limit', '6144M');
        ini_set('max_execution_time', 600);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, false);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexEss()
    {
        $searchModel = new AbsensiSearch();
        $dataProvider = $searchModel->searchEss(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexAbsenTeam()
    {
        $searchModel = new AbsensiSearch();
        $dataProvider = $searchModel->searchEss(Yii::$app->request->queryParams, true);

        $user = Yii::$app->user->identity;
        if (!$user) {
            return Yii::$app->controller->redirect(['/']);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionEssBantuan()
    {
        /** @var User $modelUser */
        $modelUser = Yii::$app->user->identity;
        $modelPegawai = $modelUser->pegawai;

        return $this->render('bantuan', [
            'modelPegawai' => $modelPegawai,
        ]);
    }

    /**
     * Displays a single Absensi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionProses()
    {
        $model = new AbsensiForm();
        $model->scenario = 'proses-data';

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
//            ini_set('memory_limit', '8192M');
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->validate()) {
                if (empty($model->pegawai_ids)) {
                    $model->pegawai_ids = [];
                }
                $info = $model->prosesData();
                if (!empty($info)) {
                    Yii::$app->session->setFlash('info-proses', Absensi::formatInfoProses($info));
                }
                $response->data = ['data' => ['url' => Url::to(['proses'])], 'message' => Yii::t('frontend', 'Berhasil Proses data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal proses data.'), 'status' => false];
            }
//            ini_set('memory_limit', '1024M');
            return $response->data;
        }

        return $this->render('proses', [
            'model' => $model,
        ]);

    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormProsesValidation()
    {
        $model = new AbsensiForm();
        $model->scenario = 'proses-data';

        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @return array|mixed|string
     */
    public function actionProsesPerGolongan()
    {
        $model = new AntrianProsesAbsensi();
        $dataProvider = $model->search();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['proses-per-golongan'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => ['url' => Url::to(['proses-per-golongan'])], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_form-proses-per-golongan', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);

    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormProsesPerGolonganValidation()
    {
        $model = new AntrianProsesAbsensi();

        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Finds the Absensi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Absensi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Absensi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * @return array|mixed|string
     */
    public function actionLaporanForm()
    {
        $modelForm = new AbsensiForm();
        $model = new AntrianLaporanAbsensi();
        $dataProvider = $model->search();
        $sistemKerja = $modelForm->getSistemKerja();
        $statusKehadiran = $modelForm->getStatusKehadiran();
        unset($statusKehadiran[3], $statusKehadiran[4]);
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;

            $response->data = [
                'data' => ['url' => Url::to(['laporan-form'])],
                'message' => $model->errors,
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['laporan-form'])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->render('laporanForm', ['model' => $model, 'sistemKerja' => $sistemKerja, 'statusKehadiran' => $statusKehadiran, 'dataProvider' => $dataProvider]);

    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionLaporanValidation()
    {
        $model = new AntrianLaporanAbsensi();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @return string
     */
    public function actionShowDetail()
    {
        $expand = Yii::$app->request->post('expandRowKey');
        if (isset($expand)) {
            try {
                $model = $this->findModel($expand);
                return $this->renderAjax('_index-detail', ['model' => $model]);
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        return '<div class="alert alert-danger">No data found</div>';
    }

    /**
     * @param string $id
     * @return array|mixed|Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteQueueProses(string $id)
    {
        $model = AntrianProsesAbsensi::findOne($id);
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model && $model->status == AntrianProsesAbsensi::DIBUAT) {
                $model->delete();
                $response->data = ['data' => ['url' => Url::to(['proses-per-golongan'])], 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
            } else {
                $response->data = ['data' => ['url' => Url::to(['proses-per-golongan'])], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
            }
            return $response->data;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param string $id
     * @return array|mixed|Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteQueueLaporan(string $id)
    {
        $model = AntrianLaporanAbsensi::findOne($id);
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model) {
                if ($model->status == AntrianProsesAbsensi::DIBUAT || $model->status == AntrianProsesAbsensi::SELESAI) {
//                    $path = Yii::getAlias('@frontend') . AntrianLaporanAbsensi::filePathLaporan . $model->id . '.xlsx';
//                    if (file_exists($path)) {
//                        unlink($path);
//                    }
                    $model->delete();
                }
                $response->data = ['data' => ['url' => Url::to(['laporan-form'])], 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
            } else {
                $response->data = ['data' => ['url' => Url::to(['laporan-form'])], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
            }
            return $response->data;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param string $id
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     */
    public function actionDownloadQueueLaporan(string $id)
    {
        $model = AntrianLaporanAbsensi::findOne($id);
        $path = Yii::getAlias('@frontend') . AntrianLaporanAbsensi::filePathLaporan . $model->id . '.xlsx';

        if (file_exists($path)) {
            $namaFile = $model->tanggal_awal . '-' . $model->tanggal_akhir . '-' . $model->golongan->nama;
            if ($model->bisnis_unit) {
                $namaFile .= '-' . AntrianProsesAbsensi::getBisnisUnit()[$model->bisnis_unit];
            }
            if ($model->jenis_perjanjian) {
                $namaFile .= '-' . $model->jenisPerjanjian->nama;
            }
            $namaFile .= '-' . $model->createdBy->username;
            return Yii::$app->response->sendFile($path, $namaFile . '.xlsx');
        }
        throw new NotFoundHttpException(Yii::t('app', 'The file does not exist.'));
    }

    public function actionDashboardAbsensi()
    {
        $searchModel = new StrukturSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('dashboard-alfa', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


    }

}

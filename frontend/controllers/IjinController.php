<?php

namespace frontend\controllers;

use frontend\models\Ijin;
use frontend\models\search\IjinSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * IjinController implements the CRUD actions for Ijin model.
 */
class IjinController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ijin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IjinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ijin model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Ijin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ijin();

        if ($model->load(Yii::$app->request->post())) {
            $model->create();
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Ijin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post())) {
//            foreach ($model->pegawai_ids as $pegawai_id) {
//                $check = Ijin::findOne(['pegawai_id' => $pegawai_id, 'tanggal' => $model->tanggal]);
//                if (!$check) {
//                    $newModel = $this->findModel($id);
//                    $newModel->scenario = 'newmodel';
//                    $newModel->attributes = $model->attributes;
//                    $newModel->pegawai_id = $pegawai_id;
//                    $newModel->save();
//                }
//            }
////            return $this->redirect(['view', 'id' => $model->id]);
//        }
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//
//    }

    /**
     * Deletes an existing Ijin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->delete($id);
        return $this->redirect(['index']);
    }


    /**
     * @param int $id
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function delete(int $id)
    {
        $model = $this->findModel($id);
        $model->delete();
        $model->prosesData([$model->pegawai_id]);
    }

    /**
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteMultiple()
    {
        $model = new Ijin();

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $model->ids = $request->post('ids');
            if ($model->ids) {
                foreach ($model->ids as $id) {
                    $this->delete($id);
                }
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
            }
            return $response->data;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Finds the Ijin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ijin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ijin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}

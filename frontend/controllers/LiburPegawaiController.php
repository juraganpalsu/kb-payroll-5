<?php

namespace frontend\controllers;

use frontend\models\LiburPegawai;
use frontend\models\search\LiburPegawaiSearch;
use PhpOffice\PhpSpreadsheet\Exception;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * LiburPegawaiController implements the CRUD actions for LiburPegawai model.
 */
class LiburPegawaiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiburPegawai models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiburPegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiburPegawai model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LiburPegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new LiburPegawai();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LiburPegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LiburPegawai model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->delete($id);
        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    private function delete(int $id)
    {
        $model = $this->findModel($id);
        $model->delete();
        $model->prosesData([$model->pegawai_id]);
    }

    /**
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteMultiple()
    {
        $model = new LiburPegawai();

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $model->ids = $request->post('ids');
            if ($model->ids) {
                foreach ($model->ids as $id) {
                    $this->delete($id);
                }
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
            }
            return $response->data;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    /**
     * Finds the LiburPegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiburPegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiburPegawai::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @return array|mixed
     * @throws MethodNotAllowedHttpException
     */
    public function actionGetMonth()
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        if ($request->isPost && $request->isAjax) {
            $model = new LiburPegawai();
            $model->scenario = 'download-file';
            $model->setAttributes($request->post());
            if ($model->validate()) {
                $url = Url::to(['libur-pegawai/get-libur-form/', 'LiburPegawai' => $request->post()]);
                $response->data = ['status' => true, 'url' => $url];;
                return $response->data;
            }
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return mixed
     * @throws MethodNotAllowedHttpException
     */
    public function actionGetLiburForm()
    {
        $request = Yii::$app->request;
        $model = new LiburPegawai();
        $model->scenario = 'download-file';
        $model->load($request->queryParams);
        if ($model->validate()) {
            try {
                return $model->downloadFile();
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return string
     */
    public function actionUploadLibur()
    {
        $model = new LiburPegawai();
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $model->scenario = 'upload-file';
            $model->setAttributes($request->post());
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->upload()) {
                $dataLibur = [];
                try {
                    $dataLibur = $model->extractLiburFromExcel();
                } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                $simpanLibur = $model->createLibur($dataLibur);
                return Json::encode(['data' => $simpanLibur, 'status' => true, 'message' => Yii::t('app', 'Berhasil Upload Data')]);
            }
        }
        return $this->render('upload', ['model' => $model]);
    }
}

<?php

namespace frontend\controllers;

use common\components\Networking;
use frontend\models\Attendance;
use frontend\models\Pegawai;
use frontend\models\search\AttendanceBeatrouteSearch;
use frontend\models\search\AttendanceSearch;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * AttendanceController implements the CRUD actions for Attendance model.
 */
class AttendanceController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Attendance models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttendanceSearch();
//        $searchModel->ip = '192.168.2.201';
//        $searchModel->getAttendanceFromMachine();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexEss()
    {
        $searchModel = new AttendanceSearch();
        $dataProvider = $searchModel->searchEss(Yii::$app->request->queryParams);

        return $this->render('index-ess', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexTeam()
    {
        $searchModel = new AttendanceSearch();
        $dataProvider = $searchModel->searchEss(Yii::$app->request->queryParams, true);

        return $this->render('index-ess', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attendance::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @return string
     */
    public function actionImportAttendance()
    {
        $model = new Attendance();
//        $model->getAttContentXls();

//        $model->getAttContent();
        return $this->render('import', ['model' => $model]);
    }

    /**
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function actionUploadFile()
    {
        $model = new Attendance();
        $model->scenario = 'import-attendance';
        if (Yii::$app->request->isPost) {
            $model->file_att = UploadedFile::getInstance($model, 'file_att');
            if ($model->upload()) {
                if ($model->file_att->extension == 'txt') {
                    $getContent = $model->getAttContent();
                }
                if (in_array($model->file_att->extension, ['xls', 'xlsx', 'csv'])) {
                    $getContent = $model->getAttContentXls();
                }
                return Json::encode(['data' => ['jumlah' => $getContent], 'status' => true, 'message' => Yii::t('app', 'Berhasil Upload Data')]);
            }
        }
        return Json::encode(['data' => '', 'status' => false, 'message' => Yii::t('app', 'Gagal Upload Data')]);
    }

    /**
     * Tarik data presensi
     * @return string
     */
    public function actionGetAttendanceFromMachine()
    {
        $model = new Attendance();
        $model->scenario = 'get-attendance-from-machine';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (Networking::ping($model->ip)) {
                $jumlah = $model->getAttendanceFromMachine();
                Yii::$app->getSession()->setFlash('berhasil', $jumlah . ' data berhasil disimpan.');
            } else {
                $model->addError('ip', Yii::t('app', 'Tidak dapat terhubung dengan mesin absen ' . $model->ip));
            }
        }
        return $this->render('getAttendanceFromMachine', ['model' => $model]);
    }


    /**
     * Tarik data presensi dari adms
     * @return string
     */
    public function actionGetAttendanceFromAdms()
    {
        $model = new Attendance();
        $model->scenario = 'get-attendance-from-adms';
        $counter = 0;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if (Networking::ping(Yii::$app->params['attendance']['host'])) {
            $perjanjianKerjas = PerjanjianKerja::find()->select('id_pegawai')->andWhere(['IN', 'pegawai_id', $model->pegawai_ids])->asArray()->all();
            $perjanjianKerjas = array_map(function ($x) {
                return $x['id_pegawai'];
            }, $perjanjianKerjas);
            $downloadAttendance = [];
            if (!empty($model->date) && $perjanjianKerjas) {
                $date = explode('s/d', $model->date);
                $start = date('Y-m-d', strtotime($date[0]));
                $end = date('Y-m-d', strtotime($date[1]));

                $modelAttendanceConsole = new \console\models\Attendance();
                $downloadAttendance = $modelAttendanceConsole->downloadFromAdms(50, \console\models\Attendance::NOIDFP, $start, $end, $perjanjianKerjas);
            }
            if (isset($downloadAttendance['status']) && $downloadAttendance['status']) {
                if (!empty($downloadAttendance['data'])) {
                    foreach ($downloadAttendance['data'] as $attendance) {
                        if ($model->saveAttendance($attendance['userid'], $attendance['checktime'])) {
                            $counter++;
                        }
                    }
                }
                Yii::$app->getSession()->setFlash('berhasil', $counter . ' data berhasil disimpan.');
            } else {
                Yii::$app->getSession()->setFlash('gagal', Yii::t('app', 'No new data found!'));
            }
//            } else {
//                $model->addError('ip', Yii::t('app', 'Tidak dapat terhubung dengan server adms.'));
//            }
        }
        if (!empty($model->pegawai_ids)) {
            $pegawai = [];
            foreach ($model->pegawai_ids as $pegawai_id) {
                $modelPegawai = Pegawai::findOne($pegawai_id);
                if ($modelPegawai) {
                    $pegawai[$modelPegawai->id] = $modelPegawai->nama_lengkap . ' - ' . $modelPegawai->nama_panggilan;
                }
            }
            $model->pegawai_ids = $pegawai;
        }
        return $this->render('get-attendance-from-adms', ['model' => $model]);
    }

    /**
     * @return string
     */
    public function actionIndexBeatroute()
    {
        $searchModel = new AttendanceBeatrouteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/attendance-beatroute/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

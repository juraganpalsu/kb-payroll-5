<?php

use kartik\datecontrol\Module;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'HRIS KOBE',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['exception'],
                    'logFile' => '@frontend/runtime/logs/exception.log',
                    'logVars' => ['_SERVER'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['download-device'],
                    'logFile' => '@frontend/runtime/logs/download-device.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['simpan-thr'],
                    'logFile' => '@frontend/runtime/logs/simpan-thr.log',
                    'logVars' => [],
                ]
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@frontend/views/themes',
                    '@dektrium/user/views' => '@frontend/views/themes/dektrium',
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'mainLayout' => '@app/views/layouts/main.php',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            // following line will restrict access to admin controller from frontend application
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
            'controllers' => [],
            'controllerMap' => [
                'registration' => 'frontend\controllers\user\RegistrationController',
                'security' => 'frontend\controllers\user\SecurityController',
                'admin' => 'frontend\controllers\user\AdminController'
            ],
            'modelMap' => [
                'User' => 'frontend\models\User',
            ],
            'admins' => ['jiwandaru', 'bintang.famy', 'bobi.arip']
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
            'displaySettings' => [
                Module::FORMAT_DATE => 'dd-MM-yyyy',
                Module::FORMAT_TIME => 'php:H:i:s',
                Module::FORMAT_DATETIME => 'dd-MM-yyyy hh:mm:ss a',
            ],
            'saveSettings' => [
                Module::FORMAT_DATE => 'php:Y-m-d',
                Module::FORMAT_TIME => 'php:H:i:s',
                Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],
//            'displayTimezone' => 'Asia/Jakarta',
//            'saveTimezone' => 'UTC',
            'autoWidget' => true,
            'autoWidgetSettings' => [
                Module::FORMAT_DATE => ['type' => 2, 'pluginOptions' => ['autoclose' => true]],
                Module::FORMAT_DATETIME => [],
                Module::FORMAT_TIME => [],
            ],
            'widgetSettings' => [
                Module::FORMAT_DATE => [
                    'class' => 'yii\jui\DatePicker',
                    'options' => [
                        'dateFormat' => 'php:d-M-Y',
                        'options' => ['class' => 'form-control'],
                    ]
                ]
            ]
        ],
        'dynagrid' => [
            'class' => '\kartik\dynagrid\Module',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'struktur' => [
            'class' => '\frontend\modules\struktur\Module',
        ],
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        ],
        'pegawai' => [
            'class' => 'frontend\modules\pegawai\Module',
        ],
        'indonesia' => [
            'class' => 'frontend\modules\indonesia\Module',
        ],
        'personalia' => [
            'class' => 'frontend\modules\personalia\Module',
        ],
        'dashboard' => [
            'class' => 'frontend\modules\dashboard\Module',
        ],
        'izin' => [
            'class' => 'frontend\modules\izin\Module',
        ],
        'spl' => [
            'class' => 'frontend\modules\spl\Module',
        ],
        'masterdata' => [
            'class' => 'frontend\modules\masterdata\Module',
        ],
        'payroll' => [
            'class' => 'frontend\modules\payroll\Module',
        ],
        'queue' => [
            'class' => 'frontend\modules\queue\Module',
        ],
        'erp' => [
            'class' => 'frontend\modules\erp\Module',
        ],
        'wsc' => [
            'class' => 'frontend\modules\wsc\Module',
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/login',
            'site/logout',
            'datecontrol/*',
//            'gii/*',
//            'indonesia/*',
//            'barang/*',
//            'pelanggan/*',
//            'transaksi/*',
//            'izin/*',
//            'libur-pegawai/*',
//            'pegawai/*',
//            'attendance/*',
//            'helper/*',
//            'cost-center-template/*',
//            'struktur/*',
//            'spl-template/*',
//            'spl/*',
//            'personalia/*',
//            'attendance-device/*',
//            'masterdata/*',
//            'payroll/*',
//            'absensi/*',
//            'erp/*'
        ]
    ],
    'params' => $params,
];

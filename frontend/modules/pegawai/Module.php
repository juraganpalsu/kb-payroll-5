<?php

namespace frontend\modules\pegawai;

/**
 * pegawai module definition class
 * @property array $_aksi
 */
class Module extends \yii\base\Module
{

    const DEFAULT = 0;
    const PROMOSI = 1;
    const DEMOSI = 2;
    const MUTASI = 3;
    const ROTASI = 4;
    const REJOIN = 5;
    const TO = 6;
    const OFF = 7;


    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\pegawai\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * @return array
     */
    public static function aksi()
    {
        return [
            self::DEFAULT => '-',
            self::PROMOSI => 'PROMOSI',
            self::DEMOSI => 'DEMOSI',
            self::MUTASI => 'MUTASI',
            self::ROTASI => 'ROTASI',
            self::REJOIN => 'REJOIN',
            self::TO => 'Take Over',
            self::OFF => 'OFF'
        ];
    }

    /**
     * @param int $aksi
     * @return mixed
     */
    public static function getAksi(int $aksi)
    {
        return self::aksi()[$aksi];
    }
}

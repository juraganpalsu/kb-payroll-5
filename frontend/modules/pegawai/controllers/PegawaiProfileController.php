<?php


namespace frontend\modules\pegawai\controllers;


use frontend\modules\pegawai\models\form\PegawaiForm;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiAsuransiLain;
use frontend\modules\pegawai\models\PegawaiBank;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiFiles;
use frontend\modules\pegawai\models\PegawaiKeluarga;
use frontend\modules\pegawai\models\PegawaiKontakDarurat;
use frontend\modules\pegawai\models\PegawaiNpwp;
use frontend\modules\pegawai\models\PegawaiPendidikan;
use frontend\modules\personalia\models\Inventaris;
use frontend\modules\personalia\models\SuratIzinMengemudi;
use kartik\widgets\ActiveForm;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * File Name: PegawaiProfileController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 29/09/19
 * Time: 11.34
 */
class PegawaiProfileController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'delete-pegawai-pendidikan' => ['post'],
                    'delete-pegawai-keluarga' => ['post'],
                    'delete-pegawai-bpjs' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param string $id
     * @return string
     */
    public function actionPegawaiProfileForm(string $id)
    {
        $model = Pegawai::findOne($id);
        if (!$model) {
            $model = new Pegawai();
        }

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-profile', ['model' => $model]);
    }

    /**
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiProfileValidation(bool $isnew = true)
    {
        $model = new Pegawai();
        $model->isNewRecord = $isnew;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $pegawaiid
     * @return string
     */
    public function actionPegawaiNpwpForm(string $pegawaiid)
    {
        $model = PegawaiNpwp::findOne(['pegawai_id' => $pegawaiid]);
        if (!$model) {
            $model = new PegawaiNpwp();
            $model->pegawai_id = $pegawaiid;
        }

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-npwp', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiNpwpValidation()
    {
        $model = new PegawaiNpwp();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $pegawaiid
     * @return string
     */
    public function actionPegawaiAsuransiForm(string $pegawaiid)
    {
        $model = PegawaiAsuransiLain::findOne(['pegawai_id' => $pegawaiid]);
        if (!$model) {
            $model = new PegawaiAsuransiLain();
            $model->pegawai_id = $pegawaiid;
        }

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-asuransi', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiAsuransiValidation()
    {
        $model = new PegawaiAsuransiLain();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $pegawaiid
     * @return string
     */
    public function actionCreatePegawaiPendidikan(string $pegawaiid)
    {
        $model = new PegawaiPendidikan();
        $model->pegawai_id = $pegawaiid;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-pendidikan', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return PegawaiPendidikan|null
     * @throws NotFoundHttpException
     */
    protected function findModelPegawaiPendidikan(string $id)
    {
        if (($model = PegawaiPendidikan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePegawaiPendidikan(string $id)
    {
        $model = $this->findModelPegawaiPendidikan($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-pendidikan', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiPendidikanValidation()
    {
        $model = new PegawaiPendidikan();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeletePegawaiPendidikan(string $id)
    {
        $model = $this->findModelPegawaiPendidikan($id);
        $model->delete();

        return $this->redirect(['/pegawai/pegawai/view', 'id' => $model->pegawai_id]);
    }

    /**
     * @param string $pegawaiid
     * @return string
     */
    public function actionCreatePegawaiBpjs(string $pegawaiid)
    {
        $model = new PegawaiBpjs();
        $model->pegawai_id = $pegawaiid;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-bpjs', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return PegawaiBpjs|null
     * @throws NotFoundHttpException
     */
    protected function findModelPegawaiBpjs(string $id)
    {
        if (($model = PegawaiBpjs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePegawaiBpjs(string $id)
    {
        $model = $this->findModelPegawaiBpjs($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-bpjs', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiBpjsValidation()
    {
        $model = new PegawaiBpjs();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeletePegawaiBpjs(string $id)
    {
        $model = $this->findModelPegawaiBpjs($id);
        $model->delete();

        return $this->redirect(['/pegawai/pegawai/view', 'id' => $model->pegawai_id]);
    }

    /**
     * @param string $pegawaiid
     * @return string
     */
    public function actionCreatePegawaiKeluarga(string $pegawaiid)
    {
        $model = new PegawaiKeluarga();
        $model->pegawai_id = $pegawaiid;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-keluarga', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return PegawaiKeluarga|null
     * @throws NotFoundHttpException
     */
    protected function findModelPegawaiKeluarga(string $id)
    {
        if (($model = PegawaiKeluarga::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePegawaiKeluarga(string $id)
    {
        $model = $this->findModelPegawaiKeluarga($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-keluarga', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiKeluargaValidation()
    {
        $model = new PegawaiKeluarga();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeletePegawaiKeluarga(string $id)
    {
        $model = $this->findModelPegawaiKeluarga($id);
        $model->delete();

        return $this->redirect(['/pegawai/pegawai/view', 'id' => $model->pegawai_id]);
    }


    /**
     * @param string $pegawaiid
     * @return string
     */
    public function actionCreatePegawaiDarurat(string $pegawaiid)
    {
        $model = new PegawaiKontakDarurat();
        $model->pegawai_id = $pegawaiid;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-darurat', ['model' => $model]);
    }


    /**
     * @param string $id
     * @return PegawaiKontakDarurat|null
     * @throws NotFoundHttpException
     */
    protected function findModelPegawaiDarurat(string $id)
    {
        if (($model = PegawaiKontakDarurat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePegawaiDarurat(string $id)
    {
        $model = $this->findModelPegawaiDarurat($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-darurat', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiDaruratValidation()
    {
        $model = new PegawaiKontakDarurat();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeletePegawaiDarurat(string $id)
    {
        $model = $this->findModelPegawaiDarurat($id);
        $model->delete();

        return $this->redirect(['/pegawai/pegawai/view', 'id' => $model->pegawai_id]);
    }


    /**
     * @param string $pegawaiid
     * @return string
     */
    public function actionPegawaiBankForm(string $pegawaiid)
    {
        $model = PegawaiBank::findOne(['pegawai_id' => $pegawaiid]);
        if (!$model) {
            $model = new PegawaiBank();
            $model->pegawai_id = $pegawaiid;
        }

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-bank', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiBankValidation()
    {
        $model = new PegawaiBank();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @return Pegawai|null
     * @throws NotFoundHttpException
     */
    protected function findModelPegawai(string $id)
    {
        if (($model = Pegawai::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $pegawaiid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdateFoto(string $pegawaiid)
    {
        $model = $this->findModelPegawai($pegawaiid);
        if (Yii::$app->request->isPost) {
            if ($model->uploadFile()) {
                return Json::encode(['data' => [], 'status' => true, 'message' => Yii::t('app', 'Berhasil Upload Data')]);
            }
            return Json::encode(['data' => [], 'status' => false, 'message' => Yii::t('app', 'Gagal Upload Data')]);
        }
        return $this->renderAjax('_form-foto-profil', ['model' => $model]);
    }

    /**
     * @param string $id Keluarga Id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionFormKeluargaBpjs($id)
    {
        $modelPegawaiKeluarga = $this->findModelPegawaiKeluarga($id);

        $model = PegawaiBpjs::findOne(['pegawai_keluarga_id' => $modelPegawaiKeluarga->id]);
        if (!$model) {
            $model = new PegawaiBpjs();
            $model->pegawai_id = $modelPegawaiKeluarga->pegawai_id;
            $model->pegawai_keluarga_id = $modelPegawaiKeluarga->id;
        }

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-keluarga-bpjs', ['model' => $model]);
    }


    /**
     * @param string $id Keluarga Id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionFormKeluargaAsuransi($id)
    {
        $modelPegawaiKeluarga = $this->findModelPegawaiKeluarga($id);

        $model = PegawaiAsuransiLain::findOne(['pegawai_keluarga_id' => $modelPegawaiKeluarga->id]);
        if (!$model) {
            $model = new PegawaiAsuransiLain();
            $model->pegawai_id = $modelPegawaiKeluarga->pegawai_id;
            $model->pegawai_keluarga_id = $modelPegawaiKeluarga->id;
        }

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-keluarga-asuransi', ['model' => $model]);
    }

    public function actionFormExport()
    {
        $model = new PegawaiForm();
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => '',
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post())) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai-profile/export', 'PegawaiForm' => $model->attributes])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-export', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormExportValidation()
    {
        $model = new PegawaiForm();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionExport()
    {
        $params = Yii::$app->request->queryParams;
        $model = new PegawaiForm();
        $model->load($params);
        if ($model->validate()) {
            return $model->export();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @param int $resign
     * @return bool
     */
    public function actionExportAll(int $resign)
    {
        set_time_limit(120);
        $model = new PegawaiForm();
        return $model->exportAll($resign);
    }

    /**
     * @param int $resign
     * @return bool
     */
    public function actionExportAllId(int $resign)
    {
        set_time_limit(120);
        $model = new PegawaiForm();
        return $model->exportAllId($resign);
    }

    /**
     * @param int $resign
     * @return bool
     */
    public function actionExportAllBca(int $resign)
    {
        set_time_limit(120);
        $model = new PegawaiForm();
        return $model->exportAllBca($resign);
    }

    /**
     * @param string $pegawaiid
     * @return array|mixed|string
     */
    public function actionCreateInventaris(string $pegawaiid)
    {
        $model = new Inventaris();
        $model->pegawai_id = $pegawaiid;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-inventaris', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return Inventaris|null
     * @throws NotFoundHttpException
     */
    protected function findModelInventaris(string $id)
    {
        if (($model = Inventaris::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateInventaris(string $id)
    {
        $model = $this->findModelInventaris($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-inventaris', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormInventarisValidation()
    {
        $model = new Inventaris();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteInventaris(string $id)
    {
        $model = $this->findModelInventaris($id);
        $model->delete();

        return $this->redirect(['/pegawai/pegawai/view', 'id' => $model->pegawai_id]);
    }

    /**
     * @param string $pegawaiid
     * @return array|mixed|string
     */
    public function actionCreateSim(string $pegawaiid)
    {
        $model = new SuratIzinMengemudi();
        $model->pegawai_id = $pegawaiid;

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-sim', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return SuratIzinMengemudi|null
     * @throws NotFoundHttpException
     */
    protected function findModelSim(string $id)
    {
        if (($model = SuratIzinMengemudi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateSim(string $id)
    {
        $model = $this->findModelSim($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-sim', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormSimValidation()
    {
        $model = new Inventaris();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteSim(string $id)
    {
        $model = $this->findModelSim($id);
        $model->delete();

        return $this->redirect(['/pegawai/pegawai/view', 'id' => $model->pegawai_id]);
    }


    /**
     * @param string $pegawaiid
     * @return array|mixed|string
     */
    public function actionUploadFile(string $pegawaiid)
    {
        $model = new PegawaiFiles();
        $model->pegawai_id = $pegawaiid;

        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [$model->errors], 'status' => false, 'message' => Yii::t('frontend', 'Gagal Upload Data')];
            if ($model->load($request->post()) && $model->upload()) {
                $response->data = ['data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])], 'status' => true, 'message' => Yii::t('frontend', 'Berhasil Upload Data')];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-upload-files', ['model' => $model]);
    }


    /**
     * @param string $id
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadFileValidation(string $id = '')
    {
        $model = new PegawaiFiles();
        if (!empty($id)) {
            $model = PegawaiFiles::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateFile(string $id)
    {
        $model = PegawaiFiles::findOne($id);

        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [$model->errors], 'status' => false, 'message' => Yii::t('frontend', 'Gagal Upload Data')];
            if ($model->load($request->post()) && $model->upload()) {
                $response->data = ['data' => ['url' => Url::to(['/pegawai/pegawai/view', 'id' => $model->pegawai_id])], 'status' => true, 'message' => Yii::t('frontend', 'Berhasil Upload Data')];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-upload-files', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return \yii\console\Response|Response
     */
    public function actionViewFile(string $id)
    {
        $model = PegawaiFiles::findOne($id);
        $dir = $model->upload_dir . '/' . $model->pegawai_id . '/';
        $fileName = $dir . $model->id . $model->nama;
        return Yii::$app->response->sendFile($fileName, null, ['inline' => true]);
    }


    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteFile(string $id)
    {
        $model = PegawaiFiles::findOne($id);
        $model->delete();

        return $this->redirect(['/pegawai/pegawai/view', 'id' => $model->pegawai_id]);
    }
}
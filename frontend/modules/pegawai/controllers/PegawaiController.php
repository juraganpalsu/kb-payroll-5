<?php

namespace frontend\modules\pegawai\controllers;

use common\models\PegawaiSistemKerja;
use frontend\models\search\PegawaiSearch;
use frontend\models\User;
use frontend\modules\izin\models\CutiSaldo;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\pegawai\models\AkunBeatroute;
use frontend\modules\pegawai\models\AreaKerja;
use frontend\modules\pegawai\models\CostCenter;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiFiles;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiKeluarga;
use frontend\modules\pegawai\models\PegawaiKontakDarurat;
use frontend\modules\pegawai\models\PegawaiPendidikan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\PkPoc;
use frontend\modules\pegawai\models\UserPegawai;
use frontend\modules\personalia\models\Inventaris;
use frontend\modules\personalia\models\SuratIzinMengemudi;
use frontend\modules\personalia\models\SuratPeringatan;
use frontend\modules\personalia\models\TrainingDetail;
use kartik\form\ActiveForm;
use mdm\admin\components\Helper;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Query;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * PegawaiController implements the CRUD actions for Pegawai model.
 */
class PegawaiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pegawai models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
        $modelPerjanjiankerja = new PerjanjianKerja();
        $modelStruktur = new PegawaiStruktur();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
            'modelStruktur' => $modelStruktur

        ]);
    }

    /**
     * Lists all Pegawai models.
     * @return mixed
     */
    public function actionIndexAll()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelPerjanjiankerja = new PerjanjianKerja();
        $modelStruktur = new PegawaiStruktur();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
            'modelStruktur' => $modelStruktur

        ]);
    }

    /**
     * Lists all Pegawai models.
     * @return mixed
     */
    public function actionBcaIndex()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->searchBca(Yii::$app->request->queryParams);
        $modelPerjanjiankerja = new PerjanjianKerja();
        $modelStruktur = new PegawaiStruktur();

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
            'modelStruktur' => $modelStruktur
        ]);
    }

    /**
     * Lists all Pegawai models.
     * @return mixed
     */
    public function actionPegawaiAktifTanpaDasarMasaKerja()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->searchPegawaiAktifTanpaDasarMasaKerja(Yii::$app->request->queryParams);

        return $this->render('index-pegawai-aktif-tanpa-dasar-masa-kerja', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Pegawai models.
     * @return mixed
     */
    public function actionPegawaiAktifTanpaMasterData()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->searchPegawaiAktifTanpaMasterData(Yii::$app->request->queryParams);

        return $this->render('index-pegawai-aktif-tanpa-master-data', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Pegawai models.
     * @return mixed
     */
    public function actionPegawaiAktifTanpaSistemKerja()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->searchPegawaiAktifSistemKerja(Yii::$app->request->queryParams);

        return $this->render('index-pegawai-aktif-tanpa-sistem-kerja', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pegawai model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $querySistemKerja = PegawaiSistemKerja::find()->andWhere(['pegawai_id' => $model->id])->orderBy('mulai_berlaku DESC');

        $providerSistemKerja = new ActiveDataProvider([
            'query' => $querySistemKerja,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        $providerStruktur = new ArrayDataProvider([
            'allModels' => $model->pegawaiStrukturs,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        $providerPerjanjianKerja = new ArrayDataProvider([
            'allModels' => $model->perjanjianKerjas,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        $providerAkunBeatroute = new ArrayDataProvider([
            'allModels' => $model->akunBeatroutes,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        $providerCostCenter = new ArrayDataProvider([
            'allModels' => $model->costCenters,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        $providerAreaKerja = new ArrayDataProvider([
            'allModels' => $model->areaKerjas,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        $providerPegawaiGolongan = new ArrayDataProvider([
            'allModels' => $model->pegawaiGolongans,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        $modelProfile = $model;

        $queryPendidikan = PegawaiPendidikan::find()->andWhere(['pegawai_id' => $model->id])->orderBy('jenjang DESC');
        $providerPendidikan = new ActiveDataProvider([
            'query' => $queryPendidikan
        ]);

        $queryBpjs = PegawaiBpjs::find()->andWhere(['pegawai_id' => $model->id, 'pegawai_keluarga_id' => null])->orderBy('jenis DESC');
        $providerBpjs = new ActiveDataProvider([
            'query' => $queryBpjs
        ]);

        $queryKeluarga = PegawaiKeluarga::find()->andWhere(['pegawai_id' => $model->id])->orderBy('hubungan, urutan_ke ASC');
        $providerKeluarga = new ActiveDataProvider([
            'query' => $queryKeluarga
        ]);

        $queryKontakDarurat = PegawaiKontakDarurat::find()->andWhere(['pegawai_id' => $model->id])->orderBy('hubungan ASC');
        $providerKontakDarurat = new ActiveDataProvider([
            'query' => $queryKontakDarurat
        ]);

        $queryInventaris = Inventaris::find()->andWhere(['pegawai_id' => $model->id])->orderBy('created_at ASC');
        $providerInventaris = new ActiveDataProvider([
            'query' => $queryInventaris
        ]);

        $querySim = SuratIzinMengemudi::find()->andWhere(['pegawai_id' => $model->id])->orderBy('created_at ASC');
        $providerSim = new ActiveDataProvider([
            'query' => $querySim
        ]);

        $querySuratPeringatan = SuratPeringatan::find()->andWhere(['pegawai_id' => $model->id])->orderBy('created_at ASC');
        $providerSuratPeringatan = new ActiveDataProvider([
            'query' => $querySuratPeringatan
        ]);

        $queryTraining = TrainingDetail::find()->andWhere(['pegawai_id' => $model->id])->orderBy('created_at ASC');
        $providerTraining = new ActiveDataProvider([
            'query' => $queryTraining
        ]);


        $queryCutiSaldo = CutiSaldo::find()->andWhere(['pegawai_id' => $model->id])->orderBy('created_at ASC');
        $providerCutiSaldo = new ActiveDataProvider([
            'query' => $queryCutiSaldo
        ]);

        $queryFiles = PegawaiFiles::find()->andWhere(['pegawai_id' => $model->id])->orderBy('created_at ASC');
        $providerFiles = new ActiveDataProvider([
            'query' => $queryFiles
        ]);


        if (Helper::checkRoute('/pegawai/pegawai/delete-assign-to-user')) {
            return $this->render('view-new', [
                'model' => $this->findModel($id),
                'providerSistemKerja' => $providerSistemKerja,
                'providerStruktur' => $providerStruktur,
                'providerPerjanjianKerja' => $providerPerjanjianKerja,
                'providerAkunBeatroute' => $providerAkunBeatroute,
                'providerCostCenter' => $providerCostCenter,
                'providerAreaKerja' => $providerAreaKerja,
                'providerPegawaiGolongan' => $providerPegawaiGolongan,
                'modelProfile' => $modelProfile,
                'providerPendidikan' => $providerPendidikan,
                'providerBpjs' => $providerBpjs,
                'providerKeluarga' => $providerKeluarga,
                'providerKontakDarurat' => $providerKontakDarurat,
                'providerInventaris' => $providerInventaris,
                'providerSim' => $providerSim,
                'providerSuratPeringatan' => $providerSuratPeringatan,
                'providerTraining' => $providerTraining,
                'providerCutiSaldo' => $providerCutiSaldo,
                'providerFiles' => $providerFiles,
            ]);
        } else {
            $modelPegawaiUser = UserPegawai::findOne(['user_id' => Yii::$app->user->id, 'pegawai_id' => $model->id]);
            if ($modelPegawaiUser) {
                return $this->render('view-new', [
                    'model' => $this->findModel($modelPegawaiUser->pegawai_id),
                    'providerSistemKerja' => $providerSistemKerja,
                    'providerStruktur' => $providerStruktur,
                    'providerPerjanjianKerja' => $providerPerjanjianKerja,
                    'providerAkunBeatroute' => $providerAkunBeatroute,
                    'providerCostCenter' => $providerCostCenter,
                    'providerAreaKerja' => $providerAreaKerja,
                    'providerPegawaiGolongan' => $providerPegawaiGolongan,
                    'modelProfile' => $modelProfile,
                    'providerPendidikan' => $providerPendidikan,
                    'providerBpjs' => $providerBpjs,
                    'providerKeluarga' => $providerKeluarga,
                    'providerKontakDarurat' => $providerKontakDarurat,
                    'providerInventaris' => $providerInventaris,
                    'providerSim' => $providerSim,
                    'providerSuratPeringatan' => $providerSuratPeringatan,
                    'providerTraining' => $providerTraining,
                    'providerCutiSaldo' => $providerCutiSaldo,
                    'providerFiles' => $providerFiles,
                ]);
            } else {
                throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
            }
        }

    }

    /**
     * Creates a new Pegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pegawai();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Pegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Pegawai model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Pegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pegawai::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the Pegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return PegawaiSistemKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelSistemKerja($id)
    {
        if (($model = PegawaiSistemKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return PegawaiStruktur|null
     * @throws NotFoundHttpException
     */
    protected function findModelPegawaiStruktur(string $id)
    {
        if (($model = PegawaiStruktur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return PerjanjianKerja|null
     * @throws NotFoundHttpException
     */
    protected function findModelPerjanjianKerja(string $id)
    {
        if (($model = PerjanjianKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return AkunBeatroute|null
     * @throws NotFoundHttpException
     */
    protected function findModelAkunBeatroute(string $id)
    {
        if (($model = AkunBeatroute::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return CostCenter|null
     * @throws NotFoundHttpException
     */
    protected function findModelCostCenter(string $id)
    {
        if (($model = CostCenter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return AreaKerja|null
     * @throws NotFoundHttpException
     */
    protected function findModelAreaKerja(string $id)
    {
        if (($model = AreaKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return PegawaiGolongan|null
     * @throws NotFoundHttpException
     */
    protected function findModelPegawaiGolongan(string $id)
    {
        if (($model = PegawaiGolongan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     *
     * Mencari pegawai bersadar kan string input
     *
     * Hanya pegawai aktif yg akan muncul
     *
     * @param null|string $q nama pegawai
     *
     * @return array id-> berisi id pegawai, text-> berisi nama pegawai
     *
     */
    public function actionGetPegawaiAktifForSelect2(string $q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $out['results'] = Pegawai::cariPegawaiAktifForSelect2($q);
        }
        return $out;
    }

    /**
     * @param string|null $q
     * @param string|null $id
     * @return array
     */
    public function actionGetPegawaiForSelect2(string $q = null, string $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = Pegawai::find();
            $query->select(['pegawai.id', 'concat(pegawai.nama_lengkap,\'-\',pegawai.nama_panggilan) AS text'])
                ->andWhere(['like', 'concat(pegawai.nama_lengkap, pegawai.nama_panggilan)', $q]);
            $query->limit(20);
            $data = $query->asArray()->all();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Pegawai::findOne($id)->nama];
        }
        return $out;
    }

    /**
     * @param string|null $q
     * @param string|null $tanggal
     * @param string|null $ppid
     * @return array
     */
    public function actionGetPegawaiBerdasarTanggalForSelect2(string $q = null, string $tanggal = null, string $ppid = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && !is_null($tanggal)) {
            $tipegaji = 0;
            if (!is_null($ppid)) {
                if ($modelPayrolPeriode = PayrollPeriode::findOne($ppid)) {
                    $tipegaji = $modelPayrolPeriode->tipe;
                    $tanggal = $modelPayrolPeriode->tanggal_awal;
                }
            }
            $out['results'] = Pegawai::cariPegawaiBerdasarkanTanggalForSelect2($q, $tanggal, $tipegaji);
        }
        return $out;
    }

    /**
     * @param string|null $q
     * @param string|null $ppid
     * @return array
     */
    public function actionGetPegawaiBerdasarPpidForSelect2(string $q = null, string $ppid = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && !is_null($ppid)) {
            $out['results'] = Pegawai::cariPegawaiBerdasarkanPpidForSelect2($q, $ppid);
        }
        return $out;
    }

    /**
     * Assign sistem kerja ke pegawai
     *
     * @param string $id
     * @return array|mixed|string
     */
    public function actionCreateSistemKerja(string $id)
    {
        try {
            /** @var Pegawai $modelPegawai */
            $modelPegawai = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        $model = new PegawaiSistemKerja();
        $model->pegawai_id = $modelPegawai->id;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $tanggalAkhir = date('Y-m-d', strtotime($model->mulai_berlaku . ' -1 day'));
                $sistemKerjaLama = $modelPegawai->getPegawaiSistemKerjaUntukTanggal($tanggalAkhir);
                if ($sistemKerjaLama) {
                    $model->nonAkatifkanSistemKerjaLama($sistemKerjaLama->id);
                }
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-sistem-kerja', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param int $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateSistemKerja(int $id)
    {

        $model = $this->findModelSistemKerja($id);
        $modelPegawai = $this->findModel($model->pegawai_id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
//                $model->nonAkatifkanSistemKerjaLama($modelPegawai->pegawaiSistemKerja->id);
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-sistem-kerja', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     *
     * Validasi assign sistem kerja ke pegawai
     *
     * @param string $id
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormSistemKerjaValidation(string $id, bool $isnew = true)
    {
        $model = new PegawaiSistemKerja();
        $model->setIsNewRecord($isnew);
        $model->pegawai_id = $id;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @return string
     */
    public function actionGetTimeTableDepdrop()
    {
        $model = new Pegawai();
        $request = Yii::$app->request;
        if ($request->post('depdrop_parents')) {
            $parents = $request->post('depdrop_parents');
            if ($parents != null) {
                $model->id = $parents[0];
                $out = $model->getTimeTableDepdrop();
                return Json::encode(['output' => $out, 'selected' => '']);
            }
        }
        return Json::encode(['output' => '', 'selected' => '']);
    }


    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateAlokasiKestruktur(string $id)
    {
        $modelPegawai = $this->findModel($id);

        $model = new PegawaiStruktur();
        $model->pegawai_id = $modelPegawai->id;
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-alokasi-struktur', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateAlokasiKestruktur(string $id)
    {
        $model = $this->findModelPegawaiStruktur($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-alokasi-struktur', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormAlokasiKestrukturValidation()
    {
        $model = new PegawaiStruktur();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteAlokasiKestruktur($id)
    {
        $model = $this->findModelPegawaiStruktur($id);
        $model->delete();

        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreatePerjanjianKerja(string $id)
    {
        $modelPegawai = $this->findModel($id);

        $model = new PerjanjianKerja();
        $model->pegawai_id = $modelPegawai->id;
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-perjanjian-kerja', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePerjanjianKerja(string $id)
    {
        $model = $this->findModelPerjanjianKerja($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-perjanjian-kerja', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPerjanjianKerjaValidation()
    {
        $model = new PerjanjianKerja();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeletePerjanjianKerja($id)
    {
        $model = $this->findModelPerjanjianKerja($id);
        $model->delete();

        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    /**
     * @param string $pegawaistrukurid
     * @param string $perjanjiankerjaid
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionAlokasiStrukturKekontrak(string $pegawaistrukurid, string $perjanjiankerjaid)
    {
        $model = $this->findModelPegawaiStruktur($pegawaistrukurid);
        $check = PkPoc::find()->where(['perjanjian_kerja_id' => $perjanjiankerjaid, 'pegawai_struktur_id' => $pegawaistrukurid])->one();
        if (empty($check)) {
            $model->createPkPoc($perjanjiankerjaid);
        }

        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }


    /**
     * @param string $pegawaistrukurid
     * @param string $perjanjiankerjaid
     * @return array|mixed
     * @throws Exception
     * @throws MethodNotAllowedHttpException
     */
    public function actionDisalokasiStrukturDrKontrak(string $pegawaistrukurid, string $perjanjiankerjaid)
    {
        $model = PkPoc::findOne(['perjanjian_kerja_id' => $perjanjiankerjaid, 'pegawai_struktur_id' => $pegawaistrukurid]);
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $query = Yii::$app->db->createCommand('DELETE FROM pk_poc WHERE perjanjian_kerja_id = :pki AND pegawai_struktur_id = :psi', [':pki' => $perjanjiankerjaid, ':psi' => $pegawaistrukurid])->execute();
            if ($model && $query) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawaiStruktur->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil hapus data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal hapus data.'), 'status' => false];
            }
            return $response->data;
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @return array|mixed
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionAssignToUser()
    {
        $model = new UserPegawai();

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            if ($model->load($request->post())) {
                $modelPegawai = $this->findModel($model->pegawai_id);
                if ($modelPegawai) {
                    if ($model->save()) {
                        $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
                    } else {
                        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
                    }
                    return $response->data;
                }
            }
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));

    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionAssignToUserValidation()
    {
        $model = new UserPegawai();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $model->load($request->post());
        if ($request->isAjax && $request->isPost) {
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $pegawaiid
     * @return array|mixed
     * @throws Exception
     * @throws MethodNotAllowedHttpException
     */
    public function actionDeleteAssignToUser(string $pegawaiid)
    {
        $model = new UserPegawai();
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->setAttribute('pegawai_id', $pegawaiid);
            $query = new Query();
            $query->createCommand()
                ->delete('user_pegawai', ['pegawai_id' => $model->pegawai_id])
                ->execute();
            if ($query) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil hapus data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal hapus data.'), 'status' => false];
            }
            return $response->data;
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateAkunBeatroute(string $id)
    {
        $modelPegawai = $this->findModel($id);

        $model = new AkunBeatroute();
        $model->pegawai_id = $modelPegawai->id;
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user->validasiKelengkapanAkun()) {
            return $this->renderAjax('perhatian');
        }

        return $this->renderAjax('_form-akun-beatroute', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateAkunBeatroute(string $id)
    {
        $model = $this->findModelAkunBeatroute($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user->validasiKelengkapanAkun()) {
            return $this->renderAjax('perhatian');
        }

        return $this->renderAjax('_form-akun-beatroute', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionFormAkunBeatrouteValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new AkunBeatroute();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = $this->findModelAkunBeatroute($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteAkunBeatroute(string $id)
    {
        $model = $this->findModelAkunBeatroute($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }


    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateCostCenter(string $id)
    {
        $modelPegawai = $this->findModel($id);

        $model = new CostCenter();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user->validasiKelengkapanAkun()) {
            return $this->renderAjax('perhatian');
        }
        return $this->renderAjax('_form-cost-center', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateCostCenter(string $id)
    {
        $model = $this->findModelCostCenter($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user->validasiKelengkapanAkun()) {
            return $this->renderAjax('perhatian');
        }
        return $this->renderAjax('_form-cost-center', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionFormCostCenterValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new CostCenter();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = $this->findModelCostCenter($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteCostCenter(string $id)
    {
        $model = $this->findModelCostCenter($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }


    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreatePegawaiGolongan(string $id)
    {
        $modelPegawai = $this->findModel($id);

        $model = new PegawaiGolongan();
        $model->pegawai_id = $modelPegawai->id;
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-pegawai-golongan', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePegawaiGolongan(string $id)
    {
        $model = $this->findModelPegawaiGolongan($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-pegawai-golongan', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormPegawaiGolonganValidation()
    {
        $model = new PegawaiGolongan();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeletePegawaiGolongan($id)
    {
        $model = $this->findModelPegawaiGolongan($id);
        $model->delete();

        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }


    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateAreaKerja(string $id)
    {
        $modelPegawai = $this->findModel($id);

        $model = new AreaKerja();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user->validasiKelengkapanAkun()) {
            return $this->renderAjax('perhatian');
        }
        return $this->renderAjax('_form-area-kerja', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateAreaKerja(string $id)
    {
        $model = $this->findModelAreaKerja($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user->validasiKelengkapanAkun()) {
            return $this->renderAjax('perhatian');
        }
        return $this->renderAjax('_form-area-kerja', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionFormAreaKerjaValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new AreaKerja();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = $this->findModelAreaKerja($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteAreaKerja(string $id)
    {
        $model = $this->findModelAreaKerja($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    /**
     * @param string $id
     * @param int $status
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionChangeResignStatus(string $id, int $status)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isPost) {
            if ($model) {
                $model->is_resign = $status;
                if ($model->save(false)) {
                    $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
                } else {
                    $response->data = ['data' => $model->errors, 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
                }
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
        }
        return $response->data;
    }

    /**
     * @param string $id
     * @param int $status
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionChangeDefaultMk(string $id, int $status)
    {
        $model = $this->findModelPerjanjianKerja($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isPost) {
            if ($model) {
                $model->dasar_masa_kerja = $status;
                if ($model->save(false)) {
                    $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
                } else {
                    $response->data = ['data' => $model->errors, 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
                }
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
        }
        return $response->data;
    }


    /**
     * @param string $id
     * @return array
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionSetEssStatus(string $id): array
    {
        $model = new Pegawai();
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->setAttribute('id', $id);
            $model = $this->findModel($model->id);
            $model->ess_status = $model->ess_status ? \common\components\Helper::TIDAK_AKTIF : \common\components\Helper::AKTIF;
            if ($model->save(false)) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
            }
            return $response->data;
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @param int $status
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionChangeDefaultCuti(string $id, int $status)
    {
        $model = $this->findModelPerjanjianKerja($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isPost) {
            if ($model) {
                $model->dasar_cuti = $status;
                if ($model->save(false)) {
                    $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
                } else {
                    $response->data = ['data' => $model->errors, 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
                }
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
        }
        return $response->data;
    }


}

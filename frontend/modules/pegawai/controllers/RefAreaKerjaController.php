<?php

namespace frontend\modules\pegawai\controllers;

use frontend\models\User;
use kartik\widgets\ActiveForm;
use Throwable;
use Yii;
use frontend\modules\pegawai\models\RefAreaKerja;
use frontend\modules\pegawai\models\search\RefAreaKerjaSearch;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * RefAreaKerjaController implements the CRUD actions for RefAreaKerja model.
 */
class RefAreaKerjaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

}

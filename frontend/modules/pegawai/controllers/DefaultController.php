<?php

namespace frontend\modules\pegawai\controllers;

use common\models\PegawaiSistemKerja;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\search\PegawaiSearch;
use kartik\form\ActiveForm;
use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `pegawai` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param string $pegawaiid
     * @param null $q
     * @return array
     * @throws Exception
     */
    public function actionGetPerjanjianKerjaSelect2(string $pegawaiid, $q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select(['id', 'CONCAT(mulai_berlaku, \' - \', akhir_berlaku) AS text'])
                ->from('perjanjian_kerja')
//                ->join('LEFT JOIN', '')
                ->where('concat(`mulai_berlaku`, `akhir_berlaku`) LIKE \'%' . $q . '%\'');
            $query->andWhere(['deleted_by' => 0]);
            $query->andWhere(['pegawai_id' => $pegawaiid]);
            $query->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

    /**
     * Menampilkan daftar pegawai yg habis kontrak
     * @return string
     */
    public function actionHabisKontrak()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->searchHabisKontrak();
        $dataProviderExport = $searchModel->searchHabisKontrak(100000);

        return $this->render('habis-kontrak', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderExport' => $dataProviderExport
        ]);
    }

    /**
     * Menampilkan daftar pegawai yg habis kontrak yang sudah diproses
     * @return string
     */
    public function actionHabisKontrakAfterProses()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->searchHabisKontrakAfterProses();
        $dataProviderExport = $searchModel->searchHabisKontrak(100000);

        return $this->render('habis-kontrak', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderExport' => $dataProviderExport
        ]);
    }

    /**
     * @param string $id
     * @return PerjanjianKerja|null
     * @throws NotFoundHttpException
     */
    protected function findModelPerjanjianKerja(string $id)
    {
        if (($model = PerjanjianKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionAksiHabisKontrak(string $id)
    {
        $model = $this->findModelPerjanjianKerja($id);
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save(false)) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => $model->errors, 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-aksi-habis-kontrak', ['model' => $model]);
    }

}

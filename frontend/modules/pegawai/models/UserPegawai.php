<?php

namespace frontend\modules\pegawai\models;

use Yii;
use \frontend\modules\pegawai\models\base\UserPegawai as BaseUserPegawai;

/**
 * This is the model class for table "user_pegawai".
 */
class UserPegawai extends BaseUserPegawai
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'pegawai_id'], 'required'],
            [['user_id', 'pegawai_id'], 'unique'],
            [['user_id'], 'integer'],
            [['pegawai_id'], 'string', 'max' => 32],
        ];
    }

}

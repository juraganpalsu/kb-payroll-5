<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\AreaKerja as BaseAreaKerja;
use Yii;

/**
 * This is the model class for table "area_kerja".
 */
class AreaKerja extends BaseAreaKerja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mulai_berlaku', 'ref_area_kerja_id', 'pegawai_id'], 'required'],
            [['mulai_berlaku', 'akhir_berlaku', 'mulai_libur', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['ref_area_kerja_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['mulai_berlaku'], 'mulaiBerlakuValidation'],
            [['akhir_berlaku'], 'akhirBerlakuValidation']
        ];
    }


    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->mulai_berlaku) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->mulai_berlaku]));
        }
    }

    /**
     * @param string $attribute
     */
    public function mulaiBerlakuValidation(string $attribute)
    {
        /** @var self $query */
        $query = self::find()->andWhere(['>=', 'akhir_berlaku', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id])->all();
        if ($query) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
        }
    }

    /**
     * @return string
     */
    public function areaGedung(): string
    {
        $gedungString = '';
        $gedung = $this->refAreaKerja->parents(1)->one();
        if ($gedung) {
            $gedungString = $gedung->name;
        }
        return $gedungString;
    }

    /**
     * @return string
     */
    public function areaWilayah(): string
    {
        $wilayahString = '';
        $gedung = $this->refAreaKerja->parents(1)->one();
        if ($gedung) {
            $wilayah = $this->refAreaKerja->parents(2)->one();
            if ($wilayah) {
                $wilayahString = $wilayah->name;
            }
        }
        return $wilayahString;
    }

    /**
     * @return string
     */
    public function nama(): string
    {
        $nama_area = '';
        if ($this->refAreaKerja) {
            $nama_area = $this->refAreaKerja->name;
        }
        return $nama_area;
    }

}

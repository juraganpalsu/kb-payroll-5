<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiNpwp as BasePegawaiNpwp;

/**
 * This is the model class for table "pegawai_npwp".
 */
class PegawaiNpwp extends BasePegawaiNpwp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor', 'pegawai_id'], 'required'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nomor'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

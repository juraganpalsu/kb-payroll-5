<?php

namespace frontend\modules\pegawai\models;

use common\components\Status;
use common\models\Pegawai as BasePegawai;
use DateTime;
use Exception;
use frontend\models\User;
use frontend\modules\payroll\models\PayrollPeriode;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "pegawai".
 * @property mixed sistemKerja_
 *
 * @property UserPegawai $userPegawai
 * @property User $user
 *
 *
 * @property array $_jenis_kelamin
 * @property array $_status_pernikahan
 * @property array $_agama
 * @property array $_gol_darah
 *
 * @property string $jenisKelamin
 * @property string $statusPernikahan
 * @property string $agama_
 *
 *
 * @property string $upload_dir
 * @property string $nama_file
 *
 * @property string $namaIdPegawai
 */
class Pegawai extends BasePegawai
{

    const LAKI_LAKI = 1;
    const PEREMPUAN = 0;

    //Status pernikahan
    const BELUM_MENIKAH = 1;
    const MENIKAH_ANAK_0 = 2;
    const MENIKAH_ANAK_1 = 3;
    const MENIKAH_ANAK_2 = 4;
    const MENIKAH_ANAK_3 = 5;
    const MENIKAH_ANAK_3P = 6;
    const CERAI_HIDUP = 7;
    const CERAI_MATI = 8;

    //Agama
    const ISLAM = 1;
    const KRISTEN = 2;
    const KATOLIK = 3;
    const HINDU = 4;
    const BUDHA = 5;
    const KONG_HU_CU = 6;
    const LAINNYA = 7;

    //Golongan Darah
    const STRIP = 0;
    const O_MIN = 1;
    const O_PLUS = 2;
    const A_MIN = 3;
    const A_PLUS = 4;
    const B_MIN = 5;
    const B_PLUS = 6;
    const AB_MIN = 7;
    const AB_PLUS = 8;

    public $_jenis_kelamin = [self::LAKI_LAKI => 'LAKI-LAKI', self::PEREMPUAN => 'PEREMPUAN'];
    public $_status_pernikahan = [
        self::BELUM_MENIKAH => 'BELUM MENIKAH',
        self::MENIKAH_ANAK_0 => 'MENIKAH',
        self::MENIKAH_ANAK_1 => 'MENIKAH ANAK 1',
        self::MENIKAH_ANAK_2 => 'MENIKAH ANAK 2',
        self::MENIKAH_ANAK_3 => 'MENIKAH ANAK 3',
        self::MENIKAH_ANAK_3P => 'MENIKAH ANAK 4',
        self::CERAI_HIDUP => 'DUDA',
        self::CERAI_MATI => 'JANDA'
    ];
    public $_agama = [
        self::ISLAM => 'ISLAM',
        self::KRISTEN => 'KRISTEN',
        self::KATOLIK => 'KATOLIK',
        self::HINDU => 'HINDU',
        self::BUDHA => 'BUDHA',
        self::KONG_HU_CU => 'KONG HU CU',
        self::LAINNYA => 'LAINNYA'
    ];

    //Golongan Darah
    public $_gol_darah = [
        self::STRIP => '-',
        self::O_MIN => 'O-',
        self::O_PLUS => 'O+',
        self::A_MIN => 'A-',
        self::A_PLUS => 'A+',
        self::B_MIN => 'B-',
        self::B_PLUS => 'B+',
        self::AB_MIN => 'AB-',
        self::AB_PLUS => 'AB+'
    ];

    public $upload_dir = 'uploads/foto-profil/';
    public $nama_file;


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        parent::beforeValidate();
        $this->nama_lengkap = trim($this->nama_lengkap);
        $this->nama_panggilan = trim($this->nama_panggilan);
        $this->tempat_lahir = trim($this->tempat_lahir);
        $this->kewarganegaraan = trim($this->kewarganegaraan);
        $this->nama_ibu_kandung = trim($this->nama_ibu_kandung);
        return true;
    }


    /**
     * Mengambil perjanjian kerja untuk kerperluan select2
     * Ini hanya perjanjian kerja untuk perpegawai
     *
     */
    public function getPerjanjianKerjaSelect2()
    {
        $datas = [];
        foreach ($this->perjanjianKerjas as $perjanjianKerja) {
            $datas[$perjanjianKerja->id] = $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak] . ' - ' . $perjanjianKerja->jenis->nama;
        }
        return $datas;
    }


    /**
     * @return bool|User
     */
    public function getUser()
    {
        if ($userPegawai = $this->userPegawai) {
            if ($userPegawai->user) {
                return $userPegawai->user;
            }
        }
        return false;
    }


    /**
     * @param int $hubungan
     * @return PegawaiKeluarga|null
     */
    public function getPegawaiKeluargaBerdasarkanHubungan(int $hubungan)
    {
        return PegawaiKeluarga::findOne(['pegawai_id' => $this->id, 'hubungan' => $hubungan]);
    }

    /**
     * @param int $jenis
     * @return PegawaiBpjs|null
     */
    public function getPegawaiBpjsBerdasarkanJenis(int $jenis)
    {
        return PegawaiBpjs::findOne(['pegawai_id' => $this->id, 'pegawai_keluarga_id' => null, 'jenis' => $jenis]);
    }


    /**
     *
     * @return string jenis kelamin
     */
    public function getJenisKelamin()
    {
        return $this->_jenis_kelamin[$this->jenis_kelamin];
    }

    /**
     *
     * @return string jenis kelamin
     */
    public function getGolonganDarah()
    {
        return $this->_gol_darah[$this->gol_darah];
    }

    /**
     *
     * @return string Status Pernikahan
     */
    public function getStatusPernikahan()
    {
        return $this->status_pernikahan ? $this->_status_pernikahan[$this->status_pernikahan] : '-';
    }

    /**
     *
     * @return string Agama
     */
    public function getAgama_()
    {
        return $this->agama ? $this->_agama[$this->agama] : '-';
    }

    /**
     * @param string $tanggalLahir
     * @return int
     * @throws Exception
     */
    public function getUsia(string $tanggalLahir = '')
    {
        if (empty($tanggalLahir)) {
            $tanggalLahir = $this->tanggal_lahir;
        }
        $from = new DateTime($tanggalLahir);
        $to = new DateTime('today');
        return $from->diff($to)->y;
    }

    /**
     * @param $rangeUsia1
     * @param $ranNomor surat otomatis per 2021geUsia2
     * @return int
     */
    public function queryAgeRange($rangeUsia1, $rangeUsia2)
    {
        return (int)BasePegawai::find()->andWhere(['is_resign' => Status::TIDAK])->andWhere(['BETWEEN', 'timestampdiff( year, tanggal_lahir, curdate())', $rangeUsia1, $rangeUsia2])->count();
    }

    /**
     * @return array
     */
    public function formatingDashboardRangeAge()
    {
        $nameRangeAge1 = 'Usia < 18';
        $nameRangeAge2 = 'Usia 18-25';
        $nameRangeAge3 = 'Usia 26-35';
        $nameRangeAge4 = 'Usia 36-45';
        $nameRangeAge5 = 'Usia > 45';
        $categories = [$nameRangeAge1, $nameRangeAge2, $nameRangeAge3, $nameRangeAge4, $nameRangeAge5];
        $series = [
            [
                'name' => $nameRangeAge1,
                'data' => [
                    $this->queryAgeRange(0, 17)
                ],
            ],
            [
                'name' => $nameRangeAge2,
                'data' => [
                    $this->queryAgeRange(18, 25)
                ],
            ],
            [
                'name' => $nameRangeAge3,
                'data' => [
                    $this->queryAgeRange(26, 35)
                ],
            ],
            [
                'name' => $nameRangeAge4,
                'data' => [
                    $this->queryAgeRange(36, 45)
                ],
            ],
            [
                'name' => $nameRangeAge5,
                'data' => [
                    $this->queryAgeRange(46, 65)
                ],
            ],
        ];
        return ['categories' => $categories, 'series' => $series];

    }


    /**
     * @return bool
     */
    public function uploadFile()
    {
        $dir = $this->upload_dir;
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        /** @var UploadedFile $file */
        $namaFile = UploadedFile::getInstance($this, 'nama_file');
        if ($namaFile->saveAs($dir . $this->id . '.jpg')) {
            return true;
        }
        return false;
    }

    /**
     * @param string $q
     * @return array|ActiveRecord[]
     */
    public static function cariPegawaiAktifForSelect2(string $q)
    {
        $query = Pegawai::find();
        $query->select(['pegawai.id', 'concat(pegawai.nama_lengkap,\'-\',perjanjian_kerja.id_pegawai) AS text'])
            ->joinWith('perjanjianKerja')
            ->andWhere(['like', 'concat(pegawai.nama_lengkap, perjanjian_kerja.id_pegawai)', $q]);
        $query->limit(20);
        $data = $query->asArray()->all();
        return array_values($data);
    }

    /**
     * @param string $q
     * @param string $tanggal
     * @param int $tipeGaji
     * @return array
     */
    public static function cariPegawaiBerdasarkanTanggalForSelect2(string $q, string $tanggal, int $tipeGaji = 0)
    {
        $query = PerjanjianKerja::find()
            ->select(['pegawai.id', 'perjanjian_kerja.pegawai_id', 'concat(pegawai.nama_lengkap,\'-\',perjanjian_kerja.id_pegawai) AS text']);
        if ($tipeGaji > 0) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) use ($tipeGaji) {
                $query->joinWith(['masterDataDefaults' => function (ActiveQuery $query) use ($tipeGaji) {
                    $query->andWhere(['tipe_gaji_pokok' => $tipeGaji]);
                }]);
            }]);
        } else {
            $query->joinWith('pegawai');
        }
        $query->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarTanggal($tanggal))
            ->andWhere(['like', 'concat(pegawai.nama_lengkap, perjanjian_kerja.id_pegawai)', $q]);
        $query->limit(20);
        $data = $query->asArray()->all();
        return array_values($data);
    }

    /**
     * @param string $q
     * @param string $ppid
     * @return array|ActiveRecord[]
     */
    public static function cariPegawaiBerdasarkanPpidForSelect2(string $q, string $ppid)
    {
        $modelPayrollPeriode = PayrollPeriode::findOne($ppid);
        $data = [];
        if ($modelPayrollPeriode) {
            $query = PerjanjianKerja::find()
                ->select(['pegawai.id', 'perjanjian_kerja.pegawai_id', 'concat(pegawai.nama_lengkap,\'-\',perjanjian_kerja.id_pegawai) AS text']);
            $query->joinWith(['pegawai' => function (ActiveQuery $query) use ($modelPayrollPeriode) {
                $query->joinWith(['masterDataDefaults' => function (ActiveQuery $query) use ($modelPayrollPeriode) {
                    $query->andWhere(['tipe_gaji_pokok' => $modelPayrollPeriode->tipe]);
                }]);
            }]);
            $query->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarRangeTanggal($modelPayrollPeriode->tanggal_awal, $modelPayrollPeriode->tanggal_akhir));
            $query->andWhere(['like', 'concat(pegawai.nama_lengkap, perjanjian_kerja.id_pegawai)', $q]);
            $query->limit(20);
            $data = $query->asArray()->all();
        }
        return array_values($data);
    }

    /**
     * @return string
     */
    public function getNamaIdPegawai()
    {
        if ($this->perjanjianKerja) {
            return $this->nama_lengkap . ' - ' . $this->perjanjianKerja->id_pegawai;
        }
        return $this->nama_lengkap;
    }

    /**
     * @return false|string
     */
    public function getTanggalAwalMasuk()
    {
        $awalMasuk = '';
        $queryPerjanjianKerja = PerjanjianKerja::find()->andWhere(['pegawai_id' => $this->id])->orderBy('mulai_berlaku ASC')->limit(1)->one();
        if ($queryPerjanjianKerja) {
            $awalMasuk = date('d-m-Y', strtotime($queryPerjanjianKerja['mulai_berlaku']));
        }
        return $awalMasuk;
    }

    /**
     * @return array|ActiveRecord|null
     */
    public function getPendidikanTerakhir()
    {
        return PegawaiPendidikan::find()->andWhere(['pegawai_id' => $this->id])->orderBy('jenjang DESC')->one();
    }

    /**
     * @param int $jenis
     * @return PegawaiBpjs|null
     */
    public function getBpjs(int $jenis)
    {
        return PegawaiBpjs::findOne(['pegawai_id' => $this->id, 'pegawai_keluarga_id' => null, 'jenis' => $jenis]);
    }

    /**
     *
     * Mencari Pegawia Aktif berdasarkan range tanggal, golongan & bisnis unit
     *
     * @param int $golonganId
     * @param int $bisnisUnit
     * @param string $tanggal_awal
     * @param string $tanggal_akhir
     * @return array|ActiveRecord[]
     */
    public function getPegawaiByGolonganDanBisnisUnitByRange(int $golonganId, int $bisnisUnit, string $tanggal_awal, string $tanggal_akhir)
    {
        return Pegawai::find()
            ->joinWith(['pegawaiGolongans' => function (ActiveQuery $query) use ($golonganId, $tanggal_awal, $tanggal_akhir) {
                $query->andWhere(['golongan_id' => $golonganId]);
                $query->andWhere('\'' . $tanggal_awal . '\' BETWEEN pegawai_golongan.mulai_berlaku AND pegawai_golongan.akhir_berlaku OR \'' . $tanggal_akhir . '\' BETWEEN pegawai_golongan.mulai_berlaku AND pegawai_golongan.akhir_berlaku OR pegawai_golongan.mulai_berlaku BETWEEN \'' . $tanggal_awal . '\' AND \'' . $tanggal_akhir . '\' OR pegawai_golongan.akhir_berlaku BETWEEN \'' . $tanggal_awal . '\' AND \'' . $tanggal_akhir . '\' OR ( \'' . $tanggal_awal . '\' <= pegawai_golongan.mulai_berlaku AND \'' . $tanggal_akhir . '\' >= pegawai_golongan.akhir_berlaku) OR ( \'' . $tanggal_awal . '\' >= pegawai_golongan.mulai_berlaku AND \'' . $tanggal_akhir . '\' <= pegawai_golongan.akhir_berlaku) OR (pegawai_golongan.mulai_berlaku <=\'' . $tanggal_akhir . '\' AND pegawai_golongan.akhir_berlaku is null)');
            }])
            ->joinWith(['perjanjianKerjas' => function (ActiveQuery $query) use ($bisnisUnit, $tanggal_awal, $tanggal_akhir) {
                $query->andWhere(['kontrak' => $bisnisUnit]);
                $query->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarRangeTanggal($tanggal_awal, $tanggal_akhir));

            }])
            ->all();
    }

    /**
     * @return PerjanjianKerja
     */
    public function getPerjanjianKerjaTerakhir(): PerjanjianKerja
    {
        $query = PerjanjianKerja::find()->select('id')->andWhere(['pegawai_id' => $this->id])->orderBy('mulai_berlaku DESC')->asArray()->one();
        $id = 0;
        if ($query) {
            $id = $query['id'];
        }
        return PerjanjianKerja::findOne($id);
    }

    /**
     * @return PegawaiGolongan
     */
    public function getGolonganTerakhir(): PegawaiGolongan
    {
        $query = PegawaiGolongan::find()->select('id')->andWhere(['pegawai_id' => $this->id])->orderBy('mulai_berlaku DESC')->asArray()->one();
        $id = 0;
        if ($query) {
            $id = $query['id'];
        }
        return PegawaiGolongan::findOne($id);
    }

    /**
     * @return PegawaiStruktur
     */
    public function getStrukturTerakhir(): PegawaiStruktur
    {
        $query = PegawaiStruktur::find()->select('id')->andWhere(['pegawai_id' => $this->id])->orderBy('mulai_berlaku DESC')->asArray()->one();
        $id = 0;
        if ($query) {
            $id = $query['id'];
        }
        return PegawaiStruktur::findOne($id);
    }

    /**
     * @return AreaKerja
     */
    public function getAreaKerjaTerakhir(): AreaKerja
    {
        $query = AreaKerja::find()->select('id')->andWhere(['pegawai_id' => $this->id])->orderBy('mulai_berlaku DESC')->asArray()->one();
        $id = 0;
        if ($query) {
            $id = $query['id'];
        }
        return AreaKerja::findOne($id);
    }


    /**
     *
     * Mengitung jumlah karyawan yg perjanjian kerjanya aktif dan strukturnya aktif
     * Data dikumpulkan berdasarkan departemen
     *
     * @return array
     */
    public static function getDataForDashboardPerDepartement(): array
    {
        $queryPegawai = Pegawai::find()
            ->joinWith('perjanjianKerja')
            ->joinWith('pegawaiStruktur')
            ->all();

        $data = [];
        /** @var Pegawai $pegawai */
        foreach ($queryPegawai as $pegawai) {
            $struktur = $pegawai->pegawaiStruktur->struktur;
            $data[$struktur->departemen_id]['name'] = $struktur->departemen->nama;
            $data[$struktur->departemen_id]['y'][] = $pegawai->id;
        }

        return $data;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function birthdayPegawai()
    {
        $query = Pegawai::find();
        $query->orderBy('pegawai.nama_lengkap ASC');
        $query->andWhere(['is_resign' => Status::TIDAK]);
        $dataPegawais = $query->all();

        $dataPegawaiBirthday = [];

        /** @var Pegawai $dataPegawai */
        foreach ($dataPegawais as $dataPegawai) {
            if ($dataPegawai->getUsia() < 17) {
                continue;
            }

            $today = new DateTime(date('Y-m-d'));
            $birthday = new DateTime($dataPegawai->tanggal_lahir);

            $struktur = $dataPegawai->pegawaiStruktur;
            if ($birthday->format('m-d') == $today->format('m-d')) {
                if (!$struktur) {
                    continue;
                }
                if ($struktur) {
                    $departemen = '';
                    if ($objStruktur = $struktur->struktur) {
                        if ($objDepartemen = $objStruktur->departemen) {
                            $departemen = $objDepartemen->nama;
                        }
                    }
                }
                $dataPegawaiBirthday[] = [
                    'id' => $dataPegawai->id,
                    'nama' => $dataPegawai->nama_lengkap,
                    'departemen' => $departemen,
                ];
            }
        }
        return $dataPegawaiBirthday;
    }


    /**
     * @return array
     */
    public function getGudang()
    {
        $gudangIds = [];
        foreach ($this->eGudangPegawais as $eGudangPegawai) {
            $gudangIds[] = $eGudangPegawai->e_gudang_id;
        }
        return $gudangIds;
    }


    /**
     *
     * Menghitung jumlah hubungan dalam keluarga
     *
     * contoh mencari jumlah anak(bukan untuk mencari jumlah istri ^__^ )
     *
     * ini versi function untuk lebih general,,,
     *
     * @param int $hubungan
     * @return int
     */
    public function getJumlahPerhubungan(int $hubungan): int
    {
        return (int)PegawaiKeluarga::find()->andWhere(['pegawai_id' => $this->id, 'hubungan' => $hubungan])->count();
    }


    /**
     * @return string
     */
    public function getStatusPtkp(): string
    {
        $ptkp = PegawaiPtkp::TK;
        if ($this->jenis_kelamin == self::LAKI_LAKI && $this->pegawaiKeluarga && $this->status_pernikahan != self::BELUM_MENIKAH) {
            $jumlahAnak = $this->getJumlahAnak();
            switch ($jumlahAnak) {
                case 1:
                    $ptkp = PegawaiPtkp::K1;
                    break;
                case 2:
                    $ptkp = PegawaiPtkp::K2;
                    break;
                case 3:
                    $ptkp = PegawaiPtkp::K3;
                    break;
                default:
                    $ptkp = PegawaiPtkp::K0;
            }
        }
        return $ptkp;
    }
}
<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiStruktur as BasePegawaiStruktur;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pegawai_struktur".
 *
 *
 * @property integer $helper_struktur_id
 * @property array $statuses
 *
 */
class PegawaiStruktur extends BasePegawaiStruktur
{
    const TIDAK_AKTIF = 0;
    const AKTIF = 1;

    public $statuses = [self::AKTIF => 'AKTIF', self::TIDAK_AKTIF => 'TIDAK AKTIF'];

    public $helper_struktur_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'struktur_id', 'mulai_berlaku'], 'required'],
            [['aksi', 'struktur_id', 'status', 'created_by', 'created_by_org_chart', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['aksi', 'helper_struktur_id', 'mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id', 'pegawai_id'], 'string', 'max' => 32],
            [['aksi', 'lock', 'status'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['mulai_berlaku'], 'mulaiBerlakuValidation'],
            [['akhir_berlaku'], 'akhirBerlakuValidation']
        ];
    }


    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->mulai_berlaku) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->mulai_berlaku]));
        }
    }

    /**
     * @param string $attribute
     */
    public function mulaiBerlakuValidation(string $attribute)
    {
        /** @var self $query */
        $query = self::find()->andWhere(['>=', 'akhir_berlaku', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id])->all();
        if ($query) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['helper_struktur_id'] = 'Struktur ID';
        return $labels;
    }

    /**
     *
     * Membuat table junction untuk ke perjanjian kerja
     *
     * @param string $perjanjiankerjaid
     */
    public function createPkPoc(string $perjanjiankerjaid)
    {
        $model = new PkPoc();
        $model->pegawai_struktur_id = $this->id;
        $model->perjanjian_kerja_id = $perjanjiankerjaid;
        $model->save();
    }


    /**
     * @param string $tanggal
     * @return array|ActiveRecord|null
     */
    public function strukturBerdasarTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        return PegawaiStruktur::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere('(mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku is null )')
            ->one();
    }

    /**
     * @param string $tanggal
     * @return array|ActiveRecord|null
     */
    public function pegawaiBerdasarTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        return PegawaiStruktur::find()->andWhere(['struktur_id' => $this->struktur_id])
            ->andWhere('(mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku is null )')
            ->all();
    }

    /**
     * HARUS ADA ID PEMBANDINGNYA -> di pakai di grid
     * @param string $tanggal
     * @return bool
     */
    public function isAktif(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        /** @var PegawaiStruktur $strukturBerdasarTanggal */
        if ($strukturBerdasarTanggal = $this->strukturBerdasarTanggal($tanggal)) {
            if ($strukturBerdasarTanggal->id == $this->id) {
                return true;
            }
        }
        return false;
    }


}

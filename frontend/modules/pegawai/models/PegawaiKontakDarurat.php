<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiKontakDarurat as BasePegawaiKontakDarurat;
use Yii;

/**
 * This is the model class for table "pegawai_kontak_darurat".
 *
 * @property string $hubungan_
 */
class PegawaiKontakDarurat extends BasePegawaiKontakDarurat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hubungan', 'nama', 'pekerjaan', 'no_handphone', 'pegawai_id'], 'required'],
            [['no_handphone', 'hubungan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['alamat', 'catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['pekerjaan'], 'string', 'max' => 45],
            [['no_handphone'], 'string', 'min' => 10, 'max' => 13],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['no_handphone'], 'validasiNoHandphone']
        ];
    }

    /**
     * @param string $attribute
     */
    public function validasiNoHandphone(string $attribute)
    {
        $queryPegawaiKeluarga = PegawaiKeluarga::find()->andWhere(['no_handphone' => $this->$attribute, 'pegawai_id' => $this->pegawai_id])->all();
        $queryPegawaiKontakDarurat = PegawaiKontakDarurat::find()->andWhere(['no_handphone' => $this->$attribute, 'pegawai_id' => $this->pegawai_id])->all();
        if ($queryPegawaiKeluarga || $queryPegawaiKontakDarurat) {
            if (empty($this->id)) {
                $this->addError($attribute, Yii::t('frontend', 'Nomor {nomor} telah digunakan, mohon gunakan nomor yg lain.', ['nomor' => $this->$attribute]));
            } else {
                $a = [];
                $b = [];
                /** @var PegawaiKeluarga $pegawaiKeluarga */
                foreach ($queryPegawaiKeluarga as $pegawaiKeluarga) {
                    $a[] = $pegawaiKeluarga->id;
                }
                /** @var PegawaiKontakDarurat $kontakDarurat */
                foreach ($queryPegawaiKontakDarurat as $kontakDarurat) {
                    $b[] = $kontakDarurat->id;
                }
                $c = array_merge($a, $b);
                $d = array_diff($c, [$this->id]);
                if (!empty($d)) {
                    $this->addError($attribute, Yii::t('frontend', 'Nomor {nomor} telah digunakan, mohon gunakan nomor yg lain.', ['nomor' => $this->$attribute]));
                }
            }
        }

    }

    /**
     * @return PegawaiKeluarga
     */
    public static function modelPegawaiKeluarga()
    {
        return new PegawaiKeluarga();
    }


    /**
     *
     * @return string Hubungan
     */
    public function getHubungan_()
    {
        return self::modelPegawaiKeluarga()->_hubungan[$this->hubungan];
    }
}

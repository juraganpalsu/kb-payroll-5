<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiBpjs as BasePegawaiBpjs;

/**
 * This is the model class for table "pegawai_bpjs".
 *
 * @property array $_jenis
 * @property string $jenis_
 */
class PegawaiBpjs extends BasePegawaiBpjs
{
    const BPJS_KETENAGAKERJAAN = 1;
    const BPJS_KESEHATAN = 2;

    public $_jenis = [self::BPJS_KETENAGAKERJAAN => 'KETENAGAKERJAAN', self::BPJS_KESEHATAN => 'KESEHATAN'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis', 'nomor', 'tanggal_kepesertaan', 'pegawai_id'], 'required'],
            [['jenis', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_kepesertaan', 'tanggal_berakhir', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['id', 'pegawai_id', 'pegawai_keluarga_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nomor'], 'string', 'max' => 15],
            [['upah'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return mixed
     */
    public function getJenis_()
    {
        return $this->_jenis[$this->jenis];
    }

}

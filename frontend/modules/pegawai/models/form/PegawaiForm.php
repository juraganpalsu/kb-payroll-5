<?php

namespace frontend\modules\pegawai\models\form;

/**
 * Created by PhpStorm.
 * File Name: PegawaiForm.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 20/10/19
 * Time: 15.55
 */

use Exception;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiPendidikan;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Class PegawaiForm
 *
 * @property integer $sistem_kerja_id
 * @property integer $busines_unit
 * @property integer $jenis_kontrak
 * @property integer $golongan_id
 */
class PegawaiForm extends Model
{
    /**
     * @var integer
     * e.g , Factory 6HK,Factory 5HK
     */
    public $sistem_kerja_id;

    /**
     * @var integer
     * Pada table perjanjian kerja -> kontrak
     * e.g KBU, KBU_BCA ...
     */
    public $busines_unit;

    /**
     * @var integer
     * Pada table perjanjian kerja -> jenis_perjanjian
     * e.g ADENDUM, ADENDUM 1 ...
     */
    public $jenis_kontrak;

    /**
     * @var integer
     * e.g SATU, DUA ,,,
     */
    public $golongan_id;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['golongan_id'], 'required'],
            [['busines_unit', 'jenis_kontrak', 'golongan_id'], 'default', 'value' => 0],
            [['busines_unit', 'jenis_kontrak', 'golongan_id'], 'integer'],
            [['sistem_kerja_id', 'busines_unit', 'jenis_kontrak', 'golongan_id'], 'safe']
        ];
    }

    /**
     * @return PerjanjianKerja
     */
    public static function perjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @return bool
     */
    public function export()
    {
        $query = Pegawai::find();


        if (!empty($this->golongan_id)) {
            $query->joinWith(['pegawaiGolongan' => function (ActiveQuery $x) {
                return $x->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);
            }]);
        }
        if (!empty($this->busines_unit)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $x) {
                return $x->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
            }]);
        }
        if (!empty($this->jenis_kontrak)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $x) {
                return $x->andWhere(['perjanjian_kerja.jenis_perjanjian' => $this->jenis_kontrak]);
            }]);
        }
        if (!empty($this->sistem_kerja_id)) {
            $query->joinWith(['pegawaiSistemKerja' => function (ActiveQuery $x) {
                return $x->andWhere(['pegawai_sistem_kerja.sistem_kerja_id' => $this->sistem_kerja_id]);
            }]);
        }

        $query->orderBy('pegawai.nama_lengkap ASC');

        $dataPegawais = $query->all();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(10);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(30);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(15);


            $activeSheet->setCellValue('A1', Yii::t('frontend', 'Id Pegawai'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Nama'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Golongan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Busines Unit'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Sistem Kerja '))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Jenis Kelamin '))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Umur'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Status Pernikahan'))->getStyle('I1')->applyFromArray($headerStyle);

            $row = 2;

            /** @var Pegawai $dataPegawai */
            foreach ($dataPegawais as $dataPegawai) {
                if (!$dataPegawai->perjanjianKerja || !$dataPegawai->pegawaiSistemKerja) {
                    continue;
                }
                $activeSheet->setCellValue('A' . $row, $dataPegawai->perjanjianKerja->id_pegawai)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai->nama_lengkap)->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai->pegawaiGolongan->golongan->nama)->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai->perjanjianKerja->namaKontrak)->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai->perjanjianKerja->jenis->nama)->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai->pegawaiSistemKerja->sistemKerja->nama)->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai->jenisKelamin)->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai->getUsia())->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai->statusPernikahan)->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));


                $row++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'I' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'I' . $row)->applyFromArray($font);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=datapegawai' . date('YmdHis') . '.xls');
            header('Cache-Control: max-age=0');

            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;

    }


    /**
     * @param int $resign
     * @return bool
     */
    public function exportAll(int $resign)
    {
        $query = Pegawai::find();

        $query->orderBy('pegawai.nama_lengkap ASC');

        $query->andWhere(['is_resign' => $resign]);

        $query->joinWith('perjanjianKerja');
        $query->andWhere(['IN', 'perjanjian_kerja.kontrak', [PerjanjianKerja::KBU, PerjanjianKerja::ADA, PerjanjianKerja::GCI, PerjanjianKerja::OSI, PerjanjianKerja::GSM]]);

        $dataPegawais = $query->all();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(3);
            $activeSheet->getColumnDimension('B')->setWidth(17);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(10);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(20);
            $activeSheet->getColumnDimension('I')->setWidth(8);
            $activeSheet->getColumnDimension('J')->setWidth(8);
            $activeSheet->getColumnDimension('K')->setWidth(10);
            $activeSheet->getColumnDimension('L')->setWidth(15);
            $activeSheet->getColumnDimension('M')->setWidth(10);
            $activeSheet->getColumnDimension('N')->setWidth(5);
            $activeSheet->getColumnDimension('O')->setWidth(12);
            $activeSheet->getColumnDimension('P')->setWidth(15);
            $activeSheet->getColumnDimension('Q')->setWidth(7);
            $activeSheet->getColumnDimension('R')->setWidth(12);
            $activeSheet->getColumnDimension('S')->setWidth(15);
            $activeSheet->getColumnDimension('T')->setWidth(15);
            $activeSheet->getColumnDimension('U')->setWidth(15);
            $activeSheet->getColumnDimension('V')->setWidth(15);
            $activeSheet->getColumnDimension('W')->setWidth(7);
            $activeSheet->getColumnDimension('X')->setWidth(15);
            $activeSheet->getColumnDimension('Y')->setWidth(15);
            $activeSheet->getColumnDimension('Z')->setWidth(15);
            $activeSheet->getColumnDimension('AA')->setWidth(15);
            $activeSheet->getColumnDimension('AB')->setWidth(15);
            $activeSheet->getColumnDimension('AC')->setWidth(7);
            $activeSheet->getColumnDimension('AD')->setWidth(15);
            $activeSheet->getColumnDimension('AE')->setWidth(8);
            $activeSheet->getColumnDimension('AF')->setWidth(10);
            $activeSheet->getColumnDimension('AG')->setWidth(10);
            $activeSheet->getColumnDimension('AH')->setWidth(10);
            $activeSheet->getColumnDimension('AI')->setWidth(10);
            $activeSheet->getColumnDimension('AJ')->setWidth(10);
            $activeSheet->getColumnDimension('AK')->setWidth(15);
            $activeSheet->getColumnDimension('AL')->setWidth(12);
            $activeSheet->getColumnDimension('AM')->setWidth(15);
            $activeSheet->getColumnDimension('AN')->setWidth(12);
            $activeSheet->getColumnDimension('AO')->setWidth(15);
            $activeSheet->getColumnDimension('AP')->setWidth(10);
            $activeSheet->getColumnDimension('AQ')->setWidth(10);
            $activeSheet->getColumnDimension('AR')->setWidth(5);
            $activeSheet->getColumnDimension('AS')->setWidth(5);
            $activeSheet->getColumnDimension('AT')->setWidth(10);
            $activeSheet->getColumnDimension('AU')->setWidth(10);

            $activeSheet->mergeCells('A1:A2')->setCellValue('A1', Yii::t('frontend', 'No'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('B1:B2')->setCellValue('B1', Yii::t('frontend', 'Departemen'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('C1:C2')->setCellValue('C1', Yii::t('frontend', 'Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('D1:D2')->setCellValue('D1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('E1:E2')->setCellValue('E1', Yii::t('frontend', 'Nama Struktur'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('F1:F2')->setCellValue('F1', Yii::t('frontend', 'Id Pegawai'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('G1:G2')->setCellValue('G1', Yii::t('frontend', 'Nomor KTP'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('H1:H2')->setCellValue('H1', Yii::t('frontend', 'Nama'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('I1:I2')->setCellValue('I1', Yii::t('frontend', 'Golongan'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('J1:J2')->setCellValue('J1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('K1:K2')->setCellValue('K1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('L1:L2')->setCellValue('L1', Yii::t('frontend', 'Tempat Lahir'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('M1:M2')->setCellValue('M1', Yii::t('frontend', 'Tgl Lahir'))->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('N1:N2')->setCellValue('N1', Yii::t('frontend', 'Umur '))->getStyle('N1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('O1:O2')->setCellValue('O1', Yii::t('frontend', 'Jenis Kelamin '))->getStyle('O1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('P1:P2')->setCellValue('P1', Yii::t('frontend', 'Status Pernikahan'))->getStyle('P1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('Q1:Q2')->setCellValue('Q1', Yii::t('frontend', 'Agama'))->getStyle('Q1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('R1:R2')->setCellValue('R1', Yii::t('frontend', 'No Handphone'))->getStyle('R1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('S1:X1')->setCellValue('S1', Yii::t('frontend', 'Alamat KTP'))->getStyle('S1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('S2', Yii::t('frontend', 'Provinsi'))->getStyle('S2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('T2', Yii::t('frontend', 'Kabupaten/Kota'))->getStyle('T2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('U2', Yii::t('frontend', 'Kecamatan'))->getStyle('U2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('V2', Yii::t('frontend', 'Desa/Kelurahan'))->getStyle('V2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('W2', Yii::t('frontend', 'Rt/Rw'))->getStyle('W2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('X2', Yii::t('frontend', 'Alamat'))->getStyle('X2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('Y1:AD1')->setCellValue('Y1', Yii::t('frontend', 'Domisili'))->getStyle('Y1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('Y2', Yii::t('frontend', 'Provinsi'))->getStyle('Y2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('Z2', Yii::t('frontend', 'Kabupaten/Kota'))->getStyle('Z2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AA2', Yii::t('frontend', 'Kecamatan'))->getStyle('AA2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AB2', Yii::t('frontend', 'Desa/Kelurahan'))->getStyle('AB2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AC2', Yii::t('frontend', 'Rt/Rw'))->getStyle('AC2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AD2', Yii::t('frontend', 'Alamat'))->getStyle('AD2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AE1:AE2')->setCellValue('AE1', Yii::t('frontend', 'Pendidikan Akhir'))->getStyle('AE1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AF1:AG1')->setCellValue('AF1', Yii::t('frontend', 'No BPJS'))->getStyle('AF1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AF2', Yii::t('frontend', 'Kesehatan'))->getStyle('AF2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AG2', Yii::t('frontend', 'Ketenagakerjaan'))->getStyle('AG2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AH1:AI1')->setCellValue('AH1', Yii::t('frontend', 'Akun Bank'))->getStyle('AH1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AH2', Yii::t('frontend', 'Nama Bank'))->getStyle('AH2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AI2', Yii::t('frontend', 'No Rek Bank'))->getStyle('AI2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AJ1:AJ2')->setCellValue('AJ1', Yii::t('frontend', 'Awal Masuk'))->getStyle('AJ1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AK1:AK2')->setCellValue('AK1', Yii::t('frontend', 'Cost Center'))->getStyle('AK1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AL1:AL2')->setCellValue('AL1', Yii::t('frontend', 'NPWP'))->getStyle('AL1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AM1:AM2')->setCellValue('AM1', Yii::t('frontend', 'Email'))->getStyle('AM1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AN1:AN2')->setCellValue('AN1', Yii::t('frontend', 'Manulife'))->getStyle('AN1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AO1:AO2')->setCellValue('AO1', Yii::t('frontend', 'Area Kerja'))->getStyle('AO1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AP1:AP2')->setCellValue('AP1', Yii::t('frontend', 'Masa Kerja(Sesuai Dasar MK)'))->getStyle('AP1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AQ1:AQ2')->setCellValue('AQ1', Yii::t('frontend', 'No Kartu Keluarga'))->getStyle('AQ1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AR1:AR2')->setCellValue('AR1', Yii::t('frontend', 'Jumlah Keluarga'))->getStyle('AR1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AS1:AS2')->setCellValue('AS1', Yii::t('frontend', 'Jumlah Anak'))->getStyle('AS1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AT1:AT2')->setCellValue('AT1', Yii::t('frontend', 'Kontak Darurat)'))->getStyle('AT1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AU1:AU2')->setCellValue('AU1', Yii::t('frontend', 'Tgl Dasar MK'))->getStyle('AU1')->applyFromArray($headerStyle);

            $row = 3;
            $no = 1;

            /** @var Pegawai $dataPegawai */
            foreach ($dataPegawais as $dataPegawai) {
//                if ($no == 100)
//                    break;
                $awalMasuk = $dataPegawai->getTanggalAwalMasuk();
                $namaDepartemen = '';
                $namaJabatan = '';
                $namaFungsiJabatan = '';
                $namaStruktur = '';
                if ($pegawaiStruktur = $dataPegawai->pegawaiStruktur) {
                    if ($struktur = $pegawaiStruktur->struktur) {
                        if ($departemen = $struktur->departemen) {
                            $namaDepartemen = $departemen->nama;
                        }
                        if ($jabatan = $struktur->jabatan) {
                            $namaJabatan = $jabatan->nama;
                        }
                        if ($fungsiJabatan = $struktur->fungsiJabatan) {
                            $namaFungsiJabatan = $fungsiJabatan->nama;
                        }
                        $namaStruktur = $struktur->nameCostume;
                    }
                }
                $idPegawai = '';
                $businerUnit = '';
                $jenisPerjanjian = '';
                $masaKerja = '';
                if ($perjanjianKerja = $dataPegawai->perjanjianKerja) {
                    $idPegawai = str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT);
                    $businerUnit = $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    $jenisPerjanjian = $perjanjianKerja->jenis->nama;
                    if ($dasarMasaKerja = $perjanjianKerja->getPerjanjianDasar()) {
                        $tanggalmk = $dasarMasaKerja->mulai_berlaku ? date('d-m-Y', strtotime($dasarMasaKerja->mulai_berlaku)) : '';
                        $masaKerja = $dasarMasaKerja->hitungMasaKerja();
                        $masaKerja = $masaKerja['tahun'] > 0 ? $masaKerja['tahun'] . ' Tahun' : '';
                    }
                }
                $golongan = '';
                if ($pegawaiGolongan = $dataPegawai->pegawaiGolongan) {
                    $golongan = $pegawaiGolongan->golongan->nama;
                }
                $costCenter = '';
                if ($pegawaiCostCenter = $dataPegawai->costCenter) {
                    $costCenter = $pegawaiCostCenter->costCenterTemplate->area_kerja;
                }
                $nomorBpjsKetenagaKerjaan = '';
                if ($bpjsKetenagaKerjaan = $dataPegawai->getPegawaiBpjsBerdasarkanJenis(PegawaiBpjs::BPJS_KETENAGAKERJAAN)) {
                    $nomorBpjsKetenagaKerjaan = $bpjsKetenagaKerjaan->nomor;
                }
                $nomorBpjsKesehatan = '';
                if ($bpjsKesehatan = $dataPegawai->getPegawaiBpjsBerdasarkanJenis(PegawaiBpjs::BPJS_KESEHATAN)) {
                    $nomorBpjsKesehatan = $bpjsKesehatan->nomor;
                }
                $akunBank = '';
                $nomorRekening = '';
                if ($pegawaiBank = $dataPegawai->pegawaiBank) {
                    $akunBank = $pegawaiBank->namaBank;
                    $nomorRekening = $pegawaiBank->nomer_rekening;
                }
                $areaKerja = '';
                if ($pegawaiAreaKerja = $dataPegawai->areaKerja) {
                    $areaKerja = $pegawaiAreaKerja->refAreaKerja->name;
                }

                $namaPendidikanTerakhir = '';
                if ($pendidikanTerakhir = PegawaiPendidikan::pendidikanTerakhir($dataPegawai->id)) {
                    $namaPendidikanTerakhir = $pendidikanTerakhir->jenjang_;
                }
                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $namaDepartemen)->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $namaJabatan)->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('D' . $row, $namaFungsiJabatan)->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('E' . $row, $namaStruktur)->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $idPegawai)->getStyle('F' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValueExplicit('G' . $row, $dataPegawai->nomor_ktp, DataType::TYPE_STRING)->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai->nama_lengkap)->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('I' . $row, $golongan)->getStyle('I' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('J' . $row, $businerUnit)->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('K' . $row, $jenisPerjanjian)->getStyle('K' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('L' . $row, $dataPegawai->tempat_lahir)->getStyle('L' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('M' . $row, date('d-m-Y', strtotime($dataPegawai->tanggal_lahir)))->getStyle('M' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('N' . $row, $dataPegawai->getUsia())->getStyle('N' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('O' . $row, $dataPegawai->jenisKelamin)->getStyle('O' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('P' . $row, $dataPegawai->statusPernikahan)->getStyle('P' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('Q' . $row, $dataPegawai->agama_)->getStyle('Q' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('R' . $row, $dataPegawai->no_handphone)->getStyle('R' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('S' . $row, $dataPegawai->pegawaiKtpProvinsi ? $dataPegawai->pegawaiKtpProvinsi->nama : '')->getStyle('S' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('T' . $row, $dataPegawai->pegawaiKtpKabupaten ? $dataPegawai->pegawaiKtpKabupaten->nama : '')->getStyle('T' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('U' . $row, $dataPegawai->pegawaiKtpKecamatan ? $dataPegawai->pegawaiKtpKecamatan->nama : '')->getStyle('U' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('V' . $row, $dataPegawai->pegawaiKtpDesa ? $dataPegawai->pegawaiKtpDesa->nama : '')->getStyle('V' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('W' . $row, $dataPegawai->ktp_rt . '/' . $dataPegawai->ktp_rw)->getStyle('W' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('X' . $row, $dataPegawai->ktp_alamat)->getStyle('X' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('Y' . $row, $dataPegawai->pegawaiDomisiliProvinsi ? $dataPegawai->pegawaiDomisiliProvinsi->nama : '')->getStyle('Y' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('Z' . $row, $dataPegawai->pegawaiDomisiliKabupaten ? $dataPegawai->pegawaiDomisiliKabupaten->nama : '')->getStyle('Z' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AA' . $row, $dataPegawai->pegawaiDomisiliKecamatan ? $dataPegawai->pegawaiDomisiliKecamatan->nama : '')->getStyle('AA' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AB' . $row, $dataPegawai->pegawaiDomisiliDesa ? $dataPegawai->pegawaiDomisiliDesa->nama : '')->getStyle('AB' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AC' . $row, $dataPegawai->domisili_rt . '/' . $dataPegawai->domisili_rw)->getStyle('AC' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AD' . $row, $dataPegawai->domisili_alamat)->getStyle('AD' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AE' . $row, $namaPendidikanTerakhir)->getStyle('AE' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AF' . $row, $nomorBpjsKesehatan)->getStyle('AF' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AG' . $row, $nomorBpjsKetenagaKerjaan)->getStyle('AG' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AH' . $row, $akunBank)->getStyle('AH' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AI' . $row, $nomorRekening)->getStyle('AI' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AJ' . $row, $awalMasuk)->getStyle('AJ' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AK' . $row, $costCenter)->getStyle('AK' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValueExplicit('AL' . $row, $dataPegawai->pegawaiNpwp ? $dataPegawai->pegawaiNpwp->nomor : '', DataType::TYPE_STRING)->getStyle('AL' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AM' . $row, $dataPegawai->alamat_email)->getStyle('AM' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AN' . $row, $dataPegawai->pegawaiAsuransiLain ? $dataPegawai->pegawaiAsuransiLain->nomor : '')->getStyle('AN' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AO' . $row, $areaKerja)->getStyle('AO' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AP' . $row, $masaKerja)->getStyle('AP' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValueExplicit('AQ' . $row, $dataPegawai->nomor_kk, DataType::TYPE_STRING)->getStyle('AQ' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AR' . $row, $dataPegawai->getJumlahKk())->getStyle('AR' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AS' . $row, $dataPegawai->getJumlahAnak())->getStyle('AS' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AT' . $row, $dataPegawai->getJumlahDarurat())->getStyle('AT' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('AU' . $row, $tanggalmk)->getStyle('AU' . $row)->applyFromArray(array_merge($center, $borderInside));

                $row++;
                $no++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'AU' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'AU' . $row)->applyFromArray($font);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=datapegawaiall' . date('YmdHis') . '.xls');
            header('Cache-Control: max-age=0');

            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;

    }

    /**
     * @param int $resign
     * @return bool
     */
    public function exportAllId(int $resign)
    {
        $query = Pegawai::find();

        $query->orderBy('pegawai.nama_lengkap ASC');

        $query->andWhere(['is_resign' => $resign]);

        $dataPegawais = $query->all();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(3);
            $activeSheet->getColumnDimension('B')->setWidth(17);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(10);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(20);
            $activeSheet->getColumnDimension('I')->setWidth(8);
            $activeSheet->getColumnDimension('J')->setWidth(8);

            $activeSheet->mergeCells('A1:A2')->setCellValue('A1', Yii::t('frontend', 'No'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('B1:B2')->setCellValue('B1', Yii::t('frontend', 'Pegawai Id'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('C1:C2')->setCellValue('C1', Yii::t('frontend', 'Perjanjian Kerja Id'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('D1:D2')->setCellValue('D1', Yii::t('frontend', 'Pegawai Golongan Id'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('E1:E2')->setCellValue('E1', Yii::t('frontend', 'Pegawai Struktur Id'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('F1:F2')->setCellValue('F1', Yii::t('frontend', 'Id Pegawai'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('G1:G2')->setCellValue('G1', Yii::t('frontend', 'Nama'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('H1:H2')->setCellValue('H1', Yii::t('frontend', 'Golongan'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('I1:I2')->setCellValue('I1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('J1:J2')->setCellValue('J1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('J1')->applyFromArray($headerStyle);

            $row = 3;
            $no = 1;

            /** @var Pegawai $dataPegawai */
            foreach ($dataPegawais as $dataPegawai) {
//                if ($no == 100)
//                    break;

                $pegawaiStruktur = $dataPegawai->pegawaiStruktur;
                $idPegawai = '';
                $businerUnit = '';
                $jenisPerjanjian = '';
                if ($perjanjianKerja = $dataPegawai->perjanjianKerja) {
                    $idPegawai = str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT);
                    $businerUnit = $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    $jenisPerjanjian = $perjanjianKerja->jenis->nama;
                }
                $golongan = '';
                if ($pegawaiGolongan = $dataPegawai->pegawaiGolongan) {
                    $golongan = $pegawaiGolongan->golongan->nama;
                }
                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai ? $dataPegawai->id : '')->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $perjanjianKerja ? $perjanjianKerja->id : '')->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('D' . $row, $pegawaiGolongan ? $pegawaiGolongan->id : '')->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('E' . $row, $pegawaiStruktur ? $pegawaiStruktur->id : '')->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $idPegawai)->getStyle('F' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai->nama_lengkap)->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('H' . $row, $golongan)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('I' . $row, $businerUnit)->getStyle('I' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('J' . $row, $jenisPerjanjian)->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));

                $row++;
                $no++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'AU' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'AU' . $row)->applyFromArray($font);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=datapegawaiallId' . date('YmdHis') . '.xls');
            header('Cache-Control: max-age=0');

            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;

    }

    /**
     * @param int $resign
     * @return bool
     */
    public function exportAllBca(int $resign)
    {
        $query = Pegawai::find();

        $query->orderBy('pegawai.nama_lengkap ASC');

        $query->andWhere(['is_resign' => $resign]);

        $query->joinWith('perjanjianKerja');
        $query->andWhere(['IN', 'perjanjian_kerja.kontrak', [PerjanjianKerja::BCA_ADA, PerjanjianKerja::BCA_KBU]]);


        $dataPegawais = $query->all();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(3);
            $activeSheet->getColumnDimension('B')->setWidth(17);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(10);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(20);
            $activeSheet->getColumnDimension('I')->setWidth(8);
            $activeSheet->getColumnDimension('J')->setWidth(8);
            $activeSheet->getColumnDimension('K')->setWidth(10);
            $activeSheet->getColumnDimension('L')->setWidth(15);
            $activeSheet->getColumnDimension('M')->setWidth(10);
            $activeSheet->getColumnDimension('N')->setWidth(5);
            $activeSheet->getColumnDimension('O')->setWidth(12);
            $activeSheet->getColumnDimension('P')->setWidth(15);
            $activeSheet->getColumnDimension('Q')->setWidth(7);
            $activeSheet->getColumnDimension('R')->setWidth(12);
            $activeSheet->getColumnDimension('S')->setWidth(15);
            $activeSheet->getColumnDimension('T')->setWidth(15);
            $activeSheet->getColumnDimension('U')->setWidth(15);
            $activeSheet->getColumnDimension('V')->setWidth(15);
            $activeSheet->getColumnDimension('W')->setWidth(7);
            $activeSheet->getColumnDimension('X')->setWidth(15);
            $activeSheet->getColumnDimension('Y')->setWidth(15);
            $activeSheet->getColumnDimension('Z')->setWidth(15);
            $activeSheet->getColumnDimension('AA')->setWidth(15);
            $activeSheet->getColumnDimension('AB')->setWidth(15);
            $activeSheet->getColumnDimension('AC')->setWidth(7);
            $activeSheet->getColumnDimension('AD')->setWidth(15);
            $activeSheet->getColumnDimension('AE')->setWidth(8);
            $activeSheet->getColumnDimension('AF')->setWidth(10);
            $activeSheet->getColumnDimension('AG')->setWidth(10);
            $activeSheet->getColumnDimension('AH')->setWidth(10);
            $activeSheet->getColumnDimension('AI')->setWidth(10);
            $activeSheet->getColumnDimension('AJ')->setWidth(10);
            $activeSheet->getColumnDimension('AK')->setWidth(15);
            $activeSheet->getColumnDimension('AL')->setWidth(12);
            $activeSheet->getColumnDimension('AM')->setWidth(15);
            $activeSheet->getColumnDimension('AN')->setWidth(12);
            $activeSheet->getColumnDimension('AO')->setWidth(15);
            $activeSheet->getColumnDimension('AP')->setWidth(10);
            $activeSheet->getColumnDimension('AQ')->setWidth(10);
            $activeSheet->getColumnDimension('AR')->setWidth(5);
            $activeSheet->getColumnDimension('AS')->setWidth(5);
            $activeSheet->getColumnDimension('AT')->setWidth(10);
            $activeSheet->getColumnDimension('AU')->setWidth(10);

            $activeSheet->mergeCells('A1:A2')->setCellValue('A1', Yii::t('frontend', 'No'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('B1:B2')->setCellValue('B1', Yii::t('frontend', 'Departemen'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('C1:C2')->setCellValue('C1', Yii::t('frontend', 'Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('D1:D2')->setCellValue('D1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('E1:E2')->setCellValue('E1', Yii::t('frontend', 'Nama Struktur'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('F1:F2')->setCellValue('F1', Yii::t('frontend', 'Id Pegawai'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('G1:G2')->setCellValue('G1', Yii::t('frontend', 'Nomor KTP'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('H1:H2')->setCellValue('H1', Yii::t('frontend', 'Nama'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('I1:I2')->setCellValue('I1', Yii::t('frontend', 'Golongan'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('J1:J2')->setCellValue('J1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('K1:K2')->setCellValue('K1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('L1:L2')->setCellValue('L1', Yii::t('frontend', 'Tempat Lahir'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('M1:M2')->setCellValue('M1', Yii::t('frontend', 'Tgl Lahir'))->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('N1:N2')->setCellValue('N1', Yii::t('frontend', 'Umur '))->getStyle('N1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('O1:O2')->setCellValue('O1', Yii::t('frontend', 'Jenis Kelamin '))->getStyle('O1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('P1:P2')->setCellValue('P1', Yii::t('frontend', 'Status Pernikahan'))->getStyle('P1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('Q1:Q2')->setCellValue('Q1', Yii::t('frontend', 'Agama'))->getStyle('Q1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('R1:R2')->setCellValue('R1', Yii::t('frontend', 'No Handphone'))->getStyle('R1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('S1:X1')->setCellValue('S1', Yii::t('frontend', 'Alamat KTP'))->getStyle('S1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('S2', Yii::t('frontend', 'Provinsi'))->getStyle('S2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('T2', Yii::t('frontend', 'Kabupaten/Kota'))->getStyle('T2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('U2', Yii::t('frontend', 'Kecamatan'))->getStyle('U2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('V2', Yii::t('frontend', 'Desa/Kelurahan'))->getStyle('V2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('W2', Yii::t('frontend', 'Rt/Rw'))->getStyle('W2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('X2', Yii::t('frontend', 'Alamat'))->getStyle('X2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('Y1:AD1')->setCellValue('Y1', Yii::t('frontend', 'Domisili'))->getStyle('Y1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('Y2', Yii::t('frontend', 'Provinsi'))->getStyle('Y2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('Z2', Yii::t('frontend', 'Kabupaten/Kota'))->getStyle('Z2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AA2', Yii::t('frontend', 'Kecamatan'))->getStyle('AA2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AB2', Yii::t('frontend', 'Desa/Kelurahan'))->getStyle('AB2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AC2', Yii::t('frontend', 'Rt/Rw'))->getStyle('AC2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AD2', Yii::t('frontend', 'Alamat'))->getStyle('AD2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AE1:AE2')->setCellValue('AE1', Yii::t('frontend', 'Pendidikan Akhir'))->getStyle('AE1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AF1:AG1')->setCellValue('AF1', Yii::t('frontend', 'No BPJS'))->getStyle('AF1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AF2', Yii::t('frontend', 'Kesehatan'))->getStyle('AF2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AG2', Yii::t('frontend', 'Ketenagakerjaan'))->getStyle('AG2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AH1:AI1')->setCellValue('AH1', Yii::t('frontend', 'Akun Bank'))->getStyle('AH1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AH2', Yii::t('frontend', 'Nama Bank'))->getStyle('AH2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('AI2', Yii::t('frontend', 'No Rek Bank'))->getStyle('AI2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AJ1:AJ2')->setCellValue('AJ1', Yii::t('frontend', 'Awal Masuk'))->getStyle('AJ1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AK1:AK2')->setCellValue('AK1', Yii::t('frontend', 'Cost Center'))->getStyle('AK1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AL1:AL2')->setCellValue('AL1', Yii::t('frontend', 'NPWP'))->getStyle('AL1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AM1:AM2')->setCellValue('AM1', Yii::t('frontend', 'Email'))->getStyle('AM1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AN1:AN2')->setCellValue('AN1', Yii::t('frontend', 'Manulife'))->getStyle('AN1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AO1:AO2')->setCellValue('AO1', Yii::t('frontend', 'Area Kerja'))->getStyle('AO1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AP1:AP2')->setCellValue('AP1', Yii::t('frontend', 'Masa Kerja(Sesuai Dasar MK)'))->getStyle('AP1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AQ1:AQ2')->setCellValue('AQ1', Yii::t('frontend', 'No Kartu Keluarga'))->getStyle('AQ1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AR1:AR2')->setCellValue('AR1', Yii::t('frontend', 'Jumlah Keluarga'))->getStyle('AR1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AS1:AS2')->setCellValue('AS1', Yii::t('frontend', 'Jumlah Anak'))->getStyle('AS1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AT1:AT2')->setCellValue('AT1', Yii::t('frontend', 'Kontak Darurat)'))->getStyle('AT1')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('AU1:AU2')->setCellValue('AU1', Yii::t('frontend', 'Tgl Dasar MK'))->getStyle('AU1')->applyFromArray($headerStyle);

            $row = 3;
            $no = 1;

            /** @var Pegawai $dataPegawai */
            foreach ($dataPegawais as $dataPegawai) {
//                    if ($no == 10)
//                        break;
                $awalMasuk = $dataPegawai->getTanggalAwalMasuk();
                $namaDepartemen = '';
                $namaJabatan = '';
                $namaFungsiJabatan = '';
                $namaStruktur = '';
                if ($pegawaiStruktur = $dataPegawai->pegawaiStruktur) {
                    if ($struktur = $pegawaiStruktur->struktur) {
                        if ($departemen = $struktur->departemen) {
                            $namaDepartemen = $departemen->nama;
                        }
                        if ($jabatan = $struktur->jabatan) {
                            $namaJabatan = $jabatan->nama;
                        }
                        if ($fungsiJabatan = $struktur->fungsiJabatan) {
                            $namaFungsiJabatan = $fungsiJabatan->nama;
                        }
                        $namaStruktur = $struktur->nameCostume;
                    }
                }
                $idPegawai = '';
                $businerUnit = '';
                $jenisPerjanjian = '';
                $masaKerja = '';
                if ($perjanjianKerja = $dataPegawai->perjanjianKerja) {
                    $idPegawai = str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT);
                    $businerUnit = $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    $jenisPerjanjian = $perjanjianKerja->jenis->nama;
                    if ($dasarMasaKerja = $perjanjianKerja->getPerjanjianDasar()) {
                        $tanggalmk = $dasarMasaKerja->mulai_berlaku ? date('d-m-Y', strtotime($dasarMasaKerja->mulai_berlaku)) : '';
                        $masaKerja = $dasarMasaKerja->hitungMasaKerja();
                        $masaKerja = $masaKerja['tahun'] > 0 ? $masaKerja['tahun'] . ' Tahun' : '';
                    }
                }
                $golongan = '';
                if ($pegawaiGolongan = $dataPegawai->pegawaiGolongan) {
                    $golongan = $pegawaiGolongan->golongan->nama;
                }
                $costCenter = '';
                if ($pegawaiCostCenter = $dataPegawai->costCenter) {
                    $costCenter = $pegawaiCostCenter->costCenterTemplate->area_kerja;
                }
                $nomorBpjsKetenagaKerjaan = '';
                if ($bpjsKetenagaKerjaan = $dataPegawai->getPegawaiBpjsBerdasarkanJenis(PegawaiBpjs::BPJS_KETENAGAKERJAAN)) {
                    $nomorBpjsKetenagaKerjaan = $bpjsKetenagaKerjaan->nomor;
                }
                $nomorBpjsKesehatan = '';
                if ($bpjsKesehatan = $dataPegawai->getPegawaiBpjsBerdasarkanJenis(PegawaiBpjs::BPJS_KESEHATAN)) {
                    $nomorBpjsKesehatan = $bpjsKesehatan->nomor;
                }
                $akunBank = '';
                $nomorRekening = '';
                if ($pegawaiBank = $dataPegawai->pegawaiBank) {
                    $akunBank = $pegawaiBank->namaBank;
                    $nomorRekening = $pegawaiBank->nomer_rekening;
                }
                $areaKerja = '';
                if ($pegawaiAreaKerja = $dataPegawai->areaKerja) {
                    if ($refAreaKerja = $pegawaiAreaKerja->refAreaKerja) {
                        $areaKerja = $refAreaKerja->name;
                    }
                }

                $namaPendidikanTerakhir = '';
                if ($pendidikanTerakhir = PegawaiPendidikan::pendidikanTerakhir($dataPegawai->id)) {
                    $namaPendidikanTerakhir = $pendidikanTerakhir->jenjang_;
                }
                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $namaDepartemen)->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $namaJabatan)->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('D' . $row, $namaFungsiJabatan)->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('E' . $row, $namaStruktur)->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $idPegawai)->getStyle('F' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai->nomor_ktp)->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValueExplicit('G' . $row, $dataPegawai->nomor_ktp, DataType::TYPE_STRING)->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai->nama_lengkap)->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('I' . $row, $golongan)->getStyle('I' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('J' . $row, $businerUnit)->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('K' . $row, $jenisPerjanjian)->getStyle('K' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('L' . $row, $dataPegawai->tempat_lahir)->getStyle('L' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('M' . $row, date('d-m-Y', strtotime($dataPegawai->tanggal_lahir)))->getStyle('M' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('N' . $row, $dataPegawai->getUsia())->getStyle('N' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('O' . $row, $dataPegawai->jenisKelamin)->getStyle('O' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('P' . $row, $dataPegawai->statusPernikahan)->getStyle('P' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('Q' . $row, $dataPegawai->agama_)->getStyle('Q' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('R' . $row, $dataPegawai->no_handphone)->getStyle('R' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('S' . $row, $dataPegawai->pegawaiKtpProvinsi ? $dataPegawai->pegawaiKtpProvinsi->nama : '')->getStyle('S' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('T' . $row, $dataPegawai->pegawaiKtpKabupaten ? $dataPegawai->pegawaiKtpKabupaten->nama : '')->getStyle('T' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('U' . $row, $dataPegawai->pegawaiKtpKecamatan ? $dataPegawai->pegawaiKtpKecamatan->nama : '')->getStyle('U' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('V' . $row, $dataPegawai->pegawaiKtpDesa ? $dataPegawai->pegawaiKtpDesa->nama : '')->getStyle('V' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('W' . $row, $dataPegawai->ktp_rt . '/' . $dataPegawai->ktp_rw)->getStyle('W' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('X' . $row, $dataPegawai->ktp_alamat)->getStyle('X' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('Y' . $row, $dataPegawai->pegawaiDomisiliProvinsi ? $dataPegawai->pegawaiDomisiliProvinsi->nama : '')->getStyle('Y' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('Z' . $row, $dataPegawai->pegawaiDomisiliKabupaten ? $dataPegawai->pegawaiDomisiliKabupaten->nama : '')->getStyle('Z' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AA' . $row, $dataPegawai->pegawaiDomisiliKecamatan ? $dataPegawai->pegawaiDomisiliKecamatan->nama : '')->getStyle('AA' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AB' . $row, $dataPegawai->pegawaiDomisiliDesa ? $dataPegawai->pegawaiDomisiliDesa->nama : '')->getStyle('AB' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AC' . $row, $dataPegawai->domisili_rt . '/' . $dataPegawai->domisili_rw)->getStyle('AC' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AD' . $row, $dataPegawai->domisili_alamat)->getStyle('AD' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AE' . $row, $namaPendidikanTerakhir)->getStyle('AE' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AF' . $row, $nomorBpjsKesehatan)->getStyle('AF' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AG' . $row, $nomorBpjsKetenagaKerjaan)->getStyle('AG' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AH' . $row, $akunBank)->getStyle('AH' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AI' . $row, $nomorRekening)->getStyle('AI' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AJ' . $row, $awalMasuk)->getStyle('AJ' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AK' . $row, $costCenter)->getStyle('AK' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValueExplicit('AL' . $row, $dataPegawai->pegawaiNpwp ? $dataPegawai->pegawaiNpwp->nomor : '', DataType::TYPE_STRING)->getStyle('AL' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AM' . $row, $dataPegawai->alamat_email)->getStyle('AM' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AN' . $row, $dataPegawai->pegawaiAsuransiLain ? $dataPegawai->pegawaiAsuransiLain->nomor : '')->getStyle('AN' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AO' . $row, $areaKerja)->getStyle('AO' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AP' . $row, $masaKerja)->getStyle('AP' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValueExplicit('AQ' . $row, $dataPegawai->nomor_kk, DataType::TYPE_STRING)->getStyle('AQ' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AR' . $row, $dataPegawai->getJumlahKk())->getStyle('AR' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AS' . $row, $dataPegawai->getJumlahAnak())->getStyle('AS' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('AT' . $row, $dataPegawai->getJumlahDarurat())->getStyle('AT' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('AU' . $row, $tanggalmk)->getStyle('AU' . $row)->applyFromArray(array_merge($center, $borderInside));

                $row++;
                $no++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'AU' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'AU' . $row)->applyFromArray($font);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=datapegawaiall' . date('YmdHis') . '.xls');
            header('Cache-Control: max-age=0');

            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;

    }

}
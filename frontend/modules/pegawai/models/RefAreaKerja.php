<?php

namespace frontend\modules\pegawai\models;

use kartik\tree\models\Tree;
use kartik\tree\Module;
use kartik\tree\TreeView;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "ref_area_kerja".
 */
class RefAreaKerja extends Tree
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_area_kerja';
    }

    /**
     * Override isDisabled method if you need as shown in the
     * example below. You can override similarly other methods
     * like isActive, isMovable etc.
     */
    public function isDisabled()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isChildAllowed()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['keterangan'], 'string'];
        return $rules;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['keterangan'] = Yii::t('frontend', 'Keterangan');
        return $attributeLabels;
    }

    /**
     * Mengambil nama urutan parents sampai dengan dibawah sistem administrator
     * @param int $depth
     * @param string $glue
     * @param string $currCss
     * @param string $new
     * @return string
     * @throws InvalidConfigException
     */
    public function getBreadcrumbs($depth = 1, $glue = ' &raquo; ', $currCss = 'kv-crumb-active', $new = 'Untitled')
    {
        /**
         * @var Module $module
         */
        if ($this->isNewRecord || empty($this)) {
            return $currCss ? Html::tag('span', $new, ['class' => $currCss]) : $new;
        }
        $depth = empty($depth) ? null : intval($depth);
        $module = TreeView::module();
        $nameAttribute = ArrayHelper::getValue($module->dataStructure, 'nameAttribute', 'name');
        /** @noinspection PhpUndefinedMethodInspection */
        $crumbNodes = $depth === null ? $this->parents()->all() : $this->parents($depth - 1)->all();
        $crumbNodes[] = $this;
        $i = 1;
        $len = count($crumbNodes);
        $crumbs = [];
        foreach ($crumbNodes as $node) {
            if ($node->id == 1) {
                continue;
            }
            $name = $node->$nameAttribute;
            if ($i === $len && $currCss) {
                $name = Html::tag('span', $name, ['class' => $currCss]);
            }
            $crumbs[] = $name;
            $i++;
        }
        return implode($glue, $crumbs);
    }

}

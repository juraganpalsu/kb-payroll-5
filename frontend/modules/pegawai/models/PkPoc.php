<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PkPoc as BasePkPoc;

/**
 * This is the model class for table "pk_poc".
 */
class PkPoc extends BasePkPoc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['perjanjian_kerja_id', 'pegawai_struktur_id'], 'required'],
            [['perjanjian_kerja_id', 'pegawai_struktur_id'], 'string', 'max' => 32],
        ];
    }

}

<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\payroll\models\PtkpSetting;
use frontend\modules\pegawai\models\base\PegawaiPtkp as BasePegawaiPtkp;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pegawai_ptkp".
 */
class PegawaiPtkp extends BasePegawaiPtkp
{


    const TK = 'tk';
    const K0 = 'k0';
    const K1 = 'k1';
    const K2 = 'k2';
    const K3 = 'k3';


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'status', 'ptkp', 'pegawai_id'], 'required'],
            [['tahun', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['ptkp'], 'number'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['status'], 'string', 'max' => 5],
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal()) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal()) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal()) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @param int $tahun
     * @return array|ActiveRecord|null
     */
    public function getValueOfPtkp(int $tahun = 0)
    {
        $tahun = $tahun ?: date('Y');
        return PtkpSetting::find()->andWhere(['<=', 'tahun', $tahun])->orderBy('tahun DESC')->asArray()->one();
    }

    /**
     * @param string $prgawaiId
     * @return PegawaiPtkp|null
     */
    public static function ptkp(string $prgawaiId)
    {
        return self::findOne(['pegawai_id' => $prgawaiId, 'tahun' => date('Y')]);
    }

}

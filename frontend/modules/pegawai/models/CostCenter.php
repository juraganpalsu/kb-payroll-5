<?php

namespace frontend\modules\pegawai\models;

use Yii;
use \frontend\modules\pegawai\models\base\CostCenter as BaseCostCenter;

/**
 * This is the model class for table "cost_center".
 */
class CostCenter extends BaseCostCenter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mulai_berlaku', 'cost_center_template_id', 'pegawai_id'], 'required'],
            [['status', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id', 'cost_center_template_id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['mulai_berlaku'], 'mulaiBerlakuValidation'],
            [['akhir_berlaku'], 'akhirBerlakuValidation']
        ];
    }


    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->mulai_berlaku) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->mulai_berlaku]));
        }
    }

    /**
     * @param string $attribute
     */
    public function mulaiBerlakuValidation(string $attribute)
    {
        /** @var self $query */
        $query = self::find()->andWhere(['>=', 'akhir_berlaku', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id])->all();
        if ($query) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
        }
    }

    /**
     * @return string
     */
    public function getNama()
    {
        return $this->costCenterTemplate->kode . ' - ' . $this->costCenterTemplate->area_kerja;
    }

}

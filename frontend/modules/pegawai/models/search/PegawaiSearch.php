<?php


namespace frontend\modules\pegawai\models\search;


use common\components\Status;
use frontend\modules\dashboard\models\DsAkhirKontrak;
use frontend\modules\dashboard\models\DsAkhirKontrakDetail;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\struktur\models\Struktur;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Created by PhpStorm.
 * File Name: PegawaiSearch.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 18/12/19
 * Time: 23.37
 */
class PegawaiSearch extends \frontend\models\search\PegawaiSearch
{

    /**
     * Mencari pegawai yg akan habis kontrak dalam waktu 2 bulan kedepan
     */
    public function dataHabisKontrak(array $aksiHabisKontrak = [Status::DEFLT])
    {
        $akhirKontraks = [];
        $awal = date('Y-m-01');
        $akhir = date('Y-m-t', strtotime('+ 2 months'));
        $queryDsAkhirKontrak = DsAkhirKontrak::find()->where(['BETWEEN', 'tanggal', $awal, $akhir])->orderBy('tanggal ASC')->all();
        if ($queryDsAkhirKontrak) {
            foreach ($queryDsAkhirKontrak as $dsAkhirKontrak) {
                /** @var DsAkhirKontrakDetail $dsAkhirKontrakDetail */
                foreach ($dsAkhirKontrak->dsAkhirKontrakDetails as $dsAkhirKontrakDetail) {
                    $perjanjianKerjas = explode(',', $dsAkhirKontrakDetail->perjanjian_kerja_ids);
                    $queryPerjanjianKerjas = PerjanjianKerja::find()->andWhere(['aksi_habis_kontrak' => $aksiHabisKontrak])->andWhere(['IN', 'id', $perjanjianKerjas])->all();
                    /** @var PerjanjianKerja $perjanjianKerja */
                    foreach ($queryPerjanjianKerjas as $perjanjianKerja) {
                        $struktur = $perjanjianKerja ? $perjanjianKerja->pegawai ? $perjanjianKerja->pegawai->pegawaiStruktur : '' : '';
                        $golongan = $perjanjianKerja ? $perjanjianKerja->pegawai ? $perjanjianKerja->pegawai->pegawaiGolongan : '' : '';
                        $area = $perjanjianKerja ? $perjanjianKerja->pegawai ? $perjanjianKerja->pegawai->areaKerja : '' : '';
                        /** @var Struktur $queryAtasanLangsung */
                        $queryAtasanLangsung = $struktur ? $struktur->struktur->parents(1)->one() : '';
                        $namaAtasanLangsung = '';
                        if ($queryAtasanLangsung) {
                            $modelPegawaiStruktur = new PegawaiStruktur();
                            $modelPegawaiStruktur->struktur_id = $queryAtasanLangsung->id;
                            $queryPegawaiStruktur = $modelPegawaiStruktur->pegawaiBerdasarTanggal();
                            if ($queryPegawaiStruktur) {
                                $namaAtasanLangsung .= '( ';
                                /** @var PegawaiStruktur $pegawaiStruktur */
                                foreach ($queryPegawaiStruktur as $pegawaiStruktur) {
                                    if ($pegawaiStruktur->pegawai) {
                                        $namaAtasanLangsung .= $pegawaiStruktur->pegawai->nama_lengkap . ', ';
                                    } else {
                                        $namaAtasanLangsung .= '-,';
                                    }
                                }
                                $namaAtasanLangsung .= ' )';
                            }
                        }
                        $queryHistoryPerjanjianKerja = PerjanjianKerja::find()->andWhere(['pegawai_id' => $perjanjianKerja->pegawai_id])->orderBy('mulai_berlaku ASC')->all();
                        $historyPerjanjianKerja = array_map(function (PerjanjianKerja $perjanjianKerja) {
                            return $perjanjianKerja->jenis->nama . '(' . date('d-m-Y', strtotime($perjanjianKerja->mulai_berlaku)) . ' s/d ' . date('d-m-Y', strtotime($perjanjianKerja->akhir_berlaku)) . ')';
                        }, $queryHistoryPerjanjianKerja);
                        $akhirKontraks[] = [
                            'id' => $perjanjianKerja->id,
                            'departemen' => $struktur ? $struktur->struktur->departemen->nama : '',
                            'jabatan' => $struktur ? $struktur->struktur->jabatan->nama : '',
                            'fungsiJabatan' => $struktur ? $struktur->struktur->fungsiJabatan->nama : '',
                            'idPegawai' => $perjanjianKerja->id_pegawai,
                            'nama' => $perjanjianKerja ? $perjanjianKerja->pegawai? $perjanjianKerja->pegawai->nama : '' : '',
                            'golongan' => $golongan ? $golongan->golongan->nama : '',
                            'atasanLangsung' => $struktur ? $queryAtasanLangsung ? $queryAtasanLangsung->name . $namaAtasanLangsung : '' : '',
                            'bisnisUnit' => $perjanjianKerja->namaKontrak,
                            'jenisKontrak' => $perjanjianKerja->jenis->nama,
                            'awalKontrak' => date('d-m-Y', strtotime($perjanjianKerja->mulai_berlaku)),
                            'akhirKontrak' => date('d-m-Y', strtotime($perjanjianKerja->akhir_berlaku)),
                            'historyKontrak' => $historyPerjanjianKerja ? implode(',', $historyPerjanjianKerja) : '',
                            'area' => $area ? $area->refAreaKerja->name : '',
                        ];
                    }
                }
            }
        }
        return $akhirKontraks;
    }


    /**
     * @param int $perPage
     * @return ArrayDataProvider
     */
    public function searchHabisKontrak(int $perPage = 20)
    {
        $akhirKontraks = $this->dataHabisKontrak();
        $provider = new ArrayDataProvider([
            'allModels' => $akhirKontraks,
            'pagination' => [
                'pageSize' => $perPage,
            ],
            'sort' => [
                'attributes' => ['nama'],
            ],
        ]);

        return $provider;
    }

    /**
     * @param int $perPage
     * @return ArrayDataProvider
     */
    public function searchHabisKontrakAfterProses(int $perPage = 20)
    {
        $akhirKontraks = $this->dataHabisKontrak([PerjanjianKerja::DILANJUTKAN, PerjanjianKerja::PHK, PerjanjianKerja::PENDING]);
        $provider = new ArrayDataProvider([
            'allModels' => $akhirKontraks,
            'pagination' => [
                'pageSize' => $perPage,
            ],
            'sort' => [
                'attributes' => ['nama'],
            ],
        ]);

        return $provider;
    }

    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function searchHabisKontrakExcel()
    {
        $dataHabisKontrak = $this->dataHabisKontrak();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(10);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(30);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(15);
            $activeSheet->getColumnDimension('J')->setWidth(15);
            $activeSheet->getColumnDimension('K')->setWidth(15);
            $activeSheet->getColumnDimension('L')->setWidth(30);
            $activeSheet->getColumnDimension('M')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'Departemen'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Jabatan'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Id Pegawai'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Nama'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Golongan'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Atasan Langsung'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Awal Kontrak'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', Yii::t('frontend', 'Akhir Kontrak'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', Yii::t('frontend', 'History Kontrak'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', Yii::t('frontend', 'Area'))->getStyle('M1')->applyFromArray($headerStyle);

            $row = 2;

            /** @var Pegawai $dataPegawai */
            foreach ($dataHabisKontrak as $dataPegawai) {
                $activeSheet->setCellValue('A' . $row, $dataPegawai['departemen'])->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['jabatan'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['fungsiJabatan'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['idPegawai'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['nama'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['golongan'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['atasanLangsung'])->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['bisnisUnit'])->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['jenisKontrak'])->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('J' . $row, $dataPegawai['awalKontrak'])->getStyle('J' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('K' . $row, $dataPegawai['akhirKontrak'])->getStyle('K' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('L' . $row, $dataPegawai['historyKontrak'])->getStyle('L' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('M' . $row, $dataPegawai['area'])->getStyle('M' . $row)->applyFromArray(array_merge($borderInside));


                $row++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'M' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'M' . $row)->applyFromArray($font);

            $Excel_writer->save('frontend/web/habis-kontrak/data-pegawai-akan-habis-kontrak-' . date('m-Y', strtotime('+ 2 months')) . '.xls');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }

}
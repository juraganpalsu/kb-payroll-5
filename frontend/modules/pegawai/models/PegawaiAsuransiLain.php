<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiAsuransiLain as BasePegawaiAsuransiLain;

/**
 * This is the model class for table "pegawai_asuransi_lain".
 *
 * @property string $nama_
 * @property array $_nama
 *
 */
class PegawaiAsuransiLain extends BasePegawaiAsuransiLain
{
    const MANULIFE = 1;

    public $_nama = [self::MANULIFE => 'MANULIFE'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor', 'tanggal_kepesertaan', 'pegawai_id'], 'required'],
            [['tanggal_kepesertaan', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id', 'pegawai_keluarga_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['nomor'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    public function getNama_()
    {
        return $this->nama ? $this->_nama[$this->nama] : '-';
    }

}

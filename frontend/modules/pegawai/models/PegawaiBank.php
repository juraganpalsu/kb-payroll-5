<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiBank as BasePegawaiBank;

/**
 * This is the model class for table "pegawai_bank".
 *
 * @property string $namaBank
 * @property array $_nama_bank
 */
class PegawaiBank extends BasePegawaiBank
{

    const BRI = 1;
    const BCA = 2;

    public $_nama_bank = [self::BRI => 'BANK BRI', self::BCA => 'BANK BCA'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_bank', 'nomer_rekening', 'pegawai_id'], 'required'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            ['nomer_rekening', 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama_bank'], 'string', 'max' => 225],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['nomer_rekening', 'string', 'length' => [10, 16]],
            ['nomer_rekening', 'match', 'pattern' => '/^[0-9]*$/i']
        ];
    }

    /**
     * @return mixed|string
     */
    public function getNamaBank()
    {
        return $this->nama_bank ? $this->_nama_bank[$this->nama_bank] : '-';
    }

}

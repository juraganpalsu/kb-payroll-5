<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiFiles as BasePegawaiFiles;
use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "pegawai_files".
 *
 *
 *
 * @property string $upload_dir
 * @property UploadedFile $doc_file
 *
 * @property array $_jenis_file
 *
 */
class PegawaiFiles extends BasePegawaiFiles
{

    public $upload_dir = 'uploads/pegawai/';
    public $doc_file;

    /**
     * 1. KTP
     * 2. NPWP
     * 3. Ijasah & Transkrip Nilai
     * 4. Kartu Keluarga
     * 5. Foto Buku Rekening
     * 6. BPJS Kesehatan
     * 7. BPJS Ketenagakerjaan
     *
     */

    const KTP = 1;
    const NPWP = 2;
    const IJAZAH_TRANSKRIP_NILAI = 3;
    const KARTU_KELUARGA = 4;
    const BUKU_REKENING = 5;
    const BPJS_KESEHATAN = 6;
    const BPJS_KETENAGAKERJAAN = 7;

    public $_jenis_file = [self::KTP => 'KTP', self::NPWP => 'NPWP', self::IJAZAH_TRANSKRIP_NILAI => 'IJAZAH_TRANSKRIP_NILAI',
        self::KARTU_KELUARGA => 'KARTU_KELUARGA', self::BUKU_REKENING => 'BUKU_REKENING', self::BPJS_KESEHATAN => 'BPJS_KESEHATAN', self::BPJS_KETENAGAKERJAAN => 'BPJS_KETENAGAKERJAAN'];

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['jenis', 'pegawai_id'], 'required'],
            [['jenis', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 225],
            [['pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, pdf', 'maxFiles' => 1],
            ['jenis', 'validasiJenisFile']
        ];
    }

    /**
     * @param string $attribute
     */
    public function validasiJenisFile(string $attribute): void
    {
        if ($this->isNewRecord && PegawaiFiles::findOne(['pegawai_id' => $this->pegawai_id, 'jenis' => $this->$attribute])) {
            $this->addError($attribute, Yii::t('frontend', 'Jenis file {jenis} telah diupload, silahkan update untuk merubah.', ['jenis' => $this->_jenis_file[$this->jenis]]));
        }
    }


    /**
     * Upload file
     *
     * @return bool
     */
    public function upload(): bool
    {
        if ($this->validate()) {
            $dir = $this->upload_dir . '/' . $this->pegawai_id . '/';
            if (!file_exists($dir)) {
                mkdir($dir);
            }

            /** @var UploadedFile $file */
            $namaFile = UploadedFile::getInstance($this, 'doc_file');
            $this->nama = $namaFile->name;
            if ($this->save()) {
                if ($namaFile->saveAs($dir . $this->id . $this->nama)) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }


    /**
     * Mengambil konfigurasi attachment untuk keperluan update data pada purchase requitition
     *
     * @param $searchFor
     * @return array
     */
    public function getAttachmentCofiguration($searchFor)
    {
        $dir = $this->upload_dir . $this->pegawai_id . '/' . $this->id . $this->nama;
        $configs['url'][] = Url::to($dir, true);
        $configs['key'][] = ['key' => $this->id];
        return $configs[$searchFor];
    }

}
//'http://kb-payroll.front/uploads/pegawai/ee0641ae2df911ebabfc0050568c1db4/4fe5ad47-5a72-11eb-87a7-28b2bd4c5a9a.pdf'
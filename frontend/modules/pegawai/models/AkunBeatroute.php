<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\AkunBeatroute as BaseAkunBeatroute;
use Yii;

/**
 * This is the model class for table "akun_beatroute".
 */
class AkunBeatroute extends BaseAkunBeatroute
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mulai_berlaku', 'beatroute_id'], 'required'],
            [['mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['beatroute_id', 'beatroute_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['mulai_berlaku'], 'mulaiBerlakuValidation'],
            [['akhir_berlaku'], 'akhirBerlakuValidation']
        ];
    }


    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->mulai_berlaku) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->mulai_berlaku]));
        }
    }

    /**
     * @param string $attribute
     */
    public function mulaiBerlakuValidation(string $attribute)
    {
        /** @var self $query */
        $query = self::find()->andWhere(['>=', 'akhir_berlaku', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id])->all();
        if ($query) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
        }
    }


}

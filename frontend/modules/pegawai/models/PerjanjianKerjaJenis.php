<?php

namespace frontend\modules\pegawai\models;

use Yii;
use \frontend\modules\pegawai\models\base\PerjanjianKerjaJenis as BasePerjanjianKerjaJenis;

/**
 * This is the model class for table "perjanjian_kerja_jenis".
 */
class PerjanjianKerjaJenis extends BasePerjanjianKerjaJenis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['nama'], 'string', 'max' => 45],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

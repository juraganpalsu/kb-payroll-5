<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiGolongan as BasePegawaiGolongan;
use Yii;

/**
 * This is the model class for table "pegawai_golongan".
 */
class PegawaiGolongan extends BasePegawaiGolongan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'golongan_id', 'mulai_berlaku'], 'required'],
            [['aksi', 'golongan_id', 'created_by', 'created_by_org_chart', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['aksi', 'mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id', 'pegawai_id'], 'string', 'max' => 32],
            [['aksi', 'lock'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['mulai_berlaku'], 'mulaiBerlakuValidation'],
            [['akhir_berlaku'], 'akhirBerlakuValidation']
        ];
    }


    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->mulai_berlaku) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->mulai_berlaku]));
        }
    }

    /**
     * @param string $attribute
     */
    public function mulaiBerlakuValidation(string $attribute)
    {
        /** @var self $query */
        $query = self::find()->andWhere(['>=', 'akhir_berlaku', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id])->all();
        if ($query) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
        }
    }

}

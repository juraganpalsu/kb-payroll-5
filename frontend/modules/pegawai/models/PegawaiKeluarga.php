<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiKeluarga as BasePegawaiKeluarga;

/**
 * This is the model class for table "pegawai_keluarga".
 *
 * @property array $_hubungan
 * @property string $hubungan_
 *
 */
class PegawaiKeluarga extends BasePegawaiKeluarga
{
    const KAKEK = 1;
    const NENEK = 2;
    const AYAH = 3;
    const IBU = 4;
    const PAMAN = 5;
    const BIBI = 6;
    const KAKAK = 7;
    const ADIK = 8;
    const SUAMI = 9;
    const ISTRI = 10;
    const ANAK = 11;
    const KELUARGA_LAIN = 12;

    public $_hubungan = [
        self::SUAMI => 'SUAMI',
        self::ISTRI => 'ISTRI',
        self::ANAK => 'ANAK',
        self::AYAH => 'AYAH',
        self::IBU => 'IBU',
        self::ADIK => 'ADIK',
        self::KAKAK => 'KAKAK',
        self::PAMAN => 'PAMAN',
        self::BIBI => 'BIBI',
        self::KAKEK => 'KAKEK',
        self::NENEK => 'NENEK',
        self::KELUARGA_LAIN => 'KELUARGA LAIN'
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_lahir', 'hubungan', 'nama', 'nik', 'pekerjaan', 'pegawai_id'], 'required'],
            [['hubungan', 'kelompok', 'urutan_ke', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_lahir', 'alamat', 'catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['pekerjaan'], 'string', 'max' => 45],
            [['no_handphone'], 'string', 'min' => 10, 'max' => 13],
            [['lock', 'urutan_ke'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['nik'], 'string', 'max' => 16],
        ];
    }

    /**
     *
     * @return string Hubungan
     */
    public function getHubungan_()
    {
        return $this->_hubungan[$this->hubungan];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if (!in_array($this->hubungan, [self::ANAK, self::KAKAK, self::ADIK])) {
            $this->urutan_ke = 0;
        }
        return true;
    }

}

<?php

namespace frontend\modules\pegawai\models;

use common\components\Status;
use DateTime;
use Exception;
use frontend\modules\izin\models\CutiSaldo;
use frontend\modules\pegawai\models\base\PerjanjianKerja as BasePerjanjianKerja;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "perjanjian_kerja".
 *
 *
 * @property PegawaiStruktur $pegawaiStruktur
 *
 * @property string $status_
 * @property array $statuses
 * @property array $_kontrak
 * @property string $namaKontrak
 * @property string $perjanjianKerja
 * @property array $statusHabisKontrak
 * @property integer $difference
 *
 */
class PerjanjianKerja extends BasePerjanjianKerja
{

    const TIDAK_AKTIF = 0;
    const AKTIF = 1;

    /**
     * Bisnis Unit / Kontrak
     */
    const KBU = 1;
    const ADA = 2;
    const BCA_KBU = 3;
    const BCA_ADA = 4;
//    const OUTSOURCING = 5;
    const GCI = 6;
    const OSI = 7;
//    const TOP_CLEAN = 8;
    const PIC_BCA = 9;
    const GSM = 10;

    const BELUM_PROSES = 1;
    const DILANJUTKAN = 2;
    const PHK = 3;
    const PENDING = 4;

    public $statuses = [self::AKTIF => 'AKTIF', self::TIDAK_AKTIF => 'TIDAK AKTIF'];


    /**
     *
     * Bisnis Unit / Kontrak
     * @var array
     */
    public $_kontrak = [
        self::KBU => 'KBU',
        self::ADA => 'ADA',
        self::BCA_KBU => 'BCA KBU',
        self::BCA_ADA => 'BCA ADA',
//        self::OUTSOURCING => 'OUTSOURCING',
        self::GCI => 'GCI',
        self::OSI => 'OSI',
//        self::TOP_CLEAN => 'TOP CLEAN',
        self::PIC_BCA => 'PIC BCA',
        self::GSM => 'GSM'
    ];


    /**
     * @var string[]
     */
    public $statusHabisKontrak = [
        self::BELUM_PROSES => 'Belum Proses',
        self::DILANJUTKAN => 'Dilanjutkan',
        self::PHK => 'PHK',
        self::PENDING => 'Pending'
    ];

    /**
     * @var $difference
     * menampung perbedaan tahun untuk masa kerja
     */
    public $difference;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_perjanjian', 'kontrak', 'mulai_berlaku', 'pegawai_id', 'id_pegawai'], 'required'],
            [['aksi', 'tanggal_resign', 'mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['aksi', 'status', 'created_by', 'created_by_org_chart', 'updated_by', 'deleted_by', 'lock', 'aksi_habis_kontrak', 'dasar_cuti'], 'integer'],
            [['keterangan', 'keterangan_habis_kontrak'], 'string'],
            ['id_pegawai', 'uniqueValidation'],
            [['id', 'pegawai_id'], 'string', 'max' => 32],
            [['id_pegawai'], 'string', 'max' => 20],
            [['id_pegawai'], 'validateWhitespace'],
            [['aksi', 'jenis_perjanjian', 'kontrak', 'status', 'dasar_masa_kerja', 'dasar_cuti', 'lock', 'aksi_habis_kontrak'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['mulai_berlaku'], 'mulaiBerlakuValidation'],
            [['akhir_berlaku'], 'akhirBerlakuValidation']
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        $condition = ['pegawai_id' => $this->pegawai_id];
        if (!$insert) {
            $condition = ['AND', ['pegawai_id' => $this->pegawai_id], ['!=', 'id', $this->id]];
        }
        if ($this->dasar_masa_kerja == Status::YA) {
            PerjanjianKerja::updateAll(['dasar_masa_kerja' => Status::TIDAK], $condition);
        }
        if ($this->dasar_cuti == Status::YA) {
            PerjanjianKerja::updateAll(['dasar_cuti' => Status::TIDAK], $condition);
        }
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->dasar_masa_kerja == Status::YA) {
            $masaKerja = $this->hitungMasaKerja();
            if ($masaKerja['tahun'] > 0) {
                try {
                    $this->setKuotaCuti();
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        }
    }


    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->mulai_berlaku) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->mulai_berlaku]));
        }
    }

    /**
     * @param string $attribute
     */
    public function mulaiBerlakuValidation(string $attribute)
    {
        /** @var self $query */
        $query = self::find()->andWhere(['>=', 'akhir_berlaku', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id])->orderBy('mulai_berlaku DESC')->one();
        if ($query) {
            $model = self::findOne($query['id']);
            if (!is_null($model->akhir_berlaku)) {
                if ($model->akhir_berlaku >= $this->$attribute) {
                    $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
                }
            } else {
                $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
            }
        }
    }

    /**
     * Validasi white space
     * @param $attribute
     */
    public function validateWhitespace($attribute)
    {
        if (preg_match('/\s/', $this->$attribute) == 1) {
            $this->addError($attribute, Yii::t('frontend', 'Tidak diperkenankan terdapat spasi.'));
        }
    }


    /**
     * @param string $attribute
     */
    public function uniqueValidation(string $attribute)
    {
        $query = self::find()->andWhere('id_pegawai = :idpegawai AND pegawai_id != :pegawaiid', [':idpegawai' => $this->$attribute, ':pegawaiid' => $this->pegawai_id])->all();
        if (!empty($query)) {
            $this->addError($attribute, Yii::t('frontend', 'Id Pegawai "{idpegawai}" telah terpakai.', ['idpegawai' => $this->$attribute]));
        }
    }


    /**
     * @param string $tanggal
     * @return ActiveQuery
     */
    public function getPegawaiStruktur(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id'])->andWhere([PegawaiStruktur::tableName() . '.deleted_by' => 0])
            ->andWhere('(mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku is null )')
            ->via('pkPocs');
    }


    /**
     * @param string $tanggal
     * @return array|ActiveRecord|null
     */
    public function perjanjianKerjaBerdasarTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        return PerjanjianKerja::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere('(mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku is null )')
            ->one();
    }

    /**
     * @param string $tanggal
     * @return bool
     */
    public function isAktif(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        /** @var PerjanjianKerja $perjanjianKerjaBerdasarTanggal */
        if ($perjanjianKerjaBerdasarTanggal = $this->perjanjianKerjaBerdasarTanggal($tanggal)) {
            if ($perjanjianKerjaBerdasarTanggal->id == $this->id) {
                if (empty($perjanjianKerjaBerdasarTanggal->tanggal_resign)) {
                    return true;
                } else {
                    if ($perjanjianKerjaBerdasarTanggal->tanggal_resign > $tanggal) {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getNamaKontrak()
    {
        return $this->_kontrak[$this->kontrak];
    }


    /**
     * Set cuti saldo pegawai
     * @param bool $fromConsole
     * @throws Exception
     */
    public function setKuotaCuti(bool $fromConsole = false)
    {
        try {
            $masaKerja = $this->hitungMasaKerja();
            $kuotaCuti = $this->hitungKuotaCuti();
            $dateTime = new DateTime('Jan 1');
            $tanggalAwal = $dateTime->format('Y-m-d');
            $akhirBerlakuObj = new DateTime('Jun 30');
            $akhirBerlakuObj = $akhirBerlakuObj->modify('+1 year');
            $akhirBerlaku = $akhirBerlakuObj->format('Y-m-d');
            $modelCutiSaldo = new CutiSaldo();
            $modelCutiSaldo->pegawai_id = $this->pegawai_id;
            if ($masaKerja['tahun'] == 1) {
                $awalMasuk = new DateTime($this->mulai_berlaku);
                //contoh kasus
                //pada bulan februari 2020, orang yg masa kerja defaultnya 10-2018 dan jan/feb-2019 akan sama2 dihitung 1 tahun
                //padahal untuk yg 2018 harusnya sudah full
                if (($dateTime->format('Y') - $awalMasuk->format('Y')) == 1) {
                    $mulaiBerlaku = date('M d', strtotime($this->mulai_berlaku));
                    $dateTime = new DateTime($mulaiBerlaku);
                    $akhirTahun = new DateTime('Dec 31');
                    $kuotaCuti = (int)$dateTime->diff($akhirTahun)->m;
                    $kuotaCuti += 1;
                    $tanggalAwal = $dateTime->format('Y-m-d');
                }
            }
            $modelCutiSaldo->mulai_berlaku = $tanggalAwal;
            $modelCutiSaldo->akhir_berlaku = $akhirBerlaku;
            $modelCutiSaldo->kuota = $kuotaCuti;
            $modelCutiSaldo->saldo = $kuotaCuti;
            $modelCutiSaldo->terpakai = 0;
            $modelCutiSaldo->kadaluarsa = 0;
            $cekCutiSaldo = CutiSaldo::findOne(['mulai_berlaku' => $tanggalAwal, 'akhir_berlaku' => $akhirBerlaku, 'pegawai_id' => $this->pegawai_id]);
            if ($cekCutiSaldo) {
                $modelCutiSaldo = $cekCutiSaldo;
                $modelCutiSaldo->kuota = $kuotaCuti;
            }
            if ($fromConsole) {
                $modelCutiSaldo->detachBehavior('createdby');
            }
            $modelPegawai = Pegawai::findOne($modelCutiSaldo->pegawai_id);
            $perjanjianKerja = $modelPegawai->getPerjanjianKerjaUntukTanggal($modelCutiSaldo->mulai_berlaku);
            if ($perjanjianKerja && $modelPegawai->getPegawaiStrukturUntukTanggal($modelCutiSaldo->mulai_berlaku) && $modelPegawai->getPegawaiGolonganUntukTanggal($modelCutiSaldo->mulai_berlaku)) {
                if ($perjanjianKerja->isAktif($modelCutiSaldo->mulai_berlaku)) {
                    $modelCutiSaldo->save();
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     *
     * Menghitung kuota cuti
     *
     * 1 - 4 = 12
     * 5 - 9 = 14
     * 10-14 = 17
     * 15 > = 19
     *
     * @param string $tanggalMulaiBerlaku
     * @param string $tanggalSampaiDengan
     * @return int
     */
    public function hitungKuotaCuti(string $tanggalMulaiBerlaku = '', string $tanggalSampaiDengan = '')
    {
        $masaKerja = $this->hitungMasaKerja($tanggalMulaiBerlaku, $tanggalSampaiDengan);
        $kuotaCuti = 0;
        if (($masaKerja['tahun'] >= 1) && ($masaKerja['tahun'] <= 4)) {
            $kuotaCuti = 12;
        } elseif (($masaKerja['tahun'] >= 5) && ($masaKerja['tahun'] <= 9)) {
            $kuotaCuti = 14;
        } elseif (($masaKerja['tahun'] >= 10) && ($masaKerja['tahun'] <= 14)) {
            $kuotaCuti = 17;
        } elseif ($masaKerja['tahun'] >= 15) {
            $kuotaCuti = 19;
        }
        return $kuotaCuti;
    }

    /**
     *
     * Menghitung masa kerja dalam tahun
     * Tanggal awal adalah terdapat pada perjanjian kerja yg diset sebagai dasar masa kerja
     *
     * @param string $tanggalMulaiBerlaku
     * @param string $tanggalSampaiDengan
     * @param bool $hitungBulan
     * @return array
     */
    public function hitungMasaKerja(string $tanggalMulaiBerlaku = '', string $tanggalSampaiDengan = '', bool $hitungBulan = false): array
    {
        $masaKerja = ['tahun' => 0, 'bulan' => 0, 'dalamBulan' => 0];
        try {
            if (empty($tanggalMulaiBerlaku)) {
                $tanggalMulaiBerlaku = $this->mulai_berlaku;
            }
            if (empty($tanggalSampaiDengan)) {
                $tanggalSampaiDengan = date('Y-m-d');
            }
            $awal = new DateTime($tanggalMulaiBerlaku);
            $akhir = new DateTime($tanggalSampaiDengan);
            $masaKerjaTahun = $awal->diff($akhir)->y;
            $masaKerjaBulan = $awal->diff($akhir)->m;
            $dalamBulan = 0;
            if ($hitungBulan) {
                if ($masaKerjaBulan > 12) {
                    $masaKerjaBulan -= 12;
                }
                $dalamBulan = (12 * $masaKerjaTahun) + $masaKerjaBulan;
            }
            $masaKerja = ['tahun' => $masaKerjaTahun, 'bulan' => $masaKerjaBulan, 'dalamBulan' => $dalamBulan];
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $masaKerja;
    }

    /**
     * @param string $pegawaiId
     * @return PerjanjianKerja|null
     */
    public static function getPerjanjianDasarStatic(string $pegawaiId)
    {
        return PerjanjianKerja::findOne(['dasar_masa_kerja' => Status::YA, 'pegawai_id' => $pegawaiId]);
    }

    /**
     * @param string $pegawaiId
     * @return PerjanjianKerja|null
     */
    public static function getDasarCutiStatic(string $pegawaiId): ?PerjanjianKerja
    {
        return PerjanjianKerja::findOne(['dasar_cuti' => Status::YA, 'pegawai_id' => $pegawaiId]);
    }

    /**
     * @return PerjanjianKerja|null
     */
    public function getPerjanjianDasar(): ?PerjanjianKerja
    {
        return self::getPerjanjianDasarStatic($this->pegawai_id);
    }

    /**
     * @return PerjanjianKerja|null
     */
    public function getDasarCuti(): ?PerjanjianKerja
    {
        return self::getDasarCutiStatic($this->pegawai_id);
    }

    /**
     * @param string $tanggal
     * @return array|ActiveRecord[]
     */
    public function perjanjianKerjaAktifBerdasarTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        return PerjanjianKerja::find()
            ->andWhere('(mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku is null )')
            ->all();
    }

    /**
     *
     * Mengambil perjanjian kerja aktif dalam satu bulan(awal sampai dengan akhir tanggal)
     * $tanggal awal adalah tanggal 1 pada bulan tersebut, dan tanggal tanggal akhir otomatis akan diambil dari akhir bulan tersebut
     *
     * @param string $tanggal_awal
     * @return array|ActiveRecord[]
     */
    public function perjanjianKerjaAktifBerdasarBulan(string $tanggal_awal)
    {
        $tanggal_akhir = date('Y-m-t', strtotime($tanggal_awal));
        return PerjanjianKerja::find()
            ->andWhere(self::queryPerjanjianKerjaAktifBerdasarRangeTanggal($tanggal_awal, $tanggal_akhir))
            ->all();
    }


    /**
     * @param string $tanggal
     * @return string
     */
    public static function queryPerjanjianKerjaAktifBerdasarTanggal(string $tanggal): string
    {
        return '(perjanjian_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND perjanjian_kerja.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'perjanjian_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND perjanjian_kerja.akhir_berlaku is null )';
    }


    /**
     * @param string $tanggalAwal
     * @param string $tanggalAkhir
     * @return string
     */
    public static function queryPerjanjianKerjaAktifBerdasarRangeTanggal(string $tanggalAwal, string $tanggalAkhir): string
    {
        return '\'' . $tanggalAwal . '\' BETWEEN perjanjian_kerja.mulai_berlaku AND perjanjian_kerja.akhir_berlaku OR \'' . $tanggalAkhir . '\' BETWEEN perjanjian_kerja.mulai_berlaku AND perjanjian_kerja.akhir_berlaku OR perjanjian_kerja.mulai_berlaku BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR perjanjian_kerja.akhir_berlaku BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR ( \'' . $tanggalAwal . '\' <= perjanjian_kerja.mulai_berlaku AND \'' . $tanggalAkhir . '\' >= perjanjian_kerja.akhir_berlaku) OR ( \'' . $tanggalAwal . '\' >= perjanjian_kerja.mulai_berlaku AND \'' . $tanggalAkhir . '\' <= perjanjian_kerja.akhir_berlaku) OR (perjanjian_kerja.mulai_berlaku <=\'' . $tanggalAkhir . '\' AND perjanjian_kerja.akhir_berlaku is null)';
    }

    /**
     * @param bool $asLastYear
     * @return false|mixed|string
     */
    public function getAkhirBerlakuPerPegawai(bool $asLastYear = false)
    {
        $query = self::find()->andWhere(['pegawai_id' => $this->pegawai_id])->orderBy('mulai_berlaku DESC')->one();
        $akhirBerlaku = date('Y-m-d');
        if ($query) {
            $akhirBerlaku = $query['akhir_berlaku'];
            if (empty($akhirBerlaku) && $asLastYear) {
                $akhirBerlaku = date('Y-m-d', strtotime('12/31'));
            } elseif (!empty($akhirBerlaku) && $asLastYear) {
                $tahunAKhirBerlaku = explode('-', $akhirBerlaku);
                $akhirBerlaku = $tahunAKhirBerlaku[0] . '-12-31';
            }
            $resign = $query['tanggal_resign'];
            if (!empty($resign)) {
                $akhirBerlaku = $resign;
            }
        }
        return $akhirBerlaku;
    }


}

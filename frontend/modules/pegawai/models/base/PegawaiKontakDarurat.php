<?php

namespace frontend\modules\pegawai\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "pegawai_kontak_darurat".
 *
 * @property string $id
 * @property integer $hubungan
 * @property string $nama
 * @property string $pekerjaan
 * @property string $alamat
 * @property string $no_handphone
 * @property string $pegawai_id
 * @property string $catatan
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Pegawai $pegawai
 */
class PegawaiKontakDarurat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hubungan', 'nama', 'pekerjaan', 'no_handphone', 'pegawai_id', 'pegawai_id'], 'required'],
            [['hubungan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['alamat', 'catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'created_by_pk', 'pegawai_id'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['pekerjaan'], 'string', 'max' => 45],
            [['no_handphone'], 'string', 'max' => 13],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai_kontak_darurat';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'hubungan' => Yii::t('frontend', 'Hubungan'),
            'nama' => Yii::t('frontend', 'Nama'),
            'pekerjaan' => Yii::t('frontend', 'Pekerjaan'),
            'alamat' => Yii::t('frontend', 'Alamat'),
            'no_handphone' => Yii::t('frontend', 'No Handphone'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'catatan' => Yii::t('frontend', 'Catatan'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }
    
    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }


    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
//            'createdby' => [
//                'class' => CreatedByBehavior::class,
//                'values' => [
//                    'byPegawai' => function () {
//                        /** @var User $user */
//                        $user = Yii::$app->user->identity;
//                        if ($userPegawai = $user->userPegawai) {
//                            return $userPegawai->pegawai->perjanjianKerja->id;
//                        }
//                        return false;
//                    },
//                    'byStruktur' => function () {
//                        /** @var User $user */
//                        $user = Yii::$app->user->identity;
//                        if ($userPegawai = $user->userPegawai) {
//                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
//                                return $pegawaiStruktur->struktur_id;
//                            }
//                        }
//                        return false;
//                    },
//                ],
//            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace frontend\modules\pegawai\models\base;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "pk_poc".
 *
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_struktur_id
 *
 * @property \frontend\modules\pegawai\models\PegawaiStruktur $pegawaiStruktur
 * @property \frontend\modules\pegawai\models\PerjanjianKerja $perjanjianKerja
 */
class PkPoc extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['perjanjian_kerja_id', 'pegawai_struktur_id'], 'required'],
            [['perjanjian_kerja_id', 'pegawai_struktur_id'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pk_poc';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'perjanjian_kerja_id' => 'Perjanjian Kerja ID',
            'pegawai_struktur_id' => 'Pegawai Struktur ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(\frontend\modules\pegawai\models\PegawaiStruktur::class, ['id' => 'pegawai_struktur_id'])->andWhere([\frontend\modules\pegawai\models\PegawaiStruktur::tableName() . '.deleted_by' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(\frontend\modules\pegawai\models\PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id'])->andWhere([\frontend\modules\pegawai\models\PerjanjianKerja::tableName() . '.deleted_by' => 0]);
    }

}

<?php

namespace frontend\modules\pegawai\models\base;

use common\models\Pegawai;
use frontend\modules\struktur\models\Struktur;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "pegawai_struktur".
 *
 * @property string $id
 * @property integer $aksi
 * @property string $pegawai_id
 * @property integer $struktur_id
 * @property string $mulai_berlaku
 * @property string $akhir_berlaku
 * @property integer $status
 * @property string $keterangan
 * @property string $created_at
 * @property integer $created_by
 * @property integer $created_by_org_chart
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Pegawai $pegawai
 * @property Struktur $struktur
 * @property \frontend\modules\pegawai\models\PkPoc[] $pkPocs
 */
class PegawaiStruktur extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'struktur_id', 'mulai_berlaku'], 'required'],
            [['struktur_id', 'status', 'created_by', 'created_by_org_chart', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id', 'pegawai_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai_struktur';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'aksi' => Yii::t('frontend', 'Aksi'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'struktur_id' => Yii::t('frontend', 'Struktur ID'),
            'mulai_berlaku' => Yii::t('frontend', 'Mulai Berlaku'),
            'akhir_berlaku' => Yii::t('frontend', 'Akhir Berlaku'),
            'status' => Yii::t('frontend', 'Status'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'created_by_org_chart' => Yii::t('frontend', 'Created By Org Chart'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStruktur()
    {
        return $this->hasOne(Struktur::class, ['id' => 'struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPkPocs()
    {
        return $this->hasMany(\frontend\modules\pegawai\models\PkPoc::class, ['pegawai_struktur_id' => 'id']);
    }


    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

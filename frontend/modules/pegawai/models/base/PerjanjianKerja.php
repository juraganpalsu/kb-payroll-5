<?php

namespace frontend\modules\pegawai\models\base;

use frontend\modules\pegawai\models\Pegawai;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "perjanjian_kerja".
 *
 * @property string $id
 * @property integer $aksi
 * @property integer $jenis_perjanjian Jenis Kontrak
 * @property integer $kontrak Busines Unit
 * @property string $id_pegawai
 * @property string $mulai_berlaku
 * @property string $akhir_berlaku
 * @property integer $status
 * @property integer $dasar_masa_kerja
 * @property integer $dasar_cuti
 * @property string $tanggal_resign
 * @property string $pegawai_id
 * @property string $keterangan
 * @property string $aksi_habis_kontrak
 * @property string $keterangan_habis_kontrak
 * @property string $created_at
 * @property integer $created_by
 * @property integer $created_by_org_chart
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Pegawai $pegawai
 * @property \frontend\modules\pegawai\models\PkPoc[] $pkPocs
 * @property \frontend\modules\pegawai\models\PegawaiStruktur[] $pegawaiStrukturs
 * @property \frontend\modules\pegawai\models\PerjanjianKerjaJenis $jenis
 */
class PerjanjianKerja extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jenis_perjanjian', 'kontrak', 'mulai_berlaku', 'status', 'pegawai_id', 'id_pegawai'], 'required'],
            [['dasar_masa_kerja', 'tanggal_resign', 'mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['aksi', 'status', 'created_by', 'created_by_org_chart', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan', 'keterangan_habis_kontrak'], 'string'],
            [['id_pegawai'], 'unique'],
            [['id', 'pegawai_id'], 'string', 'max' => 32],
            [['aksi', 'lock', 'jenis_perjanjian', 'kontrak', 'id_pegawai', 'dasar_masa_kerja'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perjanjian_kerja';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'aksi' => Yii::t('frontend', 'Aksi'),
            'jenis_perjanjian' => Yii::t('frontend', 'Jenis Kontrak'),
            'kontrak' => Yii::t('frontend', 'Busines Unit'),
            'id_pegawai' => Yii::t('frontend', 'Id Pegawai'),
            'mulai_berlaku' => Yii::t('frontend', 'Mulai'),
            'akhir_berlaku' => Yii::t('frontend', 'Akhir'),
            'status' => Yii::t('frontend', 'Status'),
            'dasar_masa_kerja' => Yii::t('frontend', 'Sebagai Dasar Masa Kerja'),
            'dasar_cuti' => Yii::t('frontend', 'Sebagai Dasar Perhitungan Cuti'),
            'tanggal_resign' => Yii::t('frontend', 'Tanggal Resign'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'aksi_habis_kontrak' => Yii::t('frontend', 'Aksi'),
            'keterangan_habis_kontrak' => Yii::t('frontend', 'Keterangan'),
            'created_by_org_chart' => Yii::t('frontend', 'Created By Org Chart'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getJenis()
    {
        return $this->hasOne(\frontend\modules\pegawai\models\PerjanjianKerjaJenis::class, ['id' => 'jenis_perjanjian']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPkPocs()
    {
        return $this->hasMany(\frontend\modules\pegawai\models\PkPoc::class, ['perjanjian_kerja_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStrukturs()
    {
        return $this->hasMany(\frontend\modules\pegawai\models\PegawaiStruktur::class, ['id' => 'pegawai_struktur_id'])->via('pkPocs');
    }


    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     *
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace frontend\modules\pegawai\models\base;

use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "user_pegawai".
 *
 * @property integer $user_id
 * @property string $pegawai_id
 *
 * @property Pegawai $pegawai
 * @property User $user
 */
class UserPegawai extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'pegawai_id'], 'required'],
            [['user_id'], 'integer'],
            [['pegawai_id'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_pegawai';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'pegawai_id' => 'Pegawai ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}

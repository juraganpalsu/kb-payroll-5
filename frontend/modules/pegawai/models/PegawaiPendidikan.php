<?php

namespace frontend\modules\pegawai\models;

use frontend\modules\pegawai\models\base\PegawaiPendidikan as BasePegawaiPendidikan;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pegawai_pendidikan".
 *
 *
 * @property array $_jenjang
 * @property string $jenjang_
 */
class PegawaiPendidikan extends BasePegawaiPendidikan
{

    //Pendidikan
    const SD = 1;
    const SMP = 2;
    const SMA_SMK = 3;
    const D1 = 4;
    const D2 = 5;
    const D3 = 6;
    const S1 = 7;
    const S2 = 8;
    const S3 = 9;

    public $_jenjang = [
        self::SD => 'SD',
        self::SMP => 'SMP',
        self::SMA_SMK => 'SMA/SMK',
        self::D1 => 'D1',
        self::D2 => 'D2',
        self::D3 => 'D3',
        self::S1 => 'S1',
        self::S2 => 'S2',
        self::S3 => 'S3',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenjang', 'nama_sekolah', 'lokasi', 'pegawai_id', 'tahun_masuk', 'tahun_lulus'], 'required'],
            [['jenjang', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama_sekolah', 'lokasi', 'jurusan'], 'string', 'max' => 225],
            [['tahun_masuk', 'tahun_lulus'], 'number', 'min' => 0, 'max' => 9999],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['tahun_lulus', 'tahunLulusValidation']
        ];
    }


    /**
     * @param $attribute
     */
    public function tahunLulusValidation(string $attribute)
    {
        if ($this->$attribute < $this->tahun_masuk) {
            $this->addError($attribute, Yii::t('frontend', 'Tahun lulus tidak boleh lebih kecil dari tahun masuk.'));
        }
    }


    /**
     *
     * @return string jenjang pendidikan
     */
    public function getJenjang_()
    {
        return $this->_jenjang[$this->jenjang];
    }

    /**
     * @return array|ActiveRecord|null
     */
    public function getPendidikanTerakhir()
    {
        return PegawaiPendidikan::find()->andWhere(['pegawai_id' => $this->pegawai_id])->orderBy('jenjang DESC')->limit(1)->one();
    }

    /**
     * @param string $pegawaiId
     * @return array|ActiveRecord|null
     */
    public static function pendidikanTerakhir(string $pegawaiId)
    {
        $model = new self;
        $model->pegawai_id = $pegawaiId;
        return $model->getPendidikanTerakhir();
    }


}

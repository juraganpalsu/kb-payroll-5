<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pegawai */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pegawai-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Pegawai').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'idfp',
        'nama',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnAbsensi = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'masuk',
        'pulang',
        'tanggal',
        [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('app', 'Pegawai')
        ],
        'time_table',
        'status_kehadiran',
        'jumlah_jam_lembur',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAbsensi,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-absensi']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Absensi').' '. $this->title),
        ],
        'columns' => $gridColumnAbsensi
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnAttendance = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('app', 'Pegawai')
        ],
        'check_time',
        'idfp',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAttendance,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-attendance']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Attendance').' '. $this->title),
        ],
        'columns' => $gridColumnAttendance
    ]);
?>
    </div>
</div>

<?php


/* @var $this yii\web\View */
/* @var $model frontend\models\Pegawai */

$this->title = Yii::t('app', 'Create Pegawai');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pegawai-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

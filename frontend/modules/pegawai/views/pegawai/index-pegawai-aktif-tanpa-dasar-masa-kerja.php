<?php
/**
 * Created by PhpStorm.
 * File Name: index-pegawai-aktif-tanpa-dasar-masa-kerja.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 03/09/2020
 * Time: 14:15
 */

use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\PegawaiStruktur;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pegawai Tanpa Dasar Masa Kerja');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pegawai-index small">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'id_pegawai',
            'value' => function (Pegawai $model) {
                return $model->perjanjianKerja ? Html::tag('strong', $model->perjanjianKerja->id_pegawai, ['class' => 'font-weight-bold']) : '';
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'nama_lengkap',
            'value' => function (Pegawai $model) {
                return Html::a(Html::tag('strong', $model->nama_lengkap), ['view', 'id' => $model->id], ['class' => 'text-danger font-weight-bold', 'title' => 'Detail Data Pegawai']);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'tanggal_awal_masuk',
            'value' => function (Pegawai $model) {
                return $model->getTanggalAwalMasuk();
            },
        ],
        [
            'attribute' => 'golongan',
            'value' => function (Pegawai $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            }
        ],
        [
            'attribute' => 'jenis_kontrak',
            'value' => function (Pegawai $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->jenis->nama;
                }
                return '';
            },
        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (Pegawai $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            },
        ],
        [
            'attribute' => 'struktur',
            'value' => function (Pegawai $model) {
                if ($struktur = $model->pegawaiStruktur) {
                    return $struktur->struktur->nameCostume;
                }
                return '';
            }
        ],
        [
            'attribute' => 'area_kerja',
            'value' => function (Pegawai $model) {
                if ($struktur = $model->areaKerja) {
                    return $struktur->refAreaKerja->name;
                }
                return '';
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => ''
        ],
    ];
    ?>
    <?php try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
//            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10],
            'noExportColumns' => [1], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'pegawai' . date('dmYHis')
        ]);

        echo GridView::widget([
            'id' => 'grid-pegawai',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['pegawai-profile/pegawai-profile-form', 'id' => '-'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Tambah Pegawai')]]) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $model frontend\models\Pegawai */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Pegawai',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pegawai-update">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>

<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 15/11/2018
 * Time: 21:41
 */


use frontend\modules\pegawai\models\AreaKerja;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\RefAreaKerja;
use kartik\datecontrol\DateControl;
use kartik\tree\TreeViewInput;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model AreaKerja */
/**
 * @var $modelPegawai Pegawai
 */


$js = <<< JS
$(function() {
    $('#form-area-kerja').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
                // location.reload();
            }
        });
        return false;
    });
        
});
JS;

$this->registerJs($js);

$pegawaiid = $modelPegawai->id;

$action = $model->isNewRecord ? Url::to(['create-area-kerja', 'id' => $modelPegawai->id]) : Url::to(['update-area-kerja', 'id' => $model->id]);
$validation = $model->isNewRecord ? Url::to(['form-area-kerja-validation', 'id' => $modelPegawai->id]) : Url::to(['form-area-kerja-validation', 'id' => $model->id, 'isnew' => false]);
?>

<div class="area-kerja-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-area-kerja',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-8">
                <?php
                echo Html::activeLabel($model, 'ref_area_kerja_id', ['class' => 'control-label has-star', 'style' => ['content' => '']]);
                echo Html::tag('span', Yii::t('frontend', '*'), ['class' => 'text-danger small mark']);
                echo TreeViewInput::widget([
                    'model' => $model,
                    'attribute' => 'ref_area_kerja_id',
                    'query' => RefAreaKerja::find()->addOrderBy('root, lft'),
                    'headingOptions' => ['label' => Yii::t('frontend', 'Area Kerja')],
                    'rootOptions' => ['label' => '<i class="fas fa-tree text-success"></i>'],
                    'fontAwesome' => true,
                    'asDropdown' => true,
                    'multiple' => false,
                    'options' => ['disabled' => false, 'id' => 'tree-input-ref_area_kerja_id'],
                    'pluginOptions' => [
                        'dropdownParent' => new JsExpression('$("#form-modal")'),
                    ]
                ]);
                echo Html::activeHint($model, 'ref_area_kerja_id');
                ?>
                <br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'mulai_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->mulai_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'akhir_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->akhir_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ])->hint(Yii::t('frontend', '*) Jika akhir berlaku diisi, maka data sudah tidak dapat diedit.'), ['class' => 'text-danger small mark']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $form->field($model, 'keterangan')->textarea(['raw' => 5, 'placeholder' => !empty($sistemKerja) ? $sistemKerja->keterangan : $model->getAttributeLabel('keterangan')]);
                ?>
            </div>
        </div>

        <?php
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <?php
        echo $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput();
        ?>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }

    ?>

    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

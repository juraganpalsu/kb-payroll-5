<?php

use mdm\admin\components\Helper;
use common\components\Status;
use common\models\Golongan;
use common\models\SistemKerja;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\PerjanjianKerjaJenis;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $modelPerjanjiankerja PerjanjianKerja
 */

$this->title = Yii::t('app', 'Pegawais');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-export-all').click(function(e){
        e.preventDefault();
        let btn = $(this);
        btn.button('loading');
        let win = window.open(btn.attr('href'), '_blank');
        win.focus();
        location.reload();
        return false;
    });
    
    $("#kv-pjax-container-pegawai").on("pjax:success", function() {
        $('.call-modal-btn').click(function(e){
            e.preventDefault();
            let idModal = $('#form-modal');
            idModal.find('.modal-title').text($(this).data('header'));
            idModal.modal('show')
               .find('#modelContent')
               .load($(this).attr('href'));
            return false;
        });
        
        $('.change-resign-status-btn').click(function(e) {
            e.preventDefault();
            let btn = $(this);
            krajeeDialog.confirm(btn.data('confirm'), function(result) {
                if(result){
                    btn.button('loading');
                    $.post(btn.attr('href'))
                    .done(function (dt) {
                        console.log(dt);
                        if(dt.status === true){
                            location.reload();
                        }
                        btn.button('reset');
                    });
                }else{
                    btn.button('reset');
                }
            });
            return false;
        });
    });
    
    $('.change-resign-status-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                        // location.href = dt.data.url;
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);
?>
<div class="pegawai-index small">
    <?php
    try {
        echo ButtonDropdown::widget([
            'label' => Yii::t('frontend', 'Ekspor Resign'),
            'options' => [
                'class' => 'btn btn-info btn-flat',
            ],
            'dropdown' => [
                'items' => [
                    ['label' => Yii::t('frontend', 'Ya'), 'url' => Url::to(['/pegawai/pegawai-profile/export-all-bca', 'resign' => Status::YA]), 'options' => ['target' => '_blank']],
                    ['label' => Yii::t('frontend', 'Tidak'), 'url' => Url::to(['/pegawai/pegawai-profile/export-all-bca', 'resign' => Status::TIDAK])]
                ]
            ],
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'id_pegawai',
            'value' => function (Pegawai $model) {
                return $model->perjanjianKerja ? Html::tag('strong', str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT), ['class' => 'font-weight-bold']) : '';
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'nama_lengkap',
            'value' => function (Pegawai $model) {
                return Html::a(Html::tag('strong', $model->nama_lengkap), ['view', 'id' => $model->id], ['class' => 'text-danger font-weight-bold']);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'tanggal_awal_masuk',
            'value' => function (Pegawai $model) {
                return $model->getTanggalAwalMasuk();
            },
        ],
        [
            'attribute' => 'golongan',
            'value' => function (Pegawai $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'jenis_kontrak',
            'value' => function (Pegawai $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->jenis->nama;
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(PerjanjianKerjaJenis::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('jenis_kontrak'), 'id' => 'grid-pegawai-search-jenis_kontrak'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (Pegawai $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => array_diff_key($modelPerjanjiankerja->_kontrak, [PerjanjianKerja::GCI => '', PerjanjianKerja::OSI => '', PerjanjianKerja::PIC_BCA => '', PerjanjianKerja::GSM => '', PerjanjianKerja::KBU => '', PerjanjianKerja::ADA => '']),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'struktur',
            'value' => function (Pegawai $model) {
                if ($struktur = $model->pegawaiStruktur) {
                    return $struktur->struktur->nameCostume;
                }
                return '';
            }
        ],
        [
            'attribute' => 'area_kerja',
            'value' => function (Pegawai $model) {
                if ($areaKerja = $model->areaKerja) {
                    return $areaKerja->refAreaKerja->name;
                }
                return '';
            }
        ],
        [
            'attribute' => 'sistem_kerja',
            'width' => '20%',
            'value' => function (Pegawai $model) {
                $text = '';
                if (!is_null($model->pegawaiSistemKerja)) {
                    $text = $model->pegawaiSistemKerja->sistemKerja->nama;
                }
                return $text;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('sistem_kerja'), 'id' => 'grid-pegawai-search-sistem_kerja'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'label' => Yii::t('frontend', 'Payroll'),
            'value' => function (Pegawai $model) {
                if (Helper::checkRoute('/masterdata/default/view')) {
                    $masterData = $model->masterDataDefaults;
                    $warnaButton = 'btn-primary';
                    if (!$masterData) {
                        $warnaButton = 'btn-danger';
                    }
                    return Html::a(Html::tag('strong', '&#36;'), ['/masterdata/default/view', 'id' => $model->id], ['class' => 'btn ' . $warnaButton . ' btn-flat btn-xs btn-block', 'title' => 'Master Data Payroll']);
                }
                return '';
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'is_resign',
            'value' => function (Pegawai $model) {
                if ($model->is_resign) {
                    return Html::a(Status::activeInactive(Status::YA), ['change-resign-status', 'id' => $model->id, 'status' => Status::TIDAK], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-resign-status-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merubah status <strong>{pegawai}</strong> ?', ['pegawai' => $model->nama_lengkap])]]);
                }
                return Html::a(Status::activeInactive(Status::TIDAK), ['change-resign-status', 'id' => $model->id, 'status' => Status::YA], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-resign-status-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merubah status <strong>{pegawai}</strong> ?', ['pegawai' => $model->nama_lengkap])]]);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::activeInactive(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('is_resign'), 'id' => 'grid-pegawai-search-is_resign'],

            'format' => 'raw'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}'
        ],
    ];
    ?>
    <?php try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [1, 2, 3, 4, 5, 6, 7, 8],
            'noExportColumns' => [10,11], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'pegawai' . date('dmYHis')
        ]);

        echo GridView::widget([
            'id' => 'grid-pegawai',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['pegawai-profile/pegawai-profile-form', 'id' => '-'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Tambah Pegawai')]]) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

<?php

/**
 * Created by PhpStorm.
 * File Name: _keluarga-detail.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 13/10/19
 * Time: 22.19
 */

use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiAsuransiLain;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var PegawaiBpjs $modelBpjs
 * @var PegawaiAsuransiLain $modelAsuransiLain
 */

?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('frontend', 'BPJS Kesehatan'); ?>
                    <?php echo isset($modelBpjs->pegawaiKeluarga) ? Html::a(Yii::t('frontend', 'Update'), ['pegawai-profile/form-keluarga-bpjs', 'id' => $modelBpjs->pegawaiKeluarga->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Update data BPJS Kesehatan ke {nama}', ['nama' => $modelBpjs->pegawaiKeluarga->nama])]]) : '';
                    ?></h3>
            </div>
            <div class="box-body no-padding">
                <?php
                $gridColumnKeluargaBpjs = [
                    [
                        'label' => Yii::t('frontend', 'Nomor'),
                        'value' => function (PegawaiBpjs $model) {
                            return $model->nomor;
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Tanggal Kepesertaan'),
                        'value' => function (PegawaiBpjs $model) {
                            return $model->tanggal_kepesertaan ? date('d-m-Y', strtotime($model->tanggal_kepesertaan)) : '-';
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Catatan'),
                        'value' => function (PegawaiBpjs $model) {
                            return $model->catatan;
                        }
                    ],
                ];
                try {
                    echo DetailView::widget([
                        'id' => 'grid-data-keluarga-bpjs',
                        'model' => $modelBpjs,
                        'attributes' => $gridColumnKeluargaBpjs,
                        'options' => ['class' => 'table table-striped table-condensed detail-view'],
                    ]);
                } catch (Exception $e) {
//                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('frontend', 'Asuransi Lain') ?>
                    <?php echo isset($modelAsuransiLain->pegawaiKeluarga) ? Html::a(Yii::t('frontend', 'Update'), ['pegawai-profile/form-keluarga-asuransi', 'id' => $modelAsuransiLain->pegawaiKeluarga->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Update data Asuransi Kesehatan ke {nama}', ['nama' => $modelAsuransiLain->pegawaiKeluarga->nama])]]) : '';
                    ?></h3>
            </div>
            <div class="box-body no-padding">
                <?php
                $gridColumnKeluargaAsuransiLain = [
                    [
                        'label' => Yii::t('frontend', 'Nama'),
                        'value' => function (PegawaiAsuransiLain $model) {
                            return $model->nama_;
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Nomor'),
                        'value' => function (PegawaiAsuransiLain $model) {
                            return $model->nomor;
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Tanggal Kepesertaan'),
                        'value' => function (PegawaiAsuransiLain $model) {
                            return $model->tanggal_kepesertaan ? date('d-m-Y', strtotime($model->tanggal_kepesertaan)) : '-';
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Catatan'),
                        'value' => function (PegawaiAsuransiLain $model) {
                            return $model->catatan;
                        }
                    ],
                ];
                try {
                    echo DetailView::widget([
                        'id' => 'grid-data-keluarga-asuransi-lain',
                        'model' => $modelAsuransiLain,
                        'attributes' => $gridColumnKeluargaAsuransiLain,
                        'options' => ['class' => 'table table-striped table-condensed detail-view'],
                    ]);
                } catch (Exception $e) {
//                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
    </div>
</div>
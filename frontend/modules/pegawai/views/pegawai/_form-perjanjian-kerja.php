<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 15/11/2018
 * Time: 21:41
 */


use common\components\Status;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\PerjanjianKerjaJenis;
use frontend\modules\pegawai\Module;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model PerjanjianKerja */
/**
 * @var $modelPegawai Pegawai
 */


$js = <<< JS
$(function() {
    $('#form-perjanjian-kerja').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
                // location.reload();
            }
        });
        return false;
    });
    
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create-perjanjian-kerja', 'id' => $modelPegawai->id]) : Url::to(['update-perjanjian-kerja', 'id' => $model->id]);
$validation = Url::to(['form-perjanjian-kerja-validation']);
?>

<div class="perjanjian-kerja-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-perjanjian-kerja',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-3">
                <?php
                $aksi = Module::aksi();
                unset($aksi[Module::ROTASI]);
                echo $form->field($model, 'aksi')->dropDownList($aksi);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php
                echo $form->field($model, 'jenis_perjanjian')->dropDownList(ArrayHelper::map(PerjanjianKerjaJenis::find()->all(), 'id', 'nama'), ['prompt' => $model->getAttributeLabel('jenis_perjanjian')]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php
                echo $form->field($model, 'id_pegawai')->textInput(['placeholder' => $model->getAttributeLabel('id_pegawai')]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'dasar_masa_kerja')->widget(SwitchInput::class, [
                    'pluginOptions' =>
                        [
                            'threeState' => false,
                            'onText' => Status::activeInactive(Status::YA),
                            'offText' => Status::activeInactive(Status::TIDAK)
                        ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php
                echo $form->field($model, 'kontrak')->dropDownList($model->_kontrak, ['prompt' => $model->getAttributeLabel('kontrak')]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'dasar_cuti')->widget(SwitchInput::class, [
                    'pluginOptions' =>
                        [
                            'threeState' => false,
                            'onText' => Status::activeInactive(Status::YA),
                            'offText' => Status::activeInactive(Status::TIDAK)
                        ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'mulai_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->mulai_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'akhir_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->akhir_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ])->hint(Yii::t('frontend', '*) Jika akhir berlaku diisi, maka data sudah tidak dapat diedit.'), ['class' => 'text-danger small mark']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $form->field($model, 'keterangan')->textarea(['raw' => 5, 'placeholder' => !empty($sistemKerja) ? $sistemKerja->keterangan : $model->getAttributeLabel('keterangan')]);
                ?>
            </div>
        </div>


        <?php

        echo $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput();
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>


        <hr>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
        </div>

        <?php

    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

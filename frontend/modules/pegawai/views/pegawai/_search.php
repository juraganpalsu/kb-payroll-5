<?php

use common\models\SistemKerja;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\PegawaiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-pegawai-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php
    try {
        ?>

        <div class="row">
            <div class="col-md-5">
                <?php
                echo $form->field($model, 'sistem_kerja')->widget(Select2::class, [
                    'data' => ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => $model->getAttributeLabel('sistem_kerja'), 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

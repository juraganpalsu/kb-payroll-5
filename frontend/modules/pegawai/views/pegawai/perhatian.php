<?php
/**
 * Created by PhpStorm
 *
 * File Name: perhatian.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 24/07/19
 * Time: 23:32
 */

?>

<h2><strong class="text-danger">Perhatian</strong></h2>
<p>Seiring bertambahnya fitur, akun anda diwajibkan terhubung dengan data karyawan agar dapat menggunakan
    semua fitur yg tersedia. Data karyawanpun harus mempunyai perjanjian kerja yg AKTIF dan Alokasi Struktur yg
    AKTIF pula.</p>

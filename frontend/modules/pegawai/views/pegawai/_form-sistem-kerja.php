<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 15/11/2018
 * Time: 21:41
 */


use common\models\Pegawai;
use common\models\SistemKerja;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PegawaiSistemKerja */
/* @var $form kartik\widgets\ActiveForm */
/* @var $modelPegawai Pegawai */


$js = <<< JS
$(function() {
    $('#form-sistem-kerja').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
                // location.reload();
            }
        });
        return false;
    });
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create-sistem-kerja', 'id' => $model->pegawai_id]) : Url::to(['update-sistem-kerja', 'id' => $model->id]);
$validation = $model->isNewRecord ? Url::to(['form-sistem-kerja-validation', 'id' => $model->pegawai_id]) : Url::to(['form-sistem-kerja-validation', 'id' => $model->id, 'isnew' => false]);
$sistemKerja = $modelPegawai->pegawaiSistemKerja;
?>

<div class="sistem-kerja-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-sistem-kerja',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>


        <div class="row">
            <div class="col-md-10">
                <?php
                echo $form->field($model, 'sistem_kerja_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->sistemKerja->nama : $model->getAttributeLabel('sistem_kerja'), 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'mulai_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->mulai_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'akhir_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->akhir_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ])->hint(Yii::t('frontend', '*) Jika akhir berlaku diisi, maka data sudah tidak dapat diedit.'), ['class' => 'text-danger small mark']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'mulai_libur')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ])->hint(Yii::t('frontend', '*) Dihitung dari hari pertama mulai berlaku sistem kerjanya(default : Mulai Berlaku).'), ['class' => 'text-danger small mark']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $form->field($model, 'keterangan')->textarea(['raw' => 5, 'placeholder' => !empty($sistemKerja) ? $sistemKerja->keterangan : $model->getAttributeLabel('keterangan')]);
                ?>
            </div>
        </div>


        <?php
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>


        <hr>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

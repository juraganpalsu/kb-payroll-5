<?php
/**
 * Created by PhpStorm
 *
 * File Name: view-new.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 18/07/19
 * Time: 22:42
 */

use common\components\Status;
use common\models\PegawaiSistemKerja;
use frontend\modules\izin\models\CutiSaldo;
use frontend\modules\pegawai\models\AkunBeatroute;
use frontend\modules\pegawai\models\AreaKerja;
use frontend\modules\pegawai\models\CostCenter;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiAsuransiLain;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiFiles;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiKeluarga;
use frontend\modules\pegawai\models\PegawaiKontakDarurat;
use frontend\modules\pegawai\models\PegawaiPendidikan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\UserPegawai;
use frontend\modules\pegawai\Module;
use frontend\modules\personalia\models\Inventaris;
use frontend\modules\personalia\models\SuratIzinMengemudi;
use frontend\modules\personalia\models\SuratPeringatan;
use frontend\modules\personalia\models\TrainingDetail;
use kartik\grid\GridView;
use kartik\popover\PopoverX;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use mdm\admin\components\Helper;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model Pegawai
 * @var $providerSistemKerja ActiveDataProvider
 * @var $providerAbsensi ActiveDataProvider
 * @var $providerAttendance ActiveDataProvider
 * @var $providerStruktur ActiveDataProvider
 * @var $providerPerjanjianKerja ActiveDataProvider
 * @var $providerAkunBeatroute ActiveDataProvider
 * @var $providerCostCenter ActiveDataProvider
 * @var $providerPegawaiGolongan ActiveDataProvider
 * @var ActiveDataProvider $providerPendidikan
 * @var ActiveDataProvider $providerBpjs
 * @var ActiveDataProvider $providerKeluarga
 * @var ActiveDataProvider $providerKontakDarurat
 * @var ActiveDataProvider $providerAreaKerja
 * @var ActiveDataProvider $providerInventaris
 * @var ActiveDataProvider $providerSim
 * @var ActiveDataProvider $providerSuratPeringatan
 * @var ActiveDataProvider $providerTraining
 * @var ActiveDataProvider $providerCutiSaldo
 * @var ActiveDataProvider $providerFiles
 *
 *
 *
 */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$jsSoruce = <<<JS
$('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.disalokasi-struktur').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('#form-assign-user').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.btn-submit-assign');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });
    
    $('.delete-assign-user-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.btn-set-status').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.change-default-masa-kerja-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        $.pjax.reload({container: '#kv-pjax-container-perjanjian-kerja', async: true});
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.change-default-cuti-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        $.pjax.reload({container: '#kv-pjax-container-perjanjian-kerja', async: true});
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
JS;

$js = <<<JS
$(function() {
    $jsSoruce;
    $("#kv-pjax-container-perjanjian-kerja").on("pjax:success", function() {
        $jsSoruce;
    });
});
JS;

$this->registerJs($js);

$css = <<<CSS
.box-profile-picture {
  position: relative;
  left: 25%;
  width: 50%;
}

.profile-user-img {
  display: block;
  width: 100%;
  height: auto;
}

.overlay-edit {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: #008CBA;
}

.box-profile-picture:hover .overlay-edit {
  opacity: 1;
}

.text-edit {
  color: white;
  font-size: 20px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
}
CSS;

$this->registerCss($css);

$modelPerjanjianKerja = $model->perjanjianKerja;
$modelPerjanjianKerjaTerakhir = $model->getPerjanjianKerjaTerakhir();
$modelGolonganTerakhir = $model->getGolonganTerakhir();
$modelStrukturTerakhir = $model->getStrukturTerakhir();
?>

<!-- Main content -->
<section class="content small">

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="box-profile-picture">
                        <?php
                        $fotoProfil = $model->upload_dir . $model->id . '.jpg';
                        if (file_exists($fotoProfil)) {
                            echo Html::img('@web/' . $model->upload_dir . $model->id . '.jpg', ['class' => 'profile-user-img img-responsive img-circle', 'alt' => Yii::t('frontend', 'User profile picture')]);
                        } else {
                            echo Html::img('@web/pictures/user8-128x128.jpg', ['class' => 'profile-user-img img-responsive img-circle', 'alt' => Yii::t('frontend', 'User profile picture')]);
                        } ?>
                        <div class="overlay-edit">
                            <?= Html::a(Yii::t('frontend', 'Update') . '<i class="fa fa-pencil margin-r-5"></i>', ['pegawai-profile/update-foto', 'pegawaiid' => $model->id], ['class' => 'text-edit call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Foto Profil')]]); ?>
                        </div>
                    </div>
                    <h3 class="profile-username text-center"><?= $model->nama ?></h3>

                    <p class="text-center text-bold text-danger"><?= $modelPerjanjianKerja ? $modelPerjanjianKerja->id_pegawai : $modelPerjanjianKerjaTerakhir ? $modelPerjanjianKerjaTerakhir->id_pegawai : '' ?></p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b><?= Yii::t('frontend', 'Struktur'); ?></b> <a class="pull-right"><?php
                                if ($model->pegawaiStruktur) {
                                    echo $model->pegawaiStruktur->struktur->name;
                                } elseif ($modelStrukturTerakhir) {
                                    echo $modelStrukturTerakhir->struktur->name;
                                } ?></a>
                            <p></p>
                        </li>

                        <li class="list-group-item">
                            <b><?= Yii::t('frontend', 'Perjanjian Kerja'); ?></b> <a class="pull-right"><?php
                                if ($modelPerjanjianKerja) {
                                    echo $modelPerjanjianKerja->jenis->nama;
                                } elseif ($modelPerjanjianKerjaTerakhir) {
                                    echo $modelPerjanjianKerjaTerakhir->jenis->nama;
                                }
                                ?></a>
                            <p></p>
                        </li>
                        <li class="list-group-item">
                            <b><?= Yii::t('frontend', 'Jabatan'); ?></b> <a class="pull-right"><?php
                                if ($model->pegawaiStruktur) {
                                    if ($model->pegawaiStruktur->struktur->jabatan) {
                                        echo $model->pegawaiStruktur->struktur->jabatan->nama;
                                    }
                                } elseif ($modelStrukturTerakhir) {
                                    if ($modelStrukturTerakhir->struktur->jabatan) {
                                        echo $modelStrukturTerakhir->struktur->jabatan->nama;
                                    }
                                }

                                ?></a>
                            <p></p>
                        </li>
                        <li class="list-group-item">
                            <b><?= Yii::t('frontend', 'Golongan'); ?></b> <a class="pull-right"><?php
                                if ($pegawaiGolongan = $model->pegawaiGolongan) {
                                    echo $pegawaiGolongan->golongan->nama;
                                } elseif ($modelGolonganTerakhir) {
                                    echo $modelGolonganTerakhir->golongan->nama;
                                }
                                ?></a>
                            <p></p>
                        </li>
                    </ul>

                    <?php
                    if (Helper::checkRoute('/pegawai/pegawai/set-ess-status')) {
                        if ($model->ess_status) {
                            echo Html::a(Yii::t('frontend', 'Ess Status(Aktif)'), ['set-ess-status', 'id' => $model->id], [
                                'class' => 'btn btn-warning btn-flat btn-xs btn-block btn-set-status',
                                'data' => [
                                    'confirm' => Yii::t('frontend', 'Yakin akan menonaktifkan Ess untuk pegawai {pegawai}?', ['pegawai' => $model->nama]),
                                    'method' => 'post',
                                ],
                            ]);
                        } else {
                            echo Html::a(Yii::t('frontend', 'Ess Status(Tidak Aktif)'), ['set-ess-status', 'id' => $model->id], [
                                'class' => 'btn btn-primary btn-flat btn-xs btn-block btn-set-status',
                                'data' => [
                                    'confirm' => Yii::t('frontend', 'Yakin akan mengaktfkan Ess untuk pegawai {pegawai}?', ['pegawai' => $model->nama]),
                                    'method' => 'post',
                                ],
                            ]);
                        }
                    } else {
                        $btnEssStatus = $model->ess_status ? Yii::t('frontend', 'Ess Status(Aktif)') : Yii::t('frontend', 'Ess Status(Tidak Aktif)');
                        echo Html::a($btnEssStatus, '#', [
                            'class' => 'btn btn-primary btn-flat btn-xs btn-block disabled',
                        ]);
                    }
                    ?>

                    <?php

                    if ($model->userPegawai) {
                        $username = $model->userPegawai->user->username;
                        if (Helper::checkRoute('/pegawai/pegawai/delete-assign-to-user')) {
                            echo Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-link']) . ' ' . Html::tag('strong', $username), ['delete-assign-to-user', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-flat btn-xs btn-block delete-assign-user-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan disalokasi user <strong>{user}</strong> dari pegawai <strong>{pegawai}</strong> ?', ['user' => $username, 'pegawai' => $model->nama])]]);
                        } else {
                            echo Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-link']) . ' ' . Html::tag('strong', $username), '#', ['class' => 'btn btn-success btn-flat btn-xs btn-block']);
                        }
                    } else {
                        if (Helper::checkRoute('/pegawai/pegawai/assign-to-user')) {
                            PopoverX::begin([
                                'size' => PopoverX::SIZE_MEDIUM,
                                'placement' => PopoverX::ALIGN_BOTTOM,
                                'toggleButton' => ['label' => Yii::t('app', 'Link Ke User'), 'class' => 'btn btn-primary btn-flat btn-xs btn-block'],
                                'header' => '<i class="glyphicon glyphicon-lock"></i>' . Yii::t('frontend', 'Link To User'),
                                'footer' => Html::button('Submit', [
                                    'class' => 'btn btn-sm btn-primary btn-submit-assign',
                                    'onclick' => '$("#form-assign-user").trigger("submit")'
                                ])
                            ]);

                            $modelUserPegawai = new UserPegawai();

                            $form = ActiveForm::begin([
                                'id' => 'form-assign-user',
                                'action' => Url::to(['assign-to-user']),
                                'formConfig' => ['showErrors' => false],
                                'enableAjaxValidation' => true,
                                'validationUrl' => Url::to(['assign-to-user-validation']),
                                'fieldConfig' => ['showLabels' => false],
                                'options' => ['id' => 'form-assign-user']
                            ]);
                            try {
                                echo $form->field($modelUserPegawai, 'user_id')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('frontend', 'Select User')],
                                    'data' => [],
                                    'maintainOrder' => true,
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'minimumInputLength' => 3,
                                        'language' => [
                                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                        ],
                                        'ajax' => [
                                            'url' => Url::to(['/helper/get-user-select2']),
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                        ],
                                    ],
                                ]);

                                echo $form->field($modelUserPegawai, 'pegawai_id')->hiddenInput(['value' => $model->id]);

                            } catch (Exception $e) {
                                Yii::info($e->getMessage(), 'exception');
                            }
                            ActiveForm::end();
                            PopoverX::end();
                        } else {
                            echo Html::a(Yii::t('app', 'Link Ke User'), '#', ['class' => 'btn btn-primary btn-flat btn-xs btn-block']);
                        }
                    }
                    ?>

                    <?= Helper::checkRoute('/pegawai/pegawai/delete') ? Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger btn-flat btn-xs btn-block',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) : Html::a(Yii::t('app', 'Delete'), '#', ['class' => 'btn btn-danger btn-flat btn-xs btn-block disabled']);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?= Yii::t('frontend', 'Lain-lain'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <strong><i class="fa fa-credit-card margin-r-5"></i> <?php
                                $linkUpdate = Html::a(Yii::t('frontend', 'NPWP') . '<i class="fa fa-pencil margin-r-5"></i>', ['pegawai-profile/pegawai-npwp-form', 'pegawaiid' => $model->id], ['class' => 'call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'NPWP')]]);
                                $disableUpdate = Html::a(Yii::t('frontend', 'NPWP'), '#');
                                if ($model->userPegawai) {
                                    $pId = '';
                                    if ($userPegawai = UserPegawai::findOne(['user_id' => Yii::$app->user->id])) {
                                        $pId = $userPegawai->pegawai_id;
                                    }
                                    if ($model->pegawaiNpwp && $model->id == $pId) {
                                        echo $disableUpdate;
                                    } else {
                                        echo $linkUpdate;
                                    }
                                } else {
                                    echo $disableUpdate;
                                }
                                ?>
                            </strong>


                            <p class="text">
                                <?php
                                if ($modelPegawaiNpwp = $model->pegawaiNpwp) {
                                    ?>
                                    <?= Yii::t('frontend', 'Nomor:'); ?>
                                    <br>
                                    <?= substr($modelPegawaiNpwp->nomor, 0, 2) . '.' . substr($modelPegawaiNpwp->nomor, 2, 3) . '.' . substr($modelPegawaiNpwp->nomor, 5, 3) . '.' . substr($modelPegawaiNpwp->nomor, 8, 1) . '-' . substr($modelPegawaiNpwp->nomor, 9, 3) . '.' . substr($modelPegawaiNpwp->nomor, 12, 3); ?>
                                    <br>
                                    <br>
                                    <?= Yii::t('frontend', 'Catatan:'); ?>
                                    <br>
                                    <?= $modelPegawaiNpwp->catatan;
                                }
                                ?>
                            </p>

                            <hr>
                            <strong><i class="fa fa-plus margin-r-5"></i> <?php
                                $linkUpdate = Html::a(Yii::t('frontend', 'Asuransi Lain') . '<i class="fa fa-pencil margin-r-5"></i>', ['pegawai-profile/pegawai-asuransi-form', 'pegawaiid' => $model->id], ['class' => 'call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Asuransi Lain')]]);
                                $disableUpdate = Html::a(Yii::t('frontend', 'Asuransi Lain'), '#');
                                if ($model->userPegawai) {
                                    $pId = '';
                                    if ($userPegawai = UserPegawai::findOne(['user_id' => Yii::$app->user->id])) {
                                        $pId = $userPegawai->pegawai_id;
                                    }
                                    if ($model->pegawaiAsuransiLain && $model->id == $pId) {
                                        echo $disableUpdate;
                                    } else {
                                        echo $linkUpdate;
                                    }
                                } else {
                                    echo $disableUpdate;
                                }
                                ?>
                            </strong>


                            <p class="text">
                                <?php
                                if ($pegawaiAsuransiLain = $model->pegawaiAsuransiLain) {
                                    ?>
                                    <?= Yii::t('frontend', 'Nama:'); ?>
                                    <br>
                                    <?= $pegawaiAsuransiLain->nama_; ?>
                                    <br>
                                    <br>
                                    <?= Yii::t('frontend', 'Nomor:'); ?>
                                    <br>
                                    <?= $pegawaiAsuransiLain->nomor; ?>
                                    <br>
                                    <br>
                                    <?= Yii::t('frontend', 'Tanggal Kepesertaan:'); ?>
                                    <br>
                                    <?= date('d-m-Y', strtotime($pegawaiAsuransiLain->tanggal_kepesertaan)); ?>
                                    <br>
                                    <br>
                                    <?= Yii::t('frontend', 'Catatan:'); ?>
                                    <br>
                                    <?= $pegawaiAsuransiLain->catatan;
                                }
                                ?>
                            </p>


                            <hr>
                            <strong><i class="fa fa-usd  margin-r-5"></i> <?php
                                $linkUpdate = Html::a(Yii::t('frontend', 'Akun Bank') . '<i class="fa fa-pencil margin-r-5"></i>', ['pegawai-profile/pegawai-bank-form', 'pegawaiid' => $model->id], ['class' => 'call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Akun Bank')]]);
                                $disableUpdate = Html::a(Yii::t('frontend', 'Akun Bank'), '#');
                                if ($model->userPegawai) {
                                    $pId = '';
                                    if ($userPegawai = UserPegawai::findOne(['user_id' => Yii::$app->user->id])) {
                                        $pId = $userPegawai->pegawai_id;
                                    }
                                    if ($model->pegawaiBank && $model->id == $pId) {
                                        echo $disableUpdate;
                                    } else {
                                        echo $linkUpdate;
                                    }
                                } else {
                                    echo $disableUpdate;
                                }
                                ?>
                            </strong>


                            <p class="text">
                                <?php
                                if ($pegawaiBank = $model->pegawaiBank) {
                                    ?>
                                    <?= Yii::t('frontend', 'Nama Bank:'); ?>
                                    <br>
                                    <?= $pegawaiBank->namaBank; ?>
                                    <br>
                                    <br>
                                    <?= Yii::t('frontend', 'Nomor Rekening:'); ?>
                                    <br>
                                    <?= $pegawaiBank->nomer_rekening; ?>
                                    <br>
                                    <br>
                                    <?= Yii::t('frontend', 'Catatan:'); ?>
                                    <br>
                                    <?= $pegawaiBank->catatan;
                                }
                                ?>
                            </p>

                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#sistem-kerja-tab"
                                          data-toggle="tab"><?= Yii::t('frontend', 'Sistem Kerja') ?></a></li>
                    <li><a href="#perjanjian-kerja-tab"
                           data-toggle="tab"><?= Yii::t('frontend', 'Perjanjian Kerja') ?></a></li>
                    <li><a href="#pegawai-golongan-tab" data-toggle="tab"><?= Yii::t('frontend', 'Golongan') ?></a></li>
                    <li><a href="#struktur-tab" data-toggle="tab"><?= Yii::t('frontend', 'Struktur') ?></a></li>
                    <li><a href="#akun-beatroute-tab" data-toggle="tab"><?= Yii::t('frontend', 'Akun Beatroute') ?></a>
                    </li>
                    <li><a href="#cost-center-tab" data-toggle="tab"><?= Yii::t('frontend', 'Cost Center') ?></a></li>
                    <li><a href="#area-kerja-tab" data-toggle="tab"><?= Yii::t('frontend', 'Area Kerja') ?></a></li>
                    <li><a href="#inventaris-tab" data-toggle="tab"><?= Yii::t('frontend', 'Inventaris') ?></a></li>
                    <li><a href="#surat-peringatan-tab"
                           data-toggle="tab"><?= Yii::t('frontend', 'Surat Peringatan') ?></a></li>
                    <li><a href="#training-tab" data-toggle="tab"><?= Yii::t('frontend', 'Training') ?></a></li>
                    <?php if (Helper::checkRoute('/pegawai/pegawai/index')) { ?>
                        <li><a href="#cuti-saldo-tab" data-toggle="tab"><?= Yii::t('frontend', 'Saldo Cuti') ?></a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="sistem-kerja-tab">
                        <?php
                        $gridColumnSistemKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            'sistemKerja.nama',
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (PegawaiSistemKerja $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (PegawaiSistemKerja $model) {
                                    return is_null($model->akhir_berlaku) ? '' : date('d-m-Y', strtotime($model->akhir_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'mulai_libur',
                                'value' => function (PegawaiSistemKerja $model) {
                                    return is_null($model->mulai_libur) ? '' : date('d-m-Y', strtotime($model->mulai_libur));
                                }
                            ],
                            'keterangan',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-sistem-kerja}',
                                'buttons' => [
                                    'update-sistem-kerja' => function ($url, PegawaiSistemKerja $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/update-sistem-kerja')) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-sistem-kerja', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Sistem Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            $cekAkhirBerlakuSistemKerja = true;
                            if ($cekSistemKerja = $model->pegawaiSistemKerja) {
                                if (empty($cekSistemKerja->akhir_berlaku)) {
                                    $cekAkhirBerlakuSistemKerja = false;
                                }
                            }
                            $tombolTambahSistemKerja = Html::a(Yii::t('frontend', 'Tambahkan Sistem Kerja'), ['create-sistem-kerja', 'id' => $model->id],
                                [
                                    'class' => 'btn btn-xs btn-success btn-flat call-modal-btn',
                                    'data' => ['header' => Yii::t('frontend', '{textHeader} {nama}', ['textHeader' => Yii::t('frontend', 'Atur sistem kerja'), 'nama' => $model->nama])]
                                ]
                            );

                            echo Gridview::widget([
                                'id' => 'grid-sistem-kerja',
                                'dataProvider' => $providerSistemKerja,
                                'columns' => $gridColumnSistemKerja,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-sistem-kerja']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Sistem Kerja')),
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Helper::checkRoute('/pegawai/pegawai/create-sistem-kerja') && $cekAkhirBerlakuSistemKerja ? $tombolTambahSistemKerja : ''
                                    ],
                                ],
                                'export' => false,
                                'bordered' => false,
                                'placeFooterAfterBody' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>


                    <div class="tab-pane" id="perjanjian-kerja-tab">
                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'class' => 'kartik\grid\ExpandRowColumn',
                                'width' => '50px',
                                'value' => function () {
                                    return GridView::ROW_COLLAPSED;
                                },
                                'detail' => function (PerjanjianKerja $model) {
                                    $providerStruktur = new ArrayDataProvider([
                                        'allModels' => $model->pegawaiStrukturs,
                                    ]);
                                    return $this->render('_perjanjian-detail', ['model' => $model, 'providerStruktur' => $providerStruktur]);
                                },
                                'enableRowClick' => true,
                                'headerOptions' => ['class' => 'kartik-sheet-style'],
                                'expandOneOnly' => true
                            ],
                            [
                                'attribute' => 'aksi',
                                'value' => function (PerjanjianKerja $model) {
                                    return Module::getAksi($model->aksi);
                                },
                            ],
                            [
                                'attribute' => 'jenis_perjanjian',
                                'value' => function (PerjanjianKerja $model) {
                                    if ($model->pegawaiStrukturs) {
                                        return $model->jenis->nama;
                                    }
                                    return Html::tag('strong', $model->jenis->nama, ['style' => 'color:red']);
                                },
                                'format' => 'raw'
                            ],
                            'id_pegawai',
                            [
                                'attribute' => 'kontrak',
                                'value' => function (PerjanjianKerja $model) {
                                    if ($model->pegawaiStrukturs) {
                                        return $model->_kontrak[$model->kontrak];
                                    }
                                    return Html::tag('strong', $model->_kontrak[$model->kontrak], ['style' => 'color:red']);
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (PerjanjianKerja $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (PerjanjianKerja $model) {
                                    return is_null($model->akhir_berlaku) ? '' : date('d-m-Y', strtotime($model->akhir_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'tanggal_resign',
                                'value' => function (PerjanjianKerja $model) {
                                    return is_null($model->tanggal_resign) ? '' : date('d-m-Y', strtotime($model->tanggal_resign));
                                }
                            ],
                            [
                                'attribute' => 'dasar_cuti',
                                'value' => function (PerjanjianKerja $model) {
                                    if ($model->dasar_cuti) {
                                        return Html::a(Status::activeInactive($model->dasar_cuti), ['change-default-cuti', 'id' => $model->id, 'status' => Status::TIDAK], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-default-cuti-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan membatalkan default Cuti ?')]]);
                                    }
                                    return Html::a(Status::activeInactive($model->dasar_cuti), ['change-default-cuti', 'id' => $model->id, 'status' => Status::YA], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-default-cuti-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan menjadikan default Cuti ?')]]);
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'dasar_masa_kerja',
                                'value' => function (PerjanjianKerja $model) {
                                    if ($model->dasar_masa_kerja) {
                                        return Html::a(Status::activeInactive($model->dasar_masa_kerja), ['change-default-mk', 'id' => $model->id, 'status' => Status::TIDAK], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-default-masa-kerja-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan membatalkan default masa kerja ?')]]);
                                    }
                                    return Html::a(Status::activeInactive($model->dasar_masa_kerja), ['change-default-mk', 'id' => $model->id, 'status' => Status::YA], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-default-masa-kerja-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan menjadikan default masa kerja ?')]]);
                                },
                                'format' => 'raw'
                            ],
                            'keterangan',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-perjanjian-kerja} {delete-perjanjian-kerja}',
                                'buttons' => [
                                    'update-perjanjian-kerja' => function ($url, PerjanjianKerja $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/update-perjanjian-kerja')) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-perjanjian-kerja', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-perjanjian-kerja' => function ($url, PerjanjianKerja $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/delete-perjanjian-kerja')) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-perjanjian-kerja', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            $cekAkhirBerlakuPerjanjianKerja = true;
                            if ($cekPerjanjianKerja = $model->perjanjianKerja) {
                                if (empty($cekPerjanjianKerja->akhir_berlaku)) {
                                    $cekAkhirBerlakuPerjanjianKerja = false;
                                }
                            }
                            echo Gridview::widget([
                                'id' => 'grid-perjanjian-kerja',
                                'dataProvider' => $providerPerjanjianKerja,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-perjanjian-kerja']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Perjanjian Kerja')),
                                ],
                                'toolbar' => [
                                    [
                                        'content' =>
                                            Helper::checkRoute('/pegawai/pegawai/create-perjanjian-kerja') && $cekAkhirBerlakuPerjanjianKerja ? Html::a(Yii::t('frontend', 'Buat Perjanjian Kerja'), ['create-perjanjian-kerja', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Buat Perjanjian Kerja Untuk {nama}', ['nama' => $model->nama])]]) : ''
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false,
                                'placeFooterAfterBody' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>


                    <div class="tab-pane" id="pegawai-golongan-tab">
                        <?php
                        $gridColumnPegawaiGolongan = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'aksi',
                                'value' => function (PegawaiGolongan $model) {
                                    return Module::getAksi($model->aksi);
                                },
                            ],
                            [
                                'attribute' => 'golongan_id',
                                'value' => function (PegawaiGolongan $model) {
                                    return $model->golongan->nama;
                                }
                            ],
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (PegawaiGolongan $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (PegawaiGolongan $model) {
                                    return is_null($model->akhir_berlaku) ? '' : date('d-m-Y', strtotime($model->akhir_berlaku));
                                }
                            ],
                            'keterangan',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{edit} {delete}',
                                'buttons' => [
                                    'edit' => function ($url, PegawaiGolongan $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/update-pegawai-golongan')) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-pegawai-golongan', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Golongan {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete' => function ($url, PegawaiGolongan $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/delete-pegawai-golongan')) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-pegawai-golongan', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            $cekAkhirBerlakuGolongan = true;
                            if ($cekPegawaiGolongan = $model->pegawaiGolongan) {
                                if (empty($cekPegawaiGolongan->akhir_berlaku)) {
                                    $cekAkhirBerlakuGolongan = false;
                                }
                            }
                            echo Gridview::widget([
                                'id' => 'grid-cost-center',
                                'dataProvider' => $providerPegawaiGolongan,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai-golongan']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Golongan')),
                                ],
                                'toolbar' => [
                                    [
                                        'content' =>
                                            Helper::checkRoute('/pegawai/pegawai/create-pegawai-golongan') && $cekAkhirBerlakuGolongan ? Html::a(Yii::t('frontend', 'Tambahkan Golongan'), ['create-pegawai-golongan', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Tambahkan Golongan Untuk {nama}', ['nama' => $model->nama])]]) : ''
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPegawaiGolongan,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>


                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="struktur-tab">
                        <?php
                        $gridColumnAlokasiStruktur = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'aksi',
                                'value' => function (PegawaiStruktur $model) {
                                    return Module::getAksi($model->aksi);
                                },
                            ],
                            [
                                'attribute' => 'struktur_id',
                                'value' => function (PegawaiStruktur $model) {
                                    return $model->struktur->nameCostume;
                                }
                            ],
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (PegawaiStruktur $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (PegawaiStruktur $model) {
                                    return is_null($model->akhir_berlaku) ? '' : date('d-m-Y', strtotime($model->akhir_berlaku));
                                }
                            ],
                            'keterangan',
                            [
                                'label' => Yii::t('frontend', 'Atasan Langsung'),
                                'value' => function (PegawaiStruktur $model) {
                                    if ($queryAtasanLangsung = $model->struktur->parents(1)->one()) {
                                        return $queryAtasanLangsung->nameCostume;
                                    }
                                    return '';
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{edit} {delete}',
                                'buttons' => [
                                    'edit' => function ($url, PegawaiStruktur $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/update-alokasi-kestruktur')) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-alokasi-kestruktur', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Struktur {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete' => function ($url, PegawaiStruktur $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/delete-alokasi-kestruktur')) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-alokasi-kestruktur', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            $cekAkhirBerlakuStruktur = true;
                            if ($cekPegawaiStruktur = $model->pegawaiStruktur) {
                                if (empty($cekPegawaiStruktur->akhir_berlaku)) {
                                    $cekAkhirBerlakuStruktur = false;
                                }
                            }
                            echo Gridview::widget([
                                'id' => 'grid-alokasi-struktur',
                                'dataProvider' => $providerStruktur,
                                'columns' => $gridColumnAlokasiStruktur,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-alokasi-struktur']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Daftar Struktur Pegawai')),
                                ],
                                'toolbar' => [
                                    [
                                        'content' =>
                                            Helper::checkRoute('/pegawai/pegawai/create-alokasi-kestruktur') && $cekAkhirBerlakuStruktur ? Html::a(Yii::t('app', 'Masukkan Ke Struktur'), ['create-alokasi-kestruktur', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Masukkan Struktur {nama}', ['nama' => $model->nama])]]) : ''
                                    ],
                                ],
                                'export' => false,
                                'bordered' => false,
                                'placeFooterAfterBody' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="akun-beatroute-tab">
                        <?php
                        $gridColumnAkunBeatroute = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            'beatroute_id',
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (AkunBeatroute $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (AkunBeatroute $model) {
                                    return is_null($model->akhir_berlaku) ? '' : date('d-m-Y', strtotime($model->akhir_berlaku));
                                }
                            ],
                            'keterangan',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{edit} {delete}',
                                'buttons' => [
                                    'edit' => function ($url, AkunBeatroute $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/update-akun-beatroute')) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-akun-beatroute', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Akun Beatroute {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete' => function ($url, AkunBeatroute $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/delete-akun-beatroute')) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-akun-beatroute', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-akun-beatroute',
                                'dataProvider' => $providerAkunBeatroute,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-beatroute']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Akun Beatroute')),
                                ],
                                'toolbar' => [
                                    [
                                        'content' =>
                                            Helper::checkRoute('/pegawai/pegawai/create-akun-beatroute') ? Html::a(Yii::t('frontend', 'Buat Akun Beatroute'), ['create-akun-beatroute', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Buat Akun Beatroute Untuk {nama}', ['nama' => $model->nama])]]) : ''
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnAkunBeatroute,
                                'bordered' => false,
                                'placeFooterAfterBody' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>


                    <div class="tab-pane" id="cost-center-tab">
                        <?php
                        $gridColumnCostCenter = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'cost_center_template_id',
                                'value' => function (CostCenter $model) {
                                    return $model->costCenterTemplate->kodeArea;
                                }
                            ],
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (CostCenter $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (CostCenter $model) {
                                    return is_null($model->akhir_berlaku) ? '' : date('d-m-Y', strtotime($model->akhir_berlaku));
                                }
                            ],
                            'keterangan',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{edit} {delete}',
                                'buttons' => [
                                    'edit' => function ($url, CostCenter $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/update-cost-center')) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-cost-center', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Cost Center {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete' => function ($url, CostCenter $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/delete-cost-center')) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-cost-center', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            $cekAkhirBerlakuCostCenter = true;
                            if ($cekPegawaiCostCenter = $model->costCenter) {
                                if (empty($cekPegawaiCostCenter->akhir_berlaku)) {
                                    $cekAkhirBerlakuCostCenter = false;
                                }
                            }
                            echo Gridview::widget([
                                'id' => 'grid-cost-center',
                                'dataProvider' => $providerCostCenter,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-cost-center']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Cost Center')),
                                ],
                                'toolbar' => [
                                    [
                                        'content' =>
                                            Helper::checkRoute('/pegawai/pegawai/create-cost-center') && $cekAkhirBerlakuCostCenter ? Html::a(Yii::t('frontend', 'Buat Cost Center'), ['create-cost-center', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Buat Cost Center Untuk {nama}', ['nama' => $model->nama])]]) : ''
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnCostCenter,
                                'bordered' => false,
                                'placeFooterAfterBody' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>


                    <div class="tab-pane" id="area-kerja-tab">
                        <?php
                        $gridColumnAreaKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'ref_area_kerja_id',
                                'value' => function (AreaKerja $model) {
                                    return $model->refAreaKerja->getBreadCrumbs(100, '-', '', '');
                                }
                            ],
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (AreaKerja $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                }
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (AreaKerja $model) {
                                    return is_null($model->akhir_berlaku) ? '' : date('d-m-Y', strtotime($model->akhir_berlaku));
                                }
                            ],
                            'keterangan',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{edit} {delete}',
                                'buttons' => [
                                    'edit' => function ($url, AreaKerja $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/update-area-kerja')) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-area-kerja', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Cost Center {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete' => function ($url, AreaKerja $model) {
                                        if (empty($model->akhir_berlaku) && Helper::checkRoute('/pegawai/pegawai/delete-area-kerja')) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-area-kerja', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            $cekAkhirBerlakuAreaKerja = true;
                            if ($cekPegawaiAreaKerja = $model->areaKerja) {
                                if (empty($cekPegawaiAreaKerja->akhir_berlaku)) {
                                    $cekAkhirBerlakuAreaKerja = false;
                                }
                            }
                            echo Gridview::widget([
                                'id' => 'grid-area-kerja',
                                'dataProvider' => $providerAreaKerja,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-area-kerja']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Area Kerja')),
                                ],
                                'toolbar' => [
                                    [
                                        'content' =>
                                            $cekAkhirBerlakuAreaKerja ? Html::a(Yii::t('frontend', 'Buat Area Kerja'), ['create-area-kerja', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Buat Area Kerja Untuk {nama}', ['nama' => $model->nama])]]) : ''
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnAreaKerja,
                                'bordered' => false,
                                'placeFooterAfterBody' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="inventaris-tab">
                        <?php
                        $gridColumnInventaris = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'ref_inventaris_id',
                                'value' => function (Inventaris $model) {
                                    return Html::tag('strong', $model->refInventaris->nama);
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'tanggal_diterima',
                                'value' => function (Inventaris $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_diterima));
                                }
                            ],
                            [
                                'attribute' => 'tanggal_dikembalikan',
                                'value' => function (Inventaris $model) {
                                    return $model->tanggal_dikembalikan ? date('d-m-Y', strtotime($model->tanggal_dikembalikan)) : '';
                                }
                            ],
                            'catatan:text',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{pegawai-profile/update-inventaris} {pegawai-profile/delete-inventaris}',
                                'buttons' => [
                                    'pegawai-profile/update-inventaris' => function ($url, Inventaris $model) {
                                        return Html::a(Yii::t('frontend', 'Edit'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Inventaris {nama}', ['nama' => $model->pegawai->nama])]]);
                                    },
                                    'pegawai-profile/delete-inventaris' => function ($url) {
                                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-pegawai-inventaris',
                                'dataProvider' => $providerInventaris,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai-inventaris']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Inventaris')),
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Tambahkan Inventaris'), ['pegawai-profile/create-inventaris', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn btn-flat', 'data' => ['header' => Yii::t('frontend', 'Inventaris {nama}', ['nama' => $model->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnInventaris,
                                'bordered' => false,
                                'placeFooterAfterBody' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="surat-peringatan-tab">
                        <?php
                        $gridColumnSuratPeringatan = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'visible' => false],
                            [
                                'attribute' => 'ref_surat_peringatan_id',
                                'label' => Yii::t('frontend', 'Ref Surat Peringatan'),
                                'value' => function (SuratPeringatan $model) {
                                    return $model->refSuratPeringatan->nama;
                                },
                            ],
                            [
                                'attribute' => 'ref_peraturan_perusahaan_id',
                                'label' => Yii::t('frontend', 'Ref Peraturan Perusahaan'),
                                'value' => function (SuratPeringatan $model) {
                                    return $model->getPeraturanPerusahaanFormated();
                                },
                            ],
                            [
                                'attribute' => 'gmp',
                                'value' => function (SuratPeringatan $model) {
                                    return $model->gmp_;
                                },
                            ],
                            [
                                'attribute' => 'pegawai_id',
                                'label' => Yii::t('frontend', 'Pegawai'),
                                'value' => function (SuratPeringatan $model) {
                                    return $model->perjanjianKerja->id_pegawai . '-' . $model->pegawai->nama_lengkap;
                                },
                            ],
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (SuratPeringatan $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                },
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (SuratPeringatan $model) {
                                    return date('d-m-Y', strtotime($model->akhir_berlaku));
                                },
                            ],
                            'catatan:ntext',
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-pegawai-surat-peringatan',
                                'dataProvider' => $providerSuratPeringatan,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-surat-peringatan']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Surat Peringatan')),
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnSuratPeringatan,
                                'bordered' => false,
                                'placeFooterAfterBody' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="training-tab">
                        <?php
                        $gridColumnTraining = [
                            ['attribute' => 'id', 'visible' => false],
                            [
                                'attribute' => 'jenis_pelatihan',
                                'value' => function (TrainingDetail $model) {
                                    return $model->training->jenisPelatihan;
                                }
                            ],
                            [
                                'attribute' => 'tanggal_pelaksanaan',
                                'value' => function (TrainingDetail $model) {
                                    return date('d-m-Y', strtotime($model->training->tanggal_pelaksanaan));
                                },
                            ],
                            'training.durasi',
                            'training.nilai_tes',
                            [
                                'attribute' => 'sertifikat',
                                'value' => function (TrainingDetail $model) {
                                    return Status::activeInactive($model->training->sertifikat);
                                }
                            ],
                            [
                                'attribute' => 'ref_nama_pelatihan_id',
                                'label' => Yii::t('frontend', 'Ref Nama Pelatihan'),
                                'value' => function (TrainingDetail $model) {
                                    return $model->training->refNamaPelatihan->nama;
                                },
                            ],
                            [
                                'attribute' => 'ref_trainer_id',
                                'label' => Yii::t('frontend', 'Ref Trainer'),
                                'value' => function (TrainingDetail $model) {
                                    return $model->training->refTrainer ? $model->training->refTrainer->nama : $model->training->daftarNamaTrainer();
                                },
                            ],
                            [
                                'attribute' => 'ref_nama_institusi_id',
                                'label' => Yii::t('frontend', 'Ref Nama Institusi'),
                                'value' => function (TrainingDetail $model) {
                                    return $model->training->refNamaInstitusi->nama;
                                },
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-training',
                                'dataProvider' => $providerTraining,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-training']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Training')),
                                    'footer' => false,
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnTraining,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="cuti-saldo-tab">
                        <?php
                        $gridColumnCutiSaldo = [
                            ['attribute' => 'id', 'visible' => false],
                            [
                                'attribute' => 'mulai_berlaku',
                                'value' => function (CutiSaldo $model) {
                                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                                },
                            ],
                            [
                                'attribute' => 'akhir_berlaku',
                                'value' => function (CutiSaldo $model) {
                                    return date('d-m-Y', strtotime($model->akhir_berlaku));
                                },
                            ],
                            'kuota',
                            'saldoAkhir',
                            'terpakai',
                            [
                                'attribute' => 'cuti_bersama',
                                'value' => function (CutiSaldo $model) {
                                    return $model->hitungCutiBersama();
                                },
                            ],
                            'catatan:text',
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-cuti-saldo',
                                'dataProvider' => $providerCutiSaldo,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-cuti-saldo']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Saldo Cuti')),
                                    'footer' => false,
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnCutiSaldo,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            print_r($e->getMessage());
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->


            <!-- PROFIL -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('frontend', 'Profil') ?></h3>

                    <div class="box-tools pull-right">
                        <?php
                        echo Html::a(Yii::t('frontend', 'Update Profile'), ['pegawai-profile/pegawai-profile-form', 'id' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Perbaharui Profil')]]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <!-- /.users-list -->
                            <?php
                            $gridColumnPofil = [
                                'nama_lengkap',
                                'nama_panggilan',
                                'jenisKelamin',
                                'tempat_lahir',
                                [
                                    'attribute' => 'tanggal_lahir',
                                    'value' => function (Pegawai $model) {
                                        return date('d-m-Y', strtotime($model->tanggal_lahir));
                                    }
                                ],
                                [
                                    'label' => $model->getAttributeLabel('nomor_ktp'),
                                    'value' => function (Pegawai $model) {
                                        return $model->nomor_ktp;
                                    }
                                ],
                                [
                                    'label' => $model->getAttributeLabel('nomor_kk'),
                                    'value' => function (Pegawai $model) {
                                        return $model->nomor_kk;
                                    }
                                ],
                                'nama_ibu_kandung'
                            ];
                            try {
                                echo DetailView::widget([
                                    'id' => 'grid-data-pegawai-profile',
                                    'model' => $model,
                                    'attributes' => $gridColumnPofil,
                                    'options' => ['class' => 'table table-striped table-condensed detail-view'],
                                ]);
                            } catch (Exception $e) {
                                Yii::info($e->getMessage(), 'exception');
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box-body no-padding">
                            <!-- /.users-list -->
                            <?php
                            $gridColumnPofil = [
                                'kewarganegaraan',
                                'agama_',
                                'golonganDarah',
                                'statusPernikahan',
                                [
                                    'label' => $model->getAttributeLabel('jumlah_anak'),
                                    'value' => function (Pegawai $model) {
                                        return $model->getJumlahAnak();
                                    }
                                ],
                                'no_telp',
                                'no_handphone',
                                'alamat_email:email',
//                                'catatan:ntext',
                            ];
                            try {
                                echo DetailView::widget([
                                    'id' => 'grid-data-pegawai-profile',
                                    'model' => $model,
                                    'attributes' => $gridColumnPofil,
                                    'options' => ['class' => 'table table-striped table-condensed detail-view'],
                                ]);
                            } catch (Exception $e) {
                                Yii::info($e->getMessage(), 'exception');
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?= Yii::t('frontend', 'Alamat KTP') ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <!-- /.users-list -->
                            <?php
                            $gridColumnPofilKtp = [
                                [
                                    'label' => Yii::t('frontend', 'Provinsi'),
                                    'value' => function (Pegawai $model) {
                                        return $model->pegawaiKtpProvinsi ? $model->pegawaiKtpProvinsi->nama : '-';
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Kabupaten/Kota'),
                                    'value' => function (Pegawai $model) {
                                        return $model->pegawaiKtpKabupaten ? $model->pegawaiKtpKabupaten->nama : '-';
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Kecamatan'),
                                    'value' => function (Pegawai $model) {
                                        return $model->pegawaiKtpKecamatan ? $model->pegawaiKtpKecamatan->nama : '-';
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Desa/Kelurahan'),
                                    'value' => function (Pegawai $model) {
                                        return $model->pegawaiKtpDesa ? $model->pegawaiKtpDesa->nama : '-';
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Rt/Rw'),
                                    'value' => function (Pegawai $model) {
                                        return $model->ktp_rt . ' / ' . $model->ktp_rw;
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Alamat'),
                                    'value' => function (Pegawai $model) {
                                        return $model->ktp_alamat;
                                    }
                                ],
                            ];
                            try {
                                echo DetailView::widget([
                                    'id' => 'grid-data-pegawai-profile-ktp',
                                    'model' => $model,
                                    'attributes' => $gridColumnPofilKtp,
                                    'options' => ['class' => 'table table-striped table-condensed detail-view'],
                                ]);
                            } catch (Exception $e) {
                                Yii::info($e->getMessage(), 'exception');
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?= Yii::t('frontend', 'Alamat Domisili') ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <!-- /.users-list -->
                            <?php
                            $gridColumnPofilDomisili = [
                                [
                                    'label' => Yii::t('frontend', 'Provinsi'),
                                    'value' => function (Pegawai $model) {
                                        return $model->pegawaiDomisiliProvinsi ? $model->pegawaiDomisiliProvinsi->nama : '-';
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Kabupaten/Kota'),
                                    'value' => function (Pegawai $model) {
                                        return $model->pegawaiDomisiliKabupaten ? $model->pegawaiDomisiliKabupaten->nama : '-';
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Kecamatan'),
                                    'value' => function (Pegawai $model) {
                                        return $model->pegawaiDomisiliKecamatan ? $model->pegawaiDomisiliKecamatan->nama : '-';
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Desa/Kelurahan'),
                                    'value' => function (Pegawai $model) {
                                        return $model->pegawaiDomisiliDesa ? $model->pegawaiDomisiliDesa->nama : '-';
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Rt/Rw'),
                                    'value' => function (Pegawai $model) {
                                        return $model->domisili_rt . ' / ' . $model->domisili_rw;
                                    }
                                ],
                                [
                                    'label' => Yii::t('frontend', 'Alamat'),
                                    'value' => function (Pegawai $model) {
                                        return $model->domisili_alamat;
                                    }
                                ],
                            ];
                            try {
                                echo DetailView::widget([
                                    'id' => 'grid-data-pegawai-profile-domisili',
                                    'model' => $model,
                                    'attributes' => $gridColumnPofilDomisili,
                                    'options' => ['class' => 'table table-striped table-condensed detail-view'],
                                ]);
                            } catch (Exception $e) {
                                Yii::info($e->getMessage(), 'exception');
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.box -->


            <!-- PENDIDIKAN -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('frontend', 'Pendidikan') ?></h3>

                    <div class="box-tools pull-right">
                        <?php
                        echo Html::a(Yii::t('frontend', 'Tambahkan Pendidikan'), ['pegawai-profile/create-pegawai-pendidikan', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Pendidikan {nama}', ['nama' => $model->nama])]]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $gridColumnPendidikan = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'jenjang',
                                'value' => function (PegawaiPendidikan $model) {
                                    return Html::tag('strong', $model->jenjang_);
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'nama_sekolah',
                            ],
                            'lokasi',
                            'jurusan',
                            'tahun_masuk',
                            'tahun_lulus',
                            'catatan:text',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{pegawai-profile/update-pegawai-pendidikan} {pegawai-profile/delete-pegawai-pendidikan}',
                                'buttons' => [
                                    'pegawai-profile/update-pegawai-pendidikan' => function ($url, PegawaiPendidikan $model) {
                                        return Html::a(Yii::t('frontend', 'Edit'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Pendidikan {nama}', ['nama' => $model->pegawai->nama])]]);
                                    },
                                    'pegawai-profile/delete-pegawai-pendidikan' => function ($url) {
                                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-pegawai-pendidikan',
                                'dataProvider' => $providerPendidikan,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai-keluarga']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => false,
                                    'footer' => false,
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnPendidikan,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!--/.box -->


            <!-- BPJS -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('frontend', 'BPJS') ?></h3>

                    <div class="box-tools pull-right">
                        <?php
                        echo Html::a(Yii::t('frontend', 'Tambahkan BPJS'), ['pegawai-profile/create-pegawai-bpjs', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'BPJS {nama}', ['nama' => $model->nama])]]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $gridColumnBpjs = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'jenis',
                                'value' => function (PegawaiBpjs $model) {
                                    return Html::tag('strong', $model->jenis_);
                                },
                                'format' => 'raw'
                            ],
                            'nomor',
                            [
                                'attribute' => 'tanggal_kepesertaan',
                                'value' => function (PegawaiBpjs $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_kepesertaan));
                                }
                            ],
                            [
                                'attribute' => 'tanggal_berakhir',
                                'value' => function (PegawaiBpjs $model) {
                                    return $model->tanggal_berakhir ? date('d-m-Y', strtotime($model->tanggal_berakhir)) : '';
                                }
                            ],
                            'upah',
                            'catatan:text',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{pegawai-profile/update-pegawai-bpjs} {pegawai-profile/delete-pegawai-bpjs}',
                                'buttons' => [
                                    'pegawai-profile/update-pegawai-bpjs' => function ($url, PegawaiBpjs $model) {
                                        return Html::a(Yii::t('frontend', 'Edit'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit BPJS {nama}', ['nama' => $model->pegawai->nama])]]);
                                    },
                                    'pegawai-profile/delete-pegawai-bpjs' => function ($url) {
                                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-pegawai-bpjs',
                                'dataProvider' => $providerBpjs,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai-keluarga']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => false,
                                    'footer' => false,
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnBpjs,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!--/.box -->

            <!-- KELUARGA -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('frontend', 'Data Kartu Keluarga') . ' ' . $model->nomor_kk ?></h3>

                    <div class="box-tools pull-right">
                        <?php
                        if (Helper::checkRoute('/pegawai/pegawai/set-ess-status')) {
                            echo Html::a(Yii::t('frontend', 'Tambahkan Keluarga'), ['pegawai-profile/create-pegawai-keluarga', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Keluarga {nama}', ['nama' => $model->nama])]]);
                        } else {
                            if ($model->ess_status || $modelPerjanjianKerjaTerakhir->kontrak == PerjanjianKerja::BCA_KBU || $modelPerjanjianKerjaTerakhir->kontrak == PerjanjianKerja::BCA_ADA) {
                                echo Html::a(Yii::t('frontend', 'Tambahkan Keluarga'), ['pegawai-profile/create-pegawai-keluarga', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Keluarga {nama}', ['nama' => $model->nama])]]);
                            } else {
                                echo Html::a(Yii::t('frontend', 'Tambahkan Keluarga'), '#', ['class' => 'btn btn-success btn-xs disabled']);
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $gridColumnKeluarga = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'class' => 'kartik\grid\ExpandRowColumn',
                                'width' => '50px',
                                'value' => function () {
                                    return GridView::ROW_COLLAPSED;
                                },
                                'detail' => function (PegawaiKeluarga $model) {
                                    return $this->render('_keluarga-detail', [
                                        'modelBpjs' => PegawaiBpjs::findOne(['pegawai_keluarga_id' => $model->id]),
                                        'modelAsuransiLain' => PegawaiAsuransiLain::findOne(['pegawai_keluarga_id' => $model->id]),
                                    ]);
                                },
                                'enableRowClick' => true,
                                'headerOptions' => ['class' => 'kartik-sheet-style'],
                                'expandOneOnly' => true
                            ],
                            [
                                'attribute' => 'hubungan',
                                'value' => function (PegawaiKeluarga $model) {
                                    return Html::tag('strong', $model->hubungan_);
                                },
                                'format' => 'raw'
                            ],
                            'nama',
                            'nik',
                            [
                                'attribute' => 'tanggal_lahir',
                                'value' => function (PegawaiKeluarga $model) {
                                    return $model->tanggal_lahir ? date('d-m-Y', strtotime($model->tanggal_lahir)) : '';
                                }
                            ],
                            'pekerjaan',
//                            'alamat:text',
                            'no_handphone',
                            [
                                'attribute' => 'urutan_ke',
                            ],
                            'catatan:text',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{pegawai-profile/form-keluarga-asuransi} {pegawai-profile/form-keluarga-bpjs} {pegawai-profile/update-pegawai-keluarga} {pegawai-profile/delete-pegawai-keluarga}',
                                'buttons' => [
                                    'pegawai-profile/form-keluarga-asuransi' => function ($url, PegawaiKeluarga $model) {
                                        return Html::a(Yii::t('frontend', 'Asuransi'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Tambahkan data Asuransi Kesehatan ke {nama}', ['nama' => $model->nama])]]);
                                    },
                                    'pegawai-profile/form-keluarga-bpjs' => function ($url, PegawaiKeluarga $model) {
                                        return Html::a(Yii::t('frontend', 'BPJS'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Tambahkan data BPJS Kesehatan ke {nama}', ['nama' => $model->nama])]]);
                                    },
                                    'pegawai-profile/update-pegawai-keluarga' => function ($url, PegawaiKeluarga $model) {
                                        return Html::a(Yii::t('frontend', 'Edit'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Keluarga {nama}', ['nama' => $model->nama])]]);
                                    },
                                    'pegawai-profile/delete-pegawai-keluarga' => function ($url) {
                                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-pegawai-keluarga',
                                'dataProvider' => $providerKeluarga,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai-keluarga']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => false,
                                    'footer' => false,
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnKeluarga,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!--/.box -->

            <!-- KONTAK DARURAT -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('frontend', 'Kontak Darurat'); ?></h3>

                    <div class="box-tools pull-right">
                        <?php
                        echo Html::a(Yii::t('frontend', 'Tambahkan Kontak Darurat'), ['pegawai-profile/create-pegawai-darurat', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Kontak Darurat {nama}', ['nama' => $model->nama])]]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $gridColumnKontakDarurat = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'hubungan',
                                'value' => function (PegawaiKontakDarurat $model) {
                                    return Html::tag('strong', $model->hubungan_);
                                },
                                'format' => 'raw'
                            ],
                            'nama',
                            'pekerjaan',
                            'alamat:text',
                            'no_handphone',
                            'catatan:text',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{pegawai-profile/update-pegawai-darurat} {pegawai-profile/delete-pegawai-darurat}',
                                'buttons' => [
                                    'pegawai-profile/update-pegawai-darurat' => function ($url, PegawaiKontakDarurat $model) {
                                        return Html::a(Yii::t('frontend', 'Edit'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Kontak Darurat {nama}', ['nama' => $model->pegawai->nama])]]);
                                    },
                                    'pegawai-profile/delete-pegawai-darurat' => function ($url) {
                                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-pegawai-kontak-darurat',
                                'dataProvider' => $providerKontakDarurat,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai-kontak-darurat']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => false,
                                    'footer' => false,
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnKontakDarurat,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!--/.box -->


            <!-- SIM -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('frontend', 'Surat Ijin Mengemudi'); ?></h3>

                    <div class="box-tools pull-right">
                        <?php
                        echo Html::a(Yii::t('frontend', 'Tambahkan SIM'), ['pegawai-profile/create-sim', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'SIM {nama}', ['nama' => $model->nama])]]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $gridColumnSim = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'ref_surat_izin_mengemudi_id',
                                'value' => function (SuratIzinMengemudi $model) {
                                    return Html::tag('strong', $model->refSuratIzinMengemudi->nama);
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'nomor',
                            ],
                            'catatan:text',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{pegawai-profile/update-sim} {pegawai-profile/delete-sim}',
                                'buttons' => [
                                    'pegawai-profile/update-sim' => function ($url, SuratIzinMengemudi $model) {
                                        return Html::a(Yii::t('frontend', 'Edit'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit SIM {nama}', ['nama' => $model->pegawai->nama])]]);
                                    },
                                    'pegawai-profile/delete-sim' => function ($url) {
                                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-pegawai-sim',
                                'dataProvider' => $providerSim,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai-sim']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => false,
                                    'footer' => false,
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnSim,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                </div>
            </div>


            <!-- FILES -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('frontend', 'Berkas'); ?></h3>

                    <div class="box-tools pull-right">
                        <?php
                        echo Html::a(Yii::t('frontend', 'Tambahkan Berkas'), ['pegawai-profile/upload-file', 'pegawaiid' => $model->id], ['class' => 'btn btn-success btn-xs call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'BERKAS {nama}', ['nama' => $model->nama])]]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $gridColumnFiles = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'jenis',
                                'value' => function (PegawaiFiles $model) {
                                    return $model->_jenis_file[$model->jenis];
                                }
                            ],
                            [
                                'attribute' => 'nama',
                                'value' => function (PegawaiFiles $model) {
                                    return Html::a($model->nama, ['/pegawai/pegawai-profile/view-file', 'id' => $model->id], ['target' => '_blank', 'data' => ['pjax' => 0]]);
                                },
                                'format' => 'raw'
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{pegawai-profile/update-file} {pegawai-profile/delete-file}',
                                'buttons' => [
                                    'pegawai-profile/update-file' => function ($url, PegawaiFiles $model) {
                                        return Html::a(Yii::t('frontend', 'Edit'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit SIM {nama}', ['nama' => $model->pegawai->nama])]]);
                                    },
                                    'pegawai-profile/delete-file' => function ($url) {
                                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo Gridview::widget([
                                'id' => 'grid-pegawai-files',
                                'dataProvider' => $providerFiles,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai-files']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => false,
                                    'footer' => false,
                                ],
                                'toolbar' => [],
                                'export' => false,
                                'columns' => $gridColumnFiles,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                </div>

                <? ?>
            </div>
            <!--/.box -->
        </div>

    </div>

</section>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

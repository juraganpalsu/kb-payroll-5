<?php
/**
 * Created by PhpStorm
 *
 * File Name: _perjanjian-detail.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 07/07/19
 * Time: 17:22
 */

/**
 *
 * @var ActiveDataProvider $providerStruktur
 *
 * @var PerjanjianKerja $model
 *
 */

use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\PkPoc;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$modelPeawaiStruktur = new PegawaiStruktur();
$modelPeawaiStruktur->pegawai_id = $model->pegawai_id;

$pegawai = Pegawai::findOne($model->pegawai_id);
if ($pegawaiStruktur = $pegawai->pegawaiStruktur) {
    if (!PkPoc::findOne(['perjanjian_kerja_id' => $model->id, 'pegawai_struktur_id' => $pegawaiStruktur->id])) {
        echo Html::a(Yii::t('app', 'Alokasi') . ' ' . Html::tag('strong', $pegawaiStruktur->struktur->nameCostume) . ' ' . Yii::t('frontend', 'keperjanjian ini'), ['alokasi-struktur-kekontrak', 'pegawaistrukurid' => $pegawaiStruktur->id, 'perjanjiankerjaid' => $model->id], ['class' => 'btn btn-success btn-xs btn-flat', 'onclick' => 'preventDefault()', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan mengalokasikan {namaPegawai} kedalam {struktur} ?', ['namaPegawai' => $model->pegawai->nama, 'struktur' => $pegawaiStruktur->struktur->nameCostume]), 'method' => 'post']]);
    }
}


$modelPerjanjianKerja = $model;


$gridColumnAlokasiStruktur = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'header' => Yii::t('frontend', 'Nama'),
        'value' => function (PegawaiStruktur $model) {
            return $model->struktur->name;
        }
    ],
    [
        'header' => Yii::t('frontend', 'Mulai'),
        'value' => function (PegawaiStruktur $model) {
            return date('d-m-Y', strtotime($model->mulai_berlaku));
        }
    ],
    [
        'header' => Yii::t('frontend', 'Akhir'),
        'value' => function (PegawaiStruktur $model) {
            return is_null($model->akhir_berlaku) ? '' : date('d-m-Y', strtotime($model->akhir_berlaku));
        }
    ],
    [
        'header' => Yii::t('frontend', 'Keterangan'),
        'value' => function (PegawaiStruktur $model) {
            return $model->keterangan;
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{delete}',
        'buttons' => [
            'delete' => function ($url, PegawaiStruktur $model) use ($modelPerjanjianKerja) {
                if ($model->isAktif()) {
                    return Html::a(Yii::t('frontend', 'Disalokasi'), ['disalokasi-struktur-dr-kontrak', 'pegawaistrukurid' => $model->id, 'perjanjiankerjaid' => $modelPerjanjianKerja->id], ['class' => 'btn btn-xs btn-danger btn-flat disalokasi-struktur', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan mendisalokasi dari struktur ini?')]]);
                }
                return '';
            }
        ],
    ],
];
?>

<div class="row">
    <div class="col-md-12">
        <?php
        try {
            echo Gridview::widget([
                'id' => 'grid-pegawai-struktur',
                'dataProvider' => $providerStruktur,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-alokasi-struktur']],
                'panel' => [],
                'toolbar' => [],
                'export' => false,
                'columns' => $gridColumnAlokasiStruktur,
                'bordered' => false
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
</div>


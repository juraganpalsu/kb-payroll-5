<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 15/11/2018
 * Time: 21:41
 */


use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\Module;
use frontend\modules\struktur\models\Struktur;
use kartik\datecontrol\DateControl;
use kartik\tree\TreeViewInput;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model PegawaiStruktur */
/**
 * @var $modelPegawai Pegawai
 * @var $perjanjiankerjaid string
 */


$js = <<< JS
$(function() {
    $('#form-alokasi-struktur').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
                // location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
    
    setValueStrukturHelper = function(value) {
        $('#pegawaistruktur-helper_struktur_id').val(value);
    }
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create-alokasi-kestruktur', 'id' => $modelPegawai->id]) : Url::to(['update-alokasi-kestruktur', 'id' => $model->id]);
$validation = Url::to(['form-alokasi-kestruktur-validation']);

?>


<div class="alokasi-struktur-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-alokasi-struktur',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-3">
                <?php
                echo $form->field($model, 'aksi')->dropDownList(Module::aksi());
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?php
                echo Html::activeLabel($model, 'struktur_id', ['class' => 'control-label has-star', 'style' => ['content' => '']]);
                echo Html::tag('span', Yii::t('frontend', '*'), ['class' => 'text-danger small mark']);
                echo TreeViewInput::widget([
                    'model' => $model,
                    'attribute' => 'struktur_id',
                    'query' => Struktur::find()->addOrderBy('root, lft'),
                    'headingOptions' => ['label' => Yii::t('frontend', 'Struktur')],
                    'rootOptions' => ['label' => '<i class="fas fa-tree text-success"></i>'],
                    'nodeLabel' => function (Struktur $node) {
                        return $node->nameCostume;
                    },
                    'fontAwesome' => true,
                    'asDropdown' => true,
                    'multiple' => false,
                    'options' => ['disabled' => false, 'id' => 'tree-input-struktur_id'],
                    'pluginOptions' => [
                        'dropdownParent' => new yii\web\JsExpression('$("#form-modal")'),
                    ],
                    'pluginEvents' => [
                        "change" => "function() { setValueStrukturHelper(this.value); }",
                    ]
                ]);
                echo Html::activeHint($model, 'struktur_id');
                ?>
                <br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'mulai_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->mulai_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'akhir_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->akhir_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ])->hint(Yii::t('frontend', '*) Jika akhir berlaku diisi, maka data sudah tidak dapat diedit.'), ['class' => 'text-danger small mark']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $form->field($model, 'keterangan')->textarea(['raw' => 5, 'placeholder' => !empty($sistemKerja) ? $sistemKerja->keterangan : $model->getAttributeLabel('keterangan')]);
                ?>
            </div>
        </div>


        <?php
        echo $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput();
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>


        <hr>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

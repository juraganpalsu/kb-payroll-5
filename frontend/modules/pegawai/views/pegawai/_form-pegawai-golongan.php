<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 15/11/2018
 * Time: 21:41
 */


use common\models\Golongan;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\Module;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model PegawaiGolongan */
/**
 * @var $modelPegawai Pegawai
 */


$js = <<< JS
$(function() {
    $('#form-pegawai-golongan').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });
    
})
JS;

$this->registerJs($js);

$pegawaiid = $modelPegawai->id;

$action = $model->isNewRecord ? Url::to(['create-pegawai-golongan', 'id' => $modelPegawai->id]) : Url::to(['update-pegawai-golongan', 'id' => $model->id]);
$validation = Url::to(['form-pegawai-golongan-validation']);
?>

<div class="pegawai-golongan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-pegawai-golongan',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-3">
                <?php
                echo $form->field($model, 'aksi')->dropDownList([Module::aksi()[Module::DEFAULT], Module::aksi()[Module::PROMOSI], Module::aksi()[Module::DEMOSI]]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <?php
                echo $form->field($model, 'golongan_id')->widget(Select2::class, [
                    'options' => ['placeholder' => Yii::t('frontend', 'Select Golongan')],
                    'data' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'mulai_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->mulai_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'akhir_berlaku')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => !empty($sistemKerja) ? $sistemKerja->akhir_berlaku : Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ])->hint(Yii::t('frontend', '*) Jika akhir berlaku diisi, maka data sudah tidak dapat diedit.'), ['class' => 'text-danger small mark']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $form->field($model, 'keterangan')->textarea(['raw' => 5, 'placeholder' => !empty($sistemKerja) ? $sistemKerja->keterangan : $model->getAttributeLabel('keterangan')]);
                ?>
            </div>
        </div>

        <?php
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <?php

        echo $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput();

        ?>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }

    ?>

    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

/**
 * Created by PhpStorm.
 * File Name: habis-kontrak.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 18/12/19
 * Time: 23.38
 */

use common\components\Status;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PegawaiSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $dataProviderExport yii\data\ArrayDataProvider */

$this->title = Yii::t('frontend', 'Kontrak Pegawai Yg Berakhir pada bulan {bulan1} - {bulan2}', ['bulan1' => date('M'), 'bulan2' => date('M Y', strtotime('+ 2 months'))]);
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
});
JS;

$this->registerJs($js);

?>
<div class="pegawai-index small">
    <?php echo Html::a(Yii::t('frontend', 'Belum Proses'), ['default/habis-kontrak'], ['class' => 'btn btn-warning btn-flat', 'data-pjax' => 0, 'title' => Yii::t('frontend', 'Daftar Pegawai')]) ?>
    <?php echo Html::a(Yii::t('frontend', 'Telah Proses'), ['default/habis-kontrak-after-proses'], ['class' => 'btn btn-info btn-flat', 'data-pjax' => 0, 'title' => Yii::t('frontend', 'Daftar Pegawai')]) ?>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'departemen',
        'jabatan',
        'fungsiJabatan',
        'idPegawai',
        'nama',
        [
            'attribute' => 'aksi_habis_kontrak',
            'header' => Yii::t('frontend', 'Aksi Habis Kontrak'),
            'value' => function ($model) {
                $modelPerjanjianKerja = PerjanjianKerja::findOne($model['id']);
                $status = $modelPerjanjianKerja->aksi_habis_kontrak == Status::DEFLT ? PerjanjianKerja::BELUM_PROSES : $modelPerjanjianKerja->aksi_habis_kontrak;
                if ($modelPerjanjianKerja->aksi_habis_kontrak == PerjanjianKerja::BELUM_PROSES) {
                    return Html::a($modelPerjanjianKerja->statusHabisKontrak[$status], ['aksi-habis-kontrak', 'id' => $modelPerjanjianKerja->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Set Aksi Habis Kontrak ', ['nama' => $modelPerjanjianKerja->pegawai ? $modelPerjanjianKerja->pegawai->nama : ''])]]) . '<br>' . Yii::t('frontend', 'Catatan: ') . $modelPerjanjianKerja->keterangan_habis_kontrak;
                }
                if ($modelPerjanjianKerja->aksi_habis_kontrak == PerjanjianKerja::DILANJUTKAN) {
                    return Html::a($modelPerjanjianKerja->statusHabisKontrak[$status], ['aksi-habis-kontrak', 'id' => $modelPerjanjianKerja->id], ['class' => 'btn btn-xs btn-success btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Set Aksi Habis Kontrak ', ['nama' => $modelPerjanjianKerja->pegawai ? $modelPerjanjianKerja->pegawai->nama : ''])]]) . '<br>' . Yii::t('frontend', 'Catatan: ') . $modelPerjanjianKerja->keterangan_habis_kontrak;
                }
                if ($modelPerjanjianKerja->aksi_habis_kontrak == PerjanjianKerja::PHK) {
                    return Html::a($modelPerjanjianKerja->statusHabisKontrak[$status], ['aksi-habis-kontrak', 'id' => $modelPerjanjianKerja->id], ['class' => 'btn btn-xs btn-warning btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Set Aksi Habis Kontrak ', ['nama' => $modelPerjanjianKerja->pegawai ? $modelPerjanjianKerja->pegawai->nama : ''])]]) . '<br>' . Yii::t('frontend', 'Catatan: ') . $modelPerjanjianKerja->keterangan_habis_kontrak;
                }
                if ($modelPerjanjianKerja->aksi_habis_kontrak == PerjanjianKerja::PENDING) {
                    return Html::a($modelPerjanjianKerja->statusHabisKontrak[$status], ['aksi-habis-kontrak', 'id' => $modelPerjanjianKerja->id], ['class' => 'btn btn-xs btn-danger btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Set Aksi Habis Kontrak ', ['nama' => $modelPerjanjianKerja->pegawai ? $modelPerjanjianKerja->pegawai->nama : ''])]]) . '<br>' . Yii::t('frontend', 'Catatan: ') . $modelPerjanjianKerja->keterangan_habis_kontrak;
                }
                return Html::a($modelPerjanjianKerja->statusHabisKontrak[$status], ['aksi-habis-kontrak', 'id' => $modelPerjanjianKerja->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Set Aksi Habis Kontrak ', ['nama' => $modelPerjanjianKerja->pegawai ? $modelPerjanjianKerja->pegawai->nama : ''])]]);
            },
            'format' => 'raw'
        ],
        'golongan',
        'atasanLangsung',
        'bisnisUnit',
        'jenisKontrak',
        'awalKontrak',
        'akhirKontrak',
        'historyKontrak',
        'area',
    ];
    ?>
    <?php try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProviderExport,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
            'noExportColumns' => [1], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'pegawai-habis-kontrak' . date('dmYHis')
        ]);

        echo GridView::widget([
            'id' => 'grid-pegawai',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

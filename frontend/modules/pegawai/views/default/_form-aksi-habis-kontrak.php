<?php

/**
 * Created by PhpStorm.
 * File Name: _form-aksi-habis-kontrak.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 29/09/20
 * Time: 21.21
 */


use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model PerjanjianKerja */

$js = <<< JS
$(function() {
    $('#submit-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        var form = $(this);
        var btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.reload();
            }
        });
        return false;
    });
    
})
JS;

$this->registerJs($js);

$action = Url::to(['aksi-habis-kontrak', 'id' => $model->id]);
$validation = Url::to(['aksi-habis-kontrak-validation', 'id' => $model->id]);
?>

<div class="submit-form">

    <?php $form = ActiveForm::begin([
        'id' => 'submit-form',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => false,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-4">
                <?php
                try {
                    echo $form->field($model, 'aksi_habis_kontrak')->dropDownList($model->statusHabisKontrak);
                } catch (InvalidConfigException $e) {
                } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $form->field($model, 'keterangan_habis_kontrak')->textarea(['raw' => 5, 'placeholder' => $model->getAttributeLabel('keterangan_habis_kontrak')]);
                ?>
            </div>
        </div>

        <?php
        echo $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput();
        echo $form->field($model, 'jenis_perjanjian', ['template' => '{input}'])->hiddenInput();
        echo $form->field($model, 'kontrak', ['template' => '{input}'])->hiddenInput();
        echo $form->field($model, 'mulai_berlaku', ['template' => '{input}'])->hiddenInput();
        echo $form->field($model, 'id_pegawai', ['template' => '{input}'])->hiddenInput();
        echo $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput();
        ?>
        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }

    ?>

    <hr>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

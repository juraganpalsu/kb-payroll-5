<?php

/**
 * Created by PhpStorm.
 * File Name: _form-foto-profil.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 08/10/19
 * Time: 22.06
 */


use frontend\modules\pegawai\models\Pegawai;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/**
 *
 * @var Pegawai $model
 */

?>

<div class="add-assets-picture text-primary">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->errorSummary($model); ?>
    <?php
    try {
        echo $form->field($model, 'nama_file')->widget(FileInput::class, [
            'options' => [
                'multiple' => false,
                'accept' => 'image/*,'
            ],
            'pluginOptions' => [
                'previewFileType' => 'any',
                'uploadUrl' => Url::to(['update-foto', 'pegawaiid' => $model->id]),
                'maxFileSize' => 7000
            ],
            'pluginEvents' => [
                'fileuploaded' => 'function(event, data){ if (data.response.status === true) location.reload();}'
            ]
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>
</div>

<?php

/**
 * Created by PhpStorm.
 * File Name: _form-upload-files.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 19/01/21
 * Time: 14.38
 */


use frontend\modules\pegawai\models\PegawaiFiles;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\bootstrap\Html;
use yii\helpers\Url;


/**
 * @var $this yii\web\View
 * @var $model PegawaiFiles
 */

$jsForm = <<<JS
$(function() {
    $('#create-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        let formData = new FormData(this);
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function (dt) {                
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($jsForm);
?>
<div class="upload-form">
    <?php $form = ActiveForm::begin([
        'id' => 'create-form',
        'action' => $model->isNewRecord ? Url::to(['upload-file', 'pegawaiid' => $model->pegawai_id]) : Url::to(['update-file', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['upload-file-validation', 'id' => $model->id]),
        'fieldConfig' => ['showLabels' => true],
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'jenis')->dropDownList($model->_jenis_file) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'doc_file')->widget(FileInput::class, [
                    'options' => [
                        'multiple' => false,
                        'accept' => 'pdf'
                    ],
                    'pluginOptions' => [
                        'previewFileType' => 'any',
                        'showUpload' => false,
                        'initialPreview' => $model->isNewRecord ? [] : $model->getAttachmentCofiguration('url'),
                        'initialPreviewAsData' => true,
//                        'initialPreviewConfig' => $model->isNewRecord ? [] : $model->getAttachmentCofiguration('key'),
//                        'deleteUrl' => Url::to(['/purchase-requisitions/remove-attachment']),
                        'overwriteInitial' => true,
                        'maxFileSize' => 7000,
                        'showRemove' => false,
                    ],
                ]);
                ?>
            </div>
        </div>

        <?= $form->field($model, 'id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('frontend', 'Upload'), ['class' => 'btn btn-success submit-btn']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>
    <?php ActiveForm::end(); ?>

</div>
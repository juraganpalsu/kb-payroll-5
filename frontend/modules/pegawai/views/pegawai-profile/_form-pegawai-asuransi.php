<?php

/**
 * Created by PhpStorm.
 * File Name: _form-pegawai-asuransi.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 30/09/19
 * Time: 10.41
 */

use frontend\modules\pegawai\models\PegawaiAsuransiLain;
use kartik\datecontrol\DateControl;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var PegawaiAsuransiLain $model
 */

$js = <<<JS
$(function() {
    $('.form-control').on('keyup', function () {
        $(this).val( $(this).val().toUpperCase() );
    });  
    
    $('#form-pegawai-asuransi').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });    
})
JS;

$this->registerJs($js);

$action = Url::to(['pegawai-asuransi-form', 'pegawaiid' => $model->pegawai_id]);
$validation = Url::to(['form-pegawai-asuransi-validation']);
?>

<div class="user-pelamar-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-pegawai-asuransi',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'nama')->dropDownList($model->_nama, ['prompt' => '-', 'placeholder' => $model->getAttributeLabel('nama')]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'nomor')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('nomor')]) ?>
            </div>
            <div class="col-sm-3">
                <?=
                $form->field($model, 'tanggal_kepesertaan')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('frontend', 'Choose Tanggal Kepesertaan'),
                            'autoclose' => true
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Simpan') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

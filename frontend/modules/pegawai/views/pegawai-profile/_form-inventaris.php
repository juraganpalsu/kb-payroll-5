<?php

/**
 * Created by PhpStorm.
 * File Name: _form-inventaris.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 04/11/19
 * Time: 23.51
 */


use frontend\modules\personalia\models\Inventaris;
use frontend\modules\personalia\models\RefInventaris;
use kartik\datecontrol\DateControl;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var Inventaris $model
 */

$js = <<<JS
$(function() {
    $('.form-control').on('keyup', function () {
        $(this).val( $(this).val().toUpperCase() );
    });  
    
    $('#form-inventaris').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });    
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create-inventaris', 'pegawaiid' => $model->pegawai_id]) : Url::to(['update-inventaris', 'id' => $model->id]);
$validation = Url::to(['form-inventaris-validation']);
?>

<div class="user-pelamar-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-inventaris',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-sm-5">
                <?php
                echo $form->field($model, 'ref_inventaris_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(RefInventaris::find()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => $model->getAttributeLabel('sistem_kerja'), 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <?=
                $form->field($model, 'tanggal_diterima')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => $model->getAttributeLabel('tanggal_diterima'),
                            'autoclose' => true
                        ]
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-3">
                <?=
                $form->field($model, 'tanggal_dikembalikan')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => $model->getAttributeLabel('tanggal_dikembalikan'),
                            'autoclose' => true
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Simpan') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
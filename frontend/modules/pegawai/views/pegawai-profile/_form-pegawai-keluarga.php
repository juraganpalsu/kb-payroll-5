<?php

/**
 * Created by PhpStorm.
 * File Name: _form-pegawai-keluarga.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 30/09/19
 * Time: 22.55
 */


use frontend\modules\pegawai\models\PegawaiKeluarga;
use kartik\datecontrol\DateControl;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var PegawaiKeluarga $model
 */

$anak = PegawaiKeluarga::ANAK;
$kakak = PegawaiKeluarga::KAKAK;
$adik = PegawaiKeluarga::ADIK;

$js = <<<JS
$(function() {
    $('.form-control').on('keyup', function () {
        $(this).val( $(this).val().toUpperCase() );
    });  
    
    $('#form-pegawai-keluarga').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });    
    
    $('#pegawaikeluarga-hubungan').on('change', function() {
        var hubungan = this;
        if (hubungan.value === '$anak' || hubungan.value === '$kakak' || hubungan.value === '$adik'){
            $('#pegawaikeluarga-urutan_ke').attr('readonly', false);
        }else {
            $('#pegawaikeluarga-urutan_ke').attr('readonly', true).val(0);
        }
    });
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create-pegawai-keluarga', 'pegawaiid' => $model->pegawai_id]) : Url::to(['update-pegawai-keluarga', 'id' => $model->id]);
$validation = Url::to(['form-pegawai-keluarga-validation']);
?>

<div class="user-pelamar-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-pegawai-keluarga',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'hubungan')->dropDownList($model->_hubungan, ['placeholder' => $model->getAttributeLabel('hubungan')]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('nama')]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'nik')->textInput(['placeholder' => $model->getAttributeLabel('nik')]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <?=
                $form->field($model, 'tanggal_lahir')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('frontend', 'Choose Tanggal Lahir'),
                            'autoclose' => true
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <?= $form->field($model, 'pekerjaan')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('pekerjaan')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'no_handphone')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('no_handphone')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'urutan_ke')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('urutan_ke')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Simpan') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
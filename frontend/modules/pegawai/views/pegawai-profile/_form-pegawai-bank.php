<?php

/**
 * Created by PhpStorm.
 * File Name: _form-pegawai-bank.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 30/09/19
 * Time: 10.41
 */

use frontend\modules\pegawai\models\PegawaiBank;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var PegawaiBank $model
 */

$js = <<<JS
$(function() {
    $('.form-control').on('keyup', function () {
        $(this).val( $(this).val().toUpperCase() );
    });  
    
    $('#form-pegawai-bank').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });    
})
JS;

$this->registerJs($js);

$action = Url::to(['pegawai-bank-form', 'pegawaiid' => $model->pegawai_id]);
$validation = Url::to(['form-pegawai-bank-validation']);
?>

<div class="user-pelamar-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-pegawai-bank',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'nama_bank')->dropDownList($model->_nama_bank, ['placeholder' => $model->getAttributeLabel('nama_bank')]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'nomer_rekening')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('nomer_rekening')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Simpan') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

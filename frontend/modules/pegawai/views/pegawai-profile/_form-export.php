<?php

/**
 * Created by PhpStorm.
 * File Name: _form-export.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 20/10/19
 * Time: 15.58
 */


use common\models\Golongan;
use common\models\SistemKerja;
use frontend\modules\pegawai\models\form\PegawaiForm;
use frontend\modules\pegawai\models\PerjanjianKerjaJenis;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var ActiveForm $form
 * @var PegawaiForm $model
 *
 *
 */

$js = <<< JS
$(function() {
    $('#excel-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                console.log(dt);
                let win = window.open(dt.data.url, '_blank');
                win.focus();
                location.reload();
            }else {
                location.reload();
            }
        });
        return false;
    });
})
JS;

$this->registerJs($js);
?>
<div class="excel-form">

    <?php $form = ActiveForm::begin([
        'id' => 'excel-form',
        'action' => Url::to(['form-export']),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['form-export-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'golongan_id')->dropDownList(ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'));
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'busines_unit')->dropDownList(PegawaiForm::perjanjianKerja()->_kontrak, ['prompt' => '*']);
                ?>
            </div>
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'jenis_kontrak')->dropDownList(ArrayHelper::map(PerjanjianKerjaJenis::find()->all(), 'id', 'nama'), ['prompt' => '*']);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <?php
                echo $form->field($model, 'sistem_kerja_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => $model->getAttributeLabel('sistem_kerja'), 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <hr>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Generate'), ['class' => 'btn btn-success btn-flat']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>

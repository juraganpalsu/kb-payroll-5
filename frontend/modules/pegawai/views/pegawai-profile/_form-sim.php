<?php

/**
 * Created by PhpStorm.
 * File Name: _form-sim.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 05/11/19
 * Time: 23.00
 */


use frontend\modules\personalia\models\RefSuratIzinMengemudi;
use frontend\modules\personalia\models\SuratIzinMengemudi;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var SuratIzinMengemudi $model
 */

$js = <<<JS
$(function() {
    $('.form-control').on('keyup', function () {
        $(this).val( $(this).val().toUpperCase() );
    });  
    
    $('#form-sim').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });    
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create-sim', 'pegawaiid' => $model->pegawai_id]) : Url::to(['update-sim', 'id' => $model->id]);
$validation = Url::to(['form-sim-validation']);
?>

<div class="user-pelamar-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-sim',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-sm-5">
                <?php
                echo $form->field($model, 'ref_surat_izin_mengemudi_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(RefSuratIzinMengemudi::find()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => $model->getAttributeLabel('ref_surat_izin_mengemudi_id'), 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'nomor')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('nomor')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Simpan') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
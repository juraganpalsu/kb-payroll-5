<?php

/**
 * Created by PhpStorm.
 * File Name: _form-pegawai-darurat.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 30/09/19
 * Time: 22.55
 */


use frontend\modules\pegawai\models\PegawaiDarurat;
use frontend\modules\pegawai\models\PegawaiKontakDarurat;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var PegawaiDarurat $model
 */

$js = <<<JS
$(function() {
    $('.form-control').on('keyup', function () {
        $(this).val( $(this).val().toUpperCase() );
    });  
    
    $('#form-pegawai-darurat').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });    
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create-pegawai-darurat', 'pegawaiid' => $model->pegawai_id]) : Url::to(['update-pegawai-darurat', 'id' => $model->id]);
$validation = Url::to(['form-pegawai-darurat-validation']);
?>

<div class="user-pelamar-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-pegawai-darurat',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'hubungan')->dropDownList(PegawaiKontakDarurat::modelPegawaiKeluarga()->_hubungan, ['placeholder' => $model->getAttributeLabel('hubungan')]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('nama')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'pekerjaan')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('pekerjaan')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'no_handphone')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('no_handphone')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'alamat')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Simpan') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
<?php

/**
 * Created by PhpStorm.
 * File Name: _form-pegawai-profile.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 29/09/19
 * Time: 17.20
 */

use frontend\modules\indonesia\models\IndonesiaProvinsi;
use frontend\modules\pegawai\models\Pegawai;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * @var $model Pegawai
 */

$js = <<<JS
$(function() {
    $('.copy-ktp').bind('click', function(e) {
        e.preventDefault();
        let alamat_ktp = $('#pegawaiprofile-alamat_sesuai_ktp').val();
        $('#pegawaiprofile-alamat_tempat_tinggal').val(alamat_ktp);
    });
        
    $('.form-control').on('keyup', function () {
        $(this).val( $(this).val().toUpperCase() );
    });  
    
    $('#form-pegawai-profile').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });
    
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['pegawai-profile-form', 'id' => '-']) : Url::to(['pegawai-profile-form', 'id' => $model->id]);
$validation = Url::to(['form-pegawai-profile-validation', 'isnew' => $model->isNewRecord]);

?>

<div class="user-pelamar-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-pegawai-profile',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'nama_lengkap')->textInput(['maxlength' => true, 'placeholder' => 'Nama Lengkap']) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'nama_panggilan')->textInput(['maxlength' => true, 'placeholder' => 'Nama Panggilan']) ?>
            </div>

            <div class="col-sm-2">
                <?=
                $form->field($model, 'jenis_kelamin')->widget(kartik\widgets\SwitchInput::class, [
                    'options' => [
                        'class' => 'btn-flat'
                    ],
                    'pluginOptions' => [
                        'handleWidth' => 90,
                        'onText' => Yii::t('frontend', 'LAKI-LAKI'),
                        'offText' => Yii::t('frontend', 'PEREMPUAN')
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <?=
                $form->field($model, 'status_pernikahan')->widget(Select2::class, [
                    'data' => array_diff_key($model->_status_pernikahan, [Pegawai::MENIKAH_ANAK_1 => '', Pegawai::MENIKAH_ANAK_2 => '', Pegawai::MENIKAH_ANAK_3 => '', Pegawai::MENIKAH_ANAK_3P => '']),
                    'options' => ['placeholder' => Yii::t('frontend', 'Pilih Status')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->hint(Yii::t('frontend', '*) Jumlah Anak otomatis terisi dari Data Keluarga'), ['class' => 'text-danger small mark']);
                ?>
            </div>
            <div class="col-sm-1">
                <?= $form->field($model, 'anak')->textInput(['value' => $model->getJumlahAnak() , 'disabled' => 'disabled']) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('tempat_lahir')]) ?>
            </div>
            <div class="col-sm-3">
                <?=
                $form->field($model, 'tanggal_lahir')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('frontend', 'Choose Tanggal Lahir'),
                            'autoclose' => true
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-3">
            </div>
        </div>

        <div class="row">
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'agama')->widget(Select2::class, [
                    'data' => $model->_agama,
                    'options' => ['placeholder' => Yii::t('frontend', 'Pilih Agama')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'nomor_ktp')->textInput(['placeholder' => $model->getAttributeLabel('nomor_ktp')]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'nomor_kk')->textInput(['placeholder' => $model->getAttributeLabel('nomor_kk')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'no_telp')->textInput(['maxlength' => true, 'placeholder' => 'No Telp']) ?>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-3">
                <?= $form->field($model, 'no_handphone')->textInput(['placeholder' => 'No Handphone']) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'alamat_email')->textInput(['maxlength' => true, 'placeholder' => 'Alamat Email']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'nama_ibu_kandung')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('nama_ibu_kandung')]) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'gol_darah')->widget(Select2::class, [
                    'data' => $model->_gol_darah,
                    'options' => ['placeholder' => Yii::t('frontend', 'Golongan Darah')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>


        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('frontend', 'Alamat KTP') ?></h3>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'ktp_provinsi_id')->widget(Select2::class, [
                        'data' => ArrayHelper::map(IndonesiaProvinsi::find()->orderBy('nama ASC')->all(), 'id', 'nama'),
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Provinsi ...')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-sm-3">
                    <?php
                    $dataKtpKabupaten = [];
                    if (!$model->isNewRecord) {
                        if ($model->ktp_kabupaten_kota_id) {
                            $dataKtpKabupaten = [$model->ktp_kabupaten_kota_id => $model->pegawaiKtpKabupaten->nama];
                        }
                    }
                    echo $form->field($model, 'ktp_kabupaten_kota_id')->widget(DepDrop::class, [
                        'data' => $dataKtpKabupaten,
                        'options' => ['placeholder' => 'Select ...'],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['pegawai-ktp_provinsi_id'],
                            'url' => Url::to(['/indonesia/default/nama-daerah', 'step' => 1]),
                            'loadingText' => 'Loading...',
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-sm-3">
                    <?php
                    $dataKtpKecamatan = [];
                    if (!$model->isNewRecord) {
                        if ($model->ktp_kecamatan_id) {
                            $dataKtpKecamatan = [$model->ktp_kecamatan_id => $model->pegawaiKtpKecamatan->nama];
                        }
                    }
                    echo $form->field($model, 'ktp_kecamatan_id')->widget(DepDrop::class, [
                        'data' => $dataKtpKecamatan,
                        'options' => ['placeholder' => 'Select ...'],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['pegawai-ktp_kabupaten_kota_id'],
                            'url' => Url::to(['/indonesia/default/nama-daerah', 'step' => 2]),
                            'loadingText' => 'Loading...',
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-sm-3">
                    <?php
                    $dataKtpDesa = [];
                    if (!$model->isNewRecord) {
                        if ($model->ktp_desa_id) {
                            $dataKtpDesa = [$model->ktp_desa_id => $model->pegawaiKtpDesa->nama];
                        }
                    }
                    echo $form->field($model, 'ktp_desa_id')->widget(DepDrop::class, [
                        'data' => $dataKtpDesa,
                        'options' => ['placeholder' => 'Select ...'],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['pegawai-ktp_kecamatan_id'],
                            'url' => Url::to(['/indonesia/default/nama-daerah', 'step' => 3]),
                            'loadingText' => 'Loading...',
                        ]
                    ]);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <?= $form->field($model, 'ktp_rt')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('ktp_rt')]) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'ktp_rw')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('ktp_rw')]) ?>
                </div>
                <div class="col-sm-5">
                    <?= $form->field($model, 'ktp_alamat')->textarea(['rows' => 3]) ?>
                </div>
            </div>
        </div>


        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('frontend', 'Alamat Domisili') ?></h3>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'domisili_provinsi_id')->widget(Select2::class, [
                        'data' => ArrayHelper::map(IndonesiaProvinsi::find()->orderBy('nama ASC')->all(), 'id', 'nama'),
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Provinsi ...')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-sm-3">
                    <?php
                    $dataDomisiliKabupaten = [];
                    if (!$model->isNewRecord) {
                        if ($model->domisili_kabupaten_kota_id) {
                            $dataDomisiliKabupaten = [$model->domisili_kabupaten_kota_id => $model->pegawaiDomisiliKabupaten->nama];
                        }
                    }
                    echo $form->field($model, 'domisili_kabupaten_kota_id')->widget(DepDrop::class, [
                        'data' => $dataDomisiliKabupaten,
                        'options' => ['placeholder' => 'Select ...'],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['pegawai-domisili_provinsi_id'],
                            'url' => Url::to(['/indonesia/default/nama-daerah', 'step' => 1]),
                            'loadingText' => 'Loading...',
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-sm-3">
                    <?php
                    $dataDomisiliKecamatan = [];
                    if (!$model->isNewRecord) {
                        if ($model->domisili_kecamatan_id) {
                            $dataDomisiliKecamatan = [$model->domisili_kecamatan_id => $model->pegawaiDomisiliKecamatan->nama];
                        }
                    }
                    echo $form->field($model, 'domisili_kecamatan_id')->widget(DepDrop::class, [
                        'data' => $dataDomisiliKecamatan,
                        'options' => ['placeholder' => 'Select ...'],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['pegawai-domisili_kabupaten_kota_id'],
                            'url' => Url::to(['/indonesia/default/nama-daerah', 'step' => 2]),
                            'loadingText' => 'Loading...',
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-sm-3">
                    <?php
                    $dataDomisiliDesa = [];
                    if (!$model->isNewRecord) {
                        if ($model->domisili_desa_id) {
                            $dataDomisiliDesa = [$model->domisili_desa_id => $model->pegawaiDomisiliDesa->nama];
                        }
                    }
                    echo $form->field($model, 'domisili_desa_id')->widget(DepDrop::class, [
                        'data' => $dataDomisiliDesa,
                        'options' => ['placeholder' => 'Select ...'],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['pegawai-domisili_kecamatan_id'],
                            'url' => Url::to(['/indonesia/default/nama-daerah', 'step' => 3]),
                            'loadingText' => 'Loading...',
                        ]
                    ]);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <?= $form->field($model, 'domisili_rt')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('domisili_rt')]) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'domisili_rw')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('domisili_rw')]) ?>
                </div>
                <div class="col-sm-5">
                    <?= $form->field($model, 'domisili_alamat')->textarea(['rows' => 3]) ?>
                </div>
            </div>
            <?= $form->field($model, 'kewarganegaraan', ['template' => '{input}'])->hiddenInput(['value' => 'INDONESIA']) ?>

        </div>


        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Simpan') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

/**
 * Created by PhpStorm.
 * File Name: index.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 30/10/19
 * Time: 21.52
 */


use frontend\modules\pegawai\models\RefAreaKerja;
use kartik\tree\Module;
use kartik\tree\TreeView;

$this->title = Yii::t('frontend', 'Area Kerja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Pegawai')];
$this->params['breadcrumbs'][] = $this->title;


$mainTemplate = <<<HTML
<div class="row">
    <div class="col-sm-6">
        {wrapper}
    </div>
    <div class="col-sm-6">
        {detail}
    </div>
</div>
HTML;

try {
    echo TreeView::widget([
        // single query     fetch to render the tree
        // use the Struktur model you have in the previous step
        'query' => RefAreaKerja::find()->addOrderBy('root, lft'),
        'headingOptions' => ['label' => Yii::t('frontend', 'Area Kerja')],
        'topRootAsHeading' => true,
        'fontAwesome' => true,     // optional
        'isAdmin' => false,         // optional (toggle to enable admin mode)
        'displayValue' => 1,        // initial display value
        'softDelete' => true,       // defaults to true
        'cacheSettings' => [
            'enableCache' => false   // defaults to true
        ],
        'showIDAttribute' => false,
        'iconEditSettings' => [
            'show' => 'list',
            'listData' => [
                'file' => 'File',
                'bell' => 'Bell',
            ]
        ],
        'nodeAddlViews' => [
            Module::VIEW_PART_2 => '@frontend/modules/pegawai/views/ref-area-kerja/_form',
        ],
        'mainTemplate' => $mainTemplate,
        'treeOptions' => ['style' => 'height:600px'],
    ]);
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
}
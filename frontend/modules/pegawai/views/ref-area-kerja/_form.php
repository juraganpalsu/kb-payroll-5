<?php
/**
 * Created by PhpStorm
 *
 * File Name: _form.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 03/07/19
 * Time: 22:23
 *
 *
 * @var Struktur $node
 * @var ActiveForm $form
 *
 */

use frontend\modules\struktur\models\Corporate;
use frontend\modules\struktur\models\Departemen;
use frontend\modules\struktur\models\Direktur;
use frontend\modules\struktur\models\Divisi;
use frontend\modules\struktur\models\FungsiJabatan;
use frontend\modules\struktur\models\Jabatan;
use frontend\modules\struktur\models\Perusahaan;
use frontend\modules\struktur\models\Section;
use frontend\modules\struktur\models\Struktur;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>

<?php try { ?>

    <div class="row">
        <div class="col-md-10">
            <?php
            echo $form->field($node, 'keterangan')->textarea();
            ?>
        </div>
    </div>

    <?php
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
}

?>
<?php

namespace frontend\modules\dashboard\models;

use frontend\modules\dashboard\models\base\DsTurnOverDetail as BaseDsTurnOverDetail;

/**
 * This is the model class for table "ds_turn_over_detail".
 */
class DsTurnOverDetail extends BaseDsTurnOverDetail
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['ds_turn_over_id'], 'required'],
            [['bulan', 'masuk', 'resign', 'total'], 'integer'],
            [['created_at'], 'safe'],
            [['id', 'ds_turn_over_id'], 'string', 'max' => 36],
            [['bulan_string'], 'string', 'max' => 45],
        ];
    }

}

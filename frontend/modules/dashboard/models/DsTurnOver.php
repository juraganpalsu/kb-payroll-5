<?php

namespace frontend\modules\dashboard\models;

use common\components\Status;
use DateTime;
use frontend\modules\dashboard\models\base\DsTurnOver as BaseDsTurnOver;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\Resign;
use Yii;

/**
 * This is the model class for table "ds_turn_over".
 */
class DsTurnOver extends BaseDsTurnOver
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['tahun'], 'integer'],
            [['created_at'], 'safe'],
            [['id'], 'string', 'max' => 36]
        ];
    }

    /**
     * @param DateTime $dateTimeObj
     * @return array
     */
    public function rekapData(DateTime $dateTimeObj): array
    {
        $tahun = $dateTimeObj->format('Y');
        $bulan = $dateTimeObj->format('m');
        $bulanString = $dateTimeObj->format('M');
        $akhirBulan = $dateTimeObj->format('Y-m-t');

        $modelPerjanjianKerja = new PerjanjianKerja();

        $queryMasuk = PerjanjianKerja::find()->andWhere(['month(mulai_berlaku)' => $bulan, 'year(mulai_berlaku)' => $tahun, 'dasar_masa_kerja' => Status::YA])->count();
        $queryResign = Resign::find()->andWhere(['month(tanggal)' => $bulan, 'year(tanggal)' => $tahun])->count();
        $queryTotalPegawaiAktif = count($modelPerjanjianKerja->perjanjianKerjaAktifBerdasarTanggal($akhirBulan));

        return [
            'tahun' => $tahun, 'bulan' => $bulan, 'bulanString' => $bulanString,
            'masuk' => $queryMasuk, 'resign' => $queryResign, 'total' => $queryTotalPegawaiAktif
        ];
    }

    /**
     * @param DateTime $dateTimeObj
     */
    public function simpanData(DateTime $dateTimeObj)
    {
        $data = $this->rekapData($dateTimeObj);
        Yii::info(Yii::t('frontend', 'Simpan data turn over untuk tahun {tahun} dan bulan {bulan}', ['tahun' => $dateTimeObj->format('Y'), 'bulan' => $dateTimeObj->format('m')]), 'generate-ds-turn-over');
        $model = new DsTurnOver();
        $model->tahun = $dateTimeObj->format('Y');
        $cek = DsTurnOver::findOne(['tahun' => $model->tahun]);
        if ($cek) {
            $model = $cek;
        }
        if ($model->save()) {
            $modelDetail = new DsTurnOverDetail();
            $modelDetail->bulan = $data['bulan'];
            $modelDetail->ds_turn_over_id = $model->id;
            $cekDetail = DsTurnOverDetail::findOne(['bulan' => $modelDetail->bulan, 'ds_turn_over_id' => $modelDetail->ds_turn_over_id]);
            if ($cekDetail) {
                $modelDetail = $cekDetail;
            }
            $modelDetail->bulan_string = $data['bulanString'];
            $modelDetail->masuk = $data['masuk'];
            $modelDetail->resign = $data['resign'];
            $modelDetail->total = $data['total'];
            $modelDetail->save();
            Yii::info(Yii::t('frontend', 'Data berhasil disimpan untuk tahun {tahun} dan bulan {bulan}', ['tahun' => $dateTimeObj->format('Y'), 'bulan' => $dateTimeObj->format('m')]), 'generate-ds-turn-over');
        }

    }

    /**
     * @param int $tahun
     * @return array
     */
    public static function getDashboardData(int $tahun = 0): array
    {
        $model = DsTurnOver::findOne(['tahun' => $tahun]);
        $dsJumlahTurnOver = [];

        if ($model) {
            foreach ($model->dsTurnOverDetails as $detail) {
                $dsJumlahTurnOver[] = ['name' => $detail->bulan_string, 'y' => round(($detail->resign / $detail->total) * 100, 2)];
            }
        }
        return $dsJumlahTurnOver;
    }

    /**
     * @param int $tahun
     * @return array
     */
    public static function getDashboardInOut(int $tahun = 0): array
    {
        $model = DsTurnOver::findOne(['tahun' => $tahun]);
        $categories = [];
        $series = [];
        if ($model) {
            foreach ($model->dsTurnOverDetails as $detail) {
                $categories[] = $detail->bulan_string;
                $series[0]['name'] = Yii::t('frontend', 'Masuk');
                $series[0]['data'][] = $detail->masuk;
                $series[1]['name'] = Yii::t('frontend', 'Resign');
                $series[1]['data'][] = $detail->resign;
            }
        }
        return ['categories' => $categories, 'series' => $series];
    }

}

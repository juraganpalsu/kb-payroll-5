<?php

namespace frontend\modules\dashboard\models;

use Yii;
use \frontend\modules\dashboard\models\base\DsAkhirKontrakDetail as BaseDsAkhirKontrakDetail;

/**
 * This is the model class for table "ds_akhir_kontrak_detail".
 */
class DsAkhirKontrakDetail extends BaseDsAkhirKontrakDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kontrak', 'ds_akhir_kontrak_id'], 'required'],
            [['kontrak', 'jumlah'], 'integer'],
            [['perjanjian_kerja_ids'], 'string'],
            [['created_at'], 'safe'],
            [['id', 'ds_akhir_kontrak_id'], 'string', 'max' => 32],
        ];
    }

}

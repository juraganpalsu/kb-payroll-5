<?php

namespace frontend\modules\dashboard\models;

use Yii;
use \frontend\modules\dashboard\models\base\DsPerTanggal as BaseDsPerTanggal;

/**
 * This is the model class for table "ds_per_tanggal".
 */
class DsPerTanggal extends BaseDsPerTanggal
{

    const ABSENSI_STATUS_KEHADIRAN = 1;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal'], 'required'],
            [['jenis'], 'integer'],
            [['tanggal', 'created_at'], 'safe'],
            [['id'], 'string', 'max' => 32],
        ];
    }

}

<?php

namespace frontend\modules\dashboard\models\base;

use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "ds_per_tanggal".
 *
 * @property string $id
 * @property integer $jenis
 * @property string $tanggal
 * @property string $created_at
 *
 * @property \frontend\modules\dashboard\models\DsAbsensi[] $dsAbsensis
 */
class DsPerTanggal extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal'], 'required'],
            [['jenis'], 'integer'],
            [['tanggal', 'created_at'], 'safe'],
            [['id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ds_per_tanggal';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'jenis' => Yii::t('frontend', 'Jenis'),
            'tanggal' => Yii::t('frontend', 'Tanggal'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDsAbsensis()
    {
        return $this->hasMany(\frontend\modules\dashboard\models\DsAbsensi::class, ['ds_per_tanggal_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
        ];
    }
}

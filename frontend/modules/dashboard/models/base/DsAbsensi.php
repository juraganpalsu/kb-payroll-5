<?php

namespace frontend\modules\dashboard\models\base;

use frontend\modules\struktur\models\Departemen;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "ds_absensi".
 *
 * @property integer $id
 * @property integer $status_kehadiran
 * @property string $departemen_id
 * @property string $departemen_string
 * @property integer $jumlah
 * @property string $pegawai_ids
 * @property string $ds_per_tanggal_id
 * @property string $created_at
 *
 * @property Departemen $departemen
 * @property \frontend\modules\dashboard\models\DsPerTanggal $dsPerTanggal
 */
class DsAbsensi extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['departemen_id', 'ds_per_tanggal_id'], 'required'],
            [['id', 'status_kehadiran', 'jumlah'], 'integer'],
            [['created_at', 'pegawai_ids'], 'safe'],
            [['departemen_id', 'ds_per_tanggal_id'], 'string', 'max' => 32],
            [['departemen_string'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ds_absensi';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'status_kehadiran' => Yii::t('frontend', 'Status Kehadiran'),
            'departemen_id' => Yii::t('frontend', 'Departemen ID'),
            'departemen_string' => Yii::t('frontend', 'Departemen String'),
            'jumlah' => Yii::t('frontend', 'Jumlah'),
            'pegawai_ids' => Yii::t('frontend', 'Pegawai Id'),
            'ds_per_tanggal_id' => Yii::t('frontend', 'Ds Per Tanggal ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDepartemen()
    {
        return $this->hasOne(Departemen::class, ['id' => 'departemen_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDsPerTanggal()
    {
        return $this->hasOne(\frontend\modules\dashboard\models\DsPerTanggal::class, ['id' => 'ds_per_tanggal_id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ]
        ];
    }
}

<?php

namespace frontend\modules\dashboard\models\base;

use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "ds_akhir_kontrak".
 *
 * @property string $id
 * @property string $tanggal
 * @property string $created_at
 *
 * @property \frontend\modules\dashboard\models\DsAkhirKontrakDetail[] $dsAkhirKontrakDetails
 */
class DsAkhirKontrak extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal'], 'required'],
            [['tanggal', 'created_at'], 'safe'],
            [['id'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ds_akhir_kontrak';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tanggal' => Yii::t('frontend', 'Tanggal'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDsAkhirKontrakDetails()
    {
        return $this->hasMany(\frontend\modules\dashboard\models\DsAkhirKontrakDetail::class, ['ds_akhir_kontrak_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
        ];
    }
}

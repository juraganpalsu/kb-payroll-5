<?php

namespace frontend\modules\dashboard\models\base;

use common\components\UUIDBehavior;
use frontend\modules\struktur\models\Departemen;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "ds_turn_over_detail".
 *
 * @property string $id
 * @property integer $bulan
 * @property string $bulan_string
 * @property integer $masuk
 * @property integer $resign
 * @property integer $total
 * @property string $ds_turn_over_id
 * @property string $created_at
 *
 * @property Departemen $departemen
 * @property \frontend\modules\dashboard\models\DsTurnOver $dsTurnOver
 */
class DsTurnOverDetail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'ds_turn_over_id'], 'required'],
            [['bulan', 'masuk', 'resign', 'total'], 'integer'],
            [['created_at'], 'safe'],
            [['id', 'ds_turn_over_id'], 'string', 'max' => 36],
            [['bulan_string'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'ds_turn_over_detail';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'bulan' => Yii::t('frontend', 'Bulan'),
            'bulan_string' => Yii::t('frontend', 'Bulan String'),
            'masuk' => Yii::t('frontend', 'Masuk'),
            'resign' => Yii::t('frontend', 'Resign'),
            'total' => Yii::t('frontend', 'Total'),
            'ds_turn_over_id' => Yii::t('frontend', 'Ds Turn Over ID'),
        ];
    }


    /**
     * @return ActiveQuery
     */
    public function getDsTurnOver(): ActiveQuery
    {
        return $this->hasOne(\frontend\modules\dashboard\models\DsTurnOver::class, ['id' => 'ds_turn_over_id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
        ];
    }
}

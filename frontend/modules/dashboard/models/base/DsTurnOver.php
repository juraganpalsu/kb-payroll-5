<?php

namespace frontend\modules\dashboard\models\base;

use common\components\UUIDBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "ds_turn_over".
 *
 * @property string $id
 * @property integer $tahun
 * @property string $created_at
 *
 * @property \frontend\modules\dashboard\models\DsTurnOverDetail[] $dsTurnOverDetails
 */
class DsTurnOver extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id'], 'required'],
            [['tahun'], 'integer'],
            [['created_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'ds_turn_over';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tahun' => Yii::t('frontend', 'Tahun'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDsTurnOverDetails(): ActiveQuery
    {
        return $this->hasMany(\frontend\modules\dashboard\models\DsTurnOverDetail::class, ['ds_turn_over_id' => 'id'])->orderBy('bulan ASC');
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
        ];
    }
}

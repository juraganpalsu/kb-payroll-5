<?php

namespace frontend\modules\dashboard\models\base;

use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "ds_akhir_kontrak_detail".
 *
 * @property string $id
 * @property integer $kontrak
 * @property integer $jumlah
 * @property string $perjanjian_kerja_ids
 * @property string $ds_akhir_kontrak_id
 * @property string $created_at
 *
 * @property \frontend\modules\dashboard\models\DsAkhirKontrak $dsAkhirKontrak
 */
class DsAkhirKontrakDetail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kontrak', 'id', 'ds_akhir_kontrak_id'], 'required'],
            [['jumlah'], 'integer'],
            [['perjanjian_kerja_ids'], 'string'],
            [['created_at'], 'safe'],
            [['id', 'ds_akhir_kontrak_id'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ds_akhir_kontrak_detail';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'kontrak' => Yii::t('frontend', 'Kontrak'),
            'jumlah' => Yii::t('frontend', 'Jumlah'),
            'perjanjian_kerja_ids' => Yii::t('frontend', 'Perjanjian Kerja Ids'),
            'ds_akhir_kontrak_id' => Yii::t('frontend', 'Ds Akhir Kontrak ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDsAkhirKontrak()
    {
        return $this->hasOne(\frontend\modules\dashboard\models\DsAkhirKontrak::class, ['id' => 'ds_akhir_kontrak_id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
        ];
    }
}

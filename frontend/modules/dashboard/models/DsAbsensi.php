<?php

namespace frontend\modules\dashboard\models;

use common\models\Absensi;
use DateInterval;
use DatePeriod;
use DateTime;
use frontend\modules\dashboard\models\base\DsAbsensi as BaseDsAbsensi;

/**
 * This is the model class for table "ds_absensi".
 */
class DsAbsensi extends BaseDsAbsensi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['departemen_id', 'ds_per_tanggal_id'], 'required'],
            [['id', 'status_kehadiran', 'jumlah'], 'integer'],
            [['pegawai_ids', 'created_at'], 'safe'],
            [['departemen_id', 'ds_per_tanggal_id'], 'string', 'max' => 32],
            [['departemen_string'], 'string', 'max' => 225]
        ];
    }

    /**
     * @return array
     */
    public function getDataForDashboard()
    {
        $begin = new DateTime('-7 day');
        $end = new DateTime('-1 day');
        $end = $end->modify('+1 day');

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval, $end);

        $modelAbsensi = new Absensi();

        $data = [];
        foreach ($daterange as $date) {
            $model = DsPerTanggal::findOne(['tanggal' => $date->format('Y-m-d'), 'jenis' => DsPerTanggal::ABSENSI_STATUS_KEHADIRAN]);
            if ($model) {
                foreach ($model->dsAbsensis as $dsAbsensi) {
                    $data[$model->tanggal]['departemen'][$dsAbsensi->departemen_id] = $dsAbsensi->departemen_string;
                    $data[$model->tanggal]['status_kehadiran'][$dsAbsensi->status_kehadiran]['name'] = $modelAbsensi->_status_kehadiran[$dsAbsensi->status_kehadiran];
                    $data[$model->tanggal]['status_kehadiran'][$dsAbsensi->status_kehadiran]['data'][] = $dsAbsensi->jumlah;
                }
            }
        }
        return $data;
    }

}

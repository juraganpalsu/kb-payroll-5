<?php

namespace frontend\modules\dashboard\models;

use Yii;
use \frontend\modules\dashboard\models\base\DsAkhirKontrak as BaseDsAkhirKontrak;

/**
 * This is the model class for table "ds_akhir_kontrak".
 */
class DsAkhirKontrak extends BaseDsAkhirKontrak
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal'], 'required'],
            [['tanggal', 'created_at'], 'safe'],
            [['id'], 'string', 'max' => 32],
        ];
    }

}

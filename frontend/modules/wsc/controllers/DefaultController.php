<?php

namespace frontend\modules\wsc\controllers;

use common\components\ApiRequest;
use Yii;
use yii\base\InvalidConfigException;
use yii\httpclient\Exception;
use yii\web\Controller;

/**
 * Default controller for the `wsc` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLoginApi()
    {
        $url = 'https://hrisservice.hc-kbu.com:8082';
        $token = 'iV0xJKgdQoA2IBFnvU4hsQs_ujiK5mCQ';
        $apiRequest = new ApiRequest($url);
        $apiRequest->useCertificates = true;
        $apiRequest->ca_service = [
            'sslVerifyPeer' => false,
            'sslCafile' => '@common/certificates/hrisservice_hc_kbu_com.crt'
        ];
        $apiRequest->access_token = $token;
        $apiRequest->method = ApiRequest::POST_METHOD;
        try {
            $responseApi = $apiRequest->createRequest()
                ->setData(['username' => 'andratama', 'password' => '*andratama#'])
                ->setUrl($url . '/v1/security/validate')
                ->send();

            if ($responseApi->isOk) {
                echo "OK <br>";
            } else {
                echo "GAGAL <br>";
            }
            print_r($responseApi->data);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } catch (InvalidConfigException $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }
}

<?php

namespace frontend\modules\wsc\controllers;

use frontend\models\User;
use frontend\modules\wsc\models\search\WscSearch;
use frontend\modules\wsc\models\Wsc;
use frontend\modules\wsc\models\WscDetail;
use frontend\modules\wsc\models\WscDetailAttachment;
use frontend\modules\wsc\models\WscDetailDiskusi;
use frontend\modules\wsc\models\WscDetailTagPagawai;
use frontend\modules\wsc\models\WscLocation;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * WscController implements the CRUD actions for Wsc model.
 */
class WscController extends Controller
{

    /**
     * @return array[]
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Wsc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WscSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $indexDetail = Url::to('index-detail');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'indexDetail' => $indexDetail,
        ]);
    }

    /**
     * Lists all Wsc models.
     * @return mixed
     */
    public function actionIndexDetail()
    {
        $searchModel = new WscSearch();
        $dataProvider = $searchModel->searchDetail(Yii::$app->request->queryParams);

        return $this->render('index-detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Lists all Wsc models.
     * @return mixed
     */
    public function actionIndexAll()
    {
        $searchModel = new WscSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
        $indexDetail = Url::to('index-detail-all');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'indexDetail' => $indexDetail,
        ]);
    }

    /**
     * Lists all Wsc models.
     * @return mixed
     */
    public function actionIndexDetailAll()
    {
        $searchModel = new WscSearch();
        $dataProvider = $searchModel->searchDetail(Yii::$app->request->queryParams, true);

        return $this->render('index-detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Wsc model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);

        $providerWscDetail = new ActiveDataProvider([
            'query' => WscDetail::find()->andWhere(['wsc_id' => $model->id])->orderBy('jam_mulai ASC'),
        ]);
        $searchModel = new WscSearch();
        $providerWscDetailTaggingMe = $searchModel->searchWscDetailTaggingMe($model->tanggal);

        $queryLocation = WscLocation::find()->andWhere(['wsc_id' => $model->id])->orderBy('id DESC');
//        $searchModel = new WscSearch();
//        $providerWscDetail = $searchModel->searchDetail($model->id);
        return $this->render('view', [
            'model' => $model,
            'providerWscDetail' => $providerWscDetail,
            'providerWscDetailTaggingMe' => $providerWscDetailTaggingMe,
            'dataLocations' => $queryLocation->all()
        ]);
    }

    /**
     * Displays a single Wsc model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionViewDetail(string $id)
    {
        $model = $this->findModelDetail($id);

        $dataDiskusi = WscDetailDiskusi::find()->andWhere(['wsc_detail_id' => $model->id])->orderBy('created_at DESC')->all();
        $dataPegawais = WscDetailTagPagawai::find()->andWhere(['wsc_detail_id' => $model->id])->orderBy('created_at DESC')->all();
        $fileAttachment = WscDetailAttachment::find()->andWhere(['wsc_detail_id' => $model->id])->orderBy('created_at DESC')->all();
        $idPegawais = [];
        /** @var WscDetailTagPagawai $dataPegawai */
        foreach ($dataPegawais as $dataPegawai) {
            $idPegawais[$dataPegawai->pegawai_id] = $dataPegawai->pegawai->nama_lengkap;
        }

        return $this->renderAjax('view-detail', [
            'model' => $model,
            'dataDiskusi' => $dataDiskusi,
            'idPegawais' => $idPegawais,
            'modelAttachment' => new WscDetailAttachment(),
            'fileAttachment' => $fileAttachment
        ]);
    }

    /**
     * Creates a new Wsc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Wsc();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Wsc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);


        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation(bool $isnew)
    {
        $model = new Wsc();
        $model->isNewRecord = $isnew;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            if ($pegawai = User::pegawai()) {
                $model->pegawai_id = $pegawai->id;
            }
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing Wsc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Wsc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Wsc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id): Wsc
    {
        if (($model = Wsc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return WscDetail
     * @throws NotFoundHttpException
     */
    protected function findModelDetail(string $id): WscDetail
    {
        if (($model = WscDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return WscDetailAttachment
     * @throws NotFoundHttpException
     */
    protected function findModelAttachment(string $id): WscDetailAttachment
    {
        if (($model = WscDetailAttachment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $idwsc
     * @return array|mixed|string
     */
    public function actionCreateDetail(string $idwsc)
    {
        $model = new WscDetail();
        $model->wsc_id = $idwsc;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->wsc_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-detail', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdateDetail(string $id)
    {
        $model = $this->findModelDetail($id);

        if (strtotime(date('Y-m-d H:i:s')) > strtotime($model->wsc->tanggal . ' ' . WscDetail::jamMaksimum) && $model->kategori == WscDetail::direncanakan) {
            throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
        }

        $model->scenario = 'default';

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->wsc_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-detail', [
            'model' => $model,
        ]);
    }


    /**
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouDetailValidation(bool $isnew)
    {
        $model = new WscDetail();
        $model->isNewRecord = $isnew;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public function actionDeleteDetail(string $id): array
    {
        $model = $this->findModelDetail($id);

        if (strtotime(date('Y-m-d H:i:s')) > strtotime($model->wsc->tanggal . ' ' . WscDetail::jamMaksimum) && $model->kategori == WscDetail::direncanakan) {
            throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
        }

        $wscId = $model->wsc_id;
        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $wscId])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];

        }
        return $response->data;
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionSetStatus(string $id)
    {
        $model = $this->findModelDetail($id);

        $model->scenario = 'statusfinish';

        if (strtotime(date('Y-m-d')) > strtotime($model->wsc->tanggal)) {

            throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
        }

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->wsc_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-status', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed
     */
    public function actionCreateDiskusi()
    {
        $model = new WscDetailDiskusi();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view-detail', 'id' => $model->wsc_detail_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
    }


    /**
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouDiskusiValidation(bool $isnew)
    {
        $model = new WscDetailDiskusi();
        $model->isNewRecord = $isnew;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return array|mixed
     */
    public function actionCreatePegawai()
    {
        $model = new WscDetailTagPagawai();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view-detail', 'id' => $model->wsc_detail_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouPegawaiValidation()
    {
        $model = new WscDetailTagPagawai();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public function actionDeletePegawai(string $id): array
    {
        $model = $this->findModelDetail($id);


        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $wscDetailTagPagawai = WscDetailTagPagawai::findOne(['wsc_detail_id' => $model->id, 'pegawai_id' => $request->post('pegawai_id')]);
            if ($wscDetailTagPagawai && $wscDetailTagPagawai->created_by == Yii::$app->user->id) {
                $wscDetailTagPagawai->delete();
                $response->data = ['data' => ['url' => Url::to(['view-detail', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil delete data.'), 'status' => true];
            }
            $response->data = ['data' => ['url' => Url::to(['view-detail', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Gagal delete data.'), 'status' => true];

        }
        return $response->data;
    }

    /**
     * @return string
     */
    public function actionIndexMyTeam()
    {
        $searchModel = new WscSearch();
        $dataProvider = $searchModel->searchMyTeam(Yii::$app->request->queryParams);

        return $this->render('index-my-team', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexTaggingMe()
    {
        $searchModel = new WscSearch();
        $dataProvider = $searchModel->searchTaggingMe(Yii::$app->request->queryParams);
        $modelDetail = new WscDetail();

        return $this->render('index-tagging-me', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelDetail' => $modelDetail
        ]);
    }

    /**
     * @return array|mixed
     */
    public function actionUploadAttachment()
    {
        $model = new WscDetailAttachment();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'status' => false, 'message' => Yii::t('frontend', 'Gagal Upload Data')];
        if ($request->isPost && $request->isAjax) {
            $model->load($request->post());
            $namafile = UploadedFile::getInstance($model, 'nama_file');
            if ($model->upload($namafile)) {
                $response->data = ['data' => ['url' => Url::to(['view-detail', 'id' => $model->wsc_detail_id])], 'status' => true, 'message' => Yii::t('frontend', 'Berhasil Upload Data')];
            }
        }
        return $response->data;
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadAttachmentValidation()
    {
        $model = new WscDetailAttachment();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public function actionDeleteAttachment(string $id)
    {
        $model = $this->findModelAttachment($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            if ($model->created_by == Yii::$app->user->id) {
                $model->delete();
                $response->data = ['data' => ['url' => Url::to(['view-detail', 'id' => $model->wsc_detail_id])], 'message' => Yii::t('frontend', 'Berhasil delete data.'), 'status' => true];
            }
            $response->data = ['data' => ['url' => Url::to(['view-detail', 'id' => $model->wsc_detail_id])], 'message' => Yii::t('frontend', 'Gagal delete data.'), 'status' => true];

        }
        return $response->data;
    }

    /**
     * @param string $id
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     */
    public function actionViewAttachment(string $id)
    {
        $model = $this->findModelAttachment($id);
        $filePath = $model->upload_dir . $model->wsc_detail_id . '/' . $model->id;
        if (file_exists($filePath)) {
            return Yii::$app->response->sendFile($filePath, $model->nama_file, ['inline' => false]);
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'File Tidak Tersedia'));
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionTambahkanKetanggal(string $id)
    {
        $model = $this->findModelDetail($id);

        if (strtotime(date('Y-m-d H:i:s')) <= strtotime($model->wsc->tanggal . ' ' . WscDetail::jamMaksimum) && $model->kategori == WscDetail::direncanakan) {
            throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
        }

        $model->scenario = 'tambahkanketanggal';

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $model->createDetail();
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->wsc_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-tambahkan-ketanggal', [
            'model' => $model,
        ]);
    }


    /**
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionTambahkanValidation(bool $isnew)
    {
        $model = new WscDetail();
        $model->scenario = 'tambahkanketanggal';
        $model->isNewRecord = $isnew;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }
}


<?php

namespace frontend\modules\wsc\models;

use frontend\models\User;
use frontend\modules\wsc\models\base\WscDetailDiskusi as BaseWscDetailDiskusi;

/**
 * This is the model class for table "wsc_detail_diskusi".
 */
class WscDetailDiskusi extends BaseWscDetailDiskusi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wsc_detail_id', 'komentar'], 'required'],
            [['komentar'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'wsc_detail_id', 'wsc_detail_diskusi_id'], 'string', 'max' => 36],
            [['pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($pegawai = User::pegawai()) {
            $this->pegawai_id = $pegawai->id;
        }
        return parent::beforeValidate();
    }


}

<?php

namespace frontend\modules\wsc\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "wsc".
 *
 * @property string $id
 * @property string $tanggal
 * @property string $pegawai_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $perjanjian_kerja_id
 * @property string $keterangan
 * @property boolean $is_started
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Pegawai $pegawai
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 * @property \frontend\modules\wsc\models\WscDetail[] $wscDetails
 * @property \frontend\modules\wsc\models\WscLocation[] $wscLocations
 * @property \frontend\modules\wsc\models\WscPictureProof[] $wscPictureProofs
 */
class Wsc extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'tanggal', 'pegawai_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'perjanjian_kerja_id'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 36],
            [['pegawai_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'perjanjian_kerja_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'wsc';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock(): string
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tanggal' => Yii::t('frontend', 'Tanggal'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'is_started' => Yii::t('frontend', 'Is Started'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWscDetails()
    {
        return $this->hasMany(\frontend\modules\wsc\models\WscDetail::class, ['wsc_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWscLocations()
    {
        return $this->hasMany(\frontend\modules\wsc\models\WscLocation::class, ['wsc_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWscPictureProofs()
    {
        return $this->hasMany(\frontend\modules\wsc\models\WscPictureProof::class, ['wsc_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => \common\components\UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

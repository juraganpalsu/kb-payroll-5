<?php

namespace frontend\modules\wsc\models\base;

use common\components\CreatedByBehavior;
use common\components\UUIDBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "wsc_detail_diskusi".
 *
 * @property string $id
 * @property string $wsc_detail_id
 * @property string $pegawai_id
 * @property string $komentar
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 * @property string $wsc_detail_diskusi_id
 *
 * @property Pegawai $pegawai
 * @property \frontend\modules\wsc\models\WscDetail $wscDetail
 * @property \frontend\modules\wsc\models\WscDetailDiskusi $wscDetailDiskusi
 * @property \frontend\modules\wsc\models\WscDetailDiskusi[] $wscDetailDiskusis
 */
class WscDetailDiskusi extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'wsc_detail_id', 'pegawai_id', 'komentar'], 'required'],
            [['komentar'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'wsc_detail_id', 'wsc_detail_diskusi_id'], 'string', 'max' => 36],
            [['pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wsc_detail_diskusi';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'wsc_detail_id' => Yii::t('frontend', 'Wsc Detail ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'komentar' => Yii::t('frontend', 'Komentar'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
            'wsc_detail_diskusi_id' => Yii::t('frontend', 'Wsc Detail Diskusi ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWscDetail()
    {
        return $this->hasOne(\frontend\modules\wsc\models\WscDetail::class, ['id' => 'wsc_detail_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWscDetailDiskusi()
    {
        return $this->hasOne(\frontend\modules\wsc\models\WscDetailDiskusi::class, ['id' => 'wsc_detail_diskusi_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWscDetailDiskusis()
    {
        return $this->hasMany(\frontend\modules\wsc\models\WscDetailDiskusi::class, ['wsc_detail_diskusi_id' => 'id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}
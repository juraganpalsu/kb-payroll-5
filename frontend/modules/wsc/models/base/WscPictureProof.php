<?php

namespace frontend\modules\wsc\models\base;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "wsc_picture_proof".
 *
 * @property integer $id
 * @property string $name
 * @property string $time
 * @property string $wsc_id
 *
 * @property \frontend\modules\wsc\models\Wsc $wsc
 */
class WscPictureProof extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'time', 'wsc_id'], 'required'],
            [['time'], 'safe'],
            [['name'], 'string', 'max' => 225],
            [['wsc_id'], 'string', 'max' => 36],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wsc_picture_proof';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'name' => Yii::t('frontend', 'Name'),
            'time' => Yii::t('frontend', 'Time'),
            'wsc_id' => Yii::t('frontend', 'Wsc ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getWsc()
    {
        return $this->hasOne(\frontend\modules\wsc\models\Wsc::class, ['id' => 'wsc_id']);
    }

}

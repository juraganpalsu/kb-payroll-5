<?php

namespace frontend\modules\wsc\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "wsc_detail".
 *
 * @property string $id
 * @property string $nama_pekerjaan
 * @property integer $durasi
 * @property string $jam_mulai
 * @property string $jam_selesai
 * @property integer $prioritas
 * @property string $deskripsi
 * @property integer $status_finish
 * @property string $keterangan
 * @property integer $kategori
 * @property string $tambahkan_ketanggal
 * @property string $wsc_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\wsc\models\Wsc $wsc
 * @property User $createdBy
 */
class WscDetail extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_pekerjaan', 'jam_mulai', 'jam_selesai', 'prioritas', 'wsc_id'], 'required'],
            [['jam_mulai', 'jam_selesai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['prioritas', 'status_finish', 'kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['deskripsi', 'keterangan'], 'string'],
            [['id', 'wsc_id'], 'string', 'max' => 36],
            [['nama_pekerjaan'], 'string', 'max' => 225],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wsc_detail';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'nama_pekerjaan' => Yii::t('frontend', 'Nama Pekerjaan'),
            'durasi' => Yii::t('frontend', 'Durasi'),
            'jam_mulai' => Yii::t('frontend', 'Jam Mulai'),
            'jam_selesai' => Yii::t('frontend', 'Jam Selesai'),
            'prioritas' => Yii::t('frontend', 'Prioritas'),
            'deskripsi' => Yii::t('frontend', 'Deskripsi'),
            'status_finish' => Yii::t('frontend', 'Status Finish'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'kategori' => Yii::t('frontend', 'Kategori'),
            'tambahkan_ketanggal' => Yii::t('frontend', 'Tambahkan Ketanggal'),
            'wsc_id' => Yii::t('frontend', 'Wsc ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getWsc()
    {
        return $this->hasOne(\frontend\modules\wsc\models\Wsc::class, ['id' => 'wsc_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => \common\components\UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace frontend\modules\wsc\models\base;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "wsc_location".
 *
 * @property integer $id
 * @property string $latitude
 * @property string $longitude
 * @property string $time
 * @property string $wsc_id
 *
 * @property \frontend\modules\wsc\models\Wsc $wsc
 */
class WscLocation extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude', 'time', 'wsc_id'], 'required'],
            [['time'], 'safe'],
            [['latitude', 'longitude'], 'string', 'max' => 45],
            [['wsc_id'], 'string', 'max' => 36],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wsc_location';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'latitude' => Yii::t('frontend', 'Latitude'),
            'longitude' => Yii::t('frontend', 'Longitude'),
            'time' => Yii::t('frontend', 'Time'),
            'wsc_id' => Yii::t('frontend', 'Wsc ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getWsc(): ActiveQuery
    {
        return $this->hasOne(\frontend\modules\wsc\models\Wsc::class, ['id' => 'wsc_id']);
    }

}

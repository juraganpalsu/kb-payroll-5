<?php

namespace frontend\modules\wsc\models\search;

use frontend\models\User;
use frontend\modules\wsc\models\Wsc;
use frontend\modules\wsc\models\WscDetail;
use frontend\modules\wsc\models\WscDetailTagPagawai;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\wsc\models\search\WscSearch represents the model behind the search form about `frontend\modules\wsc\models\Wsc`.
 *
 * @property string $nama_pekerjaan
 * @property integer $prioritas
 * @property string $deskripsi
 * @property string $departemen
 * @property integer $kategori
 * @property integer $status_finish
 */
class WscSearch extends Wsc
{

    public $nama_pekerjaan;
    public $prioritas;
    public $deskripsi;
    public $kategori;
    public $status_finish;
    public $departemen;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'tanggal', 'pegawai_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'perjanjian_kerja_id', 'keterangan', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'departemen'], 'safe'],
            [['nama_pekerjaan', 'prioritas', 'deskripsi', 'kategori', 'status_finish'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, bool $all = false)
    {
        $query = Wsc::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!$all) {
            if ($modelPegawai = User::pegawai()) {
                $query->andWhere(['pegawai_id' => $modelPegawai->id]);
            }
        }


        if (!empty($this->pegawai_id)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                $query->andWhere(['like', 'nama_lengkap', $this->pegawai_id]);
            }]);
        }
        
        if (!empty($this->departemen)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) {
            $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) {
                $query->joinWith(['struktur' => function (ActiveQuery $query) {
                    $query->joinWith(['departemen' => function (ActiveQuery $query) {
                        $query->andWhere(['like', 'departemen.nama', $this->departemen]);
                    }]);
                }]);
            }]);
        }]);
        }

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }

        $query->andFilterWhere([
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        $query->orderBy('tanggal DESC');

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDetail(array $params, bool $all = false)
    {
        $query = WscDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['wsc' => function (ActiveQuery $query) {
        }]);

        if (!$all) {
            if ($modelPegawai = User::pegawai()) {
                $query->andWhere(['pegawai_id' => $modelPegawai->id]);
            }
        }


        if (!empty($this->pegawai_id)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                $query->andWhere(['like', 'nama_lengkap', $this->pegawai_id]);
            }]);
        }

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }

        $query->andFilterWhere([
            'lock' => $this->lock,
        ]);


        $query->orderBy('tanggal DESC');

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchMyTeam($params)
    {
        $query = Wsc::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        $query->joinWith(['pegawai' => function (ActiveQuery $query) use ($user) {
            $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) use ($user) {
                $query->andWhere(['IN', 'struktur_id', $user->getStrukturBawahan()]);
            }]);
        }]);

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        $query->orderBy('tanggal DESC');

        return $dataProvider;
    }


    /**
     * @param string $tanggal
     * @return ActiveDataProvider
     */
    public function searchWscDetailTaggingMe(string $tanggal)
    {
        $detailIds = WscDetail::find()->select('wsc_detail.id')->joinWith('wsc')->andWhere(['wsc.tanggal' => $tanggal]);
        $query = WscDetailTagPagawai::find();
        $query->andWhere(['IN', 'wsc_detail_id', $detailIds]);
        if ($modelPegawai = User::pegawai()) {
            $query->andWhere(['pegawai_id' => $modelPegawai->id]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }


    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchTaggingMe(array $params)
    {
        $query = WscDetailTagPagawai::find();
        if ($modelPegawai = User::pegawai()) {
            $query->andWhere(['wsc_detail_tag_pagawai.pegawai_id' => $modelPegawai->id]);
        }

        $query->joinWith(['wscDetail' => function (ActiveQuery $query) {
            $query->joinWith(['wsc' => function (ActiveQuery $query) {
            }]);
        }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'wsc.tanggal', $awal, $akhir]);
        }

        $query->andFilterWhere(['LIKE', 'wsc_detail.nama_pekerjaan', $this->nama_pekerjaan])
            ->andFilterWhere(['LIKE', 'wsc_detail.deskripsi', $this->deskripsi]);

        if (!empty($this->prioritas)) {
            $query->andWhere(['wsc_detail.prioritas' => $this->prioritas]);
        }
        if (!empty($this->kategori)) {
            $query->andWhere(['wsc_detail.kategori' => $this->kategori]);
        }
        if (!empty($this->status_finish)) {
            $query->andWhere(['wsc_detail.status_finish' => $this->status_finish]);
        }

        $query->orderBy('wsc.tanggal DESC');
        return $dataProvider;
    }

    /**
     * @return int
     */
    public static function countMyActiveActivity(): int
    {
        $pegawaiId = 0;

        if ($modelPegawai = User::pegawai()) {
            $pegawaiId = $modelPegawai->id;
        }

        $countMyactivity = WscDetail::find()->joinWith('wsc')
            ->andWhere(['wsc.pegawai_id' => $pegawaiId])
            ->andWhere(['>=', 'wsc.tanggal', date('Y-m-d')])->count();

        return (int)$countMyactivity;
    }

    /**
     * @return int
     */
    public static function countTaggingMe(): int
    {
        $pegawaiId = 0;

        if ($modelPegawai = User::pegawai()) {
            $pegawaiId = $modelPegawai->id;
        }

        $countTaggingMe = WscDetailTagPagawai::find()
            ->andWhere(['wsc_detail_tag_pagawai.pegawai_id' => $pegawaiId])
            ->joinWith(['wscDetail' => function (ActiveQuery $query) {
                $query->joinWith(['wsc' => function (ActiveQuery $query) {
                    $query->andWhere(['>=', 'wsc.tanggal', date('Y-m-d')]);
                }]);
            }])->count();

        return (int)$countTaggingMe;
    }
}

<?php

namespace frontend\modules\wsc\models;

use DateTime;
use Exception;
use frontend\models\User;
use frontend\modules\wsc\models\base\WscDetail as BaseWscDetail;
use Yii;

/**
 * This is the model class for table "wsc_detail".
 *
 * @property array $_statusFinish
 *
 *
 */
class WscDetail extends BaseWscDetail
{

    const created = 0;
    const stuck = 1;
    const done = 2;
    const cancel = 3;
    const progress = 4;

    public $_statusFinish = [self::created => '-', self::stuck => 'STUCK', self::done => 'DONE', self::cancel => 'CANCEL', self::progress => 'PROGRESS'];

    const low = 1;
    const medium = 2;
    const hight = 3;

    public $_prioritas = [self::low => 'LOW', self::medium => 'MEDIUM', self::hight => 'HIGH'];

    const direncanakan = 1;
    const dadakan = 2;

    public $_kategori = [self::direncanakan => 'DIRENCANAKAN', self::dadakan => 'DADAKAN'];

    const jamMaksimum = '07:45';

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['nama_pekerjaan', 'jam_mulai', 'jam_selesai', 'prioritas', 'wsc_id', 'tambahkan_ketanggal'], 'required'],
            [['jam_mulai', 'jam_selesai', 'tambahkan_ketanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['prioritas', 'status_finish', 'kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['deskripsi', 'keterangan'], 'string'],
            [['id', 'wsc_id'], 'string', 'max' => 36],
            [['nama_pekerjaan'], 'string', 'max' => 225],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock', 'status_finish', 'durasi'], 'default', 'value' => 0],
            [['kategori'], 'default', 'value' => self::direncanakan],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['jam_mulai', 'validasiMulaiSelesai'],
            ['tambahkan_ketanggal', 'tambahkanKetanggalValidation']
        ];
    }

    /**
     * @return array|array[]
     */
    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['default'] = ['nama_pekerjaan', 'jam_mulai', 'jam_selesai', 'prioritas', 'wsc_id', 'deskripsi'];
        $scenario['statusfinish'] = ['status_finish', 'keterangan'];
        $scenario['tambahkanketanggal'] = ['tambahkan_ketanggal', 'wsc_id'];
        return $scenario;
    }

    /**
     * @param string $attribute
     */
    public function tambahkanKetanggalValidation(string $attribute)
    {
        $model = Wsc::findOne(['tanggal' => $this->$attribute, 'pegawai_id' => $this->wsc->pegawai_id]);
        if (!$model) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal yang dituju tidak terdapat jadwal WFH'));
        }
    }


    /**
     * @param string $attribute
     */
    public function validasiMulaiSelesai(string $attribute)
    {
        try {
            $mulai = new DateTime($this->jam_mulai);
            $selesai = new Datetime($this->jam_selesai);

            if ($mulai >= $selesai) {
                $this->addError($attribute, Yii::t('frontend', 'Jam mulai harus lebih kecil dari jam selesai'));
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            if (strtotime(date('Y-m-d H:i:s')) <= strtotime($this->wsc->tanggal . ' ' . self::jamMaksimum)) {
                $this->kategori = self::direncanakan;
            } else {
                $this->kategori = self::dadakan;
            }
        }
        try {
            $mulai = new DateTime($this->jam_mulai);
            $selesai = new Datetime($this->jam_selesai);

            $diff = $mulai->diff($selesai);

            $this->durasi = ((int)$diff->h * 60) + $diff->i;

        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function getHaveAccess(): bool
    {
        $pegawaiId = '';
        if ($modelPegawai = User::pegawai()) {
            $pegawaiId = $modelPegawai->id;
        }
        $dataPegawais = WscDetailTagPagawai::find()->select('pegawai_id')->andWhere(['wsc_detail_id' => $this->id])->column();
//        $employeeHaveAccess = (array)array_push($dataPegawais, $this->wsc->pegawai_id);

        if (in_array($pegawaiId, $dataPegawais)) {
            return true;
        }
        return false;
    }

    /**
     * @void
     */
    public function createDetail()
    {
        $model = new WscDetail();
        $model->setAttributes($this->attributes);
        $check = Wsc::findOne(['tanggal' => $this->tambahkan_ketanggal, 'pegawai_id' => $this->wsc->pegawai_id]);
        if ($check) {
            $model->wsc_id = $check->id;
            $model->save();
        }
    }


}

<?php

namespace frontend\modules\wsc\models;

use frontend\models\User;
use frontend\modules\wsc\models\base\WscDetailAttachment as BaseWscDetailAttachment;
use yii\web\UploadedFile;

/**
 * This is the model class for table "wsc_detail_attachment".
 */
class WscDetailAttachment extends BaseWscDetailAttachment
{

    public $upload_dir = 'uploads/wsc-attachment/';

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['wsc_detail_id', 'pegawai_id'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'wsc_detail_id'], 'string', 'max' => 36],
            [['nama_file'], 'string', 'max' => 225],
            [['pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($modelPegawai = User::pegawai()) {
            $this->pegawai_id = $modelPegawai->id;
        }
        return parent::beforeValidate();
    }

    /**
     * Upload file
     *
     * @param UploadedFile $namafile
     * @return bool
     */
    public function upload(UploadedFile $namafile): bool
    {
        if ($this->validate()) {
            $this->nama_file = $namafile->name;
            if ($this->save()) {
                $this->createDirectory();
                $namafile->saveAs($this->upload_dir . $this->id);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Create directory
     */
    public function createDirectory(): void
    {
        $dir = $this->upload_dir . $this->wsc_detail_id;
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        $this->upload_dir = $dir . '/';
    }
}

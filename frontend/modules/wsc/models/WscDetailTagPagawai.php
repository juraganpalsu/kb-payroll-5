<?php

namespace frontend\modules\wsc\models;

use frontend\modules\wsc\models\base\WscDetailTagPagawai as BaseWscDetailTagPagawai;

/**
 * This is the model class for table "wsc_detail_tag_pagawai".
 */
class WscDetailTagPagawai extends BaseWscDetailTagPagawai
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['wsc_detail_id', 'pegawai_id'], 'required'],
            [['permission', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'wsc_detail_id'], 'string', 'max' => 36],
            [['pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'unique', 'targetAttribute' => ['pegawai_id', 'wsc_detail_id'], 'message' => \Yii::t('frontend', 'Duplikat data.')]
        ];
    }

}

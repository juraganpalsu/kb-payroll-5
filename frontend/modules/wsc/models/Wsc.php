<?php

namespace frontend\modules\wsc\models;

use common\components\Helper;
use DateTime;
use Exception;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\wsc\models\base\Wsc as BaseWsc;
use Yii;

/**
 * This is the model class for table "wsc".
 *
 *
 * @property string $title
 */
class Wsc extends BaseWsc
{


    const jamMaksimal = '07:45:00';


    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['tanggal'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['is_started', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id'], 'string', 'max' => 36],
            [['pegawai_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'perjanjian_kerja_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'is_started'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'pegawaiAktifValidation'],
            ['tanggal', 'validasiTanggal'],
            ['pegawai_id', 'doubleValidation'],

        ];
    }

    /**
     * @param string $attribute
     */
    public function doubleValidation(string $attribute)
    {
        if ($pegawai = User::pegawai()) {
            $this->pegawai_id = $pegawai->id;
            $check = self::findOne(['pegawai_id' => $this->$attribute, 'tanggal' => $this->tanggal]);
            if ($check && $this->isNewRecord) {
                $this->addError($attribute, Yii::t('frontend', 'Anda telah memiliki data WSC pada tanggal ini.'));
            }
        } else {
            $this->addError($attribute, Yii::t('frontend', 'User anda belum terhubung dengan data pegawai.'));
        }
    }


    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tanggal)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     *
     * Validasi Back Date
     *
     * @param string $attribute
     */
    public function validasiTanggal(string $attribute)
    {
        try {
            //hari ini pukul 07:45:00 /self::jamMaksimal
            $waktuMaksimal = new DateTime(self::jamMaksimal);

            $waktuDibuat = new Datetime($this->tanggal . ' ' . date('h:i:s'));

//            if ($waktuMaksimal > $waktuDibuat) {
//                $this->addError($attribute, Yii::t('frontend', 'Batas pembuatan adalah {pukul}', ['pukul' => $waktuMaksimal->format('d-m-Y h:i:s')]));
//            }

            if (self::findOne(['tanggal' => $this->$attribute, 'pegawai_id' => $this->pegawai_id]) && $this->isNewRecord) {
                $this->addError($attribute, Yii::t('frontend', 'Jadwal anda telah ada.'));
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }


    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($pegawai = User::pegawai()) {
            $this->pegawai_id = $pegawai->id;
        }
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->pegawai->nama_lengkap . ' ' . Helper::idDate($this->tanggal);
    }


}

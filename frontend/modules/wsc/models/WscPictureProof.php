<?php

namespace frontend\modules\wsc\models;

use frontend\modules\wsc\models\base\WscPictureProof as BaseWscPictureProof;

/**
 * This is the model class for table "wsc_picture_proof".
 */
class WscPictureProof extends BaseWscPictureProof
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name', 'time', 'wsc_id'], 'required'],
            [['time'], 'safe'],
            [['name'], 'string', 'max' => 225],
            [['wsc_id'], 'string', 'max' => 36],
        ];
    }

}

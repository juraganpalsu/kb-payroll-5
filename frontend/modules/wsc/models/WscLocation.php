<?php

namespace frontend\modules\wsc\models;

use frontend\modules\wsc\models\base\WscLocation as BaseWscLocation;

/**
 * This is the model class for table "wsc_location".
 */
class WscLocation extends BaseWscLocation
{

    const baseUrlLocation = 'https://www.google.com/maps/search/?api=1&query=';

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['latitude', 'longitude', 'time', 'wsc_id'], 'required'],
            [['time'], 'safe'],
            [['latitude', 'longitude'], 'string', 'max' => 45],
            [['wsc_id'], 'string', 'max' => 36],
        ];
    }

}

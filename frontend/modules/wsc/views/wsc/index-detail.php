<?php


/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\wsc\models\search\WscSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Helper;
use frontend\modules\wsc\models\Wsc;
use frontend\modules\wsc\models\WscDetail;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Data WFH Detail');
$this->params['breadcrumbs'][] = $this->title;

?>

<?php
$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    ['attribute' => 'id', 'visible' => false],
    [
        'attribute' => 'tanggal',
        'value' => function (WscDetail $model) {
            return Html::a(Html::tag('strong', Helper::idDate($model->wsc->tanggal)), ['view', 'id' => $model->wsc->id], ['class' => 'font-weight-bold', 'title' => 'View Detail', 'data' => ['pjax' => 0]]);
        },
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'name' => 'range_tanggal',
            'value' => '',
            'convertFormat' => true,
            'pluginOptions' => [
                'locale' => [
                    'format' => 'd-m-Y',
                    'separator' => ' s/d ',
                ],
                'opens' => 'left'
            ]
        ],
        'format' => 'raw'
    ],
    [
        'attribute' => 'pegawai_id',
        'label' => Yii::t('frontend', 'Pegawai'),
        'value' => function (WscDetail $model) {
            return $model->wsc->pegawai->nama_lengkap;
        },
    ],
    'nama_pekerjaan',
    [
        'attribute' => 'prioritas',
        'value' => function (WscDetail $model) {
            return $model->_prioritas[$model->prioritas];
        }
    ],
    'jam_mulai',
    'jam_selesai',
    'durasi',
    [
        'label' => Yii::t('frontend', 'Status'),
        'value' => function (WscDetail $model) {
                return $model->_statusFinish[$model->status_finish];
        },
        'format' => 'raw'
    ],
    [
        'attribute' => 'kategori',
        'value' => function (WscDetail $model) {
            return $model->_kategori[$model->kategori];
        }
    ],
    'deskripsi',
    ['attribute' => 'lock', 'visible' => false],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view}'
    ],
];
?>
<div>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'noExportColumns' => [1, 6], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-WFH' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-wsc']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

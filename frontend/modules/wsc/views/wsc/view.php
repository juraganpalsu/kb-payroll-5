<?php

use common\components\Helper;
use frontend\modules\wsc\models\WscDetail;
use frontend\modules\wsc\models\WscDetailTagPagawai;
use frontend\modules\wsc\models\WscLocation;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\wsc\models\Wsc */

/**
 *
 * @var ActiveDataProvider $providerWscDetail
 * @var ActiveDataProvider $providerWscDetailTaggingMe
 * @var array $dataLocations
 *
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Wsc'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    
});
JS;

$this->registerJs($js);

$css = <<<CSS
/* Timeline */
.timeline,
.timeline-horizontal {
  list-style: none;
  padding: 20px;
  position: relative;
}
.timeline:before {
  top: 40px;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: #eeeeee;
  left: 50%;
  margin-left: -1.5px;
}
.timeline .timeline-item {
  margin-bottom: 20px;
  position: relative;
}
.timeline .timeline-item:before,
.timeline .timeline-item:after {
  content: "";
  display: table;
}
.timeline .timeline-item:after {
  clear: both;
}
.timeline .timeline-item .timeline-badge {
  color: #fff;
  width: 54px;
  height: 54px;
  line-height: 52px;
  font-size: 22px;
  text-align: center;
  position: absolute;
  top: 18px;
  left: 50%;
  margin-left: -25px;
  background-color: #7c7c7c;
  border: 3px solid #ffffff;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
}
.timeline .timeline-item .timeline-badge i,
.timeline .timeline-item .timeline-badge .fa,
.timeline .timeline-item .timeline-badge .glyphicon {
  top: 2px;
  left: 0px;
}
.timeline .timeline-item .timeline-badge.primary {
  background-color: #1f9eba;
}
.timeline .timeline-item .timeline-badge.info {
  background-color: #5bc0de;
}
.timeline .timeline-item .timeline-badge.success {
  background-color: #59ba1f;
}
.timeline .timeline-item .timeline-badge.warning {
  background-color: #d1bd10;
}
.timeline .timeline-item .timeline-badge.danger {
  background-color: #ba1f1f;
}
.timeline .timeline-item .timeline-panel {
  position: relative;
  width: 46%;
  float: left;
  right: 16px;
  border: 1px solid #c0c0c0;
  background: #ffffff;
  border-radius: 2px;
  padding: 20px;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
.timeline .timeline-item .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -16px;
  display: inline-block;
  border-top: 16px solid transparent;
  border-left: 16px solid #c0c0c0;
  border-right: 0 solid #c0c0c0;
  border-bottom: 16px solid transparent;
  content: " ";
}
.timeline .timeline-item .timeline-panel .timeline-title {
  margin-top: 0;
  color: inherit;
}
.timeline .timeline-item .timeline-panel .timeline-body > p,
.timeline .timeline-item .timeline-panel .timeline-body > ul {
  margin-bottom: 0;
}
.timeline .timeline-item .timeline-panel .timeline-body > p + p {
  margin-top: 5px;
}
.timeline .timeline-item:last-child:nth-child(even) {
  float: right;
}
.timeline .timeline-item:nth-child(even) .timeline-panel {
  float: right;
  left: 16px;
}
.timeline .timeline-item:nth-child(even) .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}
.timeline-horizontal {
  list-style: none;
  position: relative;
  padding: 20px 0px 20px 0px;
  display: inline-block;
}
.timeline-horizontal:before {
  height: 3px;
  top: auto;
  bottom: 26px;
  left: 56px;
  right: 0;
  width: 100%;
  margin-bottom: 20px;
}
.timeline-horizontal .timeline-item {
  display: table-cell;
  height: 150px;
  width: 20%;
  min-width: 320px;
  float: none !important;
  padding-left: 0px;
  padding-right: 20px;
  margin: 0 auto;
  vertical-align: bottom;
}
.timeline-horizontal .timeline-item .timeline-panel {
  top: auto;
  bottom: 64px;
  display: inline-block;
  float: none !important;
  left: 0 !important;
  right: 0 !important;
  width: 100%;
  margin-bottom: 20px;
}
.timeline-horizontal .timeline-item .timeline-panel:before {
  top: auto;
  bottom: -16px;
  left: 28px !important;
  right: auto;
  border-right: 16px solid transparent !important;
  border-top: 16px solid #c0c0c0 !important;
  border-bottom: 0 solid #c0c0c0 !important;
  border-left: 16px solid transparent !important;
}
.timeline-horizontal .timeline-item:before,
.timeline-horizontal .timeline-item:after {
  display: none;
}
.timeline-horizontal .timeline-item .timeline-badge {
  top: auto;
  bottom: 0px;
  left: 43px;
}
CSS;

$this->registerCss($css);


?>
    <div class="wsc-view">

        <div class="row">
            <div class="col-sm-9">
                <?php
                if (strtotime(date('Y-m-d')) <= strtotime($model->tanggal) && $model->created_by == Yii::$app->user->id) {
                    ?>
                    <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                <?php } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-5">
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'tanggal',
                        'value' => empty($model->wscLocations) && $model->tanggal == date('Y-m-d') ? Html::tag('span', Helper::idDate($model->tanggal) . '<br>' . Yii::t('frontend', 'Belum terdapat catatan lokasi, silakan aktifkan lokasi pada aplikasi mobile HRIS anda.'), ['class' => 'text-bold text-danger']) : Helper::idDate($model->tanggal),
                        'format' => 'html'
                    ],
                    [
                        'attribute' => 'pegawai.nama_lengkap',
                        'label' => Yii::t('frontend', 'Pegawai'),
                    ],
                    'keterangan:ntext',
                    ['attribute' => 'lock', 'visible' => false],
                ];
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php
                if (strtotime(date('Y-m-d')) <= strtotime($model->tanggal)) {
                    echo Html::a(Yii::t('frontend', 'Tambah Detail'), ['create-detail', 'idwsc' => $model->id], ['class' => 'btn btn-primary btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Tambahkan Pekerjaan')]]);
                } ?>
                <?php
                $gridColumnDetail = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'nama_pekerjaan',
                        'value' => function (WscDetail $model) {
//                            if ($model->getHaveAccess()) {
                            return Html::a($model->nama_pekerjaan, ['view-detail', 'id' => $model->id], ['class' => 'detail-pekerjaan text-bold btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Detail pekerjaan {pekerjaan}', ['pekerjaan' => $model->nama_pekerjaan])]]);
//                            }
//                            return $model->nama_pekerjaan;
                        },
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'jam_mulai',
                        'value' => function (WscDetail $model) {
                            return $model->jam_mulai;
                        }
                    ],
                    [
                        'attribute' => 'jam_selesai',
                        'value' => function (WscDetail $model) {
                            return $model->jam_selesai;
                        }
                    ],
                    [
                        'attribute' => 'durasi',
                        'value' => function (WscDetail $model) {
                            return $model->durasi . ' menit';
                        }
                    ],
                    [
                        'attribute' => 'prioritas',
                        'value' => function (WscDetail $model) {
                            return $model->_prioritas[$model->prioritas];
                        }
                    ],
                    [
                        'attribute' => 'deskripsi',
                        'value' => function (WscDetail $model) {
                            return $model->deskripsi;
                        }
                    ],
                    [
                        'attribute' => 'kategori',
                        'value' => function (WscDetail $model) {
                            return $model->_kategori[$model->kategori];
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Status'),
                        'value' => function (WscDetail $model) {
                            if ($model->created_by != Yii::$app->user->id) {
                                return $model->_statusFinish[$model->status_finish];
                            }
                            if (strtotime($model->wsc->tanggal) < strtotime(date('Y-m-d')) || strtotime($model->wsc->tanggal) > strtotime(date('Y-m-d'))) {
                                return $model->_statusFinish[$model->status_finish];
                            }
                            if ($model->status_finish == WscDetail::done) {
                                return Html::a($model->_statusFinish[$model->status_finish], ['set-status', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-call-modal btn-block', 'data' => ['header' => Yii::t('frontend', 'Update status {nama_pekerjaan}', ['nama_pekerjaan' => $model->nama_pekerjaan])]]) . "<br>" . $model->keterangan;
                            }
                            if ($model->status_finish == WscDetail::stuck) {
                                return Html::a($model->_statusFinish[$model->status_finish], ['set-status', 'id' => $model->id], ['class' => 'btn btn-danger btn-xs btn-call-modal btn-block', 'data' => ['header' => Yii::t('frontend', 'Update status {nama_pekerjaan}', ['nama_pekerjaan' => $model->nama_pekerjaan])]]) . "<br>" . $model->keterangan;
                            }
                            if ($model->status_finish == WscDetail::cancel) {
                                return Html::a($model->_statusFinish[$model->status_finish], ['set-status', 'id' => $model->id], ['class' => 'btn btn-warning btn-xs btn-call-modal btn-block', 'data' => ['header' => Yii::t('frontend', 'Update status {nama_pekerjaan}', ['nama_pekerjaan' => $model->nama_pekerjaan])]]) . "<br>" . $model->keterangan;
                            }
                            return Html::a($model->_statusFinish[$model->status_finish], ['set-status', 'id' => $model->id], ['class' => 'btn btn-primary btn-xs btn-call-modal btn-block', 'data' => ['header' => Yii::t('frontend', 'Update status {nama_pekerjaan}', ['nama_pekerjaan' => $model->nama_pekerjaan])]]) . "<br>" . $model->keterangan;
                        },
                        'format' => 'raw'
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{tambahkan-ketanggal} {update-detail} {delete-detail}',
                        'buttons' => [
                            'tambahkan-ketanggal' => function ($url, WscDetail $model) {
                                if ($model->created_by != Yii::$app->user->id) {
                                    return '';
                                }
                                if (strtotime($model->wsc->tanggal) < strtotime(date('Y-m-d')) || strtotime($model->wsc->tanggal) > strtotime(date('Y-m-d'))) {
                                    return '';
                                }
                                if (!empty($model->tambahkan_ketanggal)) {
                                    return Html::button(Yii::t('frontend', '+ {tanggal}', ['tanggal' => Helper::idDate($model->tambahkan_ketanggal)]), ['class' => 'btn btn-warning btn-xs']);
                                }
                                return Html::a(Yii::t('frontend', 'Tambahkan Ketanggal'), $url, ['class' => 'btn btn-warning btn-xs btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Tambahkan {nama_pekerjaan} ketanggal', ['nama_pekerjaan' => $model->nama_pekerjaan])]]);

                            },
                            'update-detail' => function ($url, WscDetail $model) {
                                if ($model->created_by != Yii::$app->user->id) {
                                    return '';
                                }
                                if (strtotime(date('Y-m-d H:i:s')) > strtotime($model->wsc->tanggal . ' ' . WscDetail::jamMaksimum) && $model->kategori == WscDetail::direncanakan) {
                                    return '';
                                }
                                if (strtotime($model->wsc->tanggal) < strtotime(date('Y-m-d'))) {
                                    return '';
                                }
                                return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-primary btn-xs btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Update {nama_pekerjaan}', ['nama_pekerjaan' => $model->nama_pekerjaan])]]);

                            },
                            'delete-detail' => function ($url, WscDetail $model) {
                                if ($model->created_by != Yii::$app->user->id) {
                                    return '';
                                }
                                if (strtotime(date('Y-m-d H:i:s')) > strtotime($model->wsc->tanggal . ' ' . WscDetail::jamMaksimum) && $model->kategori == WscDetail::direncanakan) {
                                    return '';
                                }
                                if (strtotime($model->wsc->tanggal) < strtotime(date('Y-m-d'))) {
                                    return '';
                                }
                                return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Yakin akan menghapus {nama_pekerjaan} ?', ['nama_pekerjaan' => $model->nama_pekerjaan])]]);
                            }
                        ]
                    ],
                ];
                ?>
                <?php
                try {
                    echo GridView::widget([
                        'id' => 'grid-wsc-detail',
                        'dataProvider' => $providerWscDetail,
                        'columns' => $gridColumnDetail,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-wsc-detail']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="callout callout-success">
                <h3><?= Yii::t('frontend', '#TaggingMe') ?></h3>
                <?= Html::tag('p', Yii::t('frontend', 'Daftar Pekerjaan WSC yang menyertakan anda.')) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php
            $gridColumnDetailTaggingMe = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'nama_pekerjaan',
                    'value' => function (WscDetailTagPagawai $model) {
                        return Html::a($model->wscDetail->nama_pekerjaan, ['view-detail', 'id' => $model->wsc_detail_id], ['class' => 'detail-pekerjaan text-bold btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Detail pekerjaan {pekerjaan}', ['pekerjaan' => $model->wscDetail->nama_pekerjaan])]]);
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'jam_mulai',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->jam_mulai;
                    }
                ],
                [
                    'attribute' => 'jam_selesai',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->jam_selesai;
                    }
                ],
                [
                    'attribute' => 'durasi',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->durasi . ' menit';
                    }
                ],
                [
                    'attribute' => 'prioritas',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->_prioritas[$model->wscDetail->prioritas];
                    }
                ],
                [
                    'attribute' => 'deskripsi',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->deskripsi;
                    }
                ],
                [
                    'attribute' => 'kategori',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->_kategori[$model->wscDetail->kategori];
                    }
                ],
                [
                    'label' => Yii::t('frontend', 'Status'),
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->_statusFinish[$model->wscDetail->status_finish] . "<br>" . $model->wscDetail->keterangan;
                    },
                    'format' => 'raw'
                ],
            ];
            ?>
            <?php
            try {
                echo GridView::widget([
                    'id' => 'grid-wsc-detail-tagging-me',
                    'dataProvider' => $providerWscDetailTaggingMe,
                    'columns' => $gridColumnDetailTaggingMe,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-tagging-me']],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'toolbar' => [],
                    'panel' => [
                        'heading' => false,
                        'before' => '{summary}',
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
<?php
if (mdm\admin\components\Helper::checkRoute('/wsc/wsc/index-all')) { ?>
    <div class="row">
        <div class="col-sm-3">
            <div class="callout callout-success">
                <h3><?= Yii::t('frontend', '#MyRoute') ?></h3>
                <?= Html::tag('p', Yii::t('frontend', 'Daftar lokasi anda.')) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div style="display:inline-block;width:100%;overflow-y:auto;">
                <ul class="timeline timeline-horizontal">
                    <?php /** @var WscLocation $dataLocation */
                    foreach ($dataLocations as $dataLocation) { ?>
                        <li class="timeline-item">
                            <div class="timeline-badge primary"><i class="glyphicon glyphicon-check"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <p><small class="text-muted"><i
                                                    class="glyphicon glyphicon-time"></i> <?= Html::a(Helper::idDateTime($dataLocation->time), WscLocation::baseUrlLocation . $dataLocation->latitude . ',' . $dataLocation->longitude, ['target' => '_blank']) ?>
                                        </small></p>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
<?php } ?>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
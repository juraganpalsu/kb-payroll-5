<?php


/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\wsc\models\search\WscSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var string $indexDetail */

use common\components\Helper;
use frontend\modules\wsc\models\Wsc;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Data WFH');
$this->params['breadcrumbs'][] = $this->title;

?>

<?php
$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    ['attribute' => 'id', 'visible' => false],
    [
        'attribute' => 'tanggal',
        'value' => function (Wsc $model) {
            return Html::a(Html::tag('strong', Helper::idDate($model->tanggal)), ['view', 'id' => $model->id], ['class' => 'font-weight-bold', 'title' => 'View Detail', 'data' => ['pjax' => 0]]);
        },
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'name' => 'range_tanggal',
            'value' => '',
            'convertFormat' => true,
            'pluginOptions' => [
                'locale' => [
                    'format' => 'd-m-Y',
                    'separator' => ' s/d ',
                ],
                'opens' => 'left'
            ]
        ],
        'format' => 'raw'
    ],
    [
        'attribute' => 'pegawai_id',
        'label' => Yii::t('frontend', 'Pegawai'),
        'value' => function (Wsc $model) {
            return $model->pegawai->nama_lengkap;
        },
    ],
    [
            'attribute' => 'departemen',
            'value' => function (Wsc $model) {
                if ($struktur = $model->pegawai->pegawaiStruktur) {
                    return $struktur->struktur ? $struktur->struktur->departemen ? $struktur->struktur->departemen->nama : '' : '';
                }
                return '';
            },
    ],
    'keterangan:ntext',
    ['attribute' => 'lock', 'visible' => false],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view}'
    ],
];
?>
<div>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'noExportColumns' => [1, 6], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-WFH' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-wsc']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')]) . ' ' .
                     Html::a('<i class="glyphicon glyphicon-th-list"></i>', $indexDetail , ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Detail'), 'class' => 'btn btn-primary btn-flat'])

                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

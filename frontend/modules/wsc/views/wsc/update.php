<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\wsc\models\Wsc */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Wsc',
    ]) . ' ' . $model->pegawai->nama . ' - ' . \common\components\Helper::idDate($model->tanggal);
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Wsc'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="wsc-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

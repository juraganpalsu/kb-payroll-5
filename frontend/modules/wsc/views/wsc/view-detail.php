<?php

/**
 * Created by PhpStorm.
 * File Name: view-detail.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/4/2021
 * Time: 9:40 PM
 */


use frontend\modules\wsc\models\WscDetailAttachment;
use frontend\modules\wsc\models\WscDetailDiskusi;
use frontend\modules\wsc\models\WscDetailTagPagawai;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\wsc\models\WscDetail */
/**
 *
 * @var $modelDiskusi frontend\modules\wsc\models\WscDetailDiskusi
 *
 * @var $dataDiskusi ActiveQuery
 * @var $fileAttachment ActiveQuery
 * @var array $idPegawais
 * @var WscDetailAttachment $modelAttachment
 */


$urlDelete = Url::to(['delete-pegawai', 'id' => $model->id]);

$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $(function() {
        $('#form-diskusi').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    let idModal = $('#form-modal');
                    idModal.modal('show')
                        .find('#modelContent')
                        .load(dt.data.url);
                }
            });
            return false;
        });
    })
    
    $(function() {
        $('#form-tag-pegawai').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                   let idModal = $('#form-modal');
                    idModal.modal('show')
                        .find('#modelContent')
                        .load(dt.data.url);
                }
            });
            return false;
        });
    });
    
    $('#upload-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        // btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        let formData = new FormData(this);
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function (dt) {                
                console.log(dt);
                if(dt.status === true){
                    let idModal = $('#form-modal');
                    idModal.modal('show')
                        .find('#modelContent')
                        .load(dt.data.url);
                }
                btn.button('reset');
            }
        });
        return false;
    });
    
     $('.btn-delete-attachment').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        let idModal = $('#form-modal');
                        idModal.modal('show')
                            .find('#modelContent')
                            .load(dt.data.url);
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
     
     $('.btn-reload-chat').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        btn.button('loading');
        let idModal = $('#form-modal');
        idModal.modal('show')
            .find('#modelContent')
            .load(btn.data('url'));
        btn.button('reset');
        return false;
    });
     
     
     
    
    deletePegawai = function(id){
        console.log(id);
        $.post("$urlDelete", {'pegawai_id' : id}).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                   let idModal = $('#form-modal');
                    idModal.modal('show')
                        .find('#modelContent')
                        .load(dt.data.url);
                }
            });
            return false;
    };
    
});
JS;

$this->registerJs($js);
?>
<div class="row">
    <div class="col-sm-8">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'jam_mulai',
                'value' => $model->jam_mulai
            ],
            [
                'attribute' => 'jam_selesai',
            ],
            'durasi',
            'deskripsi:ntext',
            ['attribute' => 'lock', 'visible' => false],
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>
    <div class="col-sm-4">

        <?php
        $modelTagPegawai = new WscDetailTagPagawai();

        $form = ActiveForm::begin([
            'id' => 'form-tag-pegawai',
            'action' => $modelTagPegawai->isNewRecord ? Url::to(['create-pegawai']) : Url::to(['update', 'id' => $modelTagPegawai->id]),
            'type' => ActiveForm::TYPE_VERTICAL,
            'formConfig' => ['showErrors' => false],
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['cou-pegawai-validation']),
            'fieldConfig' => ['showLabels' => true],
        ]); ?>

        <?= $form->errorSummary($modelTagPegawai); ?>

        <?php try { ?>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    echo Html::tag('label', Yii::t('frontend', 'Daftar Pegawai'), ['control-label']);
                    echo Select2::widget([
                        'id' => 'dump_pegawai',
                        'name' => 'pegawai_ids',
                        'value' => array_keys($idPegawais),
                        'data' => $idPegawais,
                        'maintainOrder' => false,
                        'options' => ['placeholder' => 'Select a color ...', 'multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'maximumInputLength' => 10
                        ],
                        'pluginEvents' => [
                            'select2:unselect' => 'function(e) {deletePegawai(e.params.data.id)}'
                        ]
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    if ($model->created_by == Yii::$app->user->id) {
                        echo $form->field($modelTagPegawai, 'pegawai_id')->widget(Select2::class, [
                            'options' => [
                                'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                                'multiple' => false,
                            ],
                            'data' => [],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2', 'tanggal' => $model->wsc->tanggal]),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term};}')
                                ],
                            ],
                            'pluginEvents' => [
                                'select2:select' => 'function() {$("#form-tag-pegawai").submit()}'
                            ]
                        ])->label(Yii::t('frontend', 'Pilih Pegawai'));
                    }
                    ?>
                </div>
            </div>

            <?= $form->field($modelTagPegawai, 'wsc_detail_id', ['template' => '{input}'])->hiddenInput(['value' => $model->id]); ?>
            <?= $form->field($modelTagPegawai, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <?php } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="wsc-form">
    <div class="row">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-12">
                    <?php

                    $modelDiskusi = new WscDetailDiskusi();

                    $form = ActiveForm::begin([
                        'id' => 'form-diskusi',
                        'action' => Url::to(['create-diskusi', 'id' => $modelDiskusi->id]),
                        'type' => ActiveForm::TYPE_VERTICAL,
                        'formConfig' => ['showErrors' => false],
                        'enableAjaxValidation' => true,
                        'validationUrl' => Url::to(['cou-diskusi-validation', 'isnew' => $modelDiskusi->isNewRecord]),
                        'fieldConfig' => ['showLabels' => true],
                    ]); ?>

                    <?= $form->errorSummary($modelDiskusi); ?>


                    <?php try { ?>

                        <?= $form->field($modelDiskusi, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($modelDiskusi, 'komentar')->textInput() ?>
                            </div>
                        </div>

                        <?= $form->field($modelDiskusi, 'wsc_detail_id', ['template' => '{input}'])->hiddenInput(['value' => $model->id]); ?>
                        <?= $form->field($modelDiskusi, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                        <div class="form-group">
                            <?= Html::submitButton($modelDiskusi->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $modelDiskusi->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            <?= Html::button('<i class="fa fa-refresh"></i>', ['class' => 'btn btn-primary btn-reload-chat', 'data' => ['url' => Url::to(['view-detail', 'id' => $model->id])]]) ?>
                        </div>

                    <?php } catch (Exception $e) {
                        Yii::info($e->getMessage(), 'exception');
                    } ?>

                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-lg-6">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?= Yii::t('frontend', 'Diskusi') ?></h3>
                        </div>
                        <div class="box-body">
                            <div class="direct-chat-messages">
                                <?php

                                /** @var WscDetailDiskusi $wscDiskusi */
                                foreach ($dataDiskusi as $wscDiskusi) {
                                    $directChtPosition = 'direct-chat-msg';
                                    $chatNamaPosition = 'direct-chat-name pull-left';
                                    $chatTimeStampPosition = 'direct-chat-timestamp pull-right';
                                    if ($wscDiskusi->created_by == Yii::$app->user->id) {
                                        $directChtPosition = 'direct-chat-msg right';
                                        $chatNamaPosition = 'direct-chat-name pull-right';
                                        $chatTimeStampPosition = 'direct-chat-timestamp pull-left';
                                    }
                                    ?>
                                    <div class="<?= $directChtPosition ?>">
                                        <div class="direct-chat-info clearfix">
                                            <span class="<?= $chatNamaPosition ?>"><?= $wscDiskusi->pegawai->nama_lengkap ?></span>
                                            <span class="<?= $chatTimeStampPosition ?>"><?= date('d-m-Y H:i:s', strtotime($wscDiskusi->created_at)) ?></span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <?php
                                        $fotoProfil = $wscDiskusi->pegawai->upload_dir . $wscDiskusi->pegawai_id . '.jpg';
                                        if (!file_exists($fotoProfil)) {
                                            $fotoProfil = Yii::getAlias('@web') . '/pictures/user8-128x128.jpg';
                                        } else {
                                            $fotoProfil = Yii::getAlias('@web') . '/' . $wscDiskusi->pegawai->upload_dir . $wscDiskusi->pegawai_id . '.jpg';
                                        }
                                        ?>
                                        <img class="direct-chat-img" src="<?= $fotoProfil ?>" alt="message user image">
                                        <!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            <?= $wscDiskusi->komentar; ?>
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title text-primary"><?= Yii::t('frontend', 'Attachment') ?></h3>
                        </div>
                        <?php
                        foreach ($fileAttachment as $file) {
                            ?>
                            <div class="box-header with-border">
                                <div class="box-tools pull-left">
                                    <?= Html::a($file->nama_file, ['view-attachment', 'id' => $file->id], ['class' => 'text-primary', 'target' => '_blank']) ?>
                                    <?= Html::a(Html::tag('span', 'X', ['class' => 'badge bg-red']), ['delete-attachment', 'id' => $file->id], ['class' => 'btn-delete-attachment', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan menghapus file ini?')]]) ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php $form = ActiveForm::begin([
                        'id' => 'upload-form',
                        'action' => Url::to(['upload-attachment']),
                        'type' => ActiveForm::TYPE_VERTICAL,
                        'formConfig' => ['showErrors' => false],
                        'enableAjaxValidation' => true,
                        'validationUrl' => Url::to(['upload-attachment-validation']),
                        'fieldConfig' => ['showLabels' => true],
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>

                    <?= $form->errorSummary($modelAttachment); ?>

                    <?php try { ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->field($modelAttachment, 'nama_file')->widget(FileInput::class, [
                                    'options' => [
                                        'multiple' => false,
                                        'accept' => 'xls, xlsx, pdf, jpg, jpeg'
                                    ],
                                    'pluginOptions' => [
                                        'previewFileType' => 'any',
                                        'showUpload' => false,
                                        'initialPreview' => [],
                                        'initialPreviewAsData' => true,
                                        'initialPreviewConfig' => [],
//                            'deleteUrl' => [],
                                        'overwriteInitial' => false,
                                        'maxFileSize' => 7000
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>


                        <?= $form->field($modelAttachment, 'wsc_detail_id', ['template' => '{input}'])->hiddenInput(['value' => $model->id]); ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('frontend', 'Upload'), ['class' => 'btn btn-success submit-btn']) ?>
                        </div>

                        <?php
                    } catch (Exception $e) {
                        Yii::info($e->getMessage(), 'exception');
                    }
                    ?>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\wsc\models\Wsc */

$this->title = Yii::t('frontend', 'Create Data WFH');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'WFH'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wsc-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

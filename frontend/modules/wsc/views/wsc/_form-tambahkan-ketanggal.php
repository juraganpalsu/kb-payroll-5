<?php

/**
 * Created by PhpStorm.
 * File Name: _form-tambahkan-ketanggal.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 5/6/2021
 * Time: 10:47 PM
 */


/* @var $this yii\web\View */
/* @var $model WscDetail */

/* @var $form kartik\widgets\ActiveForm */

use frontend\modules\wsc\models\WscDetail;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$js = <<< JS
    $(function() {
        $('#form-wsc').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    // location.href = dt.data.url;
                    location.reload()
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>

<div class="wsc-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-wsc',
        'action' => Url::to(['tambahkan-ketanggal', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['tambahkan-validation', 'isnew' => $model->isNewRecord]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>


    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'tambahkan_ketanggal')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
        </div>


        <?= $form->field($model, 'wsc_id', ['template' => '{input}'])->textInput(['style' => 'display:none', 'value' => $model->wsc_id]); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>


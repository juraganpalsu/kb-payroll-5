<?php

/**
 * Created by PhpStorm.
 * File Name: _form-detail.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 3/30/2021
 * Time: 9:41 PM
 */


/* @var $this yii\web\View */
/* @var $model WscDetail */

/* @var $form kartik\widgets\ActiveForm */

use frontend\modules\wsc\models\WscDetail;
use kartik\widgets\ActiveForm;
use kartik\widgets\TimePicker;
use yii\helpers\Html;
use yii\helpers\Url;

$js = <<< JS
    $(function() {
        $('#form-wsc').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>

<div class="wsc-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-wsc',
        'action' => $model->isNewRecord ? Url::to(['create-detail', 'idwsc' => $model->wsc_id]) : Url::to(['update-detail', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['cou-detail-validation', 'isnew' => $model->isNewRecord]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>


    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'nama_pekerjaan')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?php // $form->field($model, 'durasi', ['addon' => ['append' => ['content' => 'menit']]])->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('durasi'), 'type' => 'number']) ?>
            </div>
        </div>

        <div class="row">
            <div class=" col-md-3">
                <?= $form->field($model, 'jam_mulai')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'jam_selesai')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'prioritas')->dropDownList($model->_prioritas) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <?= $form->field($model, 'wsc_id', ['template' => '{input}'])->textInput(['style' => 'display:none', 'value' => $model->wsc_id]); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>


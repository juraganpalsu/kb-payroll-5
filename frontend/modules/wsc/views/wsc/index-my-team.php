<?php

/**
 * Created by PhpStorm.
 * File Name: index-my-team.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/25/2021
 * Time: 9:55 PM
 */


/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\wsc\models\search\WscSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Helper;
use frontend\modules\wsc\models\Wsc;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Data WFH Team Saya');
$this->params['breadcrumbs'][] = $this->title;

?>
<div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'tanggal',
            'value' => function (Wsc $model) {
                return Html::a(Html::tag('strong', Helper::idDate($model->tanggal)), ['view', 'id' => $model->id], ['class' => 'font-weight-bold', 'title' => 'View Detail', 'data' => ['pjax' => 0]]);
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
            'format' => 'raw'
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (Wsc $model) {
                return $model->pegawai->nama_lengkap;
            },
        ],
        'keterangan:ntext',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}'
        ],
    ];
    ?>

    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'noExportColumns' => [1, 6], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-WFH-MyTeam' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-wsc']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

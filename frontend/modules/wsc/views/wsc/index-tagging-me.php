<?php

/**
 * Created by PhpStorm.
 * File Name: index-tagging-me.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/25/2021
 * Time: 11:22 PM
 */


/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\wsc\models\search\WscSearch */

/**
 * @var ActiveDataProvider $dataProvider
 * @var WscDetail $modelDetail
 */

use common\components\Helper;
use frontend\modules\wsc\models\WscDetail;
use frontend\modules\wsc\models\WscDetailTagPagawai;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Daftar WSC yang menandai anda.');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Wsc'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
});
JS;

$this->registerJs($js);

?>

    <div class="row">
        <div class="col-sm-12">
            <?php
            $gridColumnDetailTaggingMe = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'tanggal',
                    'value' => function (WscDetailTagPagawai $model) {
                        return Helper::idDate($model->wscDetail->wsc->tanggal);
                    },
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [
                        'name' => 'range_tanggal',
                        'value' => '',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'd-m-Y',
                                'separator' => ' s/d ',
                            ],
                            'opens' => 'left'
                        ]
                    ],
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'nama_pekerjaan',
                    'value' => function (WscDetailTagPagawai $model) {
                        return Html::a($model->wscDetail->nama_pekerjaan, ['view-detail', 'id' => $model->wsc_detail_id], ['class' => 'detail-pekerjaan text-bold btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Detail pekerjaan {pekerjaan}', ['pekerjaan' => $model->wscDetail->nama_pekerjaan])]]);
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'jam_mulai',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->jam_mulai;
                    }
                ],
                [
                    'attribute' => 'jam_selesai',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->jam_selesai;
                    }
                ],
                [
                    'attribute' => 'durasi',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->durasi . ' menit';
                    }
                ],
                [
                    'attribute' => 'prioritas',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->_prioritas[$model->wscDetail->prioritas];
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $modelDetail->_prioritas,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Prioritas'), 'id' => 'grid-wsc-detail-tag-pegawai_prioritas'],
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'deskripsi',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->deskripsi;
                    }
                ],
                [
                    'attribute' => 'kategori',
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->_kategori[$model->wscDetail->kategori];
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $modelDetail->_kategori,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Kategori'), 'id' => 'grid-wsc-detail-tag-pegawai_kategori'],
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'status_finish',
                    'label' => Yii::t('frontend', 'Status'),
                    'value' => function (WscDetailTagPagawai $model) {
                        return $model->wscDetail->_statusFinish[$model->wscDetail->status_finish] . "<br>" . $model->wscDetail->keterangan;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $modelDetail->_statusFinish,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Prioritas'), 'id' => 'grid-wsc-detail-tag-status_finish'],
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'created_by',
                    'value' => function (WscDetailTagPagawai $model) {
                        $createdBy = $model->wscDetail->createdBy->userPegawai;
                        $nama = $createdBy ? $createdBy->pegawai->nama : '';
                        $namaTampil = explode(' ', $nama);
                        return $namaTampil[0];
                    }
                ],
            ];
            ?>
            <?php
            try {
                echo GridView::widget([
                    'id' => 'grid-wsc-detail-tagging-me',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumnDetailTaggingMe,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-tagging-me']],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'toolbar' => [],
                    'panel' => [
                        'heading' => false,
                        'before' => '{summary}',
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                ]);
            } catch (Exception $e) {
                print_r($e->getMessage());
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
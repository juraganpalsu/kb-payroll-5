<?php

namespace frontend\modules\spl\controllers;

use frontend\models\form\SplForm;
use frontend\models\search\SplSearch;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `spl` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SplSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelSplForm = new SplForm();

        return $this->render('/spl/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelSplForm' => $modelSplForm
        ]);
    }
}

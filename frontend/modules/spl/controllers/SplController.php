<?php

namespace frontend\modules\spl\controllers;

use common\components\Status;
use Exception;
use frontend\models\Absensi;
use frontend\models\form\SplExportExcelForm;
use frontend\models\form\SplForm;
use frontend\models\search\SplSearch;
use frontend\models\Spl;
use frontend\models\SplDetail;
use frontend\models\User;
use kartik\mpdf\Pdf;
use kartik\widgets\ActiveForm;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * SplController implements the CRUD actions for Spl model.
 */
class SplController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Spl models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SplSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [Spl::LEMBUR_BIASA]);
        $modelSpl = new Spl();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'redirect' => 'index',
            'modelSpl' => $modelSpl,
        ]);
    }

    /**
     * Lists all Spl models.
     * @return mixed
     */
    public function actionIndexHr()
    {
        $searchModel = new SplSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, '', false);
        $searchModel->showDeleteBtn = true;
        $searchModel->showReviseBtn = true;
        $modelSpl = new Spl();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'redirect' => 'index-hr',
            'modelSpl' => $modelSpl,
        ]);
    }

    /**
     * Lists all Spl models.
     * @return mixed
     */
    public function actionIndexEss()
    {
        $searchModel = new SplSearch();
        $dataProvider = $searchModel->searchEss(Yii::$app->request->queryParams);
        $modelSpl = new Spl();


        return $this->render('index-ess', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelSpl' => $modelSpl,
        ]);
    }

    /**
     * Lists all Spl models.
     * @return mixed
     */
    public function actionIndexAcc()
    {
        $searchModel = new SplSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, '', false, true, [Status::FINISHED, Status::APPROVED]);
        $modelSpl = new Spl();

        return $this->render('index-acc', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelSpl' => $modelSpl,
        ]);
    }

    /**
     * Lists all Spl models.
     * @return mixed
     */
    public function actionIndexLemburInsentif()
    {
        $searchModel = new SplSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [Spl::LEMBUR_INSENTIF]);

        return $this->render('index-lembur-insentif', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'redirect' => 'index-lembur-insentif'
        ]);
    }

    /**
     * Displays a single Spl model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Spl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Spl();
        $splTemplate = $model->getSplTemplateForDropDown();
        $request = Yii::$app->request;
        $model->is_planing = true;

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => false];
            if ($model->load(Yii::$app->request->post()) & $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['/spl/spl/view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
            return $response->data;
        }

        if (!$user->validasiKelengkapanAkun()) {
            return $this->render('@frontend/modules/pegawai/views/pegawai/perhatian');
        }

        return $this->render('create', [
            'model' => $model,
            'splTemplate' => $splTemplate,
            'strukturDibolehkan' => $user->getStrukturBawahan()
        ]);

    }

    /**
     * Creates a new Spl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateInsentif()
    {
        $model = new Spl();
        $model->scenario = Spl::SCENARIO_INSENTIF;
        $model->kategori = Spl::LEMBUR_INSENTIF;
        $splTemplate = $model->getTemplateInsentifForDropDown();
        $request = Yii::$app->request;
        $model->is_planing = false;

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => false];
            if ($model->load(Yii::$app->request->post()) & $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['/spl/spl/view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
            return $response->data;
        }

        if (!$user->validasiKelengkapanAkun()) {
            return $this->render('@frontend/modules/pegawai/views/pegawai/perhatian');
        }

        return $this->render('create-insentif', [
            'model' => $model,
            'splTemplate' => $splTemplate,
            'strukturDibolehkan' => $user->getStrukturBawahan()
        ]);

    }

    /**
     * Updates an existing Spl model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param string $id
     * @param string $redirect
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDelete(string $id, string $redirect = 'index')
    {
        $model = $this->findModel($id);
        $pegawaiIds = [];
        if ($details = $model->splDetails) {
            foreach ($details as $detail) {
                $pegawaiIds[] = $detail->pegawai_id;
                $detail->delete();
            }
        }
        $model->delete();
        if (!empty($pegawaiIds)) {
            $modelAbsensi = new Absensi();
            try {
                $modelAbsensi->prosesData($model->tanggal, $model->tanggal, null, $pegawaiIds);
            } catch (Exception $e) {
                Yii::info($e->getMessage());
            }
        }

        return $this->redirect([$redirect]);
    }

    /**
     * Finds the Spl model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Spl the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id)
    {
        if (($model = Spl::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return SplDetail|null
     * @throws NotFoundHttpException
     */
    protected function findModelDetail(string $id)
    {
        if (($model = SplDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Action to load a tabular form grid
     * for SplDetail
     * @return mixed
     * @throws NotFoundHttpException
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     */
    public function actionAddSplDetail()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('SplDetail');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formSplDetail', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param null|string $q id_pegawai dan nama pegawai
     * @return array id-> berisi id pegawai, text-> berisi nama pegawai
     *
     * @throws Exception
     */
    public function actionGetPegawaiForSelect2($q = null)
    {
        $model = new Spl();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $out['results'] = array_values($model->getPegawaiSelect2());
        }
        return $out;
    }

    /**
     * @param string $id
     * @return mixed
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws NotFoundHttpException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function actionPrint(string $id)
    {
        $model = $this->findModel($id);
        $content = $this->renderAjax('_print', ['model' => $model]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@frontend/web/css/laporanHarian.css',
            'cssInline' => 'table{font-size:10px}',
            'options' => ['title' => Yii::$app->name],
            'marginTop' => '5',
            'marginBottom' => '5',
            'marginLeft' => '2',
            'marginRight' => '2',
            'methods' => [
                'SetFooter' => [Yii::$app->formatter->asDatetime('now') . '|Page {PAGENO}|' . $model->seq_code],
            ]
        ]);
//        return $content;

        return $pdf->render();
    }

    /**
     * @return string
     */
    public function actionApproval()
    {
        $searchModel = new SplSearch();
        $dataProvider = $searchModel->searchApproval();
        $modelSpl = new Spl();

        return $this->render('approval', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelSpl' => $modelSpl,
        ]);
    }

    /**
     * @param bool $action
     * @return string
     */
    public function actionTampilkanUserApproval($action = true)
    {
        if (isset($_POST['expandRowKey'])) {
            try {
                $model = $this->findModel($_POST['expandRowKey']);
                $modelApprovals = $model->splApprovals;
                return $this->renderAjax('_user-approval-spl', [
                    'model' => $model,
                    'modelApprovals' => $modelApprovals,
                    'action' => $action
                ]);
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        return '<div class="alert alert-danger">' . Yii::t('frontend', 'No data found') . '</div>';
    }

    /**
     * @param bool $action
     * @return string
     */
    public function actionTampilkanUserApprovalDetail($action = true)
    {
        if (isset($_POST['expandRowKey'])) {
            try {
                $model = $this->findModelDetail($_POST['expandRowKey']);
                $modelApprovals = $model->splDetailApprovals;
                return $this->renderAjax('_user-approval-spl-detail', [
                    'model' => $model,
                    'modelApprovals' => $modelApprovals,
                    'action' => $action
                ]);
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        return '<div class="alert alert-danger">' . Yii::t('frontend', 'No data found') . '</div>';
    }

    /**
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionApprovalForm(string $id)
    {
        $model = $this->findModel($id);
        $providerSplDetail = new ArrayDataProvider([
            'allModels' => $model->splDetails,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        return $this->renderAjax('_approval-form', ['model' => $model, 'providerSplDetail' => $providerSplDetail]);
    }

    /**
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionApprove(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan approval.'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                $modelDetail = $this->findModelDetail($id);
                $modelDetail->approval(Status::APPROVE);
                $modelSpl = $this->findModel($modelDetail->spl_id);

                $lastStatus = $modelSpl->last_status;
                $isdone = in_array($lastStatus, [Status::REJECTED, Status::APPROVED]);
                //Jika telah selesai persetujuannya
                if ($isdone) {
                    //update spl approval ke status terakhir, ini dieksekusi ketika satu spl telah selesai melakukan approval
                    $modelSpl->createApproval();
                    try {
                        $infoProses = $modelDetail->spl->prosesUlangAbsensi();
                        if (!empty($infoProses)) {
                            Yii::$app->session->setFlash('info-proses', Absensi::formatInfoProses($infoProses));
                        }
                    } catch (Exception $e) {
                        Yii::info($e->getMessage(), 'exception');
                    }
                }
                $response->data = [
                    'data' => ['isdone' => $isdone],
                    'message' => Yii::t('frontend', 'Berhasil melakukan approval.'),
                    'status' => true,
                ];
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * Aksi melakukan reject
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionReject(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan reject .'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                $modelDetail = $this->findModelDetail($id);
                $modelDetail->approval(Status::REJECT, $request->post('keterangan'));
                //panggil ulang model untuk mendapatkan data terakhir, setelah di update
                $modelDetail = $this->findModelDetail($id);
                $spl = $modelDetail->spl;
                $lastStatus = $spl->last_status;
                $isdone = in_array($lastStatus, [Status::REJECTED, Status::APPROVED]);
                //Jika telah selesai persetujuannya
                if ($isdone) {
                    //update spl approval ke status terakhir, ini dieksekusi ketika satu spl telah selesai melakukan approval
                    $spl->createApproval();
                    try {
                        $infoProses = $modelDetail->spl->prosesUlangAbsensi();
                        if (!empty($infoProses)) {
                            Yii::$app->session->setFlash('info-proses', Absensi::formatInfoProses($infoProses));
                        }
                    } catch (Exception $e) {
                        Yii::info($e->getMessage(), 'exception');
                    }
                }
                $response->data = [
                    'data' => ['isdone' => $isdone],
                    'message' => Yii::t('frontend', 'Berhasil melakukan approval(REJECT).'),
                    'status' => true,
                ];
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * Approve semua product
     *
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionApproveAll(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan approve.'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                if ($model = $this->findModel($id)) {
                    $model->approval(Status::APPROVE);
                    $model = $this->findModel($id);
                    $model->createApproval();
                    try {
                        $infoProses = $model->prosesUlangAbsensi();
                        if (!empty($infoProses)) {
                            Yii::$app->session->setFlash('info-proses', Absensi::formatInfoProses($infoProses));
                        }
                    } catch (Exception $e) {
                        Yii::info($e->getMessage(), 'exception');
                    }
                    $response->data = [
                        'data' => [],
                        'message' => Yii::t('frontend', 'Berhasil melakukan approve.'),
                        'status' => true,
                    ];
                }
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * Reject semua product
     *
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionRejectAll(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan approval .'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                if ($model = $this->findModel($id)) {
                    $model->approval(Status::REJECT, $request->post('keterangan'));
                    $model = $this->findModel($id);
                    $model->createApproval($request->post('keterangan'));
                    try {
                        $infoProses = $model->prosesUlangAbsensi();
                        if (!empty($infoProses)) {
                            Yii::$app->session->setFlash('info-proses', Absensi::formatInfoProses($infoProses));
                        }
                    } catch (Exception $e) {
                        Yii::info($e->getMessage(), 'exception');
                    }
                    $response->data = [
                        'data' => [],
                        'message' => Yii::t('frontend', 'Berhasil melakukan approval .'),
                        'status' => true,
                    ];
                }
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }


    /**
     *
     * @param bool $isPlaning
     * @param bool $isRevisi
     * @param string $scenario
     * @return mixed
     * @throws NotAcceptableHttpException
     */
    public function actionCreateOrUpdateValidation(bool $isPlaning = true, bool $isRevisi = false, string $scenario = Spl::SCENARIO_STANDAR)
    {
        $model = new Spl();
        $model->scenario = $scenario;
        $model->is_planing = $isPlaning;
        $model->is_revision = $isRevisi;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * @return array|mixed
     */
    public function actionDailyReportForm()
    {
        $model = new SplForm();
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => '',
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post())) {
                $response->data = [
                    'data' => ['url' => Url::to(['daily-report', 'SplForm' => $model->attributes])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }
        return $this->renderAjax('daily-report-form', ['model' => $model]);

    }

    /**
     *
     * @return mixed
     * @throws NotAcceptableHttpException
     */
    public function actionDailyReportFormValidation()
    {
        $model = new SplForm();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * @return mixed
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws CrossReferenceException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function actionDailyReport()
    {
        $model = new SplForm();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        $dataProvider = $model->dailyReport($params);

//        print_r($dataProvider);

        $content = $this->renderAjax('_daily-report', ['model' => $model, 'dataProvider' => $dataProvider]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@web/css/laporanHarian.css',
            'cssInline' => 'table{font-size:10px}',
            'options' => ['title' => Yii::t('frontend', 'Dayli Report') . '-' . Yii::$app->name],
            'marginTop' => '5',
            'marginBottom' => '5',
            'marginLeft' => '2',
            'marginRight' => '2',
            'methods' => [
                'SetFooter' => [Yii::$app->formatter->asDatetime('now') . '|Page {PAGENO}|'],
            ]
        ]);
//        return $content;

        return $pdf->render();
    }


    /**
     * @return array|mixed
     */
    public function actionExportExcelForm()
    {
        $model = new SplExportExcelForm();
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => '',
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post())) {
                $response->data = [
                    'data' => ['url' => Url::to(['export-excel', 'SplExportExcelForm' => $model->attributes])],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }
        return $this->renderAjax('export-excel-form', ['model' => $model]);

    }

    /**
     *
     * @return mixed
     * @throws NotAcceptableHttpException
     */
    public function actionExportExcelFormValidation()
    {
        $model = new SplExportExcelForm();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }


    /**
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionExportExcel()
    {
        $params = Yii::$app->request->queryParams;
        $model = new SplExportExcelForm();
        $model->load($params);
        if ($model->validate()) {
            $model->downloadExcel($model->getSpl());
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param string $id
     * @return array|mixed
     */
    public function actionSubmit(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Belum ada data karyawan.'),
            'status' => false,
        ];
        try {
            if ($request->isAjax && $request->isPost) {
                $model = $this->findModel($id);
                $validasiTanggal = $model->validasiTanggalHelper();
                if (empty($validasiTanggal)) {
                    if ($model->splDetails) {
                        if ($model->submit()) {
                            $response->data = [
                                'data' => ['url' => Url::to(['/spl/index'])],
                                'message' => Yii::t('frontend', 'Berhasil melakukan submit.'),
                                'status' => true,
                            ];
                        }
                    }
                } else {
                    $response->data = [
                        'data' => ['url' => Url::to(['/spl/index'])],
                        'message' => $validasiTanggal,
                        'status' => false,
                    ];
                }
            }
        } catch (NotFoundHttpException $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $response->data;
    }


    /**
     * @return string
     */
    public function actionSemuaData()
    {
        $searchModel = new SplSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('semua-data', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionCreateD()
    {
        $model = new Spl();
        $splTemplate = $model->getSplTemplateForDropDown();
        $request = Yii::$app->request;
        $model->is_planing = false;

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => false];
            if ($model->load(Yii::$app->request->post()) & $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['/spl/spl/view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
            return $response->data;
        }

        if (!$user->validasiKelengkapanAkun()) {
            return $this->render('@frontend/modules/pegawai/views/pegawai/perhatian');
        }

        return $this->render('create', [
            'model' => $model,
            'splTemplate' => $splTemplate,
        ]);

    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateRevisi(string $id)
    {
        $modelOld = $this->findModel($id);
        $model = new Spl();
        $model->is_planing = false;
        $model->is_revision = true;
        $splTemplate = $model->getSplTemplateForDropDown();
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost && $modelOld) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => '',
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $model->buatRevisiDetail();
                $response->data = [
                    'data' => ['url' => ''],
                    'message' => '',
                    'status' => true,
                ];
            } else {
                Yii::info($model->errors, 'exception');
            }
            return $response->data;
        }
        $model->tanggal = $modelOld->tanggal;
        $model->spl_alasan_id = $modelOld->spl_alasan_id;
        $model->cost_center_id = $modelOld->cost_center_id;
        $model->spl_id = $modelOld->id;
        $model->keterangan = $modelOld->keterangan;
        $model->spl_template_id = $modelOld->spl_template_id;
        $model->kategori = $modelOld->kategori;
        $model->klaim_quaker = $modelOld->klaim_quaker;
//        $model->is_planing = $modelOld->is_planing;
        return $this->renderAjax('_revisi', [
            'model' => $model,
            'splTemplate' => $splTemplate
        ]);
    }


    /**
     * @param string $id
     * @return array|mixed
     */
    public function actionSetRevisi(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan submit.'),
            'status' => false,
        ];
        try {
            if ($request->isAjax && $request->isPost) {
                $model = $this->findModel($id);
                if ($model->setAsRevisi()) {
                    $response->data = [
                        'data' => ['url' => Url::to(['index-hr'])],
                        'message' => Yii::t('frontend', 'Berhasil melakukan submit.'),
                        'status' => true,
                    ];
                }
            }
        } catch (NotFoundHttpException $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $response->data;
    }


    /**
     * @param string $id
     * @return array|mixed
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteDetail(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan delete.'),
            'status' => false,
        ];
        try {
            if ($request->isAjax && $request->isPost) {
                $model = $this->findModelDetail($id);
                $model->delete();
                $nameCostume = $model->spl->createdByStruktur ? $model->spl->createdByStruktur->nameCostume : '';
                $response->data = [
                    'data' => ['url' => Url::to(['approval-form', 'id' => $model->spl_id]),
                        'header' => Yii::t('frontend', 'SPL {seqcode} dibuat oleh {dibuat}', ['seqcode' => $model->spl->seq_code, 'dibuat' => $model->spl->createdBy->pegawai->nama . '[' . $nameCostume . ']'])],
                    'message' => Yii::t('frontend', 'Berhasil melakukan delete.'),
                    'status' => true,
                ];
            }
        } catch (NotFoundHttpException $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $response->data;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionAssign($id)
    {
        $items = Yii::$app->getRequest()->post('items', []);
        $model = $this->findModel($id);
        $success = $model->createDetails($items);
        Yii::$app->getResponse()->format = 'json';
        return array_merge($model->getListPegawai(), ['success' => $success]);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionRevoke($id)
    {
        $items = Yii::$app->getRequest()->post('items', []);
        $model = $this->findModel($id);
        $success = $model->deleteDetails($items);
        Yii::$app->getResponse()->format = 'json';
        return array_merge($model->getListPegawai(), ['success' => $success]);
    }

    /***
     * @param string $fn Filename
     * @return \yii\console\Response|Response
     */
    public function actionDownloadRekapHarian(string $fn)
    {
        $fileName = Yii::$app->security->unmaskToken($fn);
        $file = Yii::getAlias('@console/runtime/spl/') . 'data-spl-' . $fileName . '.xls';
        return Yii::$app->response->sendFile($file);
    }
}

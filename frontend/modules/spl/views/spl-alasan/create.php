<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\spl\models\SplAlasan */

$this->title = Yii::t('frontend', 'Create Spl Alasan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Spl Alasan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spl-alasan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

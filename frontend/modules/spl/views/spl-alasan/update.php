<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\spl\models\SplAlasan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Spl Alasan',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Spl Alasan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="spl-alasan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

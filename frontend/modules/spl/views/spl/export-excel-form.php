<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 05/02/2019
 * Time: 21:16
 */

use frontend\models\form\SplExportExcelForm;
use common\models\Golongan;
use common\models\SistemKerja;
use frontend\modules\spl\models\SplAlasan;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model SplExportExcelForm */
/* @var $form yii\widgets\ActiveForm */
/** @var $splTemplate array */


$js = <<< JS
$(function() {
    $('#export-excel-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        var form = $(this);
        var btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                console.log(dt);
                var win = window.open(dt.data.url, '_blank');
                win.focus();
                location.reload();
            }else {
                location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

?>
<div class="export-excel-form">

    <?php $form = ActiveForm::begin([
        'id' => 'export-excel-form',
        'action' => Url::to(['export-excel-form']),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['export-excel-form-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <div class="row">
            <div class="col-md-3">
                <?php
                echo $form->field($model, 'bisnis_unit')->dropDownList(SplExportExcelForm::modelPerjanjianKerja()->_kontrak, ['prompt' => Yii::t('frontend', '*')]);
                ?>
            </div>
            <div class="col-md-3">
                <?php
                try {
                    echo $form->field($model, 'golongan_id')->dropDownList(ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'), ['prompt' => '*']);
                } catch (InvalidConfigException $e) {
                }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <?php
                echo $form->field($model, 'sistem_kerja')->widget(Select2::class, [
                    'data' => ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => $model->getAttributeLabel('sistem_kerja'), 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?php
                try {
                    echo $form->field($model, 'kategori')->dropDownList($model->_kategori);
                } catch (InvalidConfigException $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php
                echo $form->field($model, 'tanggal', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                    'options' => ['class' => 'drp-container form-group', 'id' => 'ads']
                ])->widget(DateRangePicker::class, [
                    'options' => [
                        'id' => 'splexportexcelform-tanggal-form',
                        'class' => 'form-control',
                    ],
                    'convertFormat' => true,
                    'useWithAddon' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'spl_alasan_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(SplAlasan::find()->orderBy('nama')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => $model->getAttributeLabel('spl_alasan_id')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>


        <hr>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Generate'), ['class' => 'btn btn-success btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>
    <?php ActiveForm::end(); ?>

</div>

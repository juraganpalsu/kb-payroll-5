<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Spl */
/* @var $form yii\widgets\ActiveForm */
/** @var $splTemplate array */

$js = <<< JS
$(function() {
    $('#spl-revisi-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                console.log(dt);
                // var win = window.open(dt.data.url, '_blank');
                // win.focus();
                location.reload();
            }else {
                location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

?>
<div class="spl-form">

    <?php $form = ActiveForm::begin([
        'id' => 'spl-revisi-form',
        'action' => Url::to(['create-revisi', 'id' => $model->spl_id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation', 'isPlaning' => $model->is_planing, 'isRevisi' => $model->is_revision]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php
    try {
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'spl_template_id')->widget(Select2::class, [
                    'data' => $splTemplate,
                    'options' => ['placeholder' => Yii::t('app', 'Choose Spl template')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'keterangan')->textarea(); ?>
            </div>
        </div>
        <?= $form->field($model, 'spl_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'spl_alasan_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'cost_center_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'tanggal', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'kategori', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'klaim_quaker', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <hr>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Create'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

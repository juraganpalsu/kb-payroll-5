<?php

use common\models\Divisi;
use \common\models\base\Absensi;
use common\models\Golongan;
use common\models\SistemKerja;
use common\models\Unit;
use kartik\daterange\DateRangePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\SplSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-spl-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index-hr'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-5">
            <?php
            echo $form->field($model, 'sistem_kerja')->widget(Select2::class, [
                'data' => ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama'),
                'options' => ['placeholder' => $model->getAttributeLabel('sistem_kerja'), 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?php
            echo $form->field($model, 'busines_unit')->dropDownList(Absensi::modelPerjanjianKerja()->_kontrak, ['prompt' => '*']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?php
            try {
                echo $form->field($model, 'golongan_id')->dropDownList(ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'), ['prompt' => '*']);
            } catch (InvalidConfigException $e) {
            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <?php
            //            $form->field($model, 'pegawai_id')->widget(Select2::class, [
            //                'options' => [
            //                    'placeholder' => Yii::t('app', '-- Pilih Pegawai'),
            //                    'multiple' => true,
            //                ],
            //                'data' => [],
            //                'maintainOrder' => true,
            //                'pluginOptions' => [
            //                    'allowClear' => true,
            //                    'minimumInputLength' => 3,
            //                    'language' => [
            //                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            //                    ],
            //                    'ajax' => [
            //                        'url' => Url::to(['/pegawai/pegawai/get-pegawai-for-select2']),
            //                        'dataType' => 'json',
            //                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
            //                    ],
            //                ],
            //            ]);
            ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <?php
            echo $form->field($model, 'tanggal', [
                'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                'options' => ['class' => 'drp-container form-group', 'id' => 'ads']
            ])->widget(DateRangePicker::class, [
                'options' => [
                    'id' => 'splsearch-tanggal-form',
                    'class' => 'form-control',
                ],
                'convertFormat' => true,
                'useWithAddon' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

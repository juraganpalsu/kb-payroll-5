<?php
/**
 * Created by PhpStorm.
 * File Name: create-insentif.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 23/08/2020
 * Time: 13:54
 */

/* @var $this yii\web\View */
/* @var $model frontend\models\Spl */
/** @var $splTemplate array */
/**
 */

$this->title = Yii::t('app', 'Create Insentif Lembur');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spl'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use yii\helpers\Html; ?>
<div class="spl-create">

    <?php if (Yii::$app->session->hasFlash('success-spl')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i><?= Yii::t('frontend', 'Saved!'); ?></h4>
            <?= Yii::$app->session->getFlash('success-spl') ?>

        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('fail-spl')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i><?= Yii::t('frontend', 'Failed!'); ?></h4>
            <?= Yii::$app->session->getFlash('fail-spl') ?>
        </div>
    <?php endif; ?>

    <?= $this->render('_formInsentif', [
        'model' => $model,
        'splTemplate' => $splTemplate
    ]) ?>

</div>

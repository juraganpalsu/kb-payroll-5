<?php

use common\components\Hari;
use common\components\Status;
use common\models\CostCenterTemplate;
use common\models\Spl;
use frontend\models\form\SplForm;
use frontend\models\SplTemplate;
use frontend\modules\spl\models\SplAlasan;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SplSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $modelSplForm SplForm
 */

$this->title = Yii::t('frontend', 'Spl');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="spl-index">
    <p>
        <?= Html::a(Yii::t('app', 'Approval'), ['approval'], ['class' => 'btn btn-info search-button btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Semua Data'), '#', ['class' => 'btn btn-info search-button btn-flat', 'disabled' => 'disabled']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function () {
                return GridView::ROW_COLLAPSED;
            },
            'enableRowClick' => true,
            'detailUrl' => Url::to(['tampilkan-user-approval']),
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        [
            'attribute' => 'seq_code',
            'value' => function (Spl $model) {
                $class = 'text-primary';
                if ($model->last_status == Status::APPROVED)
                    $class = 'text-success';
                if ($model->last_status == Status::REJECTED)
                    $class = 'text-danger';
                if ($model->last_status == Status::OPEN)
                    $class = 'text-warning';
                return Html::a(Html::tag('strong', $model->seq_code, ['class' => $class]), ['approval-form', 'id' => $model->id], ['class' => 'btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'SPL {seqcode} dibuat oleh {dibuat}', ['seqcode' => $model->seq_code, 'dibuat' => $model->createdBy->pegawai->nama . '[' . $model->createdByStruktur->nameCostume . ']'])]]);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'id_pegawai',
            'value' => function (Spl $model) {
                return $model->idPegawai;
            },
            'format' => 'html'
        ],
        [
            'attribute' => 'nama',
            'value' => function (Spl $model) {
                return $model->nama;
            },
            'format' => 'html'
        ],
        [
            'attribute' => 'sistem_kerja',
            'value' => function (Spl $model) {
                return $model->sistemKerja;
            },
            'hidden' => true
        ],
        [
            'attribute' => 'golongan_id',
            'value' => function (Spl $model) {
                return $model->getGolongan($model->tanggal);
            },
            'hidden' => true
        ],
        [
            'header' => Yii::t('frontend', 'Hari'),
            'value' => function (Spl $model) {
                return Hari::getHariIndonesia(date('D', strtotime($model->tanggal)));
            },
            'hidden' => true
        ],
        [
            'attribute' => 'tanggal',
            'value' => function (Spl $model) {
                return date('d-m-Y', strtotime($model->tanggal));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'header' => Yii::t('frontend', 'Jumlah Jam'),
            'value' => function (Spl $model) {
                return $model->splTemplate->jumlahKonversi;
            },
            'hidden' => true
        ],
        [
            'header' => Yii::t('frontend', 'Total Jam'),
            'value' => function (Spl $model) {
                $jumlahPegawai = count($model->splDetails);
                return $jumlahPegawai * $model->splTemplate->jumlahKonversi;
            },
            'hidden' => true
        ],
        [
            'attribute' => 'spl_template_id',
            'label' => Yii::t('app', 'Spl Template'),
            'value' => function (Spl $model) {
                $ket = $model->splTemplate->masuk . 's/d' . $model->splTemplate->pulang . '(' . $model->splTemplate->kategoriSingkatan . ')';
                return $ket;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(SplTemplate::find()->all(), 'id', 'namaKomplit'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Spl template', 'id' => 'grid-spl-search-spl_template_id']
        ],
        [
            'attribute' => 'last_status',
            'value' => function (Spl $model) {
                return Status::statuses($model->last_status);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::statuses(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status')]
        ],
        [
            'attribute' => 'cost_center_id',
            'value' => function (Spl $model) {
                return $model->costCenter ? $model->costCenter->kodeArea : '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(CostCenterTemplate::find()->all(), 'id', 'kodeArea'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Cost Center')]
        ],
        [
            'attribute' => 'spl_alasan_id',
            'value' => function (Spl $model) {
                return $model->splAlasan ? $model->splAlasan->nama : '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(SplAlasan::find()->orderBy('nama')->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Alasan')]
        ],
        [
            'label' => Yii::t('frontend', 'Created By'),
            'value' => function (Spl $model) {
                return $model->createdBy->userPegawai->pegawai->nama;
            },
        ],
        [
            'attribute' => 'keterangan',
            'contentOptions' => ['style' => 'width: 200px;'],
        ],
        ['attribute' => 'lock', 'hidden' => true],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{print}',
            'buttons' => [
                'print' => function ($url, Spl $model) {
                    if ($model->last_status != Status::REJECTED) {
                        return Html::a(Yii::t('frontend', 'Print'), $url, ['class' => 'btn btn-xs btn-success btn-flat', 'data' => ['header' => Yii::t('frontend', 'Update Sistem Kerja'), 'pjax' => 0], 'target' => '_blank']);
                    }
                    return '';
                }
            ],
            'contentOptions' => ['style' => 'width: 200px;'],
        ],
    ];
    ?>
    <?php try {

        $export = ExportMenu::widget([
            'id' => 'exportmenu-spl',
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
            'noExportColumns' => [1, 15, 16], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'spl' . date('dmYHis')
        ]);
        echo GridView::widget([
            'id' => 'grid-spl',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'options' => ['style' => 'table-layout:fixed;'],
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl'], 'enablePushState' => false,],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

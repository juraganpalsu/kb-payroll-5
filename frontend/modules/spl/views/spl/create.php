<?php


/* @var $this yii\web\View */
/* @var $model frontend\models\Spl */
/** @var $splTemplate array */
/**
 */

$this->title = Yii::t('app', 'Create Spl');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spl'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use yii\helpers\Html; ?>
<div class="spl-create">

    <?php if (Yii::$app->session->hasFlash('success-spl')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i><?= Yii::t('frontend', 'Saved!'); ?></h4>
            <?= Yii::$app->session->getFlash('success-spl') ?>

        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('fail-spl')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i><?= Yii::t('frontend', 'Failed!'); ?></h4>
            <?= Yii::$app->session->getFlash('fail-spl') ?>
        </div>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
        'splTemplate' => $splTemplate
    ]) ?>

</div>

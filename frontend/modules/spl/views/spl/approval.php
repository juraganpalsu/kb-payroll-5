<?php

use common\components\Hari;
use common\components\Status;
use common\models\Spl;
use frontend\models\SplTemplate;
use frontend\modules\spl\models\SplAlasan;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SplSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $modelSpl Spl */

$this->title = Yii::t('app', 'Spl Approval');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function ($) {    
     $('.btn-call-modal').click(function(e){
        e.preventDefault();
        var idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
     
     $('#form-modal').on('hidden.bs.modal', function () {
        location.reload();
     });
    
    $('.search-button').click(function(){
	    $('.search-form').toggle(1000);
	        return false;
    });
    
    $("#kv-pjax-container-spl").on("pjax:success", function() {
        $('.btn-call-modal').click(function(e){
            e.preventDefault();
            var idModal = $('#form-modal');
            idModal.find('.modal-title').text($(this).data('header'));
            idModal.modal('show')
               .find('#modelContent')
               .load($(this).attr('href'));
            return false;
        });
    });
    
    $('.approve-all-btn').click(function(e) {
        e.preventDefault();
        var btn = $(this);
        btn.button('loading');
        $.post(btn.attr('href'))
        .done(function (dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = btn.data('url');
            }
            btn.button('reset');
        });
        return false;
    });

    $('.reject-all-btn').click(function(e) {
        e.preventDefault();
        var btn = $(this);
        btn.button('loading');
        krajeeDialog.prompt({label:'Berikan alasan', value: '', placeholder:'Sampai 225 karakter...', maxlength: 225}, function (result) {
            if (result === ''){
                result = '-';
            } 
            if (result) {
                $.post(btn.attr('href'), {'keterangan' : result})
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = btn.data('url');
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);

?>
    <div class="spl-index">
        <p>
            <?= Html::a(Yii::t('app', 'Approval'), '#', ['class' => 'btn btn-info search-button btn-flat', 'disabled' => 'disabled']) ?>
            <?= Html::a(Yii::t('app', 'Semua Data'), ['semua-data'], ['class' => 'btn btn-info btn-flat']) ?>
        </p>
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'hidden' => true],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',
                'value' => function () {
                    return GridView::ROW_COLLAPSED;
                },
                'enableRowClick' => true,
                'detailUrl' => Url::to(['tampilkan-user-approval']),
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true
            ],
            [
                'attribute' => 'seq_code',
                'value' => function (Spl $model) {
                    return Html::a(Html::tag('strong', $model->seq_code, ['class' => 'text-danger']), ['approval-form', 'id' => $model->id], ['class' => 'btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'SPL {seqcode} dibuat oleh {dibuat}', ['seqcode' => $model->seq_code, 'dibuat' => $model->createdBy->pegawai->nama . '[' . $model->createdByStruktur->nameCostume . ']'])]]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'kategori',
                'value' => function (Spl $model) {
                    return $model->_kategori[$model->kategori];
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelSpl->_kategori,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Kategori')]
            ],
            [
                'attribute' => 'id_pegawai',
                'value' => function (Spl $model) {
                    return $model->idPegawai;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'nama',
                'value' => function (Spl $model) {
                    return $model->nama;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'sistem_kerja',
                'value' => function (Spl $model) {
                    return $model->sistemKerja;
                },
                'hidden' => true
            ],
            [
                'attribute' => 'golongan_id',
                'value' => function (Spl $model) {
                    return $model->getGolongan($model->tanggal);
                },
                'hidden' => true
            ],
            [
                'header' => Yii::t('frontend', 'Hari'),
                'value' => function (Spl $model) {
                    return Hari::getHariIndonesia(date('D', strtotime($model->tanggal)));
                },
                'hidden' => true
            ],
            [
                'attribute' => 'tanggal',
                'value' => function (Spl $model) {
                    return date('d-m-Y', strtotime($model->tanggal));
                },
            ],            
            [
                'header' => Yii::t('frontend', 'Total Jam'),
                'value' => function (Spl $model) {
                    $jumlahPegawai = count($model->splDetails);
                    return $jumlahPegawai * $model->splTemplate->jumlahKonversi;
                },
                'hidden' => true
            ],
            [
                'attribute' => 'spl_template_id',
                'label' => Yii::t('app', 'Spl Template'),
                'value' => function (Spl $model) {
                    $ket = $model->splTemplate->masuk . 's/d' . $model->splTemplate->pulang . '(' . $model->splTemplate->kategoriSingkatan . ')';
                    return $ket;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(SplTemplate::find()->all(), 'id', 'namaKomplit'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Spl template', 'id' => 'grid-spl-search-spl_template_id']
            ],
            [
                'header' => Yii::t('frontend', 'Jumlah Jam'),
                'value' => function (Spl $model) {
                    return $model->splTemplate->jumlahKonversi;
                },
            ],
            [
                'attribute' => 'is_planing',
                'value' => function (Spl $model) {
                    return Status::activeInactive($model->is_planing);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Status::activeInactive(0, true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Rencana')]
            ],
            [
                'attribute' => 'klaim_quaker',
                'value' => function (Spl $model) {
                    return Status::activeInactive($model->klaim_quaker);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Status::activeInactive(0, true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Klaim')]
            ],
            [
                'attribute' => 'spl_alasan_id',
                'value' => function (Spl $model) {
                    return $model->splAlasan ? $model->splAlasan->nama : '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(SplAlasan::find()->orderBy('nama')->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Alasan')]
            ],
            [
                'attribute' => 'created_by',
                'value' => function (Spl $model) {
                    return $model->createdBy->userPegawai->pegawai->nama;
                },
            ],
            [
                'attribute' => 'created_at',
                'value' => function (Spl $model) {
                    return date('d-m-Y H:m', strtotime($model->created_at));
                },
            ],
            [
                'attribute' => 'keterangan',
                'contentOptions' => ['style' => 'width: 200px;'],
            ],
            ['attribute' => 'lock', 'hidden' => true],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{approval-form} {approve-all} {reject-all}',
                'buttons' => [
                    'approval-form' => function ($url, Spl $model) {
                        return Html::a(Yii::t('frontend', 'View Detail'), $url, ['class' => 'btn-call-modal btn btn-primary btn-xs btn-flat', 'data' => ['header' => Yii::t('frontend', 'SPL {seqcode} dibuat oleh {dibuat}', ['seqcode' => $model->seq_code, 'dibuat' => $model->createdBy->pegawai->nama . '[' . $model->createdByStruktur->nameCostume . ']'])]]);
                    },
                    'approve-all' => function ($url, Spl $model) {
                        $class = $model->checkStatusApprovals() ? 'btn-warning' : 'btn-success';
                        return Html::a(Yii::t('frontend', 'Approve All'), $url, ['class' => 'btn btn-xs ' . $class . ' approve-all-btn btn-flat', 'data' => ['url' => Url::to(['approval', 'click' => '#spl-row-' . $model->id])]]);
                    },
                    'reject-all' => function ($url, Spl $model) {
                        return Html::a(Yii::t('frontend', 'Reject All'), $url, ['class' => 'btn btn-xs btn-danger reject-all-btn btn-flat', 'data' => ['url' => Url::to(['approval', 'click' => '#spl-row-' . $model->id])]]);
                    }
                ]
            ],
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [2, 3, 4, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
                'noExportColumns' => [1, 20, 21], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'spkl-submited' . date('dmYHis')
            ]);
            echo GridView::widget([
                'id' => 'grid-spl',
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'options' => ['style' => 'table-layout:fixed;'],
                'rowOptions' => function (Spl $model) {
                    return ['id' => 'spl-row-' . $model->id];
                },
                'pjax' => false,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl'], 'enablePushState' => false,],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
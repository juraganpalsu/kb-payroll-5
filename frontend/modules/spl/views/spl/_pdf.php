<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Spl */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spl'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spl-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Spl').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'tanggal',
        [
                'attribute' => 'splTemplate.id',
                'label' => Yii::t('app', 'Spl Template')
        ],
        'keterangan',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnSplDetail = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('app', 'Pegawai')
        ],
        [
                'attribute' => 'spl.id',
                'label' => Yii::t('app', 'Spl')
        ],
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSplDetail,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl-detail']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Spl Detail').' '. $this->title),
        ],
        'columns' => $gridColumnSplDetail
    ]);
?>
    </div>
</div>

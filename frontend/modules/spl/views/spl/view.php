<?php

use common\components\Bulan;
use common\components\Status;
use common\models\Spl;
use mdm\admin\AnimateAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\YiiAsset;
use yii\widgets\DetailView;
use kartik\dialog\DialogAsset;

/* @var $this yii\web\View */
/* @var $model frontend\models\Spl */

/** @var ActiveDataProvider $providerSplDetail */

$this->title = $model->splTemplate->namaKomplit;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Spl'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

DialogAsset::register($this);
AnimateAsset::register($this);
YiiAsset::register($this);
$opts = Json::htmlEncode([
    'items' => $model->getListPegawai(),
]);
$this->registerJs("var _opts = {$opts};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';

$js = <<<JS
$(function ($) {    
    $('.btn-submit').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        let conf = confirm(btn.data('confirm'));
            if(conf){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }else{
                        alert(dt.message)
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        return false;
    });
    
});
JS;

$this->registerJs($js);


?>
<div class="spl-view">

    <div class="row">
        <div class="col-sm-3" style="margin-top: 15px">
            <?= Html::a(Yii::t('frontend', 'Print'), ['print', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-flat', 'target' => '_blank']) ?>
            <?php if ($model->last_status == Status::OPEN) { ?>
                <?= Html::a(Yii::t('frontend', 'Submit'), ['submit', 'id' => $model->id], ['id' => 'asdsad', 'class' => 'btn btn-xs btn-warning btn-submit btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure want to submit this item?'), 'pjax' => 0]]) ?>
                <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-xs btn-flat',
                    'data' => [
                        'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumn = [
                [
                    'attribute' => 'seq_code',
                    'value' => function (Spl $model) {
                        $class = 'text-primary';
                        if ($model->last_status == Status::APPROVED)
                            $class = 'text-success';
                        if ($model->last_status == Status::REJECTED)
                            $class = 'text-danger';
                        if ($model->last_status == Status::OPEN)
                            $class = 'text-warning';
                        $nameCostume = $model->createdByStruktur ? $model->createdByStruktur->nameCostume : '';
                        return Html::a(Html::tag('strong', $model->seq_code, ['class' => $class]), '#', ['class' => 'btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'SPL {seqcode} dibuat oleh {dibuat}', ['seqcode' => $model->seq_code, 'dibuat' => $model->createdBy->pegawai->nama . '[' . $nameCostume . ']'])]]);
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'tanggal',
                    'value' => function (Spl $model) {
                        return Bulan::tanggal($model->tanggal);
                    },
                ],
                [
                    'attribute' => 'klaim_quaker',
                    'value' => function (Spl $model) {
                        return Status::activeInactive($model->klaim_quaker);
                    },
                    'visible' => $model->kategori != Spl::LEMBUR_INSENTIF ? true : false
                ],
                [
                    'attribute' => 'splTemplate.namaKomplit',
                    'label' => Yii::t('frontend', 'Spl Template'),
                ],
                [
                    'attribute' => 'splTemplate.istirahat',
                ],
                [
                    'label' => Yii::t('frontend', 'Jumlah Jam'),
                    'value' => function (Spl $model) {
                        if ($model->kategori != Spl::LEMBUR_INSENTIF) {
                            return $model->splTemplate->jumlahKonversi;
                        }
                        return $model->splTemplate->jumlah;
                    }
                ],
                [
                    'attribute' => 'spl_alasan_id',
                    'value' => function (Spl $model) {
                        return $model->splAlasan ? $model->splAlasan->nama : '';
                    },
                ],
                [
                    'attribute' => 'last_status',
                    'value' => function (Spl $model) {
                        return Status::statuses($model->last_status);
                    },
                ],
                'keterangan',
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
    <div class="assignment-index">
        <div class="row">
            <div class="col-sm-5">
                <input class="form-control search" data-target="available"
                       placeholder="<?= Yii::t('frontend', 'Search for available'); ?>">
                <select multiple size="20" class="form-control list" data-target="available"> </select>
            </div>
            <div class="col-sm-1">
                <br><br>
                <?= ($model->last_status == Status::OPEN) ? Html::a('&gt;&gt;' . $animateIcon, ['assign', 'id' => (string)$model->id], [
                    'class' => 'btn btn-success btn-assign',
                    'data-target' => 'available',
                    'title' => Yii::t('frontend', 'Assign'),
                ]) : Html::a('&gt;&gt;' . $animateIcon, '#', ['class' => 'btn btn-success', 'disabled' => 'disabled']); ?>
                <br><br>
                <?= ($model->last_status == Status::OPEN) ? Html::a('&lt;&lt;' . $animateIcon, ['revoke', 'id' => (string)$model->id], [
                    'class' => 'btn btn-danger btn-assign',
                    'data-target' => 'assigned',
                    'title' => Yii::t('frontend', 'Remove'),
                ]) : Html::a('&lt;&lt;' . $animateIcon, '#', ['class' => 'btn btn-danger', 'disabled' => 'disabled']); ?>
            </div>
            <div class="col-sm-5">
                <input class="form-control search" data-target="assigned"
                       placeholder="<?= Yii::t('frontend', 'Search for assigned'); ?>">
                <select multiple size="20" class="form-control list" data-target="assigned"> </select>
            </div>
        </div>
    </div>

</div>
<?php
/**
 * Created by PhpStorm.
 * File Name: index-lembur-insentif.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 25/09/2020
 * Time: 14:08
 */

use common\components\Hari;
use common\components\Status;
use common\models\Spl;
use frontend\models\form\SplForm;
use frontend\models\SplTemplate;
use frontend\modules\spl\models\SplAlasan;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SplSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $modelSplForm SplForm
 * @var string $redirect
 */

$this->title = Yii::t('app', 'Insentif Lembur');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function ($) {
     $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
     
     $('#form-modal').on('hidden.bs.modal', function () {
        location.reload();
     });
    
    $("#kv-pjax-container-spl").on("pjax:success", function() {
        $('.btn-call-modal').click(function(e){
            e.preventDefault();
            let idModal = $('#form-modal');
            idModal.find('.modal-title').text($(this).data('header'));
            idModal.modal('show')
               .find('#modelContent')
               .load($(this).attr('href'));
            return false;
        });
    });
    
    $('.btn-submit').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    if(dt.status === true){
                        // location.href = btn.data('url');
                        // let win = window.open(dt.data.url, '_blank');
                        // win.focus();
                        location.reload();
                    }else{
                        btn.button('reset');
                        console.log(dt);
                        krajeeDialog.alert(dt.message);
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
});
JS;

$this->registerJs($js);

?>
    <div class="spl-index">
        <p>
            <?php
            if (Helper::checkRoute('/spl/spl/create-insentif')) {
                echo Html::a(Yii::t('frontend', 'Buat Insentif Lembur'), ['create-insentif'], ['class' => 'btn btn-success btn-flat']);
            }
            ?>
        </p>
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'hidden' => true],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',
                'value' => function () {
                    return GridView::ROW_COLLAPSED;
                },
                'enableRowClick' => true,
                'detailUrl' => Url::to(['tampilkan-user-approval']),
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true
            ],
            [
                'attribute' => 'seq_code',
                'value' => function (Spl $model) {
                    $class = 'text-primary';
                    if ($model->last_status == Status::APPROVED)
                        $class = 'text-success';
                    if ($model->last_status == Status::REJECTED)
                        $class = 'text-danger';
                    if ($model->last_status == Status::OPEN)
                        $class = 'text-warning';
                    $nameCostume = $model->createdByStruktur ? $model->createdByStruktur->nameCostume : '';
                    $createdBy = $model->createdBy->pegawai;
                    return Html::a(Html::tag('strong', $model->seq_code, ['class' => $class]), ['approval-form', 'id' => $model->id], ['class' => 'btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'SPL {seqcode} dibuat oleh {dibuat}', ['seqcode' => $model->seq_code, 'dibuat' => $createdBy ? $createdBy->nama : '' . '[' . $nameCostume . ']'])]]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'id_pegawai',
                'value' => function (Spl $model) {
                    return $model->idPegawai;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'nama',
                'value' => function (Spl $model) {
                    return $model->nama;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'sistem_kerja',
                'value' => function (Spl $model) {
                    return $model->sistemKerja;
                },
                'hidden' => true
            ],
            [
                'header' => Yii::t('frontend', 'Hari'),
                'value' => function (Spl $model) {
                    return Hari::getHariIndonesia(date('D', strtotime($model->tanggal)));
                },
                'hidden' => true
            ],
            [
                'attribute' => 'tanggal',
                'value' => function (Spl $model) {
                    return date('d-m-Y', strtotime($model->tanggal));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            [
                'attribute' => 'spl_template_id',
                'label' => Yii::t('app', 'Template Lembur'),
                'value' => function (Spl $model) {
                    $ket = $model->splTemplate->masuk . 's/d' . $model->splTemplate->pulang . '(' . $model->splTemplate->kategoriSingkatan . ')';
                    return $ket;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(SplTemplate::find()->all(), 'id', 'namaKomplit'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Spl template', 'id' => 'grid-spl-search-spl_template_id']
            ],
            [
                'attribute' => 'jumlah_jam',
                'value' => function (Spl $model) {
                    return $model->splTemplate->jumlah;
                }
            ],
            [
                'header' => Yii::t('frontend', 'Total Jam'),
                'value' => function (Spl $model) {
                    $jumlahPegawai = count($model->splDetails);
                    return $jumlahPegawai * $model->splTemplate->jumlah;
                },
                'hidden' => true
            ],
            [
                'attribute' => 'last_status',
                'value' => function (Spl $model) {
                    return Status::statuses($model->last_status);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Status::statuses(0, true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status')]
            ],
            [
                'attribute' => 'spl_alasan_id',
                'value' => function (Spl $model) {
                    return $model->splAlasan ? $model->splAlasan->nama : '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(SplAlasan::find()->orderBy('nama')->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Alasan')]
            ],
            [
                'attribute' => 'created_by',
                'value' => function (Spl $model) {
                    $createdBy = $model->createdBy->userPegawai;
                    return $createdBy ? $createdBy->pegawai->nama : '';
                },
            ],
            [
                'attribute' => 'created_at',
                'value' => function (Spl $model) {
                    return date('d-m-Y H:m', strtotime($model->created_at));
                },
            ],
            [
                'attribute' => 'keterangan',
                'contentOptions' => ['style' => 'width: 200px;'],
            ],
            ['attribute' => 'lock', 'hidden' => true],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{/spl/spl/print} {/spl/spl/submit} {set-revisi} {create-revisi} {/spl/spl/delete}',
                'buttons' => [
                    '/spl/spl/print' => function ($url, Spl $model) {
                        if ($model->last_status != Status::REJECTED) {
                            return Html::a(Yii::t('frontend', 'Print'), $url, ['class' => 'btn btn-xs btn-success btn-flat', 'data' => ['header' => Yii::t('frontend', 'Update Sistem Kerja'), 'pjax' => 0], 'target' => '_blank']);
                        }
                        return '';
                    },
                    '/spl/spl/submit' => function ($url, Spl $model) {
                        if ($model->last_status == Status::OPEN) {
                            return Html::a('Submit', $url, ['class' => 'btn btn-xs btn-warning btn-submit btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure want to submit this item?'), 'pjax' => 0]]);
                        }
                        return '';
                    },
                    'set-revisi' => function ($url, Spl $model) use ($searchModel) {
                        if (in_array($model->last_status, [Status::FINISHED, Status::APPROVED]) && $searchModel->showReviseBtn) {
                            return Html::a('Revisi', $url, ['class' => 'btn btn-xs btn-primary btn-revisi btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merevisi SPL ini?'), 'pjax' => 0]]);
                        }
                        return '';
                    },
                    'create-revisi' => function ($url, Spl $model) use ($searchModel) {
                        if (in_array($model->last_status, [Status::REVISED]) && $searchModel->showReviseBtn) {
                            return Html::a('Buat Revisi', $url, ['class' => 'btn btn-xs btn-primary btn-flat btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Buat revisi untuk SPL : #{seqCode}', ['seqCode' => $model->seqCode]), 'pjax' => 0]]);
                        }
                        return '';
                    },
                    'update' => function ($url, Spl $model) {
                        if ($model->last_status == Status::OPEN) {
                            return Html::a('Update', $url, ['class' => 'btn btn-xs btn-primary btn-create', 'data-pjax' => "0"]);
                        }
                        return '';
                    },
                    '/spl/spl/delete' => function ($url, Spl $model) use ($searchModel, $redirect) {
                        if ($model->last_status == Status::OPEN || $searchModel->showDeleteBtn) {
                            return Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id, 'redirect' => $redirect], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                        }
                        return '';
                    }
                ],
                'contentOptions' => ['style' => 'width: 200px;'],
            ],
        ];
        ?>
        <?php try {

            $export = ExportMenu::widget([
                'id' => 'exportmenu-spl',
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
                'noExportColumns' => [1, 2], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'spl' . date('dmYHis')
            ]);
            echo GridView::widget([
                'id' => 'grid-spl',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'options' => ['style' => 'table-layout:fixed;'],
                'pjax' => false,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl'], 'enablePushState' => false,],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/spl/spl/create-insentif'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'SPL(rencana)'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/spl/spl/index-lembur-insentif'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            print_r($e->getMessage());
            Yii::info($e->getMessage(), 'exception');
        } ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
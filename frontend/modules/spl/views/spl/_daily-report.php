<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 05/02/2019
 * Time: 20:50
 */

use common\components\Status;
use common\models\Spl;
use common\models\SplDetail;
use frontend\models\form\SplForm;

/**
 * @var $model SplForm
 */

$header = $this->renderAjax('_daily-report-header', ['model' => $model]);
$height = 4;
$row = 1;
$rowHeader = 6;
$maxLengthName = 30;
$totalHeight = 0;
$countPeriod = 1;
$maxRow = 60;
$countDetail = 0;
echo $header;
/** @var Spl $spl */
foreach ($dataProvider as $spl) {
    $countDetail = count($spl->splDetails);
    ?>
    <tr>
        <td colspan="16" style="border: 1px solid">
            <?= $spl->seq_code ?>
        </td>
        <td colspan="4" rowspan="<?= $countDetail + 1; ?>" style="border: 1px solid"><?= $spl->keterangan; ?></td>
        <td colspan="4" style="border: 1px solid"></td>
    </tr>
    <?php
    /** @var SplDetail $splDetail */
    foreach ($spl->splDetails as $splDetail) {
        ?>
        <tr>
            <td colspan="2"
                style="text-align: center; border: 1px dotted; border-left: 1px solid"><?= $splDetail->pegawai->idfp; ?></td>
            <td colspan="6" style="border: 1px dotted"><?= $splDetail->pegawai->nama; ?></td>
            <td style="text-align: center; border: 1px dotted"><?php
                echo date('H:i', strtotime($splDetail->spl->splTemplate->masuk));
                ?></td>
            <td style="text-align: center; border: 1px dotted"><?php
                echo date('H:i', strtotime($splDetail->spl->splTemplate->pulang));
                ?></td>
            <td style="text-align: center; border: 1px dotted"><?= $splDetail->spl->splTemplate->jumlahKonversi; ?></td>
            <td style="border: 1px dotted"></td>
            <td style="border: 1px dotted"></td>
            <td style="border: 1px dotted"></td>
            <td style="border: 1px dotted"></td>
            <td style="border: 1px dotted"></td>
            <td colspan="4"
                style="border: 1px dotted; border-right: 1px solid"><?= $splDetail->tipe_approval == Status::APPROVED ? $spl->splTemplate->istirahat > 0 ? Yii::t('frontend', 'Istirahat') . $spl->splTemplate->istirahat . Yii::t('frontend', 'Jam') : Yii::t('frontend', 'Tanpa istirahat') : $splDetail->keterangan_approval; ?></td>
        </tr>
        <?php
    }
}
?>

<tr>
    <td colspan="2" style="border-top: 1px solid; text-align: center; border-left: 1px solid;"></td>
    <td colspan="3" style="border-top: 1px solid; text-align: center;"><?= Yii::t('frontend', 'Dibuat Oleh') ?>,</td>
    <td colspan="3" style="border-top: 1px solid; text-align: center;"><?= Yii::t('frontend', 'Diketahui Oleh') ?>,</td>
    <td colspan="7" style="border-top: 1px solid; text-align: center;"></td>
    <td colspan="6" style="border-top: 1px solid; text-align: center;"><?= Yii::t('frontend', 'Disetujui Oleh') ?>,</td>
    <td colspan="3"
        style="border-top: 1px solid; text-align: center; border-right: 1px solid;"><?= Yii::t('frontend', 'Diperikasa Oleh') ?>
        ,
    </td>
</tr>
<tr>
    <td colspan="24" style="height: 40px; border-left: 1px solid; border-right: 1px solid;">&nbsp;</td>
</tr>
<tr>
    <td colspan="2" style="text-align: center; border-left: 1px solid; border-bottom: 1px solid;"></td>
    <td colspan="3" style="text-align: center; border-bottom: 1px solid;">(<?= Yii::t('frontend', '____________') ?>)
    </td>
    <td colspan="3" style="text-align: center; border-bottom: 1px solid;">
        (<?= Yii::t('frontend', 'Dept./ Divisi Head') ?>)
    </td>
    <td colspan="7" style="text-align: center; border-bottom: 1px solid;"></td>
    <td colspan="3" style="text-align: center; border-bottom: 1px solid;">(<?= Yii::t('frontend', 'Factory  Manager') ?>
        )
    </td>
    <td colspan="3" style="text-align: center; border-bottom: 1px solid;">(<?= Yii::t('frontend', 'BOD') ?>)</td>
    <td colspan="3" style="text-align: center; border-right: 1px solid; border-bottom: 1px solid;">
        (<?= Yii::t('frontend', 'HC Dept.') ?>)
    </td>
</tr>
<tr>
    <td colspan="12" style="text-align: left"><?= date('d-m-Y H:i:s'); ?></td>
    <td colspan="12" style="text-align: right"><?= '' ?></td>
</tr>
</table>

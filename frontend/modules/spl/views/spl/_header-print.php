<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 8/28/17
 * Time: 10:30 PM
 */

use common\components\Status;
use frontend\models\Spl;
use yii\helpers\Html;


/** @var Spl $model */

$height = 4;
?>

<table style="border-collapse: collapse;">
    <tr>
        <td width="9mm" style="border: 1px white">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" rowspan="3"
            style="border: 1px solid; text-align: center">
            <div><?php echo Html::img('@frontend/web/pictures/image001.png', ['height' => '40px']); ?></div>
        </td>
        <td colspan="13" rowspan="3"
            style="text-align: center; border: 1px solid;">
            <h3><strong>PT KOBE BOGA UTAMA</strong></h3>
        </td>
        <td colspan="3" style="border: 1px solid; border-right: 1px solid white;">No. Kode Dok
        </td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white;">:
            KBU/FO/HCD/18/030
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border-left: 1px solid black; border-bottom: 1px solid black; ">Tingkatan Dok</td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white; ">: IV</td>
    </tr>
    <tr>
        <td colspan="3" style="border-left: 1px solid black; border-bottom: 1px solid black; ">Status Revisi</td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white;">: 04</td>
    </tr>
    <tr>
        <td colspan="4" rowspan="2"
            style=" text-align: center; border: 1px solid;">
            Human Capital <br/>Development
        </td>
        <td colspan="13" rowspan="2"
            style=" text-align: center; border-bottom: 1px solid black; border-top: 1px solid black;">
            <h4><strong>SURAT PERINTAH KERJA LEMBUR</strong></h4>
        </td>
        <td colspan="3" style="border-left: 1px solid black; border-bottom: 1px solid black; ">Tgl. Mulai Berlaku</td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white;">: 26 Februari 2018</td>
    </tr>
    <tr>
        <td colspan="3" style="border-left: 1px solid black; border-bottom: 1px solid black; ">Halaman</td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white;">: 1 dari 1</td>
    </tr>
    <tr>
        <td colspan="3"
            style=" border-left: 1px solid black; border-bottom: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo 'Department/ Section'; ?>
        </td>
        <td colspan="21" style=" border-right: 1px solid black; border-bottom: 1px solid black">
            <strong> :
                <?php echo $model->createdByStruktur->nameCostume; ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td colspan="3"
            style=" border-left: 1px solid black; border-bottom: 1px solid black; height: <?php echo $height; ?>mm ">
            <?= Yii::t('frontend', 'Tanggal'); ?>
        </td>
        <td colspan="16" style=" border-right: 1px solid black; border-bottom: 1px solid black">
            <strong> :
                <?php
                echo date('d-m-Y', strtotime($model->tanggal)); ?>
            </strong>
        </td>
        
        <td colspan="2 " style="border-bottom: 1px solid black">
            <?= Status::activeInactive($model->klaim_quaker) == 'Ya' ? Yii::t('frontend', 'Klaim Quaker') : '' ?>
        </td>
        <td colspan="12" style=" border-right: 1px solid black; border-bottom: 1px solid black">
            <strong> 
                <?= Status::activeInactive($model->klaim_quaker) == 'Ya' ? ': ' .Status::activeInactive($model->klaim_quaker) : '' ?>
            </strong>
        </td>
    
    <tr>
        <td colspan="2" rowspan="3" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            No. NIK
        </td>
        <td colspan="6" rowspan="3" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Nama
        </td>
        <td colspan="3" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Rencana
        </td>
        <td colspan="5" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Realisasi
        </td>
        <?php if ($model->kategori == Spl::LEMBUR_BIASA) { ?>
            <td colspan="4" rowspan="3" class="center head"
                style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
                <?= Yii::t('frontend', 'Kategori Alasan'); ?>
            </td>
            <td colspan="4" rowspan="3" class="center head"
                style="text-align: center; background-color: #EEEEEE; border: 1px solid; height: <?php echo $height; ?>mm ">
                <?= Yii::t('frontend', 'Keterangan'); ?>
            </td>
        <?php } ?>
        <?php if ($model->kategori == Spl::LEMBUR_INSENTIF) { ?>
            <td colspan="8" rowspan="3" class="center head"
                style="text-align: center; background-color: #EEEEEE; border: 1px solid; height: <?php echo $height; ?>mm ">
                <?= Yii::t('frontend', 'Keterangan'); ?>
            </td>
        <?php } ?>
    </tr>
    <tr>
        <td rowspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Mulai') ?>
        </td>
        <td rowspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Selesai') ?>
        </td>
        <td rowspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Jumlah
        </td>
        <td rowspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Mulai') ?>
        </td>
        <td rowspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Selesai') ?>
        </td>
        <td rowspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Jumlah
        </td>
        <td colspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Paraf
        </td>
    </tr>
    <tr>
        <td class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?= Yii::t('frontend', 'Adm') ?>
        </td>
        <td class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?= Yii::t('frontend', 'Spv') ?>
        </td>
    </tr>

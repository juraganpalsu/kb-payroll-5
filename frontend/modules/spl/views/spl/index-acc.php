<?php

/**
 * Created by PhpStorm.
 * File Name: index-acc.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 23/04/20
 * Time: 21.12
 */

use common\components\Hari;
use common\components\Status;
use common\models\Spl;
use frontend\models\SplTemplate;
use frontend\modules\spl\models\SplAlasan;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SplSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $modelSpl Spl */

$this->title = Yii::t('app', 'Spl');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function ($) {
     $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
     
     $('#form-modal').on('hidden.bs.modal', function () {
        location.reload();
     });
    
    $('.search-button').click(function(){
	    $('.search-form').toggle(1000);
	        return false;
    });
    
    $("#kv-pjax-container-spl").on("pjax:success", function() {
        $('.btn-call-modal').click(function(e){
            e.preventDefault();
            let idModal = $('#form-modal');
            idModal.find('.modal-title').text($(this).data('header'));
            idModal.modal('show')
               .find('#modelContent')
               .load($(this).attr('href'));
            return false;
        });
    });
    
});
JS;

$this->registerJs($js);

?>
    <div class="spl-index">
        <p>
            <?php
            if (Helper::checkRoute('/spl/spl/daily-report-form')) {
                echo Html::a(Yii::t('frontend', 'Daily Report'), ['daily-report-form'], ['class' => 'btn btn-success btn-flat btn-daily-report-form btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Daily Report')]]);
            }
            ?>
            <?php
            if (Helper::checkRoute('/spl/spl/export-excel-form')) {
                echo Html::a(Yii::t('frontend', 'Export As Excel'), ['export-excel-form'], ['class' => 'btn btn-success btn-flat btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Export As Axcel')]]);
            }
            ?>
            <?= Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button btn-flat']) ?>
        </p>
        <div class="search-form" style="display:none">
            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'hidden' => true],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',
                'value' => function () {
                    return GridView::ROW_COLLAPSED;
                },
                'enableRowClick' => true,
                'detailUrl' => Url::to(['tampilkan-user-approval']),
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true
            ],
            [
                'attribute' => 'seq_code',
                'value' => function (Spl $model) {
                    $class = 'text-primary';
                    if ($model->last_status == Status::APPROVED)
                        $class = 'text-success';
                    if ($model->last_status == Status::REJECTED)
                        $class = 'text-danger';
                    if ($model->last_status == Status::OPEN)
                        $class = 'text-warning';
                    $nameCostume = $model->createdByStruktur ? $model->createdByStruktur->nameCostume : '';
                    return Html::a(Html::tag('strong', $model->seq_code, ['class' => $class]), ['approval-form', 'id' => $model->id], ['class' => 'btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'SPL {seqcode} dibuat oleh {dibuat}', ['seqcode' => $model->seq_code, 'dibuat' => $model->createdBy->pegawai->nama . '[' . $nameCostume . ']'])]]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'kategori',
                'value' => function (Spl $model) {
                    return $model->_kategori[$model->kategori];
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelSpl->_kategori,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Kategori')]
            ],
            [
                'attribute' => 'id_pegawai',
                'value' => function (Spl $model) {
                    return $model->idPegawai;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'nama',
                'value' => function (Spl $model) {
                    return $model->nama;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'sistem_kerja',
                'value' => function (Spl $model) {
                    return $model->sistemKerja;
                },
                'hidden' => true
            ],
            [
                'header' => Yii::t('frontend', 'Hari'),
                'value' => function (Spl $model) {
                    return Hari::getHariIndonesia(date('D', strtotime($model->tanggal)));
                },
                'hidden' => true
            ],
            [
                'attribute' => 'tanggal',
                'value' => function (Spl $model) {
                    return date('d-m-Y', strtotime($model->tanggal));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            [
                'header' => Yii::t('frontend', 'Jumlah Jam'),
                'value' => function (Spl $model) {
                    return $model->splTemplate->jumlahKonversi;
                },
                'hidden' => true
            ],
            [
                'header' => Yii::t('frontend', 'Total Jam'),
                'value' => function (Spl $model) {
                    $jumlahPegawai = count($model->splDetails);
                    return $jumlahPegawai * $model->splTemplate->jumlahKonversi;
                },
                'hidden' => true
            ],
            [
                'attribute' => 'spl_template_id',
                'label' => Yii::t('app', 'Spl Template'),
                'value' => function (Spl $model) {
                    return $model->splTemplate->masuk . 's/d' . $model->splTemplate->pulang . '(' . $model->splTemplate->kategoriSingkatan . ')';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(SplTemplate::find()->all(), 'id', 'namaKomplit'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Spl template', 'id' => 'grid-spl-search-spl_template_id']
            ],
            [
                'attribute' => 'jumlah_jam',
                'value' => function (Spl $model) {
                    return $model->splTemplate->jumlahKonversi;
                }
            ],            
            [
                'attribute' => 'is_planing',
                'value' => function (Spl $model) {
                    return Status::activeInactive($model->is_planing);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Status::activeInactive(0, true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Rencana')]
            ],
            [
                'attribute' => 'klaim_quaker',
                'value' => function (Spl $model) {
                    return Status::activeInactive($model->klaim_quaker);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Status::activeInactive(0, true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Klaim')]
            ],
            [
                'attribute' => 'last_status',
                'value' => function (Spl $model) {
                    return Status::statuses($model->last_status);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Status::statuses(0, true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status')]
            ],
            [
                'attribute' => 'spl_alasan_id',
                'value' => function (Spl $model) {
                    return $model->splAlasan ? $model->splAlasan->nama : '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(SplAlasan::find()->orderBy('nama')->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Alasan')]
            ],
            [
                'attribute' => 'created_by',
                'value' => function (Spl $model) {
                    return $model->createdBy->userPegawai->pegawai->nama;
                },
            ],
            [
                'label' => Yii::t('frontend', 'Created At'),
                'value' => function (Spl $model) {
                    return date('d-m-Y H:m', strtotime($model->created_at));
                },
            ],
            [
                'attribute' => 'keterangan',
                'contentOptions' => ['style' => 'width: 200px;'],
            ],
            ['attribute' => 'lock', 'hidden' => true],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{/spl/spl/print}',
                'buttons' => [
                    '/spl/spl/print' => function ($url) {
                        return Html::a(Yii::t('frontend', 'Print'), $url, ['class' => 'btn btn-xs btn-success btn-flat', 'target' => '_blank']);
                    },
                ],
                'contentOptions' => ['style' => 'width: 200px;'],
            ],
        ];
        ?>
        <?php try {
            $export = ExportMenu::widget([
                'id' => 'exportmenu-spl',
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                'noExportColumns' => [1, 15, 16], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'spl' . date('dmYHis')
            ]);
            echo GridView::widget([
                'id' => 'grid-spl-acc',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'options' => ['style' => 'table-layout:fixed;'],
                'pjax' => false,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl'], 'enablePushState' => false,],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/spl/spl/create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'SPL(rencana)'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/spl/spl/index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
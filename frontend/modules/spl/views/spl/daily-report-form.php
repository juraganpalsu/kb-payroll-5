<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 05/02/2019
 * Time: 21:16
 */

use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Spl */
/* @var $form yii\widgets\ActiveForm */
/** @var $splTemplate array */


$js = <<< JS
$(function() {
    $('#daily-report-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        var form = $(this);
        var btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                console.log(dt);
                var win = window.open(dt.data.url, '_blank');
                win.focus();
                location.reload();
            }else {
                location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

?>
<div class="daily-report-form">

    <?php $form = ActiveForm::begin([
        'id' => 'daily-report-form',
        'action' => Url::to(['daily-report-form']),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['daily-report-form-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'date')->widget(DateControl::class, [
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                    ],
                ]
            ]); ?>
        </div>
    </div>

    <hr>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Generate'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

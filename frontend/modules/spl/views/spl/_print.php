<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 30/12/2018
 * Time: 21:44
 *
 * @var Spl $model
 */

use common\components\Status;
use frontend\models\Spl;
use yii\helpers\Html;

$header = $this->renderAjax('_header-print', ['model' => $model]);
$height = 4;
$row = 1;
$rowHeader = 6;
$maxLengthName = 30;
$totalHeight = 0;
$countPeriod = 1;
$maxRow = 60;

$userApproval = [];
foreach ($model->splApprovals as $approval) {
    if ($approval->tipe == Status::APPROVE) {
        $userApproval[$approval->level] = ['nama' => $approval->pegawai->nama, 'tanggal' => date('d-m-Y', strtotime($approval->tanggal)), 'struktur' => $approval->pegawaiStruktur->struktur->name];
    }
}
$userApproval = array_reverse($userApproval);
$jumlahUserApproval = count($userApproval);
$jumlahKolomApproval = $jumlahUserApproval ? floor(12 / $jumlahUserApproval) : 12;

echo $header;

$splDetails = $model->splDetails;
if (in_array($model->last_status, [Status::APPROVED, Status::REJECTED])) {
    $splDetails = $model->splDetailsApproved;
}

foreach ($splDetails as $detail) {
    ?>
    <tr>
        <td colspan="2"
            style="text-align: center; border: 1px dotted; border-left: 1px solid"><?= $detail->perjanjianKerja->id_pegawai; ?></td>
        <td colspan="6" style="border: 1px dotted"><?= $detail->pegawai->nama; ?></td>
        <td style="text-align: center; border: 1px dotted"><?php echo date('H:i', strtotime($detail->spl->splTemplate->masuk)); ?></td>
        <td style="text-align: center; border: 1px dotted"><?php
            echo date('H:i', strtotime($detail->spl->splTemplate->pulang)); ?></td>
        <td style="text-align: center; border: 1px dotted"><?= $model->kategori == Spl::LEMBUR_INSENTIF ? $detail->spl->splTemplate->jumlah : $detail->spl->splTemplate->jumlahKonversi; ?></td>
        <td style="border: 1px dotted"></td>
        <td style="border: 1px dotted"></td>
        <td style="border: 1px dotted"></td>
        <td style="border: 1px dotted"></td>
        <td style="border: 1px dotted"></td>
        <?php if ($model->kategori == Spl::LEMBUR_BIASA) { ?>
            <td colspan="4" style="border: 1px dotted"><?= $model->splAlasan->nama; ?></td>
            <td colspan="4" style="border: 1px dotted; border-right: 1px solid"><?= $model->keterangan; ?></td>
        <?php } ?>
        <?php if ($model->kategori == Spl::LEMBUR_INSENTIF) { ?>
            <td colspan="8" style="border: 1px dotted; border-right: 1px solid"><?= $model->keterangan; ?></td>
        <?php } ?>
        <!-- <td colspan="4"
            style="border: 1px dotted; border-right: 1px solid"><?= $detail->tipe_approval == Status::APPROVED ? $model->splTemplate->istirahat > 0 ? Yii::t('frontend', 'Istirahat') . $model->splTemplate->istirahat . Yii::t('frontend', 'Jam') : Yii::t('frontend', 'Tanpa istirahat') : $detail->keterangan_approval; ?></td> -->
    </tr>
<?php } ?>

<tr>
    <td colspan="4" style="border-top: 1px solid; text-align: center;"
        class="border-left"><?= Yii::t('frontend', 'Dibuat Oleh') ?>,
    </td>
    <td colspan="2" style="border-top: 1px solid; text-align: center; border-left: 1px solid;"></td>
    <td colspan="12" style="border-top: 1px solid; text-align: center;"><?= Yii::t('frontend', 'Disetujui Oleh') ?>,
    </td>
    <td colspan="3"
        style="border-top: 1px solid; text-align: center;">
    </td>
    <td colspan="3"
        style="border-top: 1px solid; text-align: center; border-right: 1px solid;"><?= Yii::t('frontend', 'Diperiksa Oleh') ?>
    </td>
</tr>
<tr>
    <td colspan="4" style="text-align: center; border-bottom: 1px solid;" class="border-left border-right">
    <br><br><br>
    (<?= date('d-m-Y', strtotime($model->created_at)) ?>)<br>
        (<?= $model->createdByPk->pegawai->nama ?>)<br>
        (<?= $model->createdByStruktur->name; ?>)
    </td>
    <td colspan="2" style="text-align: center; border-left: 1px solid; border-bottom: 1px solid;"></td>
    <?php
    if ($jumlahUserApproval > 0) {
        foreach ($userApproval as $approval) { ?>
            <td colspan="<?= $jumlahKolomApproval; ?>"
                style="text-align: center; border-bottom: 1px solid;" class="center">
                <?= Html::img('@frontend/web/images/approved_100x40.png') ?>
                <br>
                (<?= $approval['tanggal'] ?>)<br>
                (<?= $approval['nama'] ?>)<br>
                (<?= $approval['struktur']; ?>)
            </td>
            <?php
        }
    } else {
        ?>
        <td colspan="12" style="text-align: center; border-bottom: 1px solid;"></td>
        <?php
    }
    ?>
    <td colspan="3" style="text-align: center; border-bottom: 1px solid;">
        <br><br><br>
        (<?= Yii::t('frontend', 'BOD') ?>)
    </td>
    <td colspan="3" style="text-align: center; border-right: 1px solid; border-bottom: 1px solid;">
        <br><br><br>
        (<?= Yii::t('frontend', 'HC Dept.') ?>)
    </td>
</tr>
<tr>
    <td colspan="12" style="text-align: left"><?= date('d-m-Y H:i:s'); ?></td>
    <td colspan="12" style="text-align: right"><?= $model->seq_code ?></td>
</tr>
</table>

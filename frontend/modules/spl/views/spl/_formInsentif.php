<?php
/**
 * Created by PhpStorm.
 * File Name: _formInsentif.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 23/08/2020
 * Time: 13:35
 */

use frontend\modules\spl\models\SplAlasan;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Spl */
/* @var $form yii\widgets\ActiveForm */
/** @var $splTemplate array */

$js = <<< JS
$(function() {
    $('#spl-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                console.log(dt);
                location.href = dt.data.url;
            }else {
                location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

?>
<small><a href="https://bit.ly/InsentifPDF" target="_blank" style="color:red; font-weight:bold;">Ketentuan & Tutorial
        Pembuatan SPKL</a></small>
<div class="spl-form">

    <?php $form = ActiveForm::begin([
        'id' => 'spl-form',
        'action' => Url::to('create-insentif'),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation', 'isPlaning' => $model->is_planing, 'scenario' => 'insentif']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php
    try {
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'tanggal')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'spl_template_id')->widget(Select2::class, [
                    'data' => $splTemplate,
                    'options' => ['placeholder' => Yii::t('app', 'Choose Spl template')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'spl_alasan_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(SplAlasan::find()->andWhere(['like','nama','Insentif'])->orderBy('nama')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => $model->getAttributeLabel('spl_alasan_id')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true, 'placeholder' => 'Keterangan']) ?>

            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

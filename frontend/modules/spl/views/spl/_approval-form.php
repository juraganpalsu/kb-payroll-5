<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 03/01/2019
 * Time: 5:23
 */

use common\components\Status;
use common\models\Spl;
use common\models\SplDetail;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $model Spl
 * @var $this View
 */

$url = Yii::$app->request->url;

$js = <<<JS

    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        let idModal = $('#form-modal');
                        idModal.find('.modal-title').text($(this).data('header'));
                        idModal.modal('show')
                           .find('#modelContent')
                           .load(dt.data.url);
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });

    $('.approve-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
            btn.button('loading');
            $.post(btn.attr('href'))
            .done(function (dt) {
                btn.button('reset');
                console.log(dt);
                if(dt.status === true){
                    let idModal = $('#form-modal');
                    idModal.find('.modal-title').text($(this).data('header'));
                    idModal.modal('show')
                       .find('#modelContent')
                       .load('$url');
                    return false;
                }
            }); 
        return false;
    });

    $('.reject-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        btn.button('loading');
        krajeeDialog.prompt({label:'Berikan alasan', placeholder:'Sampai 225 karakter...', maxlength: 225, type: 'text'}, function (result) {
            if (result === ''){
                result = '-';
            } 
            if (result) {
                $.post(btn.attr('href'), {'keterangan' : result})
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        let idModal = $('#form-modal');
                        idModal.find('.modal-title').text($(this).data('header'));
                        idModal.modal('show')
                           .find('#modelContent')
                           .load('$url');
                    return false;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.approve-all-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
            btn.button('loading');
            $.post(btn.attr('href'))
            .done(function (dt) {
                btn.button('reset');
                console.log(dt);
                if(dt.status === true){
                    let idModal = $('#form-modal');
                        idModal.find('.modal-title').text($(this).data('header'));
                        idModal.modal('show')
                           .find('#modelContent')
                           .load('$url');
                }
                return false;
            }); 
        return false;
    });
    
    $('.reject-all-btn').click(function(e) {
       e.preventDefault();
        let btn = $(this);
        btn.button('loading');
        krajeeDialog.prompt({label:'Berikan alasan', placeholder:'Sampai 255 karakter...', maxlength: 255, type: 'text'}, function (result) {
            if (result === ''){
                result = '-';
            } 
            if (result) {
                $.post(btn.attr('href'), {'keterangan' : result})
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        let idModal = $('#form-modal');
                        idModal.find('.modal-title').text($(this).data('header'));
                        idModal.modal('show')
                           .find('#modelContent')
                           .load('$url');
                    }
                    return false;
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
JS;

$this->registerJs($js);


$css = <<<CSS
.alert {
    overflow-y: scroll;
    max-height: 400px;
}
CSS;

$this->registerCss($css);

$actionApproval = [
    'class' => 'kartik\grid\ActionColumn',
    'template' => '{delete}',
    'width' => '10%',
    'buttons' => [
        'delete' => function ($url, SplDetail $model) {
            if ($model->last_status == Status::OPEN) {
                return Html::a(Yii::t('frontend', 'Delete'), ['delete-detail', 'id' => $model->id], ['class' => 'btn btn-danger btn-xs btn-flat btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan menghapus pegawai ini?')]]);
            }
            return '';
        },
    ],
];;

if ($model->checkStatusApprovalByUser()) {
    $class = $model->checkStatusApprovals() ? 'btn-warning' : 'btn-success';
    echo Html::a(Yii::t('frontend', 'Approve All'), ['approve-all', 'id' => $model->id], ['class' => 'btn ' . $class . ' btn-xs approve-all-btn btn-flat']);
    echo Html::a(Yii::t('frontend', 'Reject All'), ['reject-all', 'id' => $model->id], ['class' => 'btn btn-danger btn-xs reject-all-btn btn-flat']);

    $actionApproval = [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{approve} {reject}',
        'width' => '10%',
        'buttons' => [
            'approve' => function ($url, SplDetail $model) {
                $class = $model->checkStatusApprovals() ? 'btn-warning' : 'btn-success';
                if ($model->approvalByUser->tipe == Status::DEFLT) {
                    return Html::a(Yii::t('frontend', 'aprove'), ['approve', 'id' => $model->id], ['class' => 'btn ' . $class . ' btn-xs approve-btn btn-flat', 'data' => ['header' => Yii::t('frontend', 'Approval SPL')]]);
                }
                return '';
            },
            'reject' => function ($url, SplDetail $model) {
                if ($model->approvalByUser->tipe == Status::DEFLT) {
                    return Html::a(Yii::t('frontend', 'reject'), ['reject', 'id' => $model->id], ['class' => 'btn btn-danger btn-xs reject-btn btn-flat', 'data' => ['header' => Yii::t('frontend', 'Approval SPL')]]);
                }
                return '';
            }
        ],
    ];
}

?>

    <div class="row">
        <div class="col-md-12">
            <?php
            /** @var ActiveDataProvider $providerSplDetail */
            if ($providerSplDetail->totalCount) {
                $gridColumnSplDetail = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'hidden' => true],
                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'width' => '50px',
                        'value' => function () {
                            return GridView::ROW_COLLAPSED;
                        },
                        'enableRowClick' => true,
                        'detail' => function (SplDetail $model) {
                            $modelApprovals = $model->splDetailApprovals;
                            return Yii::$app->controller->renderPartial('_user-approval-spl-detail', ['model' => $model, 'modelApprovals' => $modelApprovals,]);
                        },
                        'headerOptions' => ['class' => 'kartik-sheet-style'],
                        'expandOneOnly' => true,
                    ],
                    [
                        'attribute' => 'pegawai_id',
                        'value' => function (SplDetail $model) {
                            return $model->perjanjianKerja->id_pegawai . '-' . $model->pegawai->nama . '[' . $model->pegawaiStruktur->struktur->nameCostume . ']';
                        },
                        'label' => Yii::t('app', 'Pegawai')
                    ],
                    [
                        'attribute' => 'last_status',
                        'value' => function (SplDetail $model) {
                            return Status::statuses($model->last_status);
                        },
                        'label' => Yii::t('frontend', 'Status')
                    ],
                    $actionApproval,
                    ['attribute' => 'lock', 'hidden' => true],
                ];
                try {
                    echo GridView::widget([
                        'id' => 'grid-spl-approval',
                        'dataProvider' => $providerSplDetail,
                        'rowOptions' => function (SplDetail $model) {
                            return ['data' => ['key' => $model->id], 'id' => 'grid-spl-approval-row-' . $model->id];
                        },
                        'pjax' => false,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl-detail']],
                        'columns' => $gridColumnSplDetail,
                        'export' => [
                            'label' => 'Page',
                            'fontAwesome' => true,
                        ],
                        'beforeHeader' => [
                            [
                                'columns' => [
                                    [
                                        'content' => Yii::t('frontend', 'Daftar Pegawai'),
                                        'options' => [
                                            'colspan' => 5,
                                            'class' => 'text-center warning',
                                        ]
                                    ],
                                ],
                                'options' => ['class' => 'skip-export']
                            ]
                        ],
                        'bordered' => true,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'hover' => true,
                        'showPageSummary' => false,
                        'persistResize' => false,
                        'tableOptions' => ['class' => 'small']
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
            ?>
        </div>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
<?php

namespace frontend\modules\queue\models;

use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\queue\models\base\AntrianProsesAbsensi as BaseAntrianProsesAbsensi;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "antrian_proses_absensi".
 *
 * @property array $_status
 */
class AntrianProsesAbsensi extends BaseAntrianProsesAbsensi
{

    const DIBUAT = 1;
    const DIPROSES = 2;
    const SELESAI = 3;

    public $_status = [self::DIBUAT => 'DIBUAT', self::DIPROSES => 'DIPROSES', self::SELESAI => 'SELESAI'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_awal', 'tanggal_akhir', 'golongan_id'], 'required'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at'], 'safe'],
            [['bisnis_unit', 'status', 'created_by'], 'integer'],
            [['id', 'golongan_id'], 'string', 'max' => 32],
            [['status'], 'default', 'value' => 1]
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search(): ActiveDataProvider
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->orderBy('created_at DESC');
        return $dataProvider;
    }

    /**
     * @return array
     */
    public static function getBisnisUnit()
    {
        $modelPerjanjianKerja = new PerjanjianKerja();
        return $modelPerjanjianKerja->_kontrak;
    }

}

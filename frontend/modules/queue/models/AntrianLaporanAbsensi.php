<?php

namespace frontend\modules\queue\models;

use frontend\modules\queue\models\base\AntrianLaporanAbsensi as BaseAntrianLaporanAbsensi;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "antrian_laporan_absensi".
 *
 * @property array $_jenisLaporan
 */
class AntrianLaporanAbsensi extends BaseAntrianLaporanAbsensi
{

    const filePathLaporan = '/web/datas/absensi/';

    const laporan = 1;
    const rekap = 2;

    public $_jenisLaporan = [self::laporan => 'Laporan', self::rekap => 'Rekap'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_awal', 'tanggal_akhir', 'golongan_id', 'jenis_laporan'], 'required'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at'], 'safe'],
            [['bisnis_unit', 'jenis_perjanjian', 'status_kehadiran', 'status', 'jenis_laporan', 'created_by'], 'integer'],
            [['id', 'golongan_id'], 'string', 'max' => 32],
            [['status'], 'default', 'value' => 1]
        ];
    }


    /**
     * @return ActiveDataProvider
     */
    public function search(): ActiveDataProvider
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->orderBy('created_at DESC');
        return $dataProvider;
    }

    /**
     * @param int $status
     * @return mixed
     */
    public static function _status(int $status)
    {
        $model = new AntrianProsesAbsensi();
        return $model->_status[$status];
    }


}

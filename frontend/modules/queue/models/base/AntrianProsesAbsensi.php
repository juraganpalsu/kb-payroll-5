<?php

namespace frontend\modules\queue\models\base;

use common\models\Golongan;
use frontend\modules\queue\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "antrian_proses_absensi".
 *
 * @property string $id
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property string $golongan_id
 * @property integer $bisnis_unit
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 *
 * @property \frontend\models\User $createdBy
 * @property Golongan $golongan
 */
class AntrianProsesAbsensi extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_awal', 'tanggal_akhir', 'golongan_id'], 'required'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at'], 'safe'],
            [['bisnis_unit', 'status', 'created_by'], 'integer'],
            [['id', 'golongan_id'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'antrian_proses_absensi';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tanggal_awal' => Yii::t('frontend', 'Tanggal Awal'),
            'tanggal_akhir' => Yii::t('frontend', 'Tanggal Akhir'),
            'golongan_id' => Yii::t('frontend', 'Golongan ID'),
            'bisnis_unit' => Yii::t('frontend', 'Bisnis Unit'),
            'status' => Yii::t('frontend', 'Status'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\frontend\models\User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGolongan()
    {
        return $this->hasOne(Golongan::class, ['id' => 'golongan_id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
        ];
    }
}

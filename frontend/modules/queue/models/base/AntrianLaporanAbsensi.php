<?php

namespace frontend\modules\queue\models\base;

use common\models\Golongan;
use frontend\models\User;
use frontend\modules\pegawai\models\PerjanjianKerjaJenis;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the base model class for table "antrian_laporan_absensi".
 *
 * @property string $id
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property string $golongan_id
 * @property integer $bisnis_unit
 * @property integer $jenis_perjanjian
 * @property integer $status_kehadiran
 * @property integer $status
 * @property integer $jenis_laporan
 * @property string $created_at
 * @property integer $created_by
 *
 * @property User $createdBy
 * @property Golongan $golongan
 * @property PerjanjianKerjaJenis $jenisPerjanjian
 */
class AntrianLaporanAbsensi extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_awal', 'tanggal_akhir', 'golongan_id'], 'required'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at'], 'safe'],
            [['bisnis_unit', 'jenis_perjanjian', 'status_kehadiran', 'status', 'created_by'], 'integer'],
            [['id', 'golongan_id'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'antrian_laporan_absensi';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tanggal_awal' => Yii::t('frontend', 'Tanggal Awal'),
            'tanggal_akhir' => Yii::t('frontend', 'Tanggal Akhir'),
            'golongan_id' => Yii::t('frontend', 'Golongan ID'),
            'bisnis_unit' => Yii::t('frontend', 'Bisnis Unit'),
            'jenis_perjanjian' => Yii::t('frontend', 'Jenis Perjanjian'),
            'status_kehadiran' => Yii::t('frontend', 'Status Kehadiran'),
            'status' => Yii::t('frontend', 'Status'),
            'jenis_laporan' => Yii::t('frontend', 'Jenis Laporan'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGolongan()
    {
        return $this->hasOne(Golongan::class, ['id' => 'golongan_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getJenisPerjanjian()
    {
        return $this->hasOne(PerjanjianKerjaJenis::class, ['id' => 'jenis_perjanjian']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
        ];
    }
}

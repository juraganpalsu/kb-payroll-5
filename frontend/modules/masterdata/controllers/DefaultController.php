<?php

namespace frontend\modules\masterdata\controllers;

use frontend\models\search\PegawaiSearch;
use frontend\models\User;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\masterdata\models\PAdminBank;
use frontend\modules\masterdata\models\PBpjsTkSetting;
use frontend\modules\masterdata\models\PGajiPokok;
use frontend\modules\masterdata\models\PInsentifTetap;
use frontend\modules\masterdata\models\PPremiHadir;
use frontend\modules\masterdata\models\PSewaMobil;
use frontend\modules\masterdata\models\PSewaMotor;
use frontend\modules\masterdata\models\PSewaRumah;
use frontend\modules\masterdata\models\PTunjaganLainLain;
use frontend\modules\masterdata\models\PTunjaganLainTetap;
use frontend\modules\masterdata\models\PTunjanganJabatan;
use frontend\modules\masterdata\models\PTunjanganKost;
use frontend\modules\masterdata\models\PTunjanganPulsa;
use frontend\modules\masterdata\models\PUangMakan;
use frontend\modules\masterdata\models\PUangTransport;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * Default controller for the `masterdata` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
//    public function actionIndex()
//    {
//        return $this->render('index');
//    }

    public function actionIndexComponent()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
        $modelPerjanjiankerja = new PerjanjianKerja();
        $modelStruktur = new PegawaiStruktur();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
            'modelStruktur' => $modelStruktur,

        ]);
    }

    /**
     * Finds the Pegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Pegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelPegawai($id)
    {
        if (($model = Pegawai::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return MasterDataDefault the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id)
    {
        if (($model = MasterDataDefault::findOne(['pegawai_id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);
        $model = MasterDataDefault::findOne(['pegawai_id' => $id]);
        $model = $model ?? new MasterDataDefault();

        $queryGajiPokok = PGajiPokok::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerGajiPokok = new ActiveDataProvider([
            'query' => $queryGajiPokok
        ]);

        $queryPremiHadir = PPremiHadir::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerPremiHadir = new ActiveDataProvider([
            'query' => $queryPremiHadir
        ]);

        $queryUangMakan = PUangMakan::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerUangMakan = new ActiveDataProvider([
            'query' => $queryUangMakan
        ]);

        $queryUangTransport = PUangTransport::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerUangTransport = new ActiveDataProvider([
            'query' => $queryUangTransport
        ]);

        $queryTunjJabatan = PTunjanganJabatan::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerTunjJabatan = new ActiveDataProvider([
            'query' => $queryTunjJabatan
        ]);

        $queryInsentifTetap = PInsentifTetap::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerInsentifTetap = new ActiveDataProvider([
            'query' => $queryInsentifTetap
        ]);

        $queryTunjPulsa = PTunjanganPulsa::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerTunjPulsa = new ActiveDataProvider([
            'query' => $queryTunjPulsa
        ]);

        $querySewaMobil = PSewaMobil::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerSewaMobil = new ActiveDataProvider([
            'query' => $querySewaMobil
        ]);

        $querySewaMotor = PSewaMotor::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerSewaMotor = new ActiveDataProvider([
            'query' => $querySewaMotor
        ]);

        $querySewaRumah = PSewaRumah::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerSewaRumah = new ActiveDataProvider([
            'query' => $querySewaRumah
        ]);

        $queryLainTetap = PTunjaganLainTetap::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerLainTetap = new ActiveDataProvider([
            'query' => $queryLainTetap
        ]);

        $queryTunjKos = PTunjanganKost::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerTunjKos = new ActiveDataProvider([
            'query' => $queryTunjKos
        ]);

        $queryAdminBank = PAdminBank::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerAdminBank = new ActiveDataProvider([
            'query' => $queryAdminBank
        ]);

        $queryTunjLainLain = PTunjaganLainLain::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerTunjLainLain = new ActiveDataProvider([
            'query' => $queryTunjLainLain
        ]);


        $queryBpjsTkSetting = PBpjsTkSetting::find()->andWhere(['pegawai_id' => $modelPegawai->id])->orderBy('tanggal_mulai DESC');
        $providerBpjsTkSetting = new ActiveDataProvider([
            'query' => $queryBpjsTkSetting
        ]);


        $pegawaiGolongan = $modelPegawai->pegawaiGolongan;
        if (empty($pegawaiGolongan)) {
            $pegawaiGolongan = $modelPegawai->getGolonganTerakhir();
        }

        $bolehAkses = false;
        $user = User::pegawai();
        if (is_object($user)) {
            if ($userGologan = $user->pegawaiGolongan) {
                if ($userGologan->golongan->urutan > $pegawaiGolongan->golongan->urutan) {
                    $bolehAkses = true;
                }
            }
        }

        if (!$bolehAkses) {
            return $this->render('perhatian');
        }

        return $this->render('view', [
            'model' => $model,
            'modelPegawai' => $modelPegawai,
            'providerGajiPokok' => $providerGajiPokok,
            'providerPremiHadir' => $providerPremiHadir,
            'providerUangMakan' => $providerUangMakan,
            'providerUangTransport' => $providerUangTransport,
            'providerTunjJabatan' => $providerTunjJabatan,
            'providerInsentifTetap' => $providerInsentifTetap,
            'providerTunjPulsa' => $providerTunjPulsa,
            'providerSewaMobil' => $providerSewaMobil,
            'providerSewaMotor' => $providerSewaMotor,
            'providerSewaRumah' => $providerSewaRumah,
            'providerLainTetap' => $providerLainTetap,
            'providerTunjKos' => $providerTunjKos,
            'providerAdminBank' => $providerAdminBank,
            'providerTunjLainLain' => $providerTunjLainLain,
            'providerBpjsTkSetting' => $providerBpjsTkSetting,
        ]);
    }

    /**
     * @return array|mixed
     */
    public function actionMasterDataCreate()
    {
        $model = new MasterDataDefault();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            if ($model->load(Yii::$app->request->post()) & $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['/masterdata/default/view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
        }
        return $response->data;
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionMasterDataUpdate(string $id)
    {
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            if ($model->load(Yii::$app->request->post()) & $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['/masterdata/default/view', 'id' => $id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
        }
        return $response->data;
    }

    /**
     * @return mixed
     */
    public function actionMasterDataValidation()
    {
        $model = new MasterDataDefault();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }


    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateGajiPokok(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PGajiPokok();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-gaji-pokok', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateGajiPokok(string $id)
    {
        $model = PGajiPokok::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-gaji-pokok', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionFormGajiPokokValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PGajiPokok();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PGajiPokok::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteGajiPokok(string $id)
    {
        $model = PGajiPokok::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //premi-hadir

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreatePremiHadir(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PPremiHadir();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-premi-hadir', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdatePremiHadir(string $id)
    {
        $model = PPremiHadir::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-premi-hadir', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormPremiHadirValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PPremiHadir();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PPremiHadir::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeletePremiHadir(string $id)
    {
        $model = PPremiHadir::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }
    //uang-makan

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateUangMakan(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PUangMakan();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-uang-makan', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateUangMakan(string $id)
    {
        $model = PUangMakan::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-uang-makan', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormUangMakanValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PPremiHadir();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PUangMakan::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteUangMakan(string $id)
    {
        $model = PUangMakan::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }


    //uang-transport

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateUangTransport(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PUangTransport();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-uang-transport', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateUangTransport(string $id)
    {
        $model = PUangTransport::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-uang-transport', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormUangTransportValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PUangTransport();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PUangTransport::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteUangTransport(string $id)
    {
        $model = PUangTransport::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //insentif-tetap

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateInsentifTetap(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PInsentifTetap();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-insentif-tetap', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateInsentifTetap(string $id)
    {
        $model = PInsentifTetap::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-insentif-tetap', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormInsentifTetapValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PInsentifTetap();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PInsentifTetap::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteInsentifTetap(string $id)
    {
        $model = PInsentifTetap::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }


    //tunjangan-jabatan

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateTunjJabatan(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PTunjanganJabatan();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-tunj-jabatan', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateTunjJabatan(string $id)
    {
        $model = PTunjanganJabatan::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-tunj-jabatan', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormTunjJabatanValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PTunjanganJabatan();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PTunjanganJabatan::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteTunjJabatan(string $id)
    {
        $model = PTunjanganJabatan::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //tunjangan-pulsa

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateTunjPulsa(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PTunjanganPulsa();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-tunj-pulsa', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateTunjPulsa(string $id)
    {
        $model = PTunjanganPulsa::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-tunj-pulsa', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormTunjPulsaValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PTunjanganPulsa();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PTunjanganPulsa::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteTunjPulsa(string $id)
    {
        $model = PTunjanganPulsa::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //sewa-mobil

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateSewaMobil(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PSewaMobil();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-sewa-mobil', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateSewaMobil(string $id)
    {
        $model = PSewaMobil::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-sewa-mobil', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormSewaMobilValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PSewaMobil();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PSewaMobil::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteSewaMobil(string $id)
    {
        $model = PSewaMobil::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //sewa-motor

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateSewaMotor(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PSewaMotor();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-sewa-motor', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */
    public function actionUpdateSewaMotor(string $id)
    {
        $model = PSewaMotor::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-sewa-motor', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormSewaMotorValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PSewaMotor();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PSewaMotor::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteSewaMotor(string $id)
    {
        $model = PSewaMotor::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //sewa-rumah

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateSewaRumah(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PSewaRumah();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-sewa-rumah', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */

    public function actionUpdateSewaRumah(string $id)
    {
        $model = PSewaRumah::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-sewa-rumah', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormSewaRumahValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PSewaRumah();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PSewaRumah::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteSewaRumah(string $id)
    {
        $model = PSewaRumah::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }


    //tunj-lain-tetap

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateLainTetap(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PTunjaganLainTetap();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-lain-tetap', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */

    public function actionUpdateLainTetap(string $id)
    {
        $model = PTunjaganLainTetap::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-lain-tetap', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormLainTetapValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PTunjaganLainTetap();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PTunjaganLainTetap::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteLainTetap(string $id)
    {
        $model = PTunjaganLainTetap::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //tunj-kost

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateTunjKost(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PTunjanganKost();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-tunj-kost', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */

    public function actionUpdateTunjKost(string $id)
    {
        $model = PTunjanganKost::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-tunj-kost', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormTunjKostValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PTunjanganKost();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PTunjanganKost::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteTunjKost(string $id)
    {
        $model = PTunjanganKost::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //admin-bank

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateAdminBank(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PAdminBank();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-admin-bank', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */

    public function actionUpdateAdminBank(string $id)
    {
        $model = PAdminBank::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-admin-bank', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormAdminBankValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PAdminBank();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PAdminBank::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteAdminBank(string $id)
    {
        $model = PAdminBank::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

    //tunj-lain-lain

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateLainLain(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PTunjaganLainLain();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-lain-lain', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */

    public function actionUpdateLainLain(string $id)
    {
        $model = PTunjaganLainLain::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-lain-lain', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormLainLainValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PTunjaganLainLain();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PTunjaganLainLain::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteLainLain(string $id)
    {
        $model = PTunjaganLainLain::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }


    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionCreateBpjsTkSetting(string $id)
    {
        $modelPegawai = $this->findModelPegawai($id);

        $model = new PBpjsTkSetting();
        $model->pegawai_id = $modelPegawai->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-bpjs-tk-setting', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     */

    public function actionUpdateBpjsTkSetting(string $id)
    {
        $model = PBpjsTkSetting::findOne($id);

        $modelPegawai = $model->pegawai;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pegawai_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-bpjs-tk-setting', ['model' => $model, 'modelPegawai' => $modelPegawai]);
    }


    /**
     * @param string $id
     * @param bool $isnew
     * @return array
     */
    public function actionFormBpjsTkSettingValidation(string $id, bool $isnew = true)
    {
        if ($isnew) {
            $model = new PBpjsTkSetting();
            $model->setIsNewRecord($isnew);
            $model->pegawai_id = $id;
        } else {
            $model = PBpjsTkSetting::findOne($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteBpjsTkSetting(string $id)
    {
        $model = PBpjsTkSetting::findOne($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->pegawai_id]);
    }

}

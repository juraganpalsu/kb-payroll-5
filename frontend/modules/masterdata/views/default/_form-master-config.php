<?php

/**
 * Created by PhpStorm.
 * File Name: _form-master-config.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 09/04/20
 * Time: 10.36
 */


use frontend\modules\masterdata\models\MasterDataConfig;
use frontend\modules\pegawai\models\Pegawai;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model MasterDataConfig */
/**
 * @var $modelPegawai Pegawai
 */


$this->title = Yii::t('frontend', 'Master Data Config');
$this->params['breadcrumbs'][] = $this->title;


$js = <<< JS
$(function() {
    $('#form-master-config').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
        
});
JS;

$this->registerJs($js);

$action = Url::to(['master-config']);
$validation = Url::to(['form-master-config-validation']);
?>

<div class="gaji-pokok-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-master-config',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-2">
                <?php
                echo $form->field($model, 'tunjangan_anak')->textInput();
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <?php
                echo $form->field($model, 'tunjangan_loyalitas')->textInput();
                ?>
            </div>
        </div>

        <?php
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>
        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }

    ?>

    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

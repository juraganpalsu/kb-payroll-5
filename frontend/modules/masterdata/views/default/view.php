<?php

/**
 * Created by PhpStorm.
 * File Name: view.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 29/03/20
 * Time: 11.46
 */

use common\components\Status;
use common\models\Golongan;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\masterdata\models\PAdminBank;
use frontend\modules\masterdata\models\PBpjsTkSetting;
use frontend\modules\masterdata\models\PGajiPokok;
use frontend\modules\masterdata\models\PInsentifTetap;
use frontend\modules\masterdata\models\PPremiHadir;
use frontend\modules\masterdata\models\PSewaMobil;
use frontend\modules\masterdata\models\PSewaMotor;
use frontend\modules\masterdata\models\PSewaRumah;
use frontend\modules\masterdata\models\PTunjaganLainLain;
use frontend\modules\masterdata\models\PTunjaganLainTetap;
use frontend\modules\masterdata\models\PTunjanganJabatan;
use frontend\modules\masterdata\models\PTunjanganKost;
use frontend\modules\masterdata\models\PTunjanganPulsa;
use frontend\modules\masterdata\models\PUangMakan;
use frontend\modules\masterdata\models\PUangTransport;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;


/**
 *
 * @var View $this
 * @var Pegawai $modelPegawai
 * @var MasterDataDefault $model
 * @var ActiveDataProvider $providerGajiPokok
 * @var ActiveDataProvider $providerPremiHadir
 * @var ActiveDataProvider $providerUangMakan
 * @var ActiveDataProvider $providerUangTransport
 * @var ActiveDataProvider $providerTunjJabatan
 * @var ActiveDataProvider $providerInsentifTetap
 * @var ActiveDataProvider $providerTunjPulsa
 * @var ActiveDataProvider $providerSewaMobil
 * @var ActiveDataProvider $providerSewaMotor
 * @var ActiveDataProvider $providerSewaRumah
 * @var ActiveDataProvider $providerLainTetap
 * @var ActiveDataProvider $providerTunjKos
 * @var ActiveDataProvider $providerAdminBank
 * @var ActiveDataProvider $providerTunjLainLain
 * @var ActiveDataProvider $providerBpjsTkSetting
 */


$this->title = Yii::t('frontend', 'Master Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Pegawai'), 'url' => ['/pegawai/pegawai/']];
$this->params['breadcrumbs'][] = $this->title;


$modelPerjanjianKerja = $modelPegawai->perjanjianKerja;
$modelStruktur = $modelPegawai->pegawaiStruktur;
$modelPerjanjianKerjaTerakhir = $modelPegawai->getPerjanjianKerjaTerakhir();
$modelGolonganTerakhir = $modelPegawai->getGolonganTerakhir();


$js = <<< JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('#master-data-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                console.log(dt);
                location.href = dt.data.url;
            }else {
                location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

?>
<section class="content small">

    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="box-profile-picture">
                        <?php
                        $fotoProfil = $modelPegawai->upload_dir . $modelPegawai->id . '.jpg';
                        if (file_exists($fotoProfil)) {
                            echo Html::img('@web/' . $modelPegawai->upload_dir . $modelPegawai->id . '.jpg', ['class' => 'profile-user-img img-responsive img-circle', 'alt' => Yii::t('frontend', 'User profile picture')]);
                        } else {
                            echo Html::img('@web/pictures/user8-128x128.jpg', ['class' => 'profile-user-img img-responsive img-circle', 'alt' => Yii::t('frontend', 'User profile picture')]);
                        } ?>
                    </div>
                    <h3 class="profile-username text-center"><?= $modelPegawai->nama ?></h3>

                    <p class="text-center text-bold text-danger"><?= $modelPerjanjianKerja ? $modelPerjanjianKerja->id_pegawai : ''; ?></p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b><?= Yii::t('frontend', 'Struktur'); ?></b> <a class="pull-right"><?php
                                if ($modelStruktur) {
                                    echo $modelStruktur->struktur->name;

                                } ?></a>
                            <p></p>
                        </li>

                        <li class="list-group-item">
                            <b><?= Yii::t('frontend', 'Perjanjian Kerja'); ?></b> <a class="pull-right"><?php
                                if ($modelPerjanjianKerja) {
                                    echo $modelPerjanjianKerja->jenis->nama;
                                }
                                ?></a>
                            <p></p>
                        </li>
                        <li class="list-group-item">
                            <b><?= Yii::t('frontend', 'Jabatan'); ?></b> <a class="pull-right"><?php
                                if ($modelStruktur) {
                                    if ($modelStruktur->struktur->jabatan) {
                                        echo $modelStruktur->struktur->jabatan->nama;
                                    }
                                }
                                ?></a>
                            <p></p>
                        </li>
                        <li class="list-group-item">
                            <b><?= Yii::t('frontend', 'Golongan'); ?></b> <a class="pull-right"><?php
                                if ($pegawaiGolongan = $modelPegawai->pegawaiGolongan) {
                                    echo $pegawaiGolongan->golongan->nama;
                                }
                                ?></a>
                            <p></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#default-setting-tab"
                                          data-toggle="tab"><?= Yii::t('frontend', 'Default Setting') ?></a></li>
                    <li><a href="#gaji-pokok-tab" data-toggle="tab"><?= Yii::t('frontend', 'Gaji Pokok') ?></a></li>
                    <li><a href="#premi-hadir-tab" data-toggle="tab"><?= Yii::t('frontend', 'Premi Hadir') ?></a></li>
                    <li><a href="#uang-makan-tab" data-toggle="tab"><?= Yii::t('frontend', 'Uang Makan') ?></a></li>
                    <li><a href="#uang-transport-tab" data-toggle="tab"><?= Yii::t('frontend', 'Uang Transport') ?></a>
                    </li>
                    <li><a href="#t-jabatan-tab" data-toggle="tab"><?= Yii::t('frontend', 'Tunj. Jabatan') ?></a></li>
                    <li><a href="#t-insentif-tetap-tab" data-toggle="tab"><?= Yii::t('frontend', 'Insetif Tetap') ?></a>
                    </li>
                    <li><a href="#t-pulsa-tab" data-toggle="tab"><?= Yii::t('frontend', 'Tunj. Pulsa') ?></a></li>
                    <li><a href="#t-sewa-mobil-tab" data-toggle="tab"><?= Yii::t('frontend', 'Sewa Mobil') ?></a></li>
                    <li><a href="#t-sewa-motor-tab" data-toggle="tab"><?= Yii::t('frontend', 'Sewa Motor') ?></a></li>
                    <li><a href="#t-sewa-rumah-tab" data-toggle="tab"><?= Yii::t('frontend', 'Sewa Rumah') ?></a></li>
                    <li><a href="#t-lain-tetap-tab" data-toggle="tab"><?= Yii::t('frontend', 'Tunj. Lain Tetap') ?></a>
                    </li>
                    <li><a href="#t-kos-tab" data-toggle="tab"><?= Yii::t('frontend', 'Tunj. Kos') ?></a></li>
                    <li><a href="#t-admin-bank" data-toggle="tab"><?= Yii::t('frontend', 'Admin Bank') ?></a></li>
                    <li><a href="#t-lain-lain" data-toggle="tab"><?= Yii::t('frontend', 'Tunj Lain-lain') ?></a></li>
                    <li><a href="#bpjs-tk-setting" data-toggle="tab"><?= Yii::t('frontend', 'BPJS TK Setting') ?></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="default-setting-tab">

                        <?php $form = ActiveForm::begin([
                            'id' => 'master-data-form',
                            'action' => $model->isNewRecord ? Url::to('master-data-create') : Url::to(['master-data-update', 'id' => $modelPegawai->id]),
                            'type' => ActiveForm::TYPE_VERTICAL,
                            'formConfig' => ['showErrors' => false],
                            'enableAjaxValidation' => true,
                            'validationUrl' => Url::to(['master-data-validation']),
                            'fieldConfig' => ['showLabels' => true],
                        ]); ?>

                        <?= $form->errorSummary($model); ?>

                        <?php
                        try {
                            echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
                            ?>

                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field($model, 'tipe_gaji_pokok')->widget(Select2::class, [
                                        'data' => $model->jenisGaji,
                                        'options' => ['placeholder' => Yii::t('frontend', 'Tipe Gaji')],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <?php
                                if ($modelGolonganTerakhir->golongan->urutan >= Golongan::TIGA && $modelPerjanjianKerjaTerakhir->kontrak == PerjanjianKerja::KBU || $modelPerjanjianKerjaTerakhir->kontrak == PerjanjianKerja::ADA) {
                                    $model->tunjangan_anak = true;
                                    $model->tunjangan_loyalitas = true;
                                }
                                ?>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'tunjangan_anak')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'tunjangan_loyalitas')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'cabang')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'beatroute')->checkbox() ?>
                                </div>
                            </div>

                            <div class="row">
                                <?php
                                if ($model->isNewRecord) {
                                    $model->uang_lembur = true;
                                    $model->uang_makan_lembur = true;
                                    $model->uang_shift = true;
                                    $model->pot_keterlambatan = true;
                                }
                                //                                if ($modelGolonganTerakhir->golongan->urutan > 4) {
                                //                                    $model->insentif_lembur = true;
                                //                                    $model->uang_lembur = false;
                                //                                    $model->uang_makan_lembur = false;
                                //                                }

                                ?>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'pot_keterlambatan')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'uang_lembur')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'uang_makan_lembur')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'uang_shift')->checkbox() ?>
                                </div>
                            </div>

                            <div class="row">
                                <?php
                                if ($model->isNewRecord) {
                                    $model->insentif_tidak_tetap = true;
                                }
                                ?>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'insentif_tidak_tetap')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'insentif_lembur')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'prorate_komponen')->checkbox() ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <?php
                                    if ($model->isNewRecord) {
                                        $model->bonus = true;
                                        $model->thr = true;
                                    }
                                    ?>
                                    <?= $form->field($model, 'bonus')->checkbox() ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'thr')->checkbox() ?>
                                </div>
                            </div>

                            <?= $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput(['value' => $modelPegawai->id]); ?>
                            <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>

                        <?php } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        } ?>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>

                    <div class="tab-pane" id="gaji-pokok-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PGajiPokok $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PGajiPokok $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-gaji-pokok} {delete-gaji-pokok}',
                                'buttons' => [
                                    'update-gaji-pokok' => function ($url, PGajiPokok $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-gaji-pokok', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-gaji-pokok' => function ($url, PGajiPokok $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-gaji-pokok', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-gaji-pokok',
                                'dataProvider' => $providerGajiPokok,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-gaji-pokok']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Gaji Pokok')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Gaji'), ['create-gaji-pokok', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Gaji Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="premi-hadir-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PPremiHadir $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PPremiHadir $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-premi-hadir} {delete-premi-hadir}',
                                'buttons' => [
                                    'update-premi-hadir' => function ($url, PPremiHadir $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-premi-hadir', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-premi-hadir' => function ($url, PPremiHadir $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-premi-hadir', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-premi-hadir',
                                'dataProvider' => $providerPremiHadir,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-premi-hadir']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Premi Hadir')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Premi Hadir'), ['create-premi-hadir', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Premi Hadir Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="uang-makan-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PUangMakan $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PUangMakan $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-uang-makan} {delete-uang-makan}',
                                'buttons' => [
                                    'update-uang-makan' => function ($url, PUangMakan $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-uang-makan', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-uang-makan' => function ($url, PUangMakan $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-uang-makan', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-uang-makan',
                                'dataProvider' => $providerUangMakan,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-uang-makan']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Uang Makan')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Uang Makan'), ['create-uang-makan', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Uang Makan Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="uang-transport-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PUangTransport $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PUangTransport $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-uang-transport} {delete-uang-transport}',
                                'buttons' => [
                                    'update-uang-transport' => function ($url, PUangTransport $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-uang-transport', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-uang-transport' => function ($url, PUangTransport $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-uang-transport', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-uang-transport',
                                'dataProvider' => $providerUangTransport,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-uang-transport']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Uang Transport')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Uang Transport'), ['create-uang-transport', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Uang Transport Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-jabatan-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PTunjanganJabatan $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PTunjanganJabatan $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-tunj-jabatan} {delete-tunj-jabatan}',
                                'buttons' => [
                                    'update-tunj-jabatan' => function ($url, PTunjanganJabatan $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-tunj-jabatan', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-tunj-jabatan' => function ($url, PTunjanganJabatan $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-tunj-jabatan', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-tunj-jabatan',
                                'dataProvider' => $providerTunjJabatan,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-tunj-jabatan']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Tunjangan Jabatan')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Tunj Jabatan'), ['create-tunj-jabatan', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Tunj Jabatan Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-insentif-tetap-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PInsentifTetap $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PInsentifTetap $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-insentif-tetap} {delete-insentif-tetap}',
                                'buttons' => [
                                    'update-insentif-tetap' => function ($url, PInsentifTetap $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-insentif-tetap', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-insentif-tetap' => function ($url, PInsentifTetap $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-insentif-tetap', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-insentif-tetap',
                                'dataProvider' => $providerInsentifTetap,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-insentif-tetap']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Insentif Tetap')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Insentif Tetap'), ['create-insentif-tetap', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Insentif Tetap Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-pulsa-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PTunjanganPulsa $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PTunjanganPulsa $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-tunj-pulsa} {delete-tunj-pulsa}',
                                'buttons' => [
                                    'update-tunj-pulsa' => function ($url, PTunjanganPulsa $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-tunj-pulsa', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-tunj-pulsa' => function ($url, PTunjanganPulsa $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-tunj-pulsa', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-tunj-pulsa',
                                'dataProvider' => $providerTunjPulsa,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-tunj-pulsa']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Tunjangan Pulsa')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Tunjangan Pulsa'), ['create-tunj-pulsa', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Tunj Pulsa Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-sewa-mobil-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PSewaMobil $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PSewaMobil $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-sewa-mobil} {delete-sewa-mobil}',
                                'buttons' => [
                                    'update-sewa-mobil' => function ($url, PSewaMobil $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-sewa-mobil', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-sewa-mobil' => function ($url, PSewaMobil $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-sewa-mobil', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-sewa-mobil',
                                'dataProvider' => $providerSewaMobil,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-sewa-mobil']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Sewa Mobil')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Sewa Mobil'), ['create-sewa-mobil', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Sewa Mobil Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-sewa-motor-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PSewaMotor $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PSewaMotor $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-sewa-motor} {delete-sewa-motor}',
                                'buttons' => [
                                    'update-sewa-motor' => function ($url, PSewaMotor $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-sewa-motor', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-sewa-motor' => function ($url, PSewaMotor $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-sewa-motor', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-sewa-motor',
                                'dataProvider' => $providerSewaMotor,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-sewa-motor']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Sewa Motor')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Sewa Motor'), ['create-sewa-motor', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Tunj Sewa Motor Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-sewa-rumah-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PSewaRumah $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PSewaRumah $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-sewa-rumah} {delete-sewa-rumah}',
                                'buttons' => [
                                    'update-sewa-rumah' => function ($url, PSewaRumah $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-sewa-rumah', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-sewa-rumah' => function ($url, PSewaRumah $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-sewa-rumah', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-sewa-rumah',
                                'dataProvider' => $providerSewaRumah,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-sewa-rumah']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Tunjangan Sewa Rumah')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Tunj Sewa Rumah'), ['create-sewa-rumah', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Tunj Sewa Rumah Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-lain-tetap-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PTunjaganLainTetap $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PTunjaganLainTetap $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-lain-tetap} {delete-lain-tetap}',
                                'buttons' => [
                                    'update-lain-tetap' => function ($url, PTunjaganLainTetap $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-lain-tetap', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-lain-tetap' => function ($url, PTunjaganLainTetap $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-lain-tetap', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-lain-tetap',
                                'dataProvider' => $providerLainTetap,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-lain-tetap']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Tunjangan Lain Tetap')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Tunj Lain Tetap'), ['create-lain-tetap', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Tunj Lain Tetap Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-kos-tab">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PTunjanganKost $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PTunjanganKost $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-tunj-kost} {delete-tunj-kost}',
                                'buttons' => [
                                    'update-tunj-kost' => function ($url, PTunjanganKost $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-tunj-kost', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-tunj-kost' => function ($url, PTunjanganKost $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-tunj-kost', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-tunj-kost',
                                'dataProvider' => $providerTunjKos,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-tunj-kost']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Tunjangan Kost')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Tunj Kost'), ['create-tunj-kost', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Tunj Kos Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="t-admin-bank">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PAdminBank $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PAdminBank $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-admin-bank} {delete-admin-bank}',
                                'buttons' => [
                                    'update-admin-bank' => function ($url, PAdminBank $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-admin-bank', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-admin-bank' => function ($url, PAdminBank $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-admin-bank', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-admin-bank',
                                'dataProvider' => $providerAdminBank,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-admin-bank']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Admin Bank')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Admin Bank'), ['create-admin-bank', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Admin Bank Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                    <div class="tab-pane" id="t-lain-lain">

                        <?php
                        $gridColumnPerjanjianKerja = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PTunjaganLainLain $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'nominal',
                                'value' => function (PTunjaganLainLain $model) {
                                    return Yii::$app->formatter->asDecimal($model->nominal, 0);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-lain-lain} {delete-lain-lain}',
                                'buttons' => [
                                    'update-lain-lain' => function ($url, PTunjaganLainLain $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-lain-lain', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-lain-lain' => function ($url, PTunjaganLainLain $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-lain-lain', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-lain-lain',
                                'dataProvider' => $providerTunjLainLain,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-lain-lain']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'Tunj Lain-lain')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input Tunj Lain-lain'), ['create-lain-lain', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Tunj Lain-lain Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnPerjanjianKerja,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>

                    <div class="tab-pane" id="bpjs-tk-setting">

                        <?php
                        $gridColumnBpjsTkSetting = [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'id', 'hidden' => true],
                            [
                                'attribute' => 'tanggal_mulai',
                                'value' => function (PBpjsTkSetting $model) {
                                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                                }
                            ],
                            [
                                'attribute' => 'jkk',
                                'value' => function (PBpjsTkSetting $model) {
                                    return Status::activeInactive($model->jkk);
                                }
                            ],
                            [
                                'attribute' => 'jkm',
                                'value' => function (PBpjsTkSetting $model) {
                                    return Status::activeInactive($model->jkm);
                                }
                            ],
                            [
                                'attribute' => 'jht',
                                'value' => function (PBpjsTkSetting $model) {
                                    return Status::activeInactive($model->jht);
                                }
                            ],
                            [
                                'attribute' => 'ip',
                                'value' => function (PBpjsTkSetting $model) {
                                    return Status::activeInactive($model->ip);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update-bpjs-tk-setting} {delete-bpjs-tk-setting}',
                                'buttons' => [
                                    'update-bpjs-tk-setting' => function ($url, PBpjsTkSetting $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Edit'), ['update-bpjs-tk-setting', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Perjanjian Kerja {nama}', ['nama' => $model->pegawai->nama])]]);
                                        }
                                        return '';
                                    },
                                    'delete-bpjs-tk-setting' => function ($url, PBpjsTkSetting $model) {
                                        if ($model->is_latest) {
                                            return Html::a(Yii::t('frontend', 'Delete'), ['delete-bpjs-tk-setting', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                                        }
                                        return '';
                                    }
                                ],
                            ],
                            ['attribute' => 'lock', 'hidden' => true],
                        ];
                        try {
                            echo GridView::widget([
                                'id' => 'grid-bpjs-tk-setting',
                                'dataProvider' => $providerBpjsTkSetting,
                                'pjax' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-bpjs-tk-setting']],
                                'panel' => [
                                    'type' => false,
                                    'heading' => '<span></span> ' . Html::encode(Yii::t('frontend', 'BPJS Tk Setting')),
                                    'footer' => false,
                                ],
                                'toolbar' => [
                                    [
                                        'content' => Html::a(Yii::t('frontend', 'Input BPJS Tk Setting'), ['create-bpjs-tk-setting', 'id' => $modelPegawai->id], ['class' => 'btn btn-success btn-xs btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input BPJS Tk Setting Untuk {nama}', ['nama' => $modelPegawai->nama])]])
                                    ],
                                ],
                                'export' => false,
                                'columns' => $gridColumnBpjsTkSetting,
                                'bordered' => false
                            ]);
                        } catch (Exception $e) {
                            print_r($e->getMessage());
                            Yii::info($e->getMessage(), 'exception');
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

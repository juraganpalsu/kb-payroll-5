<?php

/**
 * Created by PhpStorm.
 * File Name: _form-sewa-motor.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 30/03/20
 * Time: 23.05
 */

use frontend\modules\masterdata\models\PSewaMotor;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\RefAreaKerja;
use kartik\datecontrol\DateControl;
use kartik\tree\TreeViewInput;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model PSewaMotor */
/**
 * @var $modelPegawai Pegawai
 */


$js = <<< JS
$(function() {
    $('#form-sewa-motor').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
                // location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
        
});
JS;

$this->registerJs($js);

$pegawaiid = $modelPegawai->id;

$action = $model->isNewRecord ? Url::to(['create-sewa-motor', 'id' => $modelPegawai->id]) : Url::to(['update-sewa-motor', 'id' => $model->id]);
$validation = $model->isNewRecord ? Url::to(['form-sewa-motor-validation', 'id' => $modelPegawai->id]) : Url::to(['form-sewa-motor-validation', 'id' => $model->id, 'isnew' => false]);
?>

<div class="sewa-motor-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-sewa-motor',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'tanggal_mulai')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <?php
                echo $form->field($model, 'nominal')->textInput();
                ?>
            </div>
        </div>

        <?php
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <?php
        echo $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput();
        ?>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }

    ?>

    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use common\components\Status;
use common\models\Golongan;
use common\models\SistemKerja;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\PerjanjianKerjaJenis;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\struktur\models\Departemen;
use mdm\admin\components\Helper;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $modelPerjanjiankerja PerjanjianKerja
 * @var $modelStruktur PegawaiStruktur
 * @var $departemen \frontend\modules\struktur\models\Departemen
 */

$this->title = Yii::t('app', 'Data Payroll Component');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pegawai-index small">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'id_pegawai',
            'value' => function (Pegawai $model) {
                return $model->perjanjianKerja ? Html::tag('strong', str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT), ['class' => 'font-weight-bold']) : '';
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'nama_lengkap',
            'value' => function (Pegawai $model) {
                return Html::a(Html::tag('strong', $model->nama_lengkap), ['view', 'id' => $model->id], ['class' => 'text-danger font-weight-bold']);
            },
            'format' => 'raw'
        ],
//        [
//            'attribute' => 'tanggal_awal_masuk',
//            'value' => function (Pegawai $model) {
//                return $model->getTanggalAwalMasuk();
//            },
//        ],
        [
            'attribute' => 'golongan',
            'value' => function (Pegawai $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
//        [
//            'attribute' => 'jenis_kontrak',
//            'value' => function (Pegawai $model) {
//                if ($perjanjianKerja = $model->perjanjianKerja) {
//                    return $perjanjianKerja->jenis->nama;
//                }
//                return '';
//            },
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ArrayHelper::map(PerjanjianKerjaJenis::find()->all(), 'id', 'nama'),
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('jenis_kontrak'), 'id' => 'grid-pegawai-search-jenis_kontrak'],
//            'hAlign' => 'center',
//            'format' => 'raw'
//        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (Pegawai $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $modelPerjanjiankerja->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'departemen',
            'value' => function (Pegawai $model) {
                if ($struktur = $model->pegawaiStruktur) {
                    return $struktur->struktur ? $struktur->struktur->departemen ? $struktur->struktur->departemen->nama : '' : '';
                }
                return '';
            },
        ],
//        [
//            'attribute' => 'struktur',
//            'value' => function (Pegawai $model) {
//                if ($struktur = $model->pegawaiStruktur) {
//                    return $struktur->struktur->nameCostume;
//                }
//                return '';
//            }
//        ],
        [
            'attribute' => 'tgl_gapok',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getGajiPokokAktif()) {
                        return date('d-m-Y', strtotime($masterData->getGajiPokokAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'gapok',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getGajiPokokAktif()) {
                        return $masterData->getGajiPokokAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_premi_hadir',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getPremiHadirAktif()) {
                        return date('d-m-Y', strtotime($masterData->getPremiHadirAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'premi_hadir',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getPremiHadirAktif()) {
                        return $masterData->getPremiHadirAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_uang_makan',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getUangMakanAktif()) {
                        return date('d-m-Y', strtotime($masterData->getUangMakanAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'uang_makan',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getUangMakanAktif()) {
                        return $masterData->getUangMakanAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_uang_transport',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getUangTransportAktif()) {
                        return date('d-m-Y', strtotime($masterData->getUangTransportAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'uang_transport',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getUangTransportAktif()) {
                        return $masterData->getUangTransportAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_jabatan',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjJabatanAktif()) {
                        return date('d-m-Y', strtotime($masterData->getTunjJabatanAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'jabatan',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjJabatanAktif()) {
                        return $masterData->getTunjJabatanAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_insentif_tetap',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getInsentifTetap()) {
                        return date('d-m-Y', strtotime($masterData->getInsentifTetap(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'insentif_tetap',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getInsentifTetap()) {
                        return $masterData->getInsentifTetap(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_pulsa',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjPulsaAktif()) {
                        return date('d-m-Y', strtotime($masterData->getTunjPulsaAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'pulsa',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjPulsaAktif()) {
                        return $masterData->getTunjPulsaAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_sewa_mobil',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getSewaMobilAktif()) {
                        return date('d-m-Y', strtotime($masterData->getSewaMobilAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'sewa_mobil',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getSewaMobilAktif()) {
                        return $masterData->getSewaMobilAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_sewa_motor',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getSewaMotorAktif()) {
                        return date('d-m-Y', strtotime($masterData->getSewaMotorAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'sewa_motor',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getSewaMotorAktif()) {
                        return $masterData->getSewaMotorAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_sewa_rumah',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getSewaRumahAktif()) {
                        return date('d-m-Y', strtotime($masterData->getSewaRumahAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'sewa_rumah',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getSewaRumahAktif()) {
                        return $masterData->getSewaRumahAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_lain_tetap',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjanganLainLainAktif()) {
                        return date('d-m-Y', strtotime($masterData->getTunjanganLainLainAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'lain_tetap',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjanganLainLainAktif()) {
                        return $masterData->getTunjanganLainLainAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_tunj_kos',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjKostAktif()) {
                        return date('d-m-Y', strtotime($masterData->getTunjKostAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'tunj_kos',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjKostAktif()) {
                        return $masterData->getTunjKostAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_admin_bank',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getAdminBankAktif()) {
                        return date('d-m-Y', strtotime($masterData->getAdminBankAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'admin_bank',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getAdminBankAktif()) {
                        return $masterData->getAdminBankAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
        [
            'attribute' => 'tgl_tunj_lain_lain',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjanganLainLainAktif()) {
                        return date('d-m-Y', strtotime($masterData->getTunjanganLainLainAktif(null, true)->tanggal_mulai));
                    }
                    return '';
                }
                return '';
            },
        ],
        [
            'attribute' => 'tunj_lain_lain',
            'value' => function (Pegawai $model) {
                if ($masterData = $model->masterDataDefaults) {
                    if ($gapok = $masterData->getTunjanganLainLainAktif()) {
                        return $masterData->getTunjanganLainLainAktif(null, false);
                    }
                    return 0;
                }
                return 0;
            },
        ],
//        [
//            'attribute' => 'area_kerja',
//            'value' => function (Pegawai $model) {
//                if ($areaKerja = $model->areaKerja) {
//                    return $areaKerja->refAreaKerja->name;
//                }
//                return '';
//            }
//        ],
//        [
//            'attribute' => 'masa_kerja (tahun)',
//            'value' => function (Pegawai $model) {
//                $masaKerja = '';
//                if ($dasarMasaKerja = $model->perjanjianKerja->getPerjanjianDasar()) {
//                    $masaKerja = $dasarMasaKerja->hitungMasaKerja('', '')['tahun'];
//                }
//                return $masaKerja;
//            }
//        ],
//        [
//            'attribute' => 'tgl_masa_kerja',
//            'value' => function (Pegawai $model) {
//                $tanggalmk = '';
//                if ($dasarMasaKerja = $model->perjanjianKerja->getPerjanjianDasar()) {
//                    $masaKerja = $dasarMasaKerja->hitungMasaKerja('', '')['tahun'];
//                    $tanggalmk = $masaKerja > 0 ? date('d-m-Y', strtotime($dasarMasaKerja->mulai_berlaku)) : '';
//                }
//                return $tanggalmk;
//            }
//        ],
        [
            'attribute' => 'is_resign',
            'value' => function (Pegawai $model) {
                if ($model->is_resign) {
                    return Html::a(Status::activeInactive(Status::YA), ['change-resign-status', 'id' => $model->id, 'status' => Status::TIDAK], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-resign-status-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merubah status <strong>{pegawai}</strong> ?', ['pegawai' => $model->nama_lengkap])]]);
                }
                return Html::a(Status::activeInactive(Status::TIDAK), ['change-resign-status', 'id' => $model->id, 'status' => Status::YA], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-resign-status-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merubah status <strong>{pegawai}</strong> ?', ['pegawai' => $model->nama_lengkap])]]);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::activeInactive(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('is_resign'), 'id' => 'grid-pegawai-search-is_resign'],

            'format' => 'raw'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}'
        ],
    ];
    ?>
    <?php try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
//            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
            'noExportColumns' => [1], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'payroll-component' . date('dmYHis')
        ]);

        echo GridView::widget([
            'id' => 'grid-pegawai',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pegawai']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index-component'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

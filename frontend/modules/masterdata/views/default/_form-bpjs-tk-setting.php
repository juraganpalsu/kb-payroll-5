<?php

/**
 * Created by PhpStorm.
 * File Name: _form-bpjs-tk-setting.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-09-10
 * Time: 6:33 AM
 */


use common\models\Golongan;
use frontend\modules\masterdata\models\PBpjsTkSetting;
use frontend\modules\pegawai\models\Pegawai;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model PBpjsTkSetting */
/**
 * @var $modelPegawai Pegawai
 */


$js = <<< JS
$(function() {
    $('#form-input').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
        
});
JS;

$this->registerJs($js);

$pegawaiid = $modelPegawai->id;

$action = $model->isNewRecord ? Url::to(['create-bpjs-tk-setting', 'id' => $modelPegawai->id]) : Url::to(['update-bpjs-tk-setting', 'id' => $model->id]);
$validation = $model->isNewRecord ? Url::to(['form-bpjs-tk-setting-validation', 'id' => $modelPegawai->id]) : Url::to(['form-bpjs-tk-setting-validation', 'id' => $model->id, 'isnew' => false]);
?>

<div class="input-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-input',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $validation,
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'tanggal_mulai')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <?php
            $model->jkk = true;
            $model->jkm = true;
            if ($modelPegawai->getGolonganTerakhir()->golongan->id != Golongan::NOL) {
                $model->jht = true;
                $model->ip = true;
            }
            ?>
            <div class="col-md-2">
                <?php echo $form->field($model, 'jkk')->checkbox(); ?>
            </div>
            <div class="col-md-2">
                <?php echo $form->field($model, 'jkm')->checkbox(); ?>
            </div>
            <div class="col-md-2">
                <?php echo $form->field($model, 'jht')->checkbox(); ?>
            </div>
            <div class="col-md-2">
                <?php echo $form->field($model, 'ip')->checkbox(); ?>
            </div>
        </div>

        <?php
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
        ?>

        <?php
        echo $form->field($model, 'pegawai_id', ['template' => '{input}'])->hiddenInput();
        ?>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }

    ?>

    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\masterdata\models\PTunjanganLoyalitasConfig */
/* @var $form yii\widgets\ActiveForm */

$js = <<< JS
$(function() {
    $('#form-ptunjangan-loyalitas-config').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });    
})
JS;

$this->registerJs($js);

?>


<div class="ptunjangan-loyalitas-config-form">
    <?php
    $form = ActiveForm::begin([
        'id' => 'form-ptunjangan-loyalitas-config',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>
    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'masa_kerja')->textInput(['placeholder' => Yii::t('frontend', 'Dari'), 'type' => 'number']) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'masa_kerja_sampai')->textInput(['placeholder' => Yii::t('frontend', 'Sampai'), 'type' => 'number']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'nominal')->textInput(['placeholder' => 'Nominal', 'type' => 'number']) ?>
            </div>
        </div>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\masterdata\models\PTunjanganLoyalitasConfig */

$this->title = Yii::t('frontend', 'Create Tunjangan Loyalitas Config');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'P Tunjangan Loyalitas Config'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ptunjangan-loyalitas-config-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\masterdata\models\PTunjanganLoyalitasConfig */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => ' Tunjangan Loyalitas Config',
    ]) . ' ' . $model->masa_kerja;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Tunjangan Loyalitas Config'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->masa_kerja, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="ptunjangan-loyalitas-config-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

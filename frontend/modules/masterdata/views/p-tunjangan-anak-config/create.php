<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\masterdata\models\PTunjanganAnakConfig */

$this->title = Yii::t('frontend', 'Create P Tunjangan Anak Config');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'P Tunjangan Anak Config'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ptunjangan-anak-config-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

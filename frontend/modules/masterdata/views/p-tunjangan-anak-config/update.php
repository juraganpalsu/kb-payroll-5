<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\masterdata\models\PTunjanganAnakConfig */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Tunjangan Anak Config',
]) . ' ' . $model->jumlah_anak;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Tunjangan Anak Config'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jumlah_anak, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="ptunjangan-anak-config-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

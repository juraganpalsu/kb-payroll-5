<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\masterdata\models\MasterUangShift */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="master-uang-shift-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'shift_1')->textInput(['placeholder' => 'Shift 1']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'shift_2')->textInput(['placeholder' => 'Shift 2']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'shift_3')->textInput(['placeholder' => 'Shift 3']) ?>
        </div>
    </div>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

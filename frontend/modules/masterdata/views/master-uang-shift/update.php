<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\masterdata\models\MasterUangShift */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Master Uang Shift',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Master Uang Shift'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="master-uang-shift-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

namespace frontend\modules\masterdata\models;

use common\components\PegawaiAktifValidation;
use frontend\modules\masterdata\models\base\PUangTransport as BasePUangTransport;
use Yii;

/**
 * This is the model class for table "p_uang_transport".
 */
class PUangTransport extends BasePUangTransport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_mulai', 'pegawai_id'], 'required'],
            [['nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_mulai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['is_latest'], 'default', 'value' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['tanggal_mulai', PegawaiAktifValidation::class],
            ['tanggal_mulai', 'tanggalValidation']
        ];
    }

    /**
     * @param string $attribute
     */
    public function tanggalValidation(string $attribute)
    {
        $query = PUangTransport::find()->andWhere(['>=', 'tanggal_mulai', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id, 'is_latest' => 0])->one();
        if (!empty($query)) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal tidak diperbolehkan.'));
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($insert) {
            PUangTransport::updateAll(['is_latest' => 0]);
        }
        return true;
    }
	
}

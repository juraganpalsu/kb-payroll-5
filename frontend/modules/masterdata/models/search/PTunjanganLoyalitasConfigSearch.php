<?php

namespace frontend\modules\masterdata\models\search;

use frontend\modules\masterdata\models\PTunjanganLoyalitasConfig;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\masterdata\models\search\PTunjanganLoyalitasConfigSearch represents the model behind the search form about `frontend\modules\masterdata\models\PTunjanganLoyalitasConfig`.
 */
class PTunjanganLoyalitasConfigSearch extends PTunjanganLoyalitasConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['masa_kerja', 'masa_kerja_sampai', 'nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PTunjanganLoyalitasConfig::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'masa_kerja' => $this->masa_kerja,
            'masa_kerja_sampai' => $this->masa_kerja_sampai,
            'nominal' => $this->nominal,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

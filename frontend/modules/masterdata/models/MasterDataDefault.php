<?php

namespace frontend\modules\masterdata\models;

use common\models\TimeTable as TimeTableAlias;
use frontend\modules\masterdata\models\base\MasterDataDefault as BaseMasterDataDefault;
use frontend\modules\payroll\models\InsentifTidakTetap;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\Rapel;

/**
 * This is the model class for table "master_data_default".
 *
 * @property array $jenisGaji
 */
class MasterDataDefault extends BaseMasterDataDefault
{

    const POKOK_FIX = 1; //gaji bulanan periode 21-20 gol 2 ketas
    const HARIAN = 2; // gaji harian periode 16-15 gol 1 bca
    const BORONGAN = 3; // borongan gol 1 bca
    const POKOK_FIX_H = 4; // gaji bulanan periode harian 16-15 gol 1 kbu produksi dan gol 1 bca operator
    const HARIAN_B = 5; // gaji harian periode bulanan 21-20 spg

    public $jenisGaji = [self::POKOK_FIX => 'POKOK_FIX_B', self::HARIAN => 'HARIAN_H', self::BORONGAN => 'BORONGAN'
        , self::POKOK_FIX_H => 'POKOK_FIX_H', self::HARIAN_B => 'HARIAN_B'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id',], 'required'],
            [['tipe_gaji_pokok', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'created_by_pk'], 'string', 'max' => 32],
            [['tunjangan_anak', 'prorate_komponen', 'tunjangan_loyalitas', 'cabang', 'beatroute', 'pot_keterlambatan', 'uang_lembur', 'uang_makan_lembur', 'uang_shift', 'insentif_tidak_tetap', 'insentif_lembur'], 'default', 'value' => 0],
            [['bonus', 'thr'], 'default', 'value' => '0'],
            [['perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id'], 'default', 'value' => 0],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param string $pegawaiId
     * @return MasterDataDefault|null
     */
    public static function getModel(string $pegawaiId): ?MasterDataDefault
    {
        return self::findOne(['pegawai_id' => $pegawaiId]);
    }

    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PGajiPokok|int|null
     */
    public function getGajiPokokAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PGajiPokok::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PGajiPokok::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return 0;
    }

    /**
     * @param string|null $tanggal
     * @return float|int
     */
    public function getUpahLemburPerJam(string $tanggal = null)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $gajiPokok = $this->getGajiPokokAktif($tanggal);
        $upahLemburPerJam = 0;
        if (!is_null($gajiPokok) && $this->uang_lembur) {
            $upahLemburPerJam = ($gajiPokok / 173);
        }
        return $upahLemburPerJam;
    }

    /**
     * @param float $jumlahJamLembur
     * @param string|null $tanggal
     * @return float|int
     */
    public function hitungUpahLembur(float $jumlahJamLembur, string $tanggal = null)
    {
        $uangLembur = 0;
        if ($masterData = MasterDataDefault::getModel($this->pegawai_id)) {
            if (!$masterData->uang_lembur) {
                return $uangLembur;
            }
        } else {
            return $uangLembur;
        }
        return $masterData->getUpahLemburPerJam($tanggal) * $jumlahJamLembur;
    }

    /**
     * @param string $pegawaiId
     * @param float $jumlahJamLembur
     * @param string|null $tanggal
     * @return float|int
     */
    public static function _hitungUpahLembur(string $pegawaiId, float $jumlahJamLembur, string $tanggal = null)
    {
        $model = new MasterDataDefault();
        $model->pegawai_id = $pegawaiId;
        return $model->hitungUpahLembur($jumlahJamLembur, $tanggal);
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PAdminBank|int|null
     */
    public function getAdminBankAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PAdminBank::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PAdminBank::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }

    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PTunjaganLainLain|int|null
     */
    public function getTunjanganLainLainAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PTunjaganLainLain::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PTunjaganLainLain::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }

    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PTunjanganPulsa|int|null
     */
    public function getTunjPulsaAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PTunjanganPulsa::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PTunjanganPulsa::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PPremiHadir|int|null
     */
    public function getPremiHadirAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PPremiHadir::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PPremiHadir::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PSewaMobil|int|null
     */
    public function getSewaMobilAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PSewaMobil::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PSewaMobil::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PSewaMotor|int|null
     */
    public function getSewaMotorAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PSewaMotor::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PSewaMotor::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PSewaRumah|int|null
     */
    public function getSewaRumahAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PSewaRumah::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PSewaRumah::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PTunjanganJabatan|int|null
     */
    public function getTunjJabatanAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PTunjanganJabatan::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PTunjanganJabatan::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PTunjanganKost|int|null
     */
    public function getTunjKostAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PTunjanganKost::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PTunjanganKost::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PUangTransport|int|null
     */
    public function getUangTransportAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PUangTransport::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PUangTransport::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param int $jumlahAnak
     * @param bool $asObject
     * @return PTunjanganAnakConfig|int
     */
    public static function getTunjanganAnak(int $jumlahAnak, bool $asObject = false)
    {
        $model = PTunjanganAnakConfig::findOne(['jumlah_anak' => $jumlahAnak]);
        if ($model) {
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return 0;
    }

    /**
     * @param int $masaKerja
     * @param bool $asObject
     * @return PTunjanganLoyalitasConfig|int
     */
    public static function getTunjanganLoyalitas(int $masaKerja, bool $asObject = false)
    {
        $query = PTunjanganLoyalitasConfig::find()->select('id')
            ->andWhere($masaKerja . ' BETWEEN masa_kerja AND masa_kerja_sampai')->one();
        if ($query) {
            $model = PTunjanganLoyalitasConfig::findOne($query['id']);
            if ($model) {
                if ($asObject) {
                    return $model;
                }
                return $model->nominal;
            }
        }
        return 0;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PUangMakan|int|null
     */
    public function getUangMakanAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PUangMakan::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PUangMakan::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return 0;
    }

    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PTunjaganLainTetap|int
     */
    public function getTunjanganLainTetap(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PTunjaganLainTetap::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PTunjaganLainTetap::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return 0;
    }


    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PInsentifTetap|int|null
     */
    public function getInsentifTetap(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PInsentifTetap::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PInsentifTetap::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return 0;
    }

    /**
     * @param PayrollPeriode $payrolPeriode
     * @param int $kategori
     * @return float
     */
    public function getInsentifTidakTetap(PayrollPeriode $payrolPeriode, int $kategori = InsentifTidakTetap::INSENTIF_UMUM): float
    {
        $query = InsentifTidakTetap::find()->andWhere(['pegawai_id' => $this->pegawai_id, 'payroll_periode_id' => $payrolPeriode->id, 'kategori' => $kategori]);
        return (float)$query->sum('jumlah');
    }

    /**
     * @param PayrollPeriode $payrolPeriode
     * @return float
     */
    public function getRapel(PayrollPeriode $payrolPeriode): float
    {
        $query = Rapel::find()->andWhere(['pegawai_id' => $this->pegawai_id, 'periode_bayar' => $payrolPeriode->id]);
        return (float)$query->sum('jumlah');
    }

    /**
     * @param int $shift
     * @return int
     */
    public static function getUangShift(int $shift = TimeTableAlias::SHIFT_DUA): int
    {
        $model = MasterUangShift::findOne(1);
        if ($shift == 2) {
            return $model->shift_2;
        } else if ($shift == 3) {
            return $model->shift_3;
        } else {
            return $model->shift_1;
        }

    }


}

<?php

namespace frontend\modules\masterdata\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "master_data_default".
 *
 * @property string $id
 * @property integer $tunjangan_anak
 * @property integer $prorate_komponen
 * @property integer $tunjangan_loyalitas
 * @property integer $cabang
 * @property integer $beatroute
 * @property integer $pot_keterlambatan
 * @property integer $uang_lembur
 * @property integer $uang_makan_lembur
 * @property integer $uang_shift
 * @property integer $insentif_tidak_tetap
 * @property integer $insentif_lembur
 * @property integer $bonus
 * @property integer $thr
 * @property integer $tipe_gaji_pokok
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_struktur_id
 * @property string $pegawai_golongan_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Pegawai $pegawai
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 */
class MasterDataDefault extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id'], 'required'],
            [['tipe_gaji_pokok', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'created_by_pk'], 'string', 'max' => 32],
            [['tunjangan_anak', 'prorate_komponen', 'tunjangan_loyalitas', 'cabang', 'beatroute', 'pot_keterlambatan', 'uang_lembur', 'uang_makan_lembur', 'uang_shift', 'insentif_tidak_tetap', 'insentif_lembur', 'bonus', 'thr'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_default';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tunjangan_anak' => Yii::t('frontend', 'Tunjangan Anak'),
            'tunjangan_loyalitas' => Yii::t('frontend', 'Tunjangan Loyalitas'),
            'cabang' => Yii::t('frontend', 'Cabang'),
            'prorate_komponen' => Yii::t('frontend', 'Prorate Komponen (Driver/Sales Depo)'),
            'beatroute' => Yii::t('frontend', 'Beatroute'),
            'pot_keterlambatan' => Yii::t('frontend', 'Pot Keterlambatan'),
            'uang_lembur' => Yii::t('frontend', 'Uang Lembur'),
            'uang_makan_lembur' => Yii::t('frontend', 'Uang Makan Lembur'),
            'uang_shift' => Yii::t('frontend', 'Uang Shift'),
            'insentif_tidak_tetap' => Yii::t('frontend', 'Insentif Tidak Tetap'),
            'insentif_lembur' => Yii::t('frontend', 'Insentif Lembur'),
            'bonus' => Yii::t('frontend', 'Bonus'),
            'thr' => Yii::t('frontend', 'Thr'),
            'tipe_gaji_pokok' => Yii::t('frontend', 'Tipe Gaji Pokok'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}
<?php

namespace frontend\modules\masterdata\models;

use \frontend\modules\masterdata\models\base\MasterDataConfig as BaseMasterDataConfig;

/**
 * This is the model class for table "master_data_config".
 */
class MasterDataConfig extends BaseMasterDataConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tunjangan_anak', 'tunjangan_loyalitas', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'tunjangan_anak', 'tunjangan_loyalitas'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return bool|MasterDataConfig|null
     */
    public static function getConfig()
    {
        $query = MasterDataConfig::find()->select('id')->one();
        if ($query) {
            return MasterDataConfig::findOne($query);
        }
        return false;
    }

}

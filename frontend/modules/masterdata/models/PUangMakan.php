<?php

namespace frontend\modules\masterdata\models;

use common\components\PegawaiAktifValidation;
use frontend\models\Absensi;
use frontend\modules\masterdata\models\base\PUangMakan as BasePUangMakan;
use Yii;

/**
 * This is the model class for table "p_uang_makan".
 */
class PUangMakan extends BasePUangMakan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_mulai', 'pegawai_id'], 'required'],
            [['nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_mulai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['is_latest'], 'default', 'value' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['tanggal_mulai', PegawaiAktifValidation::class],
            ['tanggal_mulai', 'tanggalValidation']
        ];
    }

    /**
     * @param string $attribute
     */
    public function tanggalValidation(string $attribute)
    {
        $query = PUangMakan::find()->andWhere(['>=', 'tanggal_mulai', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id, 'is_latest' => 0])->one();
        if (!empty($query)) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal tidak diperbolehkan.'));
        }
    }


    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($insert) {
            PUangMakan::updateAll(['is_latest' => 0]);
        }
        return true;
    }

    /**
     * @param string|null $tanggal
     * @param bool $asObject
     * @return PUangMakan|int|null
     */
    public function getUangMakanAktif(string $tanggal = null, bool $asObject = false)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PUangMakan::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        if ($query) {
            $model = PUangMakan::findOne($query['id']);
            if ($asObject) {
                return $model;
            }
            return $model->nominal;
        }
        return null;
    }


    /**
     * @param string|null $tanggal
     * @return float|int
     */
    public function hitungUangMakanLembur(string $tanggal = null)
    {
        $pengaliUml = 0;
        if ($masterData = MasterDataDefault::getModel($this->pegawai_id)) {
            if (!$masterData->uang_makan_lembur) {
                return $pengaliUml;
            }
        } else {
            return $pengaliUml;
        }

        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $modelAbsensi = Absensi::findOne(['pegawai_id' => $this->pegawai_id, 'tanggal' => $tanggal]);
        if ($modelAbsensi) {
            $pengaliUml = $modelAbsensi->hitungPengaliUangMakanLembur();
        }
        return $this->getUangMakanAktif($tanggal) * $pengaliUml;
    }

    /**
     * @param string $pegawaiId
     * @param string|null $tanggal
     * @return float|int
     */
    public static function _hitungUangMakanLembur(string $pegawaiId, string $tanggal = null)
    {
        $model = new PUangMakan();
        $model->pegawai_id = $pegawaiId;
        return $model->hitungUangMakanLembur($tanggal);
    }

}

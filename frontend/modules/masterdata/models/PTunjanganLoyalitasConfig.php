<?php

namespace frontend\modules\masterdata\models;

use frontend\modules\masterdata\models\base\PTunjanganLoyalitasConfig as BasePTunjanganLoyalitasConfig;

/**
 * This is the model class for table "p_tunjangan_loyalitas_config".
 */
class PTunjanganLoyalitasConfig extends BasePTunjanganLoyalitasConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['masa_kerja', 'masa_kerja_sampai', 'nominal'], 'required'],
            [['masa_kerja', 'nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

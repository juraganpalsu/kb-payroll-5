<?php

namespace frontend\modules\masterdata\models;

use frontend\modules\masterdata\models\base\MasterUangShift as BaseMasterUangShift;

/**
 * This is the model class for table "master_uang_shift".
 */
class MasterUangShift extends BaseMasterUangShift
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['shift_1', 'shift_2', 'shift_3', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     *
     * Mengambil nominal uang shift
     *
     * @param int $shift
     * @return int
     */
    public static function getNominal(int $shift = 1)
    {
        $model = self::findOne(1);
        return $shift == 1 ? $model->shift_1 : $shift == 2 ? $model->shift_2 : $model->shift_3;
    }


}

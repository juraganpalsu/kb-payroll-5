<?php

namespace frontend\modules\masterdata\models;

use frontend\modules\masterdata\models\base\PTunjanganAnakConfig as BasePTunjanganAnakConfig;

/**
 * This is the model class for table "p_tunjangan_anak_config".
 */
class PTunjanganAnakConfig extends BasePTunjanganAnakConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jumlah_anak', 'nominal'], 'required'],
            [['jumlah_anak', 'nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

<?php

namespace frontend\modules\indonesia\controllers;

use frontend\modules\indonesia\models\IndonesiaDesa;
use frontend\modules\indonesia\models\IndonesiaKabupatenKota;
use frontend\modules\indonesia\models\IndonesiaKecamatan;
use Yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * Default controller for the `indonesia` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param int $step
     * @return array
     */
    public function actionNamaDaerah(int $step)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        $request = Yii::$app->request;
        if ($id = $request->post('depdrop_parents')) {
            $list = [];
            if ($step == 1) {
                $list = IndonesiaKabupatenKota::find()->andWhere(['provinsi_id' => $id])->orderBy('nama ASC')->asArray()->all();
            }
            if ($step == 2) {
                $list = IndonesiaKecamatan::find()->andWhere(['kabupaten_kota_id' => $id])->orderBy('nama ASC')->asArray()->all();
            }
            if ($step == 3) {
                $list = IndonesiaDesa::find()->andWhere(['kecamatan_id' => $id])->orderBy('nama ASC')->asArray()->all();
            }
            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $indonesia) {
                    $out[] = ['id' => $indonesia['id'], 'name' => $indonesia['nama']];
                    if ($i == 0) {
                        $selected = $indonesia['id'];
                    }
                }
                return ['output' => $out, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => ''];
    }
}

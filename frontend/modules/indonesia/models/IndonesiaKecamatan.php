<?php

namespace frontend\modules\indonesia\models;

use frontend\modules\indonesia\models\base\IndonesiaKecamatan as BaseIndonesiaKecamatan;

/**
 * This is the model class for table "indonesia_kecamatan".
 */
class IndonesiaKecamatan extends BaseIndonesiaKecamatan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'kabupaten_kota_id', 'nama'], 'required'],
            [['id', 'kabupaten_kota_id'], 'integer'],
            [['nama'], 'string', 'max' => 45]
        ];
    }

}

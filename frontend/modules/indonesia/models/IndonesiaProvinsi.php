<?php

namespace frontend\modules\indonesia\models;

use frontend\modules\indonesia\models\base\IndonesiaProvinsi as BaseIndonesiaProvinsi;

/**
 * This is the model class for table "indonesia_provinsi".
 */
class IndonesiaProvinsi extends BaseIndonesiaProvinsi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama'], 'required'],
            [['id'], 'integer'],
            [['nama'], 'string', 'max' => 225]
        ];
    }

}

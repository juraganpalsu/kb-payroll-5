<?php

namespace frontend\modules\indonesia\models;

use frontend\modules\indonesia\models\base\IndonesiaKabupatenKota as BaseIndonesiaKabupatenKota;

/**
 * This is the model class for table "indonesia_kabupaten_kota".
 */
class IndonesiaKabupatenKota extends BaseIndonesiaKabupatenKota
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return 	    [
            [['id', 'provinsi_id', 'nama'], 'required'],
            [['id', 'provinsi_id'], 'integer'],
            [['nama'], 'string', 'max' => 225]
        ];
    }
	
}

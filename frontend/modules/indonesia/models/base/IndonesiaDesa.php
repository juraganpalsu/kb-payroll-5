<?php

namespace frontend\modules\indonesia\models\base;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "indonesia_desa".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $kecamatan_id
 *
 * @property \frontend\modules\indonesia\models\IndonesiaKecamatan $kecamatan
 */
class IndonesiaDesa extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama', 'kecamatan_id'], 'required'],
            [['id', 'kecamatan_id'], 'integer'],
            [['nama'], 'string', 'max' => 225]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indonesia_desa';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'nama' => Yii::t('frontend', 'Nama'),
            'kecamatan_id' => Yii::t('frontend', 'Kecamatan ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getKecamatan()
    {
        return $this->hasOne(\frontend\modules\indonesia\models\IndonesiaKecamatan::class, ['id' => 'kecamatan_id']);
    }
}

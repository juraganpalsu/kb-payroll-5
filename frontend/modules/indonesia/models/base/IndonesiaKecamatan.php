<?php

namespace frontend\modules\indonesia\models\base;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "indonesia_kecamatan".
 *
 * @property integer $id
 * @property integer $kabupaten_kota_id
 * @property string $nama
 *
 * @property \frontend\modules\indonesia\models\IndonesiaDesa[] $indonesiaDesas
 * @property \frontend\modules\indonesia\models\IndonesiaKabupatenKota $kabupatenKota
 */
class IndonesiaKecamatan extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'kabupaten_kota_id', 'nama'], 'required'],
            [['id', 'kabupaten_kota_id'], 'integer'],
            [['nama'], 'string', 'max' => 225]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indonesia_kecamatan';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'kabupaten_kota_id' => Yii::t('frontend', 'Kabupaten Kota ID'),
            'nama' => Yii::t('frontend', 'Nama'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getIndonesiaDesas()
    {
        return $this->hasMany(\frontend\modules\indonesia\models\IndonesiaDesa::class, ['kecamatan_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getKabupatenKota()
    {
        return $this->hasOne(\frontend\modules\indonesia\models\IndonesiaKabupatenKota::class, ['id' => 'kabupaten_kota_id']);
    }
}

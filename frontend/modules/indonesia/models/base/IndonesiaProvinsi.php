<?php

namespace frontend\modules\indonesia\models\base;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "indonesia_provinsi".
 *
 * @property integer $id
 * @property string $nama
 *
 * @property \frontend\modules\indonesia\models\IndonesiaKabupatenKota[] $indonesiaKabupatenKotas
 */
class IndonesiaProvinsi extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama'], 'required'],
            [['id'], 'integer'],
            [['nama'], 'string', 'max' => 225]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indonesia_provinsi';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'nama' => Yii::t('frontend', 'Nama'),
        ];
    }
    
    /**
     * @return ActiveQuery
     */
    public function getIndonesiaKabupatenKotas()
    {
        return $this->hasMany(\frontend\modules\indonesia\models\IndonesiaKabupatenKota::class, ['provinsi_id' => 'id']);
    }
    }

<?php

namespace frontend\modules\indonesia\models\base;

use mootensai\relation\RelationTrait;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "indonesia_kabupaten_kota".
 *
 * @property integer $id
 * @property integer $provinsi_id
 * @property string $nama
 *
 * @property \frontend\modules\indonesia\models\IndonesiaProvinsi $provinsi
 * @property \frontend\modules\indonesia\models\IndonesiaKecamatan[] $indonesiaKecamatans
 */
class IndonesiaKabupatenKota extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provinsi_id', 'nama'], 'required'],
            [['id', 'provinsi_id'], 'integer'],
            [['nama'], 'string', 'max' => 225]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indonesia_kabupaten_kota';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'provinsi_id' => Yii::t('frontend', 'Provinsi ID'),
            'nama' => Yii::t('frontend', 'Nama'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProvinsi()
    {
        return $this->hasOne(\frontend\modules\indonesia\models\IndonesiaProvinsi::class, ['id' => 'provinsi_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getIndonesiaKecamatans()
    {
        return $this->hasMany(\frontend\modules\indonesia\models\IndonesiaKecamatan::class, ['kabupaten_kota_id' => 'id']);
    }
}

<?php

namespace frontend\modules\indonesia\models;

use frontend\modules\indonesia\models\base\IndonesiaDesa as BaseIndonesiaDesa;

/**
 * This is the model class for table "indonesia_desa".
 */
class IndonesiaDesa extends BaseIndonesiaDesa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama', 'kecamatan_id'], 'required'],
            [['id', 'kecamatan_id'], 'integer'],
            [['nama'], 'string', 'max' => 225]
        ];
    }

}

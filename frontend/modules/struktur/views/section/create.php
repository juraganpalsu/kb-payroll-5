<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Section */

$this->title = Yii::t('frontend', 'Create Section');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Section'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

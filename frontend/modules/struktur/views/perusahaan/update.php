<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Perusahaan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Perusahaan',
]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Perusahaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="perusahaan-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

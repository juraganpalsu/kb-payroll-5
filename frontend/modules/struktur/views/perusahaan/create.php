<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Perusahaan */

$this->title = Yii::t('frontend', 'Create Perusahaan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Perusahaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

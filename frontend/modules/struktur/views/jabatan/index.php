<?php

/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('frontend', 'Jabatan');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="jabatan-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'kode',
        'keterangan:ntext',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}'
        ],
    ];

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-jabatan']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                '{export}',
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
    } ?>

</div>

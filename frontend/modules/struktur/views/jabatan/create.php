<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Jabatan */

$this->title = Yii::t('frontend', 'Create Jabatan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Jabatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jabatan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Jabatan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Jabatan',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Jabatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['index', 'FungsiJabatanSearch' => ['nama' => $model->nama]]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="jabatan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Divisi */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Divisi',
]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Divisi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="divisi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

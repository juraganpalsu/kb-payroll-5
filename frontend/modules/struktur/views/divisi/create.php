<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Divisi */

$this->title = Yii::t('frontend', 'Create Divisi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Divisi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="divisi-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Divisi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Divisi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="divisi-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Divisi').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'kode',
        'catatan:ntext',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
        [
            'attribute' => 'struktur.name',
            'label' => Yii::t('frontend', 'Struktur'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Struktur<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnStruktur = [
        ['attribute' => 'id', 'visible' => false],
        'root',
        'lft',
        'rgt',
        'lvl',
        'name',
        'icon',
        'icon_type',
        'active',
        'selected',
        'disabled',
        'readonly',
        'visible',
        'collapsed',
        'movable_u',
        'movable_d',
        'movable_l',
        'movable_r',
        'removable',
        'removable_all',
        'child_allowed',
        'kode',
        'jabatan_id',
        'fungsi_jabatan_id',
        'tipe',
    ];
    echo DetailView::widget([
        'model' => $model->struktur,
        'attributes' => $gridColumnStruktur    ]);
    ?>
</div>

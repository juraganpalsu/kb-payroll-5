<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Departemen */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Departemen',
]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Departemen'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="departemen-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

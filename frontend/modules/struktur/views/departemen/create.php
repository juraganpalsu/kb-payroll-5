<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Departemen */

$this->title = Yii::t('frontend', 'Create Departemen');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Departemen'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departemen-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

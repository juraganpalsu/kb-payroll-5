<?php

/**
 * Created by PhpStorm.
 * File Name: _history-jumlah.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 26/12/20
 * Time: 17.32
 */

use kartik\grid\GridView;
use yii\data\ActiveDataProvider;


/**
 * @var ActiveDataProvider $dataProviderMppJumlahHistory
 */


?>

<div class="row">
    <div class="col-sm-12">
        <?php
        $gridColumnMppJumlah = [
            ['class' => 'yii\grid\SerialColumn'],
            'tahun',
            'jumlah',
            'keterangan:ntext',
            ['attribute' => 'lock', 'visible' => false],
        ];

        try {
            echo GridView::widget([
                'dataProvider' => $dataProviderMppJumlahHistory,
                'columns' => $gridColumnMppJumlah,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
                'export' => [
                    'label' => 'Page',
                    'fontAwesome' => true,
                ],
                'beforeHeader' => [
                    [
                        'columns' => [
                            [
                                'content' => Yii::t('frontend', 'History update data.'),
                                'options' => [
                                    'colspan' => 5,
                                    'class' => 'text-center warning',
                                ]
                            ],
                        ],
                        'options' => ['class' => 'skip-export']
                    ]
                ],
                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'hover' => true,
                'showPageSummary' => false,
                'persistResize' => false,
                'tableOptions' => ['class' => 'small']
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>
    </div>
</div>

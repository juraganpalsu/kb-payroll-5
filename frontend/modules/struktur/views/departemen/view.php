<?php

use frontend\modules\personalia\models\Mpp;
use frontend\modules\personalia\models\MppJumlah;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Departemen */
/**
 * @var ActiveDataProvider $dataProviderMpp
 * @var ActiveDataProvider $dataProviderMppJumlah
 */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Departemen'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.delete-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);
?>
    <div class="departemen-view">

        <div class="row">
            <div class="col-sm-10">

                <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?php
                if ($dataProviderMpp->count == 0) {
                    echo Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]);
                }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    'nama',
                    'kode',
                    'catatan:ntext',
                ];
                try {
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= Html::a(Yii::t('frontend', 'Konfigurasi MPP'), ['add-golongan-jabatan', 'id' => $model->id], ['class' => 'btn btn-primary call-modal-btn btn-xs', 'data' => ['header' => Yii::t('frontend', 'Menambahkan Jabatan dan Golongan Pada Departemen')]]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php
                $gridColumnMpp = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'golongan_id',
                        'value' => function (Mpp $model) {
                            return $model->golongan->nama;
                        }
                    ],
                    [
                        'attribute' => 'jabatan_id',
                        'value' => function (Mpp $model) {
                            return $model->jabatan->nama;
                        }
                    ],
                    'keterangan:ntext',
                    ['attribute' => 'lock', 'visible' => false],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{add-mpp-jumlah} {update-golongan-jabatan} {delete-golongan-jabatan}',
                        'buttons' => [
                            'add-mpp-jumlah' => function ($url) {
                                return Html::a(Yii::t('frontend', 'Mpp'), $url, ['class' => 'btn btn-xs btn-success btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Tambahkan jumlah untuk tahun.')]]);
                            },
                            'update-golongan-jabatan' => function ($url, Mpp $model) {
                                if ($model->mppJumlahs) {
                                    return '';
                                }
                                return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Update Fungsi Jabatan')]]);
                            },
                            'delete-golongan-jabatan' => function ($url, Mpp $model) {
                                if ($model->mppJumlahs) {
                                    return '';
                                }
                                return Html::a('Delete', $url, ['class' => 'btn btn-xs btn-danger btn-flat delete-btn', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?')]]);

                            }
                        ],
                    ],
                ];

                try {
                    echo GridView::widget([
                        'dataProvider' => $dataProviderMpp,
                        'columns' => $gridColumnMpp,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php
                $gridColumnMppJumlah = [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'width' => '50px',
                        'value' => function () {
                            return GridView::ROW_COLLAPSED;
                        },
                        'enableRowClick' => true,
                        'detailUrl' => Url::to(['tampilkan-history-jumlah']),
                        'headerOptions' => ['class' => 'kartik-sheet-style'],
                        'expandOneOnly' => true
                    ],
                    ['attribute' => 'id', 'visible' => false], [
                        'label' => Yii::t('frontend', 'Golongan'),
                        'value' => function (MppJumlah $model) {
                            return $model->mpp->golongan->nama;
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Jabatan'),
                        'value' => function (MppJumlah $model) {
                            return $model->mpp->jabatan->nama;
                        }
                    ],
                    'tahun',
                    'jumlah',
                    [
                        'label' => Yii::t('frontend', 'Jumlah Aktual'),
                        'value' => function (MppJumlah $model) {
                            return $model->mppAct ? $model->mppAct->jumlah : '';
                        }
                    ],
                    'keterangan:ntext',
                    ['attribute' => 'lock', 'visible' => false],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update-mpp-jumlah} {delete-mpp-jumlah}',
                        'buttons' => [
                            'update-mpp-jumlah' => function ($url) {
                                return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Update Fungsi Jabatan')]]);
                            },
                            'delete-mpp-jumlah' => function ($url) {
                                return Html::a('Delete', $url, ['class' => 'btn btn-xs btn-danger btn-flat delete-btn', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?')]]);
                            }
                        ],
                    ],
                ];

                try {
                    echo GridView::widget([
                        'dataProvider' => $dataProviderMppJumlah,
                        'columns' => $gridColumnMppJumlah,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } ?>
            </div>
        </div>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
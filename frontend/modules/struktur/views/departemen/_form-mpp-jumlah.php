<?php

/**
 * Created by PhpStorm.
 * File Name: _form-mpp-jumlah.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 20/12/20
 * Time: 21.36
 */


/**
 *
 * @var MppJumlah $model
 * @var ActiveForm $form
 * @var View $this
 */

use common\components\Bulan;
use frontend\modules\personalia\models\Jabatan;
use frontend\modules\personalia\models\MppJumlah;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$js = <<< JS
    $(function() {
        $('#form-input').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    });
JS;

$this->registerJs($js);

?>

<div class="departemen-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-input',
        'action' => $model->isNewRecord ? Url::to(['add-mpp-jumlah', 'id' => $model->mpp_id]) : Url::to(['update-mpp-jumlah', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $model->isNewRecord ? Url::to(['mpp-jumlah-validation']) : Url::to(['mpp-jumlah-validation', 'id' => $model->id]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'mpp_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="row">
            <div class="col-sm-4">
                <?php
                echo $form->field($model, 'tahun')->dropDownList(Bulan::_tahun());
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'jumlah')->textInput(['type' => 'number']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'keterangan')->textarea(); ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>
</div>


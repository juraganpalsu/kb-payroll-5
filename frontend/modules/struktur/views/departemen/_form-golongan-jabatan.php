<?php

/**
 * Created by PhpStorm.
 * File Name: _form-golongan-jabatan.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 20/12/20
 * Time: 13.14
 */

/**
 *
 * @var Mpp $model
 * @var ActiveForm $form
 * @var View $this
 */

use common\models\Golongan;
use frontend\modules\personalia\models\Jabatan;
use frontend\modules\personalia\models\Mpp;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$js = <<< JS
    $(function() {
        $('#form-input').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    });
JS;

$this->registerJs($js);

?>

<div class="departemen-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-input',
        'action' => $model->isNewRecord ? Url::to(['add-golongan-jabatan', 'id' => $model->departemen_id]) : Url::to(['update-golongan-jabatan', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => $model->isNewRecord ? Url::to(['golongan-jabatan-validation']) : Url::to(['golongan-jabatan-validation', 'id' => $model->id]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'departemen_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="row">
            <div class="col-sm-5">
                <?php
                echo $form->field($model, 'golongan_id')->widget(Select2::class, [
                    'options' => ['placeholder' => Yii::t('frontend', 'Select Golongan')],
                    'data' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-7">
                <?= $form->field($model, 'jabatan_id')->widget(Select2::class, [
                    'options' => ['placeholder' => Yii::t('frontend', 'Select Jabatan')],
                    'data' => ArrayHelper::map(\frontend\modules\struktur\models\Jabatan::find()->orderBy('id')->all(), 'id', 'nama'),
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <?= $form->field($model, 'keterangan')->textarea(); ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>
</div>

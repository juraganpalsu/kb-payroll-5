<?php
/**
 * Created by PhpStorm
 *
 * File Name: _form.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 03/07/19
 * Time: 22:23
 *
 *
 * @var Struktur $node
 * @var ActiveForm $form
 *
 */

use common\components\Status;
use frontend\modules\struktur\models\Departemen;
use frontend\modules\struktur\models\Direktur;
use frontend\modules\struktur\models\Divisi;
use frontend\modules\struktur\models\FungsiJabatan;
use frontend\modules\struktur\models\Jabatan;
use frontend\modules\struktur\models\Perusahaan;
use frontend\modules\struktur\models\Section;
use frontend\modules\struktur\models\Struktur;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>

<?php try { ?>

    <div class="row">
        <div class="col-md-10">
            <?php
            echo $form->field($node, 'perusahaan_id')->dropDownList(ArrayHelper::map(Perusahaan::find()->orderBy('nama ASC')->all(), 'id', 'nama'), ['prompt' => '-']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?php
            echo $form->field($node, 'direktur_id')->dropDownList(ArrayHelper::map(Direktur::find()->orderBy('nama ASC')->all(), 'id', 'nama'), ['prompt' => '-']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?php
            echo $form->field($node, 'divisi_id')->dropDownList(ArrayHelper::map(Divisi::find()->orderBy('nama ASC')->all(), 'id', 'nama'), ['prompt' => '-']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?php
            echo $form->field($node, 'departemen_id')->dropDownList(ArrayHelper::map(Departemen::find()->orderBy('nama ASC')->all(), 'id', 'nama'), ['prompt' => '-']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?php
            echo $form->field($node, 'section_id')->label(Yii::t('frontend', 'Sub Dept Id'))->dropDownList(ArrayHelper::map(Section::find()->orderBy('nama ASC')->all(), 'id', 'nama'), ['prompt' => '-']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?php
            echo $form->field($node, 'jabatan_id')->dropDownList(ArrayHelper::map(Jabatan::find()->orderBy('nama ASC')->all(), 'id', 'nama'), ['prompt' => '-']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?php
            echo $form->field($node, 'fungsi_jabatan_id')->dropDownList(ArrayHelper::map(FungsiJabatan::find()->orderBy('nama ASC')->all(), 'id', 'nama'), ['prompt' => '-']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <?php
            echo $form->field($node, 'kode', ['template' => '{input}'])->hiddenInput();
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php
            $approvalStatus = Status::approvalStatus(0, true);
            unset($approvalStatus[Status::DEFLT]);
            unset($approvalStatus[Status::SKIP]);
            unset($approvalStatus[Status::CANCEL]);
            echo $form->field($node, 'auto_approve')->dropDownList($approvalStatus);
            ?>
        </div>
        <div class="col-md-4">
            <?php
            echo $form->field($node, 'durasi_auto_approve_min', [
                'addon' => [
                    'append' => ['content' => Yii::t('frontend', 'Hari')]
                ]
            ])->textInput(['type' => 'number']);
            ?>
        </div>
        <div class="col-md-4">
            <?php
            echo $form->field($node, 'durasi_auto_approve', [
                'addon' => [
                    'append' => ['content' => Yii::t('frontend', 'Hari')]
                ]
            ])->textInput(['type' => 'number']);
            ?>
        </div>
    </div>
    <?php
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
}

?>
<?php

use frontend\modules\struktur\models\Struktur;
use yii\helpers\Html;

/**
 * Created by PhpStorm
 *
 * File Name: _kelola.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 03/07/19
 * Time: 20:23
 *
 *
 *
 *
 * @var Struktur $node
 *
 */

?>

<div class="row">
    <div class="col-md-12">
        <?= Html::a(Yii::t('frontend', 'Manage Struktur'), ['/struktur/struktur/kelola', 'id' => $node->id], ['class' => 'btn btn-primary btn-xs btn-block btn-flat', 'target' => '_blank']) ?>
    </div>
</div>

<?php

use frontend\modules\struktur\models\ModulApproval;
use frontend\modules\struktur\models\Struktur;
use frontend\modules\struktur\models\StrukturApproval;
use kartik\tree\models\TreeQuery;
use kartik\tree\TreeViewInput;
use yii\helpers\Url;

/**
 * Created by PhpStorm
 *
 * File Name: _kelola.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 03/07/19
 * Time: 20:23
 *
 *
 *
 *
 * @var Struktur $model
 * @var array $modulApproval
 * @var TreeQuery $parents
 *
 */


$this->title = Yii::t('frontend', 'Setting Level Approval {struktur}', ['struktur' => $model->nameCostume]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Struktur'), 'url' => ['/struktur/struktur']];
$this->params['breadcrumbs'][] = $this->title;

$urlCreate = Url::to(['buat-rute']);
$urlHapus = Url::to(['hapus-rute']);


$js = <<<JS
$(function() {    
    $(".modul_approval").on('treeview:checked', function(event, key) {
        var modul_approval_id = $(this).data('modulid');
        var struktur_id = $(this).data('strukturid');
        var struktur_atasan_id = $(this);
        function buatrute() {
            $.ajax({
                url: '$urlCreate',
                type: 'POST',
                data: {struktur_atasan_id: struktur_atasan_id.val(), modul_approval_id: modul_approval_id, struktur_id : struktur_id},
                success: function (data) {
                    return true;
                }
            });
        }
        setTimeout(buatrute, 100);
    }).on('treeview:unchecked', function(event, key) {
        var modul_approval_id = $(this).data('modulid');
        var struktur_id = $(this).data('strukturid');
        var struktur_atasan_id = $(this);
        function deleteRoute() {
            $.ajax({
                url: '$urlHapus',
                type: 'POST',
                data: {struktur_atasan_id: struktur_atasan_id.val(), modul_approval_id: modul_approval_id, struktur_id : struktur_id},
                success: function (data) {
                    return true;
                }
            });
        }
        setTimeout(deleteRoute, 100);
    });
})
JS;
$this->registerJs($js);

?>
<div class="row">
    <?php
    /** @var ModulApproval $modul */
    foreach ($modulApproval as $modul) {
        ?>
        <div class="col-sm-6" style=" margin-bottom: 10px">
            <?php
            try {
                echo TreeViewInput::widget([
                    'name' => 'strukturApprovalInput' . $modul->id,
                    'value' => StrukturApproval::getApprovalAktif($model->id, $modul->id),
                    'query' => $parents,
                    'headingOptions' => ['label' => $modul->nama],
                    'rootOptions' => ['label' => '<i class="fas fa-tree text-success"></i>'],
                    'fontAwesome' => true,
                    'asDropdown' => false,
                    'multiple' => true,
                    'nodeLabel' => function (Struktur $node) {
                        return $node->nameCostume;
                    },
                    'options' => ['disabled' => false, 'id' => 'modul' . $modul->id, 'class' => 'modul_approval', 'data' => ['modulid' => $modul->id, 'strukturid' => $model->id]],
                    'treeOptions' => ['style' => 'height:200px']
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            } ?>
        </div>
        <?php
    }
    ?>
</div>

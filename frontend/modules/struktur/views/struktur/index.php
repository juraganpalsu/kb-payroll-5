<?php
/**
 * Created by PhpStorm
 *
 * File Name: index.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 01/07/19
 * Time: 23:38
 */

use frontend\modules\struktur\models\Struktur;
use kartik\tree\Module;
use kartik\tree\TreeView;
$this->title = Yii::t('frontend', 'Struktur');

$mainTemplate = <<<HTML
<div class="row">
    <div class="col-sm-6">
        {wrapper}
    </div>
    <div class="col-sm-6">
        {detail}
    </div>
</div>
HTML;

try {
    echo TreeView::widget([
        // single query     fetch to render the tree
        // use the Struktur model you have in the previous step
        'query' => Struktur::find()->addOrderBy('root, lft'),
        'headingOptions' => ['label' => Yii::t('frontend', 'Struktur')],
        'topRootAsHeading' => true,
        'fontAwesome' => true,     // optional
        'isAdmin' => false,         // optional (toggle to enable admin mode)
        'displayValue' => 1,        // initial display value
        'softDelete' => true,       // defaults to true
        'cacheSettings' => [
            'enableCache' => false   // defaults to true
        ],
        'showIDAttribute' => false,
        'iconEditSettings' => [
            'show' => 'list',
            'listData' => [
                'file' => 'File',
                'bell' => 'Bell',
            ]
        ],
        'nodeLabel' => function (Struktur $node) {
            return $node->nameCostume;
        },
        'nodeAddlViews' => [
            Module::VIEW_PART_2 => '@frontend/modules/struktur/views/struktur/_form',
            Module::VIEW_PART_5 => '@frontend/modules/struktur/views/struktur/_kelola',
        ],
        'mainTemplate' => $mainTemplate,
        'treeOptions' => ['style' => 'height:600px'],
    ]);
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
}
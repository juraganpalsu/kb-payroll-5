<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Direktur */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Direktur',
]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Direktur'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="direktur-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

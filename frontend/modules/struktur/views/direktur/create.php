<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Direktur */

$this->title = Yii::t('frontend', 'Create Direktur');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Direktur'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="direktur-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

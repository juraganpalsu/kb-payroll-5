<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\FungsiJabatan */

$this->title = Yii::t('frontend', 'Create Fungsi Jabatan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Fungsi Jabatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fungsi-jabatan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\FungsiJabatan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Fungsi Jabatan',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Fungsi Jabatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['index', 'JabatanSearch' => ['nama' => $model->nama]]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="fungsi-jabatan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

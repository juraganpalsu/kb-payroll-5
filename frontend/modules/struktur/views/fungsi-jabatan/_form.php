<?php

use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\FungsiJabatan */
/* @var $form yii\widgets\ActiveForm */


$js = <<< JS
$(function() {
    $('#form-fungsi-jabatan').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]);
?>

<div class="fungsi-jabatan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-fungsi-jabatan',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('nama')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'kode')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('Kode')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Corporate */

$this->title = Yii::t('frontend', 'Create Corporate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Corporate'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="corporate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

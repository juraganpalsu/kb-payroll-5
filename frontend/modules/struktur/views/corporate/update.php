<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\struktur\models\Corporate */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Corporate',
]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Corporate'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="corporate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

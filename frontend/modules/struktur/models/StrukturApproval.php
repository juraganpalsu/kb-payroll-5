<?php

namespace frontend\modules\struktur\models;

use frontend\modules\struktur\models\base\StrukturApproval as BaseStrukturApproval;
use Throwable;
use Yii;

/**
 * This is the model class for table "struktur_approval".
 */
class StrukturApproval extends BaseStrukturApproval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['struktur_id', 'struktur_atasan_id', 'modul_approval_id'], 'required'],
            [['struktur_id', 'struktur_atasan_id', 'modul_approval_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'deleted_by'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }


    /**
     * @return bool
     */
    public function buatRute()
    {
        $struktur_atasan_ids = explode(',', $this->struktur_atasan_id);
        if (!empty($struktur_atasan_ids)) {
            foreach ($struktur_atasan_ids as $atasan_id) {
                $model = new StrukturApproval();
                $model->setAttributes($this->attributes);
                $model->struktur_atasan_id = $atasan_id;

                $query = self::find()->where('struktur_id =:struktur_id AND struktur_atasan_id =:struktur_atasan_id AND modul_approval_id =:modul_approval_id',
                    [':struktur_id' => $model->struktur_id, ':struktur_atasan_id' => $model->struktur_atasan_id, ':modul_approval_id' => $model->modul_approval_id])
                    ->one();
                if ($query) {
                    /** @var StrukturApproval $model */
                    $model = $query;
                    $model->deleted_by = 0;
                }
                $model->save();
            }
            return true;

        }
        return false;
    }

    /**
     * @return bool
     * @throws Throwable
     */
    public function hapusRute()
    {
        $query = self::find()->andWhere('struktur_id =:struktur_id AND modul_approval_id =:modul_approval_id',
            [':struktur_id' => $this->struktur_id, ':modul_approval_id' => $this->modul_approval_id])->all();
        $parenIsReady = array_map(function ($data) {
            return $data['struktur_atasan_id'];
        }, $query);
        $strukturAtasanIds = array_diff($parenIsReady, explode(',', $this->struktur_atasan_id));
        if (!empty($strukturAtasanIds)) {
            try {
                foreach ($strukturAtasanIds as $strukturAtasanId) {
                    $query = self::find()->andWhere('struktur_id =:struktur_id AND struktur_atasan_id =:struktur_atasan_id AND modul_approval_id =:modul_approval_id',
                        [':struktur_id' => $this->struktur_id, ':struktur_atasan_id' => $strukturAtasanId, ':modul_approval_id' => $this->modul_approval_id])
                        ->one();
                    if ($query) {
                        $query->delete();
                    }
                }
                return true;
            } catch (Throwable $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        return false;
    }

    /**
     *
     * Mengambil data approval
     *
     * @param int $strukturid
     * @param int $modulid
     * @param bool $asArray
     * @return string|array aktive route
     */
    public static function getApprovalAktif(int $strukturid, int $modulid, bool $asArray = false)
    {
        $query = self::find()
            ->joinWith('strukturAtasan')
            ->andWhere('struktur_id =:struktur_id AND modul_approval_id =:modul_approval_id',
            [':struktur_id' => $strukturid, ':modul_approval_id' => $modulid])
            ->orderBy('struktur.lvl ASC')
            ->all();
        $approvalAktif = '';
        if (!empty($query)) {
            $getApprovalAktif = array_map(function ($data) {
                return $data['struktur_atasan_id'];
            }, $query);
            $approvalAktif = implode(',', $getApprovalAktif);
            if ($asArray) {
                $approvalAktif = $getApprovalAktif;
            }
        }
        return $approvalAktif;
    }


}

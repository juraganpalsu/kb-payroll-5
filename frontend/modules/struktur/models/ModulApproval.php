<?php

namespace frontend\modules\struktur\models;

use frontend\modules\struktur\models\base\ModulApproval as BaseModulApproval;

/**
 * This is the model class for table "modul_approval".
 */
class ModulApproval extends BaseModulApproval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['nama'], 'string', 'max' => 45],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }


}

<?php

/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 6/8/19
 * Time: 9:34 PM
 */

namespace frontend\modules\struktur\models;

use common\components\Status;
use frontend\models\User;
use Yii;
use yii\base\Model;

class Helper extends Model
{

    /**
     *
     * Mengambil urutan data approval berdasarkan struktur approval yg telah diset sebelumnya
     *
     *
     * @param int $userId User ID Pembuat request
     * @param int $modulId Approval Modul ID
     * @return array urut dari jabatan tertinggi ke jabatan terendah
     */
    public static function getRuteApproval(int $userId, int $modulId)
    {
        $getRuteApproval = [];
        if ($modelUser = User::findOne($userId)) {
            $approvalAktif = StrukturApproval::getApprovalAktif($modelUser->pegawai->pegawaiStruktur->struktur_id, $modulId, true);
            if (!empty($approvalAktif)) {
                foreach ($approvalAktif as $approval) {
                    if ($modelStruktur = Struktur::findOne($approval)) {
                        if ($modelStruktur->pegawaiStrukturs) {
                            foreach ($modelStruktur->pegawaiStrukturs as $pegawaiStruktur) {
                                if ($pegawai = $pegawaiStruktur->pegawai) {
                                    if ($userPegawai = $pegawai->userPegawai) {
                                        if ($pegawai->perjanjianKerja && $pegawai->pegawaiStruktur) {
                                            $getRuteApproval[] = ['struktur_id' => $pegawaiStruktur->struktur_id, 'level' => $pegawaiStruktur->struktur->lvl,
                                                'user_id' => $userPegawai->user_id, 'username' => $userPegawai->user->username, 'pegawai_id' => $pegawai->id,
                                                'nama_struktur' => $pegawaiStruktur->struktur->nameCostume, 'email' => $userPegawai->user->email,
                                                'perjanjian_kerja_id' => $pegawai->perjanjianKerja->id, 'pegawai_struktur_id' => $pegawaiStruktur->id,
                                                'token' => Yii::$app->security->maskToken($userPegawai->user->auth_key), 'durasi_hmin' => $pegawaiStruktur->struktur->durasi_auto_approve_min
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
//                $getRuteApproval = array_filter($getRuteApproval, function ($x) {
//                    return (!empty($x));
//                });
//                usort($getRuteApproval, function ($a, $b) {
//                    return $a['level'] <=> $b['level'];
//                });
            }
        }
        return $getRuteApproval;
    }

}

<?php


namespace frontend\modules\struktur\models\search;


use common\models\Absensi;
use frontend\models\Pegawai;
use frontend\modules\struktur\models\Struktur;
use yii\data\ActiveDataProvider;

/**
 * Created by PhpStorm.
 * File Name: StrukturSearch.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-07-25
 * Time: 4:24 PM
 */
class StrukturSearch extends Struktur
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['jabatan_id', 'fungsi_jabatan_id', 'perusahaan_id', 'direktur_id', 'divisi_id', 'departemen_id', 'section_id'], 'required'];
        $rules[] = [['kode'], 'string', 'max' => 20];
        $rules[] = [['perusahaan_id', 'direktur_id', 'divisi_id', 'departemen_id', 'section_id', 'fungsi_jabatan_id', 'jabatan_id'], 'string', 'max' => 32];
        $rules[] = [['kode', 'jabatan_id', 'name'], 'safe'];
        $rules[] = [['tipe', 'corporate_id', 'auto_approve', 'durasi_auto_approve', 'durasi_auto_approve_min'], 'default', 'value' => 0];
        return $rules;
    }


    /**
     * @param int $strukturId
     * @return int
     */
    public static function getDataAlfaPerDepartemen(int $strukturId)
    {
        $queryPegawai = Pegawai::find()->joinWith('pegawaiStruktur')->orderBy('pegawai.nama_lengkap ASC')->andWhere(['pegawai_struktur.struktur_id' => $strukturId])->all();
//        $dataStruktur = [];
        $alfa = 0;
        /** @var Pegawai $pegawai */
        foreach ($queryPegawai as $pegawai) {
            $hMinSatu = date('Y-m-d', strtotime('-1 day'));
            $hMinEmpat = date('Y-m-d', strtotime('-4 day'));
            $queryAlfa = \frontend\models\Absensi::find()->andWhere(['BETWEEN', 'tanggal', $hMinEmpat, $hMinSatu])->andWhere(['pegawai_id' => $pegawai->id, 'status_kehadiran' => Absensi::ALFA])->count();
            $alfa += $queryAlfa;
//            if ($queryAlfa == 0) {
//                continue;
//            }
//            $dataStruktur[$pegawai->pegawaiStruktur->struktur_id]['struktur'] = [
//                'id' => $pegawai->pegawaiStruktur->struktur_id,
//                'nama_struktur' => $pegawai->pegawaiStruktur->struktur->nameCostume,
//            ];
//            $dataStruktur[$pegawai->pegawaiStruktur->struktur_id]['alfa'][] = $queryAlfa;
        }

//        $arrayDataProvider = [];
//        foreach ($dataStruktur as $data) {
//            $arrayDataProvider[] = [
//                'id' => $data['struktur']['id'],
//                'nama' => $data['struktur']['nama_struktur'],
//                'jumlah_alfa' => array_sum($data['alfa'])
//            ];
//        }
//        return $arrayDataProvider;

//        return $dataStruktur;
        return $alfa;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = Struktur::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        $query->orderBy('name asc');

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }


}
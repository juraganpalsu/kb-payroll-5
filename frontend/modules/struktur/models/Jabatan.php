<?php

namespace frontend\modules\struktur\models;

use Yii;
use \frontend\modules\struktur\models\base\Jabatan as BaseJabatan;

/**
 * This is the model class for table "jabatan".
 */
class Jabatan extends BaseJabatan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'kode'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_org_chart', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['kode'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

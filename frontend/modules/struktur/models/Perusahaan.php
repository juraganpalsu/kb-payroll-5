<?php

namespace frontend\modules\struktur\models;

use frontend\modules\struktur\models\base\Perusahaan as BasePerusahaan;

/**
 * This is the model class for table "perusahaan".
 */
class Perusahaan extends BasePerusahaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'kode'], 'required'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['kode'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

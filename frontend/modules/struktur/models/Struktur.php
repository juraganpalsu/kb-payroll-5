<?php

namespace frontend\modules\struktur\models;

use frontend\models\User;
use frontend\modules\pegawai\models\PegawaiStruktur;
use kartik\tree\models\Tree;
use Yii;
use yii\db\ActiveQuery;

/**
 * Created by PhpStorm
 *
 * File Name: Struktur.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 01/07/19
 * Time: 23:26
 *
 * @property string $kode
 * @property string $jabatan_id
 * @property string $departemen_id
 * @property string $section_id
 * @property int $tipe
 * @property Jabatan $jabatan
 * @property Departemen $departemen
 * @property Section $section
 * @property FungsiJabatan $fungsiJabatan
 * @property string $nameCostume
 * @property integer $auto_approve
 * @property integer $durasi_auto_approve
 * @property integer $durasi_auto_approve_min
 *
 * @property array $_tipe
 * @property PegawaiStruktur[] $pegawaiStrukturs
 *
 */
class Struktur extends Tree
{

    const ADMINISTRATOR = 1;
    const DIREKSI = 2;
    const DIVISI = 3;
    const DEPARTEMEN = 4;
    const SEKSI = 5;
    const FUNGSI = 6;

    public $_tipe = [self::ADMINISTRATOR => 'ADMINISTRATOR', self::DIREKSI => 'UNIT',
        self::DIVISI => 'DIVISI', self::DEPARTEMEN => 'DEPARTEMEN', self::SEKSI => 'SECTION', self::FUNGSI => 'FUNGSI'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'struktur';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['jabatan_id', 'fungsi_jabatan_id', 'perusahaan_id', 'direktur_id', 'divisi_id', 'departemen_id', 'section_id'], 'required'];
        $rules[] = [['kode'], 'string', 'max' => 20];
        $rules[] = [['perusahaan_id', 'direktur_id', 'divisi_id', 'departemen_id', 'section_id', 'fungsi_jabatan_id', 'jabatan_id'], 'string', 'max' => 32];
        $rules[] = [['kode', 'jabatan_id'], 'safe'];
        $rules[] = [['tipe', 'corporate_id', 'auto_approve', 'durasi_auto_approve', 'durasi_auto_approve_min'], 'default', 'value' => 0];
        return $rules;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['jabatan_id'] = Yii::t('frontend', 'Jabatan');
        $attributeLabels['fungsi_jabatan_id'] = Yii::t('frontend', 'Fungsi Jabatan');
        $attributeLabels['auto_approve'] = Yii::t('frontend', 'Auto Approve');
        $attributeLabels['durasi_auto_approve_min'] = Yii::t('frontend', 'Durasi(H-)');
        $attributeLabels['durasi_auto_approve'] = Yii::t('frontend', 'Durasi(H+)');
        return $attributeLabels;
    }

    /**
     * Override isDisabled method if you need as shown in the
     * example below. You can override similarly other methods
     * like isActive, isMovable etc.
     */
    public function isDisabled()
    {
        /** @var User $identiry */
//        $identiry = Yii::$app->user->identity;
//        if ($identiry->username !== 'jiwandaru') {
//            return true;
//        }
//        return parent::isDisabled();
        return false;
    }

    /**
     * @return bool
     */
    public function isChildAllowed()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getKodeParents()
    {
        $parents = $this->parents()->all();
        $arrayKodeParents = [];
        if (!empty($parents)) {
            /** @var Struktur $parent */
            foreach ($parents as $parent) {
                if ($parent->isRoot()) {
                    continue;
                }
                $arrayKodeParents[] = $parent->kode;
            }
        }
        return implode('/', $arrayKodeParents);
    }

    /**
     * @return ActiveQuery
     */
    public function getJabatan()
    {
        return $this->hasOne(Jabatan::class, ['id' => 'jabatan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFungsiJabatan()
    {
        return $this->hasOne(FungsiJabatan::class, ['id' => 'fungsi_jabatan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDepartemen()
    {
        return $this->hasOne(Departemen::class, ['id' => 'departemen_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::class, ['id' => 'section_id']);
    }

    /**
     * @return string
     */
    public function getNameCostume()
    {
        if ($this->jabatan) {
            return $this->name . ' [' . $this->jabatan->kode . '] ';
        }
        return $this->name;
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiStrukturs()
    {
        return $this->hasMany(PegawaiStruktur::class, ['struktur_id' => 'id']);
    }


}
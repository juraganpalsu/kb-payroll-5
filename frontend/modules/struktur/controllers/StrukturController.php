<?php

/**
 * Created by PhpStorm
 *
 * File Name: StrukturController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 01/07/19
 * Time: 23:37
 */


namespace frontend\modules\struktur\controllers;


use frontend\models\Pegawai;
use frontend\models\User;
use frontend\modules\struktur\models\ModulApproval;
use frontend\modules\struktur\models\Struktur;
use frontend\modules\struktur\models\StrukturApproval;
use Throwable;
use Yii;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class StrukturController extends Controller
{
    /**
     *
     * Menejemen struktur organisasi
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $id
     * @return Struktur|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Struktur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    public function actionGetSelect2()
    {

    }

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionKelola(int $id)
    {
        try {
            $model = $this->findModel($id);
            $modulApproval = ModulApproval::find()->all();
            $parents = $model->parents();

            /** @var User $user */
            $user = Yii::$app->user->identity;
            if (!$user->validasiKelengkapanAkun()) {
                return $this->render('@frontend/modules/pegawai/views/pegawai/perhatian');
            }
            return $this->render('kelola', ['model' => $model, 'modulApproval' => $modulApproval, 'parents' => $parents]);
        } catch (NotFoundHttpException $e) {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionBuatRute()
    {
        $model = new StrukturApproval();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response->data = ['status' => false, 'message' => Yii::t('frontend', 'Gagal membuat rute'), 'data' => ''];
            $model->setAttributes($request->post());
            if ($model->buatRute()) {
                $response->data = ['status' => true, 'message' => Yii::t('frontend', 'Berhasil membuat rute'), 'data' => ''];
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('common', 'The requested not acceptable.'));
    }

    /**
     * @return array|mixed
     * @throws NotAcceptableHttpException
     * @throws Throwable
     */
    public function actionHapusRute()
    {
        $model = new StrukturApproval();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response->data = ['status' => false, 'message' => Yii::t('frontend', 'Gagal hapus rute'), 'data' => ''];
            $model->setAttributes($request->post());
            if ($model->hapusRute()) {
                $response->data = ['status' => true, 'message' => Yii::t('frontend', 'Berhasil hapus rute'), 'data' => ''];
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('common', 'The requested not acceptable.'));
    }


    /**
     * @return array
     */
    public function actionGetDivisiDepdrop()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($parents = $request->post('depdrop_parents')) {
            if ($parents != null) {
                $unit_id = $parents[0];
                $struktur = Struktur::findOne($unit_id);
                $out = $struktur->children()->select(['id', 'name'])->andWhere(['tipe' => Struktur::DIVISI])->asArray()->all();
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * @return array
     */
    public function actionGetDepartemenDepdrop()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        if ($parents = $request->post('depdrop_parents')) {
            $divisi_id = empty($parents[0]) ? null : $parents[0];
            if ($divisi_id != null) {
                $struktur = Struktur::findOne($divisi_id);
                $out = $struktur->children()->select(['id', 'name'])->andWhere(['tipe' => Struktur::DEPARTEMEN])->asArray()->all();
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }
}
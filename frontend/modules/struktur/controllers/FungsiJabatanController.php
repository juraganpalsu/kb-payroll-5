<?php

namespace frontend\modules\struktur\controllers;

use frontend\modules\struktur\models\FungsiJabatan;
use frontend\models\search\FungsiJabatanSearch;
use kartik\widgets\ActiveForm;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * FungsiJabatanController implements the CRUD actions for FungsiJabatan model.
 */
class FungsiJabatanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FungsiJabatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FungsiJabatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FungsiJabatan model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new FungsiJabatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FungsiJabatan();

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => [],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('create', ['model' => $model]);
    }

    /**
     * Updates an existing FungsiJabatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => false,
            ];
            if ($model->load($request->post()) && $model->save()) {
                $response->data = [
                    'data' => [],
                    'message' => '',
                    'status' => true,
                ];
            }
            return $response->data;
        }

        return $this->renderAjax('update', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new FungsiJabatan();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing FungsiJabatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $model->delete();
            $response->data = [
                'data' => [],
                'message' => '',
                'status' => true,
            ];
            return $response->data;
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Finds the FungsiJabatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FungsiJabatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FungsiJabatan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }
}

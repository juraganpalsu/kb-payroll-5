<?php

namespace frontend\modules\struktur\controllers;

use frontend\modules\personalia\models\Mpp;
use frontend\modules\personalia\models\MppJumlah;
use frontend\modules\personalia\models\MppJumlahHistory;
use frontend\modules\struktur\models\Departemen;
use frontend\modules\struktur\models\search\DepartemenSearch;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * DepartemenController implements the CRUD actions for Departemen model.
 */
class DepartemenController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Departemen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DepartemenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Departemen model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $queryMpp = Mpp::find()->andWhere(['departemen_id' => $model->id]);
        $queryMppJumlah = MppJumlah::find()->joinWith(['mpp' => function (ActiveQuery $query) use ($model) {
            $query->andWhere(['departemen_id' => $model->id]);
        }])->orderBy('tahun DESC');

        $dataProviderMpp = new ActiveDataProvider([
            'query' => $queryMpp,
        ]);
        $dataProviderMppJumlah = new ActiveDataProvider([
            'query' => $queryMppJumlah,
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProviderMpp' => $dataProviderMpp,
            'dataProviderMppJumlah' => $dataProviderMppJumlah
        ]);
    }

    /**
     * Creates a new Departemen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Departemen();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Departemen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Departemen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Departemen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Departemen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Departemen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param $id
     * @return Mpp|null
     * @throws NotFoundHttpException
     */
    protected function findModelMpp($id)
    {
        if (($model = Mpp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param $id
     * @return MppJumlah|null
     * @throws NotFoundHttpException
     */
    protected function findModelMppJumlah($id)
    {
        if (($model = MppJumlah::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionAddGolonganJabatan(string $id)
    {
        $modelDepartemen = $this->findModel($id);
        $model = new Mpp();
        $model->departemen_id = $modelDepartemen->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->departemen_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-golongan-jabatan', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateGolonganJabatan(string $id)
    {
        $model = $this->findModelMpp($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->departemen_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-golongan-jabatan', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return array
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionGolonganJabatanValidation(string $id = ''): array
    {
        $model = new Mpp();
        if (!empty($id)) {
            $model = $this->findModelMpp($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteGolonganJabatan(string $id)
    {
        $model = $this->findModelMpp($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->departemen_id])], 'message' => Yii::t('frontend', 'Berhasil delete data.'), 'status' => true];

        }
        return $response->data;
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionAddMppJumlah(string $id)
    {
        $modelMpp = $this->findModelMpp($id);
        $model = new MppJumlah();
        $model->mpp_id = $modelMpp->id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->mpp->departemen_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-mpp-jumlah', ['model' => $model]);
    }


    /**
     * @param string $id
     * @return array
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionMppJumlahValidation(string $id = ''): array
    {
        $model = new MppJumlah();
        if (!empty($id)) {
            $model = $this->findModelMppJumlah($id);
        }
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateMppJumlah(string $id)
    {
        $model = $this->findModelMppJumlah($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->mpp->departemen_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-mpp-jumlah', ['model' => $model]);
    }

    /**
     * @param string $id
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteMppJumlah(string $id)
    {
        $model = $this->findModelMppJumlah($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->mpp->departemen_id])], 'message' => Yii::t('frontend', 'Berhasil delete data.'), 'status' => true];

        }
        return $response->data;
    }

    /**
     * @return string
     */
    public function actionTampilkanHistoryJumlah(): string
    {
        if (isset($_POST['expandRowKey'])) {
            $query = MppJumlahHistory::find()->andWhere(['mpp_jumlah_id' => $_POST['expandRowKey']])->orderBy('created_at DESC');
            $dataProviderMppJumlahHistory = new ActiveDataProvider([
                'query' => $query,
            ]);
            return $this->renderAjax('_history-jumlah', [
                'dataProviderMppJumlahHistory' => $dataProviderMppJumlahHistory
            ]);
        }
        return '<div class="alert alert-danger">' . Yii::t('frontend', 'No data found') . '</div>';
    }


}

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\CutiConf */

$this->title = Yii::t('frontend', 'Create Cuti Conf');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Cuti Conf'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuti-conf-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

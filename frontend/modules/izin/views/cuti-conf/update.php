<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\CutiConf */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Cuti Conf',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Cuti Conf'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="cuti-conf-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

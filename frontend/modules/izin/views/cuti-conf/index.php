<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\izin\models\search\CutiConfSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Helper;
use frontend\modules\izin\models\CutiConf;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Cuti Conf');
$this->params['breadcrumbs'][] = $this->title;


$js = <<< JS
$(function() {     
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
     
    $('.update-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.btn-update-all').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
})
JS;

$this->registerJs($js);
?>
    <div class="cuti-conf-index">
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'tahun',
            [
                'attribute' => 'akhir_berlaku',
                'value' => function (CutiConf $model) {
                    return Helper::idDate($model->akhir_berlaku);
                }
            ],
            'keterangan:ntext',
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {update-all}',
                'buttons' => [
                    'update' => function ($url, CutiConf $model) {
                        return Html::a(Yii::t('frontend', 'Update Data'), $url, ['class' => 'btn btn-xs btn-warning btn-flat call-modal-btn', 'data' => ['pjax' => 0, 'header' => Yii::t('frontend', 'Update Akhir Berlaku untuk tahun {tahun}', ['tahun' => $model->tahun])]]);

                    },
                    'update-all' => function ($url, CutiConf $model) {
                        return Html::a(Yii::t('frontend', 'Update Semua Pegawai'), $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-update-all', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan mengupdate akhir cuti tahun {tahun} dengan tanggal {tanggal} untuk semua pegawai?', ['tahun' => $model->tahun, 'tanggal' => Helper::idDate($model->akhir_berlaku)]), 'method' => 'post']]);
                    }
                ],
            ],
        ];
        ?>
        <?php
        try {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-cuti-conf']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    '{export}',
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
<?php

use kartik\widgets\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinJenis */
/* @var $form kartik\widgets\ActiveForm */


$js = <<< JS
$(function() {
    $('#form-izin-jenis').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);


?>

<div class="izin-jenis-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-izin-jenis',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'uang_makan')->widget(SwitchInput::class, [
                    'pluginOptions' => [
                        'handleWidth' => 30,
                        'onText' => Yii::t('frontend', 'Ya'),
                        'offText' => Yii::t('frontend', 'Tidak')
                    ]
                ]); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'uang_transport')->widget(SwitchInput::class, [
                    'pluginOptions' => [
                        'handleWidth' => 30,
                        'onText' => Yii::t('frontend', 'Ya'),
                        'offText' => Yii::t('frontend', 'Tidak')
                    ]
                ]); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'hitung_masuk')->widget(SwitchInput::class, [
                    'pluginOptions' => [
                        'handleWidth' => 30,
                        'onText' => Yii::t('frontend', 'Ya'),
                        'offText' => Yii::t('frontend', 'Tidak')
                    ]
                ]); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'potong_gaji')->widget(SwitchInput::class, [
                    'pluginOptions' => [
                        'handleWidth' => 30,
                        'onText' => Yii::t('frontend', 'Ya'),
                        'offText' => Yii::t('frontend', 'Tidak')
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
            </div>
        </div>


        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

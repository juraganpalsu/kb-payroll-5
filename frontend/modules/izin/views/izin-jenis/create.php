<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinJenis */

$this->title = Yii::t('frontend', 'Create Izin Jenis');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Jenis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-jenis-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

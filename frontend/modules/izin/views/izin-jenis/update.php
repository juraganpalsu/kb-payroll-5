<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinJenis */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Izin Jenis',
]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Jenis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="izin-jenis-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\izin\models\search\IzinKeluarSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $modelPerjanjiankerja PerjanjianKerja
 */

use frontend\modules\izin\models\IzinKeluar;
use frontend\modules\pegawai\models\Pegawai;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use common\models\Golongan;
use frontend\modules\pegawai\models\PerjanjianKerja;

$this->title = Yii::t('frontend', 'Izin Keluar');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="izin-keluar-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'id_pegawai',
            'label' => Yii::t('app', 'Id Pegawai'),
            'value' => function (IzinKeluar $model) {
                return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (IzinKeluar $model) {
                return $model->pegawai->nama_lengkap;
            },
        ],
        [
            'attribute' => 'golongan',
            'label' => Yii::t('frontend', 'Golongan'),
            'value' => function (IzinKeluar $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'busines_unit',
            'label' => Yii::t('frontend', 'Bisnis Unit'),
            'value' => function (IzinKeluar $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                        return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    }
                    return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $modelPerjanjiankerja->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'tanggal',
            'label' => Yii::t('frontend', 'Tanggal'),
            'value' => function (IzinKeluar $model) {
                return date('d-m-Y', strtotime($model->tanggal));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'kategori',
            'value' => function (IzinKeluar $model) {
                return $model->_kategori[$model->kategori];
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $searchModel->_kategori,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('kategori'), 'id' => 'grid-izin-keluar-search-shift']
        ],
        'jam_mulai',
        'jam_selesai',
        'keterangan',
        [
            'attribute' => 'created_by',
            'value' => function (IzinKeluar $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (IzinKeluar $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9],
            'noExportColumns' => [1, 10], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'izin-keluar' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-izin-keluar']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>
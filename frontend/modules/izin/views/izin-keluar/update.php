<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinKeluar */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Izin Keluar',
    ]) . ' ' . $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Keluar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="izin-keluar-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

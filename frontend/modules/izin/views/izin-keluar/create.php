<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinKeluar */

$this->title = Yii::t('frontend', 'Create Izin Keluar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Keluar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-keluar-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

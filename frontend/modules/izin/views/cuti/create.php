<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\Cuti */

$this->title = Yii::t('frontend', 'Create Cuti');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Cuti'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuti-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\Cuti */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Cuti',
    ]) . ' ' . $model->pegawai->namaIdPegawai;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Cuti'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->namaIdPegawai, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="cuti-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

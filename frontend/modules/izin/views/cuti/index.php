<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\izin\models\search\CutiSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $modelPerjanjiankerja PerjanjianKerja
 */

use common\components\Status;
use frontend\modules\izin\models\Cuti;
use frontend\modules\izin\models\Pegawai;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Golongan;
use frontend\modules\pegawai\models\PerjanjianKerja;

$this->title = Yii::t('frontend', 'Cuti');
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.batal-cuti-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);

?>
<div class="cuti-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'id_pegawai',
            'label' => Yii::t('app', 'Id Pegawai'),
            'value' => function (Cuti $model) {
                return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (Cuti $model) {
                return $model->pegawai->nama_lengkap;
            },
        ],
        [
            'attribute' => 'golongan',
            'label' => Yii::t('frontend', 'Golongan'),
            'value' => function (Cuti $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
            'hAlign' => 'center',
            'format' => 'raw'
        
        ],
        [
            'attribute' => 'busines_unit',
            'label' => Yii::t('frontend', 'Bisnis Unit'),
            'value' => function (Cuti $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                        return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    }
                    return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $modelPerjanjiankerja->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
            'hAlign' => 'center',
            'format' => 'raw'
        
        ],
        [
            'attribute' => 'tanggal_mulai',
            'label' => Yii::t('app', 'Tanggal Mulai'),
            'value' => function (Cuti $model) {
                return date('d-m-Y', strtotime($model->tanggal_mulai));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'tanggal_selesai',
            'label' => Yii::t('app', 'Tanggal Selesai'),
            'value' => function (Cuti $model) {
                return date('d-m-Y', strtotime($model->tanggal_selesai));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        'keterangan:ntext',
        [
            'attribute' => 'batal',
            'value' => function (Cuti $model) {
                $batal = 'Tidak';
                if ($model->batal) {
                    $batal = 'Ya';
                }
                return $batal;
            },
            'hidden' => true,
        ],
        [
            'attribute' => 'created_by',
            'value' => function (Cuti $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (Cuti $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{batal-cuti}',
            'buttons' => [
                'batal-cuti' => function ($url, Cuti $model) {
                    if ($model->batal == Status::TIDAK) {
                        return Html::a(Yii::t('frontend', 'Batalkan'), $url, ['class' => 'btn btn-xs btn-flat btn-success batal-cuti-btn btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan membatalkan cuti?')]]);
                    }
                    return Html::a(Yii::t('frontend', 'Dibatalkan'), '#', ['class' => 'btn btn-xs btn-flat btn-warning']);
                },
            ]
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9],
            'noExportColumns' => [1, 10], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'cuti' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-cuti']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

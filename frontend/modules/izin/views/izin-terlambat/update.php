<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinTerlambat */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Izin Terlambat',
]) . ' ' . $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Terlambat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="izin-terlambat-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

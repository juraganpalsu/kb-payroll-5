<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinTerlambat */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Terlambat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-terlambat-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Izin Terlambat').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'tanggal',
        'shift',
        'keterangan',
        [
            'attribute' => 'pegawai.id',
            'label' => Yii::t('frontend', 'Pegawai'),
        ],
        [
            'attribute' => 'perjanjianKerja.id',
            'label' => Yii::t('frontend', 'Perjanjian Kerja'),
        ],
        [
            'attribute' => 'pegawaiGolongan.id',
            'label' => Yii::t('frontend', 'Pegawai Golongan'),
        ],
        [
            'attribute' => 'pegawaiStruktur.id',
            'label' => Yii::t('frontend', 'Pegawai Struktur'),
        ],
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Pegawai<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPegawai = [
        ['attribute' => 'id', 'visible' => false],
        'nama_lengkap',
        'has_uploaded_to_att',
        'has_created_account',
        'nama_panggilan',
        'nama_ibu_kandung',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'status_pernikahan',
        'kewarganegaraan',
        'agama',
        'nomor_ktp',
        'nomor_kk',
        'no_telp',
        'no_handphone',
        'alamat_email',
        'ktp_provinsi_id',
        'ktp_kabupaten_kota_id',
        'ktp_kecamatan_id',
        'ktp_desa_id',
        'ktp_rw',
        'ktp_rt',
        'domisili_provinsi_id',
        'domisili_kabupaten_kota_id',
        'domisili_kecamatan_id',
        'domisili_desa_id',
        'domisili_rw',
        'domisili_rt',
        'domisili_alamat',
        'ktp_alamat',
        'catatan',
        'is_resign',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->pegawai,
        'attributes' => $gridColumnPegawai    ]);
    ?>
    <div class="row">
        <h4>PegawaiGolongan<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPegawaiGolongan = [
        ['attribute' => 'id', 'visible' => false],
        'aksi',
        [
            'attribute' => 'pegawai.id',
            'label' => Yii::t('frontend', 'Pegawai'),
        ],
        'golongan_id',
        'mulai_berlaku',
        'akhir_berlaku',
        'keterangan',
        'created_by_org_chart',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->pegawaiGolongan,
        'attributes' => $gridColumnPegawaiGolongan    ]);
    ?>
    <div class="row">
        <h4>PegawaiStruktur<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPegawaiStruktur = [
        ['attribute' => 'id', 'visible' => false],
        'aksi',
        [
            'attribute' => 'pegawai.id',
            'label' => Yii::t('frontend', 'Pegawai'),
        ],
        'struktur_id',
        'mulai_berlaku',
        'akhir_berlaku',
        'status',
        'keterangan',
        'created_by_org_chart',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->pegawaiStruktur,
        'attributes' => $gridColumnPegawaiStruktur    ]);
    ?>
    <div class="row">
        <h4>PerjanjianKerja<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPerjanjianKerja = [
        ['attribute' => 'id', 'visible' => false],
        'aksi',
        'jenis_perjanjian',
        'kontrak',
        'id_pegawai',
        'mulai_berlaku',
        'akhir_berlaku',
        'status',
        'dasar_masa_kerja',
        'tanggal_resign',
        [
            'attribute' => 'pegawai.id',
            'label' => Yii::t('frontend', 'Pegawai'),
        ],
        'keterangan',
        'created_by_org_chart',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->perjanjianKerja,
        'attributes' => $gridColumnPerjanjianKerja    ]);
    ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinTerlambat */

$this->title = Yii::t('frontend', 'Create Izin Terlambat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Terlambat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-terlambat-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

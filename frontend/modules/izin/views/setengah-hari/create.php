<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\SetengahHari */

$this->title = 'Create Setengah Hari (Khusus Harian BCA)';
$this->params['breadcrumbs'][] = ['label' => 'Setengah Haris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setengah-hari-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

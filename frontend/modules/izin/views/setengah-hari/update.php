<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\SetengahHari */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Absensi Tidak Lengkap',
    ]) . ' ' . $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => 'Setengah Hari', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="setengah-hari-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

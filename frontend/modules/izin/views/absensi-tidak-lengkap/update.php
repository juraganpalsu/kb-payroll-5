<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\AbsensiTidakLengkap */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Absensi Tidak Lengkap',
    ]) . ' ' . $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Absensi Tidak Lengkap'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="absensi-tidak-lengkap-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

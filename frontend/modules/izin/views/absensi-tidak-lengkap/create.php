<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\AbsensiTidakLengkap */

$this->title = Yii::t('frontend', 'Create Absensi Tidak Lengkap');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Absensi Tidak Lengkap'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absensi-tidak-lengkap-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

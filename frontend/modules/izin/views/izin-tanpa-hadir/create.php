<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinTanpaHadir */

$this->title = Yii::t('frontend', 'Create Izin Tanpa Hadir');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Tanpa Hadir'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-tanpa-hadir-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\izin\models\search\IzinTanpaHadirSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

/**
 * @var $modelPerjanjiankerja PerjanjianKerja
 */

use frontend\modules\izin\models\IzinJenis;
use frontend\modules\izin\models\IzinTanpaHadir;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use common\models\Golongan;
use frontend\modules\pegawai\models\PerjanjianKerja;


$this->title = Yii::t('frontend', 'Izin Tanpa Hadir');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
});
JS;

$this->registerJs($js);
?>
    <div class="izin-tanpa-hadir-index">
        <?= Html::a(Yii::t('frontend', 'Download Template'), ['download-template'], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
        <?= Html::a(Yii::t('frontend', 'Upload Template'), ['upload-template'], ['class' => 'btn btn-success call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Upload Izin')]]) ?>
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'id_pegawai',
                'label' => Yii::t('app', 'Id Pegawai'),
                'value' => function (IzinTanpaHadir $model) {
                    return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
                },
            ],
            [
                'attribute' => 'pegawai_id',
                'label' => Yii::t('frontend', 'Pegawai'),
                'value' => function (IzinTanpaHadir $model) {
                    return $model->pegawai->nama_lengkap;
                },
            ],
            [
                'attribute' => 'izin_jenis_id',
                'label' => Yii::t('frontend', 'Jenis Izin'),
                'value' => function (IzinTanpaHadir $model) {
                    return $model->izinJenis->keterangan;
                },
                'hidden' => true
            ],
            [
                'attribute' => 'izin_jenis_id',
                'label' => Yii::t('frontend', 'Jenis Izin'),
                'value' => function (IzinTanpaHadir $model) {
                    return substr($model->izinJenis->keterangan,0,50).'...';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(IzinJenis::find()->all(), 'id', 'keterangan'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Izin jenis', 'id' => 'grid-izin-tanpa-hadir-search-izin_jenis_id']
            ],
            [
                'attribute' => 'golongan',
                'value' => function (IzinTanpaHadir $model) {
                    if ($golongan = $model->pegawaiGolongan) {
                        return $golongan->golongan->nama;
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
                'hAlign' => 'center',
                'format' => 'raw'
            ],
            [
                'attribute' => 'busines_unit',
                'value' => function (IzinTanpaHadir $model) {
                    if ($perjanjianKerja = $model->perjanjianKerja) {
                        return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelPerjanjiankerja->_kontrak,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
                'hAlign' => 'center',
                'format' => 'raw'
            ],
            [
                'attribute' => 'tanggal_mulai',
                'label' => Yii::t('app', 'Tanggal Mulai'),
                'value' => function (IzinTanpaHadir $model) {
                    return date('d-m-Y', strtotime($model->tanggal_mulai));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            [
                'attribute' => 'tanggal_selesai',
                'label' => Yii::t('app', 'Tanggal Selesai'),
                'value' => function (IzinTanpaHadir $model) {
                    return date('d-m-Y', strtotime($model->tanggal_selesai));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            'keterangan:ntext',
            [
                'attribute' => 'created_by',
                'value' => function (IzinTanpaHadir $model) {
                    $createdBy = $model->createdBy->userPegawai;
                    $nama = $createdBy ? $createdBy->pegawai->nama : '';
                    $namaTampil = explode(' ', $nama);
                    return $namaTampil[0];
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function (IzinTanpaHadir $model) {
                    return date('d-m-Y H:m', strtotime($model->created_at));
                },
            ],
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ];
        ?>

        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10],
                'noExportColumns' => [1, 5, 11], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'izin-tanpa-hadir' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-izin-tanpa-hadir']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
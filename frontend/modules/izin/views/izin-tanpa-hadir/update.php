<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinTanpaHadir */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Izin Tanpa Hadir',
]) . ' ' . $model->pegawai->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Izin Tanpa Hadir'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama_lengkap, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="izin-tanpa-hadir-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

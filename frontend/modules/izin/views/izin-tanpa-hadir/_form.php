<?php

use frontend\modules\izin\models\IzinJenis;
use kartik\datecontrol\DateControl;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\izin\models\IzinTanpaHadir */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
$(function() {
    $('#form-izin-tanpa-hadir').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
    
})
JS;

$this->registerJs($js);

?>

<div class="izin-tanpa-hadir-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-izin-tanpa-hadir',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'izin_jenis_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(IzinJenis::find()->orderBy('nama')->all(), 'id',
                        function($model) {
                            return '('. $model['nama'].')'.' - '.$model['keterangan'];
                        }
                    ),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Izin jenis')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'tanggal_mulai')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'tanggal_selesai')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'pegawai_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => false,
                    ],
                    'data' => $model->isNewRecord ? [] : [$model->pegawai_id => $model->pegawai->namaIdPegawai],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, tanggal:$("#izintanpahadir-tanggal_mulai").val()};}')
                        ],
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

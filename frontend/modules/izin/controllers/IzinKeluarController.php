<?php

namespace frontend\modules\izin\controllers;

use frontend\models\Attendance;
use frontend\modules\izin\models\IzinKeluar;
use frontend\modules\izin\models\search\IzinKeluarSearch;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * IzinKeluarController implements the CRUD actions for IzinKeluar model.
 */
class IzinKeluarController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IzinKeluar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IzinKeluarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelPerjanjiankerja = new PerjanjianKerja();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
        ]);
    }

    /**
     * Displays a single IzinKeluar model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $id
     * @return PerjanjianKerja|null
     * @throws NotFoundHttpException
     */
    protected function findModelPerjanjianKerja(string $id)
    {
        if (($model = PerjanjianKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Creates a new IzinKeluar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IzinKeluar();
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing IzinKeluar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('update', [
            'model' => $model,
        ]);

    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new IzinKeluar();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Deletes an existing IzinKeluar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $attribute = $model->attributes;
        $model->delete();
        $model->deleteAttendance();
        $model->setelahHapus($attribute);
        return $this->redirect(['index']);
    }


    /**
     * Finds the IzinKeluar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return IzinKeluar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IzinKeluar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggal
     * @return array|mixed
     */
    public function actionGetAttendance(string $pegawaiId, string $tanggal)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
        if ($request->isAjax && $request->isGet) {
            $model = new IzinKeluar();
            $response->data = ['data' => $model->getAttendances($pegawaiId, $tanggal), 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
        }
        return $response->data;
    }
}

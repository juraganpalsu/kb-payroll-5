<?php

namespace frontend\modules\izin\controllers;

use frontend\modules\izin\models\IzinTanpaHadir;
use frontend\modules\izin\models\IzinTanpaHadirForm;
use frontend\modules\izin\models\search\IzinTanpaHadirSearch;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\form\ActiveForm;
use PhpOffice\PhpSpreadsheet\Exception;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * IzinTanpaHadirController implements the CRUD actions for IzinTanpaHadir model.
 */
class IzinTanpaHadirController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IzinTanpaHadir models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IzinTanpaHadirSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelPerjanjiankerja = new PerjanjianKerja();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
        ]);
    }

    /**
     * Displays a single IzinTanpaHadir model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $id
     * @return PerjanjianKerja|null
     * @throws NotFoundHttpException
     */
    protected function findModelPerjanjianKerja(string $id)
    {
        if (($model = PerjanjianKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Creates a new IzinTanpaHadir model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IzinTanpaHadir();
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing IzinTanpaHadir model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('update', [
            'model' => $model,
        ]);

    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new IzinTanpaHadir();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Deletes an existing IzinTanpaHadir model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $attribute = $model->attributes;
        $model->delete();
        $model->setelahHapus($attribute);
        return $this->redirect(['index']);
    }


    /**
     * Finds the IzinTanpaHadir model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return IzinTanpaHadir the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IzinTanpaHadir::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function actionDownloadTemplate()
    {
        $model = new IzinTanpaHadir();
        return $model->downloadTemplate();
    }

    /**
     * @return array|mixed|string
     */
    public function actionUploadTemplate()
    {
        $model = new IzinTanpaHadirForm();
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [$model->errors], 'status' => false, 'message' => Yii::t('frontend', 'Gagal Upload Data')];
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->upload()) {
                try {
                    $dataPegawai = $model->extractDataIzin();
                    $model->simpanIzin($dataPegawai);

                } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                $response->data = ['data' => ['url' => Url::to(['index'])], 'status' => true, 'message' => Yii::t('frontend', 'Berhasil Upload Data')];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-upload', ['model' => $model]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadTemplateFormValidation()
    {
        $model = new IzinTanpaHadirForm();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }
}

<?php

namespace frontend\modules\izin\controllers;

use common\components\Status;
use frontend\modules\izin\models\Cuti;
use frontend\modules\izin\models\CutiSaldo;
use frontend\modules\izin\models\search\CutiSearch;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\form\ActiveForm;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * CutiController implements the CRUD actions for Cuti model.
 */
class CutiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Cuti models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CutiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelPerjanjiankerja = new PerjanjianKerja();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
        ]);
    }

    /**
     * @param string $id
     * @return PerjanjianKerja|null
     * @throws NotFoundHttpException
     */
    protected function findModelPerjanjianKerja(string $id)
    {
        if (($model = PerjanjianKerja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }


    /**
     * Creates a new Cuti model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cuti();
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new Cuti();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Finds the Cuti model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Cuti the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cuti::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggalMulai
     * @return array|mixed
     */
    public function actionGetSaldo(string $pegawaiId, string $tanggalMulai)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
        if ($request->isAjax && $request->isGet) {
            $queryCekSaldo = CutiSaldo::find()->andWhere('\'' . $tanggalMulai . '\' BETWEEN mulai_berlaku AND akhir_berlaku')
                ->andWhere(['pegawai_id' => $pegawaiId]);
            if ($cekCutiSaldo = $queryCekSaldo->orderBy('mulai_berlaku ASC')->one()) {
                $modelCutiSaldo = CutiSaldo::findOne($cekCutiSaldo['id']);
                if ($modelCutiSaldo->saldoAkhir > 0) {
                    $response->data = ['data' => ['id' => $modelCutiSaldo->id, 'info' => date('d-m-Y', strtotime($modelCutiSaldo->mulai_berlaku)) . ' s/d ' . date('d-m-Y', strtotime($modelCutiSaldo->akhir_berlaku)) . ', Saldo Cuti = ' . $modelCutiSaldo->saldoAkhir], 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
                } else {
                    if ($cekCutiSaldo = $queryCekSaldo->orderBy('mulai_berlaku DESC')->one()) {
                        $modelCutiSaldo = CutiSaldo::findOne($cekCutiSaldo['id']);
                        $response->data = ['data' => ['id' => $modelCutiSaldo->id, 'info' => 'sdsd' . date('d-m-Y', strtotime($modelCutiSaldo->mulai_berlaku)) . ' s/d ' . date('d-m-Y', strtotime($modelCutiSaldo->akhir_berlaku)) . ', Saldo Cuti = ' . $modelCutiSaldo->saldoAkhir], 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
                    }
                }
            }
        }
        return $response->data;
    }

    /**
     * @param string $id
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionBatalCuti(string $id)
    {
        $model = $this->findModel($id);
        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isPost) {
            if ($model) {
                $model->batal = Status::YA;
                if ($model->save()) {
                    $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
                } else {
                    $response->data = ['data' => $model->errors, 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
                }
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
        }
        return $response->data;
    }
}

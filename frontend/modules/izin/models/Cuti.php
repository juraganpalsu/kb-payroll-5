<?php

namespace frontend\modules\izin\models;

use common\components\Status;
use common\models\Absensi;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use frontend\modules\izin\models\base\Cuti as BaseCuti;
use frontend\modules\pegawai\models\Pegawai;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "cuti".
 */
class Cuti extends BaseCuti
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_mulai', 'tanggal_selesai', 'pegawai_id', 'cuti_saldo_id'], 'required'],
            [['tanggal_mulai', 'tanggal_selesai', 'created_at', 'updated_at', 'deleted_at', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id',], 'safe'],
            [['keterangan'], 'string'],
            [['batal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'cuti_saldo_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'batal'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'pegawaiAktifValidation'],
            ['cuti_saldo_id', 'cekSaldoValidation'],
            ['tanggal_selesai', 'cekTanggalValidation'],
            ['pegawai_id', 'cekDoubleValidation'],
            ['pegawai_id', 'cekDoubleIzinValidation'],
        ];
    }

    /**
     * @param string $tanggalAwal
     * @param string $tanggalAkhir
     * @return ActiveQuery
     */
    public function queryCekRangeTanggal(string $tanggalAwal, string $tanggalAkhir): ActiveQuery
    {
        return self::find()
            ->andWhere('\'' . $tanggalAwal . '\' BETWEEN tanggal_mulai AND tanggal_selesai OR \'' . $tanggalAkhir . '\' BETWEEN tanggal_mulai AND tanggal_selesai OR tanggal_mulai BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR tanggal_selesai BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR ( \'' . $tanggalAwal . '\' <= tanggal_mulai AND \'' . $tanggalAkhir . '\' >= tanggal_selesai) OR ( \'' . $tanggalAwal . '\' >= tanggal_mulai AND \'' . $tanggalAkhir . '\' <= tanggal_selesai)');
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggalAwal
     * @param string $tanggalAkhir
     * @return bool|int|string|null
     */
    public static function cekExist(string $pegawaiId, string $tanggalAwal, string $tanggalAkhir)
    {
        $model = new self();
        return $model->queryCekRangeTanggal($tanggalAwal, $tanggalAkhir)
            ->andWhere(['pegawai_id' => $pegawaiId])
            ->andWhere(['batal' => Status::TIDAK])
            ->count();
    }

    /**
     * @param string $attribute
     */
    public function cekDoubleValidation(string $attribute)
    {
        $tanggal_awal = $this->tanggal_mulai;
        $tanggal_akhir = $this->tanggal_selesai;
        if (!empty($tanggal_awal) && !empty($tanggal_akhir) && $this->batal == Status::TIDAK) {
            $query = self::cekExist($this->$attribute, $this->tanggal_mulai, $this->tanggal_selesai);
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama.'));
            }
        }

    }

    /**
     * @param string $attribute
     */
    public function cekDoubleIzinValidation(string $attribute)
    {
        $tanggal_awal = $this->tanggal_mulai;
        $tanggal_akhir = $this->tanggal_selesai;
        if (!empty($tanggal_awal) && !empty($tanggal_akhir)) {
            $cekIzinTerlambat = IzinTerlambat::cekExistRange($this->$attribute, $tanggal_awal, $tanggal_akhir);
            $cekIzinKeluar = IzinKeluar::cekExistRange($this->$attribute, $tanggal_awal, $tanggal_akhir);
            $cekAbsensiTidakLengkap = AbsensiTidakLengkap::cekExistRange($this->$attribute, $tanggal_awal, $tanggal_akhir);
            $cekSetengahHari = SetengahHari::cekExistRange($this->$attribute, $tanggal_awal, $tanggal_akhir);
            $cekIzinTanpaHadir = IzinTanpaHadir::cekExist($this->$attribute, $tanggal_awal, $tanggal_akhir);
            if ($cekIzinTanpaHadir) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Tanpa Hadir*.'));
            }
            if ($cekSetengahHari) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Setengah Hari*.'));
            }
            if ($cekIzinTerlambat) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Terlambat*.'));
            }
            if ($cekIzinKeluar) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Keluar/Tugas Kantor*.'));
            }
            if ($cekAbsensiTidakLengkap) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Absensi Tidak Lengkap*.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function cekSaldoValidation(string $attribute)
    {
        $modelCutiSaldo = CutiSaldo::findOne($this->$attribute);
        if ($modelCutiSaldo) {
            if ($modelCutiSaldo->saldoAkhir < $this->hitungHari() && $this->batal == Status::TIDAK) {
                $this->addError($attribute, Yii::t('frontend', 'Saldo cuti tidak cukup'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function cekTanggalValidation(string $attribute)
    {
        $awal = new DateTime($this->tanggal_mulai);
        $akhir = new DateTime($this->tanggal_selesai);

        if ($akhir < $awal) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal Awal Harus Lebih Kecil'));
        }
        $modelCutiSaldo = CutiSaldo::findOne($this->cuti_saldo_id);
        if ($modelCutiSaldo) {
            try {

                $interval = DateInterval::createFromDateString('1 day');
                $akhir->add($interval);
                $period = new DatePeriod($awal, $interval, $akhir);

                $tanggalTakTerdaftar = [];
                /** @var DateTime $dt */
                foreach ($period as $dt) {
                    $cekCutiSaldo = CutiSaldo::find()->andWhere(['id' => $modelCutiSaldo->id])->andWhere('\'' . $dt->format('Y-m-d') . '\' BETWEEN mulai_berlaku AND akhir_berlaku')
                        ->one();
                    if (!$cekCutiSaldo) {
                        $tanggalTakTerdaftar[] = $dt->format('d-m-Y');
                    }
                }
                if (!empty($tanggalTakTerdaftar)) {
                    $this->addError($attribute, Yii::t('frontend', 'Tanggal berikut tidak terdaftar pada saldo cuti : {tanggal}', ['tanggal' => implode(', ', $tanggalTakTerdaftar)]));
                }
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }

    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert && $this->batal == Status::TIDAK) {
            $modelCutiSaldo = CutiSaldo::findOne($this->cuti_saldo_id);
            $modelCutiSaldo->terpakai = $modelCutiSaldo->terpakai + $this->hitungHari();
            $modelCutiSaldo->saldo = $modelCutiSaldo->saldo - $this->hitungHari();
            $modelCutiSaldo->save();
        }
        if ($this->batal == Status::YA) {
            $modelCutiSaldo = CutiSaldo::findOne($this->cuti_saldo_id);
            $modelCutiSaldo->terpakai = $modelCutiSaldo->terpakai - $this->hitungHari();
            $modelCutiSaldo->saldo = $modelCutiSaldo->saldo + $this->hitungHari();
            $modelCutiSaldo->save();
        }
        $this->prosesData([$this->pegawai_id]);
    }

    /**
     * @return int
     */
    public function hitungHari()
    {
        $hitungHari = 0;
        try {
            $awal = new DateTime($this->tanggal_mulai);
            $akhir = new DateTime($this->tanggal_selesai);
            $hitungHari = $awal->diff($akhir)->d;
            $hitungHari += 1;
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $hitungHari;
    }


    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tanggal_mulai)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_mulai)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal_mulai)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal_mulai)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }


    /**
     * @param array $pegawaiids
     */
    public function prosesData(array $pegawaiids)
    {
        $modelAbsensi = new Absensi();
        try {
            $modelAbsensi->prosesData($this->tanggal_mulai, $this->tanggal_selesai, null, $pegawaiids);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

}

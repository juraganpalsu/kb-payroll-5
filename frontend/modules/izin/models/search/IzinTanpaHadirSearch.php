<?php

namespace frontend\modules\izin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\izin\models\IzinTanpaHadir;
use yii\db\ActiveQuery;

/**
 * frontend\modules\izin\models\search\IzinTanpaHadirSearch represents the model behind the search form about `frontend\modules\izin\models\IzinTanpaHadir`.
 */
class IzinTanpaHadirSearch extends IzinTanpaHadir
{
    /**
     * @var int
     */
    public $id_pegawai;

    public $busines_unit;

    public $golongan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pegawai','tanggal_mulai', 'tanggal_selesai', 'keterangan', 'izin_jenis_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['batal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock','busines_unit', 'golongan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IzinTanpaHadir::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'batal' => $this->batal,
        ]);

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        if (!empty($this->tanggal_mulai)) {
            $tanggal = explode('s/d', $this->tanggal_mulai);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal_mulai', $awal, $akhir]);
        }

        if (!empty($this->tanggal_selesai)) {
            $tanggal = explode('s/d', $this->tanggal_selesai);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal_selesai', $awal, $akhir]);
        }


        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['izin_jenis_id' => $this->izin_jenis_id])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\izin\models\search;

use frontend\modules\izin\models\CutiConf;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\izin\models\search\CutiConfSearch represents the model behind the search form about `frontend\modules\izin\models\CutiConf`.
 */
class CutiConfSearch extends CutiConf
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'akhir_berlaku', 'keterangan', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['tahun', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CutiConf::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tahun' => $this->tahun,
            'akhir_berlaku' => $this->akhir_berlaku,
            'created_by' => $this->created_by,
            'created_by_struktur' => $this->created_by_struktur,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\izin\models;

use frontend\modules\izin\models\base\IzinJenis as BaseIzinJenis;

/**
 * This is the model class for table "izin_jenis".
 */
class IzinJenis extends BaseIzinJenis
{

    const uangMakan = 'uang_makan';
    const uangTransport = 'uang_transport';
    const hitungMasuk = 'hitung_masuk';
    const potongGaji = 'potong_gaji';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['keterangan'], 'string'],
            [['uang_makan', 'uang_transport', 'hitung_masuk', 'potong_gaji'], 'boolean'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }


    /**
     * @param string $nama
     * @param string $kolom
     * @return IzinJenis|mixed|null
     */
    public static function getConfig(string $nama, string $kolom = '')
    {
        $model = self::findOne(['nama' => $nama]);
        if (empty($kolom)) {
            return $model;
        }
        return $model->$kolom;
    }

}

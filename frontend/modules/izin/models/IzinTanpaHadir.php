<?php

namespace frontend\modules\izin\models;

use common\models\Absensi;
use DateTime;
use Exception;
use frontend\modules\izin\models\base\IzinTanpaHadir as BaseIzinTanpaHadir;
use frontend\modules\pegawai\models\Pegawai;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "izin_tanpa_hadir".
 */
class IzinTanpaHadir extends BaseIzinTanpaHadir
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_mulai', 'tanggal_selesai', 'izin_jenis_id', 'pegawai_id'], 'required'],
            [['tanggal_mulai', 'tanggal_selesai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['batal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'izin_jenis_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'pegawaiAktifValidation'],
            ['tanggal_selesai', 'cekTanggalValidation'],
            ['pegawai_id', 'cekDoubleValidation'],
            ['pegawai_id', 'cekDoubleIzinValidation']
        ];
    }

    /**
     * @param string $tanggalAwal
     * @param string $tanggalAkhir
     * @return ActiveQuery
     */
    public function queryCekRangeTanggal(string $tanggalAwal, string $tanggalAkhir): ActiveQuery
    {
        return self::find()
            ->andWhere('\'' . $tanggalAwal . '\' BETWEEN tanggal_mulai AND tanggal_selesai OR \'' . $tanggalAkhir . '\' BETWEEN tanggal_mulai AND tanggal_selesai OR tanggal_mulai BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR tanggal_selesai BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR ( \'' . $tanggalAwal . '\' <= tanggal_mulai AND \'' . $tanggalAkhir . '\' >= tanggal_selesai) OR ( \'' . $tanggalAwal . '\' >= tanggal_mulai AND \'' . $tanggalAkhir . '\' <= tanggal_selesai)');
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggalAwal
     * @param string $tanggalAkhir
     * @return bool|int|string|null
     */
    public static function cekExist(string $pegawaiId, string $tanggalAwal, string $tanggalAkhir)
    {
        $model = new self();
        return $model->queryCekRangeTanggal($tanggalAwal, $tanggalAkhir)
            ->andWhere(['pegawai_id' => $pegawaiId])
            ->count();
    }

    /**
     * @param string $attribute
     */
    public function cekDoubleValidation(string $attribute)
    {
        $tanggal_awal = $this->tanggal_mulai;
        $tanggal_akhir = $this->tanggal_selesai;
        if (!empty($tanggal_awal) && !empty($tanggal_akhir)) {
            $query = self::cekExist($this->$attribute, $tanggal_awal, $tanggal_akhir);
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function cekDoubleIzinValidation(string $attribute)
    {
        $tanggal_awal = $this->tanggal_mulai;
        $tanggal_akhir = $this->tanggal_selesai;
        if (!empty($tanggal_awal) && !empty($tanggal_akhir)) {
            $cekIzinTerlambat = IzinTerlambat::cekExistRange($this->$attribute, $tanggal_awal, $tanggal_akhir);
            $cekIzinKeluar = IzinKeluar::cekExistRange($this->$attribute, $tanggal_awal, $tanggal_akhir);
            $cekAbsensiTidakLengkap = AbsensiTidakLengkap::cekExistRange($this->$attribute, $tanggal_awal, $tanggal_akhir);
            $cekSetengahHari = SetengahHari::cekExistRange($this->$attribute, $tanggal_awal, $tanggal_akhir);
            $cekCuti = Cuti::cekExist($this->$attribute, $tanggal_awal, $tanggal_akhir);
            if ($cekSetengahHari) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Setengah Hari*.'));
            }
            if ($cekCuti) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Cuti*.'));
            }
            if ($cekSetengahHari) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Setengah Hari*.'));
            }
            if ($cekIzinTerlambat) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Terlambat*.'));
            }
            if ($cekIzinKeluar) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Keluar/Tugas Kantor*.'));
            }
            if ($cekAbsensiTidakLengkap) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Absensi Tidak Lengkap*.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function cekTanggalValidation(string $attribute)
    {
        try {
            $awal = new DateTime($this->tanggal_mulai);
            $akhir = new DateTime($this->tanggal_selesai);

            if ($akhir < $awal) {
                $this->addError($attribute, Yii::t('frontend', 'Tanggal Awal Harus Lebih Kecil'));
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tanggal_mulai)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_mulai)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal_mulai)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal_mulai)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->prosesData([$this->pegawai_id]);
    }

    /**
     * @param array $attributes
     */
    public function setelahHapus(array $attributes)
    {
        $this->tanggal_mulai = $attributes['tanggal_mulai'];
        $this->tanggal_selesai = $attributes['tanggal_selesai'];
        $this->prosesData([$this->pegawai_id]);
    }

    /**
     * @param array $pegawaiids
     * @param string $tanggal
     */
    public function prosesData(array $pegawaiids, string $tanggal = '')
    {
        $modelAbsensi = new Absensi();
        try {
            if (empty($tanggal)) {
                $tanggalMulai = $this->tanggal_mulai;
                $tanggalAkhir = $this->tanggal_selesai;
            }
            $modelAbsensi->prosesData($tanggalMulai, $tanggalAkhir, null, $pegawaiids);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadTemplate()
    {
        $fileName = Yii::t('frontend', 'TemplateIzinTanpaHadir');
        $jenisIjin = IzinJenis::find()->select('nama')->orderBy('nama ASC')->column();
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(10);
        $activeSheet->getColumnDimension('D')->setWidth(15);
        $activeSheet->getColumnDimension('E')->setWidth(15);
        $activeSheet->getColumnDimension('F')->setWidth(40);

        $activeSheet->mergeCells('A1:F1')->setCellValue('A1', Yii::t('frontend', 'Template Izin Tanpa Hadir'));

        $activeSheet->mergeCells('A2:B2')->setCellValue('A2', Yii::t('frontend', 'Jenis Izin'));
        $activeSheet->mergeCells('C2:F2')->setCellValue('C2', ': ' . implode(", ", $jenisIjin));

        $activeSheet->setCellValue('A3', Yii::t('frontend', 'No'));
        $activeSheet->setCellValue('B3', Yii::t('frontend', 'Id Pegawai'));
        $activeSheet->setCellValue('C3', Yii::t('frontend', 'Jenis Izin'));
        $activeSheet->setCellValue('D3', Yii::t('frontend', 'Tanggal Mulai'));
        $activeSheet->setCellValue('E3', Yii::t('frontend', 'Tanggal Selesai'));
        $activeSheet->setCellValue('F3', Yii::t('frontend', 'Keterangan'));

        $row = 3;
        for ($no = 1; $no <= 25; $no++) {
            $row++;
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
        }

        $highestColumn = $activeSheet->getHighestColumn('3');

        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A1:F1')->applyFromArray($headerStyle, $outLineBottomBorder);
        $activeSheet->getStyle('A3:F3')->applyFromArray($headerStyle, $outLineBottomBorder);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $fileName . '.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (\PhpOffice\PhpSpreadsheet\Writer\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }


}

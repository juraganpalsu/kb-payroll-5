<?php

namespace frontend\modules\izin\models;

use common\models\Absensi;
use common\models\TimeTable;
use Exception;
use frontend\modules\izin\models\base\SetengahHari as BaseSetengahHari;
use Yii;

/**
 * Class SetengahHari
 * @package frontend\modules\izin\models
 */
class SetengahHari extends BaseSetengahHari
{
    const alias = 'SH';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'shift', 'pegawai_id'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['shift', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['keterangan'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'validasiPegawai']
        ];
    }

    /**
     * @param string $attribute
     */
    public function validasiPegawai(string $attribute)
    {
        $model = SetengahHari::findOne(['pegawai_id' => $this->$attribute, 'tanggal' => $this->tanggal]);
        if ($model) {
            $this->addError($attribute, Yii::t('frontend', 'Pegawai a/n {nama} telah dibuatkan untuk tanggal {tanggal}',
                ['nama' => $model->pegawai->nama_lengkap, 'tanggal' => date('d-m-Y', strtotime($this->tanggal))]));
        }
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggal
     * @return bool|int|string|null
     */
    public static function cekExist(string $pegawaiId, string $tanggal)
    {
        return self::find()->andWhere(['tanggal' => $tanggal, 'pegawai_id' => $pegawaiId])->count();
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggalAwal
     * @param string $tanggalAkhir
     * @return bool|int|string|null
     */
    public static function cekExistRange(string $pegawaiId, string $tanggalAwal, string $tanggalAkhir)
    {
        return self::find()->andWhere(['pegawai_id' => $pegawaiId])->andWhere(['BETWEEN', 'tanggal', $tanggalAwal, $tanggalAkhir])->count();
    }


    /**
     * @param string $attribute
     */
    public function cekDoubleIzinValidation(string $attribute)
    {
        if (!empty($this->tanggal)) {
            $cekIzinTanpaHadir = Cuti::cekExist($this->$attribute, $this->tanggal, $this->tanggal);
            $cekSetengahHari = SetengahHari::cekExist($this->$attribute, $this->tanggal);
            $cekCuti = Cuti::cekExist($this->$attribute, $this->tanggal, $this->tanggal);
            if ($cekSetengahHari) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Setengah Hari*.'));
            }
            if ($cekCuti) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Cuti*.'));
            }
            if ($cekIzinTanpaHadir) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Tanpa Hadir*.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @return TimeTable
     */
    public function modelTimeTable()
    {
        return new TimeTable();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->prosesData([$this->pegawai_id]);
    }

    /**
     * @param array $attributes
     */
    public function setelahHapus(array $attributes)
    {
        $this->tanggal = $attributes['tanggal'];
        $this->prosesData([$this->pegawai_id]);
    }


    /**
     * @param array $pegawaiids
     * @param string $tanggal
     */
    public function prosesData(array $pegawaiids, string $tanggal = '')
    {
        $modelAbsensi = new Absensi();
        try {
            if (empty($tanggal)) {
                $tanggal = $this->tanggal;
            }
            $modelAbsensi->prosesData($tanggal, $tanggal, null, $pegawaiids);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

}

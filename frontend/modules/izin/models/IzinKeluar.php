<?php

namespace frontend\modules\izin\models;

use common\models\Absensi;
use DateTime;
use Exception;
use frontend\models\Attendance;
use frontend\modules\izin\models\base\IzinKeluar as BaseIzinKeluar;
use frontend\modules\pegawai\models\Pegawai;
use Throwable;
use Yii;

/**
 * This is the model class for table "izin_keluar".
 *
 * @property array $_kategori
 */
class IzinKeluar extends BaseIzinKeluar
{


    const KELUAR = 1;
    const TUGAS_KANTOR = 2;

    public $_kategori = [self::KELUAR => 'Izin Keluar/Pulang', self::TUGAS_KANTOR => 'Izin Tugas Kantor'];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kategori', 'tanggal', 'jam_mulai', 'jam_selesai', 'keterangan', 'pegawai_id'], 'required'],
            [['kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal', 'jam_mulai', 'jam_selesai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['keterangan'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'validasiPegawai']
        ];
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggal
     * @return bool|int|string|null
     */
    public static function cekExist(string $pegawaiId, string $tanggal)
    {
        return self::find()->andWhere(['tanggal' => $tanggal, 'pegawai_id' => $pegawaiId])->count();
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggalAwal
     * @param string $tanggalAkhir
     * @return bool|int|string|null
     */
    public static function cekExistRange(string $pegawaiId, string $tanggalAwal, string $tanggalAkhir)
    {
        return self::find()->andWhere(['pegawai_id' => $pegawaiId])->andWhere(['BETWEEN', 'tanggal', $tanggalAwal, $tanggalAkhir])->count();
    }


    /**
     * @param string $attribute
     */
    public function cekDoubleIzinValidation(string $attribute)
    {
        if (!empty($this->tanggal)) {
            $cekIzinTanpaHadir = Cuti::cekExist($this->$attribute, $this->tanggal, $this->tanggal);
            $cekSetengahHari = SetengahHari::cekExist($this->$attribute, $this->tanggal);
            $cekCuti = Cuti::cekExist($this->$attribute, $this->tanggal, $this->tanggal);
            if ($cekSetengahHari) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Setengah Hari*.'));
            }
            if ($cekCuti) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Cuti*.'));
            }
            if ($cekIzinTanpaHadir) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Tanpa Hadir*.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function validasiPegawai(string $attribute)
    {
        $model = IzinKeluar::findOne(['pegawai_id' => $this->$attribute, 'tanggal' => $this->tanggal, 'jam_mulai' => $this->jam_mulai, 'jam_selesai' => $this->jam_selesai]);
        if ($model) {
            $this->addError($attribute, Yii::t('frontend', 'Pegawai a/n {nama} telah dibuatkan untuk tanggal {tanggal} jam {jam}',
                ['nama' => $model->pegawai->nama_lengkap, 'tanggal' => date('d-m-Y', strtotime($this->tanggal)), 'jam' => $this->jam_mulai]));
        }
    }


    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tanggal)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $tanggal = $this->tanggal;
        if (isset($changedAttributes['tanggal'])) {
            $tanggal = $changedAttributes['tanggal'];
        }
        $jamMulai = $this->jam_mulai;
        if (isset($changedAttributes['jam_mulai'])) {
            $jamMulai = $changedAttributes['jam_mulai'];
        }
        $jamSelesai = $this->jam_selesai;
        if (isset($changedAttributes['jam_selesai'])) {
            $jamSelesai = $changedAttributes['jam_selesai'];
        }
        try {
            $mulai = new DateTime($tanggal . ' ' . $jamMulai);
            $selesai = new DateTime($tanggal . ' ' . $jamSelesai);
            if ($insert) {
                $modelAttendance = new Attendance();
                $modelAttendance->saveAttendance($this->perjanjianKerja->id_pegawai, $mulai->format('Y-m-d H:i:s'));
                $modelAttendance->saveAttendance($this->perjanjianKerja->id_pegawai, $selesai->format('Y-m-d H:i:s'));
            } else {
                $queryAttendanceMulai = Attendance::findOne(['check_time' => $mulai->format('Y-m-d H:i:s'), 'pegawai_id' => $this->pegawai_id]);
                if ($queryAttendanceMulai) {
                    $mulaiNew = new DateTime($this->tanggal . ' ' . $this->jam_mulai);
                    $queryAttendanceMulai->check_time = $mulaiNew->format('Y-m-d H:i:s');
                    $queryAttendanceMulai->save(false);
                }
                $queryAttendanceSelesai = Attendance::findOne(['check_time' => $selesai->format('Y-m-d H:i:s'), 'pegawai_id' => $this->pegawai_id]);
                if ($queryAttendanceSelesai) {
                    $selesaiNew = new DateTime($this->tanggal . ' ' . $this->jam_selesai);
                    $queryAttendanceSelesai->check_time = $selesaiNew->format('Y-m-d H:i:s');
                    $queryAttendanceSelesai->save(false);
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        $this->prosesData([$this->pegawai_id]);
    }

    /**
     * @param array $attributes
     */
    public function setelahHapus(array $attributes)
    {
        $this->tanggal = $attributes['tanggal'];
        $this->prosesData([$this->pegawai_id]);
    }

    /**
     * @param array $pegawaiids
     * @param string $tanggal
     */
    public function prosesData(array $pegawaiids, string $tanggal = '')
    {
        $modelAbsensi = new Absensi();
        try {
            if (empty($tanggal)) {
                $tanggal = $this->tanggal;
            }
            $modelAbsensi->prosesData($tanggal, $tanggal, null, $pegawaiids);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * @throws Throwable
     */
    public function deleteAttendance()
    {
        try {
            $mulai = new DateTime($this->tanggal . ' ' . $this->jam_mulai);
            $selesai = new DateTime($this->tanggal . ' ' . $this->jam_selesai);
            $queryAttendanceMulai = Attendance::findOne(['check_time' => $mulai->format('Y-m-d H:i:s'), 'pegawai_id' => $this->pegawai_id]);
            if ($queryAttendanceMulai) {
                $queryAttendanceMulai->delete();
            }
            $queryAttendanceSelesai = Attendance::findOne(['check_time' => $selesai->format('Y-m-d H:i:s'), 'pegawai_id' => $this->pegawai_id]);
            if ($queryAttendanceSelesai) {
                $queryAttendanceSelesai->delete();
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggal
     * @return string
     */
    public function getAttendances(string $pegawaiId, string $tanggal)
    {
        $queryAttendance = Attendance::find()
            ->andWhere(['BETWEEN', 'date(check_time)', date('Y-m-d', strtotime($tanggal . '-1 day')), date('Y-m-d', strtotime($tanggal . '+1 day'))])
            ->andWhere(['pegawai_id' => $pegawaiId])->orderBy('check_time ASC')
            ->all();
        $attendances = [];
        if (!empty($queryAttendance)) {
            /** @var Attendance $attendance */
            foreach ($queryAttendance as $attendance) {
                $attendances[date('d-m-Y', strtotime($attendance->check_time))][] = date('H:i:s', strtotime($attendance->check_time));
            }
        }
        $v = '';
        if (!empty($attendances)) {
            foreach ($attendances as $key => $attendance) {
                $v .= '*' . $key . ' ( ' . implode(', ', $attendance) . ' )*, ';
            }
        }
        return $v;
    }

}

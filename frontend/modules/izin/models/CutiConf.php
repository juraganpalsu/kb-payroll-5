<?php

namespace frontend\modules\izin\models;

use frontend\modules\izin\models\base\CutiConf as BaseCutiConf;
use Yii;

/**
 * This is the model class for table "cuti_conf".
 */
class CutiConf extends BaseCutiConf
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['akhir_berlaku'], 'required'],
            [['tahun', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['akhir_berlaku', 'updated_at', 'deleted_at', 'created_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['akhir_berlaku', 'validasiAkhirBerlaku']
        ];
    }

    /**
     * @param string $attribute
     */
    public function validasiAkhirBerlaku(string $attribute)
    {
        if ($oldModel = self::findOne($this->id)) {
            $oldValue = $oldModel->akhir_berlaku;
        } else {
            //untuk generate pertama kali
            $oldValue = date('Y', strtotime('-1 year')) . '-06-30';
        }

        if ($this->$attribute < $oldValue) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir berlaku tidak boleh lebih kecil'));
        }
    }

    /**
     * @return int
     */
    public function updateALlPegawai(): int
    {
        return CutiSaldo::updateAll(['akhir_berlaku' => $this->akhir_berlaku], ['year(mulai_berlaku)' => $this->tahun]);
    }
}

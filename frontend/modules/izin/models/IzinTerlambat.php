<?php

namespace frontend\modules\izin\models;

use common\models\TimeTable;
use frontend\modules\izin\models\base\IzinTerlambat as BaseIzinTerlambat;
use frontend\modules\pegawai\models\Pegawai;
use Yii;

/**
 * This is the model class for table "izin_terlambat".
 */
class IzinTerlambat extends BaseIzinTerlambat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'shift', 'keterangan', 'pegawai_id'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['shift', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['keterangan'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'cekDoubleValidation'],
//            ['pegawai_id', 'cekDoubleIzinValidation'],
        ];
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggal
     * @return bool|int|string|null
     */
    public static function cekExist(string $pegawaiId, string $tanggal)
    {
        return self::find()->andWhere(['tanggal' => $tanggal, 'pegawai_id' => $pegawaiId])->count();
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggalAwal
     * @param string $tanggalAkhir
     * @return bool|int|string|null
     */
    public static function cekExistRange(string $pegawaiId, string $tanggalAwal, string $tanggalAkhir)
    {
        return self::find()->andWhere(['pegawai_id' => $pegawaiId])->andWhere(['BETWEEN', 'tanggal', $tanggalAwal, $tanggalAkhir])->count();
    }

    /**
     * @param string $attribute
     */
    public function cekDoubleValidation(string $attribute)
    {
        if (!empty($this->tanggal)) {
            $query = self::cekExist($this->$attribute, $this->tanggal);
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function cekDoubleIzinValidation(string $attribute)
    {
        if (!empty($this->tanggal)) {
            $cekIzinTanpaHadir = Cuti::cekExist($this->$attribute, $this->tanggal, $this->tanggal);
            $cekSetengahHari = SetengahHari::cekExist($this->$attribute, $this->tanggal);
            $cekCuti = Cuti::cekExist($this->$attribute, $this->tanggal, $this->tanggal);
            if ($cekSetengahHari) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Setengah Hari*.'));
            }
            if ($cekCuti) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Cuti*.'));
            }
            if ($cekIzinTanpaHadir) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama dengan *Izin Tanpa Hadir*.'));
            }
        }
    }


    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tanggal)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @return TimeTable
     */
    public function modelTimeTable()
    {
        return new TimeTable();
    }

}

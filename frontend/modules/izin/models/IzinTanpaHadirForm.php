<?php

/**
 * Created by PhpStorm.
 * File Name: IzinTanpaHadirForm.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 05/07/20
 * Time: 20.56
 */


namespace frontend\modules\izin\models;


use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;


/**
 * Class IzinTanpaHadirForm
 * @package frontend\modules\izin\models\form
 *
 * @property string $upload_dir;
 * @property string $doc_file;
 */
class IzinTanpaHadirForm extends Model
{

    public $upload_dir = 'uploads/izin-tanpa-hadir/';

    public $doc_file;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            /** @var UploadedFile $docFile */
            $docFile = $this->doc_file;
            $docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractDataIzin()
    {
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        $fileName = $this->upload_dir . $this->doc_file->name;
        $spreadsheet = $reader->load($fileName);
//        $name = 'POKOK FIX 21-09-2020 20-10-2020_SATU_KBU (3).xls';
//        $spreadsheet = $reader->load($this->upload_dir . $name);
        $startRow = 4;
        $activeSheet = $spreadsheet->getActiveSheet();
        $maxRow = $activeSheet->getHighestRow('B');
        $datas = [];
        foreach ($activeSheet->getRowIterator($startRow, $maxRow) as $row) {
            $pegawaiId = $activeSheet->getCell('B' . $row->getRowIndex())->getValue();
            $jenisIzin = $activeSheet->getCell('C' . $row->getRowIndex())->getValue();
            $mulai = $activeSheet->getCell('D' . $row->getRowIndex())->getValue();
            $selesai = $activeSheet->getCell('E' . $row->getRowIndex())->getValue();
            $keterangan = $activeSheet->getCell('F' . $row->getRowIndex())->getValue();
            if (($pegawaiId && $jenisIzin && $mulai && $selesai)) {
                $datas[] = [
                    'id_pegawai' => $pegawaiId,
                    'jenis_izin' => $jenisIzin,
                    'tanggal_mulai' => $mulai,
                    'tanggal_selesai' => $selesai,
                    'keterangan' => $keterangan,
                ];
            }
        }
        $this->renameFile($fileName);
        return $datas;
    }

    /**
     * @param string $fileName
     */
    private function renameFile(string $fileName)
    {
        $oldName = $fileName;
        $newName = $fileName . date('Ymdhis');

        rename($oldName, $newName);
    }

    /**
     * @param array $dataIzin
     */
    public function simpanIzin(array $dataIzin)
    {
        if (!empty($dataIzin)) {
            foreach ($dataIzin as $data) {
                $modelPerjanjianKerja = PerjanjianKerja::findOne(['id_pegawai' => $data['id_pegawai']]);
                $jenisIzin = IzinJenis::findOne(['nama' => $data['jenis_izin']]);
                if ($modelPerjanjianKerja && $jenisIzin) {
                    $model = new IzinTanpaHadir();
                    $model->tanggal_mulai = date('Y-m-d', strtotime($data['tanggal_mulai']));
                    $model->tanggal_selesai = date('Y-m-d', strtotime($data['tanggal_selesai']));
                    $model->pegawai_id = $modelPerjanjianKerja->pegawai_id;
                    $model->izin_jenis_id = $jenisIzin->id;
                    $model->keterangan = $data['keterangan'];
                    if (!$model->save()) {
                        Yii::info($model->attributes, 'exception');
                        Yii::info($model->errors, 'exception');
                    }
                }
            }
        }
    }
}
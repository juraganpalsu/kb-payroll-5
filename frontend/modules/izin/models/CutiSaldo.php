<?php

namespace frontend\modules\izin\models;

use frontend\models\Libur;
use frontend\modules\pegawai\models\Pegawai;
use phpDocumentor\Reflection\Types\This;
use Yii;
use \frontend\modules\izin\models\base\CutiSaldo as BaseCutiSaldo;

/**
 * This is the model class for table "cuti_saldo".
 *
 * @property int $saldoAkhir
 */
class CutiSaldo extends BaseCutiSaldo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mulai_berlaku', 'akhir_berlaku', 'pegawai_id'], 'required'],
            [['mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['kuota', 'terpakai', 'saldo', 'kadaluarsa', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }


    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->mulai_berlaku)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->mulai_berlaku)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->mulai_berlaku)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->mulai_berlaku)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->mulai_berlaku)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->mulai_berlaku)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->mulai_berlaku)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @return int
     */
    public function hitungCutiBersama(): int
    {
        $mulaiBerlaku = $this->mulai_berlaku;
        $tahun = date('Y', strtotime($this->mulai_berlaku));
        $akhirTahun = $tahun . '-12-31';
        return (int)Libur::find()->andWhere(['BETWEEN', 'tanggal', $mulaiBerlaku, $akhirTahun])->andWhere(['jenis' => Libur::CUTI_BERSAMA])->count('id');
    }

    /**
     * @return int
     */
    public function getSaldoAkhir(): int
    {
        return $this->saldo - $this->hitungCutiBersama();
    }

}

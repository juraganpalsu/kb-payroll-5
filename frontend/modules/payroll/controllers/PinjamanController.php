<?php

namespace frontend\modules\payroll\controllers;

use frontend\modules\payroll\models\form\PinjamanForm;
use frontend\modules\payroll\models\Pinjaman;
use frontend\modules\payroll\models\PinjamanAngsuran;
use frontend\modules\payroll\models\search\PinjamanSearch;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use Mpdf\MpdfException;
use PhpOffice\PhpSpreadsheet\Exception;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PinjamanController implements the CRUD actions for Pinjaman model.
 */
class PinjamanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pinjaman models.
     * @return mixed
     */
    public function actionIndexFinance()
    {
        $searchModel = new PinjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true, [Pinjaman::BANK, Pinjaman::KASBON, Pinjaman::KOPERASI, Pinjaman::LAIN_LAIN, Pinjaman::PINJAMAN]);
        $modelPerjanjiankerja = new PerjanjianKerja();
        $modelPinjaman = new Pinjaman();

        return $this->render('index-finance', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
            'modelPinjaman' => $modelPinjaman,
        ]);
    }

    /**
     * Lists potongan.
     * @return mixed
     */
    public function actionIndexPayroll()
    {
        $searchModel = new PinjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true, [Pinjaman::POTONGAN, Pinjaman::POTONGAN_ATL]);
        $modelPerjanjiankerja = new PerjanjianKerja();
        $modelPinjaman = new Pinjaman();


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'redirect' => 'index-payroll',
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
            'modelPinjaman' => $modelPinjaman,
        ]);
    }

    /**
     * Displays a single Pinjaman model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);

        $queryAngsuran = PinjamanAngsuran::find()->andWhere(['pinjaman_id' => $id]);
        $providerPinjamanAngsuran = new ActiveDataProvider([
            'query' => $queryAngsuran,
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerPinjamanAngsuran' => $providerPinjamanAngsuran,
        ]);
    }

    /**
     * Displays a single Pinjaman model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionViewFinance(string $id)
    {
        $model = $this->findModel($id);

        $queryAngsuran = PinjamanAngsuran::find()->andWhere(['pinjaman_id' => $id]);
        $providerPinjamanAngsuran = new ActiveDataProvider([
            'query' => $queryAngsuran,
        ]);
        return $this->render('view-finance', [
            'model' => $model,
            'providerPinjamanAngsuran' => $providerPinjamanAngsuran,
        ]);
    }

    /**
     * Creates a new Pinjaman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pinjaman();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pinjaman model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new Pinjaman();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing Pinjaman model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id)
    {
        $model = $this->findModel($id);
        $model->delete();
        if ($model->pinjamanAngsurans) {
            foreach ($model->pinjamanAngsurans as $pinjamanAngsuran) {
                $pinjamanAngsuran->delete();
            }
        }
        return $this->redirect(['index']);
    }


    /**
     * Finds the Pinjaman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Pinjaman the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id)
    {
        if (($model = Pinjaman::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the Pinjaman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PinjamanAngsuran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelAngsuran(string $id)
    {
        if (($model = PinjamanAngsuran::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the Pinjaman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PinjamanAngsuran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelPinjamanAngsuran(string $id)
    {
        if (($model = PinjamanAngsuran::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionSetDibayar(string $id)
    {
        $model = $this->findModelPinjamanAngsuran($id);
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->status = PinjamanAngsuran::DIBAYAR;
            $cek = PinjamanAngsuran::find()->andWhere(['<', 'angsuran_ke', $model->angsuran_ke])->andWhere(['pinjaman_id' => $model->pinjaman_id])->andWhere(['IN', 'status', PinjamanAngsuran::BELUM_DIBAYAR])->one();
            if ($cek) {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
                if ($model->save(false)) {
                    $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pinjaman_id])], 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
                }
            }
            return $response->data;
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));

    }


    /**
     * Updates an existing Pinjaman model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdateAngsuran(string $id)
    {
        $model = $this->findModelAngsuran($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->pinjaman_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('update-pinjaman', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUpdateAngsuranValidation()
    {
        $model = new PinjamanAngsuran();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return false|mixed
     * @throws MpdfException
     * @throws CrossReferenceException
     * @throws PdfParserException
     * @throws PdfTypeException
     * @throws InvalidConfigException
     */
    public function actionPrint(string $id)
    {
        try {
            $model = $this->findModel($id);
            $content = $this->renderAjax('_print', ['model' => $model]);
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_BROWSER,
                'content' => $content,
                'cssFile' => '@frontend/web/css/laporanHarian.css',
                'cssInline' => 'table{font-size:11px; font-family:open sans, tahoma, sans-serif}',
                'options' => ['title' => Yii::$app->name],
                'marginTop' => 0,
                'marginBottom' => '5',
                'marginLeft' => '5',
                'marginRight' => '5',
                'methods' => [
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);

            return $pdf->render();
        } catch (NotFoundHttpException $e) {
            Yii::info($e->getMessage(), 'exception');
            return false;
        }
    }

    /**
     * @return array|mixed|string
     */
    public function actionDownloadPinjamanForm()
    {
        $formModel = new PinjamanForm();
        $formModel->scenario = 'download';
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($formModel->load($request->post()) && $formModel->validate()) {
                $response->data = ['data' => ['url' => Url::to(['download-pinjaman', 'PinjamanForm' => $formModel->attributes])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-download-pinjaman', ['model' => $formModel]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionDownloadPinjamanFormValidation()
    {
        $model = new PinjamanForm();
        $model->scenario = 'download';
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionDownloadPinjaman()
    {
        $model = new PinjamanForm();
        $model->scenario = 'download';
        $model->load(Yii::$app->request->queryParams);
        if ($model->validate()) {
//            echo "<pre>";
//            print_r($model->getDataPinjamanPerPeriode());
//            echo "</pre>";
            return $model->downloadRekapPinjaman();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }


    /**
     * @param int $kategori
     * @return bool
     * @throws Exception
     */
    public function actionGetTemplate(int $kategori)
    {
        $model = new PinjamanForm();
        return $model->downloadTemplate($kategori);
    }


    /**
     * @return array|mixed|string
     */
    public function actionUploadTemplate()
    {
        $model = new PinjamanForm();
        $model->scenario = 'upload-template';
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [$model->errors], 'status' => false, 'message' => Yii::t('frontend', 'Gagal Upload Data')];
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->upload()) {
                try {
                    $model->simpan();
                    $response->data = ['data' => ['url' => Url::to(['index-payroll'])], 'status' => true, 'message' => Yii::t('frontend', 'Berhasil Upload Data')];
                } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception | Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
            return $response->data;
        }
        return $this->renderAjax('_form-upload', ['model' => $model]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadTemplateFormValidation()
    {
        $model = new PinjamanForm();
        $model->scenario = 'upload-template';
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


}

<?php

namespace frontend\modules\payroll\controllers;

use frontend\modules\payroll\models\GajiBulanan;
use frontend\modules\payroll\models\ProsesGaji;
use frontend\modules\payroll\models\RevisiGaji;
use frontend\modules\payroll\models\search\GajiBulananSearch;
use frontend\modules\payroll\models\search\ProsesGajiSearch;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * ProsesGajiController implements the CRUD actions for ProsesGaji model.
 */
class ProsesGajiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'proses-ulang' => ['post']
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA]);
        $index = 'index';
        $finish = 'index-finish';

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA], [], ProsesGaji::selesai);
        $index = Url::to('index');
        $finish = Url::to('index-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuNol()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [1]);
        $index = Url::to('index-kbu-nol');
        $finish = Url::to('index-kbu-nol-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuNolFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [1], ProsesGaji::selesai);
        $index = Url::to('index-kbu-nol');
        $finish = Url::to('index-kbu-nol-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuSatu()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [2, 8]);
        $index = Url::to('index-kbu-satu');
        $finish = Url::to('index-kbu-satu-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuSatuFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [2, 8], ProsesGaji::selesai);
        $index = Url::to('index-kbu-satu');
        $finish = Url::to('index-kbu-satu-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuDua()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [3]);
        $index = Url::to('index-kbu-dua');
        $finish = Url::to('index-kbu-dua-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuDuaFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [3], ProsesGaji::selesai);
        $index = Url::to('index-kbu-dua');
        $finish = Url::to('index-kbu-dua-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuTiga()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [4]);
        $index = Url::to('index-kbu-tiga');
        $finish = Url::to('index-kbu-tiga-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuTigaFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [4], ProsesGaji::selesai);
        $index = Url::to('index-kbu-tiga');
        $finish = Url::to('index-kbu-tiga-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuEmpat()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [5]);
        $index = Url::to('index-kbu-empat');
        $finish = Url::to('index-kbu-empat-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuEmpatFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [5], ProsesGaji::selesai);
        $index = Url::to('index-kbu-empat');
        $finish = Url::to('index-kbu-empat-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }


    /**
     * @return string
     */
    public function actionIndexKbuLima()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [6, 7, 15]);
        $index = Url::to('index-kbu-lima');
        $finish = Url::to('index-kbu-lima-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }


    /**
     * @return string
     */
    public function actionIndexKbuLimaFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [6, 7, 15], ProsesGaji::selesai);
        $index = Url::to('index-kbu-lima');
        $finish = Url::to('index-kbu-lima-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuTigaEmpatGt()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA], [16, 17]);
        $index = Url::to('index-kbu-tiga-empat-gt');
        $finish = Url::to('index-kbu-tiga-empat-gt-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuTigaEmpatGtFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA], [16, 17], ProsesGaji::selesai);
        $index = Url::to('index-kbu-tiga-empat-gt');
        $finish = Url::to('index-kbu-tiga-empat-gt-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBca()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA]);
        $index = Url::to('index-bca');
        $finish = Url::to('index-bca-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA], [], ProsesGaji::selesai);
        $index = Url::to('index-bca');
        $finish = Url::to('index-bca-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaSatu()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [2, 8]);
        $index = Url::to('index-bca-satu');
        $finish = Url::to('index-bca-satu-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaSatuFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [2, 8], ProsesGaji::selesai);
        $index = Url::to('index-bca-satu');
        $finish = Url::to('index-bca-satu-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaDua()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [3]);
        $index = Url::to('index-bca-dua');
        $finish = Url::to('index-bca-dua-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaDuaFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [3], ProsesGaji::selesai);
        $index = Url::to('index-bca-dua');
        $finish = Url::to('index-bca-dua-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaTiga()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [4]);
        $index = Url::to('index-bca-tiga');
        $finish = Url::to('index-bca-tiga-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaTigaFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [4], ProsesGaji::selesai);
        $index = Url::to('index-bca-tiga');
        $finish = Url::to('index-bca-tiga-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaAda()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_ADA], [3, 4]);
        $index = Url::to('index-bca-ada');
        $finish = Url::to('index-bca-ada-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaAdaFinish()
    {
        $searchModel = new ProsesGajiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_ADA], [3, 4], ProsesGaji::selesai);
        $index = Url::to('index-bca-ada');
        $finish = Url::to('index-bca-ada-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * Displays a single ProsesGaji model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);
        $searchModel = new GajiBulananSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $query = GajiBulanan::find()->andWhere(['proses_gaji_id' => $model->id]);
        $providerGajiBulanan = new ActiveDataProvider([
            'query' => $query,
        ]);
        $queryRevisi = RevisiGaji::find()->andWhere(['proses_gaji_id' => $model->id])->orderBy('revisi_ke DESC');
        $providerRevisiGaji = new ActiveDataProvider([
            'query' => $queryRevisi,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);

//        print_r($searchModel->potonganPkp(132021000));

//         echo "<pre>";
//         print_r($model->formatingProsesGaji());
//         echo "</pre>";
//         return;

        return $this->render('view', [
            'model' => $model,
            'providerGajiBulanan' => $providerGajiBulanan,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'providerRevisiGaji' => $providerRevisiGaji
        ]);
    }

    /**
     * Creates a new ProsesGaji model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProsesGaji();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProsesGaji model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new ProsesGaji();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing ProsesGaji model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the ProsesGaji model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ProsesGaji the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id)
    {
        if (($model = ProsesGaji::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return GajiBulanan|null
     * @throws NotFoundHttpException
     */
    protected function findModelGajiBulanan(string $id)
    {
        if (($model = GajiBulanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     * @throws MpdfException
     * @throws CrossReferenceException
     * @throws PdfParserException
     * @throws PdfTypeException
     * @throws InvalidConfigException
     */
    public function actionSlips(string $id)
    {
        $model = $this->findModel($id);
        $filePath = ProsesGaji::filePathGaji;
        $fileName = $model->id . '.pdf';
        $model->createFileSlipGaji();
        return Yii::$app->response->sendFile($filePath . $fileName, null, ['inline' => true]);
    }

    /**
     * @param string $id
     * @param int $status
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionSetFinishingStatus(string $id, int $status)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->finishing_status = $status;
            if ($model->save(false)) {
                $model->afterFinish();
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
            Yii::info($model->errors, 'exception');
        }
        return $response->data;
    }

    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionProsesUlang(string $id)
    {
        $model = $this->findModel($id);

        if ($model) {
            $model->status = ProsesGaji::dibuat;
            $model->save();
        }

        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return \yii\console\Response|Response
     */
    public function actionPrintRevisi(string $id)
    {
        $filePath = ProsesGaji::filePathGaji;
        $fileName = $id . '.pdf';
        return Yii::$app->response->sendFile($filePath . $fileName, null, ['inline' => true]);

    }

    /**
     * @param string $id
     * @return mixed
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws NotFoundHttpException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function actionPrintSlip(string $id)
    {
        $model = $this->findModelGajiBulanan($id);
        $content = $this->renderAjax('_slip', ['model' => $model]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@frontend/web/css/slip.css',
            'cssInline' => 'table{font-size:10px}',
            'options' => ['title' => Yii::$app->name],
            'marginTop' => 5,
            'marginBottom' => 5,
            'marginLeft' => 5,
            'marginRight' => 5,
        ]);
//        return $content;

        return $pdf->render();

    }

    /**
     * @param string $id
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionProsesUlangPph21(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            if ($model->save(false)) {
                $model->prosesUlangPph21();
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
            Yii::info($model->errors, 'exception');
        }
        return $response->data;
    }

}

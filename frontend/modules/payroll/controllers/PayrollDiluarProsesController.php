<?php

namespace frontend\modules\payroll\controllers;

use frontend\modules\payroll\models\form\PayrollDiluarProsesForm;
use frontend\modules\payroll\models\PayrollDiluarProses;
use frontend\modules\payroll\models\search\PayrollDiluarProsesSearch;
use kartik\form\ActiveForm;
use PhpOffice\PhpSpreadsheet\Exception;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PayrollDiluarProsesController implements the CRUD actions for PayrollDiluarProses model.
 */
class PayrollDiluarProsesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PayrollDiluarProses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PayrollDiluarProsesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PayrollDiluarProses model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the PayrollDiluarProses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PayrollDiluarProses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PayrollDiluarProses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Creates a new PayrollDiluarProses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PayrollDiluarProses();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PayrollDiluarProses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new PayrollDiluarProses();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing PayrollDiluarProses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return array|mixed|string
     */
    public function actionUploadTemplate()
    {
        $model = new PayrollDiluarProsesForm();
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [$model->errors], 'status' => false, 'message' => Yii::t('frontend', 'Gagal Upload Data')];
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->upload()) {
                try {
                    $dataPegawai = $model->extractData();
                    $model->simpan($dataPegawai);
                    $response->data = ['data' => ['url' => Url::to(['index'])], 'status' => true, 'message' => Yii::t('frontend', 'Berhasil Upload Data')];
                } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
            return $response->data;
        }
        return $this->renderAjax('_form-upload', ['model' => $model]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadTemplateFormValidation()
    {
        $model = new PayrollDiluarProsesForm();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return array|mixed
     * @throws MethodNotAllowedHttpException
     */
    public function actionGetTemplate()
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        if ($request->isPost && $request->isAjax) {
            $url = Url::to(['get-payroll-luar-proses-form', 'PayrollDiluarProses' => $request->post()]);
            $response->data = ['status' => true, 'url' => $url];
            return $response->data;
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function actionGetPayrollLuarProsesForm()
    {
        $model = new PayrollDiluarProses();
        return $model->downloadFile();
    }
}

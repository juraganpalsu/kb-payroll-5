<?php

namespace frontend\modules\payroll\controllers;

use frontend\models\User;
use frontend\modules\payroll\models\form\PayrollBpjsForm;
use frontend\modules\payroll\models\PayrollBpjs;
use frontend\modules\payroll\models\PayrollBpjsDetail;
use frontend\modules\payroll\models\PayrollBpjsTkDetail;
use frontend\modules\payroll\models\search\PayrollBpjsDetailSearch;
use frontend\modules\payroll\models\search\PayrollBpjsSearch;
use frontend\modules\payroll\models\search\PayrollBpjsTkDetailSearch;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\form\ActiveForm;
use mdm\admin\components\Helper;
use PhpOffice\PhpSpreadsheet\Exception;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PayrollBpjsController implements the CRUD actions for PayrollBpjs model.
 */
class PayrollBpjsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndexKbuSatu()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [2, 8]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndexKbuDua()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [3]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndexKbuTiga()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [4]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndexKbuEmpat()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [5]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndexKbuTigaEmpatGt()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA], [16, 17]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndexKbu()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndexAda()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::ADA]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all PayrollBpjs models.
     * @return mixed
     */
    public function actionIndexBca()
    {
        $searchModel = new PayrollBpjsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PayrollBpjs model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);
        $queryDetail = PayrollBpjsDetail::find()->andWhere(['bpjs_id' => $model->id]);
        $searchModel = new PayrollBpjsDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        $searchModelKetenagakerjaan = new PayrollBpjsTkDetailSearch();
        $dataProviderBpjsKetenagakerjaan = $searchModelKetenagakerjaan->search(Yii::$app->request->queryParams, $id);

        $bolehAkses = false;
        $user = User::pegawai();
        if (is_object($user)) {
            if ($userGologan = $user->pegawaiGolongan) {
                if (Helper::checkRoute('/payroll/payroll-bpjs/pic-bpjs') || $userGologan->golongan->urutan > $model->golongan->urutan) {
                    $bolehAkses = true;
                }
            }
        }

        if (!$bolehAkses) {
            return $this->render('@frontend/modules/masterdata/views/default/perhatian');
        }
        $providerPayrollBpjsDetail = new ActiveDataProvider([
            'query' => $queryDetail,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerPayrollBpjsDetail' => $providerPayrollBpjsDetail,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelKetenagakerjaan' => $searchModelKetenagakerjaan,
            'dataProviderBpjsKetenagakerjaan' => $dataProviderBpjsKetenagakerjaan,
        ]);
    }

    /**
     * Creates a new PayrollBpjs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PayrollBpjs();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PayrollBpjs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PayrollBpjs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdateDetail($id)
    {
        $model = $this->findModelDetail($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_formPayrollBpjsDetail', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouDetailValidation()
    {
        $model = new PayrollBpjsDetail();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new PayrollBpjs();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing PayrollBpjs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteDetail(string $id)
    {
        $model = $this->findModelDetail($id);
        $model->delete();
        return $this->redirect(Yii::$app->request->referrer);

    }


    /**
     * Finds the PayrollBpjs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PayrollBpjs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PayrollBpjs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the PayrollBpjs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PayrollBpjsDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelDetail(string $id): PayrollBpjsDetail
    {
        if (($model = PayrollBpjsDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Finds the PayrollBpjsTkDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PayrollBpjsTkDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelDetailTk(string $id): PayrollBpjsTkDetail
    {
        if (($model = PayrollBpjsTkDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return bool
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionDownloadTemplate(string $id): bool
    {
        $model = $this->findModel($id);
        if ($model) {
            return $model->downloadTemplate();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));

    }


    /**
     * @return string
     */
    public function actionUploadTemplate()
    {
        $model = new PayrollBpjsForm();
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [$model->errors], 'status' => false, 'message' => Yii::t('frontend', 'Gagal Upload Data')];
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->upload()) {
                try {
                    $dataPegawai = $model->extractDataBpjs();
                    $model->simpanBpjs($dataPegawai);

                } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $dataPegawai['payrollPeriode']['templateId']])], 'status' => true, 'message' => Yii::t('frontend', 'Berhasil Upload Data')];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-upload', ['model' => $model]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadTemplateFormValidation()
    {
        $model = new PayrollBpjsForm();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return array|mixed|void
     * @throws NotFoundHttpException
     */
    public function actionGenerateIuranBpjs(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $model->generateIuranBpjs();
            $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            return $response->data;
        }

    }


    /**
     * Updates an existing PayrollBpjsTkDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdateDetailTk(string $id)
    {
        $model = $this->findModelDetailTk($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form-payroll-bpjs-tk', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouDetailTkValidation()
    {
        $model = new PayrollBpjsTkDetail();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteDetailTk(string $id)
    {
        $model = $this->findModelDetailTk($id);
        $model->delete();
        return $this->redirect(Yii::$app->request->referrer);

    }


    /**
     * @param string $id
     * @return array
     */
    public function actionDownloadBpjsBtn(string $id): array
    {
        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal download data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $response->data = ['data' => ['url' => Url::to(['download-bpjs', 'id' => $id])], 'message' => Yii::t('frontend', 'Berhasil download data.'), 'status' => true];
        }
        return $response->data;
    }


    /**
     * @param string $id
     * @return bool
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionDownloadBpjs(string $id): bool
    {
        $model = $this->findModel($id);
        if ($model) {
            return $model->downloadBpjs();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));

    }
}

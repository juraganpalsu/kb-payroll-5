<?php

namespace frontend\modules\payroll\controllers;

use frontend\modules\payroll\models\form\GajiBoronganForm;
use frontend\modules\payroll\models\GajiBorongan;
use frontend\modules\payroll\models\GajiBoronganHeader;
use frontend\modules\payroll\models\search\GajiBoronganHeaderSearch;
use frontend\modules\payroll\models\search\GajiBoronganSearch;
use kartik\form\ActiveForm;
use PhpOffice\PhpSpreadsheet\Exception;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * GajiBoronganController implements the CRUD actions for GajiBorongan model.
 */
class GajiBoronganController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GajiBorongan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GajiBoronganHeaderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all GajiBorongan models.
     * @return mixed
     */
    public function actionIndexFinish()
    {
        $searchModel = new GajiBoronganHeaderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, GajiBoronganHeader::selesai);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModelHeader($id);
//        $fm = new GajiBoronganForm();
//        print_r($fm->extractDataGaji());
        $query = GajiBorongan::find()->andWhere(['gaji_borongan_header_id' => $model->id]);
        $searchModel = new GajiBoronganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $providerGajiBorongan = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerGajiBorongan' => $providerGajiBorongan,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new GajiBorongan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GajiBoronganHeader();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing GajiBorongan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing GajiBorongan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdateDetail(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->gajiBoronganHeader->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('_form_detail', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new GajiBoronganHeader();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing GajiBorongan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id)
    {
        $modelHeader = $this->findModelHeader($id);
        $items = $modelHeader->gajiBorongans;
        $modelHeader->delete();
        foreach ($items as $gajiBorongan) {
            $gajiBorongan->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteDetail(string $id)
    {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(Yii::$app->request->referrer);

    }


    /**
     * Finds the GajiBorongan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return GajiBorongan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id)
    {
        if (($model = GajiBorongan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param $id
     * @return GajiBoronganHeader|null
     * @throws NotFoundHttpException
     */
    protected function findModelHeader($id)
    {
        if (($model = GajiBoronganHeader::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @return array|mixed|string
     */
    public function actionDownloadTemplateForm()
    {
        $formModel = new GajiBoronganForm();
        $formModel->scenario = 'download';
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($formModel->load($request->post()) && $formModel->validate()) {
                $response->data = ['data' => ['url' => Url::to(['download-template', 'GajiBoronganForm' => $formModel->attributes])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-download-template', ['model' => $formModel]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionDownloadTemplateFormValidation()
    {
        $model = new GajiBoronganForm();
        $model->scenario = 'download';
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @return bool
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDownloadTemplate(string $id)
    {
        $modelData = $this->findModelHeader($id);
        if ($modelData) {
            $model = new GajiBoronganForm();
            return $model->downloadTemplateGaji($modelData);
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return string
     */
    public function actionUploadTemplate()
    {
        $model = new GajiBoronganForm();
        $model->scenario = 'upload';
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [$model->errors], 'status' => false, 'message' => Yii::t('app', 'Gagal Upload Data')];
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->upload()) {
                try {
                    $dataPegawai = $model->extractDataGaji();
                    if (isset($dataPegawai['payrollPeriode'])) {
                        $model->simpanGaji($dataPegawai);
                    }
                    $response->data = ['data' => ['url' => Url::to(['view', 'id' => $dataPegawai['payrollPeriode']['id']])], 'status' => true, 'message' => Yii::t('app', 'Berhasil Upload Data')];
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
            return $response->data;
        }
        return $this->render('_form-upload-template', ['model' => $model]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadTemplateFormValidation()
    {
        $model = new GajiBoronganForm();
        $model->scenario = 'upload';
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @return array|mixed|string
     */
    public function actionRekapGajiForm()
    {
        $formModel = new GajiBoronganForm();
        $formModel->scenario = 'download';
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($formModel->load($request->post()) && $formModel->validate()) {
                $response->data = ['data' => ['url' => Url::to(['rekap-gaji', 'GajiBoronganForm' => $formModel->attributes])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-rekap-gaji', ['model' => $formModel]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionRekapGajiFormValidation()
    {
        $model = new GajiBoronganForm();
        $model->scenario = 'download';
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionRekapGaji(string $id)
    {
        $modelData = $this->findModelHeader($id);
        if ($modelData) {
            $model = new GajiBoronganForm();
            return $model->downloadRekapGaji($modelData);
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }


    /**
     * @param string $id
     * @param int $status
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionSetFinishingStatus(string $id, int $status)
    {
        $model = $this->findModelHeader($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->finishing_status = $status;
            if ($model->save()) {
                $model->afterFinish();
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
            Yii::info($model->errors, 'exception');
        }
        return $response->data;
    }


}

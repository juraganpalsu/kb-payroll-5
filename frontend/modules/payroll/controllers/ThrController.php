<?php

namespace frontend\modules\payroll\controllers;

use common\models\Golongan;
use frontend\modules\payroll\models\search\ThrDetailSearch;
use frontend\modules\payroll\models\search\ThrSearch;
use frontend\modules\payroll\models\Thr;
use frontend\modules\payroll\models\ThrDetail;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * ThrController implements the CRUD actions for Thr model.
 */
class ThrController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA]);
        $index = 'index';
        $finish = 'index-finish';

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA], []);
        $index = Url::to('index');
        $finish = Url::to('index-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuSatu()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::SATU_H, Golongan::SATU_B]);
        $index = Url::to('index-kbu-satu');
        $finish = Url::to('index-kbu-satu-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuSatuFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::SATU_H, Golongan::SATU_B]);
        $index = Url::to('index-kbu-satu');
        $finish = Url::to('index-kbu-satu-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuDua()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::DUA]);
        $index = Url::to('index-kbu-dua');
        $finish = Url::to('index-kbu-dua-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuDuaFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::DUA]);
        $index = Url::to('index-kbu-dua');
        $finish = Url::to('index-kbu-dua-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuTiga()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::TIGA]);
        $index = Url::to('index-kbu-tiga');
        $finish = Url::to('index-kbu-tiga-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuTigaFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::TIGA]);
        $index = Url::to('index-kbu-tiga');
        $finish = Url::to('index-kbu-tiga-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuEmpat()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::EMPAT]);
        $index = Url::to('index-kbu-empat');
        $finish = Url::to('index-kbu-empat-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuEmpatFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::EMPAT]);
        $index = Url::to('index-kbu-empat');
        $finish = Url::to('index-kbu-empat-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }


    /**
     * @return string
     */
    public function actionIndexKbuLima()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::LIMA, Golongan::ENAM, Golongan::TUJUH]);
        $index = Url::to('index-kbu-lima');
        $finish = Url::to('index-kbu-lima-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }


    /**
     * @return string
     */
    public function actionIndexKbuLimaFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU], [Golongan::LIMA, Golongan::ENAM, Golongan::TUJUH]);
        $index = Url::to('index-kbu-lima');
        $finish = Url::to('index-kbu-lima-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuTigaEmpatGt()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA], [Golongan::TIGA_GT, Golongan::EMPAT_GT]);
        $index = Url::to('index-kbu-tiga-empat-gt');
        $finish = Url::to('index-kbu-tiga-empat-gt-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexKbuTigaEmpatGtFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::KBU, PerjanjianKerja::ADA], [Golongan::TIGA_GT, Golongan::EMPAT_GT]);
        $index = Url::to('index-kbu-tiga-empat-gt');
        $finish = Url::to('index-kbu-tiga-empat-gt-finish');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBca()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA]);
        $index = Url::to('index-bca');
        $finish = Url::to('index-bca-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA], []);
        $index = Url::to('index-bca');
        $finish = Url::to('index-bca-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaSatu()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [Golongan::SATU_H, Golongan::SATU_B]);
        $index = Url::to('index-bca-satu');
        $finish = Url::to('index-bca-satu-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaSatuFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [Golongan::SATU_H, Golongan::SATU_B]);
        $index = Url::to('index-bca-satu');
        $finish = Url::to('index-bca-satu-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaDua()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [Golongan::DUA]);
        $index = Url::to('index-bca-dua');
        $finish = Url::to('index-bca-dua-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaDuaFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [Golongan::DUA]);
        $index = Url::to('index-bca-dua');
        $finish = Url::to('index-bca-dua-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaTiga()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [Golongan::TIGA]);
        $index = Url::to('index-bca-tiga');
        $finish = Url::to('index-bca-tiga-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaTigaFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_KBU], [Golongan::TIGA]);
        $index = Url::to('index-bca-tiga');
        $finish = Url::to('index-bca-tiga-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaAda()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_ADA], [Golongan::DUA, Golongan::TIGA]);
        $index = Url::to('index-bca-ada');
        $finish = Url::to('index-bca-ada-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexBcaAdaFinish()
    {
        $searchModel = new ThrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [PerjanjianKerja::BCA_ADA], [Golongan::DUA, Golongan::TIGA]);
        $index = Url::to('index-bca-ada');
        $finish = Url::to('index-bca-ada-finish');

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'index' => $index,
            'finish' => $finish,
        ]);
    }

    /**
     * Displays a single Thr model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new ThrDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $query = ThrDetail::find()->andWhere(['thr_id' => $model->id]);
        $providerThrDetail = new ActiveDataProvider([
            'query' => $query,
        ]);

//        echo "<pre>";
//        print_r($model->hitungThr());
//        echo "</pre>";

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'providerThrDetail' => $providerThrDetail,
        ]);
    }

    /**
     * Creates a new Thr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Thr();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Thr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        /** @var Request $request */
//        $request = Yii::$app->request;
//        if ($request->isAjax && $request->isPost) {
//            /** @var Response $response */
//            $response = Yii::$app->response;
//            $response->format = Response::FORMAT_JSON;
//            if ($model->load($request->post()) && $model->save()) {
//                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
//            } else {
//                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
//            }
//            return $response->data;
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new Thr();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Deletes an existing Thr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        foreach ($model->thrDetails as $detail) {
            $detail->delete();
        }

        return $this->redirect(['index']);
    }


    /**
     * Finds the Thr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Thr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Thr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

}

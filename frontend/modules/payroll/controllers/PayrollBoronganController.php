<?php

namespace frontend\modules\payroll\controllers;

use common\components\Status;
use frontend\modules\payroll\models\PayrollBorongan;
use frontend\modules\payroll\models\search\PayrollBoronganSearch;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * PayrollBoronganController implements the CRUD actions for PayrollBorongan model.
 */
class PayrollBoronganController extends Controller
{
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PayrollBorongan models.
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new PayrollBoronganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PayrollBorongan model.
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView(string $id): string
    {
        $model = $this->findModel($id);
        $providerPayrollBoronganDetail = new ArrayDataProvider([
            'allModels' => $model->payrollBoronganDetails,
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerPayrollBoronganDetail' => $providerPayrollBoronganDetail,
        ]);
    }

    /**
     * Creates a new PayrollBorongan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PayrollBorongan();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PayrollBorongan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouValidation(): array
    {
        $model = new PayrollBorongan();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Deletes an existing PayrollBorongan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id): string
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the PayrollBorongan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PayrollBorongan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id): PayrollBorongan
    {
        if (($model = PayrollBorongan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }


    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionAssign($id): array
    {
        $items = Yii::$app->getRequest()->post('items', []);
        $success = 0;
        $model = $this->findModel($id);
        if ($model->last_status == Status::OPEN) {
            $success = $model->createDetails($items);
        }
        Yii::$app->getResponse()->format = 'json';
        return array_merge($model->getListPegawai(), ['success' => $success]);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionRevoke($id): array
    {
        $items = Yii::$app->getRequest()->post('items', []);
        $success = 0;
        $model = $this->findModel($id);
        if ($model->last_status == Status::OPEN) {
            $success = $model->deleteDetails($items);
        }
        Yii::$app->getResponse()->format = 'json';
        return array_merge($model->getListPegawai(), ['success' => $success]);
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSubmit(string $id): array
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Belum ada data karyawan.'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            $model = $this->findModel($id);
            $model->last_status = Status::FINISHED;
            if ($model->save()) {
                $response->data = [
                    'data' => ['url' => Url::to(['view', 'id' => $model->id])],
                    'message' => [],
                    'status' => true,
                ];
            }
        }
        return $response->data;
    }

}

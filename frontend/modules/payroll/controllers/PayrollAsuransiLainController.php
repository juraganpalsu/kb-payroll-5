<?php

namespace frontend\modules\payroll\controllers;

use frontend\modules\payroll\models\form\PayrollAsuransiLainForm;
use frontend\modules\payroll\models\form\PayrollBpjsForm;
use frontend\modules\payroll\models\PayrollAsuransiLain;
use frontend\modules\payroll\models\search\PayrollAsuransiLainSearch;
use kartik\form\ActiveForm;
use PhpOffice\PhpSpreadsheet\Exception;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PayrollAsuransiLainController implements the CRUD actions for PayrollAsuransiLain model.
 */
class PayrollAsuransiLainController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PayrollAsuransiLain models.
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new PayrollAsuransiLainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView(string $id): string
    {
        $model = $this->findModel($id);
        $providerPayrollAsuransiLainDetail = new ArrayDataProvider([
            'allModels' => $model->payrollAsuransiLainDetails,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerPayrollAsuransiLainDetail' => $providerPayrollAsuransiLainDetail,
        ]);
    }

    /**
     * Creates a new PayrollAsuransiLain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new PayrollAsuransiLain();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PayrollAsuransiLain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return Response|string
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new PayrollAsuransiLain();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Deletes an existing PayrollAsuransiLain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id): Response
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the PayrollAsuransiLain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PayrollAsuransiLain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id): PayrollAsuransiLain
    {
        if (($model = PayrollAsuransiLain::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }


    /**
     * @param string $id
     * @return bool
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionDownloadTemplate(string $id): bool
    {
        $model = $this->findModel($id);
        if ($model) {
//            print_r($model->getPegawais());
//            return true;
            return $model->downloadTemplate();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));

    }


    /**
     * @return string
     */
    public function actionUploadTemplate()
    {
        $model = new PayrollAsuransiLainForm();
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [$model->errors], 'status' => false, 'message' => Yii::t('frontend', 'Gagal Upload Data')];
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->upload()) {
                try {
                    $dataPegawai = $model->extractData();
                    $model->simpan($dataPegawai);
                } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $dataPegawai['payrollPeriode']['templateId']])], 'status' => true, 'message' => Yii::t('frontend', 'Berhasil Upload Data')];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-upload', ['model' => $model]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadTemplateFormValidation()
    {
        $model = new PayrollBpjsForm();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

}

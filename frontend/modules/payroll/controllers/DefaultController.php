<?php

namespace frontend\modules\payroll\controllers;

use common\models\Golongan;
use frontend\models\User;
use frontend\modules\payroll\models\search\GajiBulananSearch;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `payroll` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionMyPayroll()
    {
        $searchModel = new GajiBulananSearch();
//        $bolehAkses = false;
//        $user = User::pegawai();
//        if (is_object($user)) {
//            if ($userGologan = $user->pegawaiGolongan) {
//                if ($userGologan->golongan_id == Golongan::SATU_H) {
//                    $bolehAkses = true;
//                }
//            }
//        }
//
//        if (!$bolehAkses) {
//            return $this->render('@frontend/modules/masterdata/views/default/perhatian');
//        }
        $dataProvider = $searchModel->searchMyPayroll(Yii::$app->request->queryParams);

        return $this->render('my-payroll', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

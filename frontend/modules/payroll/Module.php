<?php

namespace frontend\modules\payroll;

/**
 * payroll module definition class
 */
class Module extends \yii\base\Module
{

    const max_biaya_jabatan = 500000;

    const tarif_pkp_1 = ['pkp' => 50000000, 'tarif_npwp' => 5, 'tarif_no_npwp' => 6];

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\payroll\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

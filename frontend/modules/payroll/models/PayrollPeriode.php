<?php

namespace frontend\modules\payroll\models;

use DateTime;
use Exception;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\base\PayrollPeriode as BasePayrollPeriode;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payroll_periode".
 *
 * @property string $namaPeriode
 */
class PayrollPeriode extends BasePayrollPeriode
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipe', 'tanggal_awal', 'tanggal_akhir'], 'required'],
            [['tipe', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
//            [['tanggal_awal'], 'mulaiBerlakuValidation'],
            [['tanggal_akhir'], 'cekTanggalValidation'],
        ];
    }

    /**
     * @param string $attribute
     */
    public function mulaiBerlakuValidation(string $attribute)
    {
        /** @var self $query */
        $query = self::find()->andWhere(['>=', 'tanggal_akhir', $this->$attribute])->andWhere(['tipe' => $this->tipe])->all();
        if ($query) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
        }
    }

    /**
     * @param string $attribute
     */
    public function cekTanggalValidation(string $attribute)
    {
        try {
            $awal = new DateTime($this->tanggal_awal);
            $akhir = new DateTime($this->tanggal_akhir);

            if ($akhir < $awal) {
                $this->addError($attribute, Yii::t('frontend', 'Tanggal Awal Harus Lebih Kecil'));
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * @return string
     */
    public function getNamaPeriode()
    {
        return ' (' . strtoupper(date('M', strtotime($this->tanggal_akhir)) . '-' . date('Y', strtotime($this->tanggal_akhir))) . ') ' . $this->_tipe()[$this->tipe] . ' ' . date('d-m-Y', strtotime($this->tanggal_awal)) . ' ' . date('d-m-Y', strtotime($this->tanggal_akhir));
    }

    /**
     *
     * tipe menggunakan jenis gaji yg ada pada master data
     *
     * @return array
     *
     */
    public function _tipe()
    {
        return self::masterDataClass()->jenisGaji;
    }

    /**
     * @return MasterDataDefault
     */
    public static function masterDataClass()
    {
        return new MasterDataDefault();
    }

    /**
     * @param int $tipeGaji
     * @param string $tanggal
     * @return array|ActiveRecord|null
     */
    public static function getPayrollPeriodeByDate(int $tipeGaji, string $tanggal)
    {
        return self::find()->andWhere(['tipe' => $tipeGaji])->andWhere("'$tanggal' BETWEEN tanggal_awal AND tanggal_akhir")->asArray()->one();
    }

}

<?php

namespace frontend\modules\payroll\models;

use common\components\Status;
use common\models\Absensi as AbsensiAlias;
use common\models\Golongan;
use common\models\Spl as SplAlias;
use common\models\SplTemplate as SplTemplateAlias;
use common\models\TimeTable as TimeTableAlias;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use frontend\models\Absensi;
use frontend\models\TimeTable;
use frontend\modules\izin\models\AbsensiTidakLengkap;
use frontend\modules\izin\models\IzinJenis;
use frontend\modules\izin\models\IzinTanpaHadir;
use frontend\modules\izin\models\SetengahHari;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\masterdata\models\PGajiPokok;
use frontend\modules\masterdata\models\PUangMakan;
use frontend\modules\payroll\models\base\ProsesGaji as BaseProsesGaji;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\mpdf\Pdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use yii\swiftmailer\Mailer;

/**
 * This is the model class for table "proses_gaji".
 *
 * @property array $_status
 */
class ProsesGaji extends BaseProsesGaji
{

    const dibuat = 1;
    const diproses = 2;
    const selesai = 3;

    const filePathGaji = '/home/gaji/';

    public $_status = [self::dibuat => 'DIBUAT', self::diproses => 'DIPROSSES', self::selesai => 'SELESAI'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payroll_periode_id', 'golongan_id', 'bisnis_unit'], 'required'],
            [['golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'status', 'finishing_status', 'updated_by', 'deleted_by', 'lock', 'status'], 'integer'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'payroll_periode_id', 'created_by_pk'], 'string', 'max' => 32],
            [['status', 'finishing_status'], 'default', 'value' => 1],
            [['lock',], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['bisnis_unit', 'doubleValidation']
        ];
    }

    /**
     * @param string $attribute
     */
    public function doubleValidation(string $attribute)
    {
        if ($this->isNewRecord) {
            $query = self::findOne(['payroll_periode_id' => $this->payroll_periode_id, 'golongan_id' => $this->golongan_id, 'bisnis_unit' => $this->bisnis_unit]);
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Penggajian telah ada.'));
            }
        }
    }

    /**
     * @return PerjanjianKerja
     */
    public static function modelPerjanjianKerja(): PerjanjianKerja
    {
        return new PerjanjianKerja();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
//    public function afterSave($insert, $changedAttributes)
//    {
//        parent::afterSave($insert, $changedAttributes);
//        $this->simpanDataGajiBulanan();
//    }

    /**
     * @param string $pegawaiId
     * @return ActiveQuery
     */
    public function queryAbsensi(string $pegawaiId): ActiveQuery
    {
        $query = Absensi::find();
        $query->andWhere(['BETWEEN', 'absensi.tanggal', $this->payrollPeriode->tanggal_awal, $this->payrollPeriode->tanggal_akhir]);
        $query->joinWith(['perjanjianKerja' => function (ActiveQuery $query) {
            if (!empty($this->bisnis_unit)) {
                $query->andWhere(['perjanjian_kerja.kontrak' => $this->bisnis_unit]);
            }
        }]);
        $query->joinWith(['pegawaiGolongan' => function (ActiveQuery $query) {
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);
        }]);
        $query->andWhere(['absensi.pegawai_id' => $pegawaiId]);

        $query->orderBy('absensi.tanggal ASC');
        return $query;
    }

    /**
     * @param string $pegawaiId
     * @return array
     */
    private function hitungPengaliUml(string $pegawaiId): array
    {
        $query = $this->queryAbsensi($pegawaiId);
        $dataAbsensis = $query->all();
        $pengaliUml1 = 0;
        $pengaliUml2 = 0;

        if (!empty($dataAbsensis) && $this->payrollPeriode->tipe == MasterDataDefault::POKOK_FIX_H) {
            /** @var Absensi $dataAbsensi */
            foreach ($dataAbsensis as $dataAbsensi) {
                $hitungpengali = $dataAbsensi->hitungPengaliUangMakanLembur();
                if ($hitungpengali == 1) {
                    $pengaliUml1 += 1;
                }
                if ($hitungpengali > 1) {
                    $pengaliUml2 += $dataAbsensi->hitungPengaliUangMakanLembur();
                }
            }
        }

        return [1 => $pengaliUml1, 2 => $pengaliUml2];
    }

    /**
     * @param MasterDataDefault $masterData
     * @return PGajiPokok
     */
    public function hitungGajiPokokHarianPerPeriode(MasterDataDefault $masterData): PGajiPokok
    {
        $query = Absensi::find();
        $query->andWhere(['BETWEEN', 'absensi.tanggal', $this->payrollPeriode->tanggal_awal, $this->payrollPeriode->tanggal_akhir]);
        $query->andWhere(['absensi.pegawai_id' => $masterData->pegawai_id, 'status_kehadiran' => AbsensiAlias::MASUK]);
        $query->orderBy('absensi.tanggal ASC');
        $absensis = $query->all();

        $gajiPokok = 0;
        if (!empty($absensis)) {
            /** @var Absensi $absensi */
            foreach ($absensis as $absensi) {
                $hariKerjaAktif = $absensi->pegawaiSistemKerja->sistemKerja->hari_kerja_aktif;
                if ($hariKerjaAktif > 0) {
                    $gajiPokok += $masterData->getGajiPokokAktif($absensi->tanggal) / $hariKerjaAktif;
                }
            }
        }

        //Jika libur hadir, gaji pokoknya dikurangi
        $queryLiburHadir = $this->liburHadirQuery($masterData->pegawai_id)->all();
        if (!empty($queryLiburHadir)) {
            /** @var Absensi $liburHadir */
            foreach ($queryLiburHadir as $liburHadir) {
                $hariKerjaAktif = $liburHadir->pegawaiSistemKerja->sistemKerja->hari_kerja_aktif;
                if ($hariKerjaAktif > 0) {
                    $gajiPokok -= $masterData->getGajiPokokAktif($liburHadir->tanggal) / $hariKerjaAktif;
                }
            }
        }

        // ATL menambah hari kerja
        $queryAtl = $this->queryAtl($masterData->pegawai_id)->all();
        if (!empty($queryAtl)) {
            /** @var AbsensiTidakLengkap $atl */
            foreach ($queryAtl as $atl) {
                $sistemKerja = $atl->pegawai->getPegawaiSistemKerjaUntukTanggal($atl->tanggal);
                if ($sistemKerja) {
                    $hariKerjaAktif = $sistemKerja->sistemKerja->hari_kerja_aktif;
                    if ($hariKerjaAktif) {
                        $gajiPokok += $masterData->getGajiPokokAktif($atl->tanggal) / $hariKerjaAktif;
                    }
                }
            }
        }

        // Setengah Hari menambah hari setengah kerja
        $querySetengahHari = $this->querySetengahHari($masterData->pegawai_id)->all();
        if (!empty($querySetengahHari)) {
            /** @var SetengahHari $sh */
            foreach ($querySetengahHari as $sh) {
                $sistemKerja = $sh->pegawai->getPegawaiSistemKerjaUntukTanggal($sh->tanggal);
                if ($sistemKerja) {
                    $hariKerjaAktif = $sistemKerja->sistemKerja->hari_kerja_aktif;
                    if ($hariKerjaAktif) {
                        $gajiPokok += $masterData->getGajiPokokAktif($sh->tanggal) / $hariKerjaAktif * 0.5;
                    }
                }
            }
        }

        $izinDispensasi = $this->hitungIjinTanpahadir($masterData->pegawai_id, 'D', true);
        if (!empty($izinDispensasi)) {
            $queryAbsensi = $this->queryAbsensi($masterData->pegawai_id)
                ->andWhere(['IN', 'absensi.tanggal', $izinDispensasi['tanggalIzin']])
                ->andWhere(['absensi.status_kehadiran' => AbsensiAlias::IJIN])->all();
            if ($queryAbsensi) {
                /** @var Absensi $absensiIzin */
                foreach ($queryAbsensi as $absensiIzin) {
                    $hariKerjaAktif = $absensiIzin->pegawaiSistemKerja->sistemKerja->hari_kerja_aktif;
                    if ($hariKerjaAktif > 0) {
                        $gajiPokok += $masterData->getGajiPokokAktif($absensiIzin->tanggal) / $hariKerjaAktif;
                    }
                }
            }
        }

        $PGajiPokokObj = new PGajiPokok();
        $PGajiPokokObj->id = '-';
        $PGajiPokokObj->nominal = $gajiPokok;

        return $PGajiPokokObj;
    }

    /**
     * @param string $pegawaiId
     * @param int $statusKehadiran
     * @return bool|int|string|null
     */
    public function hitungStatusKehadiran(string $pegawaiId, int $statusKehadiran = 0)
    {
        $query = $this->queryAbsensi($pegawaiId);
        if ($statusKehadiran > 0) {
            $query->andWhere(['absensi.status_kehadiran' => $statusKehadiran]);
        }
        return $query->count();
    }

    /**
     * @param string $pegawaiId
     * @return int
     */
    public function hitungJumlahTelat(string $pegawaiId): int
    {
        $query = $this->queryAbsensi($pegawaiId);
        $absensis = $query->andWhere(['>', 'telat', 0])->all();
        $jumlahTelat = 0;
        /** @var Absensi $absensi */
        foreach ($absensis as $absensi) {
            if ($template = $absensi->splTemplate) {
                if ($template->kategori == SplTemplateAlias::LEMBUR_KHUSUS) {
                    continue;
                }
            }
            $jumlahTelat += 1;
        }
        return $jumlahTelat;
    }

    /**
     * @param string $pegawaiId
     * @return bool|int|mixed|string|null
     */
    public function hitungJumlahJamLembur(string $pegawaiId)
    {
        $query = $this->queryAbsensi($pegawaiId);
        $dataAbsensis = $query->all();
        $jamLembur = 0;
        if (!empty($dataAbsensis)) {
            /** @var Absensi $dataAbsensi */
            foreach ($dataAbsensis as $dataAbsensi) {
                $jamLembur += $dataAbsensi->jamLembur();
            }
        }

        return $jamLembur;
    }

    /**
     * @param string $payrollPeriodeId
     * @param string $pegawaiId
     * @param int $kategori
     * @param bool $adalahGajiTerakhir
     * @return array
     */
    public function getPinjamanAngsuran(string $payrollPeriodeId, string $pegawaiId, int $kategori, bool $adalahGajiTerakhir = false)
    {
        $modelAngsuran = new PinjamanAngsuran();
        return $modelAngsuran->getAngsuran($payrollPeriodeId, $pegawaiId, $kategori, $adalahGajiTerakhir);

    }

    /**
     * @param string $payrollPeriodeId
     * @param string $pegawaiId
     * @return array|ActiveRecord|null
     */
    public function getBpjsByPayrollPeriodeKes(string $payrollPeriodeId, string $pegawaiId)
    {
        return PayrollBpjsDetail::find()
            ->joinWith(['bpjs' => function (ActiveQuery $query) use ($payrollPeriodeId) {
                $query->andWhere(['payroll_bpjs.payroll_periode_id' => $payrollPeriodeId]);
            }])
            ->andWhere(['payroll_bpjs_detail.pegawai_id' => $pegawaiId])
            ->one();
    }

    /**
     * @param string $payrollPeriodeId
     * @param string $pegawaiId
     * @return array|ActiveRecord|null
     */
    public function getBpjsByPayrollPeriodeTk(string $payrollPeriodeId, string $pegawaiId)
    {
        return PayrollBpjsTkDetail::find()
            ->joinWith(['payrollBpjs' => function (ActiveQuery $query) use ($payrollPeriodeId) {
                $query->andWhere(['payroll_bpjs.payroll_periode_id' => $payrollPeriodeId]);
            }])
            ->andWhere(['payroll_bpjs_tk_detail.pegawai_id' => $pegawaiId])
            ->one();
    }

    /**
     * @param string $pegawaiId
     * @param int $shift
     * @return bool|int|string|null
     */
    public function hitungJumlahShift(string $pegawaiId, int $shift)
    {
        $tanggal_awal = $this->payrollPeriode->tanggal_awal;
        $tanggal_akhir = $this->payrollPeriode->tanggal_akhir;
        $query = Absensi::find()
            ->andWhere(['pegawai_id' => $pegawaiId])
            ->andWhere(['BETWEEN', 'tanggal', $tanggal_awal, $tanggal_akhir])
            ->joinWith(['timeTable' => function (ActiveQuery $query) use ($shift) {
                $query->andWhere(['shift' => $shift]);
            }]);
        return $query->count();
    }

    /**
     * @param string $pegawaiId
     * @return false|float
     */
    private function hitungUpahLembur(string $pegawaiId)
    {
        $query = $this->queryAbsensi($pegawaiId);
        $dataAbsensis = $query->all();
        $upahLembur = 0;
        if (!empty($dataAbsensis)) {
            /** @var Absensi $dataAbsensi */
            foreach ($dataAbsensis as $dataAbsensi) {
                $upahLembur += round(MasterDataDefault::_hitungUpahLembur($pegawaiId, (float)$dataAbsensi->jamLembur(), $dataAbsensi->tanggal), 2);
            }
        }
        return $upahLembur;
    }

    /**
     * @param string $pegawaiId
     * @return float|int
     */
    private function hitungUml(string $pegawaiId)
    {
        $uml = 0;
        try {
            $tanggal_awal = new DateTime($this->payrollPeriode->tanggal_awal);
            $tanggal_akhir = new DateTime($this->payrollPeriode->tanggal_akhir);
            while ($tanggal_awal <= $tanggal_akhir) {
                $uml += PUangMakan::_hitungUangMakanLembur($pegawaiId, $tanggal_awal->format('Y-m-d'));
                $tanggal_awal->add(new DateInterval('P1D'));
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $uml;
    }

    /**
     * @param string $pegawaiId
     * @param string $jenis id dari table izin_jenis
     * @param bool $asArray
     * @return array|false|int
     */
    public function hitungIjinTanpahadir(string $pegawaiId, string $jenis, bool $asArray = false)
    {
        $query = IzinTanpaHadir::find();
        $jenis = IzinJenis::getConfig($jenis);
        $query->andWhere(['pegawai_id' => $pegawaiId, 'izin_jenis_id' => $jenis->id]);
        $tanggalAwal = $this->payrollPeriode->tanggal_awal;
        $tanggalAkhir = $this->payrollPeriode->tanggal_akhir;
        $query->andWhere('\'' . $tanggalAwal . '\' BETWEEN izin_tanpa_hadir.tanggal_mulai AND izin_tanpa_hadir.tanggal_selesai OR \'' . $tanggalAkhir . '\' BETWEEN izin_tanpa_hadir.tanggal_mulai AND izin_tanpa_hadir.tanggal_selesai OR izin_tanpa_hadir.tanggal_mulai BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR izin_tanpa_hadir.tanggal_selesai BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR ( \'' . $tanggalAwal . '\' <= izin_tanpa_hadir.tanggal_mulai AND \'' . $tanggalAkhir . '\' >= izin_tanpa_hadir.tanggal_selesai) OR ( \'' . $tanggalAwal . '\' >= izin_tanpa_hadir.tanggal_mulai AND \'' . $tanggalAkhir . '\' <= izin_tanpa_hadir.tanggal_selesai)');
        $dataIjin = $query->all();
        $jumlahIjin = 0;
        $tanggalIzin = [];
        if (!empty($dataIjin)) {
            /** @var IzinTanpaHadir $ijinTanpaHadir */
            foreach ($dataIjin as $ijinTanpaHadir) {
                try {
                    $tglMulai = $ijinTanpaHadir->tanggal_mulai;
                    if ($ijinTanpaHadir->tanggal_mulai < $tanggalAwal) {
                        $tglMulai = $tanggalAwal;
                    }
                    $tglAkhir = $ijinTanpaHadir->tanggal_selesai;
                    if ($ijinTanpaHadir->tanggal_selesai > $tanggalAkhir) {
                        $tglAkhir = $tanggalAkhir;
                    }
                    $mulai = new DateTime($tglMulai);
                    $akhir = new DateTime($tglAkhir);
                    $akhir = $akhir->modify('+1 day');
                    $jumlahIjin += $mulai->diff($akhir)->days;
                    $period = new DatePeriod($mulai, new DateInterval('P1D'), $akhir);
                    foreach ($period as $date) {
                        $tanggalIzin[] = $date->format('Y-m-d');
                    }
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        }
        if ($asArray) {
            return ['jumlahIzin' => $jumlahIjin, 'tanggalIzin' => $tanggalIzin];
        }
        return $jumlahIjin;
    }

    /**
     * @param string $pegawaiId
     * @return bool|int|string|null
     */
    public function hitungAtl(string $pegawaiId)
    {
        $query = AbsensiTidakLengkap::find();
        $query->andWhere(['pegawai_id' => $pegawaiId]);
        $query->andWhere(['BETWEEN', 'tanggal', $this->payrollPeriode->tanggal_awal, $this->payrollPeriode->tanggal_akhir]);
        return $query->count();
    }

    /**
     * @param string $pegawaiId
     * @return bool|int|string|null
     */
    public function hitungSh(string $pegawaiId)
    {
        $query = SetengahHari::find();
        $query->andWhere(['pegawai_id' => $pegawaiId]);
        $query->andWhere(['BETWEEN', 'tanggal', $this->payrollPeriode->tanggal_awal, $this->payrollPeriode->tanggal_akhir]);
        return $query->count();
    }

    /**
     * Menghitung jumlah libur hadir berdasarkan ciri2 data absensi
     * note : Perlu proses ulang data absensi lama, karena ada sidikit perubahan pada proses absensi
     *
     * @param string $pegawaiId
     * @return ActiveQuery
     */
    public function liburHadirQuery(string $pegawaiId): ActiveQuery
    {
        $tanggal_awal = $this->payrollPeriode->tanggal_awal;
        $tanggal_akhir = $this->payrollPeriode->tanggal_akhir;
        return Absensi::find()
            ->andWhere(['pegawai_id' => $pegawaiId, 'time_table_id' => 0, 'status_kehadiran' => Absensi::MASUK])
            ->andWhere(['!=', 'spl_id', ''])
            ->andWhere(['>', 'spl_template_id', 0])
            ->andWhere(['>', 'jumlah_jam_lembur', 0])
            ->andWhere(['BETWEEN', 'tanggal', $tanggal_awal, $tanggal_akhir]);
    }

    /**
     * @param string $pegawaiId
     * @param int $shift
     * @return ActiveQuery
     */
    public function queryAtl(string $pegawaiId, int $shift = 0): ActiveQuery
    {
        $tanggal_awal = $this->payrollPeriode->tanggal_awal;
        $tanggal_akhir = $this->payrollPeriode->tanggal_akhir;
        $query = AbsensiTidakLengkap::find();
        if ($shift > 0) {
            $query->andWhere(['shift' => $shift]);
        }
        $query->andWhere(['pegawai_id' => $pegawaiId])
            ->andWhere(['BETWEEN', 'tanggal', $tanggal_awal, $tanggal_akhir]);
        return $query;
    }

    /**
     * @param string $pegawaiId
     * @param int $shift
     * @return ActiveQuery
     */
    public function querySetengahHari(string $pegawaiId, int $shift = 0): ActiveQuery
    {
        $tanggal_awal = $this->payrollPeriode->tanggal_awal;
        $tanggal_akhir = $this->payrollPeriode->tanggal_akhir;
        $query = SetengahHari::find();
        if ($shift > 0) {
            $query->andWhere(['shift' => $shift]);
        }
        $query->andWhere(['pegawai_id' => $pegawaiId])
            ->andWhere(['BETWEEN', 'tanggal', $tanggal_awal, $tanggal_akhir]);
        return $query;
    }

    /**
     * @param string $pegawaiId
     * @param int $shift
     * @param bool $asArray
     * @return array|int jumlah libur hadir berdasarkan shift
     */
    public function hitungLiburHadiByShift(string $pegawaiId, int $shift = TimeTable::SHIFT_SATU, bool $asArray = true)
    {
        $query = $this->liburHadirQuery($pegawaiId)->all();
        $datas = [];
        if ($query) {
            /** @var Absensi $liburHadir */
            foreach ($query as $liburHadir) {
                if ($liburHadir->splTemplate) {
                    $datas[$liburHadir->splTemplate->tunjangan_shift][] = $liburHadir->spl_template_id;
                }
            }
        }
        if ($asArray) {
            return $datas;
        }
        if (isset($datas[$shift])) {
            return count($datas[$shift]);
        }
        return 0;
    }

    /**
     * @param MasterDataDefault $masterData
     * @param string $pegawaiId
     * @return PGajiPokok
     * @throws Exception
     */
    public function hitungJumlahGajiSpg(MasterDataDefault $masterData, string $pegawaiId): PGajiPokok
    {
        $query = $this->queryAbsensi($pegawaiId);
        $dataAbsensis = $query->all();

        $gajiPokok = 0;
        if (!empty($dataAbsensis)) {
            /** @var Absensi $dataAbsensi */
            foreach ($dataAbsensis as $dataAbsensi) {
                $hariKerjaAktif = $dataAbsensi->pegawaiSistemKerja->sistemKerja->hari_kerja_aktif;
                if ($dataAbsensi->status_kehadiran == AbsensiAlias::MASUK) {
                    $hitungSetengah = range(4, 7);
                    $hitungPenuh = 8;
                    $masuk = new DateTime($dataAbsensi->masuk);
                    $hari = $masuk->format('D');
                    if ($hari == 'Sat') {
                        $hitungSetengah = range(3, 4);
                        $hitungPenuh = 5;
                    }
                    $pulang = new DateTime($dataAbsensi->pulang);
                    $interval = $masuk->diff($pulang);
                    $jamInterval = $interval->format('%h');
                    if (in_array($jamInterval, $hitungSetengah)) {
                        if ($hariKerjaAktif > 0) {
                            $gajiPokok += ($masterData->getGajiPokokAktif($dataAbsensi->tanggal) / $hariKerjaAktif) * 0.5;
                        }
                    }
                    if ($jamInterval >= $hitungPenuh) {
                        if ($hariKerjaAktif > 0) {
                            $gajiPokok += $masterData->getGajiPokokAktif($dataAbsensi->tanggal) / $hariKerjaAktif;
                        }
                    }
                }
            }
        }
        $PGajiPokokObj = new PGajiPokok();
        $PGajiPokokObj->id = '-';
        $PGajiPokokObj->nominal = $gajiPokok;

        return $PGajiPokokObj;

    }

    /**
     *
     * Menghitung jumlah jam kerja dalam jam
     * Untuk keperluan menentukan status kehadiran dari SPG
     *
     * @param string $pegawaiId
     * @param bool $untukUangTransport
     * @return float|int
     * @throws Exception
     */
    public function hitungJumlahHariKerjaSpg(string $pegawaiId, bool $untukUangTransport = FALSE)
    {
        $query = $this->queryAbsensi($pegawaiId);
        $dataAbsensis = $query->all();

        $jamKerjaSatuBulan = 0;
        if (!empty($dataAbsensis)) {
            /** @var Absensi $dataAbsensi */
            foreach ($dataAbsensis as $dataAbsensi) {
                if ($dataAbsensi->status_kehadiran == Absensi::MASUK && $this->golongan_id == Golongan::SATU_B) {
                    $hitungSetengah = range(4, 7);
                    $hitungPenuh = 8;
                    $masuk = new DateTime($dataAbsensi->masuk);
                    $hari = $masuk->format('D');
                    if ($hari == 'Sat') {
                        $hitungSetengah = range(3, 4);
                        $hitungPenuh = 5;
                    }
                    $pulang = new DateTime($dataAbsensi->pulang);
                    $interval = $masuk->diff($pulang);
                    $jamInterval = $interval->format('%h');
                    if (!$untukUangTransport && in_array($jamInterval, $hitungSetengah)) {
                        $jamKerjaSatuBulan += 0.5;
                    }
                    if (!$untukUangTransport && $jamInterval >= $hitungPenuh) {
                        $jamKerjaSatuBulan += 1;
                    }
                    if ($untukUangTransport) {
                        if ($jamInterval >= 4) {
                            $jamKerjaSatuBulan += 1;
                        }
                    }
                }
            }
        }
        return $jamKerjaSatuBulan;
    }


    /**
     * @return array
     * @throws Exception
     */
    public function formatingProsesGaji(): array
    {
        $modelPegawai = new Pegawai();
        $queryPegawai = $modelPegawai->getPegawaiByGolonganDanBisnisUnitByRange($this->golongan_id, $this->bisnis_unit, $this->payrollPeriode->tanggal_awal, $this->payrollPeriode->tanggal_akhir);
        $modelPayrollPeriode = $this->payrollPeriode;
        $tanggalAwal = $this->payrollPeriode->tanggal_awal;
        $tanggalAkhir = $this->payrollPeriode->tanggal_akhir;
        $dataGajis = [];
        $dataGajis['payrollPeriode'] = [
            'id' => $modelPayrollPeriode->id,
            'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
            'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
            'namaPeriode' => $modelPayrollPeriode->namaPeriode,
            'templateId' => $this->id,
        ];
//        $a = 0;

        /** @var Pegawai $pegawai */
        foreach ($queryPegawai as $pegawai) {
//            if ($pegawai->nama_lengkap != 'CHALIFAH ISLAMI') {
//                continue;
//            }
//            $a++;
//            if ($a == 100) {
//                break;
//            }

            //echo $pegawai->nama_lengkap." 1 \n";
            $pegawaiGolongan = $pegawai->getPegawaiGolonganUntukTanggal($tanggalAkhir);
            if (empty($pegawaiGolongan)) {
                //echo "1a \n";
                $pegawaiGolongan = $pegawai->getPegawaiGolonganUntukTanggal($tanggalAwal);
                if (empty($pegawaiGolongan)) {
                    //echo "1b \n";
                    $pegawaiGolongan = $pegawai->getPegawaiGolonganResign($tanggalAwal, $tanggalAkhir);
                }
            }

            //echo "2 \n";
            $perjanjianKerja = $pegawai->getPerjanjianKerjaUntukTanggal($tanggalAkhir);
            if (empty($perjanjianKerja)) {
                //echo "2a \n";
                $perjanjianKerja = $pegawai->getPerjanjianKerjaUntukTanggal($tanggalAwal);
                if (empty($perjanjianKerja)) {
                    //echo "2b \n";
                    $perjanjianKerja = $pegawai->getPerjanjianKerjaResign($tanggalAwal, $tanggalAkhir);
                }
            }

            //echo "3 \n";
            $pegawaiStruktur = $pegawai->getPegawaiStrukturUntukTanggal($tanggalAkhir);
            if (empty($pegawaiStruktur)) {
                //echo "3a \n";
                $pegawaiStruktur = $pegawai->getPegawaiStrukturUntukTanggal($tanggalAwal);
                if (empty($pegawaiStruktur)) {
                    //echo "3b \n";
                    $pegawaiStruktur = $pegawai->getPegawaiStrukturResign($tanggalAwal, $tanggalAkhir);
                }
            }

            //echo "4 \n";
            $pegawaiSistemKerja = $pegawai->getPegawaiSistemKerjaUntukTanggal($tanggalAkhir);
            if (empty($pegawaiSistemKerja)) {
                $pegawaiSistemKerja = $pegawai->getPegawaiSistemKerjaUntukTanggal($tanggalAwal);
            }

            $pegawaiCostCenter = $pegawai->getCostCenterUntukTanggal($tanggalAkhir);
            if (empty($pegawaiCostCenter)) {
                $pegawaiCostCenter = $pegawai->getCostCenterUntukTanggal($tanggalAwal);
            }

            $masterData = $pegawai->masterDataDefaults;
//            var_dump(!empty($perjanjianKerja));
//            var_dump(!empty($pegawaiGolongan));
//            var_dump(!is_null($masterData));

            $jamLembur = $this->hitungJumlahJamLembur($pegawai->id);
            $upahLembur = $this->hitungUpahLembur($pegawai->id);

            if (!empty($perjanjianKerja) && !empty($pegawaiGolongan) && !is_null($masterData)) {

                //echo "5 \n";

                if ($masterData->tipe_gaji_pokok != $this->payrollPeriode->tipe) {
                    continue;
                }

                //echo "6 \n";

                $dasarMasaKerja = PerjanjianKerja::getPerjanjianDasarStatic($pegawai->id);
                if (empty($dasarMasaKerja)) {
                    continue;
                }

                //echo "7 \n";

                $adalahGajiTerakhir = $this->adalahGajiTerakhir($perjanjianKerja);

                $gajiPokokAktif = $masterData->getGajiPokokAktif($tanggalAkhir, true);
                if ($masterData->tipe_gaji_pokok == MasterDataDefault::HARIAN) {
                    $gajiPokokAktif = $this->hitungGajiPokokHarianPerPeriode($masterData);
                }
                if ($this->golongan_id == Golongan::SATU_B) {
                    $gajiPokokAktif = $this->hitungJumlahGajiSpg($masterData, $pegawai->id);
                }
                $getAdminBankAktif = $masterData->getAdminBankAktif($tanggalAkhir, true);
                $getTunjPulsaAktif = $masterData->getTunjPulsaAktif($tanggalAkhir, true);
                $getPremiHadirAktif = $masterData->getPremiHadirAktif($tanggalAkhir, true);
                $getSewaMobilAktif = $masterData->getSewaMobilAktif($tanggalAkhir, true);
                $getSewaMotorAktif = $masterData->getSewaMotorAktif($tanggalAkhir, true);
                $getSewaRumahAktif = $masterData->getSewaRumahAktif($tanggalAkhir, true);
                $getTunjJabatanAktif = $masterData->getTunjJabatanAktif($tanggalAkhir, true);
                $getTunjKostAktif = $masterData->getTunjKostAktif($tanggalAkhir, true);
                $getUangTransportAktif = $masterData->getUangTransportAktif($tanggalAkhir, true);
                $getTunjanganLainTetap = $masterData->getTunjanganLainTetap($tanggalAkhir, true);
                $getTunjanganLainLain = $masterData->getTunjanganLainLainAktif($tanggalAkhir, true);
                $getInsentifTetap = $masterData->getInsentifTetap($tanggalAkhir, true);
                $getInsentifTidakTetapUmum = $masterData->getInsentifTidakTetap($this->payrollPeriode);
                $getInsentifTidakTetapPph = $masterData->getInsentifTidakTetap($this->payrollPeriode, InsentifTidakTetap::INSENTIF_PPH);
                $getInsentifTidakTetapGt = $masterData->getInsentifTidakTetap($this->payrollPeriode, InsentifTidakTetap::INSENTIF_GT);
                $getRapel = $masterData->getRapel($this->payrollPeriode);
                $getUangMakanAktif = $masterData->getUangMakanAktif($tanggalAkhir, true);
                $pegawaiBank = $pegawai->pegawaiBank;
                $pinjaman = $this->getPinjamanAngsuran($this->payroll_periode_id, $pegawai->id, Pinjaman::PINJAMAN, $adalahGajiTerakhir);
                $pinjamanBank = $this->getPinjamanAngsuran($this->payroll_periode_id, $pegawai->id, Pinjaman::BANK, $adalahGajiTerakhir);
                $pinjamanKasbon = $this->getPinjamanAngsuran($this->payroll_periode_id, $pegawai->id, Pinjaman::KASBON, $adalahGajiTerakhir);
                $pinjamanKoperasi = $this->getPinjamanAngsuran($this->payroll_periode_id, $pegawai->id, Pinjaman::KOPERASI, $adalahGajiTerakhir);
                $pinjamanPotongan = $this->getPinjamanAngsuran($this->payroll_periode_id, $pegawai->id, Pinjaman::POTONGAN, $adalahGajiTerakhir);
                $pinjamanLainLain = $this->getPinjamanAngsuran($this->payroll_periode_id, $pegawai->id, Pinjaman::LAIN_LAIN, $adalahGajiTerakhir);
                $pinjamanPotonganAtl = $this->getPinjamanAngsuran($this->payroll_periode_id, $pegawai->id, Pinjaman::POTONGAN_ATL, $adalahGajiTerakhir);
                $tAnak = 0;
                $tAnakId = '';

                $masaKerja = $dasarMasaKerja->hitungMasaKerja('', $tanggalAkhir, true);
                if ($masaKerja['dalamBulan'] >= 6 && $pegawai->status_pernikahan != Pegawai::BELUM_MENIKAH) {
                    $ta = MasterDataDefault::getTunjanganAnak($pegawai->getJumlahAnak(), true);
                    if ($ta && $masterData->tunjangan_anak) {
                        $tAnak = $ta->nominal;
                        $tAnakId = $ta->id;
                    }
                }

                $tLoyalitas = 0;
                $tLoyalitasId = '';
                $tl = MasterDataDefault::getTunjanganLoyalitas($masaKerja['tahun'], true);
                if ($tl && $masterData->tunjangan_loyalitas) {
                    $tLoyalitas = $tl->nominal;
                    $tLoyalitasId = $tl->id;
                }

                $getPayrollBpjsKes = $this->getBpjsByPayrollPeriodeKes($this->payroll_periode_id, $pegawai->id);
                $getPayrollBpjsTk = $this->getBpjsByPayrollPeriodeTk($this->payroll_periode_id, $pegawai->id);

                $liburHadirByShift = $this->hitungLiburHadiByShift($pegawai->id);

                $pengaliUml = $this->hitungPengaliUml($pegawai->id);

                $masuk = $this->hitungStatusKehadiran($pegawai->id, AbsensiAlias::MASUK);
                if ($this->golongan_id == Golongan::SATU_B) {
                    $masuk = $this->hitungJumlahHariKerjaSpg($pegawai->id);
                }

                $uangTransport = $getUangTransportAktif ? $getUangTransportAktif->nominal : 0;
                if ($this->golongan_id == Golongan::SATU_B) {
                    $uangTransport = $this->hitungJumlahHariKerjaSpg($pegawai->id, TRUE) * $masterData->getUangTransportAktif();
                }

                $dataGajis['pegawais'][$pegawai->id] = [
                    'id_pegawai' => $perjanjianKerja->id_pegawai ?? 0,
                    'nama_lengkap' => $pegawai->nama_lengkap,
                    'bisnis_unit_s' => ProsesGaji::modelPerjanjianKerja()->_kontrak[$this->bisnis_unit],
                    'golongan_s' => $pegawaiGolongan->golongan ? $pegawaiGolongan->golongan->nama : '',
                    'jenis_kontrak_s' => $perjanjianKerja->jenis ? $perjanjianKerja->jenis->nama : '',
                    'struktur_s' => $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : '',
                    'masuk' => $masuk,
                    'alfa' => $this->hitungStatusKehadiran($pegawai->id, AbsensiAlias::ALFA),
                    'ijin' => $this->hitungStatusKehadiran($pegawai->id, AbsensiAlias::IJIN),
                    'libur' => $this->hitungStatusKehadiran($pegawai->id, AbsensiAlias::LIBUR),
                    'cuti' => $this->hitungStatusKehadiran($pegawai->id, AbsensiAlias::CUTI),
                    'undifined' => $this->hitungStatusKehadiran($pegawai->id, AbsensiAlias::ERROR),
                    'wfh' => $this->hitungIjinTanpahadir($pegawai->id, 'WFH'),
                    'sd' => $this->hitungIjinTanpahadir($pegawai->id, 'SD'),
                    'ith' => $this->hitungIjinTanpahadir($pegawai->id, 'ITH'),
                    'idl' => $this->hitungIjinTanpahadir($pegawai->id, 'IDL'),
                    'off' => $this->hitungIjinTanpahadir($pegawai->id, 'OFF'),
                    'cm' => $this->hitungIjinTanpahadir($pegawai->id, 'CM'),
                    'is' => $this->hitungIjinTanpahadir($pegawai->id, 'IS'),
                    'dispensasi' => $this->hitungIjinTanpahadir($pegawai->id, 'D'),
                    'tl' => $this->hitungIjinTanpahadir($pegawai->id, 'TL'),
                    'atl' => $this->hitungAtl($pegawai->id),
                    'sh' => $this->hitungSh($pegawai->id),
                    'jam_lembur' => $jamLembur,
                    's2' => $this->hitungJumlahShift($pegawai->id, TimeTableAlias::SHIFT_DUA),
                    's2_nominal' => MasterDataDefault::getUangShift(),
                    's3' => $this->hitungJumlahShift($pegawai->id, TimeTableAlias::SHIFT_TIGA),
                    's3_nominal' => MasterDataDefault::getUangShift(TimeTableAlias::SHIFT_TIGA),
                    'uang_lembur' => $upahLembur,
                    'insentif_lembur' => $this->hitungJumlahInsentifLembur($pegawai->id),
                    'uml' => $this->hitungUml($pegawai->id),
                    'pengali_uml1' => $pengaliUml[1],
                    'pengali_uml2' => $pengaliUml[2],
                    'p_gaji_pokok_id' => $gajiPokokAktif ? $gajiPokokAktif->id : '-',
                    'p_gaji_pokok' => $gajiPokokAktif ? round($gajiPokokAktif->nominal) : 0,
                    'p_admin_bank_id' => $getAdminBankAktif ? $getAdminBankAktif->id : '-',
                    'p_admin_bank' => $getAdminBankAktif ? $getAdminBankAktif->nominal : 0,
                    'pinjaman_angsuran_id' => $pinjaman ? $pinjaman['id'] : '-',
                    'pinjaman_nominal' => $pinjaman ? $pinjaman['jumlah'] : 0,
                    'angsuran_ke' => $pinjaman ? $pinjaman['angsuran_ke'] : 0,
                    'pinjaman_bank_id' => $pinjamanBank ? $pinjamanBank['id'] : '-',
                    'pinjaman_bank_nominal' => $pinjamanBank ? $pinjamanBank['jumlah'] : 0,
                    'angsuran_bank_ke' => $pinjamanBank ? $pinjamanBank['angsuran_ke'] : 0,
                    'pinjaman_kasbon_id' => $pinjamanKasbon ? $pinjamanKasbon['id'] : '-',
                    'pinjaman_kasbon_nominal' => $pinjamanKasbon ? $pinjamanKasbon['jumlah'] : 0,
                    'angsuran_kasbon_ke' => $pinjamanKasbon ? $pinjamanKasbon['angsuran_ke'] : 0,
                    'pinjaman_koperasi_id' => $pinjamanKoperasi ? $pinjamanKoperasi['id'] : '-',
                    'pinjaman_koperasi_nominal' => $pinjamanKoperasi ? $pinjamanKoperasi['jumlah'] : 0,
                    'angsuran_koperasi_ke' => $pinjamanKoperasi ? $pinjamanKoperasi['angsuran_ke'] : 0,
                    'pinjaman_potongan_id' => $pinjamanPotongan ? $pinjamanPotongan['id'] : '-',
                    'pinjaman_potongan_nominal' => $pinjamanPotongan ? $pinjamanPotongan['jumlah'] : 0,
                    'angsuran_potongan_ke' => $pinjamanPotongan ? $pinjamanPotongan['angsuran_ke'] : 0,
                    'pinjaman_lain_lain_id' => $pinjamanLainLain ? $pinjamanLainLain['id'] : '-',
                    'pinjaman_lain_lain_nominal' => $pinjamanLainLain ? $pinjamanLainLain['jumlah'] : 0,
                    'angsuran_lain_lain_ke' => $pinjamanLainLain ? $pinjamanLainLain['angsuran_ke'] : 0,
                    'pinjaman_potongan_atl_id' => $pinjamanPotonganAtl ? $pinjamanPotonganAtl['id'] : '-',
                    'pinjaman_potongan_atl_nominal' => $pinjamanPotonganAtl ? $pinjamanPotonganAtl['jumlah'] : 0,
                    'angsuran_potongan_atl_ke' => $pinjamanPotonganAtl ? $pinjamanPotonganAtl['angsuran_ke'] : 0,
                    'p_insentif_tetap_id' => $getInsentifTetap ? $getInsentifTetap->id : '-',
                    'p_premi_hadir_id' => $getPremiHadirAktif ? $getPremiHadirAktif->id : '-',
                    'p_sewa_mobil_id' => $getSewaMobilAktif ? $getSewaMobilAktif->id : '-',
                    'p_sewa_motor_id' => $getSewaMotorAktif ? $getSewaMotorAktif->id : '-',
                    'p_sewa_rumah_id' => $getSewaRumahAktif ? $getSewaRumahAktif->id : '-',
                    'p_tunjagan_lain_tetap_id' => $getTunjanganLainTetap ? $getTunjanganLainTetap->id : '-',
                    'p_tunjagan_lain_lain_id' => $getTunjanganLainLain ? $getTunjanganLainLain->id : '-',
                    'p_tunjangan_anak_config_id' => $tAnakId,
                    'p_tunjangan_jabatan_id' => $getTunjJabatanAktif ? $getTunjJabatanAktif->id : '',
                    'p_tunjangan_kost_id' => $getTunjKostAktif ? $getTunjKostAktif->id : '-',
                    'p_tunjangan_loyalitas_config_id' => $tLoyalitasId,
                    'p_tunjangan_pulsa_id' => $getTunjPulsaAktif ? $getTunjPulsaAktif->id : '-',
                    'p_uang_makan_id' => $getUangMakanAktif ? $getUangMakanAktif->id : '-',
                    'p_uang_transport_id' => $getUangTransportAktif ? $getUangTransportAktif->id : '-',
                    'p_insentif_tetap' => $getInsentifTetap ? $getInsentifTetap->nominal : 0,
                    'p_premi_hadir' => $getPremiHadirAktif ? $getPremiHadirAktif->nominal : 0,
                    'p_sewa_mobil' => $getSewaMobilAktif ? $getSewaMobilAktif->nominal : 0,
                    'p_sewa_motor' => $getSewaMotorAktif ? $getSewaMotorAktif->nominal : 0,
                    'p_sewa_rumah' => $getSewaRumahAktif ? $getSewaRumahAktif->nominal : 0,
                    'p_tunjagan_lain_tetap' => $getTunjanganLainTetap ? $getTunjanganLainTetap->nominal : 0,
                    'p_tunjagan_lain_lain' => $getTunjanganLainLain ? $getTunjanganLainLain->nominal : 0,
                    'p_tunjangan_anak' => $tAnak,
                    'p_tunjangan_jabatan' => $getTunjJabatanAktif ? $getTunjJabatanAktif->nominal : 0,
                    'p_tunjangan_kost' => $getTunjKostAktif ? $getTunjKostAktif->nominal : 0,
                    'p_tunjangan_loyalitas' => $tLoyalitas,
                    'p_tunjangan_pulsa' => $getTunjPulsaAktif ? $getTunjPulsaAktif->nominal : 0,
                    'p_uang_makan' => $getUangMakanAktif ? $getUangMakanAktif->nominal : 0,
                    'p_uang_transport' => $uangTransport,
                    'hari_kerja_aktif' => $pegawaiSistemKerja ? $pegawaiSistemKerja->sistemKerja->hari_kerja_aktif : 0,
                    'pegawai_bpjs_id' => $getPayrollBpjsKes ? $getPayrollBpjsKes['id'] : '',
                    'pegawai_bpjs_kes' => $getPayrollBpjsKes ? round($getPayrollBpjsKes['iuran_nominal_tk']) : 0,
                    'pegawai_bpjs_tk' => $getPayrollBpjsTk ? round($getPayrollBpjsTk['jht_nominal_tk'] + $getPayrollBpjsTk['iuran_pensiun_tk_nominal']) : 0,
                    'pegawai_bank_id' => $pegawaiBank ? $pegawaiBank->id : '-',
                    'pegawai_bank_nama' => $pegawaiBank ? $pegawaiBank->nama_bank : '-',
                    'pegawai_bank_nomor' => $pegawaiBank ? $pegawaiBank->nomer_rekening : '-',
                    'proses_gaji_id' => $this->id,
                    'pegawai_id' => $pegawai->id,
                    'perjanjian_kerja_id' => $perjanjianKerja->id,
                    'pegawai_golongan_id' => $pegawaiGolongan->id,
                    'pegawai_struktur_id' => $pegawaiStruktur ? $pegawaiStruktur->id : '',
                    'jumlah_telat' => $this->hitungJumlahTelat($pegawai->id),
                    'pegawai_sistem_kerja_id' => $pegawaiSistemKerja ? $pegawaiSistemKerja->id : '',
                    'cost_center_id' => $pegawaiCostCenter ? $pegawaiCostCenter->id : '',
                    'tipe_gaji_pokok' => $masterData->tipe_gaji_pokok,
                    'pot_keterlambatan' => $masterData->pot_keterlambatan,
                    'cabang' => $masterData->cabang,
                    'prorate_komponen' => $masterData->prorate_komponen,
                    'beatroute' => $masterData->beatroute,
                    'insentif_tidak_tetap_id' => 0,
                    'insentif_tidak_tetap' => $getInsentifTidakTetapUmum,
                    'insentif_pph' => $getInsentifTidakTetapPph,
                    'insentif_gt' => $getInsentifTidakTetapGt,
                    'rapel_id' => 0,
                    'rapel' => $getRapel,
                    'libur_hadir' => $this->liburHadirQuery($pegawai->id)->count(),
                    'libur_hadir_s1' => isset($liburHadirByShift[TimeTableAlias::SHIFT_SATU]) ? count($liburHadirByShift[TimeTableAlias::SHIFT_SATU]) : 0,
                    'libur_hadir_s2' => isset($liburHadirByShift[TimeTableAlias::SHIFT_DUA]) ? count($liburHadirByShift[TimeTableAlias::SHIFT_DUA]) : 0,
                    'libur_hadir_s3' => isset($liburHadirByShift[TimeTableAlias::SHIFT_TIGA]) ? count($liburHadirByShift[TimeTableAlias::SHIFT_TIGA]) : 0,
                    'atl_s1' => $this->queryAtl($pegawai->id, 1)->count(),
                    'atl_s2' => $this->queryAtl($pegawai->id, 2)->count(),
                    'atl_s3' => $this->queryAtl($pegawai->id, 3)->count(),
                    'sh_s2' => $this->querySetengahHari($pegawai->id, 2)->count(),
                    'sh_s3' => $this->querySetengahHari($pegawai->id, 3)->count(),
                    'jumlah_anak' => $pegawai ? $pegawai->getJumlahAnak() : 0,
                    'status_pernikahan' => $pegawai->status_pernikahan,
                ];
            }
        }
        return $dataGajis;
    }

    /**
     *
     */
    public function simpanDataGajiBulanan()
    {
        $dataGajis = $this->formatingProsesGaji();
        if (!empty($dataGajis)) {
            if (!empty($dataGajis['pegawais'])) {
                foreach ($dataGajis['pegawais'] as $dataGaji) {
                    $model = new GajiBulanan();
                    $model->setAttributes($dataGaji);
                    $cek = GajiBulanan::findOne(['pegawai_id' => $model->pegawai_id, 'proses_gaji_id' => $model->proses_gaji_id]);
                    if ($cek) {
                        $cek->delete();
                    }
                    if ($model->save()) {
//                    $model->rekapGajiBulananDetail();
//                    $model->rekapDataAttendance();
                    } else {
                        Yii::info($model->nama_lengkap, 'exception');
                        Yii::info($model->errors, 'exception');
                    }
                }
            }
        }
    }

    /**
     * @return bool|int|string|null
     */
    public function hitungJumlahPegawai()
    {
        return GajiBulanan::find()->andWhere(['proses_gaji_id' => $this->id])->count();
    }

    /**
     * Aksi2 yg dapat dilakukan setelah finish
     */
    public function afterFinish()
    {
        foreach ($this->gajiBulanans as $gajiBulanan) {
            PinjamanAngsuran::setDibayarAllStatic($gajiBulanan);
        }
        $this->kirimFileRekap();
        $this->setClosedBpjs();
    }

    /**
     * Mengirim file rekap
     */
    public function kirimFileRekap()
    {
        $name = $this->payrollPeriode->namaPeriode . '*' . $this->golongan->nama . '*' . PayrollBpjs::modelPerjanjianKerja()->_kontrak[$this->bisnis_unit];
        $fileName = self::filePathGaji . $name . '.xlsx';
        if (file_exists($fileName)) {
            /** @var Mailer $mail */
            $mail = Yii::$app->mailer_gmail;
            $compose = $mail->compose()
                ->setFrom('hc.system@kobe.co.id');

            $title = Yii::t('frontend', 'Rekap Data Gaji Final {name}.', ['name' => $name]);
            $message = '<html><body style="line-height: 1.5">';
            $message .= "<div style='font-weight:bold'>Dear Bapak/Ibu<br><br></div>";
            $message .= "<div>Berikut ini terlampir $title <br><br></div>";
            $message .= "<div style='font-weight:bold; color:red;'>Data yang sudah final ini, dikirim secara otomatis oleh HRIS dan data sudah tidak bisa diubah.<br><br></div>";
            $message .= "<div>Demikian, disampaikan.<br></div>";
            $message .= "<div>Terimakasih<br><br></div>";
            $message .= "<div style='font-weight:bold'>Regards<br><br></div>";
            $message .= "<div style='font-weight:bold'>HRIS - HC KBU</div>";
            $message .= "<div style='font-size: 8pt; background-color: #9d9d9d; color: #ffffff'>Data ini dikirim secara otomatis, setelah user/pic dari HC Dept klik button selesai pada kolom finishing status, pada halaman proses gaji.</div>";
            $message .= '</body></html>';

            $compose->setTo(['rama.dana@kobe.co.id', 'waskito@kobe.co.id'])
                ->setSubject($title)->setHtmlBody($message);
            $compose->attach($fileName);

            $compose->send();
        }
    }

    /**
     * @param PerjanjianKerja $perjanjianKerja
     * @return bool
     */
    public function adalahGajiTerakhir(PerjanjianKerja $perjanjianKerja): bool
    {
        $gajiTerakhir = false;
        $tanggalResign = $perjanjianKerja->tanggal_resign;
        $tanggalAkhirKontrak = $perjanjianKerja->akhir_berlaku;
        $payrollPeriode = $this->payrollPeriode;
        if (!is_null($tanggalResign)) {
            if ($tanggalResign >= $payrollPeriode->tanggal_awal && $tanggalResign <= $payrollPeriode->tanggal_akhir) {
                $gajiTerakhir = true;
            }
        }
        if (!is_null($tanggalAkhirKontrak)) {
            if ($tanggalAkhirKontrak >= $payrollPeriode->tanggal_awal && $tanggalAkhirKontrak <= $payrollPeriode->tanggal_akhir) {
                $gajiTerakhir = true;
            }
        }
        return $gajiTerakhir;
    }

    /**
     * @param bool $rename
     * @param string $newName
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function createFileSlipGaji(bool $rename = false, string $newName = ''): void
    {
        $filePath = ProsesGaji::filePathGaji;
        $fileName = $this->id . '.pdf';

        if (file_exists($filePath . $fileName)) {
            if ($rename) {
                if (empty($newName)) {
                    $newName = $this->id . '-' . date('Ymdhis') . '.pdf';
                }
                rename($filePath . $fileName, $filePath . $newName);
                $this->createFileSlipGaji();
            }
        } else {
            ini_set("pcre.backtrack_limit", "5000000");
            $content = Yii::$app->view->renderAjax('@frontend/modules/payroll/views/proses-gaji/_slips', [
                'model' => $this
            ]);
            Yii::info(Yii::t('frontend', 'Membuat file gaji periode {p}', ['p' => $this->payrollPeriode->namaPeriode]), 'proses-gaji');
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'filename' => $filePath . $fileName,
                'content' => $content,
                'cssFile' => '@frontend/web/css/slip.css',
                'cssInline' => 'table{font-size:10px}',
                'options' => ['title' => Yii::$app->name],
                'marginTop' => '5',
                'marginBottom' => '5',
                'marginLeft' => '5',
                'marginRight' => '5',
                'methods' => [
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            $pdf->render();
        }
    }

    /**
     * @param int $jumlahJam jumlah jam lembur
     * @return float|int
     */
    public function konversiInsentifLembur(int $jumlahJam): int
    {
        $jamMin = 4;
        $jamMax = 8;
        $jamProrate = [5, 6, 7];
        $insentif = 0;
        $insentifMin = 0;
        $insentifMax = 0;

        if (in_array($this->golongan_id, [Golongan::SATU_H, Golongan::SATU_H, Golongan::DUA, Golongan::TIGA, Golongan::TIGA_GT])) {
            $insentifMin = 50000;
            $insentifMax = 100000;
        }

        if (in_array($this->golongan_id, [Golongan::EMPAT, Golongan::EMPAT_GT])) {
            $insentifMin = 75000;
            $insentifMax = 125000;
        }

        if ($this->golongan_id == Golongan::LIMA) {
            $insentifMin = 100000;
            $insentifMax = 150000;
        }

        if (in_array($this->golongan_id, [Golongan::ENAM, Golongan::TUJUH])) {
            $insentifMin = 125000;
            $insentifMax = 200000;
        }

        if ($jumlahJam == $jamMin) {
            $insentif = $insentifMin;
        }
        if ($jumlahJam == $jamMax || $jumlahJam == 9) {
            $insentif = $insentifMax;
        }
        if (in_array($jumlahJam, $jamProrate) && $insentifMax > 0) {
            $insentif = ($insentifMax / $jamMax) * $jumlahJam;
        }
        return $insentif;
    }

    /**
     * @param string $pegawaiId
     * @return float|int
     */
    public function hitungJumlahInsentifLembur(string $pegawaiId)
    {
        $query = $this->queryAbsensi($pegawaiId);
        $dataAbsensis = $query->all();

        $insentifLembur = 0;
        if (!empty($dataAbsensis)) {
            /** @var Absensi $dataAbsensi */
            foreach ($dataAbsensis as $dataAbsensi) {
                $spl = $dataAbsensi->splRel;
                if ($spl && $spl->kategori == SplAlias::LEMBUR_INSENTIF) {
                    $insentifLembur += $this->konversiInsentifLembur($dataAbsensi->jamLembur());
                }
            }
        }
        return $insentifLembur;
    }

    /**
     *
     * Set status payroll bpjs menjadi closed, agar data tidak dapat lagi di edit
     *
     */
    public function setClosedBpjs()
    {
        $modelPayrolBpjs = PayrollBpjs::findOne(['payroll_periode_id' => $this->payroll_periode_id, 'golongan_id' => $this->golongan_id, 'bisnis_unit' => $this->bisnis_unit]);
        if ($modelPayrolBpjs) {
            $modelPayrolBpjs->last_status = Status::CLOSED;
            $modelPayrolBpjs->save();
        }
    }

    /**
     *
     * Proses ulang pph 21
     * Data lamanya tetap dihapus terlebih dahulu
     *
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function prosesUlangPph21()
    {
        foreach ($this->gajiBulanans as $gajiBulanan) {
            if (in_array($this->bisnis_unit, [PerjanjianKerja::KBU, PerjanjianKerja::ADA])) {
                if ($check = GajiPph21::findOne(['gaji_bulanan_id' => $gajiBulanan->id])) {
                    $check->delete();
                }
                $gajiBulanan->hitungPph21();
            }
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * File Name: PinjamanForm.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 19/10/2020
 * Time: 15:32
 */

namespace frontend\modules\payroll\models\form;


use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\Pinjaman;
use frontend\modules\payroll\models\PinjamanAngsuran;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PinjamanForm
 * @property string $payroll_periode_id
 * @property integer $kategori
 *
 *
 * @property string $upload_dir
 * @property UploadedFile $doc_file
 *
 * @package frontend\modules\payroll\models\form
 *
 */
class PinjamanForm extends Model
{
    public $payroll_periode_id;
    public $kategori;


    public $upload_dir = 'uploads/pinjaman/';

    public $doc_file;

    /**
     * @return Pinjaman
     */
    public static function pinjamanModel(): Pinjaman
    {
        return new Pinjaman();
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['payroll_periode_id'], 'required'],
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }

    /**
     * @return array|array[]
     */
    public function scenarios(): array
    {
        $scenarios = parent::scenarios();
        $scenarios['download'] = ['payroll_periode_id'];
        $scenarios['upload-template'] = ['doc_file'];
        return $scenarios;
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'payroll_periode_id' => Yii::t('frontend', 'Payroll Periode ID'),
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

    /**
     * @return array
     */
    public function getDataPinjamanPerPeriode()
    {
        $query = PinjamanAngsuran::find()
            ->andWhere(['payroll_periode_id' => $this->payroll_periode_id])->all();
        $dataPinjaman = [];
        if ($query) {
            $modelPayrollPeriode = PayrollPeriode::findOne($this->payroll_periode_id);
            $dataPinjaman['payrollPeriode'] = [
                'id' => $modelPayrollPeriode->id,
                'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
                'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
                'namaPeriode' => $modelPayrollPeriode->namaPeriode,
            ];
            /** @var PinjamanAngsuran $pinjamanAngsuran */
            foreach ($query as $pinjamanAngsuran) {
                $dataPinjaman['pegawai'][$pinjamanAngsuran->pinjaman->pegawai_id]['datapegawai'] = [
                    'pegawaiid' => $pinjamanAngsuran->pinjaman->pegawai_id,
                    'nama' => $pinjamanAngsuran->pinjaman->pegawai->nama_lengkap,
                    'golongan' => $pinjamanAngsuran->pinjaman->pegawaiGolongan ? $pinjamanAngsuran->pinjaman->pegawaiGolongan->golongan->nama : '-',
                    'bisnisUnit' => $pinjamanAngsuran->pinjaman->perjanjianKerja ? $pinjamanAngsuran->pinjaman->perjanjianKerja->namaKontrak : '-',
                    'idfp' => $pinjamanAngsuran->pinjaman->perjanjianKerja ? $pinjamanAngsuran->pinjaman->perjanjianKerja->id_pegawai : '-'
                ];
                $dataPinjaman['pegawai'][$pinjamanAngsuran->pinjaman->pegawai_id]['pinjaman'][] = [
                    'kategori' => $pinjamanAngsuran->pinjaman->_kategori[$pinjamanAngsuran->pinjaman->kategori],
                    'jumlah' => $pinjamanAngsuran->jumlah,
                    'nama' => $pinjamanAngsuran->pinjaman->nama,
                    'status' => $pinjamanAngsuran->status_,
                    'keterangan' => $pinjamanAngsuran->pinjaman->keterangan
                ];
            }
        }
        return $dataPinjaman;
    }

    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadRekapPinjaman()
    {
        $getDataPinjaman = $this->getDataPinjamanPerPeriode();
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $right = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $bold = [
            'font' => [
                'bold' => true,
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setVisible(false);
        $activeSheet->getColumnDimension('C')->setWidth(10);
        $activeSheet->getColumnDimension('D')->setWidth(30);
        $activeSheet->getColumnDimension('E')->setWidth(8);
        $activeSheet->getColumnDimension('F')->setWidth(8);
        $activeSheet->getColumnDimension('G')->setWidth(15);
        $activeSheet->getColumnDimension('H')->setWidth(25);
        $activeSheet->getColumnDimension('I')->setWidth(15);
        $activeSheet->getColumnDimension('J')->setWidth(15);
        $activeSheet->getColumnDimension('K')->setWidth(40);

        $activeSheet->setCellValue('C1', Yii::t('frontend', 'Periode'))->getStyle('A1');
        $activeSheet->setCellValue('D1', ': ' . $getDataPinjaman['payrollPeriode'] ['namaPeriode'])->getStyle('D1');
        $activeSheet->setCellValue('A2', Yii::t('frontend', 'NO'))->getStyle('A2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('B2', Yii::t('frontend', 'id'))->getStyle('B2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('C2', Yii::t('frontend', 'ID Pegawai'))->getStyle('C2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('D2', Yii::t('frontend', 'Nama Pegawai'))->getStyle('D2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('E2', Yii::t('frontend', 'Golongan'))->getStyle('E2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('F2', Yii::t('frontend', 'Bisnis Unit'))->getStyle('F2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('G2', Yii::t('frontend', 'Kategori'))->getStyle('G2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('H2', Yii::t('frontend', 'Nama Pinjaman'))->getStyle('H2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('I2', Yii::t('frontend', 'Jumlah'))->getStyle('I2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('J2', Yii::t('frontend', 'Status'))->getStyle('J2')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('K2', Yii::t('frontend', 'Keterangan'))->getStyle('K2')->applyFromArray($headerStyle);

        $no = 0;
        $row = 2;
        foreach ($getDataPinjaman['pegawai'] as $pegawai) {
            $no++;
            $row++;
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, $pegawai['datapegawai']['pegawaiid'])->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, $pegawai['datapegawai']['idfp'])->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('D' . $row, $pegawai['datapegawai']['nama'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('E' . $row, $pegawai['datapegawai']['golongan'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('F' . $row, $pegawai['datapegawai']['bisnisUnit'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
            $jumlah = 0;
            foreach ($pegawai['pinjaman'] as $pinjaman) {
                $jumlah += $pinjaman['jumlah'];
                $activeSheet->setCellValue('G' . $row, $pinjaman['kategori'])->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('H' . $row, $pinjaman['nama'])->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('I' . $row, $pinjaman['jumlah'])->getStyle('I' . $row)->applyFromArray(array_merge($right, $borderInside))->getNumberFormat()
                    ->setFormatCode('#,##0');
                $activeSheet->setCellValue('J' . $row, $pinjaman['status'])->getStyle('J' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('K' . $row, $pinjaman['keterangan'])->getStyle('K' . $row)->applyFromArray(array_merge($borderInside));
                $row++;
            }
            $activeSheet->mergeCells('A' . $row . ':F' . $row)->setCellValue('A' . $row, Yii::t('frontend', 'Jumlah'))->getStyle('A' . $row . ':F' . $row)->applyFromArray(array_merge($bold, $center));
            $activeSheet->setCellValue('I' . $row, $jumlah)->getStyle('I' . $row)->applyFromArray(array_merge($right, $borderInside, $bold))->getNumberFormat()
                ->setFormatCode('#,##0');
        }

        $highestColumn = $activeSheet->getHighestColumn('2');

        $activeSheet->getStyle('A2:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A2:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A2:' . $highestColumn . '2')->applyFromArray($headerStyle);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=DownloadPinjaman' . $getDataPinjaman['payrollPeriode']['namaPeriode'] . '.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }


    /**
     * @param int $kategori
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadTemplate(int $kategori): bool
    {
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(20);
        $activeSheet->getColumnDimension('D')->setWidth(15);
        $activeSheet->getColumnDimension('E')->setWidth(40);

        $fileKategori = self::pinjamanModel()->_kategori[Pinjaman::POTONGAN];
        if ($kategori == Pinjaman::KOPERASI) {
            $fileKategori = self::pinjamanModel()->_kategori[Pinjaman::KOPERASI];
        }

        $activeSheet->mergeCells('A1:E1')->setCellValue('A1', Yii::t('frontend', 'Template Upload {kategori} Upah', ['kategori' => $fileKategori]));

        $activeSheet->setCellValue('A2', $kategori)->getRowDimension('2')->setVisible(false);

        $activeSheet->setCellValue('A3', Yii::t('frontend', 'No'));
        $activeSheet->setCellValue('B3', Yii::t('frontend', 'Id Pegawai'));
        $activeSheet->setCellValue('C3', Yii::t('frontend', 'Periode Potong'));
        $activeSheet->setCellValue('D3', Yii::t('frontend', 'Jumlah'));
        $activeSheet->setCellValue('E3', Yii::t('frontend', 'Nama Pinjaman/Potongan'));

        $row = 3;
        for ($no = 1; $no <= 2; $no++) {
            $row++;
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, Yii::t('frontend', '1122'))->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, Yii::t('frontend', 'contoh : 2020-01'))->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('D' . $row, 0)->getStyle('D' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat();
            $activeSheet->setCellValue('E' . $row, '')->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));

        }

        $activeSheet->getStyle('A1:E1')->applyFromArray($headerStyle, $outLineBottomBorder);
        $activeSheet->getStyle('A3:E3')->applyFromArray($headerStyle, $outLineBottomBorder);
        $activeSheet->getStyle('A1:E' . $row)->applyFromArray(array_merge($font, $outLineBottomBorder));

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=TemplatePotongan.xls');
        header('Cache-Control: max-age=0');
        ob_end_clean();
        $Excel_writer->save('php://output');

        return true;
    }


    /**
     * Upload file
     *
     * @return bool
     */
    public function upload(): bool
    {
        if ($this->validate()) {
            $docFile = $this->doc_file;
            if ($docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractData(): array
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $reader->setReadDataOnly(true);
        $fileName = $this->upload_dir . $this->doc_file->name;
        $spreadsheet = $reader->load($fileName);
        $startRow = 4;
        $activeSheet = $spreadsheet->getActiveSheet();
        $kategori = $activeSheet->getCell('A2')->getValue();
        $maxRow = $activeSheet->getHighestRow('B');
        $datas = [];
        foreach ($activeSheet->getRowIterator($startRow, $maxRow) as $row) {
            $pegawaiId = $activeSheet->getCell('B' . $row->getRowIndex())->getValue();
            $periodeBayar = $activeSheet->getCell('C' . $row->getRowIndex())->getValue();
            $jumlah = $activeSheet->getCell('D' . $row->getRowIndex())->getValue();
            $nama = $activeSheet->getCell('E' . $row->getRowIndex())->getValue();
            if (($pegawaiId && $periodeBayar && $jumlah)) {
                $datas[$kategori][] = [
                    'id_pegawai' => $pegawaiId,
                    'periode_potong' => $periodeBayar,
                    'jumlah' => $jumlah,
                    'nama' => $nama,
                ];
            }
        }
        $this->renameFile($fileName);
        return $datas;
    }

    /**
     * @param string $fileName
     */
    private function renameFile(string $fileName)
    {
        $oldName = $fileName;
        $newName = $fileName . date('Ymdhis');

        rename($oldName, $newName);
    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function simpan(): void
    {
        $datas = $this->extractData();
        $kategori = key($datas);
        if (!empty($datas[$kategori])) {
            foreach ($datas[$kategori] as $data) {
                $model = new Pinjaman();
                $modelPerjanjianKerja = PerjanjianKerja::findOne(['id_pegawai' => $data['id_pegawai']]);
                if ($modelPerjanjianKerja && $pegawai = $modelPerjanjianKerja->pegawai) {
                    $tipeGajiPokok = '';
                    if ($masterData = $pegawai->masterDataDefaults) {
                        $tipeGajiPokok = $masterData->tipe_gaji_pokok;
                    }
                    $queryPayrolPeriode = PayrollPeriode::find()->andWhere(['tipe' => $tipeGajiPokok])->andWhere('tanggal_akhir LIKE "' . $data['periode_potong'] . '%"')->one();
                    $modelPayrollPeride = PayrollPeriode::findOne($queryPayrolPeriode['id']);

                    if ($modelPayrollPeride) {
                        $model->pegawai_id = $pegawai->id;
                        $model->payroll_periode_id = $modelPayrollPeride->id;
                        $model->kategori = $kategori;
                        $model->tenor = 1;
                        $model->jumlah = (int)$data['jumlah'];
                        $model->angsuran = (int)$data['jumlah'];
                        $model->status_lunas = 0;
                        $model->nama = $data['nama'];
                        $model->save();
                    }
                }
            }
        }
    }
}
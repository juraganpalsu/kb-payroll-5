<?php

namespace frontend\modules\payroll\models\form;

/**
 * Created by PhpStorm.
 * File Name: PayrollBonusForm.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 09/08/2020
 * Time: 21:29
 */

use frontend\modules\payroll\models\PayrollBonus;
use frontend\modules\payroll\models\PayrollBonusDetail;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PayrollBonusForm
 * @package frontend\modules\payroll\models\form
 *
 * @property string $upload_dir;
 * @property string $doc_file;
 */
class PayrollBonusForm extends Model
{

    public $upload_dir = 'uploads/payroll-bonus/';

    public $doc_file;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            /** @var UploadedFile $docFile */
            $docFile = $this->doc_file;
            $docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractDataBonus()
    {
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        $fileName = $this->upload_dir . $this->doc_file->name;
        $spreadsheet = $reader->load($fileName);
//        $name = 'Bonus2020_SATU-H_KBU.xls';
//        $spreadsheet = $reader->load($this->upload_dir . $name);
        $startRow = 4;
        $activeSheet = $spreadsheet->getActiveSheet();
        $maxRow = $activeSheet->getHighestRow('A');
        $datas = [];
        $templateId = $activeSheet->getCell('A1')->getValue();
        $model = PayrollBonus::findOne($templateId);
        if ($model) {
            $modelPayrollPeriode = $model->payrollPeriode;
            $datas['payrollPeriode'] = [
                'id' => $modelPayrollPeriode->id,
                'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
                'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
                'namaPeriode' => $modelPayrollPeriode->namaPeriode,
                'templateId' => $model->id
            ];
            foreach ($activeSheet->getRowIterator($startRow, $maxRow) as $row) {
                $pegawaiId = $activeSheet->getCell('B' . $row->getRowIndex())->getValue();
                $nominalBonus = $activeSheet->getCell('E' . $row->getRowIndex())->getValue();
                $golonganId = $activeSheet->getCell('F' . $row->getRowIndex())->getValue();
                $perjanjianKerjaId = $activeSheet->getCell('G' . $row->getRowIndex())->getValue();
                if ($nominalBonus > 0) {
                    $datas['bonus'][$pegawaiId] = [
                        'pegawaiId' => $pegawaiId,
                        'nominalBonus' => $nominalBonus,
                        'golonganId' => $golonganId,
                        'perjanjianKerjaId' => $perjanjianKerjaId
                    ];
                }
            }
        }
        $this->renameFile($fileName);
        return $datas;
    }

    /**
     * @param string $fileName
     */
    private function renameFile(string $fileName)
    {
        $oldName = $fileName;
        $newName = $fileName . date('Ymdhis');

        rename($oldName, $newName);
    }

    /**
     * @param array $dataBonus
     */
    public function simpanBonus(array $dataBonus)
    {
        if (isset($dataBonus['payrollPeriode']) && $dataBonus['bonus']) {
            $payrollPeriode = $dataBonus['payrollPeriode'];
            $bonus = $dataBonus['bonus'];
            if (!empty($payrollPeriode['templateId'])) {
                $no = 1;
                $model = PayrollBonus::findOne($payrollPeriode['templateId']);
                foreach ($bonus as $data) {
                    $bonusDetail = new PayrollBonusDetail();
                    $bonusDetail->payroll_bonus_id = $model->id;
                    $bonusDetail->pegawai_id = $data['pegawaiId'];
                    $bonusDetail->perjanjian_kerja_id = $data['perjanjianKerjaId'];
                    $bonusDetail->pegawai_golongan_id = $data['golonganId'];
                    $cek = PayrollBonusDetail::findOne(['payroll_bonus_id' => $model->id, 'pegawai_id' => $data['pegawaiId']]);
                    if ($cek) {
                        $bonusDetail = $cek;
                    }
                    $bonusDetail->nominal = $data ? $data['nominalBonus'] : 0;
                    if ($bonusDetail->perjanjianKerja && $bonusDetail->pegawaiGolongan) {
                        if ($bonusDetail->save()) {
                            $no++;
                        } else {
                            Yii::info($bonusDetail->errors, 'exception');
                        }
                    } else {
                        Yii::info($bonusDetail->attributes, 'exception');
                    }
                }
            }
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * File Name: InsentifTidakTetapForm.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 01/09/2020
 * Time: 15:19
 */

namespace frontend\modules\payroll\models\form;


use frontend\modules\payroll\models\InsentifTidakTetap;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class InsentifTidakTetapForm
 * @property string $upload_dir
 * @property UploadedFile $doc_file
 * @package frontend\modules\payroll\models\form
 *
 */
class InsentifTidakTetapForm extends Model
{
    public $upload_dir = 'uploads/insentif-tidak-tetap/';

    public $doc_file;


    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload(): bool
    {
        if ($this->validate()) {
            $docFile = $this->doc_file;
            if ($docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractData(): array
    {
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        $fileName = $this->upload_dir . $this->doc_file->name;
//        $fileName = $this->upload_dir . 'TemplatePayrollDiluarUpah.xls';
        $spreadsheet = $reader->load($fileName);
        $startRow = 4;
        $activeSheet = $spreadsheet->getActiveSheet();
        $maxRow = $activeSheet->getHighestRow('B');
        $datas = [];
        foreach ($activeSheet->getRowIterator($startRow, $maxRow) as $row) {
            $pegawaiId = $activeSheet->getCell('B' . $row->getRowIndex())->getValue();
            $kategori = $activeSheet->getCell('C' . $row->getRowIndex())->getValue();
            $periodeBayar = $activeSheet->getCell('D' . $row->getRowIndex())->getValue();
            $jumlah = $activeSheet->getCell('E' . $row->getRowIndex())->getValue();
            $keterangan = $activeSheet->getCell('F' . $row->getRowIndex())->getValue();
            if (($pegawaiId && $kategori && $periodeBayar && $jumlah)) {
                $datas[] = [
                    'id_pegawai' => $pegawaiId,
                    'kategori' => $kategori,
                    'payroll_periode_id' => $periodeBayar,
                    'jumlah' => $jumlah,
                    'keterangan' => $keterangan,
                ];
            }
        }
        $this->renameFile($fileName);
        return $datas;
    }

    /**
     * @param string $fileName
     */
    private function renameFile(string $fileName)
    {
        $oldName = $fileName;
        $newName = $fileName . date('Ymdhis');

        rename($oldName, $newName);
    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function simpan(): void
    {
        $datas = $this->extractData();
        if (!empty($datas)) {
            foreach ($datas as $data) {
                $model = new InsentifTidakTetap();
                $model->setAttributes($data);
                $kategori = array_flip($model->_kategori);
                $kategori = $kategori[$data['kategori']] ?? '';
                $modelPerjanjianKerja = PerjanjianKerja::findOne(['id_pegawai' => $data['id_pegawai']]);
                if ($kategori && $modelPerjanjianKerja && $pegawai = $modelPerjanjianKerja->pegawai) {
                    $tipeGajiPokok = '';
                    if ($masterData = $pegawai->masterDataDefaults) {
                        $tipeGajiPokok = $masterData->tipe_gaji_pokok;
                    }
                    $queryPayrolPeriode = PayrollPeriode::find()->andWhere(['tipe' => $tipeGajiPokok])->andWhere('tanggal_akhir LIKE "' . $model->payroll_periode_id . '%"')->one();
                    $modelPayrollPeride = PayrollPeriode::findOne($queryPayrolPeriode['id']);

                    if ($modelPayrollPeride) {
                        $model->kategori = $kategori;
                        $model->pegawai_id = $pegawai->id;
                        $model->payroll_periode_id = $modelPayrollPeride->id;
//                        $cek = InsentifTidakTetap::findOne(['kategori' => $kategori, 'pegawai_id' => $pegawai->id, 'payroll_periode_id' => $model->payroll_periode_id]);
//                        if ($cek) {
//                            $model = $cek;
//                        }
                        $model->jumlah = $data['jumlah'];
                        $model->keterangan = $data['keterangan'];
                        $model->save();
                    }
                }
            }
        }
    }

}
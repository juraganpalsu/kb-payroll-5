<?php

namespace frontend\modules\payroll\models\form;

/**
 * Created by PhpStorm.
 * File Name: PayrollBpjsForm.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 19/05/20
 * Time: 22.39
 */


use frontend\modules\payroll\models\PayrollBpjs;
use frontend\modules\payroll\models\PayrollBpjsDetail;
use frontend\modules\pegawai\models\PegawaiBpjs;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PayrollBpjsForm
 * @package frontend\modules\payroll\models\form
 *
 * @property string $upload_dir;
 * @property string $doc_file;
 */
class PayrollBpjsForm extends Model
{

    public $upload_dir = 'uploads/payroll-bpjs/';

    public $doc_file;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            /** @var UploadedFile $docFile */
            $docFile = $this->doc_file;
            $docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractDataBpjs()
    {
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        $fileName = $this->upload_dir . $this->doc_file->name;
        $spreadsheet = $reader->load($fileName);
//        $name = 'POKOK FIX 21-09-2020 20-10-2020_SATU_KBU (3).xls';
//        $spreadsheet = $reader->load($this->upload_dir . $name);
        $startRow = 4;
        $activeSheet = $spreadsheet->getActiveSheet();
        $maxRow = $activeSheet->getHighestRow('A');
        $datas = [];
        $templateId = $activeSheet->getCell('A1')->getValue();
        $model = PayrollBpjs::findOne($templateId);
        if ($model) {
            $modelPayrollPeriode = $model->payrollPeriode;
            $datas['payrollPeriode'] = [
                'id' => $modelPayrollPeriode->id,
                'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
                'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
                'namaPeriode' => $modelPayrollPeriode->namaPeriode,
                'templateId' => $model->id
            ];
            foreach ($activeSheet->getRowIterator($startRow, $maxRow) as $row) {
                $pegawaiId = $activeSheet->getCell('B' . $row->getRowIndex())->getValue();
                $idBpjsK = $activeSheet->getCell('K' . $row->getRowIndex())->getValue();
                $nominalBpjsK = $activeSheet->getCell('F' . $row->getRowIndex())->getValue();
                $idBpjsTk = $activeSheet->getCell('L' . $row->getRowIndex())->getValue();
                $nominalBpjsTk = $activeSheet->getCell('H' . $row->getRowIndex())->getValue();
                $golonganId = $activeSheet->getCell('I' . $row->getRowIndex())->getValue();
                $perjanjianKerjaId = $activeSheet->getCell('J' . $row->getRowIndex())->getValue();
                if (($idBpjsK || $idBpjsTk) && ($nominalBpjsK > 0 || $nominalBpjsTk > 0)) {
                    $datas['bpjs'][$pegawaiId] = [
                        'pegawaiId' => $pegawaiId,
                        'idBpjsK' => $idBpjsK,
                        'nominalBpjsK' => round($nominalBpjsK),
                        'idBpjsTk' => $idBpjsTk,
                        'nominalBpjsTk' => round($nominalBpjsTk),
                        'golonganId' => $golonganId,
                        'perjanjianKerjaId' => $perjanjianKerjaId
                    ];
                }
            }
        }
        $this->renameFile($fileName);
        return $datas;
    }

    /**
     * @param string $fileName
     */
    private function renameFile(string $fileName)
    {
        $oldName = $fileName;
        $newName = $fileName . date('Ymdhis');

        rename($oldName, $newName);
    }

    /**
     * @param array $dataBpjs
     */
    public function simpanBpjs(array $dataBpjs)
    {
        if (isset($dataBpjs['payrollPeriode']) && $dataBpjs['bpjs']) {
            $payrollPeriode = $dataBpjs['payrollPeriode'];
            $bpjs = $dataBpjs['bpjs'];
            if (!empty($payrollPeriode['templateId'])) {
                $no = 1;
                $model = PayrollBpjs::findOne($payrollPeriode['templateId']);
                foreach ($bpjs as $data) {
                    $pegawaiBpjsK = PegawaiBpjs::findOne($data['idBpjsK']);
                    $pegawaiBpjsTk = PegawaiBpjs::findOne($data['idBpjsTk']);
                    if (!$pegawaiBpjsK && !$pegawaiBpjsTk) {
                        Yii::info(Yii::t('frontend', 'id bpjstk {tk} dan bpjskes {kes} tidak ditemukan', ['tk' => $data['idBpjsK'], 'kes' => $data['idBpjsTk']]), 'exception');
                        continue;
                    }
                    $bpjsDetail = new PayrollBpjsDetail();
                    $bpjsDetail->bpjs_id = $model->id;
                    $bpjsDetail->pegawai_bpjs_t_id = $data['idBpjsK'];
                    $bpjsDetail->pegawai_bpjs_tk_id = $data['idBpjsTk'];
                    $bpjsDetail->pegawai_id = $data['pegawaiId'];
                    $bpjsDetail->perjanjian_kerja_id = $data['perjanjianKerjaId'];
                    $bpjsDetail->pegawai_golongan_id = $data['golonganId'];
                    $cek = PayrollBpjsDetail::findOne(['bpjs_id' => $model->id, 'pegawai_id' => $data['pegawaiId']]);
                    if ($cek) {
                        $bpjsDetail = $cek;
                    }
                    $bpjsDetail->bpjs_k_nominal = $pegawaiBpjsK ? (integer)$data['nominalBpjsK'] : 0;
                    $bpjsDetail->bpjs_tk_nominal = $pegawaiBpjsTk ? (integer)$data['nominalBpjsTk'] : 0;
                    if ($bpjsDetail->perjanjianKerja && $bpjsDetail->pegawaiGolongan) {
                        if ($bpjsDetail->save()) {
                            Yii::info($no . '. ' . $bpjsDetail->pegawai->nama, 'exception');
                            $no++;
                        } else {
                            Yii::info($bpjsDetail->errors, 'exception');
                        }
                    } else {
                        Yii::info($bpjsDetail->attributes, 'exception');
                    }
                }
            }
        }
    }
}
<?php


namespace frontend\modules\payroll\models\form;


use frontend\modules\payroll\models\PayrollAsuransiLain;
use frontend\modules\payroll\models\PayrollAsuransiLainDetail;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Created by PhpStorm.
 * File Name: PayrollAsuransiLainForm.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-09-29
 * Time: 10:42 PM
 */
class PayrollAsuransiLainForm extends Model
{

    public $upload_dir = 'uploads/payroll-suransilain/';

    public $doc_file;


    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload(): bool
    {
        if ($this->validate()) {
            /** @var UploadedFile $docFile */
            $docFile = $this->doc_file;
            $docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractData(): array
    {
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        $fileName = $this->upload_dir . $this->doc_file->name;
        $spreadsheet = $reader->load($fileName);
//        $name = 'POKOK FIX 21-09-2020 20-10-2020_SATU_KBU (3).xls';
//        $spreadsheet = $reader->load($this->upload_dir . $name);
        $startRow = 4;
        $activeSheet = $spreadsheet->getActiveSheet();
        $maxRow = $activeSheet->getHighestRow('A');
        $datas = [];
        $templateId = $activeSheet->getCell('A1')->getValue();
        $model = PayrollAsuransiLain::findOne($templateId);
        if ($model) {
            $modelPayrollPeriode = $model->payrollPeriode;
            $datas['payrollPeriode'] = [
                'id' => $modelPayrollPeriode->id,
                'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
                'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
                'namaPeriode' => $modelPayrollPeriode->namaPeriode,
                'templateId' => $model->id
            ];
            foreach ($activeSheet->getRowIterator($startRow, $maxRow) as $row) {
                $pegawaiId = $activeSheet->getCell('B' . $row->getRowIndex())->getValue();
                $nominalPerusahaan = $activeSheet->getCell('G' . $row->getRowIndex())->getValue();
                $nominalKaryawan = $activeSheet->getCell('H' . $row->getRowIndex())->getValue();
                if ($nominalPerusahaan > 0 && $pegawaiId) {
                    $datas['asuransi'][$pegawaiId] = [
                        'pegawaiId' => $pegawaiId,
                        'nominal_perusahaan' => $nominalPerusahaan,
                        'nominal_karyawan' => $nominalKaryawan,
                    ];
                }
            }
        }
        $this->renameFile($fileName);
        return $datas;
    }

    /**
     * @param string $fileName
     */
    private function renameFile(string $fileName)
    {
        $oldName = $fileName;
        $newName = $fileName . date('Ymdhis');

        rename($oldName, $newName);
    }

    /**
     * @param array $dataAsuransi
     */
    public function simpan(array $dataAsuransi)
    {
        if (isset($dataAsuransi['payrollPeriode']) && $dataAsuransi['asuransi']) {
            $payrollPeriode = $dataAsuransi['payrollPeriode'];
            $asuransi = $dataAsuransi['asuransi'];
            if (!empty($payrollPeriode['templateId'])) {
                $no = 1;
                $model = PayrollAsuransiLain::findOne($payrollPeriode['templateId']);
                foreach ($asuransi as $data) {
                    $payrollAsuransiLainDetail = new PayrollAsuransiLainDetail();
                    $payrollAsuransiLainDetail->payroll_asuransi_lain_id = $model->id;
                    $payrollAsuransiLainDetail->pegawai_id = $data['pegawaiId'];
                    $cek = PayrollAsuransiLain::findOne(['payroll_asuransi_lain_id' => $model->id, 'pegawai_id' => $data['pegawaiId']]);
                    if ($cek) {
                        $payrollAsuransiLainDetail = $cek;
                    }
                    $payrollAsuransiLainDetail->potongan_karyawan = $data['nominal_perusahaan'];
                    $payrollAsuransiLainDetail->potongan_perusahaan = $data['nominal_karyawan'];
                    if ($payrollAsuransiLainDetail->perjanjianKerja && $payrollAsuransiLainDetail->pegawaiGolongan) {
                        if ($payrollAsuransiLainDetail->save()) {
                            Yii::info($no . '. ' . $payrollAsuransiLainDetail->pegawai->nama, 'exception');
                            $no++;
                        } else {
                            Yii::info($payrollAsuransiLainDetail->errors, 'exception');
                        }
                    } else {
                        Yii::info($payrollAsuransiLainDetail->attributes, 'exception');
                    }
                }
            }
        }
    }

}
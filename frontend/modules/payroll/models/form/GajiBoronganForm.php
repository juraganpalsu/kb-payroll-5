<?php

namespace frontend\modules\payroll\models\form;

use DateInterval;
use DateTime;
use Exception;
use frontend\models\Absensi;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\GajiBorongan;
use frontend\modules\payroll\models\GajiBoronganHeader;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\Pinjaman;
use frontend\modules\payroll\models\PinjamanAngsuran;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\web\UploadedFile;


/**
 * Created by PhpStorm.
 * File Name: GajiBoronganForm.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 20/04/20
 * Time: 15.30
 */

/**
 * Class GajiBoronganForm
 * @package frontend\modules\payroll\models\form
 *
 * @property string $payroll_periode_id
 * @property string $gaji_borongan_header_id
 *
 * @property string $upload_dir;
 * @property string $doc_file;
 * @property integer $jenis_borongan_id
 */
class GajiBoronganForm extends Model
{
    public $gaji_borongan_header_id;

    public $payroll_periode_id;

    public $upload_dir = 'uploads/gaji-borongan/';

    public $doc_file;
    public $jenis_borongan_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payroll_periode_id', 'jenis_borongan_id'], 'required'],
            [['jenis_borongan_id', 'gaji_borongan_header_id'], 'integer'],
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }

    /**
     * @return array|array[]
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['download'] = ['gaji_borongan_header_id'];
        $scenarios['upload'] = ['doc_file'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
            'jenis_borongan_id' => Yii::t('frontend', 'Jenis Borongan'),
            'payroll_periode_id' => Yii::t('frontend', 'Payroll Periode'),
        ];
    }

    /**
     * @return GajiBoronganHeader
     */
    public static function gajiBoronganModel()
    {
        return new GajiBoronganHeader();
    }

    /**
     * @param GajiBoronganHeader $model
     * @return array
     */
    public function getPegawaiBorongan(GajiBoronganHeader $model)
    {
        $modelPayrollPeriode = PayrollPeriode::findOne($model->payroll_periode_id);
        $tanggal_awal = $modelPayrollPeriode->tanggal_awal;
        $tanggal_akhir = $modelPayrollPeriode->tanggal_akhir;

        $query = Pegawai::find()
            ->joinWith('masterDataDefaults')->andWhere(['master_data_default.tipe_gaji_pokok' => MasterDataDefault::BORONGAN])
            ->joinWith(['perjanjianKerjas' => function (ActiveQuery $query) use ($tanggal_awal, $tanggal_akhir) {
                $query->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarRangeTanggal($tanggal_awal, $tanggal_akhir));
            }])
            ->all();
        $pegawais = [];
        $pegawais['payrollPeriode'] = [
            'id' => $modelPayrollPeriode->id,
            'tanggalAwal' => $tanggal_awal,
            'tanggalAkhir' => $tanggal_akhir,
            'namaPeriode' => $modelPayrollPeriode->namaPeriode,
        ];
        $a = 0;
        /** @var Pegawai $pegawai */
        foreach ($query as $pegawai) {
            $pegawaiGolongan = $pegawai['pegawaiGolongans'];
            $perjanjianKerja = $pegawai['perjanjianKerjas'];
            if ($perjanjianKerja && $pegawaiGolongan) {
                $a++;
//                echo $pegawai->nama_lengkap.$a."<br>";
                $pegawais['pegawai'][$pegawai->id] = [
                    'id' => $pegawai->id,
                    'nama' => $pegawai->nama_lengkap,
                    'idfp' => $perjanjianKerja[0]['id_pegawai'],
                ];
                try {
                    $tanggal_awal = new DateTime($modelPayrollPeriode->tanggal_awal);
                    $tanggal_akhir = new DateTime($modelPayrollPeriode->tanggal_akhir);
                    while ($tanggal_awal <= $tanggal_akhir) {
                        $queryAbsensi = Absensi::findOne(['tanggal' => $tanggal_awal->format('Y-m-d'), 'pegawai_id' => $pegawai->id]);
                        if ($queryAbsensi) {
                            $pegawais['pegawai'][$pegawai->id]['absensi'][$queryAbsensi->tanggal] = [
                                'tanggal' => $queryAbsensi->tanggal,
                                'status_kehadiran' => $queryAbsensi->status_kehadiran,
                                'status_kehadiran_string' => $queryAbsensi->statusKehadiran_,
                                'shift' => $queryAbsensi->timeTable ? $queryAbsensi->timeTable->shift : 0,
                            ];
                        } else {
                            $pegawais['pegawai'][$pegawai->id]['absensi'][$tanggal_awal->format('Y-m-d')] = [
                                'tanggal' => $tanggal_awal->format('Y-m-d'),
                                'status_kehadiran' => 0,
                                'status_kehadiran_string' => '-',
                                'shift' => 0
                            ];
                        }
                        $tanggal_awal->add(new DateInterval('P1D'));
                    }
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        }
        return $pegawais;
    }

    /**
     * @param GajiBoronganHeader $model
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadTemplateGaji(GajiBoronganHeader $model)
    {
        $getDataGaji = $this->getPegawaiBorongan($model);
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setVisible(false);
        $activeSheet->getColumnDimension('C')->setWidth(15);
        $activeSheet->getColumnDimension('D')->setWidth(30);
        $activeSheet->getRowDimension('1')->setVisible(false);

        $activeSheet->setCellValue('A1', $getDataGaji['payrollPeriode']['id']);
        $activeSheet->setCellValue('B1', $model->jenis_borongan);
        $activeSheet->setCellValue('C1', $model->id);
        $activeSheet->mergeCells('A2:A3')->setCellValue('A2', Yii::t('frontend', 'NO'))->getStyle('A2:A3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('B2:B3')->setCellValue('B2', Yii::t('frontend', 'id'))->getStyle('B2:B3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('C2:C3')->setCellValue('C2', Yii::t('frontend', 'ID Pegawai'))->getStyle('C2:C3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('D2:D3')->setCellValue('D2', Yii::t('frontend', 'Nama'))->getStyle('D2:D3')->applyFromArray($headerStyle);


        foreach ($getDataGaji['pegawai'] as $pegawai) {
            if (isset($pegawai['absensi'])) {
                $kolomE = 'E';
                $kolomF = 'F';
                $kolomG = 'G';
                foreach ($pegawai['absensi'] as $absensi) {
                    $activeSheet->getColumnDimension($kolomE)->setWidth(25);
                    $activeSheet->mergeCells($kolomE . '2:' . $kolomG . '2')->setCellValue($kolomE . '2', date('d-m-Y', strtotime($absensi['tanggal'])))->getStyle($kolomE . '2:' . $kolomG . '2')->applyFromArray($headerStyle);
                    $activeSheet->getColumnDimension($kolomE)->setWidth(15);
                    $activeSheet->setCellValue($kolomE . '3', Yii::t('frontend', 'Status Kehadiran'))->getStyle($kolomE . '3')->applyFromArray($headerStyle);
                    $activeSheet->getColumnDimension($kolomF)->setWidth(10);
                    $activeSheet->setCellValue($kolomF . '3', Yii::t('frontend', 'Upah'))->getStyle($kolomF . '3')->applyFromArray($headerStyle);
                    $activeSheet->getColumnDimension($kolomG)->setWidth(10);
                    $activeSheet->setCellValue($kolomG . '3', Yii::t('frontend', 'Shift'))->getStyle($kolomG . '3')->applyFromArray($headerStyle);
                    $kolomE++;
                    $kolomE++;
                    $kolomE++;
                    $kolomF++;
                    $kolomF++;
                    $kolomF++;
                    $kolomG++;
                    $kolomG++;
                    $kolomG++;
                }
            }
        }

        $no = 0;
        $row = 3;
        foreach ($getDataGaji['pegawai'] as $pegawai) {
            if (isset($pegawai['absensi'])) {
                $kolomE = 'E';
                $kolomF = 'F';
                $kolomG = 'G';
                $no++;
                $row++;
                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $pegawai['id'])->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('C' . $row, $pegawai['idfp'])->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('D' . $row, $pegawai['nama'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                foreach ($pegawai['absensi'] as $absensi) {
                    $activeSheet->setCellValue($kolomE . $row, $absensi['status_kehadiran_string'])->getStyle($kolomE . $row)->applyFromArray(array_merge($center, $borderInside));
                    if ($absensi['status_kehadiran'] != Absensi::MASUK) {
                        $activeSheet->setCellValue($kolomF . $row, '')->getStyle($kolomF . $row)->getFill()
                            ->setFillType(Fill::FILL_SOLID)
                            ->getStartColor()->setARGB('C9C1C1');
                    }
                    $activeSheet->setCellValue($kolomG . $row, $absensi['shift'])->getStyle($kolomG . $row)->applyFromArray(array_merge($center, $borderInside));
                    $kolomE++;
                    $kolomE++;
                    $kolomE++;
                    $kolomF++;
                    $kolomF++;
                    $kolomF++;
                    $kolomG++;
                    $kolomG++;
                    $kolomG++;
                }
            }
        }

        $highestColumn = $activeSheet->getHighestColumn('3');

        $activeSheet->getStyle('A2:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A2:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A2:' . $highestColumn . '3')->applyFromArray($headerStyle);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $getDataGaji['payrollPeriode']['namaPeriode'] . '-' . $model->_jenis_borongan[$model->jenis_borongan] . '.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            /** @var UploadedFile $docFile */
            $docFile = $this->doc_file;
            $docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function extractDataGaji()
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->upload_dir . $this->doc_file->name);
//        $name = 'BORONGAN 01-04-2020 15-04-2020-NOODLE (1).xls';
//        $spreadsheet = $reader->load($this->upload_dir . $name);
        $startRow = 4;
        $maxRow = $spreadsheet->getActiveSheet()->getHighestRow('A');
        $maxColumn = $spreadsheet->getActiveSheet()->getHighestColumn('3');
        $datas = [];
        $jenisBorongan = $spreadsheet->getActiveSheet()->getCell('B1')->getValue();
        $gajiBoronganHeaderId = $spreadsheet->getActiveSheet()->getCell('C1')->getValue();
        $model = GajiBoronganHeader::findOne($gajiBoronganHeaderId);
        $modelPayrollPeriode = $model->payrollPeriode;
        if ($modelPayrollPeriode) {
            $kolomE = 'E';
            $datas['payrollPeriode'] = [
                'id' => $model->id,
                'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
                'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
                'namaPeriode' => $modelPayrollPeriode->namaPeriode,
                'jenisBorongan' => $jenisBorongan
            ];
            foreach ($spreadsheet->getActiveSheet()->getRowIterator($startRow, $maxRow) as $row) {
                $pegawaiId = $spreadsheet->getActiveSheet()->getCell('B' . $row->getRowIndex())->getValue();
                $idfp = $spreadsheet->getActiveSheet()->getCell('C' . $row->getRowIndex())->getValue();
                $nama = $spreadsheet->getActiveSheet()->getCell('D' . $row->getRowIndex())->getValue();
                $datas['pegawai'][$pegawaiId] = ['id' => $pegawaiId, 'idfp' => $idfp, 'nama' => $nama];
                $kolom = 3;
                foreach ($spreadsheet->getActiveSheet()->getColumnIterator($kolomE, $maxColumn) as $column) {
                    if ($kolom == 3) {
                        $tanggal = $spreadsheet->getActiveSheet()->getCell($column->getColumnIndex() . '2')->getValue();
                        $kolomUpah = $column->getColumnIndex();
                        $kolomUpah++;
                        $gaji = $spreadsheet->getActiveSheet()->getCell($kolomUpah . $row->getRowIndex())->getValue();
                        $kolomUpah++;
                        $shift = $spreadsheet->getActiveSheet()->getCell($kolomUpah . $row->getRowIndex())->getValue();
                        $datas['pegawai'][$pegawaiId]['absensi'][] = [
                            'tanggal' => date('Y-m-d', strtotime($tanggal)),
                            'gaji' => round($gaji),
                            'shift' => $shift
                        ];
                        $kolom = 0;
                    }
                    $kolom++;
                }
            }
        }
        return $datas;
    }

    /**
     * @param array $dataGaji
     */
    public function simpanGaji(array $dataGaji)
    {
        if (isset($dataGaji['payrollPeriode']) && $dataGaji['pegawai']) {
            $payrollPeriode = GajiBoronganHeader::findOne($dataGaji['payrollPeriode']['id']);
            $pegawais = $dataGaji['pegawai'];
            foreach ($pegawais as $pegawai) {
                foreach ($pegawai['absensi'] as $absensi) {
                    $modelsAbsensi = Absensi::findOne(['tanggal' => $absensi['tanggal'], 'pegawai_id' => $pegawai['id'], 'status_kehadiran' => Absensi::MASUK]);
                    if ($modelsAbsensi) {
                        $gajiBorongan = new GajiBorongan();
                        $gajiBorongan->tanggal = $absensi['tanggal'];
                        $gajiBorongan->gaji_borongan_header_id = $payrollPeriode['id'];
                        $gajiBorongan->pegawai_id = $pegawai['id'];
                        $cek = GajiBorongan::findOne(['tanggal' => $absensi['tanggal'], 'pegawai_id' => $pegawai['id']]);
                        if ($cek) {
                            $gajiBorongan = $cek;
                        }
                        $gajiBorongan->shift = $absensi['shift'];
                        $gajiBorongan->gaji = (int)$absensi['gaji'];
                        $gajiBorongan->save();
                    }
                }
            }
        }
    }

    /**
     * @param GajiBoronganHeader $model
     * @return array
     */
    public function getDataGajiPerPeriode(GajiBoronganHeader $model)
    {
        $query = GajiBorongan::find()->andWhere(['gaji_borongan_header_id' => $model->id])->all();
        $gajiBorongans = [];
        if ($query) {
            $modelPayrollPeriode = $model->payrollPeriode;
            $gajiBorongans['payrollPeriode'] = [
                'id' => $modelPayrollPeriode->id,
                'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
                'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
                'namaPeriode' => $modelPayrollPeriode->namaPeriode,
            ];
            /** @var GajiBorongan $gajiBorongan */
            foreach ($query as $gajiBorongan) {
                $gajiBorongans['pegawai'][$gajiBorongan->pegawai_id]['datapegawai'] = [
                    'pegawaiid' => $gajiBorongan->pegawai_id,
                    'nama' => $gajiBorongan->pegawai->nama_lengkap,
                    'idfp' => $gajiBorongan->perjanjianKerja->id_pegawai,
                    'costcenter' => $gajiBorongan->pegawai ? $gajiBorongan->pegawai->costCenter ? $gajiBorongan->pegawai->costCenter->costCenterTemplate->kode : '' : ''
                ];
                $gajiBorongans['pegawai'][$gajiBorongan->pegawai_id]['tanggal'][] = $gajiBorongan->tanggal;
                $gajiBorongans['pegawai'][$gajiBorongan->pegawai_id]['gaji'][] = $gajiBorongan->gaji;
                $gajiBorongans['pegawai'][$gajiBorongan->pegawai_id]['shift'][] = $gajiBorongan->shift;
            }
        }
        return $gajiBorongans;
    }


    /**
     * @param GajiBoronganHeader $model
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadRekapGaji(GajiBoronganHeader $model)
    {
        $getDataGaji = $this->getDataGajiPerPeriode($model);
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $right = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setVisible(false);
        $activeSheet->getColumnDimension('C')->setWidth(15);
        $activeSheet->getColumnDimension('D')->setWidth(30);
        $activeSheet->getColumnDimension('E')->setWidth(15);
        $activeSheet->getColumnDimension('F')->setWidth(10);
        $activeSheet->getColumnDimension('G')->setWidth(15);
        $activeSheet->getColumnDimension('H')->setWidth(5);
        $activeSheet->getColumnDimension('I')->setWidth(5);
        $activeSheet->getColumnDimension('J')->setWidth(10);
        $activeSheet->getColumnDimension('K')->setWidth(15);
        $activeSheet->getColumnDimension('L')->setWidth(15);
        $activeSheet->getColumnDimension('M')->setWidth(15);
        $activeSheet->getColumnDimension('N')->setWidth(15);
        $activeSheet->getColumnDimension('O')->setWidth(15);
        $activeSheet->getColumnDimension('P')->setWidth(15);
        $activeSheet->getColumnDimension('Q')->setWidth(20);

        $activeSheet->setCellValue('A1', Yii::t('frontend', 'Periode'))->getStyle('A1');
        $activeSheet->setCellValue('C1', ': ' . $model->payrollPeriode->namaPeriode)->getStyle('C1');
        $activeSheet->setCellValue('A2', Yii::t('frontend', 'Jenis'))->getStyle('A2');
        $activeSheet->setCellValue('C2', ': ' . $model->_jenis_borongan[$model->jenis_borongan])->getStyle('C2');

        $activeSheet->setCellValue('A4', Yii::t('frontend', 'NO'))->getStyle('A4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('B4', Yii::t('frontend', 'id'))->getStyle('B4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('C4', Yii::t('frontend', 'ID Pegawai'))->getStyle('C4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('D4', Yii::t('frontend', 'Nama'))->getStyle('D4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('E4', Yii::t('frontend', 'Bagian'))->getStyle('E4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('F4', Yii::t('frontend', 'Jumlah Hadir'))->getStyle('F4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('G4', Yii::t('frontend', 'Gaji Borongan'))->getStyle('G4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('H4', Yii::t('frontend', 'Shift 2'))->getStyle('H4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('I4', Yii::t('frontend', 'Shift 3'))->getStyle('I4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('J4', Yii::t('frontend', 'Uang Shift'))->getStyle('J4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('K4', Yii::t('frontend', 'Potongan'))->getStyle('K4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('L4', Yii::t('frontend', 'Koperasi'))->getStyle('L4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('M4', Yii::t('frontend', 'Kasbon'))->getStyle('M4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('N4', Yii::t('frontend', 'Total Diterima'))->getStyle('N4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('O4', Yii::t('frontend', 'Cost Center'))->getStyle('O4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('P4', Yii::t('frontend', 'Nama Bank'))->getStyle('P4')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('Q4', Yii::t('frontend', 'Nomor Rekening'))->getStyle('Q4')->applyFromArray($headerStyle);

        $no = 0;
        $row = 4;
        foreach ($getDataGaji['pegawai'] as $pegawai) {
            $no++;
            $row++;

            $jumlahHadir = count($pegawai['tanggal']);
            $gajiBorongan = array_sum($pegawai['gaji']);
            $shift2 = !empty(array_count_values($pegawai['shift'])[2]) ? array_count_values($pegawai['shift'])[2] : 0;
            $shift3 = !empty(array_count_values($pegawai['shift'])[3]) ? array_count_values($pegawai['shift'])[3] : 0;
            $uangShift = $shift2 * MasterDataDefault::getUangShift(2) + $shift3 * MasterDataDefault::getUangShift(3);
            $modelAngsuran = new PinjamanAngsuran();
            $potongan = array_sum($modelAngsuran->getAngsuran($model->payrollPeriode->id, $pegawai['datapegawai']['pegawaiid'], Pinjaman::POTONGAN));
            $potonganAtl = array_sum($modelAngsuran->getAngsuran($model->payrollPeriode->id, $pegawai['datapegawai']['pegawaiid'], Pinjaman::POTONGAN_ATL));
            $koperasi = array_sum($modelAngsuran->getAngsuran($model->payrollPeriode->id, $pegawai['datapegawai']['pegawaiid'], Pinjaman::KOPERASI));
            $kasbon = array_sum($modelAngsuran->getAngsuran($model->payrollPeriode->id, $pegawai['datapegawai']['pegawaiid'], Pinjaman::KASBON));
            $totalDiterima = $gajiBorongan + $uangShift - $potongan - $koperasi - $potonganAtl;
            $dataPegawai = \common\models\Pegawai::findOne($pegawai['datapegawai']['pegawaiid']);
            $namaBank = $dataPegawai ? $dataPegawai->pegawaiBank ? $dataPegawai->pegawaiBank->namaBank : '' : '';
            $noRekening = $dataPegawai ? $dataPegawai->pegawaiBank ? $dataPegawai->pegawaiBank->nomer_rekening : '' : '';

            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, $pegawai['datapegawai']['pegawaiid'])->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, str_pad($pegawai['datapegawai']['idfp'], 8, '0', STR_PAD_LEFT))->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('D' . $row, $pegawai['datapegawai']['nama'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('E' . $row, $model->catatan)->getStyle('E' . $row)->applyFromArray(array_merge($borderInside, $center));
            $activeSheet->setCellValue('F' . $row, $jumlahHadir)->getStyle('F' . $row)->applyFromArray(array_merge($borderInside, $center));
            $activeSheet->setCellValue('G' . $row, $gajiBorongan)->getStyle('G' . $row)->applyFromArray(array_merge($borderInside, $right));
            $activeSheet->setCellValue('H' . $row, $shift2)->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('I' . $row, $shift3)->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('J' . $row, $uangShift)->getStyle('J' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('K' . $row, $potongan + $potonganAtl)->getStyle('K' . $row)->applyFromArray(array_merge($borderInside, $right));
            $activeSheet->setCellValue('L' . $row, $koperasi)->getStyle('L' . $row)->applyFromArray(array_merge($borderInside, $right));
            $activeSheet->setCellValue('M' . $row, $kasbon)->getStyle('M' . $row)->applyFromArray(array_merge($borderInside, $right));
            $activeSheet->setCellValue('N' . $row, round($totalDiterima, 0))->getStyle('N' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('O' . $row, $pegawai['datapegawai']['costcenter'])->getStyle('O' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('P' . $row, $namaBank)->getStyle('P' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('Q' . $row, $noRekening)->getStyle('Q' . $row)->applyFromArray(array_merge($borderInside, $right));
        }

        $highestColumn = $activeSheet->getHighestColumn('4');

        $activeSheet->getStyle('A3:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A4:' . $highestColumn . '3')->applyFromArray($headerStyle);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=GajiUntukPeriode' . $getDataGaji['payrollPeriode']['namaPeriode'] . '-' . $model->_jenis_borongan[$model->jenis_borongan] . '.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }

}
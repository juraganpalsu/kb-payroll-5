<?php
/**
 * Created by PhpStorm.
 * File Name: PayrollDiluarProsesForm.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 01/09/2020
 * Time: 15:19
 */

namespace frontend\modules\payroll\models\form;


use DateTime;
use frontend\modules\payroll\models\PayrollDiluarProses;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PayrollDiluarProsesForm
 * @package frontend\modules\payroll\models\form
 *
 * @property string $upload_dir
 * @property UploadedFile $doc_file
 */
class PayrollDiluarProsesForm extends Model
{
    public $upload_dir = 'uploads/payroll-diluar-proses/';

    public $doc_file;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            $docFile = $this->doc_file;
            if ($docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractData()
    {
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        $fileName = $this->upload_dir . $this->doc_file->name;
//        $fileName = $this->upload_dir . 'TemplatePayrollDiluarUpah.xls';
        $spreadsheet = $reader->load($fileName);
        $startRow = 4;
        $activeSheet = $spreadsheet->getActiveSheet();
        $maxRow = $activeSheet->getHighestRow('B');
        $datas = [];
        foreach ($activeSheet->getRowIterator($startRow, $maxRow) as $row) {
            $pegawaiId = $activeSheet->getCell('B' . $row->getRowIndex())->getValue();
            $jenis = $activeSheet->getCell('C' . $row->getRowIndex())->getValue();
            $tanggalBayar = $activeSheet->getCell('D' . $row->getRowIndex())->getValue();
            $tanggalBayar = Date::excelToDateTimeObject($tanggalBayar);

            $periodeKurang = $activeSheet->getCell('E' . $row->getRowIndex())->getValue();
            $jumlah = $activeSheet->getCell('F' . $row->getRowIndex())->getValue();
            $keterangan = $activeSheet->getCell('G' . $row->getRowIndex())->getValue();
            if (($pegawaiId && $jenis && $tanggalBayar && $periodeKurang && $jumlah)) {
                $datas[] = [
                    'id_pegawai' => $pegawaiId,
                    'jenis' => $jenis,
                    'tanggal_bayar' => $tanggalBayar,
                    'proses_gaji_id' => $periodeKurang,
                    'jumlah' => $jumlah,
                    'keterangan' => $keterangan,
                ];
            }
        }
        $this->renameFile($fileName);
        return $datas;
    }

    /**
     * @param string $fileName
     */
    private function renameFile(string $fileName)
    {
        $oldName = $fileName;
        $newName = $fileName . date('Ymdhis');

        rename($oldName, $newName);
    }

    /**
     * @param array $datas
     */
    public function simpan(array $datas)
    {
        if (!empty($datas)) {
            foreach ($datas as $data) {
                $model = new PayrollDiluarProses();
                $model->setAttributes($data);
                /** @var DateTime $tanggalBayar */
                $tanggalBayar = $data['tanggal_bayar'];
                $model->tanggal_bayar = $tanggalBayar->format('Y-m-d');
                $jenis = array_flip($model->_jenis);
                $jenis = $jenis[$data['jenis']] ?? '';
                $modelPerjanjianKerja = PerjanjianKerja::findOne(['id_pegawai' => $data['id_pegawai']]);
                if ($jenis && $modelPerjanjianKerja && $pegawai = $modelPerjanjianKerja->pegawai) {
                    $model->jenis = $jenis;
                    $model->pegawai_id = $pegawai->id;
                    $cek = PayrollDiluarProses::findOne(['jenis' => $jenis, 'pegawai_id' => $pegawai->id, 'proses_gaji_id' => $model->proses_gaji_id]);
                    if ($cek) {
                        $model = $cek;
                    }
                    $model->jumlah = $data['jumlah'];
                    $model->keterangan = $data['keterangan'];
                    $model->save();
                }
            }
        }
    }

}
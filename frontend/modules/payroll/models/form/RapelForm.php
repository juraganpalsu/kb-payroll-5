<?php


namespace frontend\modules\payroll\models\form;


use frontend\modules\payroll\models\Rapel;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class RapelForm
 * @property string $upload_dir
 * @property UploadedFile $doc_file
 * @package frontend\modules\payroll\models\form
 */
class RapelForm extends Model
{
    public $upload_dir = 'uploads/rapel/';

    public $doc_file;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls', 'maxFiles' => 1],
        ];
    }


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload(): bool
    {
        if ($this->validate()) {
            $docFile = $this->doc_file;
            if ($docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractData(): array
    {
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        $fileName = $this->upload_dir . $this->doc_file->name;
//        $fileName = $this->upload_dir . 'TemplateRapel.xls';
        $spreadsheet = $reader->load($fileName);
        $startRow = 4;
        $activeSheet = $spreadsheet->getActiveSheet();
        $maxRow = $activeSheet->getHighestRow('B');
        $datas = [];
        foreach ($activeSheet->getRowIterator($startRow, $maxRow) as $row) {
            $pegawaiId = $activeSheet->getCell('B' . $row->getRowIndex())->getValue();
            $periodeKurang = $activeSheet->getCell('C' . $row->getRowIndex())->getValue();
            $periodeBayar = $activeSheet->getCell('D' . $row->getRowIndex())->getValue();
            $jumlah = $activeSheet->getCell('E' . $row->getRowIndex())->getValue();
            $keterangan = $activeSheet->getCell('F' . $row->getRowIndex())->getValue();
            if (($pegawaiId && $periodeBayar && $jumlah)) {
                $datas[] = [
                    'id_pegawai' => $pegawaiId,
                    'periode_kurang' => $periodeKurang,
                    'periode_bayar' => $periodeBayar,
                    'jumlah' => $jumlah,
                    'keterangan' => $keterangan,
                ];
            }
        }
        $this->renameFile($fileName);
        return $datas;
    }

    /**
     * @param string $fileName
     */
    private function renameFile(string $fileName)
    {
        $oldName = $fileName;
        $newName = $fileName . date('Ymdhis');

        rename($oldName, $newName);
    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function simpan(): void
    {
        $datas = $this->extractData();
        if (!empty($datas)) {
            foreach ($datas as $data) {
                $model = new Rapel();
                $model->setAttributes($data);
                $modelPerjanjianKerja = PerjanjianKerja::findOne(['id_pegawai' => (int)$data['id_pegawai']]);
                if ($modelPerjanjianKerja && $pegawai = $modelPerjanjianKerja->pegawai) {
                    $tipeGajiPokok = '';
                    if ($masterData = $pegawai->masterDataDefaults) {
                        $tipeGajiPokok = $masterData->tipe_gaji_pokok;
                    }
                    $queryPayrolPeriodeKurang = PayrollPeriode::find()->andWhere(['tipe' => $tipeGajiPokok])->andWhere('tanggal_akhir LIKE "' . $model->periode_kurang . '%"')->one();
                    $modelPayrollPerideKurang = PayrollPeriode::findOne($queryPayrolPeriodeKurang['id']);
                    $queryPayrolPeriode = PayrollPeriode::find()->andWhere(['tipe' => $tipeGajiPokok])->andWhere('tanggal_akhir LIKE "' . $model->periode_bayar . '%"')->one();
                    $modelPayrollPeride = PayrollPeriode::findOne($queryPayrolPeriode['id']);

                    if ($modelPayrollPeride) {
                        $model->pegawai_id = $pegawai->id;
                        $model->periode_kurang = $modelPayrollPerideKurang->id;
                        $model->periode_bayar = $modelPayrollPeride->id;
                        $model->jumlah = $data['jumlah'];
                        $model->keterangan = $data['keterangan'];
                        $model->save();
                    }
                }
            }
        }
    }

    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadTemplate()
    {
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(15);
        $activeSheet->getColumnDimension('D')->setWidth(15);
        $activeSheet->getColumnDimension('E')->setWidth(15);
        $activeSheet->getColumnDimension('F')->setWidth(15);

        $activeSheet->mergeCells('A1:F1')->setCellValue('A1', Yii::t('frontend', 'Template Upload Repel'));

        $activeSheet->setCellValue('A3', Yii::t('frontend', 'No'));
        $activeSheet->setCellValue('B3', Yii::t('frontend', 'Id Pegawai'));
        $activeSheet->setCellValue('C3', Yii::t('frontend', 'Periode Kurang'));
        $activeSheet->setCellValue('D3', Yii::t('frontend', 'Periode Bayar'));
        $activeSheet->setCellValue('E3', Yii::t('frontend', 'Jumlah'));
        $activeSheet->setCellValue('F3', Yii::t('frontend', 'Keterangan'));

        $row = 3;
        for ($no = 1; $no <= 5; $no++) {
            $row++;
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, Yii::t('frontend', '1122'))->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, Yii::t('frontend', 'contoh : 2021-01'))->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('D' . $row, Yii::t('frontend', 'contoh : 2021-01'))->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('E' . $row, 0)->getStyle('E' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat();
            $activeSheet->setCellValue('F' . $row, '')->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));

        }
        $highestColumn = $activeSheet->getHighestColumn('3');

        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A1:F1')->applyFromArray($headerStyle, $outLineBottomBorder);
        $activeSheet->getStyle('A3:F3')->applyFromArray($headerStyle, $outLineBottomBorder);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=TemplateRapel.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (\PhpOffice\PhpSpreadsheet\Writer\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }

}
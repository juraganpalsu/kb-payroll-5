<?php

namespace frontend\modules\payroll\models\search;

use frontend\modules\payroll\models\GajiPph21;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\GajiPph21Search represents the model behind the search form about `frontend\modules\payroll\models\GajiPph21`.
 * @property integer $id_pegawai
 * @property string $pegawai_id
 * @property integer $busines_unit
 * @property integer $golongan
 */
class GajiPph21Search extends GajiPph21
{
    public $id_pegawai;

    public $pegawai_id;
    public $busines_unit;
    public $payroll_periode_id;

    public $golongan;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'busines_unit', 'ptkp_status', 'gaji_bulanan_id', 'pegawai_ptkp_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'payroll_periode_id'], 'safe'],
            [['gaji', 'tunjangan_pph', 'tunjangan', 'premi_asuransi', 'bonus', 'penghasilan_bruto', 'biaya_jabatan', 'iuran_pensiun', 'pengurangan', 'penghasilan_netto', 'penghasilan_netto_setahun', 'ptkp_nominal', 'pkp', 'pph21_terhutang_setahun', 'pph21_terhutang'], 'number'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'golongan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = GajiPph21::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->pegawai_id)) {
            $query->joinWith(['pegawaiPtkp' => function (ActiveQuery $query) {
                $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                    $query->andWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);
                }]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith(['pegawaiPtkp' => function (ActiveQuery $query) {
                $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                    $query->joinWith(['perjanjianKerja' => function (ActiveQuery $query) {
                        $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
                    }]);
                }]);
            }]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith(['pegawaiPtkp' => function (ActiveQuery $query) {
                $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                    $query->joinWith(['pegawaiGolongan' => function (ActiveQuery $query) {
                        $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
                    }]);
                }]);
            }]);
        }

        if (!empty($this->payroll_periode_id)) {
            $query->joinWith(['gajiBulanan' => function (ActiveQuery $query) {
                $query->joinWith(['prosesGaji' => function (ActiveQuery $query) {
                    $query->andWhere(['proses_gaji.payroll_periode_id' => $this->payroll_periode_id]);
                }]);
            }]);
        }

        $query->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\payroll\models\search;

use frontend\modules\payroll\models\GajiBoronganHeader;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\payroll\models\search\GajiBoronganHeaderSearch represents the model behind the search form about `frontend\modules\payroll\models\GajiBoronganHeader`.
 */
class GajiBoronganHeaderSearch extends GajiBoronganHeader
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payroll_periode_id', 'catatan', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['jenis_borongan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, $statusFinish = GajiBoronganHeader::dibuat)
    {
        $query = GajiBoronganHeader::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($statusFinish)) {
            $query->andWhere(['finishing_status' => $statusFinish]);
        }

        $query->andFilterWhere([
            'jenis_borongan' => $this->jenis_borongan,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'created_by_struktur' => $this->created_by_struktur,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'payroll_periode_id', $this->payroll_periode_id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }
}

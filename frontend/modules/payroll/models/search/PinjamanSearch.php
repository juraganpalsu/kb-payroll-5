<?php

namespace frontend\modules\payroll\models\search;

use frontend\modules\payroll\models\Pinjaman;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\PinjamanSearch represents the model behind the search form about `frontend\modules\payroll\models\Pinjaman`.
 */
class PinjamanSearch extends Pinjaman
{
    /**
     * @var int
     */
    public $id_pegawai;
    public $busines_unit;
    public $golongan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pegawai', 'nama', 'payroll_periode_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'nama', 'tenor', 'angsuran', 'jumlah'], 'safe'],
            [['kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'busines_unit', 'golongan', 'status_lunas'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @param bool $filterFinance
     * @param array $kategori
     * @return ActiveDataProvider
     */
    public function search(array $params, bool $filterFinance = true, array $kategori = [])
    {
        $query = Pinjaman::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        if ($filterFinance && !empty($kategori)) {
            $query->andWhere(['IN', 'kategori', $kategori]);
        }

        $query->andFilterWhere([
            'pinjaman.kategori' => $this->kategori,
            'status_lunas' => $this->status_lunas
        ]);

        $query->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['like', 'pinjaman.nama', $this->nama])
            ->andFilterWhere(['like', 'pinjaman.tenor', $this->tenor])
            ->andFilterWhere(['like', 'pinjaman.angsuran', $this->angsuran])
            ->andFilterWhere(['like', 'pinjaman.jumlah', $this->jumlah])
            ->andFilterWhere(['pinjaman.payroll_periode_id' => $this->payroll_periode_id])
            ->orderBy('created_at DESC');

        return $dataProvider;
    }
}

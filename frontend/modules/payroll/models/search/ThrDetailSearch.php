<?php

namespace frontend\modules\payroll\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\payroll\models\ThrDetail;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\ThrDetailSearch represents the model behind the search form about `frontend\modules\payroll\models\ThrDetail`.
 */
 class ThrDetailSearch extends ThrDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'catatan', 'thr_id', 'cut_off', 'tanggal_mulai', 'golongan', 'jenis_perjanjian', 'bisnis_unit', 'nama', 'id_pegawai', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'p_gaji_pokok_id', 'p_tunjangan_loyalitas_config_id', 'p_tunjangan_anak_config_id', 'p_tunjangan_lain_tetap_id', 'p_tunjangan_jabatan_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['nominal', 'masa_kerja_bulan', 'masa_kerja_tahun', 'gaji_pokok_thr', 'p_gaji_pokok_nominal', 'p_tunjangan_loyalitas_config_nominal', 'p_tunjangan_anak_config_nominal', 'p_tunjangan_lain_tetap_nominal', 'p_tunjangan_jabatan_nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['presentase_thr'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

     /**
      * @param $params
      * @param string $idp
      * @param array $filterDept
      * @return ActiveDataProvider
      */
    public function search($params, $idp = '', $filterDept = [])
    {
        $query = ThrDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($idp) {
            $query->andWhere(['thr_id' => $idp]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if (!empty($filterDept)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) use ($filterDept) {
                $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) use ($filterDept) {
                    $query->joinWith(['struktur' => function (ActiveQuery $query) use ($filterDept) {
                        $query->joinWith(['departemen' => function (ActiveQuery $query) use ($filterDept) {
                            $query->andWhere(['like', 'departemen.nama', $filterDept]);
                        }]);
                    }]);
                }]);
            }]);

        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'id_pegawai', $this->id_pegawai])
            ->andFilterWhere(['like', 'pegawai_id', $this->pegawai_id]);

        return $dataProvider;
    }
}

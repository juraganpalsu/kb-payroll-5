<?php

namespace frontend\modules\payroll\models\search;


use frontend\models\User;
use frontend\modules\payroll\models\GajiBulanan;
use frontend\modules\payroll\models\ProsesGaji;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\GajiBulananSearch represents the model behind the search form about `frontend\modules\payroll\models\GajiBulanan`.
 */
class GajiBulananSearch extends GajiBulanan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'insentif_tidak_tetap_id', 'rapel_id', 'nama_lengkap', 'bisnis_unit_s', 'golongan_s', 'jenis_kontrak_s', 'struktur_s', 'p_gaji_pokok_id', 'p_admin_bank_id', 'pinjaman_angsuran_id', 'p_insentif_tetap_id', 'p_premi_hadir_id', 'p_sewa_mobil_id', 'p_sewa_motor_id', 'p_sewa_rumah_id', 'p_tunjagan_lain_tetap_id', 'p_tunjangan_anak_config_id', 'p_tunjangan_jabatan_id', 'p_tunjangan_kost_id', 'p_tunjangan_loyalitas_config_id', 'p_tunjangan_pulsa_id', 'p_uang_makan_id', 'p_uang_transport_id', 'pegawai_bpjs_id', 'pegawai_bank_id', 'pegawai_bank_nama', 'proses_gaji_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['id_pegawai', 'tipe_gaji_pokok', 'insentif_tidak_tetap', 'rapel', 'masuk', 'alfa', 'ijin', 'libur', 'cuti', 'undifined', 's2', 's3', 'p_gaji_pokok', 'p_admin_bank', 'pinjaman_nominal', 'angsuran_ke', 'p_insentif_tetap', 'p_premi_hadir', 'p_sewa_mobil', 'p_sewa_motor', 'p_sewa_rumah', 'p_tunjagan_lain_tetap', 'p_tunjangan_anak', 'p_tunjangan_jabatan', 'p_tunjangan_kost', 'p_tunjangan_loyalitas', 'p_tunjangan_pulsa', 'p_uang_makan', 'p_uang_transport', 'hari_kerja_aktif', 'pegawai_bpjs_kes', 'pegawai_bpjs_tk', 'pegawai_bank_nomor', 'jumlah_telat', 'pegawai_sistem_kerja_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'dispensasi', 'is', 'cm', 'off', 'idl', 'ith', 'sd', 'wfh', 'atl', 'lock'], 'integer'],
            [['jam_lembur', 'uang_lembur', 'uml'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @param string $idp
     * @param array $filterDept
     * @return ActiveDataProvider
     */
    public function search(array $params, string $idp = '', array $filterDept = []): ActiveDataProvider
    {
        $query = GajiBulanan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($idp) {
            $query->andWhere(['proses_gaji_id' => $idp]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($filterDept)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) use ($filterDept) {
                $query->joinWith(['pegawaiStruktur' => function (ActiveQuery $query) use ($filterDept) {
                    $query->joinWith(['struktur' => function (ActiveQuery $query) use ($filterDept) {
                        $query->joinWith(['departemen' => function (ActiveQuery $query) use ($filterDept) {
                            $query->andWhere(['like', 'departemen.nama', $filterDept]);
                        }]);
                    }]);
                }]);
            }]);

        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'insentif_tidak_tetap_id', $this->insentif_tidak_tetap_id])
            ->andFilterWhere(['like', 'rapel_id', $this->rapel_id])
            ->andFilterWhere(['like', 'nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'bisnis_unit_s', $this->bisnis_unit_s])
            ->andFilterWhere(['like', 'golongan_s', $this->golongan_s])
            ->andFilterWhere(['like', 'jenis_kontrak_s', $this->jenis_kontrak_s])
            ->andFilterWhere(['like', 'struktur_s', $this->struktur_s])
            ->andFilterWhere(['like', 'p_gaji_pokok_id', $this->p_gaji_pokok_id])
            ->andFilterWhere(['like', 'p_admin_bank_id', $this->p_admin_bank_id])
            ->andFilterWhere(['like', 'pinjaman_angsuran_id', $this->pinjaman_angsuran_id])
            ->andFilterWhere(['like', 'p_insentif_tetap_id', $this->p_insentif_tetap_id])
            ->andFilterWhere(['like', 'p_premi_hadir_id', $this->p_premi_hadir_id])
            ->andFilterWhere(['like', 'p_sewa_mobil_id', $this->p_sewa_mobil_id])
            ->andFilterWhere(['like', 'p_sewa_motor_id', $this->p_sewa_motor_id])
            ->andFilterWhere(['like', 'p_sewa_rumah_id', $this->p_sewa_rumah_id])
            ->andFilterWhere(['like', 'p_tunjagan_lain_tetap_id', $this->p_tunjagan_lain_tetap_id])
            ->andFilterWhere(['like', 'p_tunjangan_anak_config_id', $this->p_tunjangan_anak_config_id])
            ->andFilterWhere(['like', 'p_tunjangan_jabatan_id', $this->p_tunjangan_jabatan_id])
            ->andFilterWhere(['like', 'p_tunjangan_kost_id', $this->p_tunjangan_kost_id])
            ->andFilterWhere(['like', 'p_tunjangan_loyalitas_config_id', $this->p_tunjangan_loyalitas_config_id])
            ->andFilterWhere(['like', 'p_tunjangan_pulsa_id', $this->p_tunjangan_pulsa_id])
            ->andFilterWhere(['like', 'p_uang_makan_id', $this->p_uang_makan_id])
            ->andFilterWhere(['like', 'p_uang_transport_id', $this->p_uang_transport_id])
            ->andFilterWhere(['like', 'pegawai_bpjs_id', $this->pegawai_bpjs_id])
            ->andFilterWhere(['like', 'pegawai_bank_id', $this->pegawai_bank_id])
            ->andFilterWhere(['like', 'pegawai_bank_nama', $this->pegawai_bank_nama])
//            ->andFilterWhere(['like', 'proses_gaji_id', $this->proses_gaji_id])
            ->andFilterWhere(['like', 'pegawai_id', $this->pegawai_id]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchMyPayroll(array $params)
    {
        $query = GajiBulanan::find();


        $query->joinWith(['prosesGaji' => function (ActiveQuery $query) {
            $query->andWhere(['finishing_status' => ProsesGaji::selesai]);
            $query->joinWith(['payrollPeriode' => function(ActiveQuery $query){
                $query->andWhere(['month(tanggal_akhir)' => 7,'year(tanggal_akhir)' => 2021]);
            }]);
        }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($modelPegawai = User::pegawai()) {
            $query->andWhere(['pegawai_id' => $modelPegawai->id]);
        } else {
            $query->andWhere(['pegawai_id' => 0]);
        }

        $query->orderBy('created_at DESC');
        return $dataProvider;

    }
}

<?php

namespace frontend\modules\payroll\models\search;

use frontend\modules\payroll\models\PayrollDiluarProses;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\PayrollDiluarProsesSearch represents the model behind the search form about `frontend\modules\payroll\models\PayrollDiluarProses`.
 * @property mixed $monthDropdown
 */
class PayrollDiluarProsesSearch extends PayrollDiluarProses
{
    /**
     * @var int
     */
    public $id_pegawai;
    public $busines_unit;
    public $golongan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pegawai', 'tanggal_bayar', 'keterangan', 'proses_gaji_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['jenis', 'jumlah', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'busines_unit', 'golongan',], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PayrollDiluarProses::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        $query->andFilterWhere([
            'jenis' => $this->jenis,
            'jumlah' => $this->jumlah,
            'tanggal_bayar' => $this->tanggal_bayar,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'created_by_struktur' => $this->created_by_struktur,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'proses_gaji_id', $this->proses_gaji_id])
            ->andFilterWhere(['like', 'pegawai_id', $this->pegawai_id])
            ->andFilterWhere(['like', 'perjanjian_kerja_id', $this->perjanjian_kerja_id])
            ->andFilterWhere(['like', 'pegawai_golongan_id', $this->pegawai_golongan_id])
            ->andFilterWhere(['like', 'pegawai_struktur_id', $this->pegawai_struktur_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

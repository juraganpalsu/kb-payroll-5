<?php

namespace frontend\modules\payroll\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\payroll\models\GajiBorongan;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\GajiBoronganSearch represents the model behind the search form about `frontend\modules\payroll\models\GajiBorongan`.
 */
class GajiBoronganSearch extends GajiBorongan
{
    /**
     * @var int
     */
    public $id_pegawai;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pegawai', 'tanggal', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'gaji_borongan_header_id'], 'safe'],
            [['id_pegawai', 'gaji', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, string $idp = '', $jenisBorongan = 0)
    {
        $query = GajiBorongan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($idp) {
            $query->andWhere(['gaji_borongan_header_id' => $idp]);
        }

        if ($jenisBorongan != 0) {
            $query->joinWith(['gajiBoronganHeader' => function (ActiveQuery $q) use ($jenisBorongan) {
                $q->andWhere(['gaji_borongan_header.jenis_borongan' => $jenisBorongan]);
            }]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('pegawai');

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }


        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'gaji_borongan.tanggal', $awal, $akhir]);
        }


        $query->andFilterWhere(['like', 'gaji_borongan.gaji', $this->gaji])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\payroll\models\search;

use frontend\models\User;
use frontend\modules\payroll\models\InsentifTidakTetap;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\InsentifTidakTetapSearch represents the model behind the search form about `frontend\modules\payroll\models\InsentifTidakTetap`.
 */
class InsentifTidakTetapSearch extends InsentifTidakTetap
{
    /**
     * @var int
     */
    public $id_pegawai;

    public $busines_unit;

    public $golongan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pegawai', 'keterangan', 'payroll_periode_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'created_by'], 'safe'],
            [['kategori', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'busines_unit', 'golongan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InsentifTidakTetap::find();

        $golongan = 0;
        if ($modelPegawai = User::pegawai()) {
            $pegawaiId = $modelPegawai->id;
            if ($pegawaiGolongan = $modelPegawai->pegawaiGolongan) {
                $golongan = $pegawaiGolongan->golongan->urutan;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if ($golongan) {
            $query->joinWith(['pegawaiGolongan' => function (ActiveQuery $query) use ($golongan) {
                $query->joinWith(['golongan' => function (ActiveQuery $query) use ($golongan) {
                    $query->andWhere(['<', 'golongan.urutan', $golongan]);
                }]);
            }]);
            if ($this->golongan) {
                $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
            }
        }

        $query->joinWith('pegawai');

        $query->andFilterWhere([
            'insentif_tidak_tetap.jumlah' => $this->jumlah,
            'kategori' => $this->kategori,
        ]);

        if (!empty($this->created_by)) {
            $query->joinWith(['createdBy' => function (ActiveQuery $query) {
                $query->andWhere(['like', 'user.username', $this->created_by]);
            }]);
        }

        $query->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['like', 'insentif_tidak_tetap.keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'insentif_tidak_tetap.payroll_periode_id', $this->payroll_periode_id]);

        $query->orderBy('created_at DESC');

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\payroll\models\search;

use frontend\modules\payroll\models\PayrollAsuransiLain;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\payroll\models\search\PayrollAsuransiLainSearch represents the model behind the search form about `frontend\modules\payroll\models\PayrollAsuransiLain`.
 */
class PayrollAsuransiLainSearch extends PayrollAsuransiLain
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'catatan', 'payroll_periode_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = PayrollAsuransiLain::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'golongan_id' => $this->golongan_id,
            'bisnis_unit' => $this->bisnis_unit,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'payroll_periode_id', $this->payroll_periode_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        $query->orderBy('created_at DESC');

        return $dataProvider;
    }
}

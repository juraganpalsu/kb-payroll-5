<?php

namespace frontend\modules\payroll\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\payroll\models\PayrollBpjsDetail;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\PayrollBpjsDetailSearch represents the model behind the search form about `frontend\modules\payroll\models\PayrollBpjsDetail`.
 */
 class PayrollBpjsDetailSearch extends PayrollBpjsDetail
{
     public $id_pegawai;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','id_pegawai',  'bpjs_id', 'pegawai_bpjs_t_id', 'pegawai_bpjs_tk_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['id_pegawai', 'bpjs_k_nominal', 'bpjs_tk_nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, string $idp = '')
    {
        $query = PayrollBpjsDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($idp) {
            $query->andWhere(['bpjs_id' => $idp]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('pegawai');

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        $query->andFilterWhere(['like', 'pegawai_bpjs_t_id', $this->pegawai_bpjs_t_id])
            ->andFilterWhere(['like', 'pegawai_bpjs_tk_id', $this->pegawai_bpjs_tk_id])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);

        return $dataProvider;
    }
}

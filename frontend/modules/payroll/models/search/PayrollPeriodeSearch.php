<?php

namespace frontend\modules\payroll\models\search;

use frontend\modules\payroll\models\PayrollPeriode;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\payroll\models\search\PayrollPeriodeSearch represents the model behind the search form about `frontend\modules\payroll\models\PayrollPeriode`.
 */
class PayrollPeriodeSearch extends PayrollPeriode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_awal', 'tanggal_akhir', 'catatan', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['tipe', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PayrollPeriode::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tipe' => $this->tipe,
        ]);


        if (!empty($this->tanggal_awal)) {
            $tanggal = explode('s/d', $this->tanggal_awal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal_awal', $awal, $akhir]);
        }

        if (!empty($this->tanggal_akhir)) {
            $tanggal = explode('s/d', $this->tanggal_akhir);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal_akhir', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'catatan', $this->catatan]);

        $query->orderBy('tipe ASC, tanggal_awal DESC');

        return $dataProvider;
    }
}

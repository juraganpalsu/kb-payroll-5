<?php

namespace frontend\modules\payroll\models\search;

use frontend\modules\payroll\models\JenisBorongan;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\payroll\models\search\JenisBoronganSearch represents the model behind the search form about `frontend\modules\payroll\models\JenisBorongan`.
 */
class JenisBoronganSearch extends JenisBorongan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_produk', 'catatan', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['harga'], 'number'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JenisBorongan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'harga' => $this->harga,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'nama_produk', $this->nama_produk])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        $query->orderBy('updated_at DESC');

        return $dataProvider;
    }
}

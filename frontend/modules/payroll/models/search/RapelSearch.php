<?php

namespace frontend\modules\payroll\models\search;

use frontend\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\payroll\models\Rapel;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\RapelSearch represents the model behind the search form about `frontend\modules\payroll\models\Rapel`.
 */
class RapelSearch extends Rapel
{
    /**
     * @var int
     */
    public $id_pegawai;

    public $busines_unit;

    public $golongan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pegawai', 'keterangan', 'periode_kurang', 'periode_bayar', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'busines_unit', 'golongan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rapel::find();

        $golongan = 0;
        if ($modelPegawai = User::pegawai()) {
            $pegawaiId = $modelPegawai->id;
            if ($pegawaiGolongan = $modelPegawai->pegawaiGolongan) {
                $golongan = $pegawaiGolongan->golongan->urutan;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('pegawai');

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if ($golongan) {
            $query->joinWith(['pegawaiGolongan' => function (ActiveQuery $query) use ($golongan) {
                $query->joinWith(['golongan' => function (ActiveQuery $query) use ($golongan) {
                    $query->andWhere(['<', 'golongan.urutan', $golongan]);
                }]);
            }]);
            if ($this->golongan) {
                $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
            }
        }

        $query->andFilterWhere([
            'rapel.jumlah' => $this->jumlah,
        ]);

        $query->joinWith('pegawai');

        $query->andFilterWhere(['like', 'rapel.keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['rapel.periode_kurang' => $this->periode_kurang])
            ->andFilterWhere(['rapel.periode_bayar' => $this->periode_bayar])
            ->orderBy('created_at DESC');

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\payroll\models\search;

use frontend\modules\payroll\models\ProsesGaji;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\payroll\models\search\ProsesGajiSearch represents the model behind the search form about `frontend\modules\payroll\models\ProsesGaji`.
 */
class ProsesGajiSearch extends ProsesGaji
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'payroll_periode_id', 'catatan', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @param array $filterBisnisUnit
     * @param array $filterGolongan
     * @param int $statusFinish
     * @return ActiveDataProvider
     */
    public function search(array $params, array $filterBisnisUnit = [], array $filterGolongan = [], int $statusFinish = ProsesGaji::dibuat): ActiveDataProvider
    {
        $query = ProsesGaji::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($filterBisnisUnit)) {
            $query->andWhere(['IN', 'bisnis_unit', $filterBisnisUnit]);
        }

        if (!empty($filterGolongan)) {
            $query->andWhere(['IN', 'golongan_id', $filterGolongan]);
        }

        if (!empty($statusFinish)) {
            $query->andWhere(['finishing_status' => $statusFinish]);
        }

        $query->andFilterWhere([
            'golongan_id' => $this->golongan_id,
            'bisnis_unit' => $this->bisnis_unit,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'payroll_periode_id', $this->payroll_periode_id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk])
            ->orderBy('created_at DESC');

        return $dataProvider;
    }
}

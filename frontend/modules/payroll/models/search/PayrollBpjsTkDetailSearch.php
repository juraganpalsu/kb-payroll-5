<?php


namespace frontend\modules\payroll\models\search;


use frontend\modules\payroll\models\PayrollBpjsTkDetail;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Created by PhpStorm.
 * File Name: PayrollBpjsTkDetailSearch.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-09-05
 * Time: 8:42 AM
 */

/**
 * @property int $id_pegawai
 */
class PayrollBpjsTkDetailSearch extends PayrollBpjsTkDetail
{
    public $id_pegawai;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'id_pegawai'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @param string $idp
     * @return ActiveDataProvider
     */
    public function search(array $params, string $idp)
    {
        $query = PayrollBpjsTkDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($idp) {
            $query->andWhere(['payroll_bpjs_id' => $idp]);
        }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('pegawai');

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        $query->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);

        return $dataProvider;
    }
}
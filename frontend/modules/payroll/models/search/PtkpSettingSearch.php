<?php

namespace frontend\modules\payroll\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\payroll\models\PtkpSetting;

/**
 * frontend\modules\payroll\models\search\PtkpSettingSearch represents the model behind the search form about `frontend\modules\payroll\models\PtkpSetting`.
 */
class PtkpSettingSearch extends PtkpSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'catatan', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['tahun', 'tk', 'k0', 'k1', 'k2', 'k3', 'persen_biaya_jabatan', 'maks_nominal_jabatan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PtkpSetting::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tahun' => $this->tahun,
            'tk' => $this->tk,
            'k0' => $this->k0,
            'k1' => $this->k1,
            'k2' => $this->k2,
            'k3' => $this->k3,
            'persen_biaya_jabatan' => $this->persen_biaya_jabatan,
            'maks_nominal_jabatan' => $this->maks_nominal_jabatan,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'created_by_struktur' => $this->created_by_struktur,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

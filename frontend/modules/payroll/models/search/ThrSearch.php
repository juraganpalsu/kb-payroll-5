<?php

namespace frontend\modules\payroll\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\payroll\models\Thr;

/**
 * frontend\modules\payroll\models\search\ThrSearch represents the model behind the search form about `frontend\modules\payroll\models\Thr`.
 */
class ThrSearch extends Thr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tahun', 'catatan', 'thr_setting_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['tahun', 'golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, $filterBisnisUnit = [], array $filterGolongan = [])
    {
        $query = Thr::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($filterBisnisUnit)) {
            $query->andWhere(['IN', 'bisnis_unit', $filterBisnisUnit]);
        }

        if (!empty($filterGolongan)) {
            $query->andWhere(['IN', 'golongan_id', $filterGolongan]);
        }

        $query->andFilterWhere([
            'golongan_id' => $this->golongan_id,
            'bisnis_unit' => $this->bisnis_unit,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'thr_setting_id', $this->thr_setting_id])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk])
            ->orderBy('created_at DESC');;

        return $dataProvider;
    }
}

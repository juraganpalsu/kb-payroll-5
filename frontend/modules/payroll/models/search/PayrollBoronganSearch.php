<?php

namespace frontend\modules\payroll\models\search;

use common\components\Status;
use frontend\modules\payroll\models\PayrollBorongan;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\payroll\models\search\PayrollBoronganSearch represents the model behind the search form about `frontend\modules\payroll\models\PayrollBorongan`.
 */
class PayrollBoronganSearch extends PayrollBorongan
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'tanggal', 'mulai', 'upah_per_orang', 'selesai', 'catatan', 'jenis_borongan_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['shift', 'hasil', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = PayrollBorongan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'shift' => $this->shift,
            'mulai' => $this->mulai,
            'selesai' => $this->selesai,
            'hasil' => $this->hasil,
            'upah_per_orang' => $this->upah_per_orang,
        ]);

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'jenis_borongan_id', $this->jenis_borongan_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }


    /**
     *
     * Query data approval
     *
     * @return ActiveQuery
     */
    public function queryApproval()
    {
        $query = PayrollBorongan::find();
        $query->andWhere(['payroll_borongan.last_status' => Status::SUBMITED]);
        $query->joinWith('approvalsByUser');
        $query->orderBy('payroll_borongan.created_at DESC');
        return $query;
    }

    /**
     *
     * Data Provider Approval
     *
     * @return ActiveDataProvider
     */
    public function searchApproval()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->queryApproval(),
        ]);
        return $dataProvider;
    }

    /**
     *
     * Menghitung jumlah approval dengan status submited
     *
     * @return bool|int|string|null
     */
    public function countApproval()
    {
        return $this->queryApproval()->count();
    }
}

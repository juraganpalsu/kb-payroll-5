<?php

namespace frontend\modules\payroll\models;

use Yii;
use \frontend\modules\payroll\models\base\PtkpSetting as BasePtkpSetting;

/**
 * This is the model class for table "ptkp_setting".
 */
class PtkpSetting extends BasePtkpSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
            [
                [['tahun', 'tk', 'k0', 'k1', 'k2', 'k3', 'persen_biaya_jabatan', 'maks_nominal_jabatan'], 'required'],
                [['tahun', 'tk', 'k0', 'k1', 'k2', 'k3', 'persen_biaya_jabatan', 'maks_nominal_jabatan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
                [['catatan'], 'string'],
                [['created_at', 'updated_at', 'deleted_at'], 'safe'],
                [['id', 'created_by_pk'], 'string', 'max' => 32],
                [['lock'], 'default', 'value' => '0'],
                [['lock'], 'mootensai\components\OptimisticLockValidator']
            ];
    }

}

<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\GajiBoronganHeader as BaseGajiBoronganHeader;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "gaji_borongan_header".
 *
 * @property array $_jenis_borongan
 * @property array $_status
 */
class GajiBoronganHeader extends BaseGajiBoronganHeader
{
    const NOODLE = 1;
    const KOBE = 2;
    const QUAKER = 3;
    const BK = 4;

    public $_jenis_borongan = [self::NOODLE => 'NOODLE', self::KOBE => 'KOBE', self::QUAKER => 'QUACKER', self::BK => 'BK'];

    const dibuat = 1;
    const diproses = 2;
    const selesai = 3;

    public $_status = [self::dibuat => 'DIBUAT', self::diproses => 'DIPROSSES', self::selesai => 'SELESAI'];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payroll_periode_id', 'jenis_borongan'], 'required'],
            [['jenis_borongan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['jumlah_gaji'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['payroll_periode_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['finishing_status'], 'default', 'value' => '1'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return bool|int|string|null
     */
    public function hitungJumlahPegawai()
    {
        return GajiBorongan::find()->andWhere(['gaji_borongan_header_id' => $this->id])->select('pegawai_id')->distinct()->count();
    }

    /**
     * Aksi2 yg dapat dilakukan setelah finish
     */
    public function afterFinish()
    {
        $getPegawaiIds = GajiBorongan::find()->select('pegawai_id')->andWhere(['gaji_borongan_header_id' => $this->id])->distinct()->column();
        foreach ($getPegawaiIds as $pegawaiId) {
            $this->setDIbayarPadaPinjaman($pegawaiId);
        }
    }

    /**
     * @param string $pegawaiId
     */
    public function setDIbayarPadaPinjaman(string $pegawaiId)
    {
        $query = PinjamanAngsuran::find()->andWhere(['pinjaman_angsuran.payroll_periode_id' => $this->payroll_periode_id])
            ->joinWith(['pinjaman' => function (ActiveQuery $query) use ($pegawaiId) {
                $query->andWhere(['pinjaman.pegawai_id' => $pegawaiId]);
            }])->all();
        if (!empty($query)) {
            foreach ($query as $pinjamanAngsuran) {
                if ($model = PinjamanAngsuran::findOne($pinjamanAngsuran['id'])) {
                    $model->status = PinjamanAngsuran::DIBAYAR;
                    $model->save();
                }
            }
        }
    }

}

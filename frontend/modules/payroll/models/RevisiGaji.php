<?php

namespace frontend\modules\payroll\models;

use Yii;
use \frontend\modules\payroll\models\base\RevisiGaji as BaseRevisiGaji;

/**
 * This is the model class for table "revisi_gaji".
 */
class RevisiGaji extends BaseRevisiGaji
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proses_gaji_id'], 'required'],
            [['revisi_ke', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['proses_gaji_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        $query = RevisiGaji::find()->andWhere(['proses_gaji_id' => $this->proses_gaji_id])->limit(1)->orderBy('revisi_ke DESC')->one();
        if ($query) {
            $this->revisi_ke = $query['revisi_ke'] + 1;
        } else {
            $this->revisi_ke = 1;
        }
        return true;
    }

}

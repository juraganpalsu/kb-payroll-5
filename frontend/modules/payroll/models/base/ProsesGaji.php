<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\models\Golongan;
use frontend\models\User;
use frontend\modules\struktur\models\Struktur;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "proses_gaji".
 *
 * @property string $id
 * @property string $payroll_periode_id
 * @property integer $golongan_id
 * @property integer $bisnis_unit
 * @property integer $status
 * @property integer $finishing_status
 * @property string $catatan
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 * @property \frontend\modules\payroll\models\GajiBulanan[] $gajiBulanans
 * @property Golongan $golongan
 * @property \frontend\modules\payroll\models\PayrollPeriode $payrollPeriode
 * @property User $createdBy
 * @property Struktur $createdByStruktur
 */
class ProsesGaji extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payroll_periode_id', 'golongan_id', 'bisnis_unit'], 'required'],
            [['golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'status'], 'integer'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'payroll_periode_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'status','finishing_status'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proses_gaji';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'payroll_periode_id' => Yii::t('frontend', 'Payroll Periode ID'),
            'golongan_id' => Yii::t('frontend', 'Golongan ID'),
            'bisnis_unit' => Yii::t('frontend', 'Bisnis Unit'),
            'catatan' => Yii::t('frontend', 'Catatan'),
            'status' => Yii::t('frontend', 'Status'),
            'finishing_status' => Yii::t('frontend', 'Finishing Status'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedByStruktur()
    {
        return $this->hasOne(Struktur::class, ['id' => 'created_by_struktur']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGajiBulanans()
    {
        return $this->hasMany(\frontend\modules\payroll\models\GajiBulanan::class, ['proses_gaji_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGolongan()
    {
        return $this->hasOne(Golongan::class, ['id' => 'golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayrollPeriode()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PayrollPeriode::class, ['id' => 'payroll_periode_id']);
    }


    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     *
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}
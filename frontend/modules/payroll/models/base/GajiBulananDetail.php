<?php

namespace frontend\modules\payroll\models\base;

use common\models\PegawaiSistemKerja;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "gaji_bulanan_detail".
 *
 * @property string $id
 * @property string $masuk
 * @property string $istirahat
 * @property string $masuk_istirahat
 * @property string $pulang
 * @property string $tanggal
 * @property string $gaji_bulanan_id
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property integer $pegawai_sistem_kerja_id
 * @property integer $time_table_id
 * @property integer $status_kehadiran
 * @property string $spl_id
 * @property integer $spl_template_id
 * @property string $jumlah_jam_lembur
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\payroll\models\GajiBulanan $gajiBulanan
 * @property Pegawai $pegawai
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiSistemKerja $pegawaiSistemKerja
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 */
class GajiBulananDetail extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal', 'gaji_bulanan_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'pegawai_sistem_kerja_id'], 'required'],
            [['masuk', 'istirahat', 'masuk_istirahat', 'pulang', 'tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['pegawai_sistem_kerja_id', 'time_table_id', 'status_kehadiran', 'spl_template_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'gaji_bulanan_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'spl_id'], 'string', 'max' => 32],
            [['jumlah_jam_lembur'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gaji_bulanan_detail';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'masuk' => Yii::t('frontend', 'Masuk'),
            'istirahat' => Yii::t('frontend', 'Istirahat'),
            'masuk_istirahat' => Yii::t('frontend', 'Masuk Istirahat'),
            'pulang' => Yii::t('frontend', 'Pulang'),
            'tanggal' => Yii::t('frontend', 'Tanggal'),
            'gaji_bulanan_id' => Yii::t('frontend', 'Gaji Bulanan ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'pegawai_sistem_kerja_id' => Yii::t('frontend', 'Pegawai Sistem Kerja ID'),
            'time_table_id' => Yii::t('frontend', 'Time Table ID'),
            'status_kehadiran' => Yii::t('frontend', 'Status Kehadiran'),
            'spl_id' => Yii::t('frontend', 'Spl ID'),
            'spl_template_id' => Yii::t('frontend', 'Spl Template ID'),
            'jumlah_jam_lembur' => Yii::t('frontend', 'Jumlah Jam Lembur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }
    
    /**
     * @return ActiveQuery
     */
    public function getGajiBulanan()
    {
        return $this->hasOne(\frontend\modules\payroll\models\GajiBulanan::class, ['id' => 'gaji_bulanan_id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPegawaiSistemKerja()
    {
        return $this->hasOne(PegawaiSistemKerja::class, ['id' => 'pegawai_sistem_kerja_id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

}

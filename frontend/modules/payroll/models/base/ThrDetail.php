<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\components\DefaultPKBehavior;
use frontend\models\User;
use frontend\modules\masterdata\models\PGajiPokok;
use frontend\modules\masterdata\models\PTunjaganLainTetap;
use frontend\modules\masterdata\models\PTunjanganAnakConfig;
use frontend\modules\masterdata\models\PTunjanganJabatan;
use frontend\modules\masterdata\models\PTunjanganLoyalitasConfig;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "thr_detail".
 *
 * @property string $id
 * @property integer $nominal
 * @property string $catatan
 * @property string $thr_id
 * @property string $cut_off
 * @property double $presentase_thr
 * @property integer $masa_kerja_bulan
 * @property integer $masa_kerja_tahun
 * @property string $tanggal_mulai
 * @property integer $gaji_pokok_thr
 * @property string $golongan
 * @property string $jenis_perjanjian
 * @property string $bisnis_unit
 * @property string $nama
 * @property string $id_pegawai
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $p_gaji_pokok_id
 * @property integer $p_gaji_pokok_nominal
 * @property string $p_tunjangan_loyalitas_config_id
 * @property integer $p_tunjangan_loyalitas_config_nominal
 * @property string $p_tunjangan_anak_config_id
 * @property integer $p_tunjangan_anak_config_nominal
 * @property string $p_tunjangan_lain_tetap_id
 * @property integer $p_tunjangan_lain_tetap_nominal
 * @property string $p_tunjangan_jabatan_id
 * @property integer $p_tunjangan_jabatan_nominal
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property PGajiPokok $pGajiPokok
 * @property PTunjaganLainTetap $pTunjanganLainTetap
 * @property PTunjanganAnakConfig $pTunjanganAnakConfig
 * @property PTunjanganJabatan $pTunjanganJabatan
 * @property PTunjanganLoyalitasConfig $pTunjanganLoyalitasConfig
 * @property Pegawai $pegawai
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 * @property \frontend\modules\payroll\models\Thr $thr
 */
class ThrDetail extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'thr_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'p_gaji_pokok_id', 'p_tunjangan_loyalitas_config_id', 'p_tunjangan_anak_config_id', 'p_tunjangan_lain_tetap_id', 'p_tunjangan_jabatan_id'], 'required'],
            [['p_gaji_pokok_nominal', 'p_tunjangan_loyalitas_config_nominal', 'p_tunjangan_anak_config_nominal', 'p_tunjangan_lain_tetap_nominal', 'p_tunjangan_jabatan_nominal', 'nominal', 'masa_kerja_bulan', 'masa_kerja_tahun', 'gaji_pokok_thr', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['presentase_thr'], 'number'],
            [['cut_off', 'tanggal_mulai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'thr_id', 'pegawai_id', 'golongan', 'jenis_perjanjian', 'bisnis_unit', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'p_gaji_pokok_id', 'p_tunjangan_loyalitas_config_id', 'p_tunjangan_anak_config_id', 'p_tunjangan_lain_tetap_id', 'p_tunjangan_jabatan_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['id_pegawai'], 'string', 'max' => 20],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thr_detail';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'nominal' => Yii::t('frontend', 'Nominal THR'),
            'catatan' => Yii::t('frontend', 'Catatan'),
            'thr_id' => Yii::t('frontend', 'Thr ID'),
            'cut_off' => Yii::t('frontend', 'Cut Off'),
            'presentase_thr' => Yii::t('frontend', 'SKEMA'),
            'masa_kerja_bulan' => Yii::t('frontend', 'Masa Kerja Bulan'),
            'masa_kerja_tahun' => Yii::t('frontend', 'Masa Kerja Tahun'),
            'tanggal_mulai' => Yii::t('frontend', 'Tanggal Masa Kerja'),
            'gaji_pokok_thr' => Yii::t('frontend', 'Jumlah Upah THR'),
            'golongan' => Yii::t('frontend', 'Golongan'),
            'jenis_perjanjian' => Yii::t('frontend', 'Jenis Perjanjian'),
            'bisnis_unit' => Yii::t('frontend', 'Bisnis Unit'),
            'nama' => Yii::t('frontend', 'Nama Lengkap'),
            'id_pegawai' => Yii::t('frontend', 'Id Pegawai'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'p_gaji_pokok_id' => Yii::t('frontend', 'Gaji Pokok ID'),
            'p_gaji_pokok_nominal' => Yii::t('frontend', 'Gaji Pokok'),
            'p_tunjangan_loyalitas_config_nominal' => Yii::t('frontend', 'Tunjangan Loyalitas'),
            'p_tunjangan_anak_config_nominal' => Yii::t('frontend', 'Tunjangan Anak'),
            'p_tunjangan_lain_tetap_nominal' => Yii::t('frontend', 'Tunjangan Lain Tetap'),
            'p_tunjangan_jabatan_nominal' => Yii::t('frontend', 'Tunjangan Jabatan'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPGajiPokok()
    {
        return $this->hasOne(PGajiPokok::class, ['id' => 'p_gaji_pokok_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganLainTetap()
    {
        return $this->hasOne(PTunjaganLainTetap::class, ['id' => 'p_tunjangan_lain_tetap_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganAnakConfig()
    {
        return $this->hasOne(PTunjanganAnakConfig::class, ['id' => 'p_tunjangan_anak_config_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganJabatan()
    {
        return $this->hasOne(PTunjanganJabatan::class, ['id' => 'p_tunjangan_jabatan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganLoyalitasConfig()
    {
        return $this->hasOne(PTunjanganLoyalitasConfig::class, ['id' => 'p_tunjangan_loyalitas_config_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getThr()
    {
        return $this->hasOne(\frontend\modules\payroll\models\Thr::class, ['id' => 'thr_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
            'defaultPK' => [
                'class' => DefaultPKBehavior::class,
                'values' => [
                    'perjanjian_kerja_id' => function () {
                        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->thr->thrSetting->cut_off_resign)) {
                            return $perjanjianKerja->id;
                        }
                        return false;
                    },
                    'pegawai_golongan_id' => function () {
                        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->thr->thrSetting->cut_off_resign)) {
                            return $golongan->id;
                        }
                        return false;
                    },
                    'pegawai_struktur_id' => function () {
                        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->thr->thrSetting->cut_off_resign)) {
                            return $struktur->id;
                        }
                        return false;
                    },
                ],
            ],
        ];
    }
}
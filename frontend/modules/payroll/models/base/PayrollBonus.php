<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\models\Golongan;
use frontend\models\User;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "payroll_bonus".
 *
 * @property string $id
 * @property integer $tahun
 * @property string $catatan
 * @property string $payroll_periode_id
 * @property integer $golongan_id
 * @property integer $bisnis_unit
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Golongan $golongan
 * @property \frontend\modules\payroll\models\PayrollPeriode $payrollPeriode
 * @property \frontend\modules\payroll\models\PayrollBonusDetail[] $payrollBonusDetails
 */
class PayrollBonus extends ActiveRecord
{

     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payroll_periode_id', 'tahun', 'golongan_id', 'bisnis_unit'], 'required'],
            [['tahun', 'golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'payroll_periode_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_bonus';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tahun' => Yii::t('frontend', 'Tahun'),
            'catatan' => Yii::t('frontend', 'Catatan'),
            'payroll_periode_id' => Yii::t('frontend', 'Acuan Periode Payroll'),
            'golongan_id' => Yii::t('frontend', 'Golongan ID'),
            'bisnis_unit' => Yii::t('frontend', 'Bisnis Unit'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGolongan()
    {
        return $this->hasOne(Golongan::class, ['id' => 'golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayrollPeriode()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PayrollPeriode::class, ['id' => 'payroll_periode_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayrollBonusDetails()
    {
        return $this->hasMany(\frontend\modules\payroll\models\PayrollBonusDetail::className(), ['payroll_bonus_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

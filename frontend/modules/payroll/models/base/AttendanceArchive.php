<?php

namespace frontend\modules\payroll\models\base;

use frontend\modules\pegawai\models\Pegawai;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "attendance_archive".
 *
 * @property integer $id
 * @property string $pegawai_id
 * @property string $check_time
 * @property integer $idfp
 * @property integer $uploaded
 * @property string $device_id
 * @property integer $attendance_beatroute_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 * @property string $gaji_bulanan_id
 *
 * @property \frontend\modules\payroll\models\GajiBulanan $gajiBulanan
 * @property Pegawai $pegawai
 */
class AttendanceArchive extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'check_time', 'idfp', 'gaji_bulanan_id'], 'required'],
            [['id', 'idfp', 'attendance_beatroute_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['check_time', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['pegawai_id', 'gaji_bulanan_id'], 'string', 'max' => 32],
            [['uploaded'], 'string', 'max' => 4],
            [['device_id'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attendance_archive';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'check_time' => Yii::t('frontend', 'Check Time'),
            'idfp' => Yii::t('frontend', 'Idfp'),
            'uploaded' => Yii::t('frontend', 'Uploaded'),
            'device_id' => Yii::t('frontend', 'Device ID'),
            'attendance_beatroute_id' => Yii::t('frontend', 'Attendance Beatroute ID'),
            'lock' => Yii::t('frontend', 'Lock'),
            'gaji_bulanan_id' => Yii::t('frontend', 'Gaji Bulanan ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getGajiBulanan()
    {
        return $this->hasOne(\frontend\modules\payroll\models\GajiBulanan::class, ['id' => 'gaji_bulanan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

}

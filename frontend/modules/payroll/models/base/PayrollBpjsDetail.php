<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\components\DefaultPKBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "payroll_bpjs_detail".
 *
 * @property string $id
 * @property integer $bpjs_k_nominal
 * @property integer $bpjs_tk_nominal
 * @property string $bpjs_id
 * @property string $pegawai_bpjs_t_id
 * @property string $pegawai_bpjs_tk_id
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 * @property string $gaji_pokok
 * @property number $iuran_persen
 * @property number $iuran_nominal
 * @property number $iuran_persen_tk
 * @property number $iuran_nominal_tk
 * @property number $total_iuran
 *
 * @property \frontend\modules\payroll\models\PayrollBpjs $bpjs
 * @property Pegawai $pegawai
 * @property PegawaiBpjs $pegawaiBpjsT
 * @property PegawaiBpjs $pegawaiBpjsTk
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 */
class PayrollBpjsDetail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bpjs_id', 'pegawai_bpjs_t_id', 'pegawai_bpjs_tk_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'required'],
            [['bpjs_k_nominal', 'bpjs_tk_nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'bpjs_id', 'pegawai_bpjs_t_id', 'pegawai_bpjs_tk_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_bpjs_detail';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'bpjs_k_nominal' => Yii::t('frontend', 'Bpjs K Nominal'),
            'bpjs_tk_nominal' => Yii::t('frontend', 'Bpjs Tk Nominal'),
            'bpjs_id' => Yii::t('frontend', 'Bpjs ID'),
            'pegawai_bpjs_t_id' => Yii::t('frontend', 'Pegawai Bpjs T ID'),
            'pegawai_bpjs_tk_id' => Yii::t('frontend', 'Pegawai Bpjs Tk ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
            'gaji_pokok' => Yii::t('frontend', 'Gaji Pokok'),
            'iuran_persen' => Yii::t('frontend', 'Persentase Iuran'),
            'iuran_nominal' => Yii::t('frontend', 'Nominal Iuran'),
            'iuran_persen_tk' => Yii::t('frontend', 'Persentase Iuran TK'),
            'iuran_nominal_tk' => Yii::t('frontend', 'Nominal Iuran TK'),
            'total_iuran' => Yii::t('frontend', 'Total Iuran'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getBpjs()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PayrollBpjs::class, ['id' => 'bpjs_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiBpjsT()
    {
        return $this->hasOne(PegawaiBpjs::class, ['id' => 'pegawai_bpjs_t_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiBpjsTk()
    {
        return $this->hasOne(PegawaiBpjs::class, ['id' => 'pegawai_bpjs_tk_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
            'defaultPK' => [
                'class' => DefaultPKBehavior::class,
                'values' => [
                    'perjanjian_kerja_id' => function () {
                        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->bpjs->payrollPeriode->tanggal_awal)) {
                            return $perjanjianKerja->id;
                        }
                        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->bpjs->payrollPeriode->tanggal_akhir)) {
                            return $perjanjianKerja->id;
                        }
                        return false;
                    },
                    'pegawai_golongan_id' => function () {
                        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->bpjs->payrollPeriode->tanggal_awal)) {
                            return $golongan->id;
                        }
                        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->bpjs->payrollPeriode->tanggal_akhir)) {
                            return $golongan->id;
                        }
                        return false;
                    },
                    'pegawai_struktur_id' => function () {
                        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->bpjs->payrollPeriode->tanggal_awal)) {
                            return $struktur->id;
                        }
                        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->bpjs->payrollPeriode->tanggal_akhir)) {
                            return $struktur->id;
                        }
                        return false;
                    },
                ],
            ],
        ];
    }
}
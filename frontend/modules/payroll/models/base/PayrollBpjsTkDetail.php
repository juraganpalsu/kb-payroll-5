<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\components\DefaultPKBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "payroll_bpjs_tk_detail".
 *
 * @property string $id
 * @property number $gaji_pokok
 * @property number $jkk_persen
 * @property number $jkk_nominal
 * @property number $jkn_persen
 * @property number $jkn_nominal
 * @property number $jht_persen
 * @property number $jht_nominal
 * @property number $jht_persen_tk
 * @property number $jht_nominal_tk
 * @property number $iuran_pensiun_persen
 * @property number $iuran_pensiun_nominal
 * @property number $iuran_pensiun_tk_persen
 * @property number $iuran_pensiun_tk_nominal
 * @property number $total_iuran
 * @property string $keterangan
 * @property string $pegawai_bpjs_id
 * @property string $payroll_bpjs_id
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\payroll\models\PayrollBpjs $payrollBpjs
 * @property Pegawai $pegawai
 * @property PegawaiBpjs $pegawaiBpjs
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 */
class PayrollBpjsTkDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pegawai_bpjs_id', 'payroll_bpjs_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'required'],
            [['gaji_pokok', 'jkk_persen', 'jkk_nominal', 'jkn_persen', 'jkn_nominal', 'jht_persen', 'jht_nominal', 'jht_persen_tk', 'jht_nominal_tk', 'iuran_pensiun_persen', 'iuran_pensiun_nominal', 'iuran_pensiun_tk_persen', 'iuran_pensiun_tk_nominal', 'total_iuran'], 'number'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 36],
            [['pegawai_bpjs_id', 'payroll_bpjs_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_bpjs_tk_detail';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'gaji_pokok' => Yii::t('frontend', 'Gaji Pokok'),
            'jkk_persen' => Yii::t('frontend', 'Jkk Persen'),
            'jkk_nominal' => Yii::t('frontend', 'Jkk Nominal'),
            'jkn_persen' => Yii::t('frontend', 'Jkn Persen'),
            'jkn_nominal' => Yii::t('frontend', 'Jkn Nominal'),
            'jht_persen' => Yii::t('frontend', 'Jht Persen'),
            'jht_nominal' => Yii::t('frontend', 'Jht Nominal'),
            'jht_persen_tk' => Yii::t('frontend', 'Jht Persen Tk'),
            'jht_nominal_tk' => Yii::t('frontend', 'Jht Nominal Tk'),
            'iuran_pensiun_persen' => Yii::t('frontend', 'Iuran Pensiun Persen'),
            'iuran_pensiun_nominal' => Yii::t('frontend', 'Iuran Pensiun Nominal'),
            'iuran_pensiun_tk_persen' => Yii::t('frontend', 'Iuran Pensiun Tk Persen'),
            'iuran_pensiun_tk_nominal' => Yii::t('frontend', 'Iuran Pensiun Tk Nominal'),
            'total_iuran' => Yii::t('frontend', 'Total Iuran'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'pegawai_bpjs_id' => Yii::t('frontend', 'Pegawai Bpjs ID'),
            'payroll_bpjs_id' => Yii::t('frontend', 'Payroll Bpjs ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPayrollBpjs()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PayrollBpjs::class, ['id' => 'payroll_bpjs_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiBpjs()
    {
        return $this->hasOne(PegawaiBpjs::class, ['id' => 'pegawai_bpjs_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete(): bool
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => \common\components\UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
            'defaultPK' => [
                'class' => DefaultPKBehavior::class,
                'values' => [
                    'perjanjian_kerja_id' => function () {
                        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->payrollBpjs->payrollPeriode->tanggal_awal)) {
                            return $perjanjianKerja->id;
                        }
                        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->payrollBpjs->payrollPeriode->tanggal_akhir)) {
                            return $perjanjianKerja->id;
                        }
                        return false;
                    },
                    'pegawai_golongan_id' => function () {
                        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->payrollBpjs->payrollPeriode->tanggal_awal)) {
                            return $golongan->id;
                        }
                        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->payrollBpjs->payrollPeriode->tanggal_akhir)) {
                            return $golongan->id;
                        }
                        return false;
                    },
                    'pegawai_struktur_id' => function () {
                        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->payrollBpjs->payrollPeriode->tanggal_awal)) {
                            return $struktur->id;
                        }
                        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->payrollBpjs->payrollPeriode->tanggal_akhir)) {
                            return $struktur->id;
                        }
                        return false;
                    },
                ],
            ],
        ];
    }
}

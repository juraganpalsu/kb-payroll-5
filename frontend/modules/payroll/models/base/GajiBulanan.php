<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\models\PegawaiSistemKerja;
use frontend\models\User;
use frontend\modules\izin\models\AbsensiTidakLengkap;
use frontend\modules\izin\models\IzinTanpaHadir;
use frontend\modules\masterdata\models\PAdminBank;
use frontend\modules\masterdata\models\PGajiPokok;
use frontend\modules\masterdata\models\PInsentifTetap;
use frontend\modules\masterdata\models\PPremiHadir;
use frontend\modules\masterdata\models\PSewaMobil;
use frontend\modules\masterdata\models\PSewaMotor;
use frontend\modules\masterdata\models\PSewaRumah;
use frontend\modules\masterdata\models\PTunjaganLainTetap;
use frontend\modules\masterdata\models\PTunjanganAnakConfig;
use frontend\modules\masterdata\models\PTunjanganJabatan;
use frontend\modules\masterdata\models\PTunjanganKost;
use frontend\modules\masterdata\models\PTunjanganLoyalitasConfig;
use frontend\modules\masterdata\models\PTunjanganPulsa;
use frontend\modules\masterdata\models\PUangMakan;
use frontend\modules\masterdata\models\PUangTransport;
use frontend\modules\pegawai\models\CostCenter;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiBank;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "gaji_bulanan".
 *
 * @property string $id
 * @property integer $id_pegawai
 * @property integer $tipe_gaji_pokok
 * @property integer $pot_keterlambatan
 * @property integer $cabang
 * @property integer $prorate_komponen
 * @property integer $beatroute
 * @property string $nama_lengkap
 * @property string $bisnis_unit_s
 * @property string $golongan_s
 * @property string $jenis_kontrak_s
 * @property string $struktur_s
 * @property double $masuk
 * @property integer $alfa
 * @property integer $ijin
 * @property integer $libur
 * @property integer $cuti
 * @property integer $undifined
 * @property integer $dispensasi
 * @property integer $tl
 * @property integer $is
 * @property integer $cm
 * @property integer $off
 * @property integer $idl
 * @property integer $ith
 * @property integer $sd
 * @property integer $wfh
 * @property integer $atl
 * @property integer $sh
 * @property double $jam_lembur
 * @property integer $s2
 * @property integer $s2_nominal
 * @property integer $s3
 * @property integer $s3_nominal
 * @property double $uang_lembur
 * @property double $insentif_lembur
 * @property double $uml
 * @property integer $pengali_uml1
 * @property integer $pengali_uml2
 * @property string $p_gaji_pokok_id
 * @property integer $p_gaji_pokok
 * @property string $p_admin_bank_id
 * @property integer $p_admin_bank
 * @property string $pinjaman_angsuran_id
 * @property integer $pinjaman_nominal
 * @property integer $angsuran_ke
 * @property string $pinjaman_bank_id
 * @property integer $pinjaman_bank_nominal
 * @property integer $angsuran_bank_ke
 * @property string $pinjaman_kasbon_id
 * @property integer $pinjaman_kasbon_nominal
 * @property integer $angsuran_kasbon_ke
 * @property string $pinjaman_koperasi_id
 * @property integer $pinjaman_koperasi_nominal
 * @property integer $angsuran_koperasi_ke
 * @property string $pinjaman_potongan_id
 * @property integer $pinjaman_potongan_nominal
 * @property integer $angsuran_potongan_ke
 * @property string $pinjaman_lain_lain_id
 * @property integer $pinjaman_lain_lain_nominal
 * @property integer $angsuran_lain_lain_ke
 * @property string $pinjaman_potongan_atl_id
 * @property double $pinjaman_potongan_atl_nominal
 * @property integer $angsuran_potongan_atl_ke
 * @property string $p_insentif_tetap_id
 * @property string $p_premi_hadir_id
 * @property string $p_sewa_mobil_id
 * @property string $p_sewa_motor_id
 * @property string $p_sewa_rumah_id
 * @property string $p_tunjagan_lain_tetap_id
 * @property string $p_tunjagan_lain_lain_id
 * @property string $p_tunjangan_anak_config_id
 * @property string $p_tunjangan_jabatan_id
 * @property string $p_tunjangan_kost_id
 * @property string $p_tunjangan_loyalitas_config_id
 * @property string $p_tunjangan_pulsa_id
 * @property string $p_uang_makan_id
 * @property string $p_uang_transport_id
 * @property integer $p_insentif_tetap
 * @property integer $p_premi_hadir
 * @property integer $p_sewa_mobil
 * @property integer $p_sewa_motor
 * @property integer $p_sewa_rumah
 * @property integer $p_tunjagan_lain_tetap
 * @property integer $p_tunjagan_lain_lain
 * @property integer $p_tunjangan_anak
 * @property integer $p_tunjangan_jabatan
 * @property integer $p_tunjangan_kost
 * @property integer $p_tunjangan_loyalitas
 * @property integer $p_tunjangan_pulsa
 * @property integer $p_uang_makan
 * @property integer $p_uang_transport
 * @property integer $hari_kerja_aktif
 * @property string $pegawai_bpjs_id
 * @property integer $pegawai_bpjs_kes
 * @property integer $pegawai_bpjs_tk
 * @property string $pegawai_bank_id
 * @property string $pegawai_bank_nama
 * @property string $pegawai_bank_nomor
 * @property integer $jumlah_telat
 * @property integer $libur_hadir
 * @property integer $libur_hadir_s1
 * @property integer $libur_hadir_s2
 * @property integer $libur_hadir_s3
 * @property integer $atl_s1
 * @property integer $atl_s2
 * @property integer $atl_s3
 * @property integer $sh_s2
 * @property integer $sh_s3
 * @property integer $status_pernikahan
 * @property integer $jumlah_anak
 * @property string $proses_gaji_id
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $insentif_tidak_tetap_id
 * @property string $rapel_id
 * @property integer $pegawai_sistem_kerja_id
 * @property string $cost_center_id
 * @property integer $insentif_tidak_tetap
 * @property integer $insentif_pph
 * @property integer $insentif_gt
 * @property integer $rapel
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property PAdminBank $pAdminBank
 * @property PGajiPokok $pGajiPokok
 * @property PInsentifTetap $pInsentifTetap
 * @property PPremiHadir $pPremiHadir
 * @property PSewaMobil $pSewaMobil
 * @property PSewaMotor $pSewaMotor
 * @property PSewaRumah $pSewaRumah
 * @property PTunjaganLainTetap $pTunjaganLainTetap
 * @property PTunjanganAnakConfig $pTunjanganAnakConfig
 * @property PTunjanganJabatan $pTunjanganJabatan
 * @property PTunjanganKost $pTunjanganKost
 * @property PTunjanganLoyalitasConfig $pTunjanganLoyalitasConfig
 * @property PTunjanganPulsa $pTunjanganPulsa
 * @property PUangMakan $pUangMakan
 * @property PUangTransport $pUangTransport
 * @property Pegawai $pegawai
 * @property PegawaiBank $pegawaiBank
 * @property PegawaiBpjs $pegawaiBpjs
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 * @property PegawaiSistemKerja $pegawaiSistemKerja
 * @property CostCenter $costCenter
 * @property \frontend\modules\payroll\models\InsentifTidakTetap $insentifTidakTetap
 * @property \frontend\modules\payroll\models\Rapel $grapel
 * @property \frontend\modules\payroll\models\PinjamanAngsuran $pinjamanAngsuran
 * @property \frontend\modules\payroll\models\PinjamanAngsuran $pinjamanBank
 * @property \frontend\modules\payroll\models\PinjamanAngsuran $pinjamanKasbon
 * @property \frontend\modules\payroll\models\PinjamanAngsuran $pinjamanKoperasi
 * @property \frontend\modules\payroll\models\PinjamanAngsuran $pinjamanPotongan
 * @property \frontend\modules\payroll\models\PinjamanAngsuran $pinjamanLainLain
 * @property IzinTanpaHadir $wfhJumlah
 * @property IzinTanpaHadir $sdJumlah
 * @property IzinTanpaHadir $ithJumlah
 * @property IzinTanpaHadir $idlJumlah
 * @property IzinTanpaHadir $offJumlah
 * @property IzinTanpaHadir $cmJumlah
 * @property IzinTanpaHadir $isJumlah
 * @property IzinTanpaHadir $tlJumlah
 * @property IzinTanpaHadir $dispensasiJumlah
 * @property AbsensiTidakLengkap $absensiTidakLengakp
 * @property \frontend\modules\payroll\models\ProsesGaji $prosesGaji
 * @property \frontend\modules\payroll\models\GajiBulananDetail[] $gajiBulananDetails
 * @property \frontend\modules\payroll\models\PtkpSetting $ptkpSetting
 * @property \frontend\modules\payroll\models\GajiPph21 $gajiPhh21Child
 * @property \frontend\modules\payroll\models\GajiPph21 $gajiPhh21Parent
 */
class GajiBulanan extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pegawai', 'tipe_gaji_pokok', 'id', 'nama_lengkap', 'bisnis_unit_s', 'golongan_s', 'jenis_kontrak_s', 'struktur_s', 'pegawai_bank_id', 'proses_gaji_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'pegawai_sistem_kerja_id'], 'required'],
            [['id_pegawai', 'tipe_gaji_pokok', 'insentif_pph', 'insentif_gt', 'insentif_tidak_tetap', 'rapel', 'alfa', 'ijin', 'libur', 'cuti', 'undifined', 's2', 's3', 'p_gaji_pokok', 'p_admin_bank', 'pinjaman_nominal', 'angsuran_ke', 'p_insentif_tetap', 'p_premi_hadir', 'p_sewa_mobil', 'p_sewa_motor', 'p_sewa_rumah', 'p_tunjagan_lain_tetap', 'p_tunjagan_lain_lain', 'p_tunjangan_anak', 'p_tunjangan_jabatan', 'p_tunjangan_kost', 'p_tunjangan_loyalitas', 'p_tunjangan_pulsa', 'p_uang_makan', 'p_uang_transport', 'hari_kerja_aktif', 'pegawai_bpjs_kes', 'pegawai_bpjs_tk', 'jumlah_telat', 'pegawai_sistem_kerja_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'dispensasi', 'is', 'cm', 'off', 'idl', 'ith', 'sd', 'wfh', 'atl', 'tl', 'status_pernikahan', 'jumlah_anak', 'pengali_uml1', 'pengali_uml2', 'lock'], 'integer'],
            [['masuk', 'jam_lembur', 'uang_lembur', 'insentif_lembur', 'uml'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'p_gaji_pokok_id', 'p_admin_bank_id', 'pinjaman_angsuran_id', 'p_insentif_tetap_id', 'p_premi_hadir_id', 'p_sewa_mobil_id', 'p_sewa_motor_id', 'p_sewa_rumah_id', 'p_tunjagan_lain_tetap_id', 'p_tunjagan_lain_lain_id', 'p_tunjangan_anak_config_id', 'p_tunjangan_jabatan_id', 'p_tunjangan_kost_id', 'p_tunjangan_loyalitas_config_id', 'p_tunjangan_pulsa_id', 'p_uang_makan_id', 'p_uang_transport_id', 'pegawai_bpjs_id', 'pegawai_bank_id', 'proses_gaji_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk', 'cost_center_id'], 'string', 'max' => 32],
            [['nama_lengkap', 'bisnis_unit_s', 'golongan_s', 'jenis_kontrak_s', 'struktur_s', 'pegawai_bank_nama'], 'string', 'max' => 225],
            [['pinjaman_bank_id', 'pinjaman_kasbon_id', 'pinjaman_koperasi_id', 'pinjaman_potongan_id', 'pinjaman_lain_lain_id', 'pinjaman_potongan_atl_id'], 'string', 'max' => 32],
            [['pot_keterlambatan', 'cabang', 'prorate_komponen', 'beatroute', 'pinjaman_bank_nominal', 'pinjaman_kasbon_nominal', 'pinjaman_koperasi_nominal', 'pinjaman_potongan_nominal', 'pinjaman_lain_lain_nominal', 'pinjaman_potongan_atl_nominal'], 'integer'],
            [['angsuran_bank_ke', 'angsuran_kasbon_ke', 'angsuran_koperasi_ke', 'angsuran_potongan_ke', 'angsuran_lain_lain_ke', 'angsuran_potongan_atl_ke'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['pegawai_bank_nomor'], 'string', 'max' => 45],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gaji_bulanan';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'id_pegawai' => Yii::t('frontend', 'ID Pegawai'),
            'tipe_gaji_pokok' => Yii::t('frontend', 'Tipe Gaji'),
            'pot_keterlambatan' => Yii::t('frontend', 'Pot Keterlambatan'),
            'cabang' => Yii::t('frontend', 'Cabang'),
            'beatroute' => Yii::t('frontend', 'Beatroute'),
            'nama_lengkap' => Yii::t('frontend', 'Nama Lengkap'),
            'bisnis_unit_s' => Yii::t('frontend', 'Bisnis Unit'),
            'golongan_s' => Yii::t('frontend', 'Golongan'),
            'jenis_kontrak_s' => Yii::t('frontend', 'Jenis Kontrak'),
            'struktur_s' => Yii::t('frontend', 'Struktur'),
            'masuk' => Yii::t('frontend', 'Masuk'),
            'alfa' => Yii::t('frontend', 'Alfa'),
            'ijin' => Yii::t('frontend', 'Ijin'),
            'libur' => Yii::t('frontend', 'Libur'),
            'cuti' => Yii::t('frontend', 'Cuti'),
            'undifined' => Yii::t('frontend', 'Undifined'),
            'dispensasi' => Yii::t('frontend', 'Dispensasi'),
            'tl' => Yii::t('frontend', 'TL'),
            'is' => Yii::t('frontend', 'IS'),
            'cm' => Yii::t('frontend', 'CM'),
            'off' => Yii::t('frontend', 'OFF'),
            'idl' => Yii::t('frontend', 'IDL'),
            'ith' => Yii::t('frontend', 'ITH'),
            'sd' => Yii::t('frontend', 'SD'),
            'wfh' => Yii::t('frontend', 'WFH'),
            'atl' => Yii::t('frontend', 'ATL'),
            'sh' => Yii::t('frontend', 'SH'),
            'jam_lembur' => Yii::t('frontend', 'Jam Lembur'),
            's2' => Yii::t('frontend', 'Shift2'),
            's2_nominal' => Yii::t('frontend', 'S2 Nominal'),
            's3' => Yii::t('frontend', 'Shift3'),
            's3_nominal' => Yii::t('frontend', 'S3 Nominal'),
            'totals3' => Yii::t('frontend', 'S3'),
            'uang_lembur' => Yii::t('frontend', 'Uang Lembur'),
            'insentif_lembur' => Yii::t('frontend', 'Insentif Lembur'),
            'uml' => Yii::t('frontend', 'Uang Makan Lembur'),
            'p_gaji_pokok' => Yii::t('frontend', 'Gaji Pokok'),
            'p_admin_bank' => Yii::t('frontend', 'Admin Bank'),
            'pinjaman_angsuran_id' => Yii::t('frontend', 'Pinjaman'),
            'pinjaman_nominal' => Yii::t('frontend', 'Pinjaman Nominal'),
            'angsuran_ke' => Yii::t('frontend', 'Angsuran Ke'),
            'pinjaman_bank_id' => Yii::t('frontend', 'Pinjaman'),
            'pinjaman_bank_nominal' => Yii::t('frontend', 'Pinjaman Nominal'),
            'angsuran_bank_ke' => Yii::t('frontend', 'Angsuran Ke'),
            'pinjaman_kasbon_id' => Yii::t('frontend', 'Pinjaman'),
            'pinjaman_kasbon_nominal' => Yii::t('frontend', 'Pinjaman Nominal'),
            'angsuran_kasbon_ke' => Yii::t('frontend', 'Angsuran Ke'),
            'pinjaman_koperasi_id' => Yii::t('frontend', 'Pinjaman'),
            'pinjaman_koperasi_nominal' => Yii::t('frontend', 'Pinjaman Nominal'),
            'angsuran_koperasi_ke' => Yii::t('frontend', 'Angsuran Ke'),
            'pinjaman_potongan_id' => Yii::t('frontend', 'Pinjaman'),
            'pinjaman_potongan_nominal' => Yii::t('frontend', 'Pinjaman Nominal'),
            'angsuran_potongan_ke' => Yii::t('frontend', 'Angsuran Ke'),
            'pinjaman_lain_lain_id' => Yii::t('frontend', 'Pinjaman'),
            'pinjaman_lain_lain_nominal' => Yii::t('frontend', 'Pinjaman Nominal'),
            'angsuran_lain_lain_ke' => Yii::t('frontend', 'Angsuran Ke'),
            'pinjaman_potongan_atl_id' => Yii::t('frontend', 'Pinjaman Potongan Atl'),
            'pinjaman_potongan_atl_nominal' => Yii::t('frontend', 'Pinjaman Potongan Atl Nominal'),
            'angsuran_potongan_atl_ke' => Yii::t('frontend', 'Angsuran Potongan Atl Ke'),
            'p_insentif_tetap' => Yii::t('frontend', 'Insentif Tetap'),
            'p_premi_hadir' => Yii::t('frontend', 'Premi Hadir'),
            'p_sewa_mobil' => Yii::t('frontend', 'Sewa Mobil'),
            'p_sewa_motor' => Yii::t('frontend', 'Sewa Motor'),
            'p_sewa_rumah' => Yii::t('frontend', 'Sewa Rumah'),
            'p_tunjagan_lain_tetap' => Yii::t('frontend', 'Tunjagan Lain Tetap'),
            'p_tunjagan_lain_lain' => Yii::t('frontend', 'Tunjagan Lain Lain'),
            'p_tunjangan_anak' => Yii::t('frontend', 'Tunjangan Anak'),
            'p_tunjangan_jabatan' => Yii::t('frontend', 'Tunjangan Jabatan'),
            'p_tunjangan_kost' => Yii::t('frontend', 'Tunjangan Kost'),
            'p_tunjangan_loyalitas' => Yii::t('frontend', 'Tunjangan Loyalitas'),
            'p_tunjangan_pulsa' => Yii::t('frontend', 'Tunjangan Pulsa'),
            'p_uang_makan' => Yii::t('frontend', 'Uang Makan'),
            'p_uang_transport' => Yii::t('frontend', 'Uang Transport'),
            'hari_kerja_aktif' => Yii::t('frontend', 'Hari Kerja Aktif'),
            'pegawai_bpjs_id' => Yii::t('frontend', 'Pegawai Bpjs'),
            'pegawai_bpjs_kes' => Yii::t('frontend', 'Bpjs Kes'),
            'pegawai_bpjs_tk' => Yii::t('frontend', 'Bpjs TK'),
            'pegawai_bank_nama' => Yii::t('frontend', 'Pegawai Bank Nama'),
            'pegawai_bank_nomor' => Yii::t('frontend', 'Pegawai Bank Nomor'),
            'jumlah_telat' => Yii::t('frontend', 'Jumlah Telat'),
            'proses_gaji_id' => Yii::t('frontend', 'Proses Gaji'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'pegawai_sistem_kerja_id' => Yii::t('frontend', 'Pegawai Sistem Kerja ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'tunj_pph' => Yii::t('frontend', 'Tunjangan PPH'),
            'pot_pph' => Yii::t('frontend', 'Potongan PPH'),
            'pot_bpjs' => Yii::t('frontend', 'Potongan BPJS'),
            'pot_gaji' => Yii::t('frontend', 'Potongan Gaji'),
            'pot_lain' => Yii::t('frontend', 'Potongan Lain-lain'),
            'pot_atl' => Yii::t('frontend', 'Pot ATL'),
            'lock' => Yii::t('frontend', 'Lock'),
            'total_us' => Yii::t('frontend', 'Uang Shift'),
            'total_um' => Yii::t('frontend', 'Total Uang Makan'),
            'total_ut' => Yii::t('frontend', 'Total Uang Transport'),
            'total_gaji' => Yii::t('frontend', 'Total Gaji'),
            'bank' => Yii::t('frontend', 'Bank'),
            'koperasi' => Yii::t('frontend', 'Koperasi'),
            'lain_lain' => Yii::t('frontend', 'Lain-lain'),
            'insentif_tidak_tetap' => Yii::t('frontend', 'Insentif Tidak Tetap'),
            'insentif_pph' => Yii::t('frontend', 'Insentif PPH'),
            'insentif_gt' => Yii::t('frontend', 'Insentif GT'),
            'libur_hadir' => Yii::t('frontend', 'Libur Hadir'),
            'libur_hadir_s1' => Yii::t('frontend', 'Libur Hadir Shift 1'),
            'libur_hadir_s2' => Yii::t('frontend', 'Libur Hadir Shift 2'),
            'libur_hadir_s3' => Yii::t('frontend', 'Libur Hadir Shift 3'),
            'atl_s1' => Yii::t('frontend', 'ATL S1'),
            'atl_s2' => Yii::t('frontend', 'ATL S2'),
            'atl_s3' => Yii::t('frontend', 'ATL S3'),
            'sh_s2' => Yii::t('frontend', 'SH S2'),
            'sh_s3' => Yii::t('frontend', 'SH S3'),
            'status_pernikahan' => Yii::t('frontend', 'Status Pernikahan'),
            'jumlah_anak' => Yii::t('frontend', 'Jumlah Anak'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPAdminBank()
    {
        return $this->hasOne(PAdminBank::class, ['id' => 'p_admin_bank_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPGajiPokok()
    {
        return $this->hasOne(PGajiPokok::class, ['id' => 'p_gaji_pokok_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPInsentifTetap()
    {
        return $this->hasOne(PInsentifTetap::class, ['id' => 'p_insentif_tetap_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPPremiHadir()
    {
        return $this->hasOne(PPremiHadir::class, ['id' => 'p_premi_hadir_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPSewaMobil()
    {
        return $this->hasOne(PSewaMobil::class, ['id' => 'p_sewa_mobil_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPSewaMotor()
    {
        return $this->hasOne(PSewaMotor::class, ['id' => 'p_sewa_motor_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPSewaRumah()
    {
        return $this->hasOne(PSewaRumah::class, ['id' => 'p_sewa_rumah_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjaganLainTetap()
    {
        return $this->hasOne(PTunjaganLainTetap::class, ['id' => 'p_tunjagan_lain_tetap_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganAnakConfig()
    {
        return $this->hasOne(PTunjanganAnakConfig::class, ['id' => 'p_tunjangan_anak_config_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganJabatan()
    {
        return $this->hasOne(PTunjanganJabatan::class, ['id' => 'p_tunjangan_jabatan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganKost()
    {
        return $this->hasOne(PTunjanganKost::class, ['id' => 'p_tunjangan_kost_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganLoyalitasConfig()
    {
        return $this->hasOne(PTunjanganLoyalitasConfig::class, ['id' => 'p_tunjangan_loyalitas_config_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganPulsa()
    {
        return $this->hasOne(PTunjanganPulsa::class, ['id' => 'p_tunjangan_pulsa_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPUangMakan()
    {
        return $this->hasOne(PUangMakan::class, ['id' => 'p_uang_makan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPUangTransport()
    {
        return $this->hasOne(PUangTransport::class, ['id' => 'p_uang_transport_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiBank()
    {
        return $this->hasOne(PegawaiBank::class, ['id' => 'pegawai_bank_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiBpjs()
    {
        return $this->hasOne(PegawaiBpjs::class, ['id' => 'pegawai_bpjs_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiSistemKerja()
    {
        return $this->hasOne(PegawaiSistemKerja::class, ['id' => 'pegawai_sistem_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCostCenter()
    {
        return $this->hasOne(CostCenter::class, ['id' => 'cost_center_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPinjamanBank()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PinjamanAngsuran::class, ['id' => 'pinjaman_bank_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPinjamanKasbon()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PinjamanAngsuran::class, ['id' => 'pinjaman_kasbon_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPinjamanKoperasi()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PinjamanAngsuran::class, ['id' => 'pinjaman_koperasi_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPinjamanPotongan()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PinjamanAngsuran::class, ['id' => 'pinjaman_potongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPinjamanLainLain()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PinjamanAngsuran::class, ['id' => 'pinjaman_lain_lain_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPinjamanAngsuran()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PinjamanAngsuran::class, ['id' => 'pinjaman_angsuran_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInsentifTidakTetap()
    {
        return $this->hasOne(\frontend\modules\payroll\models\InsentifTidakTetap::class, ['id' => 'insentif_tidak_tetap_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGrapel()
    {
        return $this->hasOne(\frontend\modules\payroll\models\Rapel::class, ['id' => 'rappel_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProsesGaji()
    {
        return $this->hasOne(\frontend\modules\payroll\models\ProsesGaji::class, ['id' => 'proses_gaji_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGajiBulananDetails()
    {
        return $this->hasMany(\frontend\modules\payroll\models\GajiBulananDetail::class, ['gaji_bulanan_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGajiPph21Parent()
    {
        return $this->hasOne(\frontend\modules\payroll\models\Rapel::class, ['id' => 'rappel_id'])->andWhere(['gaji_pph21_id' => null]);
    }

    /**
     * @return ActiveQuery
     */
    public function getGajiPph21Child()
    {
        return $this->hasOne(\frontend\modules\payroll\models\Rapel::class, ['id' => 'rappel_id'])->andWhere(['NOT', 'gaji_pph21_id', null]);
    }

    /**
     * @return false|string
     */
    public function tahunPeriodePayroll()
    {
        if ($this->pegawai) {
            return date('Y', strtotime($this->prosesGaji->payrollPeriode->tanggal_akhir));
        }
        return '';
    }

    /**
     * @return ActiveQuery
     */
    public function getPtkpSetting()
    {
        return PtkpSetting::find()->andWhere(['tahun' => $this->tahunPeriodePayroll()]);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\components\UUIDBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\PegawaiPtkp;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "gaji_pph21".
 *
 * @property string $id
 * @property float $gaji
 * @property float $tunjangan_pph
 * @property float $tunjangan
 * @property float $premi_asuransi
 * @property float $bonus
 * @property float $penghasilan_bruto
 * @property float $biaya_jabatan
 * @property float $iuran_pensiun
 * @property float $pengurangan
 * @property float $penghasilan_netto
 * @property float $penghasilan_netto_setahun
 * @property integer $ptkp_status
 * @property float $ptkp_nominal
 * @property float $pkp
 * @property integer $jumlah_bulan
 * @property float $pph21_terhutang_setahun
 * @property float $pph21_terhutang
 * @property string $gaji_bulanan_id
 * @property string $pegawai_ptkp_id
 * @property string $gaji_pph21_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\payroll\models\GajiBulanan $gajiBulanan
 * @property PegawaiPtkp $pegawaiPtkp
 * @property \frontend\modules\payroll\models\GajiPph21 $gajiPhh21ToChild
 * @property \frontend\modules\payroll\models\GajiPph21 $gajiPhh21ToParent
 */
class GajiPph21 extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gaji_bulanan_id', 'pegawai_ptkp_id'], 'required'],
            [['gaji', 'tunjangan_pph', 'tunjangan', 'premi_asuransi', 'bonus', 'penghasilan_bruto', 'biaya_jabatan', 'iuran_pensiun', 'pengurangan', 'penghasilan_netto', 'penghasilan_netto_setahun', 'ptkp_nominal', 'pkp', 'pph21_terhutang_setahun', 'pph21_terhutang'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['jumlah_bulan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_ptkp_id'], 'string', 'max' => 36],
            [['ptkp_status'], 'string', 'max' => 5],
            [['gaji_bulanan_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gaji_pph21';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'gaji' => Yii::t('frontend', 'Gaji'),
            'tunjangan_pph' => Yii::t('frontend', 'Tunjangan PPH'),
            'tunjangan' => Yii::t('frontend', 'Tunjangan lainnya'),
            'premi_asuransi' => Yii::t('frontend', 'Premi Asuransi'),
            'bonus' => Yii::t('frontend', 'Bonus'),
            'penghasilan_bruto' => Yii::t('frontend', 'Penghasilan Bruto'),
            'biaya_jabatan' => Yii::t('frontend', 'Biaya Jabatan'),
            'iuran_pensiun' => Yii::t('frontend', 'Iuran Pensiun'),
            'pengurangan' => Yii::t('frontend', 'Pengurangan'),
            'penghasilan_netto' => Yii::t('frontend', 'Penghasilan Netto'),
            'penghasilan_netto_setahun' => Yii::t('frontend', 'Penghasilan Netto Setahun'),
            'ptkp_status' => Yii::t('frontend', 'PTKP Status'),
            'ptkp_nominal' => Yii::t('frontend', 'PTKP Nominal'),
            'pkp' => Yii::t('frontend', 'PKP'),
            'jumlah_bulan' => Yii::t('frontend', 'Jumlah Bulan'),
            'pph21_terhutang_setahun' => Yii::t('frontend', 'Pph21 Terhutang Setahun'),
            'pph21_terhutang' => Yii::t('frontend', 'Pph21 Terhutang'),
            'gaji_bulanan_id' => Yii::t('frontend', 'Gaji Bulanan ID'),
            'pegawai_ptkp_id' => Yii::t('frontend', 'Pegawai Ptkp ID'),
            'gaji_pph21_id' => Yii::t('frontend', 'Parent'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getGajiBulanan()
    {
        return $this->hasOne(\frontend\modules\payroll\models\GajiBulanan::class, ['id' => 'gaji_bulanan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiPtkp()
    {
        return $this->hasOne(PegawaiPtkp::class, ['id' => 'pegawai_ptkp_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getGajiPhh21ToChild()
    {
        return $this->hasOne(\frontend\modules\payroll\models\GajiPph21::class, ['gaji_bulanan_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGajiPhh21ToParent()
    {
        return $this->hasOne(\frontend\modules\payroll\models\GajiPph21::class, ['id' => 'gaji_bulanan_id']);
    }


    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete(): bool
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\components\UUIDBehavior;
use frontend\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "payroll_borongan".
 *
 * @property string $id
 * @property string $tanggal
 * @property integer $shift
 * @property string $mulai
 * @property string $selesai
 * @property integer $hasil
 * @property float $upah_per_orang
 * @property string $catatan
 * @property string $jenis_borongan_id
 * @property integer $last_status
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\payroll\models\JenisBorongan $jenisBorongan
 * @property \frontend\modules\payroll\models\PayrollBoronganDetail[] $payrollBoronganDetails
 */
class PayrollBorongan extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal', 'mulai', 'selesai', 'hasil', 'jenis_borongan_id'], 'required'],
            [['shift', 'hasil', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['mulai', 'selesai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['id', 'jenis_borongan_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_borongan';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tanggal' => Yii::t('frontend', 'Tanggal'),
            'shift' => Yii::t('frontend', 'Shift'),
            'mulai' => Yii::t('frontend', 'Mulai'),
            'selesai' => Yii::t('frontend', 'Selesai'),
            'hasil' => Yii::t('frontend', 'Hasil'),
            'upah_per_orang' => Yii::t('frontend', 'Upah Per Orang'),
            'catatan' => Yii::t('frontend', 'Catatan'),
            'jenis_borongan_id' => Yii::t('frontend', 'Jenis Borongan ID'),
            'last_status' => Yii::t('frontend', 'Last Status'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getJenisBorongan()
    {
        return $this->hasOne(\frontend\modules\payroll\models\JenisBorongan::class, ['id' => 'jenis_borongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayrollBoronganDetails()
    {
        return $this->hasMany(\frontend\modules\payroll\models\PayrollBoronganDetail::class, ['payroll_borongan_id' => 'id']);
    }


    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

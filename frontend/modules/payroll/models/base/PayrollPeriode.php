<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "payroll_periode".
 *
 * @property string $id
 * @property integer $tipe
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property string $catatan
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\payroll\models\InsentifTidakTetap[] $insentifTidakTetaps
 * @property \frontend\modules\payroll\models\Pinjaman[] $pinjamen
 * @property \frontend\modules\payroll\models\Rapel[] $rapels
 */
class PayrollPeriode extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_awal', 'tanggal_akhir'], 'required'],
            [['tipe', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_periode';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tipe' => Yii::t('frontend', 'Tipe'),
            'tanggal_awal' => Yii::t('frontend', 'Tanggal Awal'),
            'tanggal_akhir' => Yii::t('frontend', 'Tanggal Akhir'),
            'catatan' => Yii::t('frontend', 'Catatan'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }
    
    /**
     * @return ActiveQuery
     */
    public function getInsentifTidakTetaps()
    {
        return $this->hasMany(\frontend\modules\payroll\models\InsentifTidakTetap::class, ['payroll_periode_id' => 'id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPinjamen()
    {
        return $this->hasMany(\frontend\modules\payroll\models\Pinjaman::class, ['payroll_periode_id' => 'id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getRapels()
    {
        return $this->hasMany(\frontend\modules\payroll\models\Rapel::class, ['periode_bayar' => 'id']);
    }


    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}
<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\components\DefaultPKBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "payroll_diluar_proses".
 *
 * @property string $id
 * @property integer $jenis
 * @property integer $jumlah
 * @property string $tanggal_bayar
 * @property string $keterangan
 * @property string $proses_gaji_id
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\payroll\models\ProsesGaji $prosesGaji
 * @property Pegawai $pegawai
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 */
class PayrollDiluarProses extends ActiveRecord
{
    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_diluar_proses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_bayar', 'proses_gaji_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'required'],
            [['jenis', 'jumlah', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_bayar', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id', 'proses_gaji_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'jenis' => Yii::t('frontend', 'Jenis'),
            'jumlah' => Yii::t('frontend', 'Jumlah'),
            'tanggal_bayar' => Yii::t('frontend', 'Tanggal Bayar'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'proses_gaji_id' => Yii::t('frontend', 'Proses Gaji ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProsesGaji()
    {
        return $this->hasOne(\frontend\modules\payroll\models\ProsesGaji::class, ['id' => 'proses_gaji_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
            'defaultPK' => [
                'class' => DefaultPKBehavior::class,
                'values' => [
                    'perjanjian_kerja_id' => function () {
                        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->prosesGaji->payrollPeriode->tanggal_akhir)) {
                            return $perjanjianKerja->id;
                        }
                        return false;
                    },
                    'pegawai_golongan_id' => function () {
                        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->prosesGaji->payrollPeriode->tanggal_akhir)) {
                            return $golongan->id;
                        }
                        return false;
                    },
                    'pegawai_struktur_id' => function () {
                        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->prosesGaji->payrollPeriode->tanggal_akhir)) {
                            return $struktur->id;
                        }
                        return false;
                    },
                ],
            ],
        ];
    }
}

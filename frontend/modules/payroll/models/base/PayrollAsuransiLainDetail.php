<?php

namespace frontend\modules\payroll\models\base;

use common\components\CreatedByBehavior;
use common\components\DefaultPKBehavior;
use common\components\UUIDBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiAsuransiLain;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "payroll_asuransi_lain_detail".
 *
 * @property string $id
 * @property double $potongan_perusahaan
 * @property double $potongan_karyawan
 * @property string $keterangan
 * @property string $payroll_asuransi_lain_id
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $pegawai_asuransi_lain_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\payroll\models\PayrollAsuransiLain $payrollAsuransiLain
 * @property Pegawai $pegawai
 * @property PegawaiAsuransiLain $pegawaiAsuransiLain
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 */
class PayrollAsuransiLainDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payroll_asuransi_lain_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'pegawai_asuransi_lain_id'], 'required'],
            [['potongan_perusahaan', 'potongan_karyawan'], 'number'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'payroll_asuransi_lain_id'], 'string', 'max' => 36],
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'pegawai_asuransi_lain_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_asuransi_lain_detail';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'potongan_perusahaan' => Yii::t('frontend', 'Potongan Perusahaan'),
            'potongan_karyawan' => Yii::t('frontend', 'Potongan Karyawan'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'payroll_asuransi_lain_id' => Yii::t('frontend', 'Payroll Asuransi Lain ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'pegawai_asuransi_lain_id' => Yii::t('frontend', 'Pegawai Asuransi Lain ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPayrollAsuransiLain()
    {
        return $this->hasOne(\frontend\modules\payroll\models\PayrollAsuransiLain::class, ['id' => 'payroll_asuransi_lain_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiAsuransiLain()
    {
        return $this->hasOne(PegawaiAsuransiLain::class, ['id' => 'pegawai_asuransi_lain_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
            'defaultPK' => [
                'class' => DefaultPKBehavior::class,
                'values' => [
                    'perjanjian_kerja_id' => function () {
                        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->payrollAsuransiLain->payrollPeriode->tanggal_awal)) {
                            return $perjanjianKerja->id;
                        }
                        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->payrollAsuransiLain->payrollPeriode->tanggal_akhir)) {
                            return $perjanjianKerja->id;
                        }
                        return false;
                    },
                    'pegawai_golongan_id' => function () {
                        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->payrollAsuransiLain->payrollPeriode->tanggal_awal)) {
                            return $golongan->id;
                        }
                        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->payrollAsuransiLain->payrollPeriode->tanggal_akhir)) {
                            return $golongan->id;
                        }
                        return false;
                    },
                    'pegawai_struktur_id' => function () {
                        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->payrollAsuransiLain->payrollPeriode->tanggal_awal)) {
                            return $struktur->id;
                        }
                        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->payrollAsuransiLain->payrollPeriode->tanggal_akhir)) {
                            return $struktur->id;
                        }
                        return false;
                    },
                ],
            ],
        ];
    }
}
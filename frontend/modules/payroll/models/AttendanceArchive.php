<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\AttendanceArchive as BaseAttendanceArchive;

/**
 * This is the model class for table "attendance_archive".
 */
class AttendanceArchive extends BaseAttendanceArchive
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'check_time', 'idfp', 'gaji_bulanan_id'], 'required'],
            [['id', 'uploaded', 'idfp', 'attendance_beatroute_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'check_time', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['pegawai_id', 'gaji_bulanan_id'], 'string', 'max' => 32],
            [['device_id'], 'string', 'max' => 45],
            [['lock', 'uploaded'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

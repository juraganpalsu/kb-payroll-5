<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\PinjamanAngsuran as BasePinjamanAngsuran;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "pinjaman_angsuran".
 *
 * @property array $_status
 * @property string $status_
 */
class PinjamanAngsuran extends BasePinjamanAngsuran
{
    const BELUM_DIBAYAR = 0;
    const DIBAYAR = 1;

    public $_status = [self::BELUM_DIBAYAR => 'BELUM DIBAYAR', self::DIBAYAR => 'DIBAYAR'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pinjaman_id', 'jumlah'], 'required'],
            [['angsuran_ke', 'jumlah', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pinjaman_id', 'payroll_periode_id', 'created_by_pk'], 'string', 'max' => 32],
            [['status'], 'integer'],
            [['lock', 'jumlah'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->status == self::DIBAYAR) {
            $check = PinjamanAngsuran::find()->andWhere(['pinjaman_id' => $this->pinjaman_id, 'status' => self::BELUM_DIBAYAR])->count();
            if ($check == 0) {
                $modelPinjaman = Pinjaman::findOne($this->pinjaman_id);
                $modelPinjaman->status_lunas = Pinjaman::LUNAS;
                $modelPinjaman->save(false);
            }
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getStatus_()
    {
        return $this->_status[$this->status];
    }

    /**
     * @param string $payrollPeriodeId
     * @param string $pegawaiId
     * @param int $kategori
     * @param bool $adalahGajiTerakhir
     *
     *
     * Jika penggajian terakhir, maka semua yang statusnya belum dibayarkan, langsung dibayar semuanya
     *
     * @return array menggunakan format return yg lama, agar tidak banyak perubahan pada yg object yg menggunakan fn ini
     */
    public function getAngsuran(string $payrollPeriodeId, string $pegawaiId, int $kategori = Pinjaman::BANK, bool $adalahGajiTerakhir = false)
    {
        $potonganPeriodeSekarangQuery = self::find()
            ->joinWith(['pinjaman' => function (ActiveQuery $query) use ($pegawaiId, $kategori) {
                $query->andWhere(['pinjaman.pegawai_id' => $pegawaiId, 'pinjaman.kategori' => $kategori]);
            }])
            ->andWhere(['pinjaman_angsuran.status' => self::BELUM_DIBAYAR]);
        if ($adalahGajiTerakhir) {
            $jumlahPotongan = $potonganPeriodeSekarangQuery->sum('pinjaman_angsuran.jumlah');
        } else {
            $jumlahPotongan = $potonganPeriodeSekarangQuery->andWhere(['pinjaman_angsuran.payroll_periode_id' => $payrollPeriodeId])->sum('pinjaman_angsuran.jumlah');
        }
        $jumlahPotongan = is_null($jumlahPotongan) ? 0 : $jumlahPotongan;
        return ['id' => '', 'jumlah' => $jumlahPotongan, 'angsuran_ke' => 0];
    }

    /**
     * @param GajiBulanan $gajiBulanan
     */
    public function setDibayarAll(GajiBulanan $gajiBulanan)
    {
        $payrollPeriode = $gajiBulanan->prosesGaji->payrollPeriode;
        $query = self::find()->andWhere(['pinjaman_angsuran.payroll_periode_id' => $payrollPeriode->id])
            ->joinWith(['pinjaman' => function (ActiveQuery $query) use ($gajiBulanan) {
                $query->andWhere(['pinjaman.pegawai_id' => $gajiBulanan->pegawai_id]);
            }])->all();
        if (!empty($query)) {
            foreach ($query as $pinjamanAngsuran) {
                if ($model = self::findOne($pinjamanAngsuran['id'])) {
                    $model->status = self::DIBAYAR;
                    $model->save();
                }
            }
        }
    }

    /**
     * @param GajiBulanan $gajiBulanan
     */
    public function bayarSemuaSisaAngsuran(GajiBulanan $gajiBulanan)
    {
        $query = self::find()
            ->joinWith(['pinjaman' => function (ActiveQuery $query) use ($gajiBulanan) {
                $query->andWhere(['pinjaman.pegawai_id' => $gajiBulanan->pegawai_id]);
            }])
            ->andWhere(['pinjaman_angsuran.status' => self::BELUM_DIBAYAR])
            ->all();
        if (!empty($query)) {
            foreach ($query as $pinjamanAngsuran) {
                if ($model = self::findOne($pinjamanAngsuran['id'])) {
                    $model->status = self::DIBAYAR;
                    $model->save();
                }
            }
        }
    }

    /**
     * @param GajiBulanan $gajiBulanan
     */
    public static function setDibayarAllStatic(GajiBulanan $gajiBulanan)
    {
        $model = new self();
        if ($gajiBulanan->prosesGaji->adalahGajiTerakhir($gajiBulanan->perjanjianKerja)) {
            $model->bayarSemuaSisaAngsuran($gajiBulanan);
        } else {
            $model->setDibayarAll($gajiBulanan);
        }
    }

    /**
     * @param string $pinjamanid
     * @param string $kategori
     * @param string $idPegawai
     * @return int
     */
    public function countStatusDibayar(string $pinjamanid = '', string $kategori = '', string $idPegawai = '')
    {
        $query = self::find()
            ->joinWith(['pinjaman' => function (ActiveQuery $query) use ($kategori, $idPegawai) {
                $query->andWhere(['pinjaman.pegawai_id' => $idPegawai, 'pinjaman.kategori' => $kategori]);
            }])
            ->andWhere(['pinjaman_angsuran.status' => self::DIBAYAR,
                'pinjaman_angsuran.pinjaman_id' => $pinjamanid])->count();
        return (int)$query;
    }

    /**
     * @param string $pinjamanid
     * @param string $kategori
     * @param string $idPegawai
     * @return mixed|null
     */
    public static function getPeriodeAkhir(string $pinjamanid = '', string $kategori = '', string $idPegawai = '')
    {
        $query = self::find()
            ->joinWith(['pinjaman' => function (ActiveQuery $query) use ($kategori, $idPegawai) {
                $query->andWhere(['pinjaman.pegawai_id' => $idPegawai, 'pinjaman.kategori' => $kategori]);
            }])
            ->andWhere(['pinjaman_angsuran.pinjaman_id' => $pinjamanid])->orderBy('pinjaman_angsuran.angsuran_ke DESC')->one();
        return $query->payroll_periode_id;
    }

    /**
     * @param string $pinjamanid
     * @param string $kategori
     * @param string $idPegawai
     * @return int
     */
    public function sisaBayar(string $pinjamanid = '', string $kategori = '', string $idPegawai = ''): int
    {
        $query = self::find()
            ->joinWith(['pinjaman' => function (ActiveQuery $query) use ($kategori, $idPegawai) {
                $query->andWhere(['pinjaman.pegawai_id' => $idPegawai, 'pinjaman.kategori' => $kategori]);
            }])
            ->andWhere(['pinjaman_angsuran.pinjaman_id' => $pinjamanid, 'pinjaman_angsuran.status' => self::BELUM_DIBAYAR]);
        return (int)$query->count();
    }

}

<?php

namespace frontend\modules\payroll\models;

use Yii;
use \frontend\modules\payroll\models\base\ThrSetting as BaseThrSetting;

/**
 * This is the model class for table "thr_setting".
 */
class ThrSetting extends BaseThrSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'cut_off_thr', 'cut_off_resign'], 'required'],
            [['cut_off_thr', 'cut_off_resign', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['tahun', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

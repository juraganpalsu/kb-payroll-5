<?php

namespace frontend\modules\payroll\models;

use common\components\Status;
use frontend\models\User;
use frontend\modules\erp\models\EApprovalDetail;
use frontend\modules\erp\models\EStatus;
use frontend\modules\payroll\models\base\PayrollBoronganDetail as BasePayrollBoronganDetail;
use ReflectionClass;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "payroll_borongan_detail".
 *
 *
 * @property EApprovalDetail $approvalByUser
 * @property EApprovalDetail[] $approvalsByUser
 * @property EApprovalDetail $rootApproval
 * @property EApprovalDetail[] $detailApprovals
 */
class PayrollBoronganDetail extends BasePayrollBoronganDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payroll_borongan_id', 'pegawai_id'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['last_status', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'payroll_borongan_id'], 'string', 'max' => 36],
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->setStatus(Status::OPEN);
        }
    }


    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $modelStatus = new EStatus();
        $modelStatus->status = $status;
        $modelStatus->relasi_id = $this->id;
        $modelStatus->table = (new ReflectionClass($this))->getName();
        if ($modelStatus->save()) {
            $this->setLastStatus($modelStatus->status);
        }
    }

    /**
     * @param int $status
     */
    public function setLastStatus(int $status): void
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        if ($model->save()) {
            if ($model->last_status == Status::CLOSED) {
                $model->updateStatusPermintaan();
            }
        }
    }

    /**
     *
     */
    public function updateStatusPermintaan(): void
    {
        if (!$this->payrollBorongan->checkAllDetailAreClosed()) {
            $this->payrollBorongan->setStatus(Status::CLOSED);
        }
    }


    /**
     * Melakukan aproval detail
     *
     * @param int $status
     * @param string $comment
     */
    public function approval(int $status, string $comment = ''): void
    {
        if ($modelApproval = $this->approvalByUser) {
            if ($modelApproval = $modelApproval->approval($status, $comment)) {
                if (in_array($modelApproval->tipe, [Status::APPROVE, Status::REJECT]) && $modelApproval->e_approval_detail_id == 0) {
                    //update status row
                    $approvalType = $modelApproval->tipe == Status::APPROVE ? Status::APPROVED : Status::REJECTED;
                    $modelApproval->payrollBoronganDetail->setStatus($approvalType);

                    //update status row parent
                    $payrollBorongan = $modelApproval->payrollBoronganDetail->payrollBorongan;
                    if (!$payrollBorongan->checkStatusApprovalByUser()) {
                        if ($payrollBorongan->checkStatusApprovalByUser(Status::APPROVE)) {
                            $payrollBorongan->setStatus(Status::APPROVED);
                        } else {
                            $payrollBorongan->setStatus(Status::REJECTED);
                        }
                    }
                }
            }
        }
    }

    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalByUser(): ActiveQuery
    {
        return $this->hasOne(EApprovalDetail::class, ['relasi_id' => 'id'])->andWhere(['e_approval_detail.pegawai_id' => User::pegawai()->id, 'table' => self::tableName()]);
    }


    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalsByUser(): ActiveQuery
    {
        return $this->hasMany(EApprovalDetail::class, ['relasi_id' => 'id'])->andWhere(['e_approval_detail.pegawai_id' => User::pegawai()->id, 'table' => self::tableName()]);
    }

    /**
     *
     * Relasi ke table approval -> mecari root dari sebuah approval
     *
     * @return ActiveQuery
     */
    public function getRootApproval(): ActiveQuery
    {
        return $this->hasOne(EApprovalDetail::class, ['e_approval_detail_id' => 'id'])->andWhere(['e_approval_detail.e_approval_detail_id' => 0]);
    }

    /**
     * Cek semua status status approval
     *
     * @param int $status default Status::REJECT
     *
     * @return bool true jika ada status yg sesuai dengan param, sebaliknya false
     */
    public function checkStatusApprovals(int $status = Status::REJECT): bool
    {
        $rejects = [];
        array_map(function (EApprovalDetail $x) use (&$rejects) {
            $rejects[] = $x->tipe;
        }, $this->detailApprovals);

        if (in_array($status, $rejects)) {
            return true;
        }
        return false;
    }


    /**
     * @return ActiveQuery
     */
    public function getDetailApprovals(): ActiveQuery
    {
        return $this->hasMany(EApprovalDetail::class, ['relasi_id' => 'id'])->andWhere(['e_approval_detail.table' => self::tableName()]);
    }


}

<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\PayrollDiluarProses as BasePayrollDiluarProses;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;

/**
 * This is the model class for table "payroll_diluar_proses".
 * @property array $_jenis
 */
class PayrollDiluarProses extends BasePayrollDiluarProses
{
    const INSENTIF = 1;
    const KEKURANGAN_UPAH = 2;
    const TERLEWAT_UPAH = 3;

    public $_jenis = [self::INSENTIF => 'INSENTIF', self::KEKURANGAN_UPAH => 'KEKURANGAN UPAH', self::TERLEWAT_UPAH => 'TERLEWAT UPAH'];


    public $upload_dir = 'uploads/';

    public $doc_file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_bayar', 'proses_gaji_id', 'pegawai_id'], 'required'],
            [['jenis', 'jumlah', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_bayar', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id', 'proses_gaji_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
        ];
    }


    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadFile()
    {
        $jenis = array_values($this->_jenis);
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(10);
        $activeSheet->getColumnDimension('D')->setWidth(15);
        $activeSheet->getColumnDimension('E')->setWidth(20);
        $activeSheet->getColumnDimension('F')->setWidth(15);
        $activeSheet->getColumnDimension('G')->setWidth(40);

        $activeSheet->mergeCells('A1:F1')->setCellValue('A1', Yii::t('frontend', 'Template Upload Payroll Diluar Proses'));

        $activeSheet->mergeCells('A2:B2')->setCellValue('A2', Yii::t('frontend', 'Jenis'));
        $activeSheet->mergeCells('C2:F2')->setCellValue('C2', ': ' . implode(", ", $jenis));

        $activeSheet->setCellValue('A3', Yii::t('frontend', 'No'));
        $activeSheet->setCellValue('B3', Yii::t('frontend', 'Id Pegawai'));
        $activeSheet->setCellValue('C3', Yii::t('frontend', 'Jenis'));
        $activeSheet->setCellValue('D3', Yii::t('frontend', 'Tanggal Bayar'));
        $activeSheet->setCellValue('E3', Yii::t('frontend', 'Periode Kurang'));
        $activeSheet->setCellValue('F3', Yii::t('frontend', 'Jumlah'));
        $activeSheet->setCellValue('G3', Yii::t('frontend', 'Keterangan'));

        $row = 3;
        for ($no = 1; $no <= 25; $no++) {
            $row++;
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, Yii::t('frontend', '1234'))->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, Yii::t('frontend', 'INSENTIF'))->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('D' . $row, '')->getStyle('D' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDD);
            $activeSheet->setCellValue('E' . $row, Yii::t('frontend', 'periode Id'))->getStyle('E' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('F' . $row, 0)->getStyle('F' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat();
            $activeSheet->setCellValue('G' . $row, '')->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));

        }
        $highestColumn = $activeSheet->getHighestColumn('3');

        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A1:G1')->applyFromArray($headerStyle, $outLineBottomBorder);
        $activeSheet->getStyle('A3:G3')->applyFromArray($headerStyle, $outLineBottomBorder);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=TemplatePayrollDiluarUpah.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }
}

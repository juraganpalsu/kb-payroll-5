<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\PayrollBpjsDetail as BasePayrollBpjsDetail;

/**
 * This is the model class for table "payroll_bpjs_detail".
 */
class PayrollBpjsDetail extends BasePayrollBpjsDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bpjs_id', 'pegawai_id', 'iuran_nominal_tk', 'iuran_nominal'], 'required'],
            [['bpjs_k_nominal', 'bpjs_tk_nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'pegawai_bpjs_t_id', 'pegawai_bpjs_tk_id'], 'safe'],
            [['id', 'bpjs_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['bpjs_k_nominal', 'bpjs_tk_nominal', 'lock', 'gaji_pokok', 'iuran_persen', 'iuran_nominal', 'iuran_persen_tk', 'iuran_nominal_tk', 'total_iuran'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['lock', 'iuran_nominal_tk', 'iuran_nominal', 'total_iuran'], 'default', 'value' => 0],
            [['gaji_pokok', 'iuran_persen', 'iuran_nominal', 'iuran_persen_tk', 'iuran_nominal_tk', 'total_iuran'], 'number'],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->total_iuran = $this->iuran_nominal + $this->iuran_nominal_tk;
        return parent::beforeSave($insert);
    }

}

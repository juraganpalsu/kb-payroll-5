<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\PayrollAsuransiLain as BasePayrollAsuransiLain;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiAsuransiLain;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "payroll_asuransi_lain".
 */
class PayrollAsuransiLain extends BasePayrollAsuransiLain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payroll_periode_id', 'golongan_id', 'bisnis_unit'], 'required'],
            [['catatan'], 'string'],
            [['golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['payroll_periode_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }


    /**
     * @return integer
     */
    public function hitungJumlahPegawai(): int
    {
        return (int)PayrollAsuransiLainDetail::find()->andWhere(['payroll_asuransi_lain_id' => $this->id])->count();
    }

    /**
     * @return array
     */
    public function getPegawais()
    {
        $tanggal_awal = $this->payrollPeriode->tanggal_awal;
        $tanggal_akhir = $this->payrollPeriode->tanggal_akhir;
        $query = Pegawai::find()
            ->joinWith(['pegawaiGolongans' => function (ActiveQuery $query) use ($tanggal_awal, $tanggal_akhir) {
                $query->andWhere(['golongan_id' => $this->golongan_id]);
                $query->andWhere('\'' . $tanggal_awal . '\' BETWEEN pegawai_golongan.mulai_berlaku AND pegawai_golongan.akhir_berlaku OR \'' . $tanggal_akhir . '\' BETWEEN pegawai_golongan.mulai_berlaku AND pegawai_golongan.akhir_berlaku OR pegawai_golongan.mulai_berlaku BETWEEN \'' . $tanggal_awal . '\' AND \'' . $tanggal_akhir . '\' OR pegawai_golongan.akhir_berlaku BETWEEN \'' . $tanggal_awal . '\' AND \'' . $tanggal_akhir . '\' OR ( \'' . $tanggal_awal . '\' <= pegawai_golongan.mulai_berlaku AND \'' . $tanggal_akhir . '\' >= pegawai_golongan.akhir_berlaku) OR ( \'' . $tanggal_awal . '\' >= pegawai_golongan.mulai_berlaku AND \'' . $tanggal_akhir . '\' <= pegawai_golongan.akhir_berlaku) OR (pegawai_golongan.mulai_berlaku <=\'' . $tanggal_akhir . '\' AND pegawai_golongan.akhir_berlaku is null)');
            }])
            ->joinWith(['perjanjianKerjas' => function (ActiveQuery $query) use ($tanggal_awal, $tanggal_akhir) {
                $query->andWhere(['kontrak' => $this->bisnis_unit]);
                $query->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarRangeTanggal($tanggal_awal, $tanggal_akhir));

            }])
            ->all();
        $modelPayrollPeriode = PayrollPeriode::findOne($this->payroll_periode_id);
        $pegawais = [];
        $pegawais['payrollPeriode'] = [
            'id' => $modelPayrollPeriode->id,
            'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
            'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
            'namaPeriode' => $modelPayrollPeriode->namaPeriode,
            'templateId' => $this->id,
        ];
        /** @var Pegawai $pegawai */
        foreach ($query as $pegawai) {
            $pegawaiGolongan = $pegawai['pegawaiGolongans'];
            $perjanjianKerja = $pegawai['perjanjianKerjas'];
            $pegawaiId = Pegawai::findOne($pegawai['id']);
            $masterData = $pegawaiId->masterDataDefaults;
            if (empty($masterData)) {
                continue;
            }
            if ($masterData->tipe_gaji_pokok != $modelPayrollPeriode->tipe) {
                continue;
            }
            $dasarMasaKerja = PerjanjianKerja::getPerjanjianDasarStatic($pegawai['id']);
            if (empty($dasarMasaKerja)) {
                continue;
            }

            if ($perjanjianKerja && $pegawaiGolongan) {
                /** @var PegawaiAsuransiLain $pegawaiAsuransiLain */
                foreach ($pegawai->pegawaiAsuransiLains as $pegawaiAsuransiLain) {
                    if (is_null($pegawaiAsuransiLain->pegawai_keluarga_id)) {
                        $pegawais['pegawais'][$pegawaiAsuransiLain->id] = [
                            'id' => $pegawai->id,
                            'nama' => $pegawai->nama_lengkap,
                            'idfp' => $perjanjianKerja[0]['id_pegawai'],
                            'golongan_id' => $pegawaiGolongan[0]['id'],
                            'perjanjian_kerja_id' => $perjanjianKerja[0]['id'],
                            'tanggal_resign' => $perjanjianKerja[0]['tanggal_resign'],
                            'nomor' => $pegawaiAsuransiLain->nomor,
                            'tanggal_kepesertaan' => $pegawaiAsuransiLain->tanggal_kepesertaan,
                            'catatan' => $pegawaiAsuransiLain->catatan
                        ];
                    } else {
                        $pegawais['pegawais'][$pegawaiAsuransiLain->id] = [
                            'id' => $pegawai->id,
                            'nama' => $pegawai->nama_lengkap . ' - ' . $pegawaiAsuransiLain->pegawaiKeluarga->nama,
                            'idfp' => 0,
                            'golongan_id' => 0,
                            'perjanjian_kerja_id' => 0,
                            'tanggal_resign' => 0,
                            'nomor' => $pegawaiAsuransiLain->nomor,
                            'tanggal_kepesertaan' => $pegawaiAsuransiLain->tanggal_kepesertaan,
                            'catatan' => $pegawaiAsuransiLain->catatan
                        ];
                    }
                }
            }
        }
        return $pegawais;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function downloadTemplate(): bool
    {
        $pegawais = $this->getPegawais();
        $fileName = $this->payrollPeriode->namaPeriode . '*' . $this->golongan->nama . '*' . PayrollBpjs::modelPerjanjianKerja()->_kontrak[$this->bisnis_unit];
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setVisible(false);
        $activeSheet->getColumnDimension('C')->setWidth(15);
        $activeSheet->getColumnDimension('D')->setWidth(30);
        $activeSheet->getColumnDimension('E')->setWidth(15);
        $activeSheet->getColumnDimension('F')->setWidth(15);
        $activeSheet->getColumnDimension('G')->setWidth(15);
        $activeSheet->getColumnDimension('H')->setWidth(15);

        $activeSheet->setCellValue('A1', $pegawais['payrollPeriode']['templateId']);
        $activeSheet->setCellValue('B1', '');
        $activeSheet->mergeCells('A2:A3')->setCellValue('A2', Yii::t('frontend', 'NO'))->getStyle('A2:A3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('B2:B3')->setCellValue('B2', Yii::t('frontend', 'id'))->getStyle('B2:B3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('C2:C3')->setCellValue('C2', Yii::t('frontend', 'ID Pegawai'))->getStyle('C2:C3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('D2:D3')->setCellValue('D2', Yii::t('frontend', 'Nama'))->getStyle('D2:D3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('E2:E3')->setCellValue('E2', Yii::t('frontend', 'Nomor'))->getStyle('E2:E3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('F2:F3')->setCellValue('F2', Yii::t('frontend', 'Tanggal Kepesertaan'))->getStyle('F2:F3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('G2:G3')->setCellValue('G2', Yii::t('frontend', 'Iuran Perusahaan'))->getStyle('G2:G3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('H2:H3')->setCellValue('H2', Yii::t('frontend', 'Iuran Karyawan'))->getStyle('H2:H3')->applyFromArray($headerStyle);

        $no = 0;
        $row = 3;
        foreach ($pegawais['pegawais'] as $pegawai) {
            $no++;
            $row++;
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, $pegawai['id'])->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, str_pad($pegawai['idfp'], 8, '0', STR_PAD_LEFT))->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('D' . $row, $pegawai['nama'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('E' . $row, $pegawai['nomor'])->getStyle('E' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('F' . $row, $pegawai['tanggal_kepesertaan'])->getStyle('F' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('G' . $row, '')->getStyle('G' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('H' . $row, '')->getStyle('H' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');

        }

        $highestColumn = $activeSheet->getHighestColumn('3');

        $activeSheet->getStyle('A2:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A2:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A2:' . $highestColumn . '3')->applyFromArray($headerStyle);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $fileName . '.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }

}

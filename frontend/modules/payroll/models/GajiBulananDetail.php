<?php

namespace frontend\modules\payroll\models;

use Yii;
use \frontend\modules\payroll\models\base\GajiBulananDetail as BaseGajiBulananDetail;

/**
 * This is the model class for table "gaji_bulanan_detail".
 */
class GajiBulananDetail extends BaseGajiBulananDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'gaji_bulanan_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'pegawai_sistem_kerja_id'], 'required'],
            [['id', 'gaji_bulanan_id', 'masuk', 'istirahat', 'masuk_istirahat', 'pulang', 'tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['pegawai_sistem_kerja_id', 'time_table_id', 'status_kehadiran', 'spl_template_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['gaji_bulanan_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'spl_id'], 'string', 'max' => 32],
            [['jumlah_jam_lembur'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

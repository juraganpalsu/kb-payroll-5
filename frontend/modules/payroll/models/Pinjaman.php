<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\Pinjaman as BasePinjaman;
use Yii;

/**
 * This is the model class for table "pinjaman".
 *
 * @property array $_kategori
 * @property array $_status
 * @property string $status_
 */
class Pinjaman extends BasePinjaman
{

    const BANK = 1;
    const KASBON = 2;
    const PINJAMAN = 3;
    const POTONGAN = 4;
    const LAIN_LAIN = 5;
    const KOPERASI = 6;
    const POTONGAN_ATL = 7;

    public $_kategori = [self::BANK => 'BANK', self::KASBON => 'KASBON', self::KOPERASI => 'KOPERASI', self::PINJAMAN => 'PINJAMAN', self::POTONGAN => 'POTONGAN UPAH', self::LAIN_LAIN => 'LAIN-LAIN', self::POTONGAN_ATL => 'POTONGAN ATL'];

    const BELUM = 0;
    const LUNAS = 1;

    public $_status = [self::BELUM => 'BELUM', self::LUNAS => 'LUNAS'];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'payroll_periode_id', 'pegawai_id', 'jumlah', 'tenor'], 'required'],
            [['kategori', 'jumlah', 'tenor', 'angsuran', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'status_lunas'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'payroll_periode_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['keterangan'], 'string'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['payroll_periode_id', 'validasiTipeGajiPokok'],
            ['tenor', 'validasiKetersediaanPeriode'],
        ];
    }

    /**
     * @return mixed
     */
    public function getStatus_()
    {
        return $this->_status[$this->status_lunas];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        $this->angsuran = $this->jumlah / $this->tenor;
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $query = PayrollPeriode::find()
                ->andWhere(['>=', 'tanggal_awal', $this->payrollPeriode->tanggal_awal])
                ->andWhere(['tipe' => $this->pegawai->masterDataDefaults->tipe_gaji_pokok])
                ->limit($this->tenor)
                ->orderBy('tanggal_awal ASC')->all();
            $angsuran = 1;
            foreach ($query as $data) {
                $modelAngsuran = new PinjamanAngsuran();
                $modelAngsuran->angsuran_ke = $angsuran;
                $modelAngsuran->jumlah = (int)$this->angsuran;
                $modelAngsuran->pinjaman_id = $this->id;
                $modelAngsuran->payroll_periode_id = $data['id'];
                if ($modelAngsuran->save()) {
                    $angsuran++;
                }
            }
        }
        return true;
    }

    /**
     * @param string $attribute
     */
    public function validasiTipeGajiPokok(string $attribute)
    {
        if ($this->payrollPeriode && $pegawai = $this->pegawai) {
            if ($masterDataDefaults = $pegawai->masterDataDefaults) {
                if ($this->payrollPeriode->tipe != $masterDataDefaults->tipe_gaji_pokok) {
                    $this->addError($attribute, Yii::t('frontend', 'Tipe gaji pokok tidak sesuai.'));
                }
            }
        }
        if ($pegawai = $this->pegawai) {
            if (!$pegawai->masterDataDefaults) {
                $this->addError($attribute, Yii::t('frontend', 'Tipe gaji pokok belum diisi.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function validasiKetersediaanPeriode(string $attribute)
    {
        if ($this->payrollPeriode) {
            $query = PayrollPeriode::find()->andWhere(['>=', 'tanggal_awal', $this->payrollPeriode->tanggal_awal])->limit($this->tenor)
                ->orderBy('tanggal_awal ASC');
            $count = $query->count();
            if ($count < $this->tenor) {
                $this->addError($attribute, Yii::t('frontend', 'Payroll periode belum tersedia.'));
            }
        }
    }

    /**
     * @return int
     */
    public function countDibayar()
    {
        if (!empty($this->pegawai_id)) {
            $model = new PinjamanAngsuran();
            $count = $model->countStatusDibayar($this->id, $this->kategori, $this->pegawai_id);
            return $count;
        }
        return 0;
    }

    /**
     * @return mixed|string
     */
    public function periodeAkhir()
    {
        $periodeAkhir = '';
        if (!empty($this->pegawai_id)) {
            $data = PinjamanAngsuran::getPeriodeAkhir($this->id, $this->kategori, $this->pegawai_id);
            if ($data) {
                $query = PayrollPeriode::find()->andWhere(['id' => $data])->one();
                $periodeAkhir = $query->namaPeriode;
            }
            return $periodeAkhir;
        }
        return $periodeAkhir;
    }

    /**
     * @return int
     */
    public function sisaBayar(): int
    {
        $model = new PinjamanAngsuran();
        return $model->sisaBayar($this->id, $this->kategori, $this->pegawai_id);
    }

}

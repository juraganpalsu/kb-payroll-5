<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\PayrollAsuransiLainDetail as BasePayrollAsuransiLainDetail;

/**
 * This is the model class for table "payroll_asuransi_lain_detail".
 */
class PayrollAsuransiLainDetail extends BasePayrollAsuransiLainDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id'], 'required'],
            [['potongan_perusahaan', 'potongan_karyawan'], 'number'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'payroll_asuransi_lain_id'], 'string', 'max' => 36],
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'pegawai_asuransi_lain_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\JenisBorongan as BaseJenisBorongan;

/**
 * This is the model class for table "jenis_borongan".
 */
class JenisBorongan extends BaseJenisBorongan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_produk', 'harga'], 'required'],
            [['harga'], 'number'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 36],
            [['nama_produk'], 'string', 'max' => 225],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\PayrollBonus as BasePayrollBonus;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "payroll_bonus".
 */
class PayrollBonus extends BasePayrollBonus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'payroll_periode_id', 'golongan_id', 'bisnis_unit'], 'required'],
            [['tahun', 'golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return PerjanjianKerja
     */
    public static function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @return array
     */
    public function getPegawais()
    {
        $tanggal_awal = $this->payrollPeriode->tanggal_awal;
        $tanggal_akhir = $this->payrollPeriode->tanggal_akhir;
        $query = Pegawai::find()
            ->joinWith(['pegawaiGolongans' => function (ActiveQuery $query) use ($tanggal_awal, $tanggal_akhir) {
                $query->andWhere(['golongan_id' => $this->golongan_id]);
                $query->andWhere('\'' . $tanggal_awal . '\' BETWEEN pegawai_golongan.mulai_berlaku AND pegawai_golongan.akhir_berlaku OR \'' . $tanggal_akhir . '\' BETWEEN pegawai_golongan.mulai_berlaku AND pegawai_golongan.akhir_berlaku OR pegawai_golongan.mulai_berlaku BETWEEN \'' . $tanggal_awal . '\' AND \'' . $tanggal_akhir . '\' OR pegawai_golongan.akhir_berlaku BETWEEN \'' . $tanggal_awal . '\' AND \'' . $tanggal_akhir . '\' OR ( \'' . $tanggal_awal . '\' <= pegawai_golongan.mulai_berlaku AND \'' . $tanggal_akhir . '\' >= pegawai_golongan.akhir_berlaku) OR ( \'' . $tanggal_awal . '\' >= pegawai_golongan.mulai_berlaku AND \'' . $tanggal_akhir . '\' <= pegawai_golongan.akhir_berlaku) OR (pegawai_golongan.mulai_berlaku <=\'' . $tanggal_akhir . '\' AND pegawai_golongan.akhir_berlaku is null)');
            }])
            ->joinWith(['perjanjianKerjas' => function (ActiveQuery $query) use ($tanggal_awal, $tanggal_akhir) {
                $query->andWhere(['kontrak' => $this->bisnis_unit]);
                $query->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarRangeTanggal($tanggal_awal, $tanggal_akhir));

            }])
            ->all();
        $modelPayrollPeriode = PayrollPeriode::findOne($this->payroll_periode_id);
        $pegawais = [];
        $pegawais['payrollPeriode'] = [
            'id' => $modelPayrollPeriode->id,
            'tanggalAwal' => $modelPayrollPeriode->tanggal_awal,
            'tanggalAkhir' => $modelPayrollPeriode->tanggal_akhir,
            'namaPeriode' => $modelPayrollPeriode->namaPeriode,
            'templateId' => $this->id,
        ];
        /** @var Pegawai $pegawai */
        foreach ($query as $pegawai) {
            $masterData = $pegawai->masterDataDefaults;
            if (empty($masterData)) {
                continue;
            }
            if (!$masterData->bonus) {
                continue;
            }
            $pegawaiGolongan = $pegawai['pegawaiGolongans'];
            $perjanjianKerja = $pegawai['perjanjianKerjas'];
            if ($perjanjianKerja && $pegawaiGolongan) {
                $pegawais['pegawais'][$pegawai->id] = [
                    'id' => $pegawai->id,
                    'nama' => $pegawai->nama_lengkap,
                    'idfp' => $perjanjianKerja[0]['id_pegawai'],
                    'golongan_id' => $pegawaiGolongan[0]['id'],
                    'perjanjian_kerja_id' => $perjanjianKerja[0]['id'],
                ];
            }
        }
        return $pegawais;
    }

    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadTemplate()
    {
        $pegawais = $this->getPegawais();
        $fileName = Yii::t('frontend', 'Bonus') . $this->tahun . '*' . $this->golongan->nama . '*' . PayrollBonus::modelPerjanjianKerja()->_kontrak[$this->bisnis_unit];
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setVisible(false);
        $activeSheet->getColumnDimension('C')->setWidth(15);
        $activeSheet->getColumnDimension('D')->setWidth(30);
        $activeSheet->getColumnDimension('E')->setWidth(15);
        $activeSheet->getColumnDimension('F')->setVisible(false);
        $activeSheet->getColumnDimension('G')->setVisible(false);
        $activeSheet->getRowDimension('1')->setVisible(false);

        $activeSheet->setCellValue('A1', $pegawais['payrollPeriode']['templateId']);
        $activeSheet->setCellValue('B1', '');
        $activeSheet->mergeCells('A2:A3')->setCellValue('A2', Yii::t('frontend', 'NO'))->getStyle('A2:A3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('B2:B3')->setCellValue('B2', Yii::t('frontend', 'id'))->getStyle('B2:B3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('C2:C3')->setCellValue('C2', Yii::t('frontend', 'ID Pegawai'))->getStyle('C2:C3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('D2:D3')->setCellValue('D2', Yii::t('frontend', 'Nama'))->getStyle('D2:D3')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('E2:E3')->setCellValue('E2', Yii::t('frontend', 'Nominal'))->getStyle('E2:E3')->applyFromArray($headerStyle);
        $activeSheet->setCellValue('F1', '');
        $activeSheet->setCellValue('G1', '');

        $no = 0;
        $row = 3;
        foreach ($pegawais['pegawais'] as $pegawai) {
            $no++;
            $row++;
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, $pegawai['id'])->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, $pegawai['idfp'])->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('D' . $row, $pegawai['nama'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('E' . $row, '')->getStyle('E' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()
                ->setFormatCode('#,##0');
            $activeSheet->setCellValue('F' . $row, $pegawai['golongan_id']);
            $activeSheet->setCellValue('G' . $row, $pegawai['perjanjian_kerja_id']);
        }

        $highestColumn = $activeSheet->getHighestColumn('3');

        $activeSheet->getStyle('A2:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A2:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A2:' . $highestColumn . '3')->applyFromArray($headerStyle);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $fileName . '.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }

    /**
     * @return bool|int|string|null
     */
    public function hitungJumlahPegawai()
    {
        return PayrollBonusDetail::find()->andWhere(['payroll_bonus_id' => $this->id])->count();
    }

}

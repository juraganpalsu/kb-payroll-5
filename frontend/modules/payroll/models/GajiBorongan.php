<?php

namespace frontend\modules\payroll\models;

use common\components\PegawaiAktifValidation;
use frontend\models\Absensi;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\base\GajiBorongan as BaseGajiBorongan;
use Yii;

/**
 * This is the model class for table "gaji_borongan".
 */
class GajiBorongan extends BaseGajiBorongan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'pegawai_id', 'shift'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['gaji', 'shift', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['gaji_borongan_header_id'], 'string', 'max' => 36],
            [['lock', 'gaji'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['tanggal', PegawaiAktifValidation::class],
            ['pegawai_id', 'validasiPegawai'],
            ['tanggal', 'validasiTanggal'],
            ['tanggal', 'validasiStatusKehadiran'],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validasiPegawai(string $attribute)
    {
        if ($this->pegawai->masterDataDefaults->tipe_gaji_pokok != MasterDataDefault::BORONGAN) {
            $this->addError($attribute, Yii::t('frontend', '{pegawai} bukan merupakan pegawai BORONGAN.', ['pegawai' => $this->pegawai->masterDataDefaults->tipe_gaji_pokok]));
        }
    }

    /**
     * @param string $attribute
     */
    public function validasiTanggal(string $attribute)
    {
        if ($this->tanggal > date('Y-m-d')) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal tidak diperkenankan.'));
        }
    }

    /**
     * @param string $attribute
     */
    public function validasiStatusKehadiran(string $attribute)
    {
        $absensi = Absensi::findOne(['pegawai_id' => $this->pegawai_id, 'tanggal' => $this->tanggal]);
        if ($absensi) {
            if ($absensi->status_kehadiran != Absensi::MASUK) {
                $this->addError($attribute, Yii::t('frontend', 'Tidak dapat digaji karena status kehadirannya : {sk}.', ['sk' => $absensi->statusKehadiran_]));
            }
        } else {
            $this->addError($attribute, Yii::t('frontend', 'Tidak terdapat data absensi.'));
        }
    }


}

<?php

namespace frontend\modules\payroll\models;

use common\components\Status;
use common\models\Spl as SplAlias;
use frontend\models\User;
use frontend\modules\erp\models\EApproval;
use frontend\modules\erp\models\EStatus;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\base\PayrollBorongan as BasePayrollBorongan;
use frontend\modules\payroll\models\search\PayrollBoronganSearch;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\struktur\models\Helper;
use ReflectionClass;
use Throwable;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Query;

/**
 * This is the model class for table "payroll_borongan".
 *
 *
 * @property EApproval $approvalByUser
 * @property EApproval[] $approvalsByUser
 * @property EApproval[] $approvals
 *
 */
class PayrollBorongan extends BasePayrollBorongan
{

    const PAYROLL_BORONGAN_APPROVAL_MODUL_ID = 3;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['tanggal', 'mulai', 'selesai', 'hasil', 'jenis_borongan_id'], 'required'],
            [['tanggal', 'mulai', 'selesai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['last_status', 'shift', 'hasil', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['id', 'jenis_borongan_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock', 'upah_per_orang'], 'default', 'value' => 0],
            [['last_status'], 'default', 'value' => Status::OPEN],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$insert) {
            $this->beforeFinish();
        }
        return parent::beforeSave($insert);
    }

    /**
     * Simpan upah per orang
     */
    public function beforeFinish()
    {
        if ($this->last_status == Status::FINISHED) {
            $this->upah_per_orang = $this->hitungNilai();
        }
    }


    /**
     * @return array
     */
    public function getListPegawai(): array
    {
        $available = [];
        $assigned = [];
        try {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $query = new Query();
            $query->select(['p.id as id', 'pk.id_pegawai', 'p.nama_lengkap', 'CONCAT(p.nama_lengkap, \' - \', pk.id_pegawai) AS text'])
                ->from('pegawai p')
                ->join('JOIN', 'master_data_default mdd', 'p.id = mdd.pegawai_id AND mdd.tipe_gaji_pokok = ' . MasterDataDefault::BORONGAN)
                ->join('LEFT JOIN', 'pegawai_struktur ps', 'p.id = ps.pegawai_id AND ((ps.mulai_berlaku <=\'' . $this->tanggal . '\' AND ps.akhir_berlaku >= \'' . $this->tanggal . '\'' . ') OR (' . 'ps.mulai_berlaku <=\'' . $this->tanggal . '\' AND ps.akhir_berlaku is null)) AND ps.struktur_id IN (' . implode(', ', $user->getStrukturBawahan()) . ')')
                ->join('LEFT JOIN', 'perjanjian_kerja pk', 'p.id = pk.pegawai_id AND ((pk.mulai_berlaku <=\'' . $this->tanggal . '\' AND pk.akhir_berlaku >= \'' . $this->tanggal . '\'' . ') OR (' . 'pk.mulai_berlaku <=\'' . $this->tanggal . '\' AND pk.akhir_berlaku is null))');
            $query->andWhere(['p.deleted_by' => 0, 'ps.deleted_by' => 0, 'pk.deleted_by' => 0, 'is_resign' => 0]);
            $command = $query->createCommand();

            foreach ($command->queryAll() as $item) {
                $available[$item['id']] = $item['text'];
            }

            $assigned = [];
            foreach ($this->payrollBoronganDetails as $detail) {
                $assigned[$detail->pegawai_id] = $detail->pegawai->namaIdPegawai;
                unset($available[$detail->pegawai_id]);
            }


        } catch (Exception $e) {
        }
        return [
            'available' => $available,
            'assigned' => $assigned,
        ];
    }

    /**
     * @param array $pegawais
     * @return int
     */
    public function createDetails(array $pegawais): int
    {
        $savedDetail = [];
        $tanggal = $this->tanggal;
        foreach ($pegawais as $pegawaiId) {
            if ($modelPegawai = Pegawai::findOne($pegawaiId)) {
                $modelPerjanjianKerja = $modelPegawai->getPerjanjianKerjaUntukTanggal($tanggal);
                $modelPegawaiStruktur = $modelPegawai->getPegawaiStrukturUntukTanggal($tanggal);
                if ($modelPerjanjianKerja && $modelPegawaiStruktur) {
                    $modelDetail = PayrollBoronganDetail::find()->joinWith(['payrollBorongan' => function (ActiveQuery $query) use ($tanggal) {
                        $query->andWhere(['tanggal' => $tanggal]);
                    }])->andWhere(['pegawai_id' => $pegawaiId])->all();
                    if (empty($modelDetail)) {
                        $splDetail = new PayrollBoronganDetail();
                        $splDetail->pegawai_id = $modelPegawai->id;
                        $splDetail->payroll_borongan_id = $this->id;
                        if ($splDetail->save()) {
                            array_push($savedDetail, $splDetail->pegawai_id);
                        }
                    }
                }
            }
        }
        return count($savedDetail);
    }

    /**
     * @param array $pegawais
     * @return int
     */
    public function deleteDetails(array $pegawais): int
    {
        $deletedDetail = [];
        foreach ($pegawais as $pegawaiId) {
            $modelDetail = PayrollBoronganDetail::findOne(['payroll_borongan_id' => $this->id, 'pegawai_id' => $pegawaiId]);
            if ($modelDetail) {
                try {
                    if ($modelDetail->delete()) {
                        $deletedDetail[] = $pegawaiId;
                    }
                } catch (Throwable $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        }
        return count($deletedDetail);
    }

    /**
     * @return float|int
     */
    public function hitungNilai()
    {
        return ($this->jenisBorongan->harga * $this->hasil) / count($this->payrollBoronganDetails);
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->setStatus(Status::OPEN);
        }
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return explode('-', $this->id)[0];
    }


    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $modelStatus = new EStatus();
        $modelStatus->status = $status;
        $modelStatus->relasi_id = $this->id;
        $modelStatus->table = (new ReflectionClass($this))->getName();
        if ($modelStatus->save()) {
            $this->setLastStatus($modelStatus->status);
        }
    }

    /**
     * @param int $status
     */
    public function setLastStatus(int $status)
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        $model->save();
    }


    /**
     *
     */
    public function submit(): bool
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $getUserApprovals = Helper::getRuteApproval($user->id, SplAlias::SPL_APPROVAL_MODUL_ID);

        //cek apakah terdapat user approval untuk modul ini
        if (!empty($getUserApprovals)) {
            $this->setStatus(Status::SUBMITED);
            $this->setStatusDetail(Status::SUBMITED);

            $this->createApprovals();
            $this->createApprovalDetails();
        } else {
            $this->setStatus(Status::FINISHED);
            $this->setStatusDetail(Status::FINISHED);
        }

        return true;
    }

    /**
     * @param int $status
     */
    public function setStatusDetail(int $status)
    {
        foreach ($this->payrollBoronganDetails as $detail) {
            $detail->setStatus($status);
        }
    }


    /**
     * @return bool
     */
    public function checkAllDetailAreClosed(): bool
    {
        return PayrollBoronganDetail::find()->andWhere(['payroll_borongan_id' => $this->id])->andWhere(['!=', 'last_status', Status::CLOSED])->exists();
    }

    /**
     *
     */
    public function createApprovals()
    {
        EApproval::createApprovals(self::PAYROLL_BORONGAN_APPROVAL_MODUL_ID, $this->id, self::tableName());
    }


    /**
     * Menyimpan data kedalam model table approval
     * --detail
     * ---user approval level 0
     * ---user approval level 1
     * ---user approval level 2
     * ---dst
     */
    public function createApprovalDetails()
    {
        foreach ($this->payrollBoronganDetails as $payrollBoronganDetail) {
            EApproval::createApprovals(self::PAYROLL_BORONGAN_APPROVAL_MODUL_ID, $payrollBoronganDetail->id, PayrollBoronganDetail::tableName(), false);
        }
    }


    /**
     * Malakukan check approval semua detail, apakah telah dilakukan aksi approval atau belum
     *
     * Jika semua product telah dilakukan approval oleh user approval, maka return false, karena sudah tidak ada status Status::DEFLT
     * Jika ada walaupun hanya satu item yg belum di approval, maka return true, karena masih ada status Status::DEFLT
     *
     *
     * @param int $status status yang akan dicek, secara default adalah status Status::DEFLT,
     * ini untuk mengecek apakah semua item telah dilakukan approval atau belum
     * Jika diisi paramnya dengan Status::APPROVE, maka akan cek apakah ada salah satu product item yg di approve
     * Jika ada satu item yg sesuai dengan param, maka return true, sebaliknya false
     *
     * @return bool
     */
    public function checkStatusApprovalByUser(int $status = Status::DEFLT): bool
    {
        $approvalTypes = [];
        array_map(function (PayrollBoronganDetail $x) use (&$approvalTypes) {
            if ($x->approvalByUser) {
                $approvalTypes[] = $x->approvalByUser->tipe;
            }
        }, $this->payrollBoronganDetails);
        if (in_array($status, $approvalTypes)) {
            return true;
        }
        return false;
    }

    /**
     * Cek status approval pada suatu detail
     *
     * @param int $status
     * @return bool true jika ada yg sesuai dengan param, sebaliknya false
     */
    public function checkStatusApprovals(int $status = Status::REJECT): bool
    {
        $model = self::findOne($this->id);
        $rejects = [];
        array_map(function (PayrollBoronganDetail $x) use (&$rejects, $status) {
            $rejects[] = $x->checkStatusApprovals($status);
        }, $model->payrollBoronganDetails);
        if (in_array(true, $rejects)) {
            return true;
        }
        return false;
    }

    /**
     * Melakukan aproval items
     *
     * @param int $status
     * @param string $comment
     */
    public function approval(int $status, string $comment = '')
    {
        array_map(function (PayrollBoronganDetail $x) use ($status, $comment) {
            $x->approval($status, $comment);
        }, $this->payrollBoronganDetails);
    }


    /**
     * @param string $komentar
     */
    public function createApproval(string $komentar = '')
    {
        if (!$this->checkStatusApprovalByUser()) {
            if ($this->checkStatusApprovalByUser(Status::APPROVE)) {
                $this->approvalByUser->createApproval();
            } else {
                $this->approvalByUser->createApproval(Status::REJECT, $komentar);
            }
        }
    }


    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalByUser(): ActiveQuery
    {
        $pegawaiId = 0;
        if ($pegawai = User::pegawai()) {
            $pegawaiId = $pegawai->id;
        }
        return $this->hasOne(EApproval::class, ['relasi_id' => 'id'])->andWhere(['e_approval.pegawai_id' => $pegawaiId, 'table' => self::tableName()]);
    }

    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalsByUser(): ActiveQuery
    {
        $pegawaiId = 0;
        if ($pegawai = User::pegawai()) {
            $pegawaiId = $pegawai->id;
        }
        return $this->hasMany(EApproval::class, ['relasi_id' => 'id'])->andWhere(['e_approval.pegawai_id' => $pegawaiId, 'table' => self::tableName()]);
    }

    /**
     * Mengambil semua data user approval
     *
     * @return ActiveQuery
     */
    public function getApprovals(): ActiveQuery
    {
        return $this->hasMany(EApproval::class, ['relasi_id' => 'id'])->andWhere(['table' => self::tableName()]);
    }

    /**
     * @return bool|int|string|null
     */
    public static function countApprovalStatic()
    {
        $model = new PayrollBoronganSearch();
        return $model->countApproval();
    }
}

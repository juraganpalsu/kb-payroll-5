<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\GajiPph21 as BaseGajiPph21;

/**
 * This is the model class for table "gaji_pph21".
 */
class GajiPph21 extends BaseGajiPph21
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gaji_bulanan_id', 'pegawai_ptkp_id'], 'required'],
            [['gaji', 'tunjangan_pph', 'tunjangan', 'premi_asuransi', 'bonus', 'penghasilan_bruto', 'biaya_jabatan', 'iuran_pensiun', 'pengurangan', 'penghasilan_netto', 'penghasilan_netto_setahun', 'ptkp_nominal', 'pkp', 'pph21_terhutang_setahun', 'pph21_terhutang'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['jumlah_bulan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_ptkp_id'], 'string', 'max' => 36],
            [['ptkp_status'], 'string', 'max' => 5],
            [['gaji_bulanan_id', 'gaji_pph21_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

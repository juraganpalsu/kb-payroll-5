<?php

namespace frontend\modules\payroll\models;

use frontend\modules\masterdata\models\PBpjsTkSetting;
use frontend\modules\payroll\models\base\PayrollBpjsTkDetail as BasePayrollBpjsTkDetail;

/**
 * This is the model class for table "payroll_bpjs_tk_detail".
 */
class PayrollBpjsTkDetail extends BasePayrollBpjsTkDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_bpjs_id', 'payroll_bpjs_id', 'pegawai_id', 'jkk_nominal', 'jkn_nominal', 'jht_nominal', 'jht_nominal_tk', 'iuran_pensiun_nominal', 'iuran_pensiun_tk_nominal'], 'required'],
            [['gaji_pokok', 'jkk_persen', 'jkk_nominal', 'jkn_persen', 'jkn_nominal', 'jht_persen', 'jht_nominal', 'jht_persen_tk', 'jht_nominal_tk', 'iuran_pensiun_persen', 'iuran_pensiun_nominal', 'iuran_pensiun_tk_persen', 'iuran_pensiun_tk_nominal', 'total_iuran'], 'number'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 36],
            [['pegawai_bpjs_id', 'payroll_bpjs_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'jkk_nominal', 'jkn_nominal', 'jht_nominal', 'jht_nominal_tk', 'iuran_pensiun_nominal', 'iuran_pensiun_tk_nominal'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->total_iuran = $this->jkk_nominal + $this->jkn_nominal + $this->jht_nominal + $this->jht_nominal_tk + $this->iuran_pensiun_nominal + $this->iuran_pensiun_tk_nominal;
        return parent::beforeSave($insert);
    }


    /**
     * @param string|null $tanggal
     * @return PBpjsTkSetting|null
     */
    public function getAktifSetting(string $tanggal = null)
    {
        if ($tanggal == null) {
            $tanggal = date('Y-m-d');
        }
        $query = PBpjsTkSetting::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere(['<=', 'tanggal_mulai', $tanggal])->orderBy('tanggal_mulai DESC')->one();
        return PBpjsTkSetting::findOne($query['id']);
    }

}

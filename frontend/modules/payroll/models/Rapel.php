<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\Rapel as BaseRapel;
use Yii;
use frontend\modules\pegawai\models\Pegawai;
/**
 * This is the model class for table "rapel".
 */
class Rapel extends BaseRapel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jumlah', 'periode_kurang', 'periode_bayar', 'pegawai_id'], 'required'],
            [['jumlah', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'periode_kurang', 'periode_bayar', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['periode_bayar', 'validasiPeriodeBayar']
        ];
    }

    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty(date('Y-m-d'))) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal(date('Y-m-d'))) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal(date('Y-m-d'))) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal(date('Y-m-d'))) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    public function validasiPeriodeBayar(string $attribute)
    {
        if($this->periode_kurang == $this->periode_bayar) {
            $this->addError($attribute, Yii::t('frontend', 'Periode Bayar tidak boleh sama dengan Periode Kurang'));
        }

    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal(date('Y-m-d'))) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal(date('Y-m-d'))) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal(date('Y-m-d'))) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }


}

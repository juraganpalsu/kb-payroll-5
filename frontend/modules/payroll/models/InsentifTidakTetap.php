<?php

namespace frontend\modules\payroll\models;

use frontend\modules\payroll\models\base\InsentifTidakTetap as BaseInsentifTidakTetap;
use frontend\modules\pegawai\models\Pegawai;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;

/**
 * This is the model class for table "insentif_tidak_tetap".
 * @property array $_kategori
 */
class InsentifTidakTetap extends BaseInsentifTidakTetap
{
    const INSENTIF_UMUM = 1;
    const INSENTIF_PPH = 2;
    const INSENTIF_GT = 3;

    public $_kategori = [self::INSENTIF_UMUM => 'INSENTIF', self::INSENTIF_PPH => 'INSENTIF PPH', self::INSENTIF_GT => 'INSENTIF GT(BCA)'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jumlah', 'payroll_periode_id', 'pegawai_id'], 'required'],
            [['kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['jumlah'], 'double'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'payroll_periode_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
        ];
    }


    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty(date('Y-m-d'))) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal(date('Y-m-d'))) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal(date('Y-m-d'))) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal(date('Y-m-d'))) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal(date('Y-m-d'))) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal(date('Y-m-d'))) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal(date('Y-m-d'))) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadFile()
    {
        $kategori = array_values($this->_kategori);
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(10);
        $activeSheet->getColumnDimension('D')->setWidth(20);
        $activeSheet->getColumnDimension('E')->setWidth(15);
        $activeSheet->getColumnDimension('F')->setWidth(40);

        $activeSheet->mergeCells('A1:F1')->setCellValue('A1', Yii::t('frontend', 'Template Upload Insentif Tidak Tetap'));

        $activeSheet->mergeCells('A2:B2')->setCellValue('A2', Yii::t('frontend', 'Jenis'));
        $activeSheet->mergeCells('C2:F2')->setCellValue('C2', ': ' . implode(", ", $kategori));

        $activeSheet->setCellValue('A3', Yii::t('frontend', 'No'));
        $activeSheet->setCellValue('B3', Yii::t('frontend', 'Id Pegawai'));
        $activeSheet->setCellValue('C3', Yii::t('frontend', 'Jenis'));
        $activeSheet->setCellValue('D3', Yii::t('frontend', 'Periode Bayar'));
        $activeSheet->setCellValue('E3', Yii::t('frontend', 'Jumlah'));
        $activeSheet->setCellValue('F3', Yii::t('frontend', 'Keterangan'));

        $row = 3;
        for ($no = 1; $no <= 25; $no++) {
            $row++;
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, Yii::t('frontend', '1122'))->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, Yii::t('frontend', 'INSENTIF'))->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('D' . $row, Yii::t('frontend', 'contoh : 2020-01'))->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('E' . $row, 0)->getStyle('E' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat();
            $activeSheet->setCellValue('F' . $row, '')->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));

        }
        $highestColumn = $activeSheet->getHighestColumn('3');

        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A1:F1')->applyFromArray($headerStyle, $outLineBottomBorder);
        $activeSheet->getStyle('A3:F3')->applyFromArray($headerStyle, $outLineBottomBorder);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=TemplateInsentifTidakTetap.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }

}

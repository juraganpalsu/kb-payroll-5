<?php

namespace frontend\modules\payroll\models;

use common\models\Golongan;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\base\PayrollBpjs as BasePayrollBpjs;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;

/**
 * This is the model class for table "payroll_bpjs".
 */
class PayrollBpjs extends BasePayrollBpjs
{


    const UMK = 4230792;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payroll_periode_id', 'golongan_id', 'bisnis_unit'], 'required'],
            [['catatan'], 'string'],
            [['golongan_id', 'bisnis_unit', 'last_status', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'payroll_periode_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'last_status'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['bisnis_unit', 'doubleValidation'],
            ['bisnis_unit', 'validateRole']
        ];
    }

    /**
     * @param string $attribute
     */
    public function doubleValidation(string $attribute)
    {
        if ($this->isNewRecord) {
            $query = self::findOne(['payroll_periode_id' => $this->payroll_periode_id, 'golongan_id' => $this->golongan_id, 'bisnis_unit' => $this->bisnis_unit]);
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Payroll BPJS telah ada.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function validateRole(string $attribute)
    {
//        if (Helper::checkRoute('/payroll/payroll-bpjs/index-kbu') && !in_array($this->$attribute, [PerjanjianKerja::KBU])) {
//            $this->addError($attribute, Yii::t('frontend', 'Anda tidak diijinkan untuk memproses bisnis unit tersebut.'));
//        }
//        if (Helper::checkRoute('/payroll/payroll-bpjs/index-ada') && !in_array($this->$attribute, [PerjanjianKerja::ADA])) {
//            $this->addError($attribute, Yii::t('frontend', 'Anda tidak diijinkan untuk memproses bisnis unit tersebut.'));
//        }
//        if (Helper::checkRoute('/payroll/payroll-bpjs/index-bca') && !in_array($this->$attribute, [PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA])) {
//            $this->addError($attribute, Yii::t('frontend', 'Anda tidak diijinkan untuk memproses bisnis unit tersebut.'));
//        }
//        if (Helper::checkRoute('/payroll/payroll-bpjs/index') && !in_array($this->$attribute, [PerjanjianKerja::BCA_KBU, PerjanjianKerja::BCA_ADA, PerjanjianKerja::KBU, PerjanjianKerja::ADA])) {
//            $this->addError($attribute, Yii::t('frontend', 'Anda tidak diijinkan untuk memproses bisnis unit tersebut.'));
//        }
    }


    /**
     * @return PerjanjianKerja
     */
    public static function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @return bool|int|string|null
     */
    public function hitungJumlahPegawai()
    {
        return PayrollBpjsDetail::find()->andWhere(['bpjs_id' => $this->id])->count();
    }


    /**
     *
     * Menghitung iuran BPJS TK
     * Masih ada beberapa hal yang manual, perlu diperbaiki
     *
     */
    public function generateIuranBpjs()
    {
        $pegawais = $this->getPegawais();
        foreach ($pegawais['pegawais'] as $pegawai) {
            $bpjsTkId = $pegawai['bpjs_tk_id'];
            $akomodirModelLama = $pegawai;
            $pegawai = Pegawai::findOne($pegawai['id']);

            $dasarPerhitunganIuran = 0;
            if (in_array($this->bisnis_unit, [PerjanjianKerja::ADA, PerjanjianKerja::KBU])) {
                if ($masterData = $pegawai->masterDataDefaults) {
                    $gajiPokokAktif = $masterData->getGajiPokokAktif($this->payrollPeriode->tanggal_akhir);
                    $getTunjJabatanAktif = $masterData->getTunjJabatanAktif($this->payrollPeriode->tanggal_akhir);
                    $getTunjanganLainTetap = $masterData->getTunjanganLainTetap($this->payrollPeriode->tanggal_akhir);

                    $dasarMasaKerja = PerjanjianKerja::getPerjanjianDasarStatic($pegawai->id);
                    if (empty($dasarMasaKerja)) {
                        continue;
                    }
                    $masaKerja = $dasarMasaKerja->hitungMasaKerja('', $this->payrollPeriode->tanggal_akhir, true);
                    $tAnak = 0;
                    if ($masaKerja['dalamBulan'] >= 6 && $pegawai->status_pernikahan != Pegawai::BELUM_MENIKAH) {
                        $ta = MasterDataDefault::getTunjanganAnak($pegawai->getJumlahAnak(), true);
                        if ($ta && $masterData->tunjangan_anak) {
                            $tAnak = $ta->nominal;
                        }
                    }

                    $tLoyalitas = 0;
                    $tl = MasterDataDefault::getTunjanganLoyalitas($masaKerja['tahun'], true);
                    if ($tl && $masterData->tunjangan_loyalitas) {
                        $tLoyalitas = $tl->nominal;
                    }
                    $dasarPerhitunganIuran = 0;
                    if ($gajiPokokAktif) {
                        $dasarPerhitunganIuran = $gajiPokokAktif + $getTunjJabatanAktif + $getTunjanganLainTetap + $tAnak + $tLoyalitas;
                        if ($dasarPerhitunganIuran <= self::UMK) {
                            $dasarPerhitunganIuran = self::UMK;
                        }
                    }
                }
            } else {
                $dasarPerhitunganIuran = self::UMK;
            }
            $this->createBpjsKesehatanDetail($akomodirModelLama, $dasarPerhitunganIuran);
            if ($bpjsTkId) {
                $this->createBpjsTkDetail($pegawai->id, $bpjsTkId, $dasarPerhitunganIuran);
            }
        }
    }

    /**
     * @param array $data
     * @param float $dasarPerhitunganIuran
     */
    private function createBpjsKesehatanDetail(array $data, float $dasarPerhitunganIuran): void
    {
        $bpjsDetail = new PayrollBpjsDetail();
        $bpjsDetail->bpjs_id = $this->id;
        $bpjsDetail->pegawai_bpjs_t_id = $data['bpjs_k_id'];
        $bpjsDetail->pegawai_bpjs_tk_id = $data['bpjs_tk_id'];
        $bpjsDetail->pegawai_id = $data['id'];
        $cek = PayrollBpjsDetail::findOne(['bpjs_id' => $this->id, 'pegawai_id' => $data['id']]);
        if ($cek) {
            $bpjsDetail = $cek;
        }
        $modelPayrollBpjsTk = new PayrollBpjsTkDetail();
        $modelPayrollBpjsTk->pegawai_id = $bpjsDetail->pegawai_id;
        $getSetting = $modelPayrollBpjsTk->getAktifSetting($this->payrollPeriode->tanggal_awal);
        if (!$getSetting) {
            $getSetting = $modelPayrollBpjsTk->getAktifSetting($this->payrollPeriode->tanggal_akhir);
        }
        $bpjsDetail->gaji_pokok = $dasarPerhitunganIuran > 12000000 ? 12000000 : $dasarPerhitunganIuran;
        $bpjsDetail->iuran_persen = 4;
        $bpjsDetail->iuran_persen_tk = 1;
        $bpjsDetail->iuran_nominal = 0;
        $bpjsDetail->iuran_nominal_tk = 0;

        if ($getSetting) {
            if ($getSetting->kesehatan) {
                $bpjsDetail->iuran_nominal = round($bpjsDetail->gaji_pokok * ($bpjsDetail->iuran_persen / 100), 2);
                $bpjsDetail->iuran_nominal_tk = round($bpjsDetail->gaji_pokok * ($bpjsDetail->iuran_persen_tk / 100), 2);
            }
        }

        if ($pegawaiGolongan = $bpjsDetail->pegawaiGolongan) {
            if ($pegawaiGolongan->golongan->id == Golongan::NOL) {
                $bpjsDetail->gaji_pokok = 0;
                $bpjsDetail->iuran_nominal = 0;
                $bpjsDetail->iuran_nominal_tk = 0;
            }
        }

        $bpjsDetail->bpjs_k_nominal = 0;
        $bpjsDetail->bpjs_tk_nominal = 0;
        $bpjsDetail->save();
    }

    /**
     * @param string $pegawaiId
     * @param string $bpjsId
     * @param float $dasarPerhitunganIuran
     */
    private function createBpjsTkDetail(string $pegawaiId, string $bpjsId, float $dasarPerhitunganIuran): void
    {
        $modelPayrollBpjsTk = new PayrollBpjsTkDetail();
        $modelPayrollBpjsTk->pegawai_id = $pegawaiId;
        $modelPayrollBpjsTk->pegawai_bpjs_id = $bpjsId;
        $modelPayrollBpjsTk->payroll_bpjs_id = $this->id;
        if ($check = PayrollBpjsTkDetail::findOne(['payroll_bpjs_id' => $this->id, 'pegawai_id' => $pegawaiId])) {
            $modelPayrollBpjsTk = $check;
        }
        $getSetting = $modelPayrollBpjsTk->getAktifSetting($this->payrollPeriode->tanggal_awal);
        if (!$getSetting) {
            $getSetting = $modelPayrollBpjsTk->getAktifSetting($this->payrollPeriode->tanggal_akhir);
        }
        $modelPayrollBpjsTk->gaji_pokok = $dasarPerhitunganIuran;
        $modelPayrollBpjsTk->jkk_persen = $getSetting ? $getSetting->jkk ? $modelPayrollBpjsTk->payrollBpjs->bisnis_unit == PerjanjianKerja::KBU ? 0.89 : 0.24 : 0 : 0;
        $modelPayrollBpjsTk->jkk_nominal = $getSetting ? $getSetting->jkk ? round($modelPayrollBpjsTk->gaji_pokok * ($modelPayrollBpjsTk->jkk_persen / 100), 2) : 0 : 0;
        $modelPayrollBpjsTk->jkn_persen = $getSetting ? $getSetting->jkm ? 0.30 : 0 : 0;
        $modelPayrollBpjsTk->jkn_nominal = $getSetting ? $getSetting->jkm ? round($modelPayrollBpjsTk->gaji_pokok * ($modelPayrollBpjsTk->jkn_persen / 100), 2) : 0 : 0;
        $modelPayrollBpjsTk->jht_persen = $getSetting ? $getSetting->jht ? 3.70 : 0 : 0;
        $modelPayrollBpjsTk->jht_nominal = $getSetting ? $getSetting->jht ? round($modelPayrollBpjsTk->gaji_pokok * ($modelPayrollBpjsTk->jht_persen / 100), 2) : 0 : 0;
        $modelPayrollBpjsTk->jht_persen_tk = $getSetting ? $getSetting->jht ? 2 : 0 : 0;
        $modelPayrollBpjsTk->jht_nominal_tk = $getSetting ? $getSetting->jht ? round($modelPayrollBpjsTk->gaji_pokok * ($modelPayrollBpjsTk->jht_persen_tk / 100), 2) : 0 : 0;
        $modelPayrollBpjsTk->iuran_pensiun_persen = $getSetting ? $getSetting->ip ? 2 : 0 : 0;
        $dasarGajiIp = $modelPayrollBpjsTk->gaji_pokok > 8754600 ? 8754600 : $modelPayrollBpjsTk->gaji_pokok;
        $modelPayrollBpjsTk->iuran_pensiun_nominal = $getSetting ? $getSetting->ip ? round($dasarGajiIp * ($modelPayrollBpjsTk->iuran_pensiun_persen / 100), 2) : 0 : 0;
        $modelPayrollBpjsTk->iuran_pensiun_tk_persen = $getSetting ? $getSetting->ip ? 1 : 0 : 0;
        $modelPayrollBpjsTk->iuran_pensiun_tk_nominal = $getSetting ? $getSetting->ip ? round($modelPayrollBpjsTk->gaji_pokok * ($modelPayrollBpjsTk->iuran_pensiun_tk_persen / 100), 2) : 0 : 0;
        $modelPayrollBpjsTk->save();
    }


    /**
     * @return bool
     * @throws Exception
     */
    public function downloadBpjs(): bool
    {
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $fileName = 'Rekap Data BPJS' . $this->payrollPeriode->namaPeriode . '*' . $this->golongan->nama . '*' . PayrollBpjs::modelPerjanjianKerja()->_kontrak[$this->bisnis_unit];
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->setTitle(Yii::t('frontend', 'BPJS Kesehatan'));

        $activeSheet->getRowDimension('1')->setVisible(false);

        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(30);
        $activeSheet->getColumnDimension('D')->setWidth(15);
        $activeSheet->getColumnDimension('E')->setWidth(15);
        $activeSheet->getColumnDimension('F')->setWidth(16);
        $activeSheet->getColumnDimension('G')->setWidth(15);
        $activeSheet->getColumnDimension('H')->setWidth(15);
        $activeSheet->getColumnDimension('I')->setWidth(15);

        $activeSheet->setCellValue('A1', '');
        $activeSheet->setCellValue('B1', '');
        $activeSheet->mergeCells('A7:A8')->setCellValue('A7', Yii::t('frontend', 'NO'))->getStyle('A7:A8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('B7:B8')->setCellValue('B7', Yii::t('frontend', 'ID Pegawai'))->getStyle('B7:B8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('C7:C8')->setCellValue('C7', Yii::t('frontend', 'Nama'))->getStyle('C7:C8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('D7:D8')->setCellValue('D7', Yii::t('frontend', 'Nomor BPJS'))->getStyle('D7:D8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('E7:E8')->setCellValue('E7', Yii::t('frontend', 'Gaji Pokok'))->getStyle('E7:E8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('F7:F8')->setCellValue('F7', Yii::t('frontend', 'Iuran Perusahaan (4%)'))->getStyle('F7:F8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('G7:G8')->setCellValue('G7', Yii::t('frontend', 'Iuran Karyawan (1%)'))->getStyle('G7:G8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('H7:H8')->setCellValue('H7', Yii::t('frontend', 'Total Iuran'))->getStyle('H7:H8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('I7:I8')->setCellValue('I7', Yii::t('frontend', 'Tgl Resign'))->getStyle('I7:I8')->applyFromArray($headerStyle);

        $no = 0;
        $row = 8;
        foreach ($this->payrollBpjsDetails as $payrollBpjsDetail) {
            $no++;
            $row++;
            $idPegawai = $payrollBpjsDetail->perjanjianKerja ? str_pad($payrollBpjsDetail->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
            $tglResign = $payrollBpjsDetail->perjanjianKerja ? $payrollBpjsDetail->perjanjianKerja->tanggal_resign ? date('d-m-Y', strtotime($payrollBpjsDetail->perjanjianKerja->tanggal_resign)) : '' : '';
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, $idPegawai)->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, $payrollBpjsDetail->pegawai->nama)->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('D' . $row, $payrollBpjsDetail->pegawaiBpjsT ? $payrollBpjsDetail->pegawaiBpjsT->nomor : 0)->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('E' . $row, $payrollBpjsDetail->gaji_pokok)->getStyle('E' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('F' . $row, $payrollBpjsDetail->iuran_nominal)->getStyle('F' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('G' . $row, $payrollBpjsDetail->iuran_nominal_tk)->getStyle('G' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('H' . $row, $payrollBpjsDetail->total_iuran)->getStyle('H' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('I' . $row, $tglResign)->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
        }

        $highestColumn = $activeSheet->getHighestColumn('8');

        $activeSheet->getStyle('A7:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A7:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A7:' . $highestColumn . '8')->applyFromArray($headerStyle);


        //++++++++++++++++++++++++++++++++++++++++++++++++++++++ Ketenagakerjaan +++++++++++++++++++++++++++++++++++++++

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->setTitle(Yii::t('frontend', 'BPJS Ketenagakerjaan'));


        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setWidth(15);
        $activeSheet->getColumnDimension('C')->setWidth(30);
        $activeSheet->getColumnDimension('D')->setWidth(15);
        $activeSheet->getColumnDimension('E')->setWidth(15);
        $activeSheet->getColumnDimension('F')->setWidth(15);
        $activeSheet->getColumnDimension('G')->setWidth(15);
        $activeSheet->getColumnDimension('H')->setWidth(16);
        $activeSheet->getColumnDimension('I')->setWidth(15);
        $activeSheet->getColumnDimension('J')->setWidth(15);
        $activeSheet->getColumnDimension('K')->setWidth(15);
        $activeSheet->getColumnDimension('L')->setWidth(15);
        $activeSheet->getColumnDimension('I')->setWidth(15);

        $activeSheet->setCellValue('A1', '');
        $activeSheet->setCellValue('B1', '');
        $activeSheet->mergeCells('A7:A8')->setCellValue('A7', Yii::t('frontend', 'NO'))->getStyle('A7:A8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('B7:B8')->setCellValue('B7', Yii::t('frontend', 'ID Pegawai'))->getStyle('B7:B8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('C7:C8')->setCellValue('C7', Yii::t('frontend', 'Nama'))->getStyle('C7:C8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('D7:D8')->setCellValue('D7', Yii::t('frontend', 'Nomor BPJS'))->getStyle('D7:D8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('E7:E8')->setCellValue('E7', Yii::t('frontend', 'Gaji Pokok'))->getStyle('E7:E8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('F7:F8')->setCellValue('F7', Yii::t('frontend', $this->bisnis_unit == PerjanjianKerja::KBU ? 'JKK (0.89%)' : 'JKK (0.24%)'))->getStyle('F7:F8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('G7:G8')->setCellValue('G7', Yii::t('frontend', 'JKM (0.30%)'))->getStyle('G7:G8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('H7:H8')->setCellValue('H7', Yii::t('frontend', 'JHT Perusahaan (3.7%)'))->getStyle('H7:H8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('I7:I8')->setCellValue('I7', Yii::t('frontend', 'JHT TK (2%)'))->getStyle('I7:I8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('J7:J8')->setCellValue('J7', Yii::t('frontend', 'IP Perusahaan (2%)'))->getStyle('J7:J8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('K7:K8')->setCellValue('K7', Yii::t('frontend', 'IP TK (1%)'))->getStyle('K7:K8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('L7:L8')->setCellValue('L7', Yii::t('frontend', 'Total Iuran'))->getStyle('L7:L8')->applyFromArray($headerStyle);
        $activeSheet->mergeCells('M7:M8')->setCellValue('M7', Yii::t('frontend', 'Tgl Resign'))->getStyle('L7:L8')->applyFromArray($headerStyle);

        $no = 0;
        $row = 8;
        foreach ($this->payrollBpjsTkDetails as $payrollBpjsTkDetail) {
            $no++;
            $row++;
            $idPegawai = $payrollBpjsTkDetail->perjanjianKerja ? str_pad($payrollBpjsTkDetail->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
            $tglResign = $payrollBpjsTkDetail->perjanjianKerja ? $payrollBpjsTkDetail->perjanjianKerja->tanggal_resign ? date('d-m-Y', strtotime($payrollBpjsTkDetail->perjanjianKerja->tanggal_resign)) : '' : '';
            $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('B' . $row, $idPegawai)->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('C' . $row, $payrollBpjsTkDetail->pegawai->nama)->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
            $activeSheet->setCellValue('D' . $row, $payrollBpjsTkDetail->pegawaiBpjs->nomor)->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
            $activeSheet->setCellValue('E' . $row, $payrollBpjsTkDetail->gaji_pokok)->getStyle('E' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('F' . $row, $payrollBpjsTkDetail->jkk_nominal)->getStyle('F' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('G' . $row, $payrollBpjsTkDetail->jkn_nominal)->getStyle('G' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('H' . $row, $payrollBpjsTkDetail->jht_nominal)->getStyle('H' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('I' . $row, $payrollBpjsTkDetail->jht_nominal_tk)->getStyle('I' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('J' . $row, $payrollBpjsTkDetail->iuran_pensiun_nominal)->getStyle('J' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('K' . $row, $payrollBpjsTkDetail->iuran_pensiun_tk_nominal)->getStyle('K' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('L' . $row, $payrollBpjsTkDetail->total_iuran)->getStyle('L' . $row)->applyFromArray(array_merge($borderInside))->getNumberFormat()->setFormatCode('#,##0');
            $activeSheet->setCellValue('M' . $row, $tglResign)->getStyle('M' . $row)->applyFromArray(array_merge($borderInside));
        }

        $highestColumn = $activeSheet->getHighestColumn('12');

        $activeSheet->getStyle('A7:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
        $activeSheet->getStyle('A7:' . $highestColumn . $row)->applyFromArray($font);
        $activeSheet->getStyle('A7:' . $highestColumn . '8')->applyFromArray($headerStyle);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $fileName . '.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }
}

<?php

namespace frontend\modules\payroll\models;

use Yii;
use \frontend\modules\payroll\models\base\ThrDetail as BaseThrDetail;

/**
 * This is the model class for table "thr_detail".
 */
class ThrDetail extends BaseThrDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['thr_id', 'pegawai_id'], 'required'],
            [['p_gaji_pokok_nominal', 'p_tunjangan_loyalitas_config_nominal', 'p_tunjangan_anak_config_nominal', 'p_tunjangan_lain_tetap_nominal', 'p_tunjangan_jabatan_nominal', 'nominal', 'masa_kerja_bulan', 'masa_kerja_tahun', 'gaji_pokok_thr', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['cut_off', 'tanggal_mulai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['presentase_thr'], 'number'],
            [['id', 'thr_id', 'golongan', 'jenis_perjanjian', 'bisnis_unit', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'p_gaji_pokok_id', 'p_tunjangan_loyalitas_config_id', 'p_tunjangan_anak_config_id', 'p_tunjangan_lain_tetap_id', 'p_tunjangan_jabatan_id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['id_pegawai'], 'string', 'max' => 20],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

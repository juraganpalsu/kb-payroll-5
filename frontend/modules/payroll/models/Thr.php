<?php

namespace frontend\modules\payroll\models;

use frontend\models\Pegawai;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\base\Thr as BaseThr;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "thr".
 *
 *
 * @property array $finishingStatus
 *
 */
class Thr extends BaseThr
{


    const created = 1;
    const processing = 2;
    const finished = 3;

    public $finishingStatus = [self::created => 'CREATED', self::processing => 'PROCESSING', self::finished => 'FINISHED'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['persentase_pembayaran', 'thr_setting_id', 'golongan_id', 'bisnis_unit'], 'required'],
            [['catatan'], 'string'],
            [['persentase_pembayaran', 'tahun', 'golongan_id', 'bisnis_unit', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'thr_setting_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['finishing_status'], 'default', 'value' => self::created],
            [['persentase_pembayaran'], 'default', 'value' => 100],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['bisnis_unit', 'doubleValidation']
        ];
    }

    /**
     * @param string $attribute
     */
    public function doubleValidation(string $attribute)
    {
        if ($this->isNewRecord) {
            $query = self::findOne(['thr_setting_id' => $this->thr_setting_id, 'golongan_id' => $this->golongan_id, 'bisnis_unit' => $this->bisnis_unit]);
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Data THR telah ada.'));
            }
        }
    }

    /**
     * @return PerjanjianKerja
     */
    public static function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($this->thrSetting) {
            $this->tahun = $this->thrSetting->tahun;
        }
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
//    public function afterSave($insert, $changedAttributes)
//    {
//        parent::afterSave($insert, $changedAttributes);
//        if ($insert) {
//            $this->simpanDataThr();
//        }
//
//    }

    /**
     * @param int $masaKerjaTahun
     * @param int $masaKerjaBulan
     * @return float|int
     */
    public function ketentuanThr(int $masaKerjaTahun, int $masaKerjaBulan)
    {
        if ($masaKerjaTahun == 0) {
            if ($masaKerjaBulan == 0) {
                $persentase = 0;
            } else {
                $persentase = $masaKerjaBulan / 12;
            }
        } else {
            if ($masaKerjaTahun >= 1 && $masaKerjaTahun < 3) {
                $persentase = 1;
            } elseif ($masaKerjaTahun >= 3 && $masaKerjaTahun < 5) {
                $persentase = 1.04;
            } elseif ($masaKerjaTahun >= 5 && $masaKerjaTahun < 7) {
                $persentase = 1.06;
            } elseif ($masaKerjaTahun >= 7 && $masaKerjaTahun < 15) {
                $persentase = 1.08;
            } else {
                $persentase = 1.1;
            }
        }
        return $persentase;
    }

    /**
     * @return array
     */
    public function hitungThr(): array
    {
        $tanggal = $this->thrSetting->cut_off_resign;
        $query = \frontend\modules\pegawai\models\Pegawai::find()
            ->joinWith(['pegawaiGolongans' => function (ActiveQuery $query) use ($tanggal) {
                $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);
                $query->andWhere('pegawai_golongan.akhir_berlaku >= \'' . $tanggal . '\'' . ' OR ' . 'pegawai_golongan.akhir_berlaku is null');
            }])
            ->joinWith(['perjanjianKerjas' => function (ActiveQuery $query) use ($tanggal) {
                $query->andWhere(['kontrak' => $this->bisnis_unit]);
                $query->andWhere('perjanjian_kerja.akhir_berlaku >= \'' . $tanggal . '\'' . ' OR ' . 'perjanjian_kerja.akhir_berlaku is null');

            }]);
        $query->orderBy('pegawai.nama_lengkap ASC');

        $pegawais = $query->all();

        $datas = [];
        /** @var Pegawai $pegawai */
        foreach ($pegawais as $pegawai) {
            $masterData = $pegawai->masterDataDefaults;
            if (empty($masterData)) {
                continue;
            }
            if (!$masterData->thr) {
                continue;
            }

//            $pkCutOffResign = PerjanjianKerja::getPerjanjianDasarStatic($pegawai->id);
//            $pkCutOffThr = PerjanjianKerja::getPerjanjianDasarStatic($pegawai->id);
            $dasarMasaKerja = PerjanjianKerja::getPerjanjianDasarStatic($pegawai->id);
            if (empty($dasarMasaKerja)) {
                continue;
            }
            $tglCutOff = $this->thrSetting->cut_off_thr;
            $tglResign = '';
            $queryPerjanjianKerja = PerjanjianKerja::find()
                ->andWhere(['pegawai_id' => $pegawai->id])
                ->andWhere(['>=', 'mulai_berlaku', $dasarMasaKerja->mulai_berlaku])
                ->andWhere(['!=', 'tanggal_resign', ''])
                ->orderBy('mulai_berlaku DESC')
                ->one();
            if (!empty($queryPerjanjianKerja)) {
                $tglResign = $queryPerjanjianKerja['tanggal_resign'];
            }
            if (!empty($tglResign)) {
                if ($tglResign >= $this->thrSetting->cut_off_resign && $tglResign <= $this->thrSetting->cut_off_thr) {
                    $tglCutOff = $tglResign;
                }
            }

            $pgCutOffResign = $pegawai->getPegawaiGolonganUntukTanggal($this->thrSetting->cut_off_resign);
            $pgCutOffThr = $pegawai->getPegawaiGolonganUntukTanggal($this->thrSetting->cut_off_thr);

            if ($dasarMasaKerja && ($pgCutOffResign || $pgCutOffThr)) {

//                $cutOffPk = $pkCutOffResign;
//                if ($pkCutOffThr) {
//                    $cutOffPk = $pkCutOffThr;
//                }

                $cutOffPg = $pgCutOffResign;
                if ($pgCutOffThr) {
                    $cutOffPg = $pgCutOffThr;
                }

                $gajiPokok = 0;
                $gajiPokokId = '';
                $tLainTetap = 0;
                $tLainTetapId = '';
                $insentifTetap = 0;
                $insentifTetapId = '';
                $tJabatan = 0;
                $tJabatanId = '';

                $gp = $masterData->getGajiPokokAktif(null, true);
                if ($gp) {
                    $gajiPokok = $gp->nominal;
                    $gajiPokokId = $gp->id;
                }
                $tlt = $masterData->getTunjanganLainTetap(null, true);
                if ($tlt) {
                    $tLainTetap = $tlt->nominal;
                    $tLainTetapId = $tlt->id;
                }
                //insentif tetap
                $it = $masterData->getInsentifTetap(null, true);
                if ($it) {
                    $insentifTetap = $it->nominal;
                    $insentifTetapId = $it->id;
                }
                $tj = $masterData->getTunjJabatanAktif(null, true);
                if ($tj) {
                    $tJabatan = $tj->nominal;
                    $tJabatanId = $tj->id;
                }

                $tLoyalitas = 0;
                $tLoyalitasId = '';
//                $masaKerja = 0;
//                if ($pkCutOffResign) {
//                    $tglMulaiBerlaku = $pkCutOffResign->mulai_berlaku;
//                    $tglCutOff = $this->thrSetting->cut_off_resign;
//                    $masaKerja = $dasarMasaKerja->hitungMasaKerja('', $this->thrSetting->cut_off_resign, true);
//                }
//                if ($pkCutOffThr) {
//                    $tglMulaiBerlaku = $pkCutOffThr->mulai_berlaku;
//                    $tglCutOff = $this->thrSetting->cut_off_thr;
                $masaKerja = $dasarMasaKerja->hitungMasaKerja('', $tglCutOff, true);
                $perjanjianKerjaSaatIni = $pegawai->getPerjanjianKerjaUntukTanggal($tglCutOff);
//                }
                $tl = MasterDataDefault::getTunjanganLoyalitas($masaKerja['tahun'], true);
                if ($tl && $masterData->tunjangan_loyalitas) {
                    $tLoyalitas = $tl->nominal;
                    $tLoyalitasId = $tl->id;
                }

                $tAnak = 0;
                $tAnakId = '';
                $ta = MasterDataDefault::getTunjanganAnak($pegawai->getJumlahAnak(), true);
                if ($pegawai->status_pernikahan != \frontend\modules\pegawai\models\Pegawai::BELUM_MENIKAH && $masaKerja['dalamBulan'] >= 6 && $ta && $masterData->tunjangan_anak) {
                    $tAnak = $ta->nominal;
                    $tAnakId = $ta->id;
                }

                //menggunakan pengalian persentase on the fly
                //Kedepannya agar lebih meyakinkan, simpan semua komponen
                //tanggal mulai, cut off, masa kerja(bulan & tahun), persentase
                //yg dikalikan hanya gaji pokoknya saja
                $presentaseThr = $this->ketentuanThr($masaKerja['tahun'], $masaKerja['bulan']);
                $gajiPokokThr = $gajiPokok + $tLoyalitas + $tAnak + $tLainTetap + $tJabatan + $insentifTetap;

                $nominal = $gajiPokokThr * $presentaseThr;
                $nominal = $nominal * ($this->persentase_pembayaran / 100);

                $datas[$pegawai->id] = [
                    'id' => $pegawai->id,
                    'nama' => $pegawai->nama,
                    'id_pegawai' => $perjanjianKerjaSaatIni ? $perjanjianKerjaSaatIni->id_pegawai : '-',
                    'bisnis_unit' => $perjanjianKerjaSaatIni ? $perjanjianKerjaSaatIni->namaKontrak : $this->bisnis_unit,
                    'jenis_perjanjian' => $perjanjianKerjaSaatIni ? $perjanjianKerjaSaatIni->jenis->nama : '-',
                    'golongan' => $cutOffPg->golongan->nama,
                    'gaji_pokok' => $gajiPokok,
                    'gaji_pokok_id' => $gajiPokokId,
                    'gaji_pokok_thr' => round($gajiPokokThr),
                    'tunjangan_loyalitas' => $tLoyalitas,
                    'tunjangan_loyalitas_id' => $tLoyalitasId,
                    'tunjangan_anak' => $tAnak,
                    'tunjangan_anak_id' => $tAnakId,
                    'tunjangan_lain_tetap' => $tLainTetap,
                    'tunjangan_lain_tetap_id' => $tLainTetapId,
                    'insentif_tetap' => $insentifTetap,
                    'insentif_tetap_id' => $insentifTetapId,
                    'tunjangan_jabatan' => $tJabatan,
                    'tunjangan_jabatan_id' => $tJabatanId,
                    'nominal' => round($nominal),
                    'tanggal_mulai' => $dasarMasaKerja->mulai_berlaku,
                    'masa_kerja_tahun' => $masaKerja['tahun'],
                    'masa_kerja_bulan' => $masaKerja['bulan'],
                    'presentase_thr' => $presentaseThr,
                    'cut_off' => $tglCutOff
                ];
            }
        }
        return $datas;
    }

    /**
     * @return void
     */
    public function simpanDataThr()
    {
        $hitungThr = $this->hitungThr();
        if ($hitungThr) {
            foreach ($hitungThr as $thr) {
                $modelThrDetail = new ThrDetail();
                $modelThrDetail->nominal = $thr['nominal'];
                $modelThrDetail->thr_id = $this->id;
                $modelThrDetail->pegawai_id = $thr['id'];
                $modelThrDetail->p_gaji_pokok_id = $thr['gaji_pokok_id'];
                $modelThrDetail->p_gaji_pokok_nominal = $thr['gaji_pokok'];
                $modelThrDetail->p_tunjangan_loyalitas_config_id = $thr['tunjangan_loyalitas_id'];
                $modelThrDetail->p_tunjangan_loyalitas_config_nominal = $thr['tunjangan_loyalitas'];
                $modelThrDetail->p_tunjangan_anak_config_id = $thr['tunjangan_anak_id'];
                $modelThrDetail->p_tunjangan_anak_config_nominal = $thr['tunjangan_anak'];
                $modelThrDetail->p_tunjangan_lain_tetap_id = $thr['tunjangan_lain_tetap_id'];
                $modelThrDetail->p_tunjangan_lain_tetap_nominal = $thr['tunjangan_lain_tetap'];
                $modelThrDetail->p_tunjangan_jabatan_id = $thr['tunjangan_jabatan_id'];
                $modelThrDetail->p_tunjangan_jabatan_nominal = $thr['tunjangan_jabatan'];
                $modelThrDetail->id_pegawai = $thr['id_pegawai'];
                $modelThrDetail->nama = $thr['nama'];
                $modelThrDetail->bisnis_unit = $thr['bisnis_unit'];
                $modelThrDetail->jenis_perjanjian = $thr['jenis_perjanjian'];
                $modelThrDetail->golongan = $thr['golongan'];
                $modelThrDetail->gaji_pokok_thr = $thr['gaji_pokok_thr'];
                $modelThrDetail->tanggal_mulai = $thr['tanggal_mulai'];
                $modelThrDetail->masa_kerja_tahun = $thr['masa_kerja_tahun'];
                $modelThrDetail->masa_kerja_bulan = $thr['masa_kerja_bulan'];
                $modelThrDetail->presentase_thr = $thr['presentase_thr'];
                $modelThrDetail->cut_off = $thr['cut_off'];

//                if (empty($modelThrDetail->p_gaji_pokok_id)) {
//                    Yii::info($this->thrSetting->tahun . '*' . $this->golongan->nama . '*' . Thr::modelPerjanjianKerja()->_kontrak[$this->bisnis_unit] . '->' . $modelThrDetail->pegawai->nama . '->' . $modelThrDetail->pegawai_id, 'simpan-thr');
//                    continue;
//                }
                $check = ThrDetail::findOne(['thr_id' => $this->id, 'pegawai_id' => $modelThrDetail->pegawai_id]);
                if ($check) {
                    continue;
                }
                if (!$modelThrDetail->save()) {
                    Yii::info($modelThrDetail->errors, 'simpan-thr');
                }
            }
        }
    }

    /**
     * @return bool|int|string|null
     */
    public function hitungJumlahPegawai()
    {
        return ThrDetail::find()->andWhere(['thr_id' => $this->id])->count();
    }


}

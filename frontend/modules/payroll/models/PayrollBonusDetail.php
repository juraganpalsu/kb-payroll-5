<?php

namespace frontend\modules\payroll\models;

use Yii;
use \frontend\modules\payroll\models\base\PayrollBonusDetail as BasePayrollBonusDetail;

/**
 * This is the model class for table "payroll_bonus_detail".
 */
class PayrollBonusDetail extends BasePayrollBonusDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'payroll_bonus_id'], 'required'],
            [['nominal', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'payroll_bonus_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

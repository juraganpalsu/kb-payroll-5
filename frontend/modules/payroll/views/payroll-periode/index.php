<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\PayrollPeriodeSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\payroll\models\PayrollPeriode;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Payroll Periode');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-periode-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'tipe',
            'value' => function (PayrollPeriode $model) {
                return $model->_tipe()[$model->tipe];
            },
            'filter' => Html::activeDropDownList($searchModel, 'tipe', $searchModel->_tipe(), ['class' => 'form-control', 'prompt' => '*']),
        ],
        [
            'attribute' => 'tanggal_awal',
            'label' => Yii::t('frontend', 'Tanggal Awal'),
            'value' => function (PayrollPeriode $model) {
                return date('d-m-Y', strtotime($model->tanggal_awal));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'tanggal_akhir',
            'label' => Yii::t('frontend', 'Tanggal Alkhir'),
            'value' => function (PayrollPeriode $model) {
                return date('d-m-Y', strtotime($model->tanggal_akhir));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        'catatan:ntext',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}'
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3],
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'peraturan-perusahaan' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payroll-periode']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
</div>

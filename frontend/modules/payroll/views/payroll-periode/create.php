<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollPeriode */

$this->title = Yii::t('frontend', 'Create Payroll Periode');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Periode'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-periode-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

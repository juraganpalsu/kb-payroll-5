<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollPeriode */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Payroll Periode',
    ]) . ' ' . $model->tipe[$model->tipe];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Periode'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->namaPeriode, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="payroll-periode-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

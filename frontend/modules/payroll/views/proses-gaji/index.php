<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\ProsesGajiSearch */

/**
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var string $index
 * @var string $finish
 */

use common\models\Golongan;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\ProsesGaji;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Proses Gaji - KBU');
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {    
    $('.set-finish').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>
<div class="proses-gaji-index">
    <p>
        <?= Html::a('<i class="fa fa-hourglass-2"></i>' . Yii::t('app', ' Dibuat'), $index, ['class' => 'btn btn-info btn-flat']) ?>
        <?= Html::a('<i class="fa fa-check-square-o"></i>' . Yii::t('app', ' Selesai'), $finish, ['class' => 'btn btn-info btn-flat']) ?>
    </p>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'payroll_periode_id',
            'label' => Yii::t('frontend', 'Payroll Periode'),
            'value' => function (ProsesGaji $model) {
                return Html::a($model->payrollPeriode->namaPeriode, ['view', 'id' => $model->id], ['title' => 'View All Data', 'data' => ['pjax' => 0]]);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(PayrollPeriode::find()->all(), 'id', 'namaPeriode'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Payroll periode', 'id' => 'grid-proses-gaji-search-payroll_periode_id'],
            'format' => 'raw'
        ],
        [
            'attribute' => 'golongan_id',
            'label' => Yii::t('frontend', 'Golongan'),
            'value' => function (ProsesGaji $model) {
                return $model->golongan->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Golongan', 'id' => 'grid-thr-search-golongan_id']
        ],
        [
            'attribute' => 'bisnis_unit',
            'value' => function (ProsesGaji $model) {
                return ProsesGaji::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ProsesGaji::modelPerjanjianKerja()->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Bisnis Unit'), 'id' => 'grid-thr-search-bisnis_unit']
        ],
        [
            'label' => Yii::t('frontend', 'Jumlah'),
            'value' => function (ProsesGaji $model) {
                return $model->hitungJumlahPegawai();
            },
        ],
        'catatan:ntext',
        [
            'attribute' => 'status',
            'label' => Yii::t('frontend', 'Proses Status'),
            'value' => function (ProsesGaji $model) {
                return $model->status > 0 ? $model->_status[$model->status] : '';
            }
        ],
        [
            'attribute' => 'finishing_status',
            'value' => function (ProsesGaji $model) {
                if ($model->finishing_status == ProsesGaji::dibuat && $model->created_by == Yii::$app->user->id) {
                    return Html::a($model->_status[$model->finishing_status], ['set-finishing-status', 'id' => $model->id, 'status' => ProsesGaji::selesai], ['class' => 'btn btn-xs btn-block btn-warning set-finish', 'data' => ['confirm' => Yii::t('frontend', 'Apakah Anda Yakin proses gaji ini sudah Finish ??'), 'method' => 'post']]);
                }
                return Html::a($model->_status[$model->finishing_status], ['#'], ['class' => 'btn btn-xs btn-block btn-success']);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'created_by',
            'value' => function (ProsesGaji $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        'created_at',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{proses-ulang} {slips} {delete}',
            'buttons' => [
                'proses-ulang' => function ($url, ProsesGaji $model) {
                    if ($model->finishing_status == ProsesGaji::dibuat && $model->status == ProsesGaji::selesai) {
                        return Html::a(Yii::t('frontend', 'Proses Ulang'), $url, ['class' => 'btn btn-xs btn-info btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan memproses ulang data ini ??'), 'method' => 'post']]);
                    }
                    return '';
                },
                'delete' => function ($url, ProsesGaji $model) {
                    if ($model->finishing_status != ProsesGaji::selesai) {
                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                    }
                    return '';
                },
                'slips' => function ($url, ProsesGaji $model) {
                    if ($model->finishing_status == ProsesGaji::selesai) {
                        return Html::a(Yii::t('frontend', 'Print Slip'), $url, ['class' => 'btn btn-xs btn-warning btn-flat', 'target' => '_blank', 'data' => ['pjax' => 0]]);
                    }
                    return '';
                },
            ],
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'proses-gaji' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proses-gaji']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
</div>
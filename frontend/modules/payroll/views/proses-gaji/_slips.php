<?php

use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\ProsesGaji;
use frontend\modules\pegawai\models\PerjanjianKerja;
use yii\helpers\Html;
use yii\web\View;

/**
 * Created by PhpStorm.
 * File Name: _slips.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 21/06/20
 * Time: 23.19
 */

/**
 * @var View $this
 * @var ProsesGaji $model
 */


foreach ($model->gajiBulanans as $gajiBulanan) {
$subTotal = 0;
$subTotal2 = 0;
$Total = 0;
?>
<div class="slip-gaji-index">
    <table width="200mm" style="page-break-after: always; border: 2px solid black">
        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td class="dept" colspan="4" rowspan="4" style="text-align: center">
                <div>
                    <?php
                    if ($model->bisnis_unit == PerjanjianKerja::KBU) {
                        echo Html::img('@frontend/web/pictures/image001.png', ['height' => '60px']);
                    } elseif ($model->bisnis_unit == PerjanjianKerja::ADA) {
                        echo Html::img('@frontend/web/pictures/image002.png', ['height' => '60px']);
                    } else {
                        echo Html::img('@frontend/web/pictures/image003.png', ['height' => '60px']);
                    }
                    ?>
                </div>
            </td>
            <td colspan="14" style="  border: 0;">&nbsp;</td>
            <td colspan="6" class="nodok">No. Kode.Dok</td>
        </tr>
        <tr>
            <th rowspan="2" colspan="14" style="font-size: 12pt">
                <strong>SLIP GAJI KARYAWAN</strong></th>
            </th>
            <td class="nodok" colspan="6">KBU/FO/HRD/15/024</td>
        </tr>
        <tr>
            <td class="nodok" colspan="6">Tingkatan dokument : V</td>
        </tr>
        <tr>
            <th colspan="14" style="font-size: 12pt"><?php
                if ($model->bisnis_unit == PerjanjianKerja::KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($model->bisnis_unit == PerjanjianKerja::ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                } else {
                    echo 'PT BINA CIPTA ABADI';
                }
                ?>
            <td class="nodok" colspan="6">Status Revisi : 00</td>
        </tr>
        <tr>
            <th class="dept" colspan="4">HC</th>
            <td colspan="14">&nbsp;</td>
            <td class="nodok" colspan="6">Tgl. Mulai berlaku : 8 Juni 2015</td>
        </tr>
        <tr>
            <th class="dept" colspan="4">Departement</th>
            <td colspan="14">&nbsp;</td>
            <td class="nodok" colspan="6">Halaman : 1 dari 1</td>
        </tr>

        <tr>
            <td class="top" style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt"
                colspan="2"><?= Yii::t('frontend', 'NIK') ?></td>
            <td class="top" style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" colspan="6">
                : <?= str_pad($gajiBulanan->id_pegawai, 8, '0', STR_PAD_LEFT) ?></td>
            <td class="top" style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt"
                colspan="2"><?= Yii::t('frontend', 'Dept') ?></td>
            <td class="top" style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" colspan="6">
                : <?= $gajiBulanan->pegawaiStruktur->struktur->departemen->nama; ?></td>
            <td class="top" style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt"
                colspan="2"><?= Yii::t('frontend', 'Gaji bulan') ?> </td>
            <td class="top" style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" colspan="6">
                : <?= date('F - Y', strtotime($gajiBulanan->prosesGaji->payrollPeriode->tanggal_akhir)); ?></td>
        </tr>
        <tr>
            <td style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" class="bottom"
                colspan="2"><?= Yii::t('frontend', 'Nama') ?></td>
            <td style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" class="bottom" colspan="6">
                : <?= $gajiBulanan->nama_lengkap ?></td>
            <td style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" class="bottom"
                colspan="2"><?= Yii::t('frontend', 'No Rek') ?></td>
            <td style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" class="bottom" colspan="6">
                : <?= $gajiBulanan->pegawai_bank_nomor ?></td>
            <td style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" class="bottom"
                colspan="2"><?= Yii::t('frontend', 'Tanggal') ?> </td>
            <td style="background-color: #b5bbc8;font-weight: bold; font-size: 10pt" class="bottom" colspan="6">
                : <?= date('d-m-Y'); ?></td>
        </tr>
        <tr>
            <td colspan="24">&nbsp;</td>
        </tr>
        <?php if ($gajiBulanan->p_gaji_pokok > 0 && !in_array($gajiBulanan->tipe_gaji_pokok, [MasterDataDefault::HARIAN, MasterDataDefault::HARIAN_B])) { ?>
            <tr>
                <td colspan="9"><?= $gajiBulanan->getAttributeLabel('gaji'); ?></td>
                <td class="center"
                    colspan="3"><?= $gajiBulanan->jumlahAlfa() != 0 ? $gajiBulanan->jumlahAlfa() : '' ?></td>
                <td colspan="9"><?= $gajiBulanan->jumlahAlfa() != 0 ? Yii::t('frontend', 'Absen') : '' ?></td>
                <td colspan="3">
                    <?php
                    $gajiPokok = $gajiBulanan->totalGajiPokok();
                    $subTotal = $subTotal + (int)$gajiPokok;
                    echo Yii::$app->formatter->asDecimal($gajiPokok)
                    ?>
                </td>
            </tr>
        <?php } ?>
        <?php if (in_array($gajiBulanan->tipe_gaji_pokok, [MasterDataDefault::HARIAN, MasterDataDefault::HARIAN_B])) { ?>
            <tr>
                <td colspan="9"><?= $gajiBulanan->getAttributeLabel('gaji / jumlah_hadir'); ?></td>
                <td class="center"
                    colspan="3"><?= $gajiBulanan->jumlahHadir() ?></td>
                <td colspan="9"><?= Yii::t('frontend', 'Hari') ?></td>
                <td colspan="3">
                    <?php
                    $gajiPokok = $gajiBulanan->totalGajiPokok();
                    $subTotal = $subTotal + (int)$gajiPokok;
                    echo Yii::$app->formatter->asDecimal($gajiPokok)
                    ?>
                </td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_premi_hadir > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="9"><?= $gajiBulanan->getAttributeLabel('p_premi_hadir'); ?></td>
                <td class="center" colspan="3"><?= $gajiBulanan->jumlah_telat ?></td>
                <td colspan="9"><?= Yii::t('frontend', 'Telat') ?></td>
                <td colspan="3"><?php
                    $premihadir = $gajiBulanan->premiHadir();
                    $subTotal = $subTotal + (int)$premihadir;
                    echo Yii::$app->formatter->asDecimal($premihadir); ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_uang_makan > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="9"><?= $gajiBulanan->getAttributeLabel('p_uang_makan'); ?></td>
                <td class="center" colspan="3"><?= $gajiBulanan->totalHariUangMakan() ?></td>
                <td colspan="3"><?= Yii::t('frontend', 'Hari x') ?></td>
                <td colspan="6"><?= Yii::$app->formatter->asDecimal($gajiBulanan->p_uang_makan) ?></td>
                <td colspan="3"><?php
                    $totalUangMakan = $gajiBulanan->hitungTotalUangMakan();
                    $subTotal = $subTotal + $totalUangMakan;
                    echo Yii::$app->formatter->asDecimal($totalUangMakan);
                    ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_uang_transport > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="9"><?= $gajiBulanan->getAttributeLabel('p_uang_transport'); ?></td>
                <td class="center" colspan="3"><?= $gajiBulanan->totalHariUangTransport() ?></td>
                <td colspan="3"><?= Yii::t('frontend', 'Hari x') ?></td>
                <td colspan="6"><?= Yii::$app->formatter->asDecimal($gajiBulanan->p_uang_transport) ?></td>
                <td colspan="3"><?php
                    $totalUangTransport = $gajiBulanan->hitungTotalUangTransport();
                    $subTotal = $subTotal + $totalUangTransport;
                    echo Yii::$app->formatter->asDecimal($totalUangTransport);
                    ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->jam_lembur > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="9"><?= $gajiBulanan->getAttributeLabel('uang_lembur'); ?></td>
                <td class="center" colspan="3"><?= $gajiBulanan->jam_lembur ?></td>
                <td colspan="3"><?= Yii::t('frontend', 'Jam x') ?></td>
                <td colspan="6"></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->uang_lembur;
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->uang_lembur);
                    ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->insentif_lembur > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="9"><?= $gajiBulanan->getAttributeLabel('insentif_lembur'); ?></td>
                <td colspan="12"></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->insentif_lembur;
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->insentif_lembur);
                    ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->hitungTotalUangShift() > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="9"><?= $gajiBulanan->getAttributeLabel('total_us'); ?></td>
                <td class="center" colspan="3"><?= $gajiBulanan->s2 + $gajiBulanan->s3 ?></td>
                <td colspan="9"><?= Yii::t('frontend', 'Hari') ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->hitungTotalUangShift();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->hitungTotalUangShift()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->uml > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('uml'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->totalUml();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->totalUml()); ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_tunjangan_jabatan > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_tunjangan_jabatan'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->tunjJabatan();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->tunjJabatan()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_insentif_tetap > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_insentif_tetap'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->insetifTetap();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->insetifTetap()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_tunjangan_pulsa > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_tunjangan_pulsa'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->tunjPulsa();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->tunjPulsa()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->tunjPulsaWfh() > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= Yii::t('frontend', 'Pulsa WFH') ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->tunjPulsaWfh();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->tunjPulsaWfh()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_sewa_mobil > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_sewa_mobil'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->sewaMobil();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->sewaMobil()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_sewa_motor > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_sewa_motor'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->sewaMotor();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->sewaMotor()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_sewa_rumah > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_sewa_rumah'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->sewaRumah();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->sewaRumah()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_tunjagan_lain_tetap > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_tunjagan_lain_tetap'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->tunjLainTetap();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->tunjLainTetap()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_tunjangan_kost > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_tunjangan_kost'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->tunjKost();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->tunjKost()) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_admin_bank > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_admin_bank'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->p_admin_bank;
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->p_admin_bank) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_tunjangan_anak > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_tunjangan_anak'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->p_tunjangan_anak;
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->p_tunjangan_anak) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->p_tunjangan_loyalitas > 0) { ?>
            <tr style="height: 27px;">
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('p_tunjangan_loyalitas'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->p_tunjangan_loyalitas;
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->p_tunjangan_loyalitas) ?></td>
            </tr>
        <?php } ?>
        <!--        --><?php //if ($gajiBulanan->potonganPph() != 0 ) { ?>
        <!--        <tr>-->
        <!--            <td colspan="21">--><? //= $gajiBulanan->getAttributeLabel('tunj_pph'); ?><!--</td>-->
        <!--            <td colspan="3">-->
        <!--                --><?php
        //                //                $subTotal = $subTotal + $gajiBulanan->potPph();
        //                echo Yii::$app->formatter->asDecimal($gajiBulanan->potonganPph()) ?>
        <!--            </td>-->
        <!--        </tr>-->
        <!--                --><?php //} ?>
        <?php if ($gajiBulanan->insentif_tidak_tetap > 0) { ?>
            <tr>
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('insentif_tidak_tetap'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->insentif_tidak_tetap;
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->insentif_tidak_tetap) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->insentif_gt > 0) { ?>
            <tr>
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('insentif_gt'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->insentif_gt;
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->insentif_gt) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->rapel > 0) { ?>
            <tr>
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('rapel'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->rapel;
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->rapel) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->tunjLainLain() > 0) { ?>
            <tr>
                <td colspan="21"><?= $gajiBulanan->getAttributeLabel('lain_lain'); ?></td>
                <td colspan="3"><?php
                    $subTotal = $subTotal + $gajiBulanan->tunjLainLain();
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->tunjLainLain()) ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="13"></td>
            <th align="left" class="subtotal" colspan="8"><?= Yii::t('frontend', 'Sub Total') ?></th>
            <td class="subtotal" colspan="3"><?= Yii::$app->formatter->asDecimal($subTotal) ?></td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <th colspan="7"
                style="text-align: center; border: 1px solid black"><?= Yii::t('frontend', 'Dibuat Oleh') ?></th>
            <td colspan="2"></td>
            <th align="left" colspan="8"><?= Yii::t('frontend', 'Potongan') ?></th>
            <td colspan="3"></td>
        </tr>
        <?php if ($gajiBulanan->potonganBpjs() > 0) { ?>
            <tr>
                <td colspan="4"></td>
                <td class="left right" colspan="7"></td>
                <td colspan="2"></td>
                <td align="left" colspan="8"><?= $gajiBulanan->getAttributeLabel('pot_bpjs') ?></td>
                <td colspan="3"><?php
                    $potonganBpjs = $gajiBulanan->potonganBpjs();
                    $subTotal2 = $subTotal2 + (int)$potonganBpjs;
                    echo Yii::$app->formatter->asDecimal($potonganBpjs) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->pinjaman_potongan_nominal > 0) { ?>
            <tr>
                <td colspan="4"></td>
                <td class="left right" colspan="7"></td>
                <td colspan="2"></td>
                <td align="left" colspan="8"><?= $gajiBulanan->getAttributeLabel('pot_gaji') ?></td>
                <td colspan="3"><?php
                    $potgaji = $gajiBulanan->pinjaman_potongan_nominal;
                    $subTotal2 = $subTotal2 + (int)$potgaji;
                    echo Yii::$app->formatter->asDecimal($potgaji) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->potonganATL() > 0) { ?>
            <tr>
                <td colspan="4"></td>
                <td class="left right" colspan="7"></td>
                <td colspan="2"></td>
                <td align="left" colspan="8"><?= $gajiBulanan->getAttributeLabel('pot_atl') ?></td>
                <td colspan="3"><?php
                    $potgaji = $gajiBulanan->potonganATL();
                    $subTotal2 = $subTotal2 + (int)$potgaji;
                    echo Yii::$app->formatter->asDecimal($potgaji) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->pinjaman_nominal > 0) { ?>
            <tr>
                <td colspan="4"></td>
                <td class="left right" colspan="7"></td>
                <td colspan="2"></td>
                <td align="left" colspan="8"><?= $gajiBulanan->getAttributeLabel('pinjaman') ?></td>
                <td colspan="3"><?php
                    $pinjaman = $gajiBulanan->pinjaman_nominal;
                    $subTotal2 = $subTotal2 + (int)$pinjaman;
                    echo Yii::$app->formatter->asDecimal($pinjaman) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->pinjaman_koperasi_nominal > 0) { ?>
            <tr>
                <td colspan="4"></td>
                <td class="left right" colspan="7"></td>
                <td colspan="2"></td>
                <td align="left" colspan="8"><?= $gajiBulanan->getAttributeLabel('koperasi') ?></td>
                <td colspan="3"><?php
                    $koperasi = $gajiBulanan->pinjaman_koperasi_nominal;
                    $subTotal2 = $subTotal2 + (int)$koperasi;
                    echo Yii::$app->formatter->asDecimal($koperasi) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->pinjaman_bank_nominal > 0) { ?>
            <tr>
                <td colspan="4"></td>
                <td class="left right" colspan="7"></td>
                <td colspan="2"></td>
                <td align="left" colspan="8"><?= $gajiBulanan->getAttributeLabel('bank') ?></td>
                <td colspan="3"><?php
                    $bank = $gajiBulanan->pinjaman_bank_nominal;
                    $subTotal2 = $subTotal2 + (int)$bank;
                    echo Yii::$app->formatter->asDecimal($bank) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->pinjaman_kasbon_nominal > 0) { ?>
            <tr>
                <td colspan="4"></td>
                <td class="left right" colspan="7"></td>
                <td colspan="2"></td>
                <td align="left" colspan="8"><?= $gajiBulanan->getAttributeLabel('kasbon') ?></td>
                <td colspan="3"><?php
                    $kasbon = $gajiBulanan->pinjaman_kasbon_nominal;
                    $subTotal2 = $subTotal2 + (int)$kasbon;
                    echo Yii::$app->formatter->asDecimal($kasbon) ?></td>
            </tr>
        <?php } ?>
        <?php if ($gajiBulanan->pinjaman_lain_lain_nominal > 0) { ?>
            <tr>
                <td colspan="4"></td>
                <td class="left right" colspan="7"></td>
                <td colspan="2"></td>
                <td align="left" colspan="8"><?= $gajiBulanan->getAttributeLabel('pot_lain') ?></td>
                <td colspan="3"><?php
                    $potlain = $gajiBulanan->pinjaman_lain_lain_nominal;
                    $subTotal2 = $subTotal2 + (int)$potlain;
                    echo Yii::$app->formatter->asDecimal($potlain) ?></td>
            </tr>
        <?php } ?>
        <!--        --><?php //if ($gajiBulanan->pot_pph != 0 ) { ?>
        <!--        <tr>-->
        <!--            <td colspan="4"></td>-->
        <!--            <td class="left right" colspan="7"></td>-->
        <!--            <td colspan="2"></td>-->
        <!--            <td colspan="8">--><? //= $gajiBulanan->getAttributeLabel('pot_pph') ?><!--</td>-->
        <!--            <td colspan="3">-->
        <!--                --><?php
        //                $potPph = $gajiBulanan->potonganPph();
        //                $subTotal2 = $subTotal2 + (int)$potPph;
        //                echo Yii::$app->formatter->asDecimal($potPph) ?>
        <!--            </td>-->
        <!--        </tr>-->
        <!--                --><?php //} ?>
        <tr>
            <td colspan="4"></td>
            <td colspan="7"
                style="text-align: center; font-weight: bold; border: 1px solid black;"></td>
            <td colspan="2"></td>
            <th align="left" class="subtotal" colspan="8"><?= Yii::t('frontend', 'Sub Total') ?></th>
            <td class="subtotal" colspan="3"><?= Yii::$app->formatter->asDecimal($subTotal2) ?></td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td colspan="7"
                style="text-align: center; font-weight: bold; border: 1px solid black;"><?= Yii::t('frontend', 'HC Dept.') ?></td>
            <td colspan="13"></td>
        </tr>
        <?php if ($gajiBulanan->insentif_pph > 0) { ?>
            <tr>
                <td colspan="13"></td>
                <th align="left" colspan="7"><?= Yii::t('frontend', 'Insentif PPH') ?></th>
                <td colspan="4"><?php
                    echo Yii::$app->formatter->asDecimal($gajiBulanan->insentif_pph) ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="13"></td>
            <th align="left" class="total" colspan="7"><?= Yii::t('frontend', 'Total') ?></th>
            <td class="total" colspan="4"><?php
                $Total = $subTotal - $subTotal2 + $gajiBulanan->insentif_pph;
                echo Yii::$app->formatter->asDecimal(round($Total)) ?></td>
        </tr>

    </table>
    <?php } ?>
</div>
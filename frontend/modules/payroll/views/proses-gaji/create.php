<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\ProsesGaji */

$this->title = Yii::t('frontend', 'Create Proses Gaji');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Proses Gaji'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proses-gaji-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

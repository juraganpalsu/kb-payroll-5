<?php

/**
 * Created by PhpStorm.
 * File Name: _slips-header.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 21/06/20
 * Time: 23.20
 */

use yii\helpers\Html;

?>

<table width="200mm">
    <tr>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="6" rowspan="4">
            <div><?php echo Html::img('@frontend/web/pictures/image001.png', ['height' => '40px']); ?></div>
        </td>
        <td colspan="12" style="  border: 0;">&nbsp;</td>
        <td colspan="6" class="nodok">No. Kode.Dok</td>
    </tr>
    <tr>
        <th colspan="12">PT ARTA DWITUNGGAL ABADI</th>
        <td class="nodok" colspan="6">KBU/FO/HRD/15/024</td>
    </tr>
    <tr>
        <td colspan="12">&nbsp;</td>
        <td class="nodok" colspan="6">Tingkatan dokument : V</td>
    </tr>
    <tr>
        <th colspan="12"><strong>SLIP GAJI KARYAWAN</strong></th>
        <td class="nodok" colspan="6">Status Revisi : 00</td>
    </tr>
    <tr>
        <th colspan="6">HC</th>
        <td class="tengah" colspan="12">&nbsp;</td>
        <td class="nodok" colspan="6">Tgl. Mulai berlaku : 8 Juni 2015</td>
    </tr>
    <tr>
        <th colspan="6">Departement</th>
        <td class="tengah" colspan="12">&nbsp;</td>
        <td class="nodok" colspan="6">Halaman : 1 dari 1</td>
    </tr>

<?php

use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\GajiBulanan;
use frontend\modules\payroll\models\ProsesGaji;
use frontend\modules\payroll\models\RevisiGaji;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\ProsesGaji */
/* @var $searchModel frontend\modules\payroll\models\search\GajiBulananSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var ActiveDataProvider $providerGajiBulanan
 * @var ActiveDataProvider $providerRevisiGaji
 */

$this->title = $model->payrollPeriode->namaPeriode;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Proses Gaji'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    
    $('.btn-proses-ulang-pph21').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                        btn.button('reset');
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>
<div class="proses-gaji-view">
    <div class="row">
        <div class="col-sm-6">
            <?= Html::a(Yii::t('frontend', 'Proses Ulang PPh21'), ['proses-ulang-pph21', 'id' => $model->id], ['class' => 'btn btn-success btn-flat btn-proses-ulang-pph21', 'data' => ['confirm' => Yii::t('frontend', 'Anda Yakin ??'), 'method' => 'post']]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'payrollPeriode.namaPeriode',
                    'label' => Yii::t('frontend', 'Payroll Periode'),
                ], [
                    'attribute' => 'golongan.nama',
                    'label' => Yii::t('frontend', 'Golongan'),
                ],
                [
                    'attribute' => 'bisnis_unit',
                    'value' => function (ProsesGaji $model) {
                        return ProsesGaji::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
                    }
                ],
                'catatan:ntext',
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
        <div class="col-sm-5">
            <div class="payment-process-index">
                <?php
                $gridColumnRevisiGaji = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'revisi_ke',
                        'value' => function (RevisiGaji $model) {
                            return Html::tag('strong', Html::a($model->revisi_ke, ['print-revisi', 'id' => $model->id], ['target' => '_blank', 'data' => ['pjax' => 0], 'class' => 'text-danger']));
                        },
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function (RevisiGaji $model) {
                            return date('d-m-Y H:m', strtotime($model->created_at));
                        },
                    ],
                    [
                        'attribute' => 'created_by',
                        'value' => function (RevisiGaji $model) {
                            return $model->createdBy->username;
                        },
                    ],
                ];
                ?>
                <?php
                try {
                    echo GridView::widget([
                        'id' => 'kv-grid-payment-process',
                        'dataProvider' => $providerRevisiGaji,
                        'columns' => $gridColumnRevisiGaji,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-revisi-gaji']],
                        'beforeHeader' => [
                            [
                                'columns' => [
                                    [
                                        'content' => Yii::t('frontend', 'Daftar Revisi Gaji'),
                                        'options' => [
                                            'colspan' => 4,
                                            'class' => 'text-center warning',
                                        ]
                                    ],
                                ],
                                'options' => ['class' => 'skip-export']
                            ]
                        ],
                        'export' => false,
                        'bordered' => true,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'hover' => true,
                        'showPageSummary' => false,
                        'persistResize' => false,
                        'tableOptions' => ['class' => 'small'],
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } ?>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumn = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'id_pegawai',
                    'label' => Yii::t('app', 'Id Pegawai'),
                    'value' => function (GajiBulanan $model) {
                        if ($model->prosesGaji->finishing_status == ProsesGaji::selesai) {
                            return Html::a(Html::tag('strong', str_pad($model->id_pegawai, 8, '0', STR_PAD_LEFT)), ['print-slip', 'id' => $model->id], ['data' => ['pjax' => 0], 'title' => 'Print Slip', 'target' => '_blank']);
                        }
                        return str_pad($model->id_pegawai, 8, '0', STR_PAD_LEFT);

                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'nama_lengkap',
                    'value' => function (GajiBulanan $model) {
                        $statusPegawai = explode(" ", $model->statusPegawai());
                        if ($statusPegawai[0] == 'Baru') {
                            return Html::tag('strong', $model->nama_lengkap, ['class' => 'font-weight-bold text-success']);
                        }
                        if ($statusPegawai[0] == 'Resign') {
                            return Html::tag('strong', $model->nama_lengkap, ['class' => 'font-weight-bold text-danger']);
                        }
                        return $model->nama_lengkap;
                    },
                    'pageSummary' => 'Total',
                    'vAlign' => 'middle',
                    'width' => '210px',
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'bisnis_unit_s',
                    'value' => function (GajiBulanan $model) {
                        return $model->bisnis_unit_s;
                    },
                ],
                [
                    'attribute' => 'golongan_s',
                    'value' => function (GajiBulanan $model) {
                        return $model->golongan_s;
                    },
                ],
                'jenis_kontrak_s',
                'struktur_s',
                [
                    'label' => Yii::t('frontend', 'HK Efective'),
                    'value' => function (GajiBulanan $model) {
                        return $model->hari_kerja_aktif;
                    },
                ],
                [
                    'attribute' => 'alfa',
                    'value' => function (GajiBulanan $model) {
                        return $model->alfa;
                    },
                ],
                [
                    'attribute' => 'cuti',
                    'value' => function (GajiBulanan $model) {
                        return $model->cuti;
                    },
                ],
                [
                    'attribute' => 'undifined',
                    'label' => Yii::t('frontend', 'UFN'),
                    'value' => function (GajiBulanan $model) {
                        return $model->undifined;
                    },
                ],
                [
                    'attribute' => 'dispensasi',
                    'label' => Yii::t('frontend', 'Dispen'),
                    'value' => function (GajiBulanan $model) {
                        return $model->dispensasi;
                    },
                ],
                [
                    'attribute' => 'is',
                    'value' => function (GajiBulanan $model) {
                        return $model->is;
                    },
                ],
                [
                    'attribute' => 'ith',
                    'value' => function (GajiBulanan $model) {
                        return $model->ith;
                    },
                ],
                [
                    'attribute' => 'cm',
                    'value' => function (GajiBulanan $model) {
                        return $model->cm;
                    },
                ],
                [
                    'attribute' => 'idl',
                    'value' => function (GajiBulanan $model) {
                        return $model->idl;
                    },
                ],
                [
                    'attribute' => 'sd',
                    'value' => function (GajiBulanan $model) {
                        return $model->sd;
                    },
                ],
                [
                    'attribute' => 'wfh',
                    'value' => function (GajiBulanan $model) {
                        return $model->wfh;
                    },
                ],
                [
                    'attribute' => 'atl',
                    'value' => function (GajiBulanan $model) {
                        return $model->jumlahATL();
                    },
                ],
                [
                    'attribute' => 'libur_hadir',
                    'value' => function (GajiBulanan $model) {
                        return $model->libur_hadir;
                    },
                ],
                [
                    'attribute' => 'p_gaji_pokok',
                    'value' => function (GajiBulanan $model) {
                        return $model->gajiPokok();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'label' => Yii::t('frontend', 'Hari Kerja'),
                    'value' => function (GajiBulanan $model) {
                        if ($model->tipe_gaji_pokok != MasterDataDefault::HARIAN) {
                            return $model->jumlahHariKerja();
                        }
                        return '';
                    },
                    'format' => ['decimal', 2],
                ],
                [
                    'label' => Yii::t('frontend', 'Jumlah Hadir'),
                    'value' => function (GajiBulanan $model) {
                        return $model->jumlahHadir();
                    },
                    'format' => ['decimal', 2],
                ],
                [
                    'label' => Yii::t('frontend', 'Jumlah Absen'),
                    'value' => function (GajiBulanan $model) {
                        return $model->jumlahAlfa();
                    },
                ],
                [
                    'label' => Yii::t('frontend', 'Jumlah Upah'),
                    'value' => function (GajiBulanan $model) {
                        return $model->totalGajiPokok();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'jam_lembur',
                    'value' => function (GajiBulanan $model) {
                        return $model->jam_lembur;
                    },
                ],
                [
                    'attribute' => 'uang_lembur',
                    'value' => function (GajiBulanan $model) {
                        return $model->uang_lembur;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'uml',
                    'value' => function (GajiBulanan $model) {
                        return $model->totalUml();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'insentif_lembur',
                    'value' => function (GajiBulanan $model) {
                        return $model->insentif_lembur;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_uang_makan',
                    'value' => function (GajiBulanan $model) {
                        return $model->p_uang_makan;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'total_um',
                    'value' => function (GajiBulanan $model) {
                        return $model->hitungTotalUangMakan();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_uang_transport',
                    'value' => function (GajiBulanan $model) {
                        return $model->uangTransport();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'total_ut',
                    'value' => function (GajiBulanan $model) {
                        return $model->hitungTotalUangTransport();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 's2',
                    'value' => function (GajiBulanan $model) {
                        return $model->shift2();
                    },
                ],
                [
                    'attribute' => 's3',
                    'value' => function (GajiBulanan $model) {
                        return $model->shift3();
                    },
                ],
                [
                    'attribute' => 'total_us',
                    'value' => function (GajiBulanan $model) {
                        return $model->hitungTotalUangShift();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'jumlah_telat',
                    'value' => function (GajiBulanan $model) {
                        return $model->jumlah_telat;
                    },
                ],
                [
                    'attribute' => 'p_premi_hadir',
                    'value' => function (GajiBulanan $model) {
                        return $model->premiHadir();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'insentif_tidak_tetap',
                    'value' => function (GajiBulanan $model) {
                        return $model->insentif_tidak_tetap;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'rapel',
                    'value' => function (GajiBulanan $model) {
                        return $model->rapel;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_admin_bank',
                    'value' => function (GajiBulanan $model) {
                        return $model->p_admin_bank;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_sewa_motor',
                    'value' => function (GajiBulanan $model) {
                        return $model->sewaMotor();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_sewa_mobil',
                    'value' => function (GajiBulanan $model) {
                        return $model->sewaMobil();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_sewa_rumah',
                    'value' => function (GajiBulanan $model) {
                        return $model->sewaRumah();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_tunjagan_lain_tetap',
                    'value' => function (GajiBulanan $model) {
                        return $model->tunjLainTetap();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_tunjagan_lain_lain',
                    'value' => function (GajiBulanan $model) {
                        return $model->tunjLainLain();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_tunjangan_anak',
                    'value' => function (GajiBulanan $model) {
                        return $model->p_tunjangan_anak;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_tunjangan_jabatan',
                    'value' => function (GajiBulanan $model) {
                        return $model->tunjJabatan();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_tunjangan_kost',
                    'value' => function (GajiBulanan $model) {
                        return $model->tunjKost();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_tunjangan_pulsa',
                    'value' => function (GajiBulanan $model) {
                        return $model->tunjPulsa();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'label' => Yii::t('frontend', 'Pulsa WFH'),
                    'value' => function (GajiBulanan $model) {
                        return $model->tunjPulsaWfh();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_tunjangan_loyalitas',
                    'value' => function (GajiBulanan $model) {
                        return $model->p_tunjangan_loyalitas;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'p_insentif_tetap',
                    'value' => function (GajiBulanan $model) {
                        return $model->insetifTetap();
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'tunj_pph',
                    'value' => function (GajiBulanan $model) {
                        return 0;
                    },
//                    'format' => ['decimal', 0],
                ],
                [
                    'attribute' => 'pot_gaji',
                    'value' => function (GajiBulanan $model) {
                        return $model->pinjaman_potongan_nominal;
                    },
                ],
                [
                    'attribute' => 'pot_atl',
                    'value' => function (GajiBulanan $model) {
                        return $model->potonganATL();
                    },
                ],
                [
                    'label' => Yii::t('frontend', 'Sub Total'),
                    'value' => function (GajiBulanan $model) {
                        return Yii::$app->formatter->asDecimal($model->subTotal1());
                    },
                ],
                [
                    'attribute' => 'pegawai_bpjs_kes',
                    'value' => function (GajiBulanan $model) {
                        return $model->pegawai_bpjs_kes;
                    },
                    'visible' => $model->bisnis_unit == PerjanjianKerja::BCA_ADA || $model->bisnis_unit == PerjanjianKerja::BCA_KBU ? false : true
                ],
                [
                    'attribute' => 'pegawai_bpjs_tk',
                    'value' => function (GajiBulanan $model) {
                        return $model->pegawai_bpjs_tk;
                    },
                    'visible' => $model->bisnis_unit == PerjanjianKerja::BCA_ADA || $model->bisnis_unit == PerjanjianKerja::BCA_KBU ? false : true
                ],
                [
                    'label' => Yii::t('frontend', 'Potongan BPJS'),
                    'value' => function (GajiBulanan $model) {
                        return $model->potonganBpjs();
                    },
                    'visible' => $model->bisnis_unit == PerjanjianKerja::BCA_ADA || $model->bisnis_unit == PerjanjianKerja::BCA_KBU ? false : true
                ],
                [
                    'attribute' => 'pinjaman',
                    'value' => function (GajiBulanan $model) {
                        return $model->pinjaman_nominal;
                    },
                ],
                [
                    'attribute' => 'bank',
                    'value' => function (GajiBulanan $model) {
                        return $model->pinjaman_bank_nominal;
                    },
                ],
                [
                    'attribute' => 'koperasi',
                    'value' => function (GajiBulanan $model) {
                        return $model->pinjaman_koperasi_nominal;
                    },
                ],
                [
                    'attribute' => 'kasbon',
                    'value' => function (GajiBulanan $model) {
                        return $model->pinjaman_kasbon_nominal;
                    },
                ],
                [
                    'attribute' => 'pot_lain',
                    'value' => function (GajiBulanan $model) {
                        return $model->pinjaman_lain_lain_nominal;
                    },
                ],
                [
                    'attribute' => 'pot_pph',
                    'value' => function (GajiBulanan $model) {
                        return 0;
                    },
//                    'format' => ['decimal', 0],
                ],
                [
                    'label' => Yii::t('frontend', 'Sub Total2'),
                    'value' => function (GajiBulanan $model) {
                        return $model->subTotal2();
                    },
                    'format' => ['decimal', 0],
                    'pageSummary' => true
                ],
                [
                    'attribute' => 'insentif_pph',
                    'value' => function (GajiBulanan $model) {
                        return $model->insentif_pph;
                    },
                    'format' => ['decimal', 0],
                ],
                [
                    'label' => Yii::t('frontend', 'Total Diterima'),
                    'value' => function (GajiBulanan $model) {
                        return $model->Total();
                    },
                    'format' => ['decimal', 0],
                    'pageSummary' => true
                ],
                [
                    'attribute' => 'pegawai_bpjs_kes',
                    'value' => function (GajiBulanan $model) {
                        return $model->pegawai_bpjs_kes;
                    },
                    'visible' => $model->bisnis_unit == PerjanjianKerja::KBU || $model->bisnis_unit == PerjanjianKerja::ADA ? false : true
                ],
                [
                    'attribute' => 'pegawai_bpjs_tk',
                    'value' => function (GajiBulanan $model) {
                        return $model->pegawai_bpjs_tk;
                    },
                    'visible' => $model->bisnis_unit == PerjanjianKerja::KBU || $model->bisnis_unit == PerjanjianKerja::ADA ? false : true
                ],
                [
                    'label' => Yii::t('frontend', 'Potongan BPJS'),
                    'value' => function (GajiBulanan $model) {
                        return $model->potonganBpjs();
                    },
                    'visible' => $model->bisnis_unit == PerjanjianKerja::KBU || $model->bisnis_unit == PerjanjianKerja::ADA ? false : true
                ],
                [
                    'attribute' => 'insentif_gt',
                    'value' => function (GajiBulanan $model) {
                        return $model->insentif_gt;
                    },
                    'format' => ['decimal', 0],
                    'visible' => $model->bisnis_unit == PerjanjianKerja::KBU || $model->bisnis_unit == PerjanjianKerja::ADA ? false : true
                ],
                [
                    'label' => Yii::t('frontend', 'Total Diterima Yayasan'),
                    'value' => function (GajiBulanan $model) {
                        return $model->Total(TRUE);
                    },
                    'format' => ['decimal', 0],
                    'pageSummary' => true,
                    'visible' => $model->bisnis_unit == PerjanjianKerja::KBU || $model->bisnis_unit == PerjanjianKerja::ADA ? false : true
                ],
                [
                    'label' => Yii::t('frontend', 'Status'),
                    'value' => function (GajiBulanan $model) {
                        $data = explode(" ", $model->statusPegawai());
                        if ($data[0] == 'Baru') {
                            return Html::tag('strong', $model->statusPegawai(), ['class' => 'font-weight-bold text-success']);
                        }
                        if ($data[0] == 'Resign') {
                            return Html::tag('strong', $model->statusPegawai(), ['class' => 'font-weight-bold text-danger']);
                        }
                        return $model->statusPegawai();
                    },
                    'format' => 'raw',
                ],
                [
                    'label' => Yii::t('frontend', 'Cost Center'),
                    'value' => function (GajiBulanan $model) {
                        if ($costCenter = $model->costCenter) {
                            return $costCenter->costCenterTemplate->kode;
                        }
                        return '';
                    },
                ],
                [
                    'label' => Yii::t('frontend', 'Nama Bank'),
                    'value' => function (GajiBulanan $model) {
                        if ($pegawaiBank = $model->pegawaiBank) {
                            return $pegawaiBank->_nama_bank[$model->pegawai_bank_nama];
                        }
                        return '';
                    },
                ],
                [
                    'label' => Yii::t('frontend', 'No Rekening'),
                    'value' => function (GajiBulanan $model) {
                        return $model->pegawai_bank_nomor;
                    },
                ],
                ['attribute' => 'lock', 'visible' => false],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => ''
                ],
            ];
            ?>
            <?php
            try {
                $export = ExportMenu::widget([
                    'dataProvider' => $providerGajiBulanan,
//                    'filterModel' => $searchModel,
                    'columns' => $gridColumn,
//                    'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37],
                    'noExportColumns' => [1],
                    'dropdownOptions' => [
                        'class' => 'btn btn-secondary'
                    ],
                    'filename' => 'rekap-gaji-' . date('M', strtotime($model->payrollPeriode->tanggal_akhir)) . '-' . ProsesGaji::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit] . '-' . $model->golongan->nama . date('dmYHis')
                ]);
                echo GridView::widget([
                    'id' => 'grid-gaji-bulanan',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumn,
                    'showPageSummary' => true,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-gaji-bulanan']],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'toolbar' => [
                        [
                            'content' =>
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['view', 'id' => $model->id], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                        ],
                        [
                            'content' => $export
                        ],
                    ],
                    'panel' => [
                        'heading' => false,
                        'before' => '{summary}',
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            } ?>
        </div>
    </div>
</div>
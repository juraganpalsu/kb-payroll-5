<?php
/**
 * Created by PhpStorm
 *
 * File Name: perhatian.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 24/07/19
 * Time: 23:32
 */

$this->title = Yii::t('app', 'Perhatian!');
?>

<div class="box-body">
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
        Data ini hanya bisa dibuka oleh PIC terkait atau otorisasi yang sudah ditentukan.
    </div>
</div>
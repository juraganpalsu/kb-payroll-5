<?php

use common\models\Golongan;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\ProsesGaji;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\pegawai\models\PerjanjianKerja;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\ProsesGaji */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#form').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
                btn.button('reset');
            });
            return false;
        });
    })
JS;

$this->registerJs($js);

?>

<div class="proses-gaji-form">
    <div class="row">
        <div class="col-md-6">
            <?php $form = ActiveForm::begin([
                'id' => 'form',
                'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
                'type' => ActiveForm::TYPE_VERTICAL,
                'formConfig' => ['showErrors' => false],
                'enableAjaxValidation' => true,
                'validationUrl' => Url::to(['create-or-update-validation']),
                'fieldConfig' => ['showLabels' => true],
            ]); ?>

            <?= $form->errorSummary($model); ?>

            <?php try { ?>

                <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                <div class="row">
                    <div class="col-md-10">
                        <?= $form->field($model, 'payroll_periode_id')->widget(Select2::class, [
                            'data' => ArrayHelper::map(PayrollPeriode::find()->andWhere(['!=', 'tipe', MasterDataDefault::BORONGAN])->orderBy('id')->orderBy('tanggal_awal DESC')->all(), 'id', 'namaPeriode'),
                            'options' => ['placeholder' => Yii::t('frontend', 'Choose Payroll periode')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                </div>

                <?php
                // gol 1 kbu
                $data = '';
                if (Helper::checkRoute('/payroll/proses-gaji/create')) {
                    // KBU GOL 1 payroll KBU
                    if (Helper::checkRoute('/payroll/proses-gaji/satu') && Helper::checkRoute('/payroll/proses-gaji/kbu')) {
                        $data = ['NOL', 'SATU-H', 'LIMA', 'ENAM', 'TUJUH'];
                    }
                    // BCA GOL 1,2,3 Payroll BCA
                    if (Helper::checkRoute('/payroll/proses-gaji/satu') && Helper::checkRoute('/payroll/proses-gaji/dua') && Helper::checkRoute('/payroll/proses-gaji/tiga') && Helper::checkRoute('/payroll/proses-gaji/bca-kbu') || Helper::checkRoute('/payroll/proses-gaji/bca-ada')) {
                        $data = ['SATU-H', 'SATU-B', 'DUA', 'TIGA'];
                    }
                    // ADA GOL 3, 4 Mas Rama
                    if (Helper::checkRoute('/payroll/proses-gaji/empat') && Helper::checkRoute('/payroll/proses-gaji/ada')) {
                        $data = ['DUA', 'TIGA-GT', 'EMPAT-GT'];
                    }
                    // ADA GOL 2,3,4 Pak Waskito
                    if (Helper::checkRoute('/payroll/proses-gaji/dua') && Helper::checkRoute('/payroll/proses-gaji/tiga') && Helper::checkRoute('/payroll/proses-gaji/empat') && Helper::checkRoute('/payroll/proses-gaji/kbu')) {
                        $data = ['DUA', 'TIGA', 'EMPAT'];
                    }
                    // ADA GOL 2,3,4 Bu Yeni
//                    if (Helper::checkRoute('/payroll/proses-gaji/lima') && Helper::checkRoute('/payroll/proses-gaji/enam') && Helper::checkRoute('/payroll/proses-gaji/tujuh') && Helper::checkRoute('/payroll/proses-gaji/kbu')) {
//                        $data = ['LIMA', 'ENAM', 'TUJUH'];
//                    }
                    // all
                    if (Helper::checkRoute('/payroll/proses-gaji/all')) {
                        $data = ['NOL', 'SATU-H', 'SATU-B', 'DUA', 'TIGA', 'EMPAT', 'LIMA', 'ENAM', 'TUJUH', 'TIGA-GT', 'EMPAT-GT'];
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-9">
                            <?= $form->field($model, 'golongan_id')->widget(Select2::class, [
                                'data' => ArrayHelper::map(Golongan::find()->andWhere(['IN', 'nama', $data])->orderBy('id')->all(), 'id', 'nama'),
                                'options' => ['placeholder' => Yii::t('frontend', 'Choose Golongan')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php
                // KBU
                $all = [PerjanjianKerja::GCI => '', PerjanjianKerja::OSI => '', PerjanjianKerja::PIC_BCA => '', PerjanjianKerja::GSM => '', PerjanjianKerja::KBU => '', PerjanjianKerja::ADA => '', PerjanjianKerja::BCA_ADA => '', PerjanjianKerja::BCA_KBU => ''];
                if (Helper::checkRoute('/payroll/proses-gaji/kbu')) {
                    unset($all[PerjanjianKerja::KBU]);
                    ?>
                    <div class="row">
                        <div class="col-md-9">
                            <?= $form->field($model, 'bisnis_unit')->dropDownList(array_diff_key(ProsesGaji::modelPerjanjianKerja()->_kontrak, $all)) ?>
                        </div>
                    </div>
                <?php } ?>
                <?php
                // ADA
                $all = [PerjanjianKerja::GCI => '', PerjanjianKerja::OSI => '', PerjanjianKerja::PIC_BCA => '', PerjanjianKerja::GSM => '', PerjanjianKerja::KBU => '', PerjanjianKerja::ADA => '', PerjanjianKerja::BCA_ADA => '', PerjanjianKerja::BCA_KBU => ''];
                if (Helper::checkRoute('/payroll/proses-gaji/ada')) {
                    unset($all[PerjanjianKerja::ADA], $all[PerjanjianKerja::KBU]);
                    ?>
                    <div class="row">
                        <div class="col-md-9">
                            <?= $form->field($model, 'bisnis_unit')->dropDownList(array_diff_key(ProsesGaji::modelPerjanjianKerja()->_kontrak, $all)) ?>
                        </div>
                    </div>
                <?php } ?>
                <?php
                // ADA
                $all = [PerjanjianKerja::GCI => '', PerjanjianKerja::OSI => '', PerjanjianKerja::PIC_BCA => '', PerjanjianKerja::GSM => '', PerjanjianKerja::KBU => '', PerjanjianKerja::ADA => '', PerjanjianKerja::BCA_ADA => '', PerjanjianKerja::BCA_KBU => ''];
                if (Helper::checkRoute('/payroll/proses-gaji/bca-kbu') || Helper::checkRoute('/payroll/proses-gaji/bca-ada')) {
                    unset($all[PerjanjianKerja::BCA_KBU], $all[PerjanjianKerja::BCA_ADA]);
                    ?>
                    <div class="row">
                        <div class="col-md-9">

                            <?= $form->field($model, 'bisnis_unit')->dropDownList(array_diff_key(ProsesGaji::modelPerjanjianKerja()->_kontrak, $all)) ?>
                        </div>
                    </div>
                <?php } ?>
                <?php
                // ALL
                $all = [PerjanjianKerja::GCI => '', PerjanjianKerja::OSI => '', PerjanjianKerja::PIC_BCA => '', PerjanjianKerja::GSM => '', PerjanjianKerja::KBU => '', PerjanjianKerja::ADA => '', PerjanjianKerja::BCA_ADA => '', PerjanjianKerja::BCA_KBU => ''];
                if (Helper::checkRoute('/payroll/proses-gaji/all')) {
                    unset($all[PerjanjianKerja::BCA_KBU], $all[PerjanjianKerja::BCA_ADA], $all[PerjanjianKerja::KBU], $all[PerjanjianKerja::ADA])
                    ?>
                    <div class="row">
                        <div class="col-md-9">

                            <?= $form->field($model, 'bisnis_unit')->dropDownList(array_diff_key(ProsesGaji::modelPerjanjianKerja()->_kontrak, $all)) ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success submit-btn' : 'btn btn-primary submit-btn']) ?>
                </div>

            <?php } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            } ?>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr class="warning">
                            <th scope="col">#</th>
                            <th scope="col">Tipe Gaji</th>
                            <th scope="col">Keterangan</th>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>POKOK_FIX_B</td>
                            <td>Gaji bulanan periode 21-20 gol 2 ketas</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>HARIAN_H</td>
                            <td>Gaji harian periode 16-15 gol 1 bca</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>POKOK_FIX_H</td>
                            <td>Gaji bulanan periode harian 16-15 gol 1 kbu produksi dan gol 1 bca operator</td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td>HARIAN_B</td>
                            <td>Gaji harian periode bulanan 21-20 spg</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
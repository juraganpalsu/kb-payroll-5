<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Rapel */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Rapel',
    ]) . ' ' . $model->pegawai->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Rapel'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama_lengkap, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="rapel-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

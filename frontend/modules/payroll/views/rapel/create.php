<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Rapel */

$this->title = Yii::t('frontend', 'Create Rapel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Rapel'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rapel-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

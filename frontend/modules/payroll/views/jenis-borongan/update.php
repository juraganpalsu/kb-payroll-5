<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\JenisBorongan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Jenis Borongan',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Jenis Borongan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="jenis-borongan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

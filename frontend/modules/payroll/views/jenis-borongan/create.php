<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\JenisBorongan */

$this->title = Yii::t('frontend', 'Create Jenis Borongan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Jenis Borongan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-borongan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

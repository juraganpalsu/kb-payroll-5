<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\JenisBoronganSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\payroll\models\JenisBorongan;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Jenis Borongan');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>
<div class="jenis-borongan-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'nama_produk',
        'harga',
        'catatan:ntext',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, JenisBorongan $model) {
                    return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-success btn-flat btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Update data {nama}', ['nama' => $model->nama_produk])]]);
                },
                'delete' => function ($url, JenisBorongan $model) {
                    return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                },
            ],
        ],
    ];
    ?>

    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'payroll-asuransi-lain' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-jenis-borongan']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Buat Jenis Borongan')]]) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
</div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

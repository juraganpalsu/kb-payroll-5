<?php

/**
 * Created by PhpStorm.
 * File Name: _print.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 31/08/20
 * Time: 22.01
 */

use frontend\modules\payroll\models\Pinjaman;

/**
 * @var Pinjaman $model
 */


echo $this->renderAjax('_print-header', ['model' => $model]);
$pokokHutang = $model->jumlah;
foreach ($model->pinjamanAngsurans as $angsuran) {
    ?>
    <tr>
        <td colspan="2" class="border-bottom-dotted center">
            <?= $angsuran->angsuran_ke; ?>
        </td>
        <td colspan="9" class="border-bottom-dotted">
            <?= $angsuran->payrollPeriode->namaPeriode ?>
        </td>
        <td colspan="2" class="border-bottom-dotted right">
            <?= Yii::$app->formatter->asDecimal($angsuran->jumlah, 0) ?>
        </td>
        <td colspan="2" class="border-bottom-dotted right">
            <?php
            $pokokHutang -= $angsuran->jumlah;
            ?>
            <?= Yii::$app->formatter->asDecimal($pokokHutang, 0) ?>
        </td>
        <td colspan="3" class="border-bottom-dotted center">
            <?= $angsuran->status_ ?>
        </td>
        <td colspan="6" class="border-bottom-dotted">
            <?= $angsuran->catatan ?>
        </td>
    </tr>

    <?php
}
?>
</table>


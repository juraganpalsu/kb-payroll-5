<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Pinjaman */

$this->title = Yii::t('frontend', 'Create Pinjaman');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Pinjaman'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pinjaman-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use frontend\modules\payroll\models\Pinjaman;
use frontend\modules\payroll\models\PinjamanAngsuran;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Pinjaman */

/**
 * @var ActiveDataProvider $providerPinjamanAngsuran
 */

$this->title = Yii::t('frontend', 'Pinjaman') . ' ' . $model->pegawai->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Pinjaman'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {    
    
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.set-dibayar').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    
});
JS;

$this->registerJs($js);
?>
    <div class="pinjaman-view">
        <div class="row">
            <div class='col-sm-12'>
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],

                    [
                        'attribute' => 'kategori',
                        'value' => function (Pinjaman $model) {
                            return $model->_kategori[$model->kategori];
                        },
                    ],
                    'nama',
                    'jumlah',
                    'tenor',
                    'angsuran',
                    [
                        'attribute' => 'payroll_periode_id',
                        'label' => Yii::t('frontend', 'Periode Potong(Mulai)'),
                        'value' => function (Pinjaman $model) {
                            return $model->payrollPeriode->namaPeriode;
                        },
                    ],
                    'keterangan',
                    ['attribute' => 'lock', 'visible' => false],
                ];
                try {
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-12'>
                <?php
                $gridColumnPinjamanAngsuran = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    'angsuran_ke',
                    'jumlah',
                    [
                        'attribute' => 'status',
                        'value' => function (PinjamanAngsuran $model) {
                            return $model->status_;
                        },
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'payroll_periode_id',
                        'value' => function (PinjamanAngsuran $model) {
                            return $model->payrollPeriode->namaPeriode;
                        }
                    ],
                    'catatan:text',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update-angsuran}',
                        'buttons' => [
                            'update-angsuran' => function ($url, PinjamanAngsuran $model) {
                                if (Helper::checkRoute('/payroll/pinjaman/finance')) {
                                    return Html::a(Yii::t('frontend', 'Edit'), $url, ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Edit Angsuran ke {ke}', ['ke' => $model->angsuran_ke])]]);
                                }
                            },
                        ],
                    ],
                    ['attribute' => 'lock', 'visible' => false],
                ];
                try {
                    echo GridView::widget([
                        'dataProvider' => $providerPinjamanAngsuran,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pinjaman-angsuran']],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('frontend', 'Pinjaman Angsuran')),
                        ],
                        'export' => false,
                        'columns' => $gridColumnPinjamanAngsuran
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
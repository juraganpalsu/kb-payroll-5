<?php

/**
 * Created by PhpStorm.
 * File Name: _form-download-pinjaman.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 20/04/20
 * Time: 16.01
 */


use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\PayrollPeriode;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\payroll\models\Pinjaman */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#download-pinjaman').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    let win = window.open(dt.data.url, '_blank');
                    win.focus();
                }
                btn.button('reset');
            });
            return false;
        });
    })
JS;

$this->registerJs($js);


?>

<div class="download-pinjaman-form">

    <?php $form = ActiveForm::begin([
        'id' => 'download-pinjaman',
        'action' => Url::to(['download-pinjaman-form']),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['download-pinjaman-form-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'payroll_periode_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(PayrollPeriode::find()->orderBy('id')->orderBy('tanggal_awal DESC')->all(), 'id', 'namaPeriode'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Payroll periode')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('frontend', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

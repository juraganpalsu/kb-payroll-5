<?php

use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\Pinjaman;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Pinjaman */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#form-rapel').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
                btn.button('reset');
            });
            return false;
        });
    });
    $('.form-control').on('keyup', function () {
            $(this).val( $(this).val().toUpperCase() );
        }); 

    $('#pinjaman-jumlah').on('keyup', function() {
        let jumlah = $(this).val();
        let tenor = $('#pinjaman-tenor').val();
        let angsuran = jumlah / tenor;
        if (tenor !== ''){
            $('#pinjaman-angsuran').val(angsuran);
        }
    });
    $('#pinjaman-tenor').on('keyup', function() {
        let tenor = $(this).val();
        let jumlah = $('#pinjaman-jumlah').val();
        let angsuran = jumlah / tenor;
        if (jumlah !== ''){
            $('#pinjaman-angsuran').val(angsuran);
        }
    });
JS;

$this->registerJs($js);

?>

<div class="pinjaman-form">
    <div class="row">
        <div class="col-md-6">

            <?php $form = ActiveForm::begin([
                'id' => 'form-rapel',
                'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
                'type' => ActiveForm::TYPE_VERTICAL,
                'formConfig' => ['showErrors' => false],
                'enableAjaxValidation' => true,
                'validationUrl' => Url::to(['create-or-update-validation']),
                'fieldConfig' => ['showLabels' => true],
            ]); ?>

            <?= $form->errorSummary($model); ?>

            <?php try { ?>
                <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
                <?php
                // Finance
                $all = [Pinjaman::BANK => '', Pinjaman::KASBON => '', Pinjaman::PINJAMAN => '', Pinjaman::KOPERASI => '', Pinjaman::LAIN_LAIN => '', Pinjaman::POTONGAN => '', Pinjaman::POTONGAN_ATL => ''];
                if (Helper::checkRoute('/payroll/pinjaman/finance')) {
                    unset($all[Pinjaman::BANK], $all[Pinjaman::KASBON], $all[Pinjaman::PINJAMAN], $all[Pinjaman::KOPERASI], $all[Pinjaman::LAIN_LAIN]);
                    ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'kategori')->dropDownList(array_diff_key($model->_kategori, $all)) ?>
                        </div>
                    </div>
                <?php } ?>
                <?php
                // Payroll
                $all = [Pinjaman::BANK => '', Pinjaman::KASBON => '', Pinjaman::PINJAMAN => '', Pinjaman::KOPERASI => '', Pinjaman::LAIN_LAIN => '', Pinjaman::POTONGAN => '', Pinjaman::POTONGAN_ATL => ''];
                if (Helper::checkRoute('/payroll/pinjaman/payroll')) {
                    unset($all[Pinjaman::POTONGAN], $all[Pinjaman::POTONGAN_ATL]);
                    ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'kategori')->dropDownList(array_diff_key($model->_kategori, $all)) ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-10">
                        <?= $form->field($model, 'payroll_periode_id')->widget(Select2::class, [
                            'data' => ArrayHelper::map(PayrollPeriode::find()->orderBy('id')->orderBy('tanggal_awal DESC')->all(), 'id', 'namaPeriode'),
                            'options' => ['placeholder' => Yii::t('frontend', 'Choose Payroll periode')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'pluginEvents' => [
                                'select2:select' => 'function() {$("#ppid-temp").val($(this).val())}'
                            ]
                        ]); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <?= Html::hiddenInput('tanggal-temp', date('Y-m-d'), ['id' => 'tanggal-temp']) ?>
                        <?= Html::hiddenInput('ppid-temp', date('Y-m-d'), ['id' => 'ppid-temp']) ?>
                        <?= $form->field($model, 'pegawai_id')->widget(Select2::class, [
                            'options' => [
                                'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                                'multiple' => false,
                            ],
                            'data' => $model->isNewRecord ? [] : [$model->pegawai_id => $model->pegawai->namaIdPegawai],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-ppid-for-select2']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term, ppid:$("#ppid-temp").val()};}')
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama Pinjaman']) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <?= $form->field($model, 'jumlah', ['addon' => ['prepend' => ['content' => Yii::t('frontend', 'Rp')]]])->textInput(['placeholder' => 'Jumlah']) ?>
                    </div>

                    <div class="col-md-4">
                        <?= $form->field($model, 'tenor', ['addon' => ['append' => ['content' => Yii::t('frontend', 'Bulan')]]])->textInput(['placeholder' => 'Tenor']) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <?= $form->field($model, 'angsuran', ['addon' => ['prepend' => ['content' => Yii::t('frontend', 'Rp')]]])->textInput(['placeholder' => 'Angsuran Perbulan', 'disabled' => true]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <?= $form->field($model, 'keterangan')->textarea(['rows' => 3]) ?>
                    </div>
                </div>

                <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            } ?>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr class="warning">
                            <th scope="col">#</th>
                            <th scope="col">Tipe Gaji</th>
                            <th scope="col">Keterangan</th>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>POKOK_FIX_B</td>
                            <td>Gaji bulanan periode 21-20 gol 2 ketas</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>HARIAN_H</td>
                            <td>Gaji harian periode 16-15 gol 1 bca</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>POKOK_FIX_H</td>
                            <td>Gaji bulanan periode harian 16-15 gol 1 kbu produksi dan gol 1 bca operator</td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td>HARIAN_B</td>
                            <td>Gaji harian periode bulanan 21-20 spg</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>

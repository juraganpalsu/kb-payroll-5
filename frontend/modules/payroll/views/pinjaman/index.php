<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\PinjamanSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var string $redirect */

/**
 * @var Pinjaman $modelPinjaman
 * @var PerjanjianKerja $modelPerjanjiankerja
 */

use common\models\Golongan;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\Pinjaman;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('frontend', 'Potongan Upah');

$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>
    <div class="pinjaman-index">
        <p>
            <?php
            try {
                echo ButtonDropdown::widget([
                    'label' => Yii::t('frontend', 'Download Template'),
                    'options' => [
                        'class' => 'btn btn-warning btn-flat',
                    ],
                    'dropdown' => [
                        'items' => [
                            ['label' => Yii::t('frontend', 'Potongan'), 'url' => Url::to(['get-template', 'kategori' => Pinjaman::POTONGAN]), 'linkOptions' => ['target' => '_blank']],
                            ['label' => Yii::t('frontend', 'Pinjaman'), 'url' => Url::to(['get-template', 'kategori' => Pinjaman::KOPERASI]), 'linkOptions' => ['target' => '_blank']]
                        ]
                    ],
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
            <?= Html::a(Yii::t('frontend', 'Download Pinjaman'), ['download-pinjaman-form'], ['class' => 'btn btn-success call-modal-btn', 'title' => Yii::t('frontend', 'Download Pinjaman'), 'data' => ['header' => Yii::t('frontend', 'Download Pinjaman')]]); ?>
            <?= Html::a(Yii::t('frontend', 'Upload Template'), ['upload-template'], ['class' => 'btn btn-success call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Upload Template')]]) ?>

        </p>
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'id_pegawai',
                'label' => Yii::t('app', 'Id Pegawai'),
                'value' => function (Pinjaman $model) {
                    return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
                },
            ],
            [
                'attribute' => 'pegawai_id',
                'label' => Yii::t('frontend', 'Pegawai'),
                'value' => function (Pinjaman $model) {
                    return Html::a(Html::tag('strong', $model->pegawai->nama_lengkap), ['view', 'id' => $model->id], ['class' => 'font-weight-bold', 'title' => 'view data']);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'golongan',
                'label' => Yii::t('frontend', 'Golongan'),
                'value' => function (Pinjaman $model) {
                    if ($golongan = $model->pegawaiGolongan) {
                        return $golongan->golongan->nama;
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
                'hAlign' => 'center',
                'format' => 'raw'
            ],
            [
                'attribute' => 'busines_unit',
                'label' => Yii::t('frontend', 'Bisnis Unit'),
                'value' => function (Pinjaman $model) {
                    if ($perjanjianKerja = $model->perjanjianKerja) {
                        return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelPerjanjiankerja->_kontrak,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
                'hAlign' => 'center',
                'format' => 'raw'
            ],
            [
                'attribute' => 'kategori',
                'value' => function (Pinjaman $model) {
                    return $model->_kategori[$model->kategori];
                },
                'filter' => Html::activeDropDownList($searchModel, 'kategori', $searchModel->_kategori, ['class' => 'form-control', 'prompt' => '*']),
            ],
            'nama',
            [
                'attribute' => 'jumlah',
                'value' => function (Pinjaman $model) {
                    return $model->jumlah;
                },
                'format' => ['decimal', 0],
            ],
            'tenor',
            [
                'attribute' => 'angsuran /bulan',
                'value' => function (Pinjaman $model) {
                    return $model->angsuran;
                },
                'format' => ['decimal', 0],
            ],
            [
                'attribute' => 'tenor_dibayar',
                'value' => function (Pinjaman $model) {
                    return $model->countDibayar();
                },
            ],
            [
                'attribute' => 'sisa_bayar',
                'value' => function (Pinjaman $model) {
                    return $model->sisaBayar();
                },
                'format' => ['decimal', 0]
            ],
            [
                'attribute' => 'status_lunas',
                'value' => function (Pinjaman $model) {
                    $status = Pinjaman::BELUM;
                    $warnaText = 'text-danger';
                    if ($model->sisaBayar() == 0) {
                        $warnaText = 'text-success';
                        $status = Pinjaman::LUNAS;
                    }
                    return Html::tag('strong', $model->_status[$status], ['class' => $warnaText . ' font-weight-bold']);
                },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $modelPinjaman->_status,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status')],
                'format' => 'raw'
            ],
            [
                'attribute' => 'payroll_periode_id',
                'label' => Yii::t('frontend', 'Periode Potong(Mulai)'),
                'value' => function (Pinjaman $model) {
                    return $model->payrollPeriode->namaPeriode;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(PayrollPeriode::find()->all(), 'id', 'namaPeriode'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Payroll periode', 'id' => 'grid-pinjaman-search-payroll_periode_id']
            ],
            [
                'label' => Yii::t('frontend', 'Periode Potong(Akhir)'),
                'value' => function (Pinjaman $model) {
                    return $model->periodeAkhir();
                }
            ],
            'keterangan:ntext',
            [
                'attribute' => 'created_by',
                'value' => function (Pinjaman $model) {
                    $createdBy = $model->createdBy->userPegawai;
                    $nama = $createdBy ? $createdBy->pegawai->nama : '';
                    $namaTampil = explode(' ', $nama);
                    return $namaTampil[0];
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function (Pinjaman $model) {
                    return date('d-m-Y H:m', strtotime($model->created_at));
                },
            ],
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{print} {delete}',
                'buttons' => [
                    'print' => function ($url, Pinjaman $model) {
                        return Html::a(Yii::t('frontend', 'Print'), $url, ['class' => 'btn btn-xs btn-primary btn-flat', 'data' => ['pjax' => 0], 'target' => '_blank']);
                    },
                    'delete' => function ($url, Pinjaman $model) {
                        if ($model->countDibayar() === 0 && Helper::checkRoute('/payroll/pinjaman/payroll')) {
                            return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                        }
                        return '';
                    },
                ],
            ],
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'noExportColumns' => [1],
//            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10],
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'pinjaman-potongan' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pinjaman']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
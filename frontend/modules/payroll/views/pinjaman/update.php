<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Pinjaman */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Pinjaman',
    ]) . ' ' . $model->pegawai->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Pinjaman'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama_lengkap, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="pinjaman-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/**
 * Created by PhpStorm.
 * File Name: update-pinjaman.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 30/08/20
 * Time: 23.27
 */

use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Pinjaman */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#form-angsuran').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
                btn.button('reset');
            });
            return false;
        });
    });
JS;

$this->registerJs($js);

?>

<div class="angsuran-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-angsuran',
        'action' => Url::to(['update-angsuran', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['update-angsuran-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'jumlah', ['addon' => ['prepend' => ['content' => Yii::t('frontend', 'Rp')]]])->textInput(['placeholder' => 'Jumlah', 'disabled' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <?= $form->field($model, 'pinjaman_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'jumlah', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

<?php

/**
 * Created by PhpStorm.
 * File Name: _print-header.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 31/08/20
 * Time: 22.01
 */


use common\models\ReceiveOrders;
use frontend\modules\payroll\models\Pinjaman;
use yii\helpers\Html;

/**
 * @var Pinjaman $model
 *
 */

?>
<table width="200mm">
    <tr>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
    </tr>
    <tr>
        <td width="8mm"
            style="vertical-align: top;">
            <div><?php echo Html::img('@frontend/web/pictures/image001.png', ['height' => '40px']); ?></div>
        </td>
        <td width="48mm" colspan="6"
            style=" vertical-align: top; padding-left: 3mm;">
            <div><strong>PT. KOBE BOGA UTAMA</strong><br/></div>
        </td>
        <td class="center" width="80mm" colspan="10">
            <?= Html::tag('span', Yii::t('frontend', 'KARTU HUTANG'), ['style' => 'font: bold 16px arial; text-decoration: underline']); ?>
            <br>
            <?= Html::tag('span', ''); ?>
        </td>
        <td width="8mm">
            &nbsp;
        </td>
        <td width="16mm" colspan="2"
            style="vertical-align: text-top">
            <?= Yii::t('frontend', 'Created') . ' :'; ?>
        </td>
        <td width="32mm" colspan="4"
            style="vertical-align: text-top">
            <?= date('d-m-Y H:i:s', strtotime($model->created_at)); ?>
        </td>
    </tr>
    <tr>
        <td colspan="24">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
            <?= Yii::t('frontend', 'Nama') . ' :'; ?>
        </td>
        <td colspan="11" class="bold">
            <?= $model->perjanjianKerja->pegawai->namaIdPegawai; ?>
        </td>
        <td colspan="3">
            <?= Yii::t('frontend', 'Kategori') . ' :'; ?>
        </td>
        <td colspan="7" class="bold">
            <?= $model->_kategori[$model->kategori];; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <?= Yii::t('frontend', 'Bisnis Unit') . ' :'; ?>
        </td>
        <td colspan="11" class="bold">
            <?= $model->perjanjianKerja->namaKontrak; ?>
        </td>
        <td colspan="3"><?= Yii::t('frontend', 'Nama Pinjaman') . ' :'; ?></td>
        <td colspan="7" class="bold"><?= $model->nama ?></td>
    </tr>
    <tr>
        <td colspan="3">
        </td>
        <td colspan="11" class="bold">
        </td>
        <td colspan="3"><?= Yii::t('frontend', 'Status Lunas') . ' :'; ?></td>
        <td colspan="7"
            class="bold"><?= $model->sisaBayar() == 0 ? $model->_status[Pinjaman::LUNAS] : $model->_status[Pinjaman::BELUM]; ?></td>
    </tr>
    <tr>
        <td colspan="24" class="head">
            <?= Yii::t('frontend', 'Detail Angsuran') ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center head border-bottom">
            <?= Yii::t('frontend', 'Angsuran Ke') ?>
        </td>
        <td colspan="9" class="center head border-bottom">
            <?= Yii::t('frontend', 'Periode Potong') ?>
        </td>
        <td colspan="2" class="center head border-bottom">
            <?= Yii::t('frontend', 'Jumlah') ?>
        </td>
        <td colspan="2" class="center head border-bottom">
            <?= Yii::t('frontend', 'Sisa') ?>
        </td>
        <td colspan="3" class="center head border-bottom">
            <?= Yii::t('frontend', 'Status') ?>
        </td>
        <td colspan="6" class="center head border-bottom">
            <?= Yii::t('frontend', 'Catatan') ?>
        </td>
    </tr>


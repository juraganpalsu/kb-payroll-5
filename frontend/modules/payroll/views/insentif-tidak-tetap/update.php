<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\InsentifTidakTetap */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Insentif Tidak Tetap',
    ]) . ' ' . $model->pegawai->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Insentif Tidak Tetap'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama_lengkap, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="insentif-tidak-tetap-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

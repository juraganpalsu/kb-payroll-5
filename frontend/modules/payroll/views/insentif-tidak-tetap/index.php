<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\InsentifTidakTetapSearch */

/**
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $modelPerjanjiankerja \frontend\modules\pegawai\models\PerjanjianKerja
 */

use common\models\Golongan;
use frontend\modules\payroll\models\InsentifTidakTetap;
use frontend\modules\payroll\models\PayrollPeriode;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Insentif Tidak Tetap');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.get-template').on('click', function(e) {
        e.preventDefault();
        let btn = $(this);
        $.post(btn.attr('href'))
        .done(function(dt) {
            var win = window.open(dt.url);
            win.focus();
            return true;
        }).fail(function() {
            console.log('Gagal')
        })
    });
});
JS;

$this->registerJs($js);
?>
    <div class="insentif-tidak-tetap-index">
        <p>
            <?= Html::a(Yii::t('frontend', 'Download Template Insentif'), ['get-template'], ['class' => 'btn btn-warning get-template']) ?>
            <?= Html::a(Yii::t('frontend', 'Upload Template'), ['upload-template'], ['class' => 'btn btn-success call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Upload Template')]]) ?>
        </p>
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'kategori',
                'value' => function (InsentifTidakTetap $model) {
                    return $model->_kategori[$model->kategori];
                },
                'filter' => Html::activeDropDownList($searchModel, 'kategori', $searchModel->_kategori, ['class' => 'form-control', 'prompt' => '*']),
            ],
            [
                'attribute' => 'id_pegawai',
                'label' => Yii::t('app', 'Id Pegawai'),
                'value' => function (InsentifTidakTetap $model) {
                    return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
                },
            ],
            [
                'attribute' => 'pegawai_id',
                'label' => Yii::t('frontend', 'Pegawai'),
                'value' => function (InsentifTidakTetap $model) {
                    return $model->pegawai->nama_lengkap;
                },
            ],
            [
                'attribute' => 'golongan',
                'label' => Yii::t('frontend', 'Golongan'),
                'value' => function (InsentifTidakTetap $model) {
                    if ($golongan = $model->pegawaiGolongan) {
                        return $golongan->golongan->nama;
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
                'hAlign' => 'center',
                'format' => 'raw'
            ],
            [
                'attribute' => 'busines_unit',
                'label' => Yii::t('frontend', 'Bisnis Unit'),
                'value' => function (InsentifTidakTetap $model) {
                    if ($perjanjianKerja = $model->perjanjianKerja) {
                        return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelPerjanjiankerja->_kontrak,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
                'hAlign' => 'center',
                'format' => 'raw'
            ],
            [
                'attribute' => 'jumlah',
                'value' => function (InsentifTidakTetap $model) {
                    return $model->jumlah;
                },
                'format' => ['decimal', 0],
            ],
            'keterangan:ntext',
            [
                'attribute' => 'payroll_periode_id',
                'label' => Yii::t('frontend', 'Payroll Periode Bayar'),
                'value' => function (InsentifTidakTetap $model) {
                    return $model->payrollPeriode->namaPeriode;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(PayrollPeriode::find()->all(), 'id', 'namaPeriode'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Payroll periode', 'id' => 'grid-insentif-tidak-tetap-search-payroll_periode_id']
            ],
            [
                'attribute' => 'created_by',
                'value' => function (InsentifTidakTetap $model) {
                    return $model->createdBy->username;
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function (InsentifTidakTetap $model) {
                    return date('d-m-Y H:m', strtotime($model->created_at));
                },
            ],
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9],
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'insentif-tidak-tetap' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-insentif-tidak-tetap']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            print_r($e->getMessage());
            Yii::info($e->getMessage(), 'exception');
        } ?>

    </div>
<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
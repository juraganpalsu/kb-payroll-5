<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\InsentifTidakTetap */

$this->title = Yii::t('frontend', 'Create Insentif Tidak Tetap');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Insentif Tidak Tetap'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insentif-tidak-tetap-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

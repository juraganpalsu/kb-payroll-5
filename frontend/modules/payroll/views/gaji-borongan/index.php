<?php

/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $searchModel \frontend\modules\payroll\models\search\GajiBoronganHeaderSearch */


use frontend\modules\payroll\models\GajiBoronganHeader;
use frontend\modules\payroll\models\PayrollPeriode;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Gaji Borongan');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {    
    $('.set-finish').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                        // location.href = dt.data.url;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>
<div class="gaji-borongan-index">
    <p>
        <?= Html::a('<i class="fa fa-hourglass-2"></i>' . Yii::t('app', ' Dibuat'), ['index'], ['class' => 'btn btn-info btn-flat']) ?>
        <?= Html::a('<i class="fa fa-check-square-o"></i>' . Yii::t('app', ' Selesai'), ['index-finish'], ['class' => 'btn btn-info btn-flat']) ?>
    </p>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'payroll_periode_id',
            'label' => Yii::t('frontend', 'Payroll Periode'),
            'value' => function (GajiBoronganHeader $model) {
                return Html::a(Html::tag('strong', $model->payrollPeriode->namaPeriode), ['view', 'id' => $model->id, 'jenis_borongan' => $model->jenis_borongan], ['title' => 'View Detail Data', 'data' => ['pjax' => 0]]);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(PayrollPeriode::find()->all(), 'id', 'namaPeriode'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Payroll periode', 'id' => 'grid-gaji-borongan-payroll_periode_id'],
            'format' => 'raw'
        ],
        [
            'attribute' => 'jenis_borongan',
            'value' => function (GajiBoronganHeader $model) {
                return $model->_jenis_borongan[$model->jenis_borongan];
            },
            'filter' => $searchModel->_jenis_borongan
        ],
        [
            'attribute' => 'finishing_status',
            'value' => function (GajiBoronganHeader $model) {
                if ($model->finishing_status == GajiBoronganHeader::dibuat && $model->created_by == Yii::$app->user->id) {
                    return Html::a($model->_status[$model->finishing_status], ['set-finishing-status', 'id' => $model->id, 'status' => GajiBoronganHeader::selesai], ['class' => 'btn btn-xs btn-block btn-warning set-finish', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan finish data ini ??'), 'method' => 'post']]);
                }
                return Html::a($model->_status[$model->finishing_status], [''], ['class' => 'btn btn-xs btn-block btn-success']);
            },
            'format' => 'raw'
        ],
        [
            'label' => Yii::t('frontend', 'Jumlah'),
            'value' => function (GajiBoronganHeader $model) {
                return $model->hitungJumlahPegawai();
            },
        ],
        'catatan:ntext',
        [
            'attribute' => 'created_by',
            'value' => function (GajiBoronganHeader $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        'created_at',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, GajiBoronganHeader $model) {
                    if ($model->finishing_status != GajiBoronganHeader::selesai) {
                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                    }
                    return '';
                },
            ],
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3],
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'gaji-borongan' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-gaji-borongan']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>
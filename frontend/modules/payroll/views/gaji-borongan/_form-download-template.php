<?php

/**
 * Created by PhpStorm.
 * File Name: _form-download-template.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 20/04/20
 * Time: 16.01
 */


use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\payroll\models\form\GajiBoronganForm;
use frontend\modules\payroll\models\PayrollPeriode;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\payroll\models\GajiBoronganHeader */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#gaji-borongan').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    let win = window.open(dt.data.url, '_blank');
                    win.focus();
                }
                btn.button('reset');
            });
            return false;
        });
    })
JS;

$this->registerJs($js);


?>

<div class="gaji-borongan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'gaji-borongan',
        'action' => Url::to(['download-template-form']),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['download-template-form-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'payroll_periode_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(PayrollPeriode::find()->andWhere(['tipe' => MasterDataDefault::BORONGAN])->orderBy('id')->orderBy('tanggal_awal DESC')->all(), 'id', 'namaPeriode'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Payroll periode')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <?php
                echo $form->field($model, 'jenis_borongan_id')->dropDownList(GajiBoronganForm::gajiBoronganModel()->_jenis_borongan);
                ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('frontend', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

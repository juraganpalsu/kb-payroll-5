<?php

use frontend\modules\payroll\models\GajiBorongan;
use frontend\modules\payroll\models\GajiBoronganHeader;
use frontend\modules\payroll\models\search\GajiBoronganSearch;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model GajiBoronganHeader */
/* @var $searchModel GajiBoronganSearch */
/* @var $dataProvider ActiveDataProvider */
/**
 * @var ActiveDataProvider $providerGajiBorongan
 */

$this->title = $model->payrollPeriode->namaPeriode;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Gaji Borongan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    
});
JS;

$this->registerJs($js);

?>
    <div class="gaji-borongan-view">

        <div class="row">
            <div class="col-sm-9">
                <p>
                    <?= Html::a(Yii::t('frontend', 'Download Template'), ['download-template', 'id' => $model->id], ['class' => 'btn btn-primary', 'title' => Yii::t('frontend', 'Download Template'), 'target' => '_blank']); ?>
                    <?= Html::a(Yii::t('frontend', 'Upload Template'), ['upload-template'], ['class' => 'btn btn-primary', 'title' => Yii::t('frontend', 'Upload Template'), 'target' => '_blank', 'data' => ['header' => Yii::t('frontend', 'Download Template Gaji')]]); ?>
                    <?= Html::a(Yii::t('frontend', 'Download Rekap'), ['rekap-gaji', 'id' => $model->id], ['class' => 'btn btn-success', 'title' => Yii::t('frontend', 'Rekap Gaji'), 'target' => '_blank']); ?>
                </p>
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'payrollPeriode.namaPeriode',
                        'label' => Yii::t('frontend', 'Payroll Periode'),
                    ],
                    [
                        'attribute' => 'jenis_borongan',
                        'value' => function (GajiBoronganHeader $model) {
                            return $model->_jenis_borongan[$model->jenis_borongan];
                        },
                    ],
                    'catatan:ntext',
                    ['attribute' => 'lock', 'visible' => false],
                ];
                try {
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                $gridColumn = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'id_pegawai',
                        'label' => Yii::t('app', 'Id Pegawai'),
                        'value' => function (GajiBorongan $model) {
                            return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
                        },
                    ],
                    [
                        'attribute' => 'pegawai_id',
                        'label' => Yii::t('frontend', 'Pegawai'),
                        'value' => function (GajiBorongan $model) {
                            return $model->pegawai->nama_lengkap;
                        },
                    ],
                    'tanggal',
                    'shift',
                    'gaji',
                    ['attribute' => 'lock', 'visible' => false],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update-detail} {delete-detail}',
                        'buttons' => [
                            'delete-detail' => function ($url, GajiBorongan $model) {
                                if ($model->gajiBoronganHeader->finishing_status != GajiBoronganHeader::selesai) {
                                    return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                                }
                                return '';
                            },
                            'update-detail' => function ($url, GajiBorongan $model) {
                                if ($model->gajiBoronganHeader->finishing_status != GajiBoronganHeader::selesai) {
                                    return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-success btn-flat btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Update Gaji {nama} {tgl}', ['nama' => $model->pegawai->nama_lengkap,'tgl' => \common\components\Helper::idDate($model->tanggal) ])]]);
                                }
                                return '';
                            },
                        ],
                    ],
                ];

                try {
                    $export = ExportMenu::widget([
                        'dataProvider' => $providerGajiBorongan,
                        'columns' => $gridColumn,
//                        'selectedColumns' => [2, 3, 4, 5, 6],
                        'dropdownOptions' => [
                            'class' => 'btn btn-secondary'
                        ],
                        'filename' => 'detail-borongan' . date('dmYHis')
                    ]);
                    echo Gridview::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => $gridColumn,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-gaji-borongan']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [
                            [
                                'content' => ''
                            ],
                            [
                                'content' => $export
                            ],
                        ],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    print_r($e->getMessage());
                    Yii::info($e->getMessage(), 'exception');
                } ?>
            </div>
        </div>
    </div>
<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
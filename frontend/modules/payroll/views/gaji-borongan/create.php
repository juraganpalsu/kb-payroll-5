<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\GajiBorongan */

$this->title = Yii::t('frontend', 'Input Gaji Borongan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Gaji Borongan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gaji-borongan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

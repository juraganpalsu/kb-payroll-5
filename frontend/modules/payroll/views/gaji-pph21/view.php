<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\\GajiPph21 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gaji Pph21', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gaji-pph21-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Gaji Pph21'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'gaji',
        'tunjangan',
        'premi_asuransi',
        'bonus',
        'penghasilan_bruto',
        'biaya_jabatan',
        'iuran_pensiun',
        'pengurangan',
        'penghasilan_netto',
        'penghasilan_netto_setahun',
        'ptkp_status',
        'ptkp_nominal',
        'pkp',
        'pph21_terhutang_setahun',
        'pph21_terhutang',
        [
            'attribute' => 'gajiBulanan.id',
            'label' => 'Gaji Bulanan',
        ],
        [
            'attribute' => 'pegawaiPtkp.id',
            'label' => 'Pegawai Ptkp',
        ],
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>GajiBulanan<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnGajiBulanan = [
        ['attribute' => 'id', 'visible' => false],
        'id_pegawai',
        'tipe_gaji_pokok',
        'pot_keterlambatan',
        'cabang',
        'prorate_komponen',
        'beatroute',
        'insentif_tidak_tetap_id',
        'insentif_tidak_tetap',
        'insentif_pph',
        'insentif_gt',
        'rapel_id',
        'rapel',
        'nama_lengkap',
        'bisnis_unit_s',
        'golongan_s',
        'jenis_kontrak_s',
        'struktur_s',
        'masuk',
        'alfa',
        'ijin',
        'libur',
        'cuti',
        'undifined',
        'dispensasi',
        'tl',
        'is',
        'cm',
        'off',
        'idl',
        'ith',
        'sd',
        'wfh',
        'atl',
        'sh',
        'jam_lembur',
        's2',
        's2_nominal',
        's3',
        's3_nominal',
        'uang_lembur',
        'insentif_lembur',
        'uml',
        'pengali_uml1',
        'pengali_uml2',
        'p_gaji_pokok_id',
        'p_gaji_pokok',
        'p_admin_bank_id',
        'p_admin_bank',
        'pinjaman_angsuran_id',
        'pinjaman_lain_lain_nominal',
        'angsuran_lain_lain_ke',
        'angsuran_potongan_ke',
        'angsuran_koperasi_ke',
        'angsuran_kasbon_ke',
        'angsuran_bank_ke',
        'pinjaman_lain_lain_id',
        'pinjaman_potongan_nominal',
        'pinjaman_potongan_id',
        'pinjaman_koperasi_nominal',
        'pinjaman_koperasi_id',
        'pinjaman_kasbon_nominal',
        'pinjaman_kasbon_id',
        'pinjaman_bank_nominal',
        'pinjaman_bank_id',
        'pinjaman_nominal',
        'angsuran_ke',
        'pinjaman_potongan_atl_id',
        'pinjaman_potongan_atl_nominal',
        'angsuran_potongan_atl_ke',
        'p_insentif_tetap_id',
        'p_premi_hadir_id',
        'p_sewa_mobil_id',
        'p_sewa_motor_id',
        'p_sewa_rumah_id',
        'p_tunjagan_lain_tetap_id',
        'p_tunjagan_lain_lain_id',
        'p_tunjangan_anak_config_id',
        'p_tunjangan_jabatan_id',
        'p_tunjangan_kost_id',
        'p_tunjangan_loyalitas_config_id',
        'p_tunjangan_pulsa_id',
        'p_uang_makan_id',
        'p_uang_transport_id',
        'p_insentif_tetap',
        'p_premi_hadir',
        'p_sewa_mobil',
        'p_sewa_motor',
        'p_sewa_rumah',
        'p_tunjagan_lain_tetap',
        'p_tunjagan_lain_lain',
        'p_tunjangan_anak',
        'p_tunjangan_jabatan',
        'p_tunjangan_kost',
        'p_tunjangan_loyalitas',
        'p_tunjangan_pulsa',
        'p_uang_makan',
        'p_uang_transport',
        'hari_kerja_aktif',
        'pegawai_bpjs_id',
        'pegawai_bpjs_kes',
        'pegawai_bpjs_tk',
        'pegawai_bank_id',
        'pegawai_bank_nama',
        'pegawai_bank_nomor',
        'jumlah_telat',
        'libur_hadir',
        'libur_hadir_s1',
        'libur_hadir_s2',
        'libur_hadir_s3',
        'atl_s1',
        'atl_s2',
        'atl_s3',
        'sh_s2',
        'sh_s3',
        'status_pernikahan',
        'jumlah_anak',
        'proses_gaji_id',
        'pegawai_id',
        'perjanjian_kerja_id',
        'pegawai_golongan_id',
        'pegawai_struktur_id',
        'pegawai_sistem_kerja_id',
        'cost_center_id',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->gajiBulanan,
        'attributes' => $gridColumnGajiBulanan    ]);
    ?>
    <div class="row">
        <h4>PegawaiPtkp<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPegawaiPtkp = [
        ['attribute' => 'id', 'visible' => false],
        'tahun',
        'status',
        'ptkp',
        'keterangan',
        'pegawai_id',
        'perjanjian_kerja_id',
        'pegawai_golongan_id',
        'pegawai_struktur_id',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->pegawaiPtkp,
        'attributes' => $gridColumnPegawaiPtkp    ]);
    ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\\GajiPph21 */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="gaji-pph21-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'gaji')->textInput(['maxlength' => true, 'placeholder' => 'Gaji']) ?>

    <?= $form->field($model, 'tunjangan')->textInput(['maxlength' => true, 'placeholder' => 'Tunjangan']) ?>

    <?= $form->field($model, 'premi_asuransi')->textInput(['maxlength' => true, 'placeholder' => 'Premi Asuransi']) ?>

    <?= $form->field($model, 'bonus')->textInput(['maxlength' => true, 'placeholder' => 'Bonus']) ?>

    <?= $form->field($model, 'penghasilan_bruto')->textInput(['maxlength' => true, 'placeholder' => 'Penghasilan Bruto']) ?>

    <?= $form->field($model, 'biaya_jabatan')->textInput(['maxlength' => true, 'placeholder' => 'Biaya Jabatan']) ?>

    <?= $form->field($model, 'iuran_pensiun')->textInput(['maxlength' => true, 'placeholder' => 'Iuran Pensiun']) ?>

    <?= $form->field($model, 'pengurangan')->textInput(['maxlength' => true, 'placeholder' => 'Pengurangan']) ?>

    <?= $form->field($model, 'penghasilan_netto')->textInput(['maxlength' => true, 'placeholder' => 'Penghasilan Netto']) ?>

    <?= $form->field($model, 'penghasilan_netto_setahun')->textInput(['maxlength' => true, 'placeholder' => 'Penghasilan Netto Setahun']) ?>

    <?= $form->field($model, 'ptkp_status')->textInput(['maxlength' => true, 'placeholder' => 'Ptkp Status']) ?>

    <?= $form->field($model, 'ptkp_nominal')->textInput(['maxlength' => true, 'placeholder' => 'Ptkp Nominal']) ?>

    <?= $form->field($model, 'pkp')->textInput(['maxlength' => true, 'placeholder' => 'Pkp']) ?>

    <?= $form->field($model, 'pph21_terhutang_setahun')->textInput(['maxlength' => true, 'placeholder' => 'Pph21 Terhutang Setahun']) ?>

    <?= $form->field($model, 'pph21_terhutang')->textInput(['maxlength' => true, 'placeholder' => 'Pph21 Terhutang']) ?>

    <?= $form->field($model, 'gaji_bulanan_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\GajiBulanan::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Gaji bulanan'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'pegawai_ptkp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\PegawaiPtkp::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Pegawai ptkp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'created_by_pk')->textInput(['maxlength' => true, 'placeholder' => 'Created By Pk']) ?>

    <?= $form->field($model, 'created_by_struktur')->textInput(['placeholder' => 'Created By Struktur']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $searchModel GajiPph21Search */

use common\models\Golongan;
use frontend\modules\payroll\models\GajiPph21;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\search\GajiPph21Search;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Data Gaji Pph21';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="gaji-pph21-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'payroll_periode_id',
            'label' => Yii::t('frontend', 'Payroll Periode'),
            'value' => function (GajiPph21 $model) {
                return $model->gajiBulanan->prosesGaji->payrollPeriode->namaPeriode;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(PayrollPeriode::find()->all(), 'id', 'namaPeriode'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('payroll_periode_id'), 'id' => 'grid-gaji-pph21-search-payroll_periode_id'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (GajiPph21 $model) {
                return $model->pegawaiPtkp->pegawai->nama_lengkap;
            },
        ],
        [
            'attribute' => 'golongan',
            'value' => function (GajiPph21 $model) {
                if ($golongan = $model->pegawaiPtkp->pegawai->getGolonganTerakhir()) {
                    return $golongan->golongan->nama;
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (GajiPph21 $model) {
                if ($perjanjianKerja = $model->pegawaiPtkp->pegawai->getPerjanjianKerjaTerakhir()) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => (new PerjanjianKerja())->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'gaji',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'tunjangan_pph',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'tunjangan',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'premi_asuransi',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'bonus',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'penghasilan_bruto',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'biaya_jabatan',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'iuran_pensiun',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'pengurangan',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'penghasilan_netto',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'penghasilan_netto_setahun',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'ptkp_status',
            'value' => function (GajiPph21 $model) {
                return strtoupper($model->ptkp_status);
            },
        ],
        [
            'attribute' => 'ptkp_nominal',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'pkp',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'pph21_terhutang_setahun',
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'pph21_terhutang',
            'format' => ['decimal', 0],
        ],
        ['attribute' => 'lock', 'visible' => false],
//        [
//            'class' => 'yii\grid\ActionColumn',
//        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
//            'selectedColumns' => [2, 3, 4, 5],
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-gaji-pph21' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proses-gaji']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

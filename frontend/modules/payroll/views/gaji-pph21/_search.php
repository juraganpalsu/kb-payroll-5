<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\search\GajiPph21Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-gaji-pph21-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'gaji')->textInput(['maxlength' => true, 'placeholder' => 'Gaji']) ?>

    <?= $form->field($model, 'tunjangan_pph')->textInput(['maxlength' => true, 'placeholder' => 'Tunjangan Pph']) ?>

    <?= $form->field($model, 'tunjangan')->textInput(['maxlength' => true, 'placeholder' => 'Tunjangan']) ?>

    <?= $form->field($model, 'premi_asuransi')->textInput(['maxlength' => true, 'placeholder' => 'Premi Asuransi']) ?>

    <?php /* echo $form->field($model, 'bonus')->textInput(['maxlength' => true, 'placeholder' => 'Bonus']) */ ?>

    <?php /* echo $form->field($model, 'penghasilan_bruto')->textInput(['maxlength' => true, 'placeholder' => 'Penghasilan Bruto']) */ ?>

    <?php /* echo $form->field($model, 'biaya_jabatan')->textInput(['maxlength' => true, 'placeholder' => 'Biaya Jabatan']) */ ?>

    <?php /* echo $form->field($model, 'iuran_pensiun')->textInput(['maxlength' => true, 'placeholder' => 'Iuran Pensiun']) */ ?>

    <?php /* echo $form->field($model, 'pengurangan')->textInput(['maxlength' => true, 'placeholder' => 'Pengurangan']) */ ?>

    <?php /* echo $form->field($model, 'penghasilan_netto')->textInput(['maxlength' => true, 'placeholder' => 'Penghasilan Netto']) */ ?>

    <?php /* echo $form->field($model, 'penghasilan_netto_setahun')->textInput(['maxlength' => true, 'placeholder' => 'Penghasilan Netto Setahun']) */ ?>

    <?php /* echo $form->field($model, 'ptkp_status')->textInput(['maxlength' => true, 'placeholder' => 'Ptkp Status']) */ ?>

    <?php /* echo $form->field($model, 'ptkp_nominal')->textInput(['maxlength' => true, 'placeholder' => 'Ptkp Nominal']) */ ?>

    <?php /* echo $form->field($model, 'pkp')->textInput(['maxlength' => true, 'placeholder' => 'Pkp']) */ ?>

    <?php /* echo $form->field($model, 'pph21_terhutang_setahun')->textInput(['maxlength' => true, 'placeholder' => 'Pph21 Terhutang Setahun']) */ ?>

    <?php /* echo $form->field($model, 'pph21_terhutang')->textInput(['maxlength' => true, 'placeholder' => 'Pph21 Terhutang']) */ ?>

    <?php /* echo $form->field($model, 'gaji_bulanan_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\models\GajiBulanan::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Gaji bulanan'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'pegawai_ptkp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\models\PegawaiPtkp::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Pegawai ptkp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'created_by_pk')->textInput(['maxlength' => true, 'placeholder' => 'Created By Pk']) */ ?>

    <?php /* echo $form->field($model, 'created_by_struktur')->textInput(['placeholder' => 'Created By Struktur']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

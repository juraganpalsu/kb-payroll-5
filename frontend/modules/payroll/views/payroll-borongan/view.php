<?php

use common\components\Helper;
use common\components\Status;
use kartik\dialog\DialogAsset;
use mdm\admin\AnimateAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollBorongan */

$this->title = $model->jenisBorongan->nama_produk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Borongan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


DialogAsset::register($this);
AnimateAsset::register($this);
YiiAsset::register($this);
//print_r($model->getListPegawai());
$opts = Json::htmlEncode([
    'items' => $model->getListPegawai(),
]);
$this->registerJs("var _opts = {$opts};");
$this->registerJs($this->render('_script.js'));
$animateIcon = ' <i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>';

$js = <<<JS
$(function ($) {    
    $('.btn-submit').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        let conf = confirm(btn.data('confirm'));
            if(conf){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }else{
                        alert(dt.message)
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        return false;
    });
    
});
JS;

$this->registerJs($js);


?>
<div class="payroll-borongan-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Payroll Borongan') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?php if ($model->last_status == Status::OPEN) { ?>
                <?= Html::a(Yii::t('frontend', 'Submit'), ['submit', 'id' => $model->id], ['id' => 'asdsad', 'class' => 'btn btn-xs btn-warning btn-submit btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure want to submit this item?'), 'pjax' => 0]]) ?>
                <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-xs btn-flat',
                    'data' => [
                        'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'jenisBorongan.nama_produk',
                    'label' => Yii::t('frontend', 'Jenis Borongan'),
                ],
                [
                    'attribute' => 'tanggal',
                    'value' => Helper::idDate($model->tanggal)
                ],
                'shift',
                'mulai',
                'selesai',
                [
                    'attribute' => 'hasil',
                    'value' => Helper::asDecimal($model->hasil)
                ],
                [
                    'attribute' => 'upah_per_orang',
                    'value' => Helper::asDecimal($model->upah_per_orang)
                ],
                'catatan:ntext',
                ['attribute' => 'lock', 'visible' => false],
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]);
            ?>
        </div>
    </div>
</div>
<div class="assignment-index">
    <div class="row">
        <div class="col-sm-5">
            <input class="form-control search" data-target="available"
                   placeholder="<?= Yii::t('frontend', 'Search for available'); ?>">
            <select multiple size="20" class="form-control list" data-target="available"> </select>
        </div>
        <div class="col-sm-1">
            <br><br>
            <?= ($model->last_status == Status::OPEN) ? Html::a('&gt;&gt;' . $animateIcon, ['assign', 'id' => (string)$model->id], [
                'class' => 'btn btn-success btn-assign',
                'data-target' => 'available',
                'title' => Yii::t('frontend', 'Assign'),
            ]) : Html::a('&gt;&gt;' . $animateIcon, '#', ['class' => 'btn btn-success', 'disabled' => 'disabled']); ?>
            <br><br>
            <?= ($model->last_status == Status::OPEN) ? Html::a('&lt;&lt;' . $animateIcon, ['revoke', 'id' => (string)$model->id], [
                'class' => 'btn btn-danger btn-assign',
                'data-target' => 'assigned',
                'title' => Yii::t('frontend', 'Remove'),
            ]) : Html::a('&lt;&lt;' . $animateIcon, '#', ['class' => 'btn btn-danger', 'disabled' => 'disabled']); ?>
        </div>
        <div class="col-sm-5">
            <input class="form-control search" data-target="assigned"
                   placeholder="<?= Yii::t('frontend', 'Search for assigned'); ?>">
            <select multiple size="20" class="form-control list" data-target="assigned"> </select>
        </div>
    </div>
</div>
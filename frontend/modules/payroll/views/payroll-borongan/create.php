<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollBorongan */

$this->title = Yii::t('frontend', 'Create Payroll Borongan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Borongan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-borongan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

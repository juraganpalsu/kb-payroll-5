<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\PayrollBoronganSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Helper;
use common\components\Status;
use common\models\TimeTable;
use frontend\modules\payroll\models\JenisBorongan;
use frontend\modules\payroll\models\PayrollBorongan;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Payroll Borongan');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>
    <div class="payroll-borongan-index">

        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'tanggal',
                'value' => function (PayrollBorongan $model) {
                    return Helper::idDate($model->tanggal);
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            [
                'attribute' => 'shift',
                'value' => function (PayrollBorongan $model) {
                    return (new TimeTable())->_shift[$model->shift];
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => (new TimeTable())->_shift,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('shift'), 'id' => 'grid-absensi-tidak-lengkap-search-shift']
            ],
            'mulai',
            'selesai',
            [
                'attribute' => 'hasil',
                'value' => function (PayrollBorongan $model) {
                    return Helper::asDecimal($model->hasil);
                }
            ],
            [
                'attribute' => 'upah_per_orang',
                'value' => function (PayrollBorongan $model) {
                    return Helper::asDecimal($model->upah_per_orang);
                }
            ],
            'catatan:ntext',
            [
                'attribute' => 'jenis_borongan_id',
                'label' => Yii::t('frontend', 'Jenis Borongan'),
                'value' => function ($model) {
                    return $model->jenisBorongan->nama_produk;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(JenisBorongan::find()->asArray()->all(), 'id', 'nama_produk'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Jenis borongan', 'id' => 'grid-payroll-borongan-search-jenis_borongan_id']
            ],
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
                'buttons' => [
                    'view' => function ($url, PayrollBorongan $model) {
                        return Html::a(Yii::t('frontend', 'View'), $url, ['class' => 'btn btn-xs btn-success btn-flat']);
                    },
                    'delete' => function ($url, PayrollBorongan $model) {
                        if ($model->last_status == Status::OPEN) {
                            return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                        }
                        return '';
                    },
                ],
            ],
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'payroll-asuransi-lain' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payroll-borongan']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Buat Payroll    Borongan')]]) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
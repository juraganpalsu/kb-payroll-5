<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\PayrollBonusSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\payroll\models\PayrollBonus;
use frontend\modules\payroll\models\PayrollPeriode;
use common\models\Golongan;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('frontend', 'Payroll Bonus');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
});
JS;

$this->registerJs($js);
?>
<div class="payroll-bonus-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'tahun',
        [
            'attribute' => 'golongan_id',
            'label' => Yii::t('frontend', 'Golongan'),
            'value' => function (PayrollBonus $model) {
                return $model->golongan->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Golongan', 'id' => 'grid-payroll-bpjs-search-golongan_id']
        ],
        [
            'attribute' => 'bisnis_unit',
            'value' => function (PayrollBonus $model) {
                return PayrollBonus::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => PayrollBonus::modelPerjanjianKerja()->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Bisnis Unit'), 'id' => 'grid-thr-search-bisnis_unit']
        ],
        [
            'label' => Yii::t('frontend', 'Jumlah'),
            'value' => function (PayrollBonus $model) {
                return $model->hitungJumlahPegawai();
            },
        ],
        [
            'attribute' => 'payroll_periode_id',
            'label' => Yii::t('frontend', 'Payroll Periode'),
            'value' => function (PayrollBonus $model) {
                return $model->payrollPeriode->namaPeriode;
            },
        ],
        'catatan:ntext',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {delete}'
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3],
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'peraturan-perusahaan' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payroll-bpjs']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Input Data Bonus')]]) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
</div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

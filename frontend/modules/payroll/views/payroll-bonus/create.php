<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollBonus */

$this->title = 'Create Payroll Bonus';
$this->params['breadcrumbs'][] = ['label' => 'Payroll Bonus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-bonus-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

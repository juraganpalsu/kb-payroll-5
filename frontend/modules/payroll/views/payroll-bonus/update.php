<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollBonus */

$this->title = 'Update Payroll Bonus: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payroll Bonus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payroll-bonus-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

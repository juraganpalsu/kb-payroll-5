<?php

use frontend\modules\payroll\models\PayrollBonus;
use frontend\modules\payroll\models\PayrollBonusDetail;
use kartik\export\ExportMenu;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollBonus */

/**
 * @var ActiveDataProvider $providerPayrollBonusDetail
 */

$this->title = Yii::t('frontend','Bonus '). $model->tahun . '*' . $model->golongan->nama . '*' . PayrollBonus::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
$this->params['breadcrumbs'][] = ['label' => 'Payroll Bonus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
});
JS;

$this->registerJs($js);
?>
<div class="payroll-bonus-view">

    <div class="row">
        <div class="col-sm-6">
            <?= Html::a(Yii::t('frontend', 'Download Template'), ['download-template', 'id' => $model->id], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
            <?= Html::a(Yii::t('frontend', 'Upload Template'), ['upload-template', 'id' => $model->id], ['class' => 'btn btn-success call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Upload template BPJS #{for}', ['for' => $this->title])]]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'payrollPeriode.namaPeriode',
                    'label' => Yii::t('frontend', 'Payroll Periode'),
                ],
                [
                    'attribute' => 'golongan.nama',
                    'label' => Yii::t('frontend', 'Golongan'),
                ], [
                    'attribute' => 'bisnis_unit',
                    'value' => function (PayrollBonus $model) {
                        return PayrollBonus::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
                    },
                ],
                'catatan:ntext',
                ['attribute' => 'lock', 'visible' => false],
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumnPayrollBonusDetail = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'pegawai_id',
                    'value' => function (PayrollBonusDetail $model) {
                        return $model->pegawai->nama;
                    }
                ],
                'nominal',
                ['attribute' => 'lock', 'visible' => false],
            ];

            try {
                $export = ExportMenu::widget([
                    'dataProvider' => $providerPayrollBonusDetail,
                    'columns' => $gridColumnPayrollBonusDetail,
                    'selectedColumns' => [2, 3],
                    'dropdownOptions' => [
                        'class' => 'btn btn-secondary'
                    ],
                    'filename' => 'peraturan-perusahaan' . date('dmYHis')
                ]);
                echo Gridview::widget([
                    'dataProvider' => $providerPayrollBonusDetail,
                    'columns' => $gridColumnPayrollBonusDetail,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payroll-bpjs-detail']],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'toolbar' => [
                        [
                            'content' => ''
                        ],
                        [
                            'content' => $export
                        ],
                    ],
                    'panel' => [
                        'heading' => false,
                        'before' => '{summary}',
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                ]);
            } catch (Exception $e) {
                print_r($e->getMessage());
                Yii::info($e->getMessage(), 'exception');
            } ?>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

<?php

/**
 * Created by PhpStorm.
 * File Name: _form-payroll-bpjs-tk.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-09-05
 * Time: 8:55 AM
 */


use frontend\modules\payroll\models\PayrollBpjsDetail;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model PayrollBpjsDetail */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#form-input').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.reload();
                    //location.href = dt.data.url;
                }
                btn.button('reset');
            });
            return false;
        });
    })
JS;

$this->registerJs($js);


?>

<div class="form-input-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-input',
        'action' => Url::to(['update-detail-tk', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['cou-detail-tk-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'jkk_nominal') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'jkn_nominal') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'jht_nominal') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'jht_nominal_tk') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'iuran_pensiun_nominal') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'iuran_pensiun_tk_nominal') ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('frontend', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>



<?php

use common\models\Golongan;
use frontend\modules\payroll\models\PayrollBpjs;
use frontend\modules\payroll\models\PayrollPeriode;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollBpjs */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#form-input').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
                btn.button('reset');
            });
            return false;
        });
    })
JS;

$this->registerJs($js);

?>
<div class="payroll-bpjs-form">
    <div class="row">
        <div class="col-md-6">

            <?php $form = ActiveForm::begin([
                'id' => 'form-input',
                'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
                'type' => ActiveForm::TYPE_VERTICAL,
                'formConfig' => ['showErrors' => false],
                'enableAjaxValidation' => true,
                'validationUrl' => Url::to(['create-or-update-validation']),
                'fieldConfig' => ['showLabels' => true],
            ]); ?>

            <?= $form->errorSummary($model); ?>

            <?php try { ?>

                <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'payroll_periode_id')->widget(Select2::class, [
                            'data' => ArrayHelper::map(PayrollPeriode::find()->orderBy('id')->all(), 'id', 'namaPeriode'),
                            'options' => ['placeholder' => Yii::t('frontend', 'Choose Payroll periode')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'golongan_id')->widget(Select2::class, [
                            'data' => ArrayHelper::map(Golongan::find()->orderBy('id')->all(), 'id', 'nama'),
                            'options' => ['placeholder' => Yii::t('frontend', 'Choose Golongan')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <?= $form->field($model, 'bisnis_unit')->dropDownList(PayrollBpjs::modelPerjanjianKerja()->_kontrak) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>


            <?php } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            } ?>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr class="warning">
                            <th scope="col">#</th>
                            <th scope="col">Tipe Gaji</th>
                            <th scope="col">Keterangan</th>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>POKOK_FIX_B</td>
                            <td>Gaji bulanan periode 21-20 gol 2 ketas</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>HARIAN_H</td>
                            <td>Gaji harian periode 16-15 gol 1 bca</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>POKOK_FIX_H</td>
                            <td>Gaji bulanan periode harian 16-15 gol 1 kbu produksi dan gol 1 bca operator</td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td>HARIAN_B</td>
                            <td>Gaji harian periode bulanan 21-20 spg</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>

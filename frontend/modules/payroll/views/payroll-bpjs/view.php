<?php

use common\components\Helper;
use common\components\Status;
use frontend\modules\payroll\models\PayrollBpjs;
use frontend\modules\payroll\models\PayrollBpjsDetail;
use frontend\modules\payroll\models\PayrollBpjsTkDetail;
use frontend\modules\payroll\models\search\PayrollBpjsDetailSearch;
use frontend\modules\payroll\models\search\PayrollBpjsTkDetailSearch;
use frontend\modules\pegawai\models\PerjanjianKerja;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollBpjs */
/* @var $searchModel PayrollBpjsDetailSearch */
/* @var $dataProvider ActiveDataProvider */
/* @var $dataProviderBpjsKetenagakerjaan ActiveDataProvider */
/* @var $searchModelKetenagakerjaan PayrollBpjsTkDetailSearch */

/**
 * @var ActiveDataProvider $providerPayrollBpjsDetail
 */

$this->title = $model->payrollPeriode->namaPeriode . '*' . $model->golongan->nama . '*' . PayrollBpjs::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Bpjs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.btn-generate-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    
    $('.btn-delete-detail-tk').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    
    $('.btn-download-bpjs').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                        btn.button('reset');
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>
    <div class="payroll-bpjs-view">

        <div class="row">
            <div class="col-sm-6">
                <?php // Html::a(Yii::t('frontend', 'Download Template'), ['download-template', 'id' => $model->id], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
                <?php // Html::a(Yii::t('frontend', 'Upload Template'), ['upload-template', 'id' => $model->id], ['class' => 'btn btn-success call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Upload template BPJS #{for}', ['for' => $this->title])]]) ?>
                <?= ($model->last_status == Status::DEFLT) ? Html::a(Yii::t('frontend', 'Generate Detail'), ['generate-iuran-bpjs', 'id' => $model->id], ['class' => 'btn btn-warning btn-flat btn-generate-detail', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin melakukan aksi ini ??'), 'method' => 'post']]) : ''; ?>
                <?= Html::a(Yii::t('frontend', 'Download'), ['download-bpjs-btn', 'id' => $model->id], ['class' => 'btn btn-success btn-flat btn-download-bpjs', 'data' => ['confirm' => Yii::t('frontend', 'Download data BPJS ??'), 'method' => 'post']]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'payrollPeriode.namaPeriode',
                        'label' => Yii::t('frontend', 'Payroll Periode'),
                    ],
                    [
                        'attribute' => 'golongan.nama',
                        'label' => Yii::t('frontend', 'Golongan'),
                    ],
                    [
                        'attribute' => 'bisnis_unit',
                        'value' => function (PayrollBpjs $model) {
                            return PayrollBpjs::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
                        },
                    ],
                    'catatan:ntext',
                    ['attribute' => 'lock', 'visible' => false],
                ];
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                $gridColumnPayrollBpjsDetail = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'id_pegawai',
                        'label' => Yii::t('app', 'Id Pegawai'),
                        'value' => function (PayrollBpjsDetail $model) {
                            return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
                        },
                    ],
                    [
                        'label' => Yii::t('frontend', 'Nama'),
                        'attribute' => 'pegawai_id',
                        'value' => function (PayrollBpjsDetail $model) {
                            return $model->pegawai->nama;
                        }
                    ],
                    [
                        'attribute' => 'pegawaiBpjsT.nomor',
                        'label' => Yii::t('frontend', 'Nomor BPJS Kesehatan')
                    ],
                    [
                        'attribute' => 'gaji_pokok',
                        'value' => function (PayrollBpjsDetail $model) {
                            return Helper::asDecimal($model->gaji_pokok);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Iuran Perusahaan(4%)'),
                        'attribute' => 'iuran_nominal',
                        'value' => function (PayrollBpjsDetail $model) {
                            return Helper::asDecimal($model->iuran_nominal);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Iuran Karyawan(1%)'),
                        'attribute' => 'iuran_nominal_tk',
                        'value' => function (PayrollBpjsDetail $model) {
                            return Helper::asDecimal($model->iuran_nominal_tk);
                        }
                    ],
                    [
                        'attribute' => 'total_iuran',
                        'value' => function (PayrollBpjsDetail $model) {
                            return Helper::asDecimal($model->total_iuran);
                        }
                    ],
                    ['attribute' => 'lock', 'visible' => false],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update-detail} {delete-detail}',
                        'buttons' => [
                            'delete-detail' => function ($url, PayrollBpjsDetail $model) {
                                if ($model->bpjs->last_status == Status::DEFLT) {
                                    return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                                }
                                return '';
                            },
                            'update-detail' => function ($url, PayrollBpjsDetail $model) {
                                if ($model->bpjs->last_status == Status::DEFLT) {
                                    return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-success btn-flat btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Update BPJS KESEHATAN {nama}', ['nama' => $model->pegawai->nama_lengkap])]]);
                                }
                                return '';
                            },
                        ],
                    ],
                ];

                try {
                    $export = ExportMenu::widget([
                        'dataProvider' => $providerPayrollBpjsDetail,
                        'columns' => $gridColumnPayrollBpjsDetail,
//                        'selectedColumns' => [2, 3, 4, 5, 6],
                        'dropdownOptions' => [
                            'class' => 'btn btn-secondary'
                        ],
                        'filename' => 'detail-bpjs' . $this->title . date('dmYHis')
                    ]);
                    echo Gridview::widget([
                        'id' => 'grid-bpjs-kesehatan',
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumnPayrollBpjsDetail,
                        'filterModel' => $searchModel,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payroll-bpjs-detail']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [
                            [
                                'content' => ''
                            ],
                            [
                                'content' => $export
                            ],
                        ],
                        'panel' => [
                            'heading' => Yii::t('frontend', 'BPJS Kesehatan'),
                            'before' => false,
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                $gridColumnPayrollBpjsKetenagakerjaan = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'id_pegawai',
                        'label' => Yii::t('app', 'Id Pegawai'),
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
                        },
                    ],
                    [
                        'label' => Yii::t('frontend', 'Nama'),
                        'attribute' => 'pegawai_id',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return $model->pegawai->nama;
                        }
                    ],
                    [
                        'attribute' => 'pegawai_bpjs_id',
                        'label' => Yii::t('frontend', 'Nomor BPJS TK'),
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return $model->pegawaiBpjs->nomor;
                        }
                    ],
                    [
                        'attribute' => 'gaji_pokok',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return Helper::asDecimal($model->gaji_pokok);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'JKK {bu}', ['bu' => $model->bisnis_unit == PerjanjianKerja::KBU ? '(0.89%)' : '(0.24%)']),
                        'attribute' => 'jkk_nominal',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return Helper::asDecimal($model->jkk_nominal);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'JKM(0.30%)'),
                        'attribute' => 'jkn_nominal',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return Helper::asDecimal($model->jkn_nominal);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'JHT Perusahaan(3.7%)'),
                        'attribute' => 'jkn_nominal',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return Helper::asDecimal($model->jht_nominal);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'JHT TK(2%)'),
                        'attribute' => 'jkn_nominal',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return Helper::asDecimal($model->jht_nominal_tk);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'IP Perusahaan(2%)'),
                        'attribute' => 'iuran_pensiun_nominal',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return Helper::asDecimal($model->iuran_pensiun_nominal);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'IP TK(1%)'),
                        'attribute' => 'iuran_pensiun_nominal',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return Helper::asDecimal($model->iuran_pensiun_tk_nominal);
                        }
                    ],
                    [
                        'attribute' => 'total_iuran',
                        'value' => function (PayrollBpjsTkDetail $model) {
                            return Helper::asDecimal($model->total_iuran);
                        }
                    ],
                    ['attribute' => 'lock', 'visible' => false],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update-detail-tk} {delete-detail-tk}',
                        'buttons' => [
                            'delete-detail-tk' => function ($url, PayrollBpjsTkDetail $model) {
                                if ($model->payrollBpjs->last_status == Status::DEFLT) {
                                    return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete-detail-tk', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                                }
                                return '';
                            },
                            'update-detail-tk' => function ($url, PayrollBpjsTkDetail $model) {
                                if ($model->payrollBpjs->last_status == Status::DEFLT) {
                                    return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-success btn-flat btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Update BPJS KETENAGAKERJAAN {nama}', ['nama' => $model->pegawai->nama_lengkap])]]);
                                }
                                return '';
                            },
                        ],
                    ],
                ];

                try {
                    echo Gridview::widget([
                        'id' => 'grid-bpjs-ketenagakerjaan',
                        'dataProvider' => $dataProviderBpjsKetenagakerjaan,
                        'columns' => $gridColumnPayrollBpjsKetenagakerjaan,
                        'filterModel' => $searchModelKetenagakerjaan,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payroll-bpjs-ketenagakerjaan-detail']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [
                            [
                                'content' => ''
                            ],
                        ],
                        'panel' => [
                            'heading' => Yii::t('frontend', 'BPJS Ketenagakerjaan'),
                            'before' => false,
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                } ?>
            </div>
        </div>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollBpjs */

$this->title = Yii::t('frontend', 'Create Payroll Bpjs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Bpjs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-bpjs-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

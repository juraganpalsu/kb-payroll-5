<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollDiluarProses */

$this->title = Yii::t('frontend', 'Create Payroll Diluar Proses');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Diluar Proses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-diluar-proses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $searchModel PayrollDiluarProsesSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\payroll\models\PayrollDiluarProses;
use frontend\modules\payroll\models\PayrollPeriode;
use frontend\modules\payroll\models\search\PayrollDiluarProsesSearch;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Payroll Diluar Proses');
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.get-template').on('click', function(e) {
        e.preventDefault();
        let btn = $(this);
        $.post(btn.attr('href'))
        .done(function(dt) {
            var win = window.open(dt.url);
            win.focus();
            return true;
        }).fail(function() {
            console.log('Gagal')
        })
    });
});
JS;

$this->registerJs($js);
?>
<div class="payroll-diluar-proses-index">

    <p>
        <?= Html::a(Yii::t('frontend', 'Download Template Payroll'), ['get-template'], ['class' => 'btn btn-warning get-template']) ?>
        <?= Html::a(Yii::t('frontend', 'Upload Template'), ['upload-template'], ['class' => 'btn btn-success call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Upload Template')]]) ?>
    </p>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'id_pegawai',
            'value' => function (PayrollDiluarProses $model) {
                return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'value' => function (PayrollDiluarProses $model) {
                return $model->pegawai->nama_lengkap;
            },
        ],
        [
            'attribute' => 'jenis',
            'value' => function (PayrollDiluarProses $model) {
                return $model->_jenis[$model->jenis];
            },
        ],
        'jumlah',
        [
            'attribute' => 'tanggal_bayar',
            'value' => function (PayrollDiluarProses $model) {
                return date('d-m-Y', strtotime($model->tanggal_bayar));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'proses_gaji_id',
            'label' => Yii::t('frontend', 'Payroll Periode (Kurang)'),
            'value' => function (PayrollDiluarProses $model) {
                return $model->prosesGaji->payrollPeriode->namaPeriode;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(PayrollPeriode::find()->all(), 'id', 'namaPeriode'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Payroll periode', 'id' => 'grid--proses_gaji_id']
        ],
        'keterangan:ntext',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3],
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'gaji-borongan' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-gaji-borongan']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>
<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

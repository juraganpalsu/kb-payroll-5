<?php

/**
 * Created by PhpStorm.
 * File Name: my-payroll.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/29/2021
 * Time: 09:20 PM
 */

use frontend\modules\payroll\models\GajiBulanan;
use frontend\modules\payroll\models\search\GajiBulananSearch;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;


/**
 *
 * @var GajiBulananSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */


$this->title = Yii::t('frontend', 'Daftar Penggajian Saya');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'id_pegawai',
                'label' => Yii::t('frontend', 'Id Pegawai'),
                'value' => function (GajiBulanan $model) {
                    return Html::tag('strong', str_pad($model->id_pegawai, 8, '0', STR_PAD_LEFT));
                },
                'format' => 'raw'
            ],
            [
                'label' => Yii::t('frontend', 'Payroll Periode'),
                'value' => function (GajiBulanan $model) {
                    return Html::a(Html::tag('strong', str_pad($model->prosesGaji->payrollPeriode->namaPeriode, 8, '0', STR_PAD_LEFT)), ['/payroll/proses-gaji/print-slip', 'id' => $model->id], ['data' => ['pjax' => 0], 'title' => 'Print Slip', 'target' => '_blank']);
                },
                'format' => 'raw'
            ],
        ];
        ?>
        <?php
        try {
            echo GridView::widget([
                'id' => 'grid-gaji-bulanan',
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-gaji-bulanan']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>
    </div>
</div>

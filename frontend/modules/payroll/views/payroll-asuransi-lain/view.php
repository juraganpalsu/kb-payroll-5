<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollAsuransiLain */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Asuransi Lain'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-asuransi-lain-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Payroll Asuransi Lain').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'catatan:ntext',
        [
            'attribute' => 'payrollPeriode.id',
            'label' => Yii::t('frontend', 'Payroll Periode'),
        ],
        [
            'attribute' => 'golongan.id',
            'label' => Yii::t('frontend', 'Golongan'),
        ],
        'bisnis_unit',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Golongan<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnGolongan = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'urutan',
        'keterangan',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->golongan,
        'attributes' => $gridColumnGolongan    ]);
    ?>
    <div class="row">
        <h4>PayrollPeriode<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPayrollPeriode = [
        ['attribute' => 'id', 'visible' => false],
        'tipe',
        'tanggal_awal',
        'tanggal_akhir',
        'catatan:ntext',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->payrollPeriode,
        'attributes' => $gridColumnPayrollPeriode    ]);
    ?>
    
    <div class="row">
<?php
if($providerPayrollAsuransiLainDetail->totalCount){
    $gridColumnPayrollAsuransiLainDetail = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'potongan_perusahaan',
            'potongan_karyawan',
            'keterangan:ntext',
                        [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('frontend', 'Pegawai')
            ],
            [
                'attribute' => 'perjanjianKerja.id',
                'label' => Yii::t('frontend', 'Perjanjian Kerja')
            ],
            [
                'attribute' => 'pegawaiGolongan.id',
                'label' => Yii::t('frontend', 'Pegawai Golongan')
            ],
            [
                'attribute' => 'pegawaiStruktur.id',
                'label' => Yii::t('frontend', 'Pegawai Struktur')
            ],
            [
                'attribute' => 'pegawaiAsuransiLain.id',
                'label' => Yii::t('frontend', 'Pegawai Asuransi Lain')
            ],
            'created_by_pk',
            'created_by_struktur',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPayrollAsuransiLainDetail,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payroll-asuransi-lain-detail']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('frontend', 'Payroll Asuransi Lain Detail')),
        ],
        'export' => false,
        'columns' => $gridColumnPayrollAsuransiLainDetail
    ]);
}
?>

    </div>
</div>

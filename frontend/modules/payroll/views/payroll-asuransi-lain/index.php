<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\PayrollAsuransiLainSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\Golongan;
use frontend\modules\payroll\models\PayrollAsuransiLain;
use frontend\modules\payroll\models\PayrollBpjs;
use frontend\modules\payroll\models\PayrollPeriode;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Payroll Asuransi Lain');
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>
<div class="payroll-asuransi-lain-index">


    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'payroll_periode_id',
            'label' => Yii::t('frontend', 'Payroll Periode'),
            'value' => function (PayrollAsuransiLain $model) {
                return Html::a(Html::tag('strong', $model->payrollPeriode->namaPeriode), ['view', 'id' => $model->id], ['title' => 'View Detail Data', 'data' => ['pjax' => 0]]);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(PayrollPeriode::find()->all(), 'id', 'namaPeriode'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Payroll periode', 'id' => 'grid-payroll-bpjs-search-payroll_periode_id'],
            'format' => 'raw'
        ],
        [
            'attribute' => 'golongan_id',
            'label' => Yii::t('frontend', 'Golongan'),
            'value' => function (PayrollAsuransiLain $model) {
                return $model->golongan->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Golongan', 'id' => 'grid-payroll-bpjs-search-golongan_id']
        ],
        [
            'attribute' => 'bisnis_unit',
            'value' => function (PayrollAsuransiLain $model) {
                return PayrollBpjs::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => PayrollBpjs::modelPerjanjianKerja()->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Bisnis Unit'), 'id' => 'grid-thr-search-bisnis_unit']
        ],
        [
            'label' => Yii::t('frontend', 'Jumlah'),
            'value' => function (PayrollAsuransiLain $model) {
                return $model->hitungJumlahPegawai();
            },
        ],
        'catatan:ntext',
        ['attribute' => 'lock', 'visible' => false],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{download-template} {generate-iuran-bpjs} {upload-template} {delete}',
            'buttons' => [
                'delete' => function ($url, PayrollAsuransiLain $model) {
//                    if ($model->last_status == Status::DEFLT) {
                    return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
//                    }
//                    return '';
                },
                'download-template' => function ($url, PayrollAsuransiLain $model) {
                    return Html::a(Yii::t('frontend', 'Download Template'), $url, ['class' => 'btn btn-xs btn-success btn-flat', 'target' => '_blank']);
                },
                'upload-template' => function ($url, PayrollAsuransiLain $model) {
                    return Html::a(Yii::t('frontend', 'Upload Template'), $url, ['class' => 'btn btn-success call-modal-btn btn-flat btn-xs', 'data' => ['header' => Yii::t('frontend', 'Upload template BPJS #{for}', ['for' => $model->payrollPeriode->namaPeriode])]]);
                },
            ],
        ],
    ];
    ?>

    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'payroll-asuransi-lain' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payroll-asuransi-lain']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Buat Data asuransi-lain')]]) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
</div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PayrollAsuransiLain */

$this->title = Yii::t('frontend', 'Create Payroll Asuransi Lain');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Payroll Asuransi Lain'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-asuransi-lain-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

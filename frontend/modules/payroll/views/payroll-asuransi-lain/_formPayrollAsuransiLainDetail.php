<div class="form-group" id="add-payroll-asuransi-lain-detail">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'PayrollAsuransiLainDetail',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'potongan_perusahaan' => ['type' => TabularForm::INPUT_TEXT],
        'potongan_karyawan' => ['type' => TabularForm::INPUT_TEXT],
        'keterangan' => ['type' => TabularForm::INPUT_TEXTAREA],
        'pegawai_id' => [
            'label' => 'Pegawai',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\models\Pegawai::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => Yii::t('frontend', 'Choose Pegawai')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'perjanjian_kerja_id' => [
            'label' => 'Perjanjian kerja',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\models\PerjanjianKerja::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => Yii::t('frontend', 'Choose Perjanjian kerja')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'pegawai_golongan_id' => [
            'label' => 'Pegawai golongan',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\models\PegawaiGolongan::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => Yii::t('frontend', 'Choose Pegawai golongan')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'pegawai_struktur_id' => [
            'label' => 'Pegawai struktur',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\models\PegawaiStruktur::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => Yii::t('frontend', 'Choose Pegawai struktur')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'pegawai_asuransi_lain_id' => [
            'label' => 'Pegawai asuransi lain',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\payroll\models\PegawaiAsuransiLain::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => Yii::t('frontend', 'Choose Pegawai asuransi lain')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'created_by_pk' => ['type' => TabularForm::INPUT_TEXT],
        'created_by_struktur' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('frontend', 'Delete'), 'onClick' => 'delRowPayrollAsuransiLainDetail(' . $key . '); return false;', 'id' => 'payroll-asuransi-lain-detail-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('frontend', 'Add Payroll Asuransi Lain Detail'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPayrollAsuransiLainDetail()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>


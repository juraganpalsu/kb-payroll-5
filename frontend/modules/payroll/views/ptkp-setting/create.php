<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PtkpSetting */

$this->title = Yii::t('frontend', 'Create Ptkp Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ptkp Setting'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ptkp-setting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

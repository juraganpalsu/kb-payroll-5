<?php

use common\components\Bulan;
use kartik\datecontrol\DateControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\PtkpSetting */
/* @var $form yii\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#form-ptkp-setting').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
                btn.button('reset');
            });
            return false;
        });
    })
JS;

$this->registerJs($js);

?>

<div class="ptkp-setting-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ptkp-setting',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'tahun')->dropDownList(Bulan::_tahun());
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'tk', ['addon' => ['prepend' => ['content' => Yii::t('frontend', 'Rp')]]])->textInput(['placeholder' => 'TK0']);
                ?>
            </div>
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'k0', ['addon' => ['prepend' => ['content' => Yii::t('frontend', 'Rp')]]])->textInput(['placeholder' => 'K0']);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'k1', ['addon' => ['prepend' => ['content' => Yii::t('frontend', 'Rp')]]])->textInput(['placeholder' => 'K1']);
                ?>
            </div>
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'k2', ['addon' => ['prepend' => ['content' => Yii::t('frontend', 'Rp')]]])->textInput(['placeholder' => 'K2']);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'k3', ['addon' => ['prepend' => ['content' => Yii::t('frontend', 'Rp')]]])->textInput(['placeholder' => 'K3']);
                ?>
            </div>
            <div class="col-md-2">
                <?php
                echo $form->field($model, 'persen_biaya_jabatan',['addon' => ['append' => ['content' => '%']]])->textInput(['placeholder' => 'Persen Biaya Jabatan']);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'maks_nominal_jabatan', ['addon' => ['append' => ['content' => Yii::t('frontend', 'Bulan')]]])->textInput(['placeholder' => 'Maks Biaya Jabatan']);
                ?>
            </div>
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'catatan')->textarea(['rows' => 3]);
                ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\PtkpSettingSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\payroll\models\PtkpSetting;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;


$this->title = Yii::t('frontend', 'PTKP Setting');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ptkp-setting-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'tahun',
        [
            'attribute' => 'tk',
            'value' => function (PtkpSetting $model) {
                return $model->tk;
            },
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'k0',
            'value' => function (PtkpSetting $model) {
                return $model->k0;
            },
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'k1',
            'value' => function (PtkpSetting $model) {
                return $model->k1;
            },
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'k2',
            'value' => function (PtkpSetting $model) {
                return $model->k2;
            },
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'k3',
            'value' => function (PtkpSetting $model) {
                return $model->k3;
            },
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'persen_biaya_jabatan',
            'value' => function (PtkpSetting $model) {
                return $model->persen_biaya_jabatan .'%';
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'maks_nominal_jabatan',
            'value' => function (PtkpSetting $model) {
                return $model->maks_nominal_jabatan;
            },
            'format' => ['decimal', 0],
        ],
        'catatan:ntext',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}'
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8],
            'noExportColumns' => [1, 9, 10], // id
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'peraturan-perusahaan' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-thr-setting']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

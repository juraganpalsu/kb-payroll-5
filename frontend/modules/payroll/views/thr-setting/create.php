<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\ThrSetting */

$this->title = Yii::t('frontend', 'Create Thr Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Thr Setting'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thr-setting-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

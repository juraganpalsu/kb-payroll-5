<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\ThrSetting */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Thr Setting',
    ]) . ' ' . $model->tahun;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Thr Setting'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tahun, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="thr-setting-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

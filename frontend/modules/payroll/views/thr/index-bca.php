<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\payroll\models\search\ThrSearch */

/**
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var string $index
 * @var string $finish
 */


use common\models\Golongan;
use frontend\modules\payroll\models\Thr;
use frontend\modules\payroll\models\ThrSetting;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Data THR BCA');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thr-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'thr_setting_id',
            'label' => Yii::t('frontend', 'Thr Setting'),
            'value' => function (Thr $model) {
                return $model->thrSetting->tahun;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(ThrSetting::find()->all(), 'id', 'tahun'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Thr setting', 'id' => 'grid-thr-search-thr_setting_id']
        ],
        [
            'attribute' => 'golongan_id',
            'label' => Yii::t('frontend', 'Golongan'),
            'value' => function (Thr $model) {
                return $model->golongan->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Golongan', 'id' => 'grid-thr-search-golongan_id']
        ],
        [
            'attribute' => 'bisnis_unit',
            'value' => function (Thr $model) {
                return Thr::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Thr::modelPerjanjianKerja()->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Bisnis Unit'), 'id' => 'grid-thr-search-bisnis_unit']
        ],
        [
            'label' => Yii::t('frontend', 'Jumlah'),
            'value' => function (Thr $model) {
                return $model->hitungJumlahPegawai();
            },
        ],
        [
            'attribute' => 'persentase_pembayaran',
            'value' => function (Thr $model) {
                return $model->persentase_pembayaran . '%';
            }
        ],
        'catatan:ntext',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {delete}'
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3],
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'thr' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-thr']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
</div>
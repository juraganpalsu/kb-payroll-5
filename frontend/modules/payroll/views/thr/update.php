<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Thr */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Data THR',
    ]) . ' ' . $model->thrSetting->tahun;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Thr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->thrSetting->tahun, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="thr-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Thr */

$this->title = Yii::t('frontend', 'Create THR (Tunjangan Hari Raya)');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Thr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thr-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

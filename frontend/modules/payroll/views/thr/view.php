<?php

use common\components\Helper;
use frontend\modules\payroll\models\Thr;
use frontend\modules\payroll\models\ThrDetail;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\payroll\models\Thr */
/* @var $searchModel \frontend\modules\payroll\models\search\ThrDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 *
 * @var ActiveDataProvider $providerThrDetail
 */

$this->title = Yii::t('frontend', 'Data THR ') . $model->thrSetting->tahun . ' - ' . $model->golongan->nama . ' - ' . Thr::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Thr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thr-view">

    <div class="row">
        <div class="col-sm-11">

            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'thrSetting.tahun',
                    'label' => Yii::t('frontend', 'Thr Setting'),
                ],
                [
                    'attribute' => 'golongan.nama',
                    'label' => Yii::t('frontend', 'Golongan'),
                ],
                [
                    'attribute' => 'bisnis_unit',
                    'value' => function (Thr $model) {
                        return Thr::modelPerjanjianKerja()->_kontrak[$model->bisnis_unit];
                    }
                ],
                [
                    'attribute' => 'persentase_pembayaran',
                    'value' => function (Thr $model) {
                        return $model->persentase_pembayaran . '%';
                    }
                ],
                'catatan:ntext',
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'thrSetting.cut_off_thr',
                ],
                [
                    'attribute' => 'thrSetting.cut_off_resign',
                ],
                [
                    'attribute' => 'finishing_status',
                    'value' => function (Thr $model) {
                        return Html::tag('span', $model->finishingStatus[$model->finishing_status], ['class' => 'text-bold']);
                    },
                    'format' => 'html'
                ],
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php
            try {
                $gridColumnThrDetail = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'id_pegawai',
                        'value' => function (ThrDetail $model) {
                            return $model->id_pegawai;
                        },
                    ],
                    [
                        'attribute' => 'nama',
                        'value' => function (ThrDetail $model) {
                            return $model->nama;
                        },
                    ],
                    [
                        'attribute' => 'golongan',
                        'value' => function (ThrDetail $model) {
                            return $model->golongan;
                        },
                    ],
                    [
                        'attribute' => 'bisnis_unit',
                        'value' => function (ThrDetail $model) {
                            return $model->bisnis_unit;
                        },
                    ],
                    [
                        'attribute' => 'p_gaji_pokok_nominal',
                        'value' => function (ThrDetail $model) {
                            return Yii::$app->formatter->asDecimal($model->p_gaji_pokok_nominal, 0);
                        },
                    ],
                    [
                        'attribute' => 'p_tunjangan_loyalitas_config_nominal',
                        'value' => function (ThrDetail $model) {
                            return Yii::$app->formatter->asDecimal($model->p_tunjangan_loyalitas_config_nominal, 0);
                        },
                    ],
                    [
                        'attribute' => 'p_tunjangan_anak_config_nominal',
                        'value' => function (ThrDetail $model) {
                            return Yii::$app->formatter->asDecimal($model->p_tunjangan_anak_config_nominal, 0);
                        },
                    ],
                    [
                        'attribute' => 'p_tunjangan_lain_tetap_nominal',
                        'value' => function (ThrDetail $model) {
                            return Yii::$app->formatter->asDecimal($model->p_tunjangan_lain_tetap_nominal, 0);
                        },
                    ],
                    [
                        'attribute' => 'p_tunjangan_jabatan_nominal',
                        'value' => function (ThrDetail $model) {
                            return Yii::$app->formatter->asDecimal($model->p_tunjangan_jabatan_nominal, 0);
                        },
                    ],
                    [
                        'attribute' => 'gaji_pokok_thr',
                        'value' => function (ThrDetail $model) {
                            return Yii::$app->formatter->asDecimal($model->gaji_pokok_thr, 0);
                        },
                    ],
                    [
                        'attribute' => 'tanggal_mulai',
                        'value' => function (ThrDetail $model) {
                            return Helper::idDate($model->tanggal_mulai);
                        },
                    ],
                    [
                        'attribute' => 'cut_off',
                        'value' => function (ThrDetail $model) {
                            return Helper::idDate($model->cut_off);
                        },
//                        'hidden' => true
                    ],
                    [
                        'attribute' => 'masa_kerja_tahun',
                        'value' => function (ThrDetail $model) {
                            return $model->masa_kerja_tahun;
                        },
                    ],
                    [
                        'attribute' => 'masa_kerja_bulan',
                        'value' => function (ThrDetail $model) {
                            return $model->masa_kerja_bulan;
                        },
                    ],
                    [
                        'attribute' => 'presentase_thr',
                        'value' => function (ThrDetail $model) {
                            if ($model->presentase_thr == 1) {
                                return 'Jumlah Upah';
                            }
                            if ($model->presentase_thr == 1.04) {
                                return 'Jumlah Upah + 4%';
                            }
                            if ($model->presentase_thr == 1.06) {
                                return 'Jumlah Upah + 6%';
                            }
                            if ($model->presentase_thr == 1.08) {
                                return 'Jumlah Upah + 8%';
                            }
                            if ($model->presentase_thr == 1.1) {
                                return 'Jumlah Upah + 10%';
                            }
                            return 'Prorate';
                        },
                    ],
                    [
                        'attribute' => 'nominal',
                        'value' => function (ThrDetail $model) {
                            return Yii::$app->formatter->asDecimal($model->nominal, 0);
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Nama Bank'),
                        'value' => function (ThrDetail $model) {
                            if ($pegawaiBank = $model->pegawai->pegawaiBank) {
                                return $pegawaiBank->namaBank;
                            }
                            return '';
                        },
                    ],
                    [
                        'label' => Yii::t('frontend', 'No Rekening'),
                        'value' => function (ThrDetail $model) {
                            if ($pegawaiBank = $model->pegawai->pegawaiBank) {
                                return $pegawaiBank->nomer_rekening;
                            }
                            return '';
                        },
                    ],
                    ['attribute' => 'lock', 'visible' => false],
                ];

                $export = ExportMenu::widget([
                    'dataProvider' => $providerThrDetail,
                    'columns' => $gridColumnThrDetail,
                    'noExportColumns' => [1],
//                    'selectedColumns' => [2, 3, 4, 5, 6, 7],
                    'dropdownOptions' => [
                        'class' => 'btn btn-secondary'
                    ],
                    'filename' => 'Rekap THR - ' . $this->title
                ]);
                echo Gridview::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumnThrDetail,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-thr-detail']],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'toolbar' => [
                        [
                            'content' => ''
                        ],
                        [
                            'content' => $export
                        ],
                    ],
                    'panel' => [
                        'heading' => false,
                        'before' => '{summary}',
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
</div>

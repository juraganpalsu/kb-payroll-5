<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\erp\models\search\EStokSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\erp\models\EBarang;
use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\EStok;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Data Stok');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estok-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'e_gudang_id',
            'label' => Yii::t('frontend', 'Gudang'),
            'value' => function (EStok $model) {
                return $model->eBarang ? $model->eGudang->nama : Yii::t('frontend', 'Gudang Telah Dihapus');
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-estok-search-e_gudang_id']
        ],
        [
            'attribute' => 'e_barang_id',
            'label' => Yii::t('frontend', 'Barang'),
            'value' => function (EStok $model) {
                return $model->eBarang ? $model->eBarang->nama : Html::tag('strong', Yii::t('frontend', 'Barang Telah Dihapus'), ['class' => 'text-danger']);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EBarang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Barang', 'id' => 'grid-estok-search-e_barang_id'],
            'format' => 'raw'
        ],
        'jumlah',
        [
            'label' => Yii::t('frontend', 'Satuan'),
            'value' => function (EStok $model) {
                if ($barang = $model->eBarang) {
                    if ($satuan = $barang->eSatuan) {
                        return $satuan->nama;
                    }
                    return Yii::t('frontend', 'Satuan Telah Dihapus');
                }
                return Yii::t('frontend', 'Barang Telah Dihapus');
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'noExportColumns' => [1, 6], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-stok' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-estok']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>
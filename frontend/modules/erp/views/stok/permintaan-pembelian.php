<?php

/**
 * Created by PhpStorm.
 * File Name: permintaan-pembelian.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-07-04
 * Time: 9:05 AM
 */


use frontend\modules\erp\models\EGudang;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EPermintaan */
/* @var $form kartik\widgets\ActiveForm */

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EPermintaan */

$this->title = Yii::t('frontend', 'Buat Permintaan Pembelian');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Stok'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<< JS
    $(function() {
        $('#form-permintaan').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>


<div class="epermintaan-create">
    <div class="epermintaan-form">

        <?php $form = ActiveForm::begin([
            'id' => 'form-permintaan',
            'action' => $model->isNewRecord ? Url::to(['permintaan-pembelian']) : Url::to(['update-pp', 'id' => $model->id]),
            'type' => ActiveForm::TYPE_VERTICAL,
            'formConfig' => ['showErrors' => false],
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['cou-pp-validation']),
            'fieldConfig' => ['showLabels' => true],
        ]); ?>

        <?= $form->errorSummary($model); ?>


        <?php try { ?>

            <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'no')->textInput() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'e_gudang_id')->widget(Select2::class, [
                        'data' => ArrayHelper::map(EGudang::find()->orderBy('id')->all(), 'id', 'nama'),
                        'options' => ['placeholder' => Yii::t('frontend', 'Choose Gudang')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <?=
                    $form->field($model, 'tanggal')->widget(DateControl::class, [
                        'type' => DateControl::FORMAT_DATE,
                        'saveFormat' => 'php:Y-m-d',
                        'ajaxConversion' => true,
                        'options' => [
                            'pluginOptions' => [
                                'placeholder' => $model->getAttributeLabel('tanggal'),
                                'autoclose' => true
                            ]
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        <?php } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php

/**
 * Created by PhpStorm.
 * File Name: index-pp.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-07-04
 * Time: 11:41 PM
 */

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\erp\models\EPpb */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\EPpb;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Data Permintaan Pembelian');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estok-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'no',
        [
            'attribute' => 'e_gudang_id',
            'label' => Yii::t('frontend', 'Gudang'),
            'value' => function (EPpb $model) {
                return $model->eGudang ? $model->eGudang->nama : Yii::t('frontend', 'Gudang Telah Dihapus');
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-estok-search-e_gudang_id']
        ],
        'tanggal',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view-pp} {update} {delete}',
            'buttons' => [
                'view-pp' => function ($url) {
                    return Html::a(Yii::t('frontend', 'View'), $url, ['class' => 'btn btn-xs btn-success', 'data-pjax' => "0"]);
                },
                'update' => function ($url, EPpb $model) {
                    return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-primary btn-create', 'data-pjax' => "0"]);
                },
                'delete' => function ($url, EPpb $model) {
                    return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                }
            ],
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'noExportColumns' => [1, 6], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-stok' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-estok']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['permintaan-pembelian'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index-pp'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>
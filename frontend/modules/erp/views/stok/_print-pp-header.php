<?php

/**
 * Created by PhpStorm.
 * File Name: _print-pp-header.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-07-05
 * Time: 10:42 PM
 */

use common\components\Helper;
use frontend\modules\erp\models\EPpb;
use yii\helpers\Html;


/**
 * @var EPpb $model
 *
 * @var boolean $pagebreak
 *
 */

$height = 4;

?>
<table width="200mm" <?= $pagebreak ? 'style="page-break-after: always;"' : '' ?>>
    <tr>
        <td width="9mm" style="border: 1px white">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
        <td width="9mm">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" rowspan="3"
            style="border: 1px solid; text-align: center">
            <div><?php echo Html::img('@frontend/web/pictures/image001.png', ['height' => '40px']); ?></div>
        </td>
        <td colspan="13" rowspan="3"
            style="text-align: center; border: 1px solid;">
            <h3><strong>PT KOBE BOGA UTAMA</strong></h3>
        </td>
        <td colspan="3" style="border: 1px solid; border-right: 1px solid white;">No. Kode Dok
        </td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white;">:
            KBU/FO/PRC/21/009
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border-left: 1px solid black; border-bottom: 1px solid black; ">Tingkatan Dok</td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white; ">: IV</td>
    </tr>
    <tr>
        <td colspan="3" style="border-left: 1px solid black; border-bottom: 1px solid black; ">Status Revisi</td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white;">: 02</td>
    </tr>
    <tr>
        <td colspan="4" rowspan="2"
            style=" text-align: center; border: 1px solid;">
            <?= Yii::t('frontend', 'Purchasing Department') ?>
        </td>
        <td colspan="13" rowspan="2"
            style=" text-align: center; border-bottom: 1px solid black; border-top: 1px solid black;">
            <h4><strong><?= Yii::t('frontend', 'PERMINTAAN PEMBELIAN BARANG (P2B)'); ?></strong></h4>
        </td>
        <td colspan="3" style="border-left: 1px solid black; border-bottom: 1px solid black; ">Tgl. Mulai Berlaku</td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white;">: 01 April 2021</td>
    </tr>
    <tr>
        <td colspan="3" style="border-left: 1px solid black; border-bottom: 1px solid black; ">Halaman</td>
        <td colspan="4" style="border: 1px solid; border-left : 1px solid white;">: 1 dari 1</td>
    </tr>
    <tr>
        <td colspan="3"
            style=" border-left: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'NO'); ?>
        </td>
        <td colspan="10" style="">
            <strong> :
                <?php echo $model->no; ?>
            </strong>
        </td>
        <td colspan="3"
            style=" height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Tujuan'); ?>
        </td>
        <td colspan="8" style=" border-right: 1px solid black;">
            <strong> :
                <?php echo ''; ?>
            </strong>
        </td>
    </tr>

    <tr>
        <td colspan="3"
            style=" border-left: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Tanggal'); ?>
        </td>
        <td colspan="10" style="">
            <strong> :
                <?php echo Helper::idDate($model->tanggal); ?>
            </strong>
        </td>
        <td colspan="3"
            style=" height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Tanggal Diminta'); ?>
        </td>
        <td colspan="8" style=" border-right: 1px solid black;">
            <strong> :
                <?php ''; ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td colspan="3"
            style=" border-left: 1px solid black; border-bottom: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Dept./Divisi Pengguna'); ?>
        </td>
        <td colspan="10" style="border-bottom: 1px solid black">
            <strong> :
            </strong>
        </td>
        <td colspan="3"
            style="border-bottom: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo ''; ?>
        </td>
        <td colspan="8" style=" border-right: 1px solid black; border-bottom: 1px solid black">
            <strong>
            </strong>
        </td>
    </tr>
    <tr>
        <td rowspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'No') ?>
        </td>
        <td rowspan="2" colspan="3" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Kode Barang') ?>
        </td>
        <td rowspan="2" colspan="6" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Nama Barang') ?>
        </td>
        <td rowspan="2" colspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Jumlah') ?>
        </td>
        <td rowspan="2" colspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Satuan') ?>
        </td>
        <td class="center head" colspan="6"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Kontrol') ?>
        </td>
        <td rowspan="2" colspan="4" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black;border-right: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?php echo Yii::t('frontend', 'Keterangan') ?>
        </td>
    </tr>
    <tr>
        <td class="center head" colspan="2"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?= Yii::t('frontend', 'Sisa') ?>
        </td>
        <td class="center head" colspan="2"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?= Yii::t('frontend', 'Buffer Stock') ?>
        </td>
        <td class="center head" colspan="2"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            <?= Yii::t('frontend', 'Kebutuhan') ?>
        </td>
    </tr>

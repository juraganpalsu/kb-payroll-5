<?php

/**
 * Created by PhpStorm.
 * File Name: view-pp.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-07-05
 * Time: 12:14 AM
 */

use frontend\modules\erp\models\EPpbItem;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EPpb */

/**
 * @var ActiveDataProvider $providerEPpbItem
 */

$this->title = $model->eGudang->nama . ' - ' . $model->no;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Permintaan Pembelian'), 'url' => ['index-pp']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.btn-submit').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.btn-load-minimum-stok').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);
?>
    <div class="epermintaan-view">

        <div class="row">
            <div class="col-sm-3">
                <?= Html::a(Yii::t('frontend', 'Update'), ['update-pp', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('frontend', 'Delete'), ['delete-pp', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
                <?= Html::a(Yii::t('frontend', 'Print'), ['print-pp', 'id' => $model->id], ['class' => 'btn btn-warning', 'target' => '_blank']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    'no',
                    [
                        'attribute' => 'eGudang.nama',
                        'label' => Yii::t('frontend', 'Gudang'),
                    ],
                    'tanggal',
                    ['attribute' => 'lock', 'visible' => false],
                ];
                try {
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= Html::a(Yii::t('frontend', 'Tambah Detail'), ['pp-item', 'idpp' => $model->id], ['class' => 'btn btn-primary btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Tambahkan Permintaan Pembelian Barang')]]) ?>
                <?= Html::a(Yii::t('frontend', 'Load Minimum Stok'), ['load-minimum-stok', 'idpp' => $model->id], ['class' => 'btn btn-warning btn-load-minimum-stok', 'data' => [
                    'confirm' => Yii::t('frontend', 'Tambahkan Semua Barang Yang Telah Mencapai Minimum Stok?'),
                    'method' => 'post'
                ]]) ?>
                <?php
                $gridColumnDetail = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'label' => Yii::t('frontend', 'Barang'),
                        'value' => function (EPpbItem $model) {
                            return $model->eStok->eBarang->nama;
                        }
                    ],
                    'jumlah',
                    [
                        'label' => Yii::t('frontend', 'Satuan'),
                        'value' => function (EPpbItem $model) {
                            return $model->eStok->eBarang->eSatuan->nama;
                        }
                    ],
                    'sisa',
                    'buffer_stock',
                    'kebutuhan',
                    'keterangan',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update-pp-item} {delete-detail}',
                        'buttons' => [
                            'update-pp-item' => function ($url, EPpbItem $model) {
                                return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-primary btn-xs btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Update barang {barang}', ['barang' => $model->eStok->eBarang->nama])]]);

                            },
                            'delete-detail' => function ($url, EPpbItem $model) {
                                return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Yakin akan menghapus {barang} ?', ['barang' => $model->eStok->eBarang->nama])]]);

                            }
                        ]
                    ],
                ];
                ?>
                <?php
                try {
                    echo GridView::widget([
                        'dataProvider' => $providerEPpbItem,
                        'columns' => $gridColumnDetail,
                        'pjax' => false,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-permintaan-detail']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                    print_r($e->getMessage());
                }
                ?>
            </div>
        </div>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
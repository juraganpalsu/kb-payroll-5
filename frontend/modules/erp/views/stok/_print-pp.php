<?php

/**
 * Created by PhpStorm.
 * File Name: _print-pp.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-07-05
 * Time: 10:42 PM
 */

/**
 * @var EPpb $model
 *
 */


use frontend\modules\erp\models\EPpb;


$countAllRow = count($model->ePpbItems);
$row = 0;
$maxRow = 30;


$headerWithoutPageBreak = $this->renderAjax('_print-pp-header', ['model' => $model, 'pagebreak' => false]);
$header = $this->renderAjax('_print-pp-header', ['model' => $model, 'pagebreak' => true]);
if ($countAllRow < $maxRow) {
    $header = $headerWithoutPageBreak;
}
echo $header;

$no = 1;
foreach ($model->ePpbItems as $item) {

    ?>
    <tr>
        <td style="text-align: center; border: 1px dotted; border-left: 1px solid"><?= $no; ?></td>
        <td colspan="3" style="border: 1px dotted"><?= explode('-', $item->eStok->eBarang->id)[0]; ?></td>
        <td colspan="6" style="border: 1px dotted"><?= $item->eStok->eBarang->nama; ?></td>
        <td colspan="2" style="border: 1px dotted"><?= $item->jumlah; ?></td>
        <td colspan="2" style="text-align: center; border: 1px dotted"><?= $item->eStok->eBarang->eSatuan->nama; ?></td>
        <td colspan="2" style="text-align: center; border: 1px dotted"><?= $item->sisa; ?></td>
        <td colspan="2" style="text-align: center; border: 1px dotted"><?= $item->buffer_stock ?></td>
        <td colspan="2" style="text-align: center; border: 1px dotted"><?= $item->kebutuhan; ?></td>
        <td colspan="4" style="border: 1px dotted; border-right: 1px solid"><?= $item->keterangan ?></td>
    </tr>

    <?php
    $row++;
    $countAllRow--;
    if ($row == $maxRow) {
//        echo "<tr><td colspan='24' style='border: 1px solid white'>&nbsp;</td></tr>";
        echo "</table>";
        if ($countAllRow > $maxRow) {
            echo $header;
        } else {
            echo $headerWithoutPageBreak;
        }
        $row = 0;
    }
    $no++;
} ?>

<tr>
    <td colspan="6" style="border-top: 1px solid; text-align: center"><?= Yii::t('frontend', 'Diminta oleh,') ?><br><br><br><br>(......................................)
    </td>
    <td colspan="6" style="border-top: 1px solid; text-align: center"><?= Yii::t('frontend', 'Diketahui oleh,') ?>
        <br><br><br><br>(......................................)
    </td>
    <td colspan="6" style="border-top: 1px solid; text-align: center"><?= Yii::t('frontend', 'Disetujui oleh,') ?>
        <br><br><br><br>(......................................)
    </td>
    <td colspan="6" style="border-top: 1px solid; text-align: center"><?= Yii::t('frontend', 'Diterima oleh,') ?>
        <br><br><br><br>(......................................)
    </td>
</tr>
<tr>
    <td colspan="24">
        <strong><?= Yii::t('frontend', 'Note : Khusus untuk barang Logistik, ttd disetujui oleh tidak perlu diisi') ?></strong>
    </td>
</tr>
</table>

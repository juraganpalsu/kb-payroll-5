<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EHarga */

$this->title = Yii::t('frontend', 'Create E Harga');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Harga'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eharga-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

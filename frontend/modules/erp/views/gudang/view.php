<?php

use frontend\modules\erp\models\EGudangPegawai;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EGudang */

/**
 * @var ActiveDataProvider $dataProviderPegawai
 */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Gudang'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-post').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
});
JS;

$this->registerJs($js);
?>

    <div class="egudang-view">

        <div class="row">
            <div class="col-sm-3">
                <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?php Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    'nama',
                    ['attribute' => 'lock', 'visible' => false],
                ];
                try {
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>

        <h2><?= Yii::t('frontend', 'Daftar Pegawai Pengelola') ?></h2>
        <div class="row">
            <div class="col-sm-6">
                <?= Html::a(Yii::t('frontend', 'Tambah Pegawai'), ['add-pegawai', 'idgudang' => $model->id], ['class' => 'btn btn-primary btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Tambahkan Pegawai')]]) ?>
                <?php
                $gridColumn = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'pegawai_id',
                        'value' => function (EGudangPegawai $model) {
                            return $model->pegawai->nama_lengkap;
                        }
                    ],
                    [
                        'attribute' => 'notifikasi',
                        'value' => function (EGudangPegawai $model) {
                            $class = 'btn-danger';
                            $tambahanText = '<br> *Pastikan email yang bersangkukan aktif';
                            if ($model->notifikasi) {
                                $tambahanText = '';
                                $class = 'btn-success';
                            }
                            return Html::a(Yii::t('frontend', 'Notifikasi'), ['set-notifikasi', 'id' => $model->id], ['class' => 'btn btn-xs ' . $class . ' btn-post', 'data' => ['confirm' => Yii::t('frontend', 'Yakin akan merubah status notifikasi?') . $tambahanText]]);
                        },
                        'format' => 'raw'
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{delete-pegawai}',
                        'buttons' => [
                            'delete-pegawai' => function ($url, EGudangPegawai $model) {
                                return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-post', 'data' => ['confirm' => Yii::t('frontend', 'Yakin akan menghapus {pegawai} ?', ['pegawai' => $model->pegawai->nama])]]);
                            }
                        ]
                    ],
                ];
                ?>
                <?php
                try {
                    echo GridView::widget([
                        'dataProvider' => $dataProviderPegawai,
                        'columns' => $gridColumn,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-izin-keluar']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>

            </div>
        </div>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EGudang */

$this->title = Yii::t('frontend', 'Create Gudang');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Gudang'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="egudang-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

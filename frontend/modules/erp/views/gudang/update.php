<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EGudang */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'E Gudang',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Gudang'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="egudang-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/**
 * Created by PhpStorm.
 * File Name: index-detail.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 3/15/2021
 * Time: 9:52 PM
 */

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\erp\models\search\EPermintaanSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Helper;
use common\components\Status;
use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\EPermintaanDetail;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Data Permintaan Detail');

$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Permintaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="EPermintaanDetail-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'seq_code',
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->seqCode;
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'e_gudang_id',
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->eGudang->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-EPermintaanDetail-search-e_gudang_id']
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->pegawai->nama;
            },
        ],
        [
            'attribute' => 'area_kerja_id',
            'label' => Yii::t('frontend', 'Area Kerja'),
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->areaKerja->refAreaKerja->name;
            },
        ],
        [
            'attribute' => 'created_at',
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->created_at ? Helper::idDate($model->ePermintaan->created_at) : '';
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'label' => Yii::t('frontend', 'Barang'),
            'attribute' => 'nama_barang',
            'value' => function (EPermintaanDetail $model) {
                return $model->eStok->eBarang->nama;
            }
        ],
        [
            'attribute' => 'jumlah',
            'label' => Yii::t('frontend', 'Jumlah Diminta'),
            'value' => function (EPermintaanDetail $model) {
                return $model->jumlah;
            }
        ],
        [
            'label' => Yii::t('frontend', 'Jumlah Keluar'),
            'value' => function (EPermintaanDetail $model) {
                return Html::tag('strong', $model->hitungQtyYgTelahDikeluarkan(), ['class' => 'text-danger']);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'e_satuan_id',
            'value' => function (EPermintaanDetail $model) {
                return $model->eSatuan->nama;
            }
        ],
        [
            'attribute' => 'last_status',
            'value' => function (EPermintaanDetail $model) {
                return Status::statuses($model->ePermintaan->last_status);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::statuses(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status'), 'id' => 'grid-permintaan-search-last_status']
        ],
        [
            'attribute' => 'keterangan',
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->keterangan;
            },
            'format' => 'text'
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'noExportColumns' => [1, 6], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-permintaan-detail' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-EPermintaanDetail']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

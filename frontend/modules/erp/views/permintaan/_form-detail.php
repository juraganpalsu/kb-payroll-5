<?php

/**
 * Created by PhpStorm.
 * File Name: _form-detail.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 25/02/21
 * Time: 23.06
 */


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EPermintaanDetail */

/* @var $form kartik\widgets\ActiveForm */

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$imageUrl = Url::to(['barang/get-image']);

$js = <<< JS
    $(function() {
        $('#form-epermintaan').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    });
    
    getImage = function (id){
           $.get('$imageUrl', {'id' : id}).done(function(dt) {
               console.log(dt);
               $(".image").html(dt);
            });
           return false;
    }
JS;

$this->registerJs($js);
?>

<div class="epermintaan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-epermintaan',
        'action' => $model->isNewRecord ? Url::to(['create-detail', 'idpermintaan' => $model->e_permintaan_id]) : Url::to(['update-detail', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['cou-detail-validation', 'isnew' => $model->isNewRecord]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>


    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-6">
                <?php
                echo $form->field($model, 'e_stok_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Barang'),
                        'multiple' => false,
                        'disabled' => !$model->isNewRecord
                    ],
                    'data' => $model->isNewRecord ? [] : [$model->e_stok_id => $model->eStok->eBarang->nama],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/erp/barang/get-barang-by-gudang-for-select2', 'gudangid' => $model->ePermintaan->e_gudang_id]),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term};}')
                        ],
                    ],
                    'pluginEvents' => [
                        'select2:select' => 'function() {getImage(this.value)}'
                    ]
                ]);
                if (!$model->isNewRecord) {
                    echo $form->field($model, 'e_stok_id', ['template' => '{input}'])->hiddenInput();
                }
                ?>
            </div>
            <div class="col-md-6">
                <span class="image" style="position: absolute"></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'jumlah')->textInput(['maxlength' => true, 'placeholder' => Yii::t('frontend', 'Jumlah')]) ?>
            </div>
        </div>

        <?= $form->field($model, 'e_permintaan_id', ['template' => '{input}'])->textInput(['style' => 'display:none', 'value' => $model->e_permintaan_id]); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>


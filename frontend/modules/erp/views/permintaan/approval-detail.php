<?php

/**
 * Created by PhpStorm.
 * File Name: approval-detail.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-06-08
 * Time: 8:41 PM
 */


/* @var $this yii\web\View */
/**
 * @var $searchModel frontend\modules\erp\models\search\EPermintaanSearch
 * @var $model EPermintaan
 */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Helper;
use common\components\Status;
use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\EPermintaan;
use frontend\modules\erp\models\EPermintaanDetail;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('frontend', 'Data Permintaan Detail');

$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Permintaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$url = Yii::$app->request->url;

$js = <<<JS

    $('.approve-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
            btn.button('loading');
            $.post(btn.attr('href'))
            .done(function (dt) {
                btn.button('reset');
                console.log(dt);
                if(dt.status === true){
                    let idModal = $('#form-modal');
                    idModal.find('.modal-title').text($(this).data('header'));
                    idModal.modal('show')
                       .find('#modelContent')
                       .load('$url');
                    return false;
                }
            }); 
        return false;
    });

    $('.reject-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        btn.button('loading');
        krajeeDialog.prompt({label:'Berikan alasan', placeholder:'Sampai 225 karakter...', maxlength: 225, type: 'text'}, function (result) {
            if (result === ''){
                result = '-';
            } 
            if (result) {
                $.post(btn.attr('href'), {'keterangan' : result})
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    return false;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.approve-all-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
            btn.button('loading');
            $.post(btn.attr('href'))
            .done(function (dt) {
                btn.button('reset');
                console.log(dt);
                if(dt.status === true){
                    location.reload();
                }
                return false;
            }); 
        return false;
    });
    
    $('.reject-all-btn').click(function(e) {
       e.preventDefault();
        let btn = $(this);
        btn.button('loading');
        krajeeDialog.prompt({label:'Berikan alasan', placeholder:'Sampai 255 karakter...', maxlength: 255, type: 'text'}, function (result) {
            if (result === ''){
                result = '-';
            } 
            if (result) {
                $.post(btn.attr('href'), {'keterangan' : result})
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    return false;
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
JS;

$this->registerJs($js);


?>
<div class="EPermintaanDetail-index">

    <?php
    if ($model->checkStatusApprovalByUser()) {
        $class = $model->checkStatusApprovals() ? 'btn-warning' : 'btn-success';
        echo Html::a(Yii::t('frontend', 'Approve All'), ['approve-all', 'id' => $model->id], ['class' => 'btn ' . $class . ' btn-xs approve-all-btn btn-flat']);
        echo Html::a(Yii::t('frontend', 'Reject All'), ['reject-all', 'id' => $model->id], ['class' => 'btn btn-danger btn-xs reject-all-btn btn-flat']);
    }
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function () {
                return GridView::ROW_COLLAPSED;
            },
            'enableRowClick' => true,
            'detailUrl' => Url::to(['show-user-approval-detail']),
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        [
            'attribute' => 'seq_code',
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->seqCode;
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'e_gudang_id',
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->eGudang->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-EPermintaanDetail-search-e_gudang_id']
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->pegawai->nama;
            },
        ],
        [
            'attribute' => 'area_kerja_id',
            'label' => Yii::t('frontend', 'Area Kerja'),
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->areaKerja->refAreaKerja->name;
            },
        ],
        [
            'attribute' => 'created_at',
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->created_at ? Helper::idDate($model->ePermintaan->created_at) : '';
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'label' => Yii::t('frontend', 'Barang'),
            'attribute' => 'nama_barang',
            'value' => function (EPermintaanDetail $model) {
                return $model->eStok->eBarang->nama;
            }
        ],
        [
            'attribute' => 'jumlah',
            'label' => Yii::t('frontend', 'Jumlah Diminta'),
            'value' => function (EPermintaanDetail $model) {
                return $model->jumlah;
            }
        ],
        [
            'label' => Yii::t('frontend', 'Jumlah Keluar'),
            'value' => function (EPermintaanDetail $model) {
                return Html::tag('strong', $model->hitungQtyYgTelahDikeluarkan(), ['class' => 'text-danger']);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'e_satuan_id',
            'value' => function (EPermintaanDetail $model) {
                return $model->eSatuan->nama;
            }
        ],
        [
            'attribute' => 'last_status',
            'value' => function (EPermintaanDetail $model) {
                return Status::statuses($model->last_status);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::statuses(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status'), 'id' => 'grid-permintaan-search-last_status']
        ],
        [
            'attribute' => 'keterangan',
            'value' => function (EPermintaanDetail $model) {
                return $model->ePermintaan->keterangan;
            },
            'format' => 'text'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{approve-detail} {reject-detail}',
            'buttons' => [
                'approve-detail' => function ($url, EPermintaanDetail $model) {
                    if ($model->approvalByUser->tipe == Status::DEFLT) {
                        return Html::a(Yii::t('frontend', 'Approve'), $url, ['class' => 'btn btn-xs btn-success btn-flat btn-create approve-btn', 'data-pjax' => "0"]);
                    }
                    return '';
                },
                'reject-detail' => function ($url, EPermintaanDetail $model) {
                    if ($model->approvalByUser->tipe == Status::DEFLT) {
                        return Html::a(Yii::t('frontend', 'Reject'), $url, ['class' => 'btn btn-xs btn-danger btn-flat reject-btn', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                    }
                    return '';
                }
            ],
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    ?>
    <?php
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-EPermintaanDetail']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        print_r($e->getMessage());
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\erp\models\search\EPermintaanSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Helper;
use common\components\Status;
use frontend\models\User;
use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\EPermintaan;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('frontend', 'Data Permintaan');
$this->params['breadcrumbs'][] = $this->title;

//
$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    $('.btn-force-close').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);

?>
    <div class="epermintaan-index">
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',
                'value' => function () {
                    return GridView::ROW_COLLAPSED;
                },
                'enableRowClick' => true,
                'detailUrl' => Url::to(['show-user-approval']),
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true
            ],
            [
                'attribute' => 'seq_code',
                'label' => Yii::t('app', 'Kode Permintaan'),
                'value' => function (EPermintaan $model) {
                    $gudangIds = [];
                    if ($modelPegawai = User::pegawai()) {
                        $gudangIds = $modelPegawai->getGudang();
                    }
                    $btnColor = 'btn-warning';
                    if ($model->eBarangKeluars) {
                        $btnColor = 'btn-primary';
                    }
                    if (in_array($model->e_gudang_id, $gudangIds) && $model->last_status == Status::FINISHED || $model->last_status == Status::APPROVED ) {
                        return Html::a($model->seqCode, ['barang-keluar/create', 'permintaanid' => $model->id], ['class' => 'btn btn-xs btn-flat ' . $btnColor . ' btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Buat Barang Keluar')]]);
                    }
                    return $model->seqCode;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'e_gudang_id',
                'value' => function (EPermintaan $model) {
                    return $model->eGudang->nama;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-epermintaan-search-e_gudang_id']
            ],
            [
                'attribute' => 'pegawai_id',
                'label' => Yii::t('frontend', 'Pegawai'),
                'value' => function (EPermintaan $model) {
                    return $model->pegawai->nama;
                },
            ],
            [
                'attribute' => 'area_kerja_id',
                'label' => Yii::t('frontend', 'Area Kerja'),
                'value' => function (EPermintaan $model) {
                    return $model->areaKerja->refAreaKerja->name;
                },
            ],
            [
                'attribute' => 'created_at',
                'value' => function (EPermintaan $model) {
                    return $model->created_at ? Helper::idDate($model->created_at) : '';
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            [
                'attribute' => 'last_status',
                'value' => function (EPermintaan $model) {
                    return Status::statuses($model->last_status);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Status::statuses(0, true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status'), 'id' => 'grid-permintaan-search-last_status']
            ],
            'keterangan:ntext',
            [
                'attribute' => 'created_at',
                'value' => function (EPermintaan $model) {
                    return date('d-m-Y H:m', strtotime($model->created_at));
                },
            ],
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{force-close} {view} {update} {delete}',
                'buttons' => [
                    'force-close' => function ($url, EPermintaan $model) {
                        $gudangIds = [];
                        if ($modelPegawai = User::pegawai()) {
                            $gudangIds = $modelPegawai->getGudang();
                        }
                        if (in_array($model->e_gudang_id, $gudangIds) && $model->last_status == Status::FINISHED || $model->last_status == Status::APPROVED ) {
                            return Html::a(Yii::t('frontend', 'Close'), $url, ['class' => 'btn btn-xs btn-flat btn-warning btn-force-close', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to force close this item?'), 'method' => 'post']]);
                        }
                        return '';
                    },
                    'view' => function ($url) {
                        return Html::a(Yii::t('frontend', 'View'), $url, ['class' => 'btn btn-xs btn-success', 'data-pjax' => "0"]);
                    },
                    'update' => function ($url, EPermintaan $model) {
                        if ($model->last_status == Status::OPEN) {
                            return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-primary btn-create', 'data-pjax' => "0"]);
                        }
                        return '';
                    },
                    'delete' => function ($url, EPermintaan $model) {
                        if ($model->last_status == Status::OPEN) {
                            return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                        }
                        return '';
                    }
                ],
            ],
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [2, 3, 4, 5],
                'noExportColumns' => [1, 6], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'data-barang-masuk' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => false,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-epermintaan']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-th-list"></i>', ['index-detail'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Detail'), 'class' => 'btn btn-primary btn-flat']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
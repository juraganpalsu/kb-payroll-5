<?php

/**
 * Created by PhpStorm.
 * File Name: approval.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-06-08
 * Time: 7:33 PM
 */

use common\components\Helper;
use common\components\Status;
use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\EPermintaan;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('frontend', 'Approval Permintaan');
$this->params['breadcrumbs'][] = $this->title;

/**
 * @var ActiveDataProvider $dataProvider
 */


$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    
    $('.approve-all-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
            btn.button('loading');
            $.post(btn.attr('href'))
            .done(function (dt) {
                btn.button('reset');
                console.log(dt);
                if(dt.status === true){
                    location.reload();
                }
                return false;
            }); 
        return false;
    });
    
    $('.reject-all-btn').click(function(e) {
       e.preventDefault();
        let btn = $(this);
        btn.button('loading');
        krajeeDialog.prompt({label:'Berikan alasan', placeholder:'Sampai 255 karakter...', maxlength: 255, type: 'text'}, function (result) {
            if (result === ''){
                result = '-';
            } 
            if (result) {
                $.post(btn.attr('href'), {'keterangan' : result})
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    return false;
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});

JS;

$this->registerJs($js);

?>
<div class="epermintaan-approval">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function () {
                return GridView::ROW_COLLAPSED;
            },
            'enableRowClick' => true,
            'detailUrl' => Url::to(['show-user-approval']),
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        [
            'attribute' => 'seq_code',
            'label' => Yii::t('app', 'Kode Permintaan'),
            'value' => function (EPermintaan $model) {
                return Html::a($model->seqCode, ['approval-detail', 'id' => $model->id], ['class' => 'btn btn-xs btn-flat btn-success btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'View Approval Detail Permintaan')]]);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'e_gudang_id',
            'value' => function (EPermintaan $model) {
                return $model->eGudang->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-epermintaan-search-e_gudang_id']
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (EPermintaan $model) {
                return $model->pegawai->nama;
            },
        ],
        [
            'attribute' => 'area_kerja_id',
            'label' => Yii::t('frontend', 'Area Kerja'),
            'value' => function (EPermintaan $model) {
                return $model->areaKerja->refAreaKerja->name;
            },
        ],
        [
            'attribute' => 'created_at',
            'value' => function (EPermintaan $model) {
                return $model->created_at ? Helper::idDate($model->created_at) : '';
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'last_status',
            'value' => function (EPermintaan $model) {
                return Status::statuses($model->last_status);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::statuses(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status'), 'id' => 'grid-permintaan-search-last_status']
        ],
        'keterangan:ntext',
        [
            'attribute' => 'created_at',
            'value' => function (EPermintaan $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{approve-all} {reject-all}',
            'buttons' => [
                'approve-all' => function ($url, EPermintaan $model) {
                    if ($model->checkStatusApprovalByUser()) {
                        return Html::a(Yii::t('frontend', 'Approve All'), $url, ['class' => 'btn btn-xs btn-success btn-flat btn-create approve-all-btn', 'data-pjax' => "0"]);
                    }
                    return '';
                },
                'reject-all' => function ($url, EPermintaan $model) {
                    if ($model->checkStatusApprovalByUser()) {
                        return Html::a(Yii::t('frontend', 'Reject All'), $url, ['class' => 'btn btn-xs btn-danger btn-flat reject-all-btn', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                    }
                    return '';
                }
            ],
        ],
    ];
    ?>
    <?php
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-epermintaan']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent'></div>
<?php
Modal::end();
?>

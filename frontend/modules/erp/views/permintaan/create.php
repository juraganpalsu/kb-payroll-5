<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EPermintaan */

$this->title = Yii::t('frontend', 'Create Permintaan Barang');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Permintaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="epermintaan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

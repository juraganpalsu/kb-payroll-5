<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EPermintaan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Permintaan',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Permintaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="epermintaan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use common\components\Status;
use frontend\modules\erp\models\EPermintaanDetail;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EPermintaan */

/**
 * @var ActiveDataProvider $providerEPermintaanDetail
 */

$this->title = $model->eGudang->nama . ' - ' . $model->seqCode;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Permintaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.btn-submit').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);
?>
    <div class="epermintaan-view">

        <div class="row">
            <div class="col-sm-3">

                <?php if ($model->last_status == Status::OPEN) { ?>

                    <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                    <?=
                    Html::a(Yii::t('frontend', 'Submit'), ['submit', 'id' => $model->id], ['class' => 'btn btn-warning btn-submit', 'data' => ['confirm' => Yii::t('frontend', 'Setelah disubmit, data tidak dapat diedit, Anda Yakin?'), 'method' => 'post']])
                    ?>

                <?php } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    'seq_code',
                    [
                        'attribute' => 'eGudang.nama',
                        'label' => Yii::t('frontend', 'Gudang'),
                    ],
                    [
                        'attribute' => 'last_status',
                        'value' => Status::statuses($model->last_status)
                    ],
                    'keterangan:ntext',
                    ['attribute' => 'lock', 'visible' => false],
                ];
                try {
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php if ($model->last_status == Status::OPEN) { ?>
                    <?= Html::a(Yii::t('frontend', 'Tambah Detail'), ['create-detail', 'idpermintaan' => $model->id], ['class' => 'btn btn-primary btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Tambahkan Permintaan Barang')]]) ?>
                <?php } ?>
                <?php
                $gridColumnDetail = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'width' => '50px',
                        'value' => function () {
                            return GridView::ROW_COLLAPSED;
                        },
                        'enableRowClick' => true,
                        'detailUrl' => Url::to(['show-user-approval-detail']),
                        'headerOptions' => ['class' => 'kartik-sheet-style'],
                        'expandOneOnly' => true
                    ],
                    [
                        'label' => Yii::t('frontend', 'Barang'),
                        'value' => function (EPermintaanDetail $model) {
                            return $model->eStok->eBarang->nama;
                        }
                    ],
                    'jumlah',
                    [
                        'label' => Yii::t('frontend', 'Satuan'),
                        'value' => function (EPermintaanDetail $model) {
                            return $model->eStok->eBarang->eSatuan->nama;
                        }
                    ],
                    [
                        'attribute' => 'last_status',
                        'value' => function (EPermintaanDetail $model) {
                            return Status::statuses($model->last_status);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update-detail} {delete-detail}',
                        'buttons' => [
                            'update-detail' => function ($url, EPermintaanDetail $model) {
                                if ($model->last_status == Status::OPEN) {
                                    return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-primary btn-xs btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Update barang {barang}', ['barang' => $model->eStok->eBarang->nama])]]);
                                }
                                return '';
                            },
                            'delete-detail' => function ($url, EPermintaanDetail $model) {
                                if ($model->last_status == Status::OPEN) {
                                    return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-delete-detail', 'data' => ['confirm' => Yii::t('frontend', 'Yakin akan menghapus {barang} ?', ['barang' => $model->eStok->eBarang->nama])]]);
                                }
                                return '';
                            }
                        ]
                    ],
                ];
                ?>
                <?php
                try {
                    echo GridView::widget([
                        'dataProvider' => $providerEPermintaanDetail,
                        'columns' => $gridColumnDetail,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-permintaan-detail']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
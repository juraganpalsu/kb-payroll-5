<?php

/**
 * Created by PhpStorm.
 * File Name: _user-approval.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-06-13
 * Time: 7:34 PM
 */


use common\components\Status;
use frontend\modules\erp\models\EApproval;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;


/**
 * @var array $modelApprovals
 */


$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'pegawai_id',
        'value' => function (EApproval $model) {
            $urutan = str_repeat('--- ', $model->urutan);
            return $urutan . $model->pegawai->nama;
        },
        'format' => 'html'
    ],
    [
        'attribute' => 'tipe',
        'label' => Yii::t('frontend', 'Tipe Approval'),
        'value' => function (EApproval $model) {
            return $model->tipe ? Status::approvalStatus($model->tipe) : '';
        },
    ],
    [
        'attribute' => 'tanggal',
        'value' => function (EApproval $model) {
            return $model->tanggal ? date('d-m-Y H:i:s', strtotime($model->tanggal)) : '';
        },
    ],
    [
        'attribute' => 'komentar',
        'value' => function (EApproval $model) {
            return $model->komentar ?? '';
        },
    ],
];
try {
    $dataProvider = new ArrayDataProvider([
        'allModels' => $modelApprovals,
        'pagination' => [
            'pageSize' => -1
        ]
    ]);
    echo '<div class="row">';
    echo '<div class="col-md-8">';
    echo GridView::widget([
        'id' => 'spl-approval-user-grid',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'spl-approval-user-grid-pjax']],
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
        ],
        'beforeHeader' => [
            [
                'columns' => [
                    [
                        'content' => Yii::t('frontend', 'User Approval'),
                        'options' => [
                            'colspan' => 5,
                            'class' => 'text-center warning',
                        ]
                    ],
                ],
                'options' => ['class' => 'skip-export']
            ]
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
        'tableOptions' => ['class' => 'small']
    ]);

    echo '</div>';

    echo '</div>';
} catch (Exception $e) {
    print_r($e->getMessage());
    Yii::info($e->getMessage(), 'exception');
}
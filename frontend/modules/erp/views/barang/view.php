<?php

use frontend\modules\erp\models\EBarang;
use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\form\EBarangForm;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarang */

/**
 *
 * @var ActiveDataProvider $dataProviderGudang
 */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Data Barang'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$modelBarang = $model;
$modelForm = new EBarangForm();

$js = <<<JS
$(function() {
    $('.btn-tambahkan-ke-gudang').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
});
JS;

$this->registerJs($js);
?>
    <div class="ebarang-view">

        <div class="row">
            <div class="col-sm-3" style="margin-top: 15px">

                <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
                <?=
                Html::a(Yii::t('frontend', 'Tambah Gambar'), ['upload-file', 'id' => $model->id], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Tambah Gambar'), 'class' => 'btn btn-success btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Upload Gambar untuk {barang}', ['barang' => $model->nama])]])
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8">
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    'nama',
                    'harga_terakhir',
                    'minimum_stok',
                    'maksimum_stok',
                    [
                        'attribute' => 'e_satuan_id',
                        'value' => function (EBarang $model) {
                            return $model->eSatuan->nama;
                        }
                    ],
                    [
                        'attribute' => 'kategori',
                        'value' => function (EBarang $model) {
                            return $model->kategori_;
                        }
                    ],
                ];
                try {
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
            <div class="col-sm-4">
                <?= Html::img('@web/' . $modelForm->upload_dir . $model->id . '.' . $model->getFileExtension(), ['height' => 200, 'width' => 200]) ?>
            </div>
        </div>

        <div class="egudang-index">
            <div class="col-sm-12">
                <?php
                $gridColumn = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    'nama',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{tambahkan-ke-gudang}',
                        'buttons' => [
                            'tambahkan-ke-gudang' => function ($url, EGudang $model) use ($modelBarang) {
                                if ($modelStok = $modelBarang->getStok($model->id)) {
                                    return $modelStok->jumlah;
                                }
                                return Html::a(Yii::t('frontend', 'Tambahkan Ke Gudang'), ['tambahkan-ke-gudang', 'idgudang' => $model->id, 'idbarang' => $modelBarang->id], ['class' => 'btn btn-xs btn-primary btn-tambahkan-ke-gudang', 'data' => ['confirm' => Yii::t('frontend', 'Tambahkan {barang} ke gudang {gudang} ?', ['barang' => $modelBarang->nama, 'gudang' => $model->nama])]]);
                            }
                        ]
                    ],
                ];
                ?>
                <?php
                try {
                    echo GridView::widget([
                        'dataProvider' => $dataProviderGudang,
                        'columns' => $gridColumn,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-gudang']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
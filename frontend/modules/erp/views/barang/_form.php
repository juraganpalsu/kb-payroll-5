<?php

use frontend\modules\erp\models\ESatuan;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarang */
/* @var $form yii\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#form-ebarang').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>

<div class="ebarang-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ebarang',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-1">
                <?= $form->field($model, 'kategori')->dropDownList($model->_kategori) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">

                <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-1">
                <?= $form->field($model, 'e_satuan_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(ESatuan::find()->orderBy('id')->asArray()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Pilih satuan')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'minimum_stok')->textInput(['maxlength' => true, 'placeholder' => 'Minimum Stok']) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'maksimum_stok')->textInput(['maxlength' => true, 'placeholder' => 'Maksimum Stok']) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>

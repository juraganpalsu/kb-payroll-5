<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarang */

$this->title = Yii::t('frontend', 'Create Data Barang');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Data Barang'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ebarang-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

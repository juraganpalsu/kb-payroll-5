<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\erp\models\search\EBarangSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Status;
use frontend\modules\erp\models\ESatuan;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use frontend\modules\erp\models\EBarang;

$this->title = Yii::t('frontend', 'Data Barang');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function ($) {  
    $('.change-aktif-status-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);
?>
<div class="ebarang-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'kategori',
            'value' => function (EBarang $model) {
                return $model->_kategori[$model->kategori];
            },
            'filter' => Html::activeDropDownList($searchModel, 'kategori', $searchModel->_kategori, ['class' => 'form-control', 'prompt' => '*']),
        ],
        'nama',
        'harga_terakhir',
        [
            'attribute' => 'e_satuan_id',
            'label' => Yii::t('frontend', 'Satuan'),
            'value' => function (EBarang $model) {
                return $model->eSatuan->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(ESatuan::find()->asArray()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'satuan', 'id' => 'grid-ebarang-search-e_satuan_id']

        ],
        'minimum_stok',
        [
            'attribute' => 'is_aktif',
            'value' => function (EBarang $model) {
                if ($model->is_aktif) {
                    return Html::a(Status::activeInactive(Status::YA), ['change-aktif-status', 'id' => $model->id, 'status' => Status::TIDAK], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-aktif-status-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merubah status <strong>{nama}</strong> ?', ['nama' => $model->nama])]]);
                }
                return Html::a(Status::activeInactive(Status::TIDAK), ['change-aktif-status', 'id' => $model->id, 'status' => Status::YA], ['class' => 'btn btn-warning btn-flat btn-xs btn-block change-aktif-status-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merubah status <strong>{nama}</strong> ?', ['nama' => $model->nama])]]);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::activeInactive(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('is_aktif'), 'id' => 'grid-pegawai-search-is_aktif'],
            'format' => 'raw'
        ],
        [
            'attribute' => 'created_by',
            'value' => function (EBarang $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (EBarang $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {view} {delete}',
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'noExportColumns' => [1, 6], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-barang' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-izin-keluar']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

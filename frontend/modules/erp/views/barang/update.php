<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarang */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Data Barang',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Data Barang'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="ebarang-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

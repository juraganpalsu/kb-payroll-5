<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\search\EBarangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-ebarang-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'harga_terakhir')->textInput(['maxlength' => true, 'placeholder' => 'Harga Terakhir']) ?>

    <?= $form->field($model, 'e_satuan_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\erp\models\ESatuan::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('frontend', 'Choose satuan')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'kategori')->textInput(['placeholder' => 'Kategori']) ?>

    <?php /* echo $form->field($model, 'created_by_pk')->textInput(['maxlength' => true, 'placeholder' => 'Created By Pk']) */ ?>

    <?php /* echo $form->field($model, 'created_by_struktur')->textInput(['placeholder' => 'Created By Struktur']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('frontend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

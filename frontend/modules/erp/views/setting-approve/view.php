<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\ESettingApprove */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Setting Approve'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="esetting-approve-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'E Setting Approve').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'pegawai.id',
            'label' => Yii::t('frontend', 'Pegawai'),
        ],
        [
            'attribute' => 'atasanPegawai.id',
            'label' => Yii::t('frontend', 'Atasan Pegawai'),
        ],
        'keterangan:ntext',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Pegawai<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPegawai = [
        ['attribute' => 'id', 'visible' => false],
        'nama_lengkap',
        'has_uploaded_to_att',
        'has_created_account',
        'ess_status',
        'has_downloadded_to_training_app',
        'nama_panggilan',
        'nama_ibu_kandung',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'status_pernikahan',
        'kewarganegaraan',
        'agama',
        'nomor_ktp',
        'nomor_kk',
        'no_telp',
        'no_handphone',
        'alamat_email',
        'ktp_provinsi_id',
        'ktp_kabupaten_kota_id',
        'ktp_kecamatan_id',
        'ktp_desa_id',
        'ktp_rw',
        'ktp_rt',
        'domisili_provinsi_id',
        'domisili_kabupaten_kota_id',
        'domisili_kecamatan_id',
        'domisili_desa_id',
        'domisili_rw',
        'domisili_rt',
        'domisili_alamat',
        'ktp_alamat',
        'catatan',
        'is_resign',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->pegawai,
        'attributes' => $gridColumnPegawai    ]);
    ?>
    <div class="row">
        <h4>Pegawai<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPegawai = [
        ['attribute' => 'id', 'visible' => false],
        'nama_lengkap',
        'has_uploaded_to_att',
        'has_created_account',
        'ess_status',
        'has_downloadded_to_training_app',
        'nama_panggilan',
        'nama_ibu_kandung',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'status_pernikahan',
        'kewarganegaraan',
        'agama',
        'nomor_ktp',
        'nomor_kk',
        'no_telp',
        'no_handphone',
        'alamat_email',
        'ktp_provinsi_id',
        'ktp_kabupaten_kota_id',
        'ktp_kecamatan_id',
        'ktp_desa_id',
        'ktp_rw',
        'ktp_rt',
        'domisili_provinsi_id',
        'domisili_kabupaten_kota_id',
        'domisili_kecamatan_id',
        'domisili_desa_id',
        'domisili_rw',
        'domisili_rt',
        'domisili_alamat',
        'ktp_alamat',
        'catatan',
        'is_resign',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->atasanPegawai,
        'attributes' => $gridColumnPegawai    ]);
    ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\ESettingApprove */

$this->title = Yii::t('frontend', 'Create E Setting Approve');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Setting Approve'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="esetting-approve-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

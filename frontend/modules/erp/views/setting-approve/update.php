<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\ESettingApprove */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'E Setting Approve',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Setting Approve'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="esetting-approve-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EApproval */

$this->title = Yii::t('frontend', 'Create E Approval');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Approval'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eapproval-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

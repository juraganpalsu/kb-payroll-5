<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EApproval */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="eapproval-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'tipe')->textInput(['placeholder' => 'Tipe']) ?>

    <?= $form->field($model, 'tanggal')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('frontend', 'Choose Tanggal'),
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'komentar')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'level')->textInput(['placeholder' => 'Level']) ?>

    <?= $form->field($model, 'table')->textInput(['maxlength' => true, 'placeholder' => 'Table']) ?>

    <?= $form->field($model, 'relasi_id')->textInput(['maxlength' => true, 'placeholder' => 'Relasi']) ?>

    <?= $form->field($model, 'created_by_pk')->textInput(['maxlength' => true, 'placeholder' => 'Created By Pk']) ?>

    <?= $form->field($model, 'created_by_struktur')->textInput(['placeholder' => 'Created By Struktur']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

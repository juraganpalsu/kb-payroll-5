<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\search\EApprovalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-eapproval-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'tipe')->textInput(['placeholder' => 'Tipe']) ?>

    <?= $form->field($model, 'tanggal')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('frontend', 'Choose Tanggal'),
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'komentar')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'level')->textInput(['placeholder' => 'Level']) ?>

    <?php /* echo $form->field($model, 'table')->textInput(['maxlength' => true, 'placeholder' => 'Table']) */ ?>

    <?php /* echo $form->field($model, 'relasi_id')->textInput(['maxlength' => true, 'placeholder' => 'Relasi']) */ ?>

    <?php /* echo $form->field($model, 'created_by_pk')->textInput(['maxlength' => true, 'placeholder' => 'Created By Pk']) */ ?>

    <?php /* echo $form->field($model, 'created_by_struktur')->textInput(['placeholder' => 'Created By Struktur']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('frontend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

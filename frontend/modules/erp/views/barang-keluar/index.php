<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\erp\models\search\EBarangKeluarSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\erp\models\EBarangKeluar;
use frontend\modules\erp\models\EGudang;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Data Barang Keluar');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="ebarang-keluar-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'seq_code',
            'value' => function (EBarangKeluar $model) {
                return $model->ePermintaan->seqCode;
            },
        ],
        [
            'label' => Yii::t('frontend', 'Diminta Oleh'),
            'value' => function (EBarangKeluar $model) {
                return $model->ePermintaan->pegawai->nama;
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Dikeluarkan Oleh'),
            'value' => function (EBarangKeluar $model) {
                return $model->pegawai->nama;
            },
        ],
        [
            'attribute' => 'e_gudang_id',
            'value' => function (EBarangKeluar $model) {
                return $model->eGudang->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-ebarang-keluar-search-e_gudang_id']
        ],
        'keterangan:ntext',
        [
            'attribute' => 'created_at',
            'value' => function (EBarangKeluar $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}'
        ],
    ];
    ?>

    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5],
            'noExportColumns' => [1, 6], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-barang-keluar' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ebarang-keluar']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-th-list"></i>', ['index-detail'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Detail'), 'class' => 'btn btn-primary btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>
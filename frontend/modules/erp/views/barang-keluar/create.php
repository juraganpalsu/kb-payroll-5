<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarangKeluar */

$this->title = Yii::t('frontend', 'Create Barang Keluar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Barang Keluar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ebarang-keluar-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

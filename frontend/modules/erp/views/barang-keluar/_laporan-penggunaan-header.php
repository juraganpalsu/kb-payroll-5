<?php

/**
 * Created by PhpStorm.
 * File Name: _laporan-penggunaan-header.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-06-22
 * Time: 10:14 PM
 */

use frontend\modules\erp\models\form\DeflReport;
use yii\helpers\Html;

/**
 * @var DeflReport $model
 *
 */

?>
<table width="200mm">
    <tr>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
        <td width="8mm">&nbsp;</td>
    </tr>
    <tr>
        <td width="8mm"
            style="vertical-align: top;">
            <div><?php echo Html::img('@frontend/web/pictures/image001.png', ['height' => '40px']); ?></div>
        </td>
        <td width="48mm" colspan="6"
            style=" vertical-align: top; padding-left: 3mm;">
            <div><strong>PT. KOBE BOGA UTAMA</strong><br/></div>
        </td>
        <td class="center" width="80mm" colspan="10">
            <?= Html::tag('span', Yii::t('frontend', 'LAPORAN PENGGUNAAN BARANG'), ['style' => 'font: bold 16px arial; text-decoration: underline']); ?>
            <br>
            <?= Html::tag('span', ''); ?>
        </td>
        <td width="8mm">
            &nbsp;
        </td>
        <td width="16mm" colspan="2"
            style="vertical-align: text-top">
            <?= Yii::t('frontend', 'Created') . ' :'; ?>
        </td>
        <td width="32mm" colspan="4"
            style="vertical-align: text-top">
            <?= date('d-m-Y H:i:s'); ?>
        </td>
    </tr>
    <tr>
        <td colspan="24">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
            <?= Yii::t('frontend', 'Tanggal') . ' :'; ?>
        </td>
        <td colspan="11" class="bold">
            <?= $model->tanggal; ?>
        </td>
        <td colspan="3">
            <?= Yii::t('frontend', 'Kategori') . ' :'; ?>
        </td>
        <td colspan="7" class="bold">
            <?= $model->modelBarang()->_kategori[$model->kategori]; ?>
        </td>
    </tr>
    <tr>
        <td colspan="24" class="head">
            <?= Yii::t('frontend', 'Detail Barang Keluar') ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center head border-bottom">
            <?= Yii::t('frontend', 'Tanggal') ?>
        </td>
        <td colspan="13" class="center head border-bottom">
            <?= Yii::t('frontend', 'Nama Barang') ?>
        </td>
        <td colspan="2" class="center head border-bottom">
            <?= Yii::t('frontend', 'Jumlah') ?>
        </td>
        <td colspan="7" class="center head border-bottom">
            <?= Yii::t('frontend', 'Satuan') ?>
        </td>
        </td>
    </tr>


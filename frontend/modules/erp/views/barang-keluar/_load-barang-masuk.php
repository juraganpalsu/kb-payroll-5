<?php

use frontend\modules\erp\models\EBarangMasukDetail;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * @var ActiveDataProvider $providerEBarangMasukDetail
 * @var string $barangkeluarid
 * @var string $permintaandetailid
 */


$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
    
    $('.btn-submit').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);
?>
    <div class="ebarang-masuk-view">


        <div class="row">
            <div class="col-sm-12">
                <?php
                $gridColumnDetail = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'label' => Yii::t('frontend', 'PO'),
                        'value' => function (EBarangMasukDetail $model) {
                            return $model->eBarangMasuk->po_number;
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Jumlah Masuk'),
                        'value' => function (EBarangMasukDetail $model) {
                            return $model->jumlah;
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Sisa'),
                        'value' => function (EBarangMasukDetail $model) {
                            return $model->jumlah - $model->hitungQtyYgTelahDikeluarkan();
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Stok'),
                        'value' => function (EBarangMasukDetail $model) {
                            return $model->eStok->jumlah;
                        }
                    ],
                    [
                        'label' => Yii::t('frontend', 'Satuan'),
                        'value' => function (EBarangMasukDetail $model) {
                            return $model->eStok->eBarang->eSatuan->nama;
                        }
                    ],
                    'harga',
                    'keterangan',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{gunakan}',
                        'buttons' => [
                            'gunakan' => function ($url, EBarangMasukDetail $model) use ($barangkeluarid, $permintaandetailid) {
                                return Html::a(Yii::t('frontend', 'Gunakan'), ['create-detail', 'id' => $model->id, 'barangkeluarid' => $barangkeluarid, 'permintaandetailid' => $permintaandetailid], ['class' => 'btn btn-warning btn-xs btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Gunakan dari PO {po}', ['po' => $model->eBarangMasuk->po_number])]]);
                            },
                        ]
                    ],
                ];
                ?>
                <?php
                try {
                    echo GridView::widget([
                        'dataProvider' => $providerEBarangMasukDetail,
                        'columns' => $gridColumnDetail,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-barang-masuk-detail']],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'toolbar' => [],
                        'panel' => [
                            'heading' => false,
                            'before' => '{summary}',
                        ],
                        'striped' => true,
                        'responsive' => true,
                        'hover' => true,
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
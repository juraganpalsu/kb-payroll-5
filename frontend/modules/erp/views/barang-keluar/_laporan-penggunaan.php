<?php

/**
 * Created by PhpStorm.
 * File Name: _laporan-penggunaan.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-06-21
 * Time: 10:01 PM
 */


use frontend\modules\erp\models\form\DeflReport;

/**
 * @var DeflReport $model
 * @var array $datas
 */


echo $this->renderAjax('_laporan-penggunaan-header', ['model' => $model]);
foreach ($datas as $keyTanggal => $tanggal) {
    ?>
    <tr>
        <td colspan="24" class="border-bottom-dotted">
            <?= $keyTanggal; ?>
        </td>
    </tr>
    <?php foreach ($tanggal as $dataKeluar) { ?>
        <tr>
            <td colspan="2" class="border-bottom-dotted center">&nbsp;</td>
            <td colspan="3" class="border-bottom-dotted"><?= $dataKeluar['permintaanId'] ?></td>
            <td colspan="5" class="border-bottom-dotted"><?= $dataKeluar['namaPeminta'] ?></td>
            <td colspan="5" class="border-bottom-dotted"><?= $dataKeluar['yangMengeluarkan'] ?></td>
            <td colspan="3" class="border-bottom-dotted center"><?= $dataKeluar['gudang'] ?></td>
            <td colspan="6" class="border-bottom-dotted"><?= $dataKeluar['keterangan'] ?></td>
        </tr>
        <?php foreach ($dataKeluar['details'] as $detail) { ?>
            <tr>
                <td colspan="2" class="border-bottom-dotted center">&nbsp;</td>
                <td colspan="3" class="border-bottom-dotted center"><?= Yii::t('frontend', 'Nama Barang') ?></td>
                <td colspan="10" class="border-bottom-dotted"><?= $detail['barang'] ?></td>
                <td colspan="2" class="border-bottom-dotted"><?= Yii::t('frontend', 'Jumlah') ?></td>
                <td colspan="2" class="border-bottom-dotted"><?= $detail['jumlah'] ?></td>
                <td colspan="2" class="border-bottom-dotted center"><?= Yii::t('frontend', 'Satuan') ?></td>
                <td colspan="3" class="border-bottom-dotted"><?= $detail['satuan'] ?></td>
            </tr>
            <?php
        }
    }
}
?>
</table>


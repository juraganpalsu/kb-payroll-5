<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarangKeluarDetail */

/* @var $form kartik\widgets\ActiveForm */

use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$js = <<< JS
    $(function() {
        $('#form-ebarang-keluar').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>

<div class="ebarang-keluar-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ebarang-keluar',
        'action' => $model->isNewRecord ? Url::to(['create-detail', 'id' => $model->e_barang_masuk_detail_id, 'barangkeluarid' => $model->e_barang_keluar_id, 'permintaandetailid' => $model->e_permintaan_detail_id]) : Url::to(['update-detail', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['cou-detail-validation', 'isnew' => $model->isNewRecord]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>


    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'jumlah')->textInput(['type' => 'number', 'max' => $model->jumlah, 'placeholder' => Yii::t('frontend', 'Jumlah')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
            </div>
        </div>


        <?= $form->field($model, 'e_barang_keluar_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'e_permintaan_detail_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'e_barang_masuk_detail_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'e_stok_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'e_satuan_id', ['template' => '{input}'])->hiddenInput(); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success submit-btn' : 'btn btn-primary submit-btn']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>


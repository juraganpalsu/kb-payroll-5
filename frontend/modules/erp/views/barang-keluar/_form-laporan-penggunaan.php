<?php

/**
 * Created by PhpStorm.
 * File Name: _form-laporan-penggunaan.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-06-21
 * Time: 9:40 PM
 */

use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\form\DeflReport;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model DeflReport */
/* @var $form kartik\widgets\ActiveForm */


$this->title = Yii::t('frontend', 'Form Laporan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Barang Keluar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<< JS
    $(function() {
        $('#form-input').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    let win = window.open(dt.data.url, '_blank');
                    win.focus();
                    location.reload();
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>

<div class="penggunaan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-input',
        'action' => Url::to(['defl-report-form']),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['defl-report-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'kategori')->dropDownList($model->modelBarang()->_kategori) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'tanggal', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                    'options' => ['class' => 'drp-container form-group', 'id' => 'ads']
                ])->widget(DateRangePicker::class, [
                    'options' => [
                        'id' => 'splsearch-tanggal-form',
                        'class' => 'form-control',
                    ],
                    'convertFormat' => true,
                    'useWithAddon' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'gudangId')->widget(Select2::class, [
                    'data' => ArrayHelper::map(EGudang::find()->orderBy('id')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Pilih satuan')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('frontend', 'Create'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        print_r($e->getMessage());
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>


<?php

use common\components\Status;
use frontend\modules\erp\models\EBarangKeluarDetail;
use frontend\modules\erp\models\EPermintaanDetail;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarangKeluar */


/**
 * @var ActiveDataProvider $providerEBarangKeluarDetail
 * @var ActiveDataProvider $providerEPermintaanDetail
 *
 */


$this->title = $model->ePermintaan->seqCode . ' - ' . $model->keterangan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Barang Keluar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    
    $('.btn-delete-detail').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        btn.button('loading');
        $.post(btn.attr('href'))
        .done(function (dt) {
            console.log(dt);
            if(dt.status === true){
                location.reload();
            }
        });
        return false;
    });
    
    $('.btn-submit').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);

$modelBarangKeluar = $model;

?>
    <div class="ebarang-keluar-view">

        <div class="row">
            <div class="col-sm-3">

                <?php if ($model->last_status == Status::OPEN) { ?>

                    <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                    <?php
                    if ($model->eBarangKeluarDetails) {
                        echo Html::a(Yii::t('frontend', 'Submit'), ['submit', 'id' => $model->id], ['class' => 'btn btn-warning btn-submit', 'data' => ['confirm' => Yii::t('frontend', 'Setelah disubmit, data tidak dapat diedit, Anda Yakin?'), 'method' => 'post']]);
                    }
                    ?>

                <?php } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?php
                $gridColumn = [
                    [
                        'attribute' => 'id', 'visible' => false
                    ],
                    [
                        'attribute' => 'e_permintaan_id',
                        'value' => $model->ePermintaan->seqCode
                    ],
                    [
                        'attribute' => 'e_gudang_id',
                        'label' => Yii::t('frontend', 'Gudang Yg Dituju'),
                        'value' => $model->eGudang->nama
                    ],
                    [
                        'label' => Yii::t('frontend', 'Diminta Oleh'),
                        'value' => $model->ePermintaan->pegawai->nama
                    ],
                    [
                        'attribute' => 'pegawai_id',
                        'label' => Yii::t('frontend', 'Dikeluarkan Oleh'),
                        'value' => $model->pegawai->nama
                    ],
                    'keterangan:ntext',
                    [
                        'attribute' => 'lock', 'visible' => false
                    ],
                ];
                try {
                    echo DetailView::widget(['model' => $model,
                        'attributes' => $gridColumn]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2><?= Yii::t('frontend', 'Daftar Barang Keluar') ?></h2>
            <?php
            $gridColumnBarangKeluarDetail = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                [
                    'label' => Yii::t('frontend', 'Barang'),
                    'value' => function (EBarangKeluarDetail $model) {
                        return $model->eStok->eBarang->nama;
                    }
                ],
                'jumlah',
                [
                    'label' => Yii::t('frontend', 'Satuan'),
                    'value' => function (EBarangKeluarDetail $model) {
                        return $model->eStok->eBarang->eSatuan->nama;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete-detail}',
                    'buttons' => [
                        'delete-detail' => function ($url, EBarangKeluarDetail $model) {
                            if ($model->last_status == Status::OPEN) {
                                return Html::a(Yii::t('frontend', 'Batal'), $url, ['class' => 'btn btn-warning btn-xs btn-delete-detail']);
                            }
                            return '';
                        },
                    ]
                ],
            ];
            ?>
            <?php
            try {
                echo GridView::widget([
                    'dataProvider' => $providerEBarangKeluarDetail,
                    'columns' => $gridColumnBarangKeluarDetail,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-barang-keluar-detail']],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'toolbar' => [],
                    'panel' => [
                        'heading' => false,
                        'before' => '{summary}',
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2><?= Yii::t('frontend', 'Daftar Permintaan') ?></h2>
            <?php
            $gridColumnDetail = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                [
                    'label' => Yii::t('frontend', 'Barang'),
                    'value' => function (EPermintaanDetail $model) {
                        return $model->eStok->eBarang->nama;
                    }
                ],
                'jumlah',
                [
                    'label' => Yii::t('frontend', 'Satuan'),
                    'value' => function (EPermintaanDetail $model) {
                        return $model->eStok->eBarang->eSatuan->nama;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{load-barang-masuk}',
                    'buttons' => [
                        'load-barang-masuk' => function ($url, EPermintaanDetail $model) use ($modelBarangKeluar) {
                            if ($model->last_status == Status::FINISHED && !$modelBarangKeluar->cekPermintaanTelahDimasukkan($model->id) && $model->hitungQtyYgTelahDikeluarkan() < $model->jumlah) {
                                return Html::a(Yii::t('frontend', 'Tambahkan'), ['load-barang-masuk', 'permintaandetailid' => $model->id, 'barangkeluarid' => $modelBarangKeluar->id], ['class' => 'btn btn-primary btn-xs btn-call-modal', 'data' => ['header' => Yii::t('frontend', 'Daftar masuk {barang}', ['barang' => $model->eStok->eBarang->nama])]]);
                            }
                            return '';
                        },
                    ]
                ],
            ];
            ?>
            <?php
            try {
                echo GridView::widget([
                    'dataProvider' => $providerEPermintaanDetail,
                    'columns' => $gridColumnDetail,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-permintaan-detail']],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'toolbar' => [],
                    'panel' => [
                        'heading' => false,
                        'before' => '{summary}',
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
<?php
Modal::begin(['options' => ['id' => 'form-modal',
    'tabindex' => false],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
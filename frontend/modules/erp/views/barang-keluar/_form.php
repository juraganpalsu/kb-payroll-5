<?php

use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarangKeluar */
/* @var $form kartik\widgets\ActiveForm */


$js = <<< JS
    $(function() {
        $('#form-barang-keluar').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>

<div class="epermintaan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-barang-keluar',
        'action' => $model->isNewRecord ? Url::to(['create', 'permintaanid' => $model->e_permintaan_id]) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>


    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-sm-8">
                <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <?= $form->field($model, 'e_permintaan_id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <?= $form->field($model, 'e_gudang_id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success submit-btn' : 'btn btn-primary submit-btn']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>


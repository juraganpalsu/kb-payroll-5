<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarangKeluar */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Barang Keluar',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Barang Keluar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="ebarang-keluar-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

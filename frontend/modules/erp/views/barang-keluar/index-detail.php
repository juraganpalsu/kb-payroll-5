<?php
/**
 * Created by PhpStorm.
 * File Name: index-detail.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 12/03/2021
 * Time: 23:04
 */

/* @var $this yii\web\View */
/* @var $searchModel \frontend\modules\erp\models\search\EBarangKeluarSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Status;
use frontend\modules\erp\models\EBarangKeluarDetail;
use frontend\modules\erp\models\EGudang;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('frontend', 'Data Barang Keluar Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Barang Keluar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ebarang-keluar-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'seq_code',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eBarangKeluar->ePermintaan->seqCode;
            },
        ],
        [
            'attribute' => 'kategori',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eStok->eBarang->kategori_;
            }
        ],
        [
            'attribute' => 'nama_barang',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eStok->eBarang->nama;
            }
        ],
        [
            'attribute' => 'harga',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eStok->eBarang->harga_terakhir;
            }
        ],
        [
            'attribute' => 'jumlah',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->jumlah;
            }
        ],
        [
            'attribute' => 'e_satuan_id',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eSatuan->nama;
            },
        ],
        [
            'attribute' => 'pegawai',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eBarangKeluar->ePermintaan->pegawai->nama;
            }
        ],
        [
            'attribute' => 'wilayah',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eBarangKeluar->ePermintaan->areaKerja->areaWilayah();
            }
        ],
        [
            'attribute' => 'gedung',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eBarangKeluar->ePermintaan->areaKerja->areaGedung();
            }
        ],
        [
            'attribute' => 'area',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eBarangKeluar->ePermintaan->areaKerja->nama();
            }
        ],
        [
            'attribute' => 'last_status',
            'value' => function (EBarangKeluarDetail $model) {
                return Status::statuses($model->last_status);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::statuses(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-ebarang-masuk-search-last_status']
        ],
        [
            'attribute' => 'e_gudang_id',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eBarangKeluar->eGudang->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-ebarang-masuk-search-e_gudang_id']
        ],
        [
            'attribute' => 'diproses_oleh',
            'value' => function (EBarangKeluarDetail $model) {
                return $model->eBarangKeluar->pegawai->nama;
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (EBarangKeluarDetail $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
            'noExportColumns' => [1, 15], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-barang-keluar' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ebarang-masuk-detail']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        print_r($e->getMessage());
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

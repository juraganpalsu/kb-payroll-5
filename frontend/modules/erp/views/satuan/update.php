<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\ESatuan */

$this->title = Yii::t('frontend', 'Update {modelClass} : ', [
        'modelClass' => 'Satuan',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Satuan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="esatuan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\ESatuan */

$this->title = Yii::t('frontend', 'Create Satuan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Setting Satuan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="esatuan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

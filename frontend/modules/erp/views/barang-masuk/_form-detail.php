<?php

/**
 * Created by PhpStorm.
 * File Name: _form-detail.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 21/02/21
 * Time: 22.06
 */


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarangMasukDetail */

/* @var $form kartik\widgets\ActiveForm */

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$js = <<< JS
    $(function() {
        $('#form-ebarang-masuk').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>

<div class="ebarang-masuk-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ebarang-masuk',
        'action' => $model->isNewRecord ? Url::to(['create-detail', 'idbarangmasuk' => $model->e_barang_masuk_id]) : Url::to(['update-detail', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['cou-detail-validation', 'isnew' => $model->isNewRecord]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>


    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-6">
                <?php
                echo $form->field($model, 'e_stok_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Barang'),
                        'multiple' => false,
                        'disabled' => $model->isNewRecord ? false : true
                    ],
                    'data' => $model->isNewRecord ? [] : [$model->e_stok_id => $model->eStok->eBarang->nama],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/erp/barang/get-barang-by-gudang-for-select2', 'gudangid' => $model->eBarangMasuk->e_gudang_id]),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term};}')
                        ],
                    ],
                ]);
                if (!$model->isNewRecord) {
                    echo $form->field($model, 'e_stok_id', ['template' => '{input}'])->hiddenInput();
                }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'jumlah')->textInput(['maxlength' => true, 'placeholder' => Yii::t('frontend', 'Jumlah')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'harga')->textInput(['maxlength' => true, 'placeholder' => Yii::t('frontend', 'Harga')]) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
            </div>
        </div>


        <?= $form->field($model, 'e_barang_masuk_id', ['template' => '{input}'])->textInput(['style' => 'display:none', 'value' => $model->e_barang_masuk_id]); ?>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>


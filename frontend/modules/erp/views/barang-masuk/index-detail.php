<?php

/**
 * Created by PhpStorm.
 * File Name: index-detail.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 3/11/2021
 * Time: 2:58 PM
 */


/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\erp\models\search\EBarangMasukSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Status;
use frontend\modules\erp\models\EBarangMasukDetail;
use frontend\modules\erp\models\EGudang;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('frontend', 'Data Barang Masuk Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Barang Masuk'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ebarang-masuk-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'po_number',
            'value' => function (EBarangMasukDetail $model) {
                return $model->eBarangMasuk->po_number;
            }
        ],
        [
            'attribute' => 'kategori',
            'value' => function (EBarangMasukDetail $model) {
                return $model->eStok->eBarang->kategori_;
            }
        ],
        [
            'label' => Yii::t('frontend', 'Barang'),
            'attribute' => 'nama_barang',
            'value' => function (EBarangMasukDetail $model) {
                return $model->eStok->eBarang->nama;
            }
        ],
        [
            'attribute' => 'harga',
            'value' => function (EBarangMasukDetail $model) {
                return $model->eStok->eBarang->harga_terakhir;
            }
        ],
        [
            'attribute' => 'jumlah',
            'value' => function (EBarangMasukDetail $model) {
                return $model->jumlah;
            }
        ],
        [
            'attribute' => 'e_satuan_id',
            'value' => function (EBarangMasukDetail $model) {
                return $model->eSatuan->nama;
            }
        ],
        [
            'attribute' => 'last_status',
            'value' => function (EBarangMasukDetail $model) {
                return Status::statuses($model->last_status);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::statuses(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-ebarang-masuk-search-last_status']
        ],
        [
            'attribute' => 'e_gudang_id',
            'value' => function (EBarangMasukDetail $model) {
                return $model->eBarangMasuk->eGudang->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(EGudang::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Gudang', 'id' => 'grid-ebarang-masuk-search-e_gudang_id']
        ],
        [
            'attribute' => 'created_by',
            'value' => function (EBarangMasukDetail $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (EBarangMasukDetail $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6],
            'noExportColumns' => [1, 7], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'data-barang-masuk' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ebarang-masuk-detail']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        print_r($e->getMessage());
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

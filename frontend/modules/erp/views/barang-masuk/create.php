<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarangMasuk */

$this->title = Yii::t('frontend', 'Create E Barang Masuk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'E Barang Masuk'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ebarang-masuk-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

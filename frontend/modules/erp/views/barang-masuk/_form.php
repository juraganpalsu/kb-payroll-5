<?php

use frontend\modules\erp\models\EGudang;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\erp\models\EBarangMasuk */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
    $(function() {
        $('#form-ebarang-masuk').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
            });
            return false;
        });
    })
JS;

$this->registerJs($js);
?>

<div class="ebarang-masuk-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ebarang-masuk',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>


    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'tanggal_terima')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'po_number')->textInput(['maxlength' => true, 'placeholder' => 'Po Number']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'e_gudang_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(EGudang::find()->orderBy('id')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Gudang')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
            </div>
        </div>


        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>

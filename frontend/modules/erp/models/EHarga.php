<?php

namespace frontend\modules\erp\models;

use frontend\modules\erp\models\base\EHarga as BaseEHarga;

/**
 * This is the model class for table "e_harga".
 */
class EHarga extends BaseEHarga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_barang_id'], 'required'],
            [['harga'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_barang_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->updateHargaTerakhir();
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Mengupdate harga terakhir
     *
     * Harga terakhir akan diupdate setiap kali ada perubahan harga
     *
     */
    public function updateHargaTerakhir(): void
    {
        $modelBarang = EBarang::findOne($this->e_barang_id);
        $modelBarang->harga_terakhir = $this->harga;
        $modelBarang->save();
    }

}

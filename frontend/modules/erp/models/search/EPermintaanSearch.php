<?php

namespace frontend\modules\erp\models\search;

use common\components\Status;
use frontend\models\User;
use frontend\modules\erp\models\EPermintaan;
use frontend\modules\erp\models\EPermintaanDetail;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\erp\models\search\EPermintaanSearch represents the model behind the search form about `frontend\modules\erp\models\EPermintaan`.
 *
 * @property string $nama_barang
 */
class EPermintaanSearch extends EPermintaan
{

    public $nama_barang;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'keterangan', 'e_gudang_id', 'pegawai_id', 'area_kerja_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'seq_code', 'nama_barang'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'last_status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EPermintaan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /** @var User $modelUser */
        $modelUser = Yii::$app->user->identity;

        $strukturs = array_merge($modelUser->getStrukturBawahan(), $modelUser->getStrukturAtasan());

        $gudangIds = [];

        if ($modelPegawai = User::pegawai()) {
            $gudangIds = $modelPegawai->getGudang();
        }

        $query->andWhere(['OR', ['IN', 'e_permintaan.created_by_struktur', $strukturs], ['IN', 'e_permintaan.e_gudang_id', $gudangIds]]);

        if (!empty($this->pegawai_id)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                $q->andWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);
            }]);
        }

        if (!empty($this->created_at)) {
            $tanggal = explode('s/d', $this->created_at);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'e_permintaan.created_at', $awal, $akhir]);
        }


        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'seq_code', $this->seq_code])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['last_status' => $this->last_status])
            ->andFilterWhere(['like', 'area_kerja_id', $this->area_kerja_id]);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }

    /**
     *
     * Query data approval
     *
     * @return ActiveQuery
     */
    public function queryApproval()
    {
        $query = EPermintaan::find();
        $query->andWhere(['e_permintaan.last_status' => Status::SUBMITED]);
        $query->joinWith('approvalsByUser');
        $query->orderBy('e_permintaan.seq_code DESC');
        return $query;
    }

    /**
     *
     * Data Provider Approval
     *
     * @return ActiveDataProvider
     */
    public function searchApproval()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->queryApproval(),
        ]);
        return $dataProvider;
    }

    /**
     *
     * Menghitung jumlah approval dengan status submited
     *
     * @return bool|int|string|null
     */
    public function countApproval()
    {
        return $this->queryApproval()->count();
    }


    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchDetail($params): ActiveDataProvider
    {
        $query = EPermintaanDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('ePermintaan');
        $query->joinWith(['eStok' => function (ActiveQuery $query) {
            $query->joinWith(['eBarang']);
        }]);

        if (!empty($this->pegawai_id)) {
            $query->joinWith(['ePermintaan' => function (ActiveQuery $query) {
                $query->joinWith(['pegawai' => function (ActiveQuery $q) {
                    $q->andWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);
                }]);
            }]);
        }

        /** @var User $modelUser */
        $modelUser = Yii::$app->user->identity;

        $strukturs = array_merge($modelUser->getStrukturBawahan(), $modelUser->getStrukturAtasan());

        $gudangIds = [];

        if ($modelPegawai = User::pegawai()) {
            $gudangIds = $modelPegawai->getGudang();
        }

        $query->andWhere(['OR', ['IN', 'e_permintaan.created_by_struktur', $strukturs], ['IN', 'e_permintaan.e_gudang_id', $gudangIds]]);


        if (!empty($this->created_at)) {
            $tanggal = explode('s/d', $this->created_at);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'e_permintaan.created_at', $awal, $akhir]);
        }


        if (!empty($this->created_at)) {
            $tanggal = explode('s/d', $this->created_at);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'e_permintaan.tanggal_terima', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'e_barang_masuk.keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'e_barang.nama', $this->nama_barang])
            ->andFilterWhere(['e_barang_masuk.last_status' => $this->last_status])
            ->andFilterWhere(['e_barang_masuk.e_gudang_id' => $this->e_gudang_id]);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }


    /**
     * @param string $id
     * @return ActiveDataProvider
     */
    public function searchApprovalDetail(string $id): ActiveDataProvider
    {
        $query = EPermintaanDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andWhere(['e_permintaan_id' => $id]);

        $query->joinWith('approvalsByUser');

        return $dataProvider;
    }
}

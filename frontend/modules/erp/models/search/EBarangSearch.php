<?php

namespace frontend\modules\erp\models\search;

use frontend\modules\erp\models\EBarang;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\erp\models\search\EBarangSearch represents the model behind the search form about `frontend\modules\erp\models\EBarang`.
 */
class EBarangSearch extends EBarang
{
    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            [['id', 'nama', 'e_satuan_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'is_aktif'], 'safe'],
            [['harga_terakhir'], 'number'],
            [['kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = EBarang::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'harga_terakhir' => $this->harga_terakhir,
            'kategori' => $this->kategori,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'created_by_struktur' => $this->created_by_struktur,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
            'is_aktif' => $this->is_aktif,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'e_satuan_id', $this->e_satuan_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\erp\models\search;

use Yii;
use frontend\modules\erp\models\EBarangKeluar;
use frontend\modules\erp\models\EBarangKeluarDetail;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\erp\models\search\EBarangKeluarSearch represents the model behind the search form about `frontend\modules\erp\models\EBarangKeluar`.
 *
 * @property string $nama_barang
 * @property integer $seq_code
 */
class EBarangKeluarSearch extends EBarangKeluar
{
    public $nama_barang;
    public $seq_code;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'keterangan', 'e_permintaan_id', 'e_gudang_id', 'pegawai_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'nama_barang', 'seq_code'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'seq_code'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EBarangKeluar::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->seq_code)) {
            $query->joinWith(['ePermintaan' => function (ActiveQuery $query) {
                $query->andWhere(['like', 'e_permintaan.seq_code', $this->seq_code]);
            }]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'e_permintaan_id', $this->e_permintaan_id])
            ->andFilterWhere(['like', 'e_gudang_id', $this->e_gudang_id])
            ->andFilterWhere(['like', 'pegawai_id', $this->pegawai_id]);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchDetail($params): ActiveDataProvider
    {
        $query = EBarangKeluarDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->seq_code)) {
            $query->joinWith(['eBarangKeluar' => function (ActiveQuery $query) {
                $query->joinWith(['ePermintaan' => function (ActiveQuery $query) {
                    $query->andWhere(['like', 'e_permintaan.seq_code', $this->seq_code]);
                }]);
            }]);
        }

        $query->joinWith('eBarangKeluar');
        $query->joinWith(['eStok' => function (ActiveQuery $query) {
            $query->joinWith(['eBarang']);
        }]);


        $query->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'e_permintaan_id', $this->e_permintaan_id])
            ->andFilterWhere(['like', 'e_gudang_id', $this->e_gudang_id])
            ->andFilterWhere(['like', 'e_barang.nama', $this->nama_barang])
            ->andFilterWhere(['like', 'pegawai_id', $this->pegawai_id]);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }
}

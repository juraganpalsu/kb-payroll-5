<?php

namespace frontend\modules\erp\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\erp\models\EStokPergerakan;

/**
 * frontend\modules\erp\models\search\EStokPergerakanSearch represents the model behind the search form about `frontend\modules\erp\models\EStokPergerakan`.
 */
 class EStokPergerakanSearch extends EStokPergerakan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'e_stok_id', 'e_satuan_id', 'table', 'relasi_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['jumlah'], 'number'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EStokPergerakan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'jumlah' => $this->jumlah,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'created_by_struktur' => $this->created_by_struktur,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'e_stok_id', $this->e_stok_id])
            ->andFilterWhere(['like', 'e_satuan_id', $this->e_satuan_id])
            ->andFilterWhere(['like', 'table', $this->table])
            ->andFilterWhere(['like', 'relasi_id', $this->relasi_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\erp\models\search;

use frontend\models\User;
use frontend\modules\erp\models\EStok;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\erp\models\search\EStokSearch represents the model behind the search form about `frontend\modules\erp\models\EStok`.
 */
class EStokSearch extends EStok
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'e_barang_id', 'e_gudang_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['jumlah'], 'number'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = EStok::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $gudangIds = [];
        if ($modelPegawai = User::pegawai()) {
            $gudangIds = $modelPegawai->getGudang();
        }

        $query->andWhere(['IN', 'e_gudang_id', $gudangIds]);

        $query->andFilterWhere([
            'jumlah' => $this->jumlah,
            'e_barang_id' => $this->e_barang_id,
            'e_gudang_id' => $this->e_gudang_id,
        ]);

        return $dataProvider;
    }
}

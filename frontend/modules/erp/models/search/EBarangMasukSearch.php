<?php

namespace frontend\modules\erp\models\search;

use frontend\modules\erp\models\EBarangMasuk;
use frontend\modules\erp\models\EBarangMasukDetail;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\erp\models\search\EBarangMasukSearch represents the model behind the search form about `frontend\modules\erp\models\EBarangMasuk`.
 *
 * @property string $nama_barang
 */
class EBarangMasukSearch extends EBarangMasuk
{

    public $nama_barang;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'po_number', 'keterangan', 'e_gudang_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at', 'nama_barang', 'tanggal_terima'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'last_status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = EBarangMasuk::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->tanggal_terima)) {
            $tanggal = explode('s/d', $this->tanggal_terima);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'e_barang_masuk.tanggal_terima', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'po_number', $this->po_number])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'e_gudang_id', $this->e_gudang_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchDetail($params): ActiveDataProvider
    {
        $query = EBarangMasukDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('eBarangMasuk');
        $query->joinWith(['eStok' => function (ActiveQuery $query) {
            $query->joinWith(['eBarang']);
        }]);


        if (!empty($this->tanggal_terima)) {
            $tanggal = explode('s/d', $this->tanggal_terima);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'e_barang_masuk.tanggal_terima', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'e_barang_masuk.po_number', $this->po_number])
            ->andFilterWhere(['like', 'e_barang_masuk.keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'e_barang_masuk.keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'e_barang.nama', $this->nama_barang])
            ->andFilterWhere(['e_barang_masuk.last_status' => $this->last_status])
            ->andFilterWhere(['e_barang_masuk.e_gudang_id' => $this->e_gudang_id]);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }
}

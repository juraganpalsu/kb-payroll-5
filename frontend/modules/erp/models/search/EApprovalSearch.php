<?php

namespace frontend\modules\erp\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\erp\models\EApproval;

/**
 * frontend\modules\erp\models\search\EApprovalSearch represents the model behind the search form about `frontend\modules\erp\models\EApproval`.
 */
 class EApprovalSearch extends EApproval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipe', 'level', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal', 'komentar', 'table', 'relasi_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EApproval::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tipe' => $this->tipe,
            'tanggal' => $this->tanggal,
            'level' => $this->level,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'created_by_struktur' => $this->created_by_struktur,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'komentar', $this->komentar])
            ->andFilterWhere(['like', 'table', $this->table])
            ->andFilterWhere(['like', 'relasi_id', $this->relasi_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

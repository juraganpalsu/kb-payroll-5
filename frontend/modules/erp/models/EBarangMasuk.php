<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use frontend\modules\erp\models\base\EBarangMasuk as BaseEBarangMasuk;
use ReflectionClass;

/**
 * This is the model class for table "e_barang_masuk".
 */
class EBarangMasuk extends BaseEBarangMasuk
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['po_number', 'e_gudang_id', 'tanggal_terima'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_gudang_id'], 'string', 'max' => 36],
            [['po_number'], 'string', 'max' => 45],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock', 'last_status'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->setStatus(Status::OPEN);
        }
    }


    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $modelStatus = new EStatus();
        $modelStatus->status = $status;
        $modelStatus->relasi_id = $this->id;
        $modelStatus->table = (new ReflectionClass($this))->getName();
        if ($modelStatus->save()) {
            $this->setLastStatus($modelStatus->status);
        }
    }

    /**
     * @param int $status
     */
    public function setLastStatus(int $status): void
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        $model->save();
    }


    /**
     *
     */
    public function submit(): void
    {
        $this->setStatus(Status::FINISHED);
        $this->updateStok();
    }

    /**
     *
     */
    public function updateStok(): void
    {
        foreach ($this->eBarangMasukDetails as $detail) {
            $detail->setStatus(Status::FINISHED);
            $detail->updateStok();
        }
    }

}

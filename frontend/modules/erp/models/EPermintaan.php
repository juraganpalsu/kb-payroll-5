<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use Exception;
use frontend\models\User;
use frontend\modules\erp\models\base\EPermintaan as BaseEPermintaan;
use frontend\modules\erp\models\search\EPermintaanSearch;
use frontend\modules\struktur\models\Helper;
use ReflectionClass;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;

/**
 * This is the model class for table "e_permintaan".
 *
 * @property string $code
 * @property string $seqCode
 *
 *
 * @property EApproval $approvalByUser
 * @property EApproval[] $approvalsByUser
 * @property EApproval[] $approvals
 *
 */
class EPermintaan extends BaseEPermintaan
{


    const PERMINTAAN_APPROVAL_MODUL_ID = 2;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['e_gudang_id'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'last_status', 'seq_code'], 'integer'],
            [['id', 'e_gudang_id'], 'string', 'max' => 36],
            [['pegawai_id', 'area_kerja_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return string
     */
    public function getSeqCode()
    {
        return 'PRMT-' . $this->seq_code;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $modelPegawai = User::pegawai();
        if ($modelPegawai && $insert) {
            $this->pegawai_id = $modelPegawai->id;
            if ($areaKerja = $modelPegawai->areaKerja) {
                $this->area_kerja_id = $areaKerja->id;
            }
        }
        return parent::beforeSave($insert);
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->setStatus(Status::OPEN);
        }
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return explode('-', $this->id)[0];
    }


    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $modelStatus = new EStatus();
        $modelStatus->status = $status;
        $modelStatus->relasi_id = $this->id;
        $modelStatus->table = (new ReflectionClass($this))->getName();
        if ($modelStatus->save()) {
            $this->setLastStatus($modelStatus->status);
        }
    }

    /**
     * @param int $status
     */
    public function setLastStatus(int $status)
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        $model->save();
    }


    /**
     *
     */
    public function submit(): bool
    {
        try {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $getUserApprovals = Helper::getRuteApproval($user->id, self::PERMINTAAN_APPROVAL_MODUL_ID);

            //cek apakah terdapat user approval untuk modul ini
            if (!empty($getUserApprovals)) {
                $this->setStatus(Status::SUBMITED);
                $this->setStatusDetail(Status::SUBMITED);

                $this->createApprovals();
                $this->createApprovalDetails();
            } else {
                $this->setStatus(Status::FINISHED);
                $this->setStatusDetail(Status::FINISHED);
            }

            $this->sendMailNotification();


            return true;
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return false;
    }

    /**
     * @param int $status
     */
    public function setStatusDetail(int $status)
    {
        foreach ($this->ePermintaanDetails as $detail) {
            $detail->setStatus($status);
        }
    }

    /**
     * @return float
     */
    public function hitungTotalJumlahPermintaan(): float
    {
        return (float)EPermintaanDetail::find()->andWhere(['e_permintaan_id' => $this->id])->sum('jumlah');
    }

    /**
     * @return float
     */
    public function hitungYgTelahDikeluarkan(): float
    {
        return (float)EBarangKeluarDetail::find()->joinWith('eBarangKeluar')->andWhere(['e_barang_keluar.e_permintaan_id' => $this->id])->sum('e_barang_keluar_detail.jumlah');
    }

    /**
     * @return bool
     */
    public function checkAllDetailAreClosed(): bool
    {
        return EPermintaanDetail::find()->andWhere(['e_permintaan_id' => $this->id])->andWhere(['!=', 'last_status', Status::CLOSED])->exists();
    }

    /**
     *
     */
    public function createApprovals()
    {
        $approvals = Helper::getRuteApproval(Yii::$app->user->id, self::PERMINTAAN_APPROVAL_MODUL_ID);
        if (!empty($approvals)) {
            $parentLevel = 0;
            $parentId = '0';
            $oldParentId = '0';
            $urutan = 1;
            foreach ($approvals as $approval) {
                $EApproval = new EApproval();
                $EApproval->pegawai_id = $approval['pegawai_id'];
                $EApproval->perjanjian_kerja_id = $approval['perjanjian_kerja_id'];
                $EApproval->pegawai_struktur_id = $approval['pegawai_struktur_id'];
                $EApproval->level = $approval['level'];
                $EApproval->relasi_id = $this->id;
                $EApproval->table = self::tableName();
                if ($parentId == '0') {
                    $EApproval->urutan = $urutan;
                    $EApproval->e_approval_id = $parentId;
                } else {
                    if ($parentLevel == $EApproval->level) {
                        $EApproval->urutan = $urutan;
                        $EApproval->e_approval_id = $oldParentId;
                    } else {
                        $urutan++;
                        $EApproval->urutan = $urutan;
                        $EApproval->e_approval_id = $parentId;
                    }
                }
                if ($EApproval->save()) {
                    $parentId = $EApproval->id;
                    $oldParentId = $EApproval->e_approval_id;
                    $parentLevel = $EApproval->level;
                }
            }
        }
    }


    /**
     * Menyimpan data kedalam model table approval
     * --detail
     * ---user approval level 0
     * ---user approval level 1
     * ---user approval level 2
     * ---dst
     */
    public function createApprovalDetails()
    {
        foreach ($this->ePermintaanDetails as $ePermintaanDetail) {
            $approvals = Helper::getRuteApproval(Yii::$app->user->id, self::PERMINTAAN_APPROVAL_MODUL_ID);
            if (!empty($approvals)) {
                $parentLevel = 0;
                $parentId = '0';
                $oldParentId = '0';
                $urutan = 1;
                foreach ($approvals as $approval) {
                    $detailApproval = new EApprovalDetail();
                    $detailApproval->pegawai_id = $approval['pegawai_id'];
                    $detailApproval->perjanjian_kerja_id = $approval['perjanjian_kerja_id'];
                    $detailApproval->pegawai_struktur_id = $approval['pegawai_struktur_id'];
                    $detailApproval->level = $approval['level'];
                    $detailApproval->relasi_id = $ePermintaanDetail->id;
                    $detailApproval->table = EPermintaanDetail::tableName();
                    if ($parentId == '0') {
                        $detailApproval->urutan = $urutan;
                        $detailApproval->e_approval_detail_id = $parentId;
                    } else {
                        if ($parentLevel == $detailApproval->level) {
                            $detailApproval->urutan = $urutan;
                            $detailApproval->e_approval_detail_id = $oldParentId;
                        } else {
                            $urutan++;
                            $detailApproval->urutan = $urutan;
                            $detailApproval->e_approval_detail_id = $parentId;
                        }
                    }
                    if ($detailApproval->save()) {
                        $parentId = $detailApproval->id;
                        $oldParentId = $detailApproval->e_approval_detail_id;
                        $parentLevel = $detailApproval->level;
                    }
                }
            }
        }
    }


    /**
     * Malakukan check approval semua detail, apakah telah dilakukan aksi approval atau belum
     *
     * Jika semua product telah dilakukan approval oleh user approval, maka return false, karena sudah tidak ada status Status::DEFLT
     * Jika ada walaupun hanya satu item yg belum di approval, maka return true, karena masih ada status Status::DEFLT
     *
     *
     * @param int $status status yang akan dicek, secara default adalah status Status::DEFLT,
     * ini untuk mengecek apakah semua item telah dilakukan approval atau belum
     * Jika diisi paramnya dengan Status::APPROVE, maka akan cek apakah ada salah satu product item yg di approve
     * Jika ada satu item yg sesuai dengan param, maka return true, sebaliknya false
     *
     * @return bool
     */
    public function checkStatusApprovalByUser(int $status = Status::DEFLT): bool
    {
        $approvalTypes = [];
        array_map(function (EPermintaanDetail $x) use (&$approvalTypes) {
            if ($x->approvalByUser) {
                $approvalTypes[] = $x->approvalByUser->tipe;
            }
        }, $this->ePermintaanDetails);
        if (in_array($status, $approvalTypes)) {
            return true;
        }
        return false;
    }

    /**
     * Cek status approval pada suatu detail
     *
     * @param int $status
     * @return bool true jika ada yg sesuai dengan param, sebaliknya false
     */
    public function checkStatusApprovals(int $status = Status::REJECT): bool
    {
        $model = self::findOne($this->id);
        $rejects = [];
        array_map(function (EPermintaanDetail $x) use (&$rejects, $status) {
            $rejects[] = $x->checkStatusApprovals($status);
        }, $model->ePermintaanDetails);
        if (in_array(true, $rejects)) {
            return true;
        }
        return false;
    }

    /**
     * Melakukan aproval items
     *
     * @param int $status
     * @param string $comment
     */
    public function approval(int $status, string $comment = '')
    {
        array_map(function (EPermintaanDetail $x) use ($status, $comment) {
            $x->approval($status, $comment);
        }, $this->ePermintaanDetails);
    }


    /**
     * @param string $komentar
     */
    public function createApproval(string $komentar = '')
    {
        if (!$this->checkStatusApprovalByUser()) {
            if ($this->checkStatusApprovalByUser(Status::APPROVE)) {
                $this->approvalByUser->createApproval();
            } else {
                $this->approvalByUser->createApproval(Status::REJECT, $komentar);
            }
        }
    }


    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalByUser(): ActiveQuery
    {
        $pegawaiId = 0;
        if ($pegawai = User::pegawai()) {
            $pegawaiId = $pegawai->id;
        }
        return $this->hasOne(EApproval::class, ['relasi_id' => 'id'])->andWhere(['e_approval.pegawai_id' => $pegawaiId, 'table' => self::tableName()]);
    }

    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalsByUser(): ActiveQuery
    {
        $pegawaiId = 0;
        if ($pegawai = User::pegawai()) {
            $pegawaiId = $pegawai->id;
        }
        return $this->hasMany(EApproval::class, ['relasi_id' => 'id'])->andWhere(['e_approval.pegawai_id' => $pegawaiId, 'table' => self::tableName()]);
    }

    /**
     * Mengambil semua data user approval
     *
     * @return ActiveQuery
     */
    public function getApprovals(): ActiveQuery
    {
        return $this->hasMany(EApproval::class, ['relasi_id' => 'id'])->andWhere(['table' => self::tableName()]);
    }

    /**
     *
     */
    public function sendMailNotification()
    {
        $getEmailUserWarehouse = EGudangPegawai::find()->where(['notifikasi' => Status::YA, 'e_gudang_id' => $this->e_gudang_id])->all();

        $emails = [];
        /** @var EGudangPegawai $item */
        foreach ($getEmailUserWarehouse as $item) {
            $emails[] = $item->pegawai->alamat_email;
        }

        if ($emails) {
            /** @var Mailer $mail */
            $mail = Yii::$app->mailer_gmail;
            $compose = $mail->compose()
                ->setFrom('hc.system@kobe.co.id');

            $title = Yii::t('frontend', 'Permintaan #{nomor}', ['nomor' => $this->seqCode]);

            $compose->setTo($emails)
                ->setSubject($title);

            $compose->setHtmlBody(Yii::t('frontend', 'Untuk melihat detail permintaan, silahkan {klikDisini} ', ['klikDisini' => Html::a(Yii::t('frontend', 'disini'), Url::to(['erp/permintaan/view', 'id' => $this->id], true))]));
            $compose->send();
        }
    }

    /**
     * @return bool|int|string|null
     */
    public static function countApprovalStatic()
    {
        $model = new EPermintaanSearch();
        return $model->countApproval();
    }
}

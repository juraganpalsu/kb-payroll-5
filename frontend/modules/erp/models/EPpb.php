<?php

namespace frontend\modules\erp\models;

use frontend\models\User;
use frontend\modules\erp\models\base\EPpb as BaseEPpb;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "e_ppb".
 */
class EPpb extends BaseEPpb
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['no', 'tanggal', 'e_gudang_id'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_gudang_id'], 'string', 'max' => 36],
            [['no'], 'string', 'max' => 45],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = EPpb::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
        $gudangIds = [];
        if ($modelPegawai = User::pegawai()) {
            $gudangIds = $modelPegawai->getGudang();
        }

        $query->andWhere(['IN', 'e_gudang_id', $gudangIds]);

        $query->andFilterWhere([
            'e_gudang_id' => $this->e_gudang_id,
        ]);

        return $dataProvider;
    }


    /**
     * Masih butuh diskusi
     */
    public function loadMinimumStok()
    {
        $query = EStok::find()
            ->joinWith('eBarang')
            ->andWhere(['<=', 'e_stok.jumlah', 'e_barang.minimum_stok'])
            ->andWhere(['e_stok.e_gudang_id' => $this->e_gudang_id])
            ->orderBy('e_barang.nama ASC')
            ->all();
        /** @var EStok $stok */
        foreach ($query as $stok) {
            $barang = $stok->eBarang;
            $modelItem = new EPpbItem();
            $modelItem->jumlah = $barang->maksimum_stok ? $barang->maksimum_stok - $stok->jumlah : $barang->minimum_stok;
            $modelItem->sisa = (int)$stok->jumlah;
            $modelItem->keterangan = '-';
            $modelItem->e_stok_id = $stok->id;
            $modelItem->e_ppb_id = $this->id;
            $modelItem->save();
//            print_r($modelItem->errors);
        }
    }

}

<?php

namespace frontend\modules\erp\models;

use frontend\modules\erp\models\base\EStok as BaseEStok;

/**
 * This is the model class for table "e_stok".
 */
class EStok extends BaseEStok
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['e_barang_id', 'e_gudang_id'], 'required'],
            [['jumlah'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_barang_id', 'e_gudang_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock', 'jumlah'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param float $jumlah
     * @param bool $penambahan
     */
    public function updateStok(float $jumlah, bool $penambahan = true)
    {
        if ($penambahan) {
            $this->jumlah += $jumlah;
        } else {
            $this->jumlah -= $jumlah;
        }
        $this->save();
    }

}

<?php

namespace frontend\modules\erp\models;

use Yii;
use \frontend\modules\erp\models\base\EStokPergerakan as BaseEStokPergerakan;

/**
 * This is the model class for table "e_stok_pergerakan".
 */
class EStokPergerakan extends BaseEStokPergerakan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'e_stok_id', 'e_satuan_id', 'relasi_id'], 'required'],
            [['jumlah'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_stok_id', 'e_satuan_id', 'relasi_id'], 'string', 'max' => 36],
            [['table'], 'string', 'max' => 225],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}

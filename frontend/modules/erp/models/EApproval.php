<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use frontend\modules\erp\models\base\EApproval as BaseEApproval;
use frontend\modules\struktur\models\Helper;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "e_approval".
 *
 * @property EApproval $approval
 */
class EApproval extends BaseEApproval
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['tipe', 'level', 'urutan', 'e_approval_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['relasi_id'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['komentar'], 'string'],
            [['table'], 'string', 'max' => 225],
            [['relasi_id'], 'string', 'max' => 36],
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * Melakukan approval,
     * Jika yg melakukan approval mempunyai childs, maka semua childs yg belum melakukan approval akan di tandai SKIPPED
     * @param int $approval_type tipe approval
     * @param string $comment komentar jika detail ditolak
     */
    public function createApproval(int $approval_type = Status::APPROVE, string $comment = '')
    {
        $model = self::findOne($this->id);
        $model->tanggal = $model->tipe == Status::DEFLT ? date('Y-m-d H:i:s') : $model->tanggal;
        $model->tipe = $model->tipe == Status::DEFLT ? $approval_type : $model->tipe;
        $model->komentar = !empty($comment) && $model->tipe == Status::REJECT ? $comment : $model->komentar;
        if ($model->save()) {
            if ($parent = $model->eApproval) {
                if ($parent->tipe == Status::DEFLT && ($model->tipe == Status::REJECT || $model->tipe == Status::SKIP)) {
                    $model->eApproval->createApproval(Status::SKIP);
                }
            }
            if ($child = $model->approval) {
                if ($child->tipe == Status::DEFLT) {
                    $model->approval->createApproval(Status::SKIP);
                }
            }
        }
    }

    /**
     * Relasi has one untuk nested untuk mengambil anak2 nya
     *
     * @return ActiveQuery
     */
    public function getApproval(): ActiveQuery
    {
        return $this->hasOne(EApproval::class, ['e_approval_id' => 'id']);
    }

    /**
     * @param int $modulId
     * @param string $relasiId
     * @param string $tableName
     * @param bool $isHeader
     */
    public static function createApprovals(int $modulId, string $relasiId, string $tableName, bool $isHeader = true)
    {
        $approvals = Helper::getRuteApproval(Yii::$app->user->id, $modulId);
        if (!empty($approvals)) {
            $parentLevel = 0;
            $parentId = '0';
            $oldParentId = '0';
            $urutan = 1;
            foreach ($approvals as $approval) {
                $EApproval = $isHeader ? new EApproval() : new EApprovalDetail();
                $EApproval->pegawai_id = $approval['pegawai_id'];
                $EApproval->perjanjian_kerja_id = $approval['perjanjian_kerja_id'];
                $EApproval->pegawai_struktur_id = $approval['pegawai_struktur_id'];
                $EApproval->level = $approval['level'];
                $EApproval->relasi_id = $relasiId;
                $EApproval->table = $tableName;
                if ($parentId == '0') {
                    $EApproval->urutan = $urutan;
                    if ($isHeader) {
                        $EApproval->e_approval_id = $parentId;
                    } else {
                        $EApproval->e_approval_detail_id = $parentId;
                    }
                } else {
                    if ($parentLevel == $EApproval->level) {
                        $EApproval->urutan = $urutan;
                        if ($isHeader) {
                            $EApproval->e_approval_id = $oldParentId;
                        } else {
                            $EApproval->e_approval_detail_id = $oldParentId;
                        }
                    } else {
                        $urutan++;
                        $EApproval->urutan = $urutan;
                        if ($isHeader) {
                            $EApproval->e_approval_id = $parentId;
                        } else {
                            $EApproval->e_approval_detail_id = $parentId;
                        }
                    }
                }
                if ($EApproval->save()) {
                    $parentId = $EApproval->id;
                    $oldParentId = $isHeader ? $EApproval->e_approval_id : $EApproval->e_approval_detail_id;
                    $parentLevel = $EApproval->level;
                }
            }
        }
    }

}

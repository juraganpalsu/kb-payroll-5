<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use frontend\models\User;
use frontend\modules\erp\models\base\EPermintaanDetail as BaseEPermintaanDetail;
use ReflectionClass;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "e_permintaan_detail".
 *
 *
 * @property EApprovalDetail $approvalByUser
 * @property EApprovalDetail[] $approvalsByUser
 * @property EApprovalDetail $rootApproval
 * @property EApprovalDetail[] $detailApprovals
 *
 */
class EPermintaanDetail extends BaseEPermintaanDetail
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['e_permintaan_id', 'e_stok_id'], 'required'],
            [['jumlah'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'last_status'], 'integer'],
            [['id', 'e_permintaan_id', 'e_stok_id', 'e_satuan_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate(): bool
    {
        if ($this->e_stok_id) {
            $this->e_satuan_id = $this->eStok->eBarang->e_satuan_id;
        }
        return parent::beforeValidate();
    }


    /**
     * @param string $attribute
     */
    public function existValidation(string $attribute): void
    {
        if ($this->isNewRecord) {
            $query = self::findOne(['e_stok_id' => $this->$attribute, 'e_permintaan_id' => $this->e_permintaan_id]);
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Barang {barang} ini telah ditambahkan, silahkan edit untuk memperbaharui', ['barang' => $this->eStok->eBarang->nama]));
            }
        }
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->setStatus(Status::OPEN);
        }
    }


    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $modelStatus = new EStatus();
        $modelStatus->status = $status;
        $modelStatus->relasi_id = $this->id;
        $modelStatus->table = (new ReflectionClass($this))->getName();
        if ($modelStatus->save()) {
            $this->setLastStatus($modelStatus->status);
        }
    }

    /**
     * @param int $status
     */
    public function setLastStatus(int $status): void
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        if ($model->save()) {
            if ($model->last_status == Status::CLOSED) {
                $model->updateStatusPermintaan();
            }
        }
    }

    /**
     *
     */
    public function updateStatusPermintaan(): void
    {
        if (!$this->ePermintaan->checkAllDetailAreClosed()) {
            $this->ePermintaan->setStatus(Status::CLOSED);
        }
    }


    /**
     * @return int
     */
    public function hitungQtyYgTelahDikeluarkan(): int
    {
        return (int)EBarangKeluarDetail::find()->andWhere(['e_permintaan_detail_id' => $this->id])->sum('jumlah');
    }

    /**
     * Melakukan aproval detail
     *
     * @param int $status
     * @param string $comment
     */
    public function approval(int $status, string $comment = ''): void
    {
        if ($modelApproval = $this->approvalByUser) {
            if ($modelApproval = $modelApproval->approval($status, $comment)) {
                if (in_array($modelApproval->tipe, [Status::APPROVE, Status::REJECT]) && $modelApproval->e_approval_detail_id == 0) {
                    //update status row
                    $approvalType = $modelApproval->tipe == Status::APPROVE ? Status::APPROVED : Status::REJECTED;
                    $modelApproval->ePermintaanDetail->setStatus($approvalType);

                    //update status row parent
                    $ePermintaan = $modelApproval->ePermintaanDetail->ePermintaan;
                    if (!$ePermintaan->checkStatusApprovalByUser()) {
                        if ($ePermintaan->checkStatusApprovalByUser(Status::APPROVE)) {
                            $ePermintaan->setStatus(Status::APPROVED);
                        } else {
                            $ePermintaan->setStatus(Status::REJECTED);
                        }
                    }
                }
            }
        }
    }

    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalByUser(): ActiveQuery
    {
        return $this->hasOne(EApprovalDetail::class, ['relasi_id' => 'id'])->andWhere(['e_approval_detail.pegawai_id' => User::pegawai()->id, 'table' => self::tableName()]);
    }


    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalsByUser(): ActiveQuery
    {
        return $this->hasMany(EApprovalDetail::class, ['relasi_id' => 'id'])->andWhere(['e_approval_detail.pegawai_id' => User::pegawai()->id, 'table' => self::tableName()]);
    }

    /**
     *
     * Relasi ke table approval -> mecari root dari sebuah approval
     *
     * @return ActiveQuery
     */
    public function getRootApproval(): ActiveQuery
    {
        return $this->hasOne(EApprovalDetail::class, ['e_approval_detail_id' => 'id'])->andWhere(['e_approval_detail.e_approval_detail_id' => 0]);
    }

    /**
     * Cek semua status status approval
     *
     * @param int $status default Status::REJECT
     *
     * @return bool true jika ada status yg sesuai dengan param, sebaliknya false
     */
    public function checkStatusApprovals(int $status = Status::REJECT): bool
    {
        $rejects = [];
        array_map(function (EApprovalDetail $x) use (&$rejects) {
            $rejects[] = $x->tipe;
        }, $this->detailApprovals);

        if (in_array($status, $rejects)) {
            return true;
        }
        return false;
    }


    /**
     * @return ActiveQuery
     */
    public function getDetailApprovals(): ActiveQuery
    {
        return $this->hasMany(EApprovalDetail::class, ['relasi_id' => 'id'])->andWhere(['e_approval_detail.table' => self::tableName()]);
    }


}

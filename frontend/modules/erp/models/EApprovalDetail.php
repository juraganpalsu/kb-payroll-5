<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use frontend\modules\erp\models\base\EApprovalDetail as BaseEApprovalDetail;
use frontend\modules\payroll\models\PayrollBoronganDetail;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "e_approval_detail".
 *
 * @property EApprovalDetail $detailApproval
 * @property EPermintaanDetail $ePermintaanDetail
 * @property PayrollBoronganDetail $payrollBoronganDetail
 *
 *
 */
class EApprovalDetail extends BaseEApprovalDetail
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['tipe', 'level', 'urutan', 'e_approval_detail_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['relasi_id'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['komentar'], 'string'],
            [['table'], 'string', 'max' => 225],
            [['relasi_id'], 'string', 'max' => 36],
            [['pegawai_struktur_id', 'perjanjian_kerja_id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return mixed
     */
    public function getApproval()
    {
        return Status::approvalStatus($this->tipe);
    }


    /**
     * Melakukan approval,
     * Jika yg melakukan approval mempunyai childs, maka semua childs yg belum melakukan approval akan di tandai SKIPPED
     * @param int $approval_type tipe approval
     * @param string $comment komentar jika detail ditolak
     * @return EApprovalDetail
     */
    public function approval(int $approval_type = Status::APPROVE, string $comment = ''): EApprovalDetail
    {
        $model = self::findOne($this->id);
        $model->tanggal = $model->tipe == Status::DEFLT ? date('Y-m-d H:i:s') : $model->tanggal;
        $model->tipe = $model->tipe == Status::DEFLT ? $approval_type : $model->tipe;
        $model->komentar = !empty($comment) && $model->tipe == Status::REJECT ? $comment : $model->komentar;
        if ($model->save()) {
            if ($parent = $model->eApprovalDetail) {
                if ($parent->tipe == Status::DEFLT && ($model->tipe == Status::REJECT || $model->tipe == Status::SKIP)) {
                    $model->eApprovalDetail->approval(Status::SKIP);
                }
            }
            if ($child = $model->detailApproval) {
                if ($child->tipe == Status::DEFLT) {
                    $model->detailApproval->approval(Status::SKIP);
                }
            }
        }
        return $model;
    }


    /**
     *
     * Relasi ke table EPermintaanDetail
     *
     * @return ActiveQuery
     *
     */
    public function getEPermintaanDetail(): ActiveQuery
    {
        return $this->hasOne(EPermintaanDetail::class, ['id' => 'relasi_id']);
    }

    /**
     *
     * Create Approval EPermintaan
     */
    public function createApprovalEPermintaan()
    {
        $ePermintaan = $this->ePermintaanDetail->ePermintaan;
        if (!$ePermintaan->checkStatusApprovalByUser()) {
            if ($ePermintaan->checkStatusApprovalByUser(Status::APPROVE)) {
                $ePermintaan->createApproval();
            }
        }
    }


    /**
     * Relasi has one untuk nested untuk mengambil anak2 nya
     *
     * @return ActiveQuery
     */
    public function getDetailApproval()
    {
        return $this->hasOne(EApprovalDetail::class, ['e_approval_detail_id' => 'id']);
    }


    /**
     *
     * Relasi ke table PayrollBoronganDetail
     *
     * @return ActiveQuery
     *
     */
    public function getPayrollBoronganDetail(): ActiveQuery
    {
        return $this->hasOne(PayrollBoronganDetail::class, ['id' => 'relasi_id']);
    }


    /**
     *
     * Create Approval EPermintaan
     */
    public function createApprovalPayrollBorongan()
    {
        $payrollBorongan = $this->payrollBoronganDetail->payrollBorongan;
        if (!$payrollBorongan->checkStatusApprovalByUser()) {
            if ($payrollBorongan->checkStatusApprovalByUser(Status::APPROVE)) {
                $payrollBorongan->createApproval();
            }
        }
    }

}
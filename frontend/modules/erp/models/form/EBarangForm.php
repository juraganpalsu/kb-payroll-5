<?php

namespace frontend\modules\erp\models\form;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class EBarangForm
 * @property string $upload_dir;
 * @property string $doc_file;
 * @property string $id;
 * @package frontend\modules\erp\models\form
 *
 */
class EBarangForm extends Model
{
    public $upload_dir = 'uploads/barang/';

    public $doc_file;

    public $id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1],
        ];
    }

    /**
     * @return array|array[]
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['upload'] = ['doc_file'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }

}
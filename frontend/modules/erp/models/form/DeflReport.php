<?php


namespace frontend\modules\erp\models\form;


use common\components\Status;
use frontend\modules\erp\models\EBarang;
use frontend\modules\erp\models\EBarangKeluarDetail;
use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Created by PhpStorm.
 * File Name: DeflReport.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-06-21
 * Time: 9:31 PM
 */

/**
 * Class DeflReport
 * @package frontend\modules\erp\models\form
 *
 *
 * @property string $tanggal
 * @property integer $kategori
 * @property string $gudangId
 *
 *
 */
class DeflReport extends Model
{

    public $tanggal;

    public $kategori;

    public $gudangId;


    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            [['tanggal', 'kategori', 'gudangId'], 'required'],
            [['kategori'], 'integer'],
            [['tanggal', 'gudangId'], 'string']
        ];
    }

    /**
     * @return EBarang
     */
    public function modelBarang()
    {
        return new EBarang();
    }

    /**
     *
     */
    public function formatingDeftReport()
    {
        $tanggalAwal = explode('s/d', $this->tanggal)[0];
        $tanggalAkhir = explode('s/d', $this->tanggal)[1];
        $gudangId = $this->gudangId;
        $queryBarangKeluarDetails = EBarangKeluarDetail::find()
            ->joinWith(['eBarangKeluar' => function (ActiveQuery $query) use ($tanggalAwal, $tanggalAkhir, $gudangId) {
                $query->andWhere(['BETWEEN', 'date(e_barang_keluar.created_at)', date('Y-m-d', strtotime($tanggalAwal)), date('Y-m-d', strtotime($tanggalAkhir))])
                    ->andWhere(['e_barang_keluar.last_status' => Status::FINISHED, 'e_barang_keluar.e_gudang_id' => $gudangId]);
            }])
            ->joinWith(['eStok' => function (ActiveQuery $query) {
                $query->joinWith(['eBarang' => function (ActiveQuery $query) {
                    $query->andWhere(['kategori' => $this->kategori]);
                }]);
            }])
            ->orderBy('e_barang_keluar.created_at ASC')
            ->all();
        $datas = [];
        /** @var EBarangKeluarDetail $barangKeluarDetail */
        foreach ($queryBarangKeluarDetails as $barangKeluarDetail) {
            $barangKeluar = $barangKeluarDetail->eBarangKeluar;
            $datas[date('Y-m-d', strtotime($barangKeluar->created_at))][$barangKeluar->id] = [
                'permintaanId' => $barangKeluar->ePermintaan->seqCode,
                'namaPeminta' => $barangKeluar->ePermintaan->pegawai->nama_,
                'yangMengeluarkan' => $barangKeluar->pegawai->nama_,
                'gudang' => $barangKeluar->eGudang->nama,
                'keterangan' => $barangKeluar->keterangan,
            ];
            $datas[date('Y-m-d', strtotime($barangKeluar->created_at))][$barangKeluar->id]['details'][$barangKeluarDetail->id] = [
                'barang' => $barangKeluarDetail->eStok->eBarang->nama,
                'jumlah' => $barangKeluarDetail->jumlah,
                'satuan' => $barangKeluarDetail->eSatuan->nama,
            ];
        }
        return $datas;
    }


}
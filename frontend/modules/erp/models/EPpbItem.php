<?php

namespace frontend\modules\erp\models;

use frontend\modules\erp\models\base\EPpbItem as BaseEPpbItem;
use Yii;

/**
 * This is the model class for table "e_ppb_item".
 */
class EPpbItem extends BaseEPpbItem
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['e_ppb_id', 'jumlah', 'e_stok_id'], 'required'],
            [['jumlah', 'sisa', 'buffer_stock', 'kebutuhan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['e_ppb_id', 'e_stok_id', 'e_satuan_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['e_stok_id', 'doubleValidation'],
//            ['jumlah', 'minimumStockValidation'],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->e_satuan_id = $this->eStok->eBarang->e_satuan_id;
        $this->sisa = $this->eStok->jumlah;
        return parent::beforeSave($insert);
    }

    /**
     * @param string $attribute
     */
    public function doubleValidation(string $attribute)
    {
        $check = self::findOne(['e_ppb_id' => $this->e_ppb_id, 'e_stok_id' => $this->e_stok_id]);
        if ($check) {
            $this->addError($attribute, Yii::t('frontend', '{barang} ini telah ada.', ['barang' => $this->eStok->eBarang->nama]));
        }
    }

    /**
     * @param string $attribute
     */
    public function minimumStockValidation(string $attribute)
    {
        if ($this->$attribute > $this->eStok->eBarang->minimum_stok) {
            $this->addError($attribute, Yii::t('frontend', '{barang} ini belum mencapai minimum stok', ['barang' => $this->eStok->eBarang->nama]));
        }
    }

}

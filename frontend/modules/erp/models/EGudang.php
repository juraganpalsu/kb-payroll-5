<?php

namespace frontend\modules\erp\models;

use frontend\modules\erp\models\base\EGudang as BaseEGudang;

/**
 * This is the model class for table "e_gudang".
 */
class EGudang extends BaseEGudang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 225],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return array
     */
    public function getPegawais(): array
    {
        $pegawaiIds = [];
        foreach ($this->eGudangPegawais as $eGudangPegawai) {
            $pegawaiIds[] = $eGudangPegawai->pegawai_id;
        }
        return $pegawaiIds;
    }

    /**
     * @param string $id
     * @return array
     */
    public static function getPegawaisStatic(string $id): array
    {
        $model = self::findOne($id);
        return $model->getPegawais();
    }

}

<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use frontend\modules\erp\models\base\EBarangMasukDetail as BaseEBarangMasukDetail;
use ReflectionClass;
use Yii;

/**
 * This is the model class for table "e_barang_masuk_detail".
 */
class EBarangMasukDetail extends BaseEBarangMasukDetail
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['e_stok_id', 'e_barang_masuk_id', 'jumlah', 'harga'], 'required'],
            [['jumlah', 'harga'], 'number'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_stok_id', 'e_barang_masuk_id', 'e_satuan_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock', 'last_status'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['e_stok_id', 'existValidation']
        ];
    }

    /**
     * @param string $attribute
     */
    public function existValidation(string $attribute)
    {
        if ($this->isNewRecord) {
            $query = self::findOne(['e_stok_id' => $this->$attribute, 'e_barang_masuk_id' => $this->e_barang_masuk_id]);
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Barang {barang} ini telah ditambahkan, silahkan edit untuk memperbaharui', ['barang' => $this->eStok->eBarang->nama]));
            }
        }
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->setStatus(Status::OPEN);
        }
    }


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($modelStok = EStok::findOne($this->e_stok_id)) {
            $this->e_satuan_id = $modelStok->eBarang->e_satuan_id;
        }
        return parent::beforeValidate();
    }


    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $modelStatus = new EStatus();
        $modelStatus->status = $status;
        $modelStatus->relasi_id = $this->id;
        $modelStatus->table = (new ReflectionClass($this))->getName();
        if ($modelStatus->save()) {
            $this->setLastStatus($modelStatus->status);
        }
    }

    /**
     * @param int $status
     */
    public function setLastStatus(int $status): void
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        $model->save();
    }

    /**
     *
     */
    public function updateStok(): void
    {
        $this->eStok->updateStok($this->jumlah);
        $this->createHistoryHarga();
    }


    /**
     *
     * Update harga setiap kali ada perubahan harga
     *
     */
    public function createHistoryHarga()
    {
        if ($modelBarang = EBarang::findOne($this->eStok->e_barang_id)) {
            if ($modelBarang->harga_terakhir != $this->harga) {
                $modelHarga = new EHarga();
                $modelHarga->e_barang_id = $this->eStok->e_barang_id;
                $modelHarga->harga = $this->harga;
                $modelHarga->save();
            }
        }
    }


    /**
     * @return float
     */
    public function hitungQtyYgTelahDikeluarkan(): float
    {
        return (float)EBarangKeluarDetail::find()->andWhere(['e_barang_masuk_detail_id' => $this->id])->sum('jumlah');
    }


}

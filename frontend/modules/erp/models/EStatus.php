<?php

namespace frontend\modules\erp\models;

use frontend\modules\erp\models\base\EStatus as BaseEStatus;

/**
 * This is the model class for table "e_status".
 */
class EStatus extends BaseEStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['relasi_id'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['status', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['table'], 'string', 'max' => 225],
            [['relasi_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

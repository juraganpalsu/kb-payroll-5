<?php

namespace frontend\modules\erp\models;

use frontend\modules\erp\models\base\EBarang as BaseEBarang;

/**
 * This is the model class for table "e_barang".
 * @property array $_kategori
 * @property string $kategori_
 */
class EBarang extends BaseEBarang
{
    const ATK = 1;
    const GENERAL = 2;

    public $_kategori = [self::ATK => 'ATK', self::GENERAL => 'GENERAL'];

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['nama', 'e_satuan_id', 'minimum_stok', 'maksimum_stok'], 'required'],
            [['harga_terakhir', 'maksimum_stok'], 'number'],
            [['kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'e_satuan_id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 225],
            [['gambar'], 'string', 'max' => 245],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock', 'minimum_stok'], 'default', 'value' => '0'],
            [['is_aktif'], 'default', 'value' => '1'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return string
     */
    public function getKategori_(): string
    {
        return $this->_kategori[$this->kategori];
    }


    /**
     * @param string $gudangId
     * @return EStok|null
     */
    public function getStok(string $gudangId)
    {
        return EStok::findOne(['e_barang_id' => $this->id, 'e_gudang_id' => $gudangId]);
    }

    /**
     * @param string $gudangId
     * @param string $q
     * @return array
     */
    public static function getBarangByGudangForSelect2(string $gudangId, string $q): array
    {
        $modelGudang = EGudang::findOne($gudangId);
        $data = [];
        if ($modelGudang) {
            $query = EStok::find()->select(['e_stok.id AS id', 'e_barang.nama AS text', 'e_stok.e_barang_id']);
            $query->joinWith('eBarang');
            $query->andWhere(['LIKE', 'e_barang.nama', $q]);
            $query->andWhere(['e_gudang_id' => $modelGudang->id]);
            $query->limit(20);
            $data = $query->asArray()->all();
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getFileExtension(): string
    {
        $explode = explode('.', $this->gambar);
        return strtolower((string)end($explode));
    }


}

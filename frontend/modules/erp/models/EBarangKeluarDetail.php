<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use frontend\modules\erp\models\base\EBarangKeluarDetail as BaseEBarangKeluarDetail;
use ReflectionClass;
use Yii;

/**
 * This is the model class for table "e_barang_keluar_detail".
 */
class EBarangKeluarDetail extends BaseEBarangKeluarDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_barang_keluar_id', 'e_satuan_id', 'e_stok_id', 'e_permintaan_detail_id', 'e_barang_masuk_detail_id'], 'required'],
            [['jumlah'], 'number'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'last_status'], 'integer'],
            [['id', 'e_barang_keluar_id', 'e_satuan_id', 'e_stok_id', 'e_permintaan_detail_id', 'e_barang_masuk_detail_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['e_barang_masuk_detail_id', 'validasiSisaPerBarangMasuk'],
            ['jumlah', 'validasiJumlahBerdasarkanPermintaan'],
            ['jumlah', 'validasiSisaStok'],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validasiSisaPerBarangMasuk(string $attribute): void
    {
        if ($this->isNewRecord) {
            $modelBarangMasukDetail = EBarangMasukDetail::findOne($this->$attribute);
            $jumlahYgSudahKeluar = $modelBarangMasukDetail->hitungQtyYgTelahDikeluarkan();
            if (($jumlahYgSudahKeluar + $this->jumlah) > $modelBarangMasukDetail->jumlah) {
                $this->addError($attribute, Yii::t('frontend', 'Jumlah barang masuk tidak mencukupi'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function validasiSisaStok(string $attribute): void
    {
        if ($this->isNewRecord) {
            if ($this->jumlah > $this->eStok->jumlah) {
                $this->addError($attribute, Yii::t('frontend', 'Stok mencukupi'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function validasiJumlahBerdasarkanPermintaan(string $attribute): void
    {
        if ($this->isNewRecord) {
            if ($this->ePermintaanDetail->jumlah < ($this->hitungQtyYgTelahDikeluarkanBerdasarPermintaan() + $this->$attribute)) {
                $this->addError($attribute, Yii::t('frontend', 'Jumlah yg dikeluarkan lebih banyak dari yg diminta'));
            }
        }
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->setStatus(Status::OPEN);
        }
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $modelStatus = new EStatus();
        $modelStatus->status = $status;
        $modelStatus->relasi_id = $this->id;
        $modelStatus->table = (new ReflectionClass($this))->getName();
        if ($modelStatus->save()) {
            $this->setLastStatus($modelStatus->status);
        }
    }

    /**
     * @param int $status
     */
    public function setLastStatus(int $status)
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        $model->save();
        if ($model->last_status == Status::FINISHED) {
            $model->eStok->updateStok($model->jumlah, false);
            $model->updateStatusPermintaan();
        }
    }

    public function updateStatusPermintaan()
    {
        if ($this->ePermintaanDetail->jumlah == $this->hitungQtyYgTelahDikeluarkanBerdasarPermintaan()) {
            $this->ePermintaanDetail->setStatus(Status::CLOSED);
        }
    }


    /**
     * @return int
     */
    public function hitungQtyYgTelahDikeluarkanBerdasarPermintaan(): int
    {
        return (int)EBarangKeluarDetail::find()->andWhere(['e_permintaan_detail_id' => $this->e_permintaan_detail_id])->sum('jumlah');
    }
}

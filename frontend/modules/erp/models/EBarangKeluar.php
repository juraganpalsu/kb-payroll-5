<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use frontend\models\User;
use frontend\modules\erp\models\base\EBarangKeluar as BaseEBarangKeluar;
use ReflectionClass;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "e_barang_keluar".
 *
 * @property string $code
 *
 */
class EBarangKeluar extends BaseEBarangKeluar
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['e_permintaan_id', 'e_gudang_id'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'last_status'], 'integer'],
            [['id', 'e_permintaan_id', 'e_gudang_id'], 'string', 'max' => 36],
            [['pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $modelPegawai = User::pegawai();
        if ($modelPegawai) {
            $this->pegawai_id = $modelPegawai->id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->setStatus(Status::OPEN);
        }
    }


    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $modelStatus = new EStatus();
        $modelStatus->status = $status;
        $modelStatus->relasi_id = $this->id;
        $modelStatus->table = (new ReflectionClass($this))->getName();
        if ($modelStatus->save()) {
            $this->setLastStatus($modelStatus->status);
        }
    }

    /**
     * @param int $status
     */
    public function setLastStatus(int $status)
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        $model->save();
    }


    /**
     * @return string
     */
    public function getCode(): string
    {
        return explode('-', $this->id)[0];
    }

    /**
     * @param string $eStokId
     * @return ActiveQuery
     */
    public function loadBarangMasukDetail(string $eStokId): ActiveQuery
    {
        return EBarangMasukDetail::find()
            ->andWhere('e_barang_masuk_detail.jumlah > (IFNULL((SELECT sum(e_barang_keluar_detail.jumlah) FROM e_barang_keluar_detail WHERE e_barang_keluar_detail.e_barang_masuk_detail_id = e_barang_masuk_detail.id AND e_barang_keluar_detail.deleted_by = 0), 0))')
            ->andWhere(['e_barang_masuk_detail.e_stok_id' => $eStokId]);
    }

    /**
     * @param string $permintaanDetailId
     * @return bool
     */
    public function cekPermintaanTelahDimasukkan(string $permintaanDetailId): bool
    {
        return EBarangKeluarDetail::find()->andWhere(['e_permintaan_detail_id' => $permintaanDetailId, 'e_barang_keluar_id' => $this->id])->exists();
    }


    /**
     *
     */
    public function submit()
    {
        $this->setStatus(Status::FINISHED);
        $this->setStatusDetail(Status::FINISHED);
    }


    /**
     * @param int $status
     */
    public function setStatusDetail(int $status)
    {
        foreach ($this->eBarangKeluarDetails as $detail) {
            $detail->setStatus($status);
        }
    }
}

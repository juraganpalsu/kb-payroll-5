<?php

namespace frontend\modules\erp\models\base;

use common\components\CreatedByBehavior;
use common\components\UUIDBehavior;
use frontend\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "{{%e_permintaan_detail}}".
 *
 * @property string $id
 * @property string $jumlah
 * @property string $e_permintaan_id
 * @property string $e_stok_id
 * @property string $e_satuan_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 * @property integer $last_status
 *
 * @property \frontend\modules\erp\models\EBarangKeluarDetail[] $eBarangKeluarDetails
 * @property \frontend\modules\erp\models\EPermintaan $ePermintaan
 * @property \frontend\modules\erp\models\EStok $eStok
 */
class EPermintaanDetail extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'e_permintaan_id', 'e_stok_id'], 'required'],
            [['jumlah'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_permintaan_id', 'e_stok_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'e_permintaan_detail';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'jumlah' => Yii::t('frontend', 'Jumlah'),
            'e_permintaan_id' => Yii::t('frontend', 'Permintaan ID'),
            'e_stok_id' => Yii::t('frontend', 'Stok ID'),
            'e_satuan_id' => Yii::t('frontend', 'Satuan ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEBarangKeluarDetails()
    {
        return $this->hasMany(\frontend\modules\erp\models\EBarangKeluarDetail::class, ['e_permintaan_detail_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEPermintaan()
    {
        return $this->hasOne(\frontend\modules\erp\models\EPermintaan::class, ['id' => 'e_permintaan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEStok()
    {
        return $this->hasOne(\frontend\modules\erp\models\EStok::class, ['id' => 'e_stok_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getESatuan()
    {
        return $this->hasOne(\frontend\modules\erp\models\ESatuan::class, ['id' => 'e_satuan_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete(): bool
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}


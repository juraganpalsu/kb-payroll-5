<?php

namespace frontend\modules\erp\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "e_approval_detail".
 *
 * @property integer $id
 * @property integer $tipe
 * @property string $tanggal
 * @property string $komentar
 * @property integer $level
 * @property integer $urutan
 * @property string $table
 * @property string $relasi_id
 * @property integer $e_approval_detail_id
 * @property string $pegawai_struktur_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\erp\models\EApprovalDetail $eApprovalDetail
 * @property \frontend\modules\erp\models\EApprovalDetail[] $eApprovalDetails
 * @property Pegawai $pegawai
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 */
class EApprovalDetail extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipe', 'level', 'urutan', 'e_approval_detail_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal', 'relasi_id', 'pegawai_struktur_id', 'perjanjian_kerja_id', 'pegawai_id'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['komentar'], 'string'],
            [['table'], 'string', 'max' => 225],
            [['relasi_id'], 'string', 'max' => 36],
            [['pegawai_struktur_id', 'perjanjian_kerja_id', 'pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'e_approval_detail';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'tipe' => Yii::t('frontend', 'Tipe'),
            'tanggal' => Yii::t('frontend', 'Tanggal'),
            'komentar' => Yii::t('frontend', 'Komentar'),
            'level' => Yii::t('frontend', 'Level'),
            'urutan' => Yii::t('frontend', 'Urutan'),
            'table' => Yii::t('frontend', 'Table'),
            'relasi_id' => Yii::t('frontend', 'Relasi ID'),
            'e_approval_detail_id' => Yii::t('frontend', 'E Approval Detail ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }
    
    /**
     * @return ActiveQuery
     */
    public function getEApprovalDetail(): ActiveQuery
    {
        return $this->hasOne(\frontend\modules\erp\models\EApprovalDetail::class, ['id' => 'e_approval_detail_id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getEApprovalDetails(): ActiveQuery
    {
        return $this->hasMany(\frontend\modules\erp\models\EApprovalDetail::class, ['e_approval_detail_id' => 'id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPegawai(): ActiveQuery
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur(): ActiveQuery
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }
        
    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja(): ActiveQuery
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete(): bool
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}
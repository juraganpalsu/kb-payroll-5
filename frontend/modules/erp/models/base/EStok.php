<?php

namespace frontend\modules\erp\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use common\components\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "{{%e_stok}}".
 *
 * @property string $id
 * @property string $jumlah
 * @property string $e_barang_id
 * @property string $e_gudang_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\erp\models\EBarangKeluarDetail[] $eBarangKeluarDetails
 * @property \frontend\modules\erp\models\EBarangMasukDetail[] $eBarangMasukDetails
 * @property \frontend\modules\erp\models\EPermintaanDetail[] $ePermintaanDetails
 * @property \frontend\modules\erp\models\EBarang $eBarang
 * @property \frontend\modules\erp\models\EGudang $eGudang
 * @property \frontend\modules\erp\models\EStokPergerakan[] $eStokPergerakans
 */
class EStok extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jumlah', 'e_barang_id', 'e_gudang_id'], 'required'],
            [['jumlah'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_barang_id', 'e_gudang_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%e_stok}}';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'jumlah' => Yii::t('frontend', 'Jumlah'),
            'e_barang_id' => Yii::t('frontend', 'Barang ID'),
            'e_gudang_id' => Yii::t('frontend', 'Gudang ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEBarangKeluarDetails()
    {
        return $this->hasMany(\frontend\modules\erp\models\EBarangKeluarDetail::class, ['e_stok_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEBarangMasukDetails()
    {
        return $this->hasMany(\frontend\modules\erp\models\EBarangMasukDetail::class, ['e_stok_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEPermintaanDetails()
    {
        return $this->hasMany(\frontend\modules\erp\models\EPermintaanDetail::class, ['e_stok_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEBarang()
    {
        return $this->hasOne(\frontend\modules\erp\models\EBarang::class, ['id' => 'e_barang_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEGudang()
    {
        return $this->hasOne(\frontend\modules\erp\models\EGudang::class, ['id' => 'e_gudang_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEStokPergerakans()
    {
        return $this->hasMany(\frontend\modules\erp\models\EStokPergerakan::class, ['e_stok_id' => 'id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete(): bool
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

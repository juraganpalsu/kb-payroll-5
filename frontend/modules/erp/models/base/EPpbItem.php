<?php

namespace frontend\modules\erp\models\base;

use common\components\CreatedByBehavior;
use common\components\GenerateSeqCode;
use common\components\UUIDBehavior;
use frontend\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "e_ppb_item".
 *
 * @property string $id
 * @property string $e_ppb_id
 * @property integer $jumlah
 * @property integer $sisa
 * @property integer $buffer_stock
 * @property integer $kebutuhan
 * @property string $keterangan
 * @property string $e_stok_id
 * @property string $e_satuan_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\erp\models\EPpb $ePpb
 * @property \frontend\modules\erp\models\ESatuan $eSatuan
 * @property \frontend\modules\erp\models\EStok $eStok
 */
class EPpbItem extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['e_ppb_id', 'jumlah', 'e_stok_id', 'e_satuan_id'], 'required'],
            [['jumlah', 'sisa', 'buffer_stock', 'kebutuhan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['e_ppb_id', 'e_stok_id', 'e_satuan_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'e_ppb_item';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock(): string
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'e_ppb_id' => Yii::t('frontend', 'E Ppb ID'),
            'jumlah' => Yii::t('frontend', 'Jumlah'),
            'sisa' => Yii::t('frontend', 'Sisa'),
            'buffer_stock' => Yii::t('frontend', 'Buffer Stock'),
            'kebutuhan' => Yii::t('frontend', 'Kebutuhan'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'e_stok_id' => Yii::t('frontend', 'E Stok ID'),
            'e_satuan_id' => Yii::t('frontend', 'E Satuan ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEPpb(): ActiveQuery
    {
        return $this->hasOne(\frontend\modules\erp\models\EPpb::class, ['id' => 'e_ppb_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getESatuan(): ActiveQuery
    {
        return $this->hasOne(\frontend\modules\erp\models\ESatuan::class, ['id' => 'e_satuan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEStok(): ActiveQuery
    {
        return $this->hasOne(\frontend\modules\erp\models\EStok::class, ['id' => 'e_stok_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @return bool
     */
    public function beforeSoftDelete(): bool
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ]
        ];
    }
}
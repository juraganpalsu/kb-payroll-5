<?php

namespace frontend\modules\erp\models\base;

use common\components\CreatedByBehavior;
use common\components\UUIDBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "{{%e_barang_keluar}}".
 *
 * @property string $id
 * @property string $keterangan
 * @property string $e_permintaan_id
 * @property string $e_gudang_id
 * @property string $pegawai_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 * @property integer $last_status
 *
 * @property \frontend\modules\erp\models\EGudang $eGudang
 * @property \frontend\modules\erp\models\EPermintaan $ePermintaan
 * @property Pegawai $pegawai
 * @property \frontend\modules\erp\models\EBarangKeluarDetail[] $eBarangKeluarDetails
 * @property User $createdBy
 */
class EBarangKeluar extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'e_permintaan_id', 'e_gudang_id', 'pegawai_id'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'e_permintaan_id', 'e_gudang_id'], 'string', 'max' => 36],
            [['pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%e_barang_keluar}}';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock(): string
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'e_permintaan_id' => Yii::t('frontend', 'Permintaan ID'),
            'seq_code' => Yii::t('frontend', 'Permintaan ID'),
            'e_gudang_id' => Yii::t('frontend', 'Gudang ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEGudang(): ActiveQuery
    {
        return $this->hasOne(\frontend\modules\erp\models\EGudang::class, ['id' => 'e_gudang_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEPermintaan(): ActiveQuery
    {
        return $this->hasOne(\frontend\modules\erp\models\EPermintaan::class, ['id' => 'e_permintaan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai(): ActiveQuery
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEBarangKeluarDetails(): ActiveQuery
    {
        return $this->hasMany(\frontend\modules\erp\models\EBarangKeluarDetail::class, ['e_barang_keluar_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete(): bool
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace frontend\modules\erp\models\base;

use common\components\CreatedByBehavior;
use common\components\UUIDBehavior;
use frontend\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "{{%e_barang}}".
 *
 * @property string $id
 * @property string $nama
 * @property string $harga_terakhir
 * @property string $e_satuan_id
 * @property integer $kategori
 * @property integer $is_aktif
 * @property string $gambar
 * @property integer $minimum_stok
 * @property integer $maksimum_stok
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\erp\models\ESatuan $eSatuan
 * @property \frontend\modules\erp\models\EHarga[] $eHargas
 * @property \frontend\modules\erp\models\EStok[] $eStoks
 * @property User $createdBy
 */
class EBarang extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama', 'e_satuan_id'], 'required'],
            [['harga_terakhir', 'maksimum_stok'], 'number'],
            [['kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'e_satuan_id'], 'string', 'max' => 36],
            [['nama', 'gambar'], 'string', 'max' => 225],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock', 'minimum_stok'], 'default', 'value' => '0'],
            [['is_aktif'], 'default', 'value' => '1'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'e_barang';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'nama' => Yii::t('frontend', 'Nama'),
            'harga_terakhir' => Yii::t('frontend', 'Harga Terakhir'),
            'e_satuan_id' => Yii::t('frontend', 'Satuan ID'),
            'kategori' => Yii::t('frontend', 'Kategori'),
            'is_aktif' => Yii::t('frontend', 'Status Aktif'),
            'gambar' => Yii::t('frontend', 'Gambar'),
            'minimum_stok' => Yii::t('frontend', 'Minimum Stok'),
            'maksimum_stok' => Yii::t('frontend', 'Maksimum Stok'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getESatuan()
    {
        return $this->hasOne(ESatuan::class, ['id' => 'e_satuan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEHargas()
    {
        return $this->hasMany(EHarga::class, ['e_barang_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEStoks()
    {
        return $this->hasMany(EStok::class, ['e_barang_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

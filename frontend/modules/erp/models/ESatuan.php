<?php

namespace frontend\modules\erp\models;

use frontend\modules\erp\models\base\ESatuan as BaseESatuan;

/**
 * This is the model class for table "e_satuan".
 */
class ESatuan extends BaseESatuan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 225],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

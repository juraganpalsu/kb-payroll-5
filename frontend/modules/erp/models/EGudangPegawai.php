<?php

namespace frontend\modules\erp\models;

use common\components\Status;
use frontend\modules\erp\models\base\EGudangPegawai as BaseEGudangPegawai;

/**
 * This is the model class for table "e_gudang_pegawai".
 */
class EGudangPegawai extends BaseEGudangPegawai
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_gudang_id', 'pegawai_id'], 'required'],
            [['status', 'notifikasi', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'e_gudang_id'], 'string', 'max' => 36],
            [['pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'notifikasi'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return bool
     */
    public function setNotifikasi(): bool
    {
        $model = self::findOne($this->id);
        $model->notifikasi = $model->notifikasi ? Status::TIDAK : Status::YA;
        return $model->save();
    }

}

<?php

namespace frontend\modules\erp\models;

use Yii;
use \frontend\modules\erp\models\base\ESettingApprove as BaseESettingApprove;

/**
 * This is the model class for table "e_setting_approve".
 */
class ESettingApprove extends BaseESettingApprove
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'pegawai_id', 'atasan_pegawai_id'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 36],
            [['pegawai_id', 'atasan_pegawai_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}

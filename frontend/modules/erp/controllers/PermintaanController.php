<?php

namespace frontend\modules\erp\controllers;

use common\components\Status;
use frontend\modules\erp\models\EPermintaan;
use frontend\modules\erp\models\EPermintaanDetail;
use frontend\modules\erp\models\search\EPermintaanSearch;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * EPermintaanController implements the CRUD actions for EPermintaan model.
 */
class PermintaanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EPermintaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EPermintaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EPermintaan model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);
        $providerEPermintaanDetail = new ActiveDataProvider([
            'query' => EPermintaanDetail::find()->andWhere(['e_permintaan_id' => $model->id]),
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerEPermintaanDetail' => $providerEPermintaanDetail,
        ]);
    }

    /**
     * Creates a new EPermintaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EPermintaan();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EPermintaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new EPermintaan();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * Deletes an existing EPermintaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the EPermintaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EPermintaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id)
    {
        if (($model = EPermintaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }


    /**
     * @param string $id
     * @return EPermintaanDetail
     * @throws NotFoundHttpException
     */
    protected function findModelDetail(string $id): EPermintaanDetail
    {
        if (($model = EPermintaanDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $idpermintaan
     * @return array|mixed|string
     */
    public function actionCreateDetail(string $idpermintaan)
    {
        $model = new EPermintaanDetail();
        $model->e_permintaan_id = $idpermintaan;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->e_permintaan_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-detail', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateDetail(string $id)
    {
        $model = $this->findModelDetail($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->e_permintaan_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-detail', [
            'model' => $model,
        ]);
    }


    /**
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouDetailValidation(bool $isnew)
    {
        $model = new EPermintaanDetail();
        $model->isNewRecord = $isnew;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteDetail(string $id): array
    {
        $model = $this->findModelDetail($id);
        $ePermintaanId = $model->e_permintaan_id;
        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $ePermintaanId])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];

        }
        return $response->data;
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSubmit(string $id): array
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->submit();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
        }
        return $response->data;
    }


    /**
     * Lists all EPermintaan models.
     * @return mixed
     */
    public function actionIndexDetail()
    {
        $searchModel = new EPermintaanSearch();
        $dataProvider = $searchModel->searchDetail(Yii::$app->request->queryParams);

        return $this->render('index-detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @return string
     */
    public function actionApproval()
    {
        $searchModel = new EPermintaanSearch();
        $dataProvider = $searchModel->searchApproval();

        return $this->render('approval', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionApprovalDetail(string $id): string
    {
        $searchModel = new EPermintaanSearch();
        $model = $this->findModel($id);
        $dataProvider = $searchModel->searchApprovalDetail($id);

        return $this->renderAjax('approval-detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }


    /**
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionApproveDetail(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan approval.'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                $modelDetail = $this->findModelDetail($id);
                $modelDetail->approval(Status::APPROVE);
                $modelParent = $this->findModel($modelDetail->e_permintaan_id);

                $lastStatus = $modelParent->last_status;
                $isdone = in_array($lastStatus, [Status::REJECTED, Status::APPROVED]);
                //Jika telah selesai persetujuannya
                if ($isdone) {
                    //update approval ke status terakhir, ini dieksekusi ketika satu row telah selesai melakukan approval
                    $modelParent->createApproval();
                }
                $response->data = [
                    'data' => ['isdone' => $isdone],
                    'message' => Yii::t('frontend', 'Berhasil melakukan approval.'),
                    'status' => true,
                ];
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * Aksi melakukan reject
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionRejectDetail(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan reject .'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                $modelDetail = $this->findModelDetail($id);
                $modelDetail->approval(Status::REJECT, $request->post('keterangan'));
                //panggil ulang model untuk mendapatkan data terakhir, setelah di update
                $modelDetail = $this->findModelDetail($id);
                $permintaan = $modelDetail->ePermintaan;
                $lastStatus = $permintaan->last_status;
                $isdone = in_array($lastStatus, [Status::REJECTED, Status::APPROVED]);
                //Jika telah selesai persetujuannya
                if ($isdone) {
                    //update permintaan approval ke status terakhir, ini dieksekusi ketika satu permintaan telah selesai melakukan approval
                    $permintaan->createApproval();
                }
                $response->data = [
                    'data' => ['isdone' => $isdone],
                    'message' => Yii::t('frontend', 'Berhasil melakukan approval(REJECT).'),
                    'status' => true,
                ];
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * Approve semua product
     *
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionApproveAll(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan approve.'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                if ($model = $this->findModel($id)) {
                    $model->approval(Status::APPROVE);
                    $model = $this->findModel($id);
                    $model->createApproval();
                    $response->data = [
                        'data' => [],
                        'message' => Yii::t('frontend', 'Berhasil melakukan approve.'),
                        'status' => true,
                    ];
                }
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }

    /**
     * Reject semua product
     *
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionRejectAll(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan approval .'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                if ($model = $this->findModel($id)) {
                    $model->approval(Status::REJECT, $request->post('keterangan'));
                    $model = $this->findModel($id);
                    $model->createApproval($request->post('keterangan'));
                    $response->data = [
                        'data' => [],
                        'message' => Yii::t('frontend', 'Berhasil melakukan approval .'),
                        'status' => true,
                    ];
                }
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }


    /**
     * @param bool $action
     * @return string
     */
    public function actionShowUserApproval(bool $action = true): string
    {
        if (isset($_POST['expandRowKey'])) {
            try {
                $model = $this->findModel($_POST['expandRowKey']);
                $modelApprovals = $model->approvals;
                return $this->renderAjax('_user-approval', [
                    'modelApprovals' => $modelApprovals,
                    'action' => $action
                ]);
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        return '<div class="alert alert-danger">' . Yii::t('frontend', 'No data found') . '</div>';
    }

    /**
     * @param bool $action
     * @return string
     */
    public function actionShowUserApprovalDetail(bool $action = true): string
    {
        if (isset($_POST['expandRowKey'])) {
            try {
                $model = $this->findModelDetail($_POST['expandRowKey']);
                $modelApprovals = $model->detailApprovals;
                return $this->renderAjax('_user-approval-detail', [
                    'modelApprovals' => $modelApprovals,
                    'action' => $action
                ]);
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
        return '<div class="alert alert-danger">' . Yii::t('frontend', 'No data found') . '</div>';
    }

    /**
     *
     * Melakukan Force Close pada permintaan
     *
     * @param string $id
     * @return array|mixed
     * @throws NotAcceptableHttpException
     */
    public function actionForceClose(string $id)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'data' => '',
            'message' => Yii::t('frontend', 'Gagal melakukan Force Close.'),
            'status' => false,
        ];
        if ($request->isAjax && $request->isPost) {
            try {
                if ($model = $this->findModel($id)) {
                    $model->setStatus(Status::CLOSED);
                    $model->setStatusDetail(Status::CLOSED);
                    $response->data = [
                        'data' => [],
                        'message' => Yii::t('frontend', 'Berhasil melakukan Force Close.'),
                        'status' => true,
                    ];
                }
            } catch (NotFoundHttpException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            return $response->data;
        }
        throw new NotAcceptableHttpException(Yii::t('frontend', 'The requested not acceptable.'));
    }
}

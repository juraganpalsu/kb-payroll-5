<?php

namespace frontend\modules\erp\controllers;

use frontend\modules\erp\models\EPpb;
use frontend\modules\erp\models\EPpbItem;
use frontend\modules\erp\models\EStok;
use frontend\modules\erp\models\search\EStokSearch;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * EStokController implements the CRUD actions for EStok model.
 */
class StokController extends Controller
{
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EStok models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EStokSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EStok model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);
        $providerEBarangKeluarDetail = new ArrayDataProvider([
            'allModels' => $model->eBarangKeluarDetails,
        ]);
        $providerEBarangMasukDetail = new ArrayDataProvider([
            'allModels' => $model->eBarangMasukDetails,
        ]);
        $providerEPermintaanDetail = new ArrayDataProvider([
            'allModels' => $model->ePermintaanDetails,
        ]);
        $providerEStokPergerakan = new ArrayDataProvider([
            'allModels' => $model->eStokPergerakans,
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerEBarangKeluarDetail' => $providerEBarangKeluarDetail,
            'providerEBarangMasukDetail' => $providerEBarangMasukDetail,
            'providerEPermintaanDetail' => $providerEPermintaanDetail,
            'providerEStokPergerakan' => $providerEStokPergerakan,
        ]);
    }


    /**
     * Finds the EStok model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EStok the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id): EStok
    {
        if (($model = EStok::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return EPpb|null
     * @throws NotFoundHttpException
     */
    protected function findModelEpp(string $id)
    {
        if (($model = EPpb::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @return array|mixed|string
     */
    public function actionPermintaanPembelian()
    {
        $model = new EPpb();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view-pp', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('permintaan-pembelian', [
            'model' => $model,
        ]);

    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePp(string $id)
    {
        $model = $this->findModelEpp($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view-pp', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('permintaan-pembelian', [
            'model' => $model,
        ]);

    }


    /**
     *
     * validasi permintaan pembelian
     *
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouPpValidation()
    {
        $model = new EPpb();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return string
     */
    public function actionIndexPp()
    {
        $searchModel = new EPpb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-pp', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewPp(string $id)
    {
        $model = $this->findModelEpp($id);
        $providerEPpbItem = new ActiveDataProvider([
            'query' => EPpbItem::find()->andWhere(['e_ppb_id' => $model->id]),
        ]);
        return $this->render('view-pp', [
            'model' => $model,
            'providerEPpbItem' => $providerEPpbItem,
        ]);
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function actionDeletePp(string $id): array
    {
        $model = $this->findModelEpp($id);
        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['view-pp', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
        }
        return $response->data;
    }


    /**
     * @param string $id
     * @return EPpbItem
     * @throws NotFoundHttpException
     */
    protected function findModelDetail(string $id): EPpbItem
    {
        if (($model = EPpbItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $idpp
     * @return array|mixed|string
     */
    public function actionPpItem(string $idpp)
    {
        $model = new EPpbItem();
        $model->e_ppb_id = $idpp;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view-pp', 'id' => $model->e_ppb_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-pp', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUpdatePpItem(string $id)
    {
        $model = $this->findModelDetail($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view-pp', 'id' => $model->e_ppb_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('_form-pp', [
            'model' => $model,
        ]);
    }


    /**
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouPpItemValidation(bool $isnew)
    {
        $model = new EPpbItem();
        $model->isNewRecord = $isnew;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function actionDeleteDetail(string $id): array
    {
        $model = $this->findModelDetail($id);
        $ePermintaanId = $model->e_ppb_id;
        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $ePermintaanId])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
        }
        return $response->data;
    }


    /**
     * @param string $id
     * @return mixed
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws NotFoundHttpException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function actionPrintPp(string $id)
    {
        $model = $this->findModelEpp($id);
        $content = $this->renderAjax('_print-pp', ['model' => $model]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@frontend/web/css/laporanHarian.css',
            'cssInline' => 'table{font-size:10px}',
            'options' => ['title' => Yii::$app->name],
            'marginTop' => '5',
            'marginBottom' => '5',
            'marginLeft' => '2',
            'marginRight' => '2',
            'methods' => [
                'SetFooter' => [Yii::$app->formatter->asDatetime('now') . '|Page {PAGENO}|' . $model->tanggal],
            ]
        ]);
//        return $content;

        return $pdf->render();
    }

    /**
     * @param string $idpp
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionLoadMinimumStok(string $idpp)
    {
        $model = $this->findModelEpp($idpp);
//        print_r($model->attributes);
        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->loadMinimumStok();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
        }
        return $response->data;

    }


}

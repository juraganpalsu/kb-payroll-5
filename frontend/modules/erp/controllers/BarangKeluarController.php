<?php

namespace frontend\modules\erp\controllers;

use frontend\modules\erp\models\EBarangKeluar;
use frontend\modules\erp\models\EBarangKeluarDetail;
use frontend\modules\erp\models\EBarangMasukDetail;
use frontend\modules\erp\models\EPermintaan;
use frontend\modules\erp\models\EPermintaanDetail;
use frontend\modules\erp\models\form\DeflReport;
use frontend\modules\erp\models\search\EBarangKeluarSearch;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * EBarangKeluarController implements the CRUD actions for EBarangKeluar model.
 */
class BarangKeluarController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EBarangKeluar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EBarangKeluarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EBarangKeluar model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);
        $providerEBarangKeluarDetail = new ActiveDataProvider([
            'query' => EBarangKeluarDetail::find()->andWhere(['e_barang_keluar_id' => $model->id]),
        ]);
        $providerEPermintaanDetail = new ActiveDataProvider([
            'query' => EPermintaanDetail::find()->andWhere(['e_permintaan_id' => $model->e_permintaan_id]),
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerEBarangKeluarDetail' => $providerEBarangKeluarDetail,
            'providerEPermintaanDetail' => $providerEPermintaanDetail,
        ]);
    }

    /**
     * Creates a new EBarangKeluar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $permintaanid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate(string $permintaanid)
    {
        $model = new EBarangKeluar();
        $modelPermintaan = $this->findModelPermintaan($permintaanid);
        $model->e_permintaan_id = $modelPermintaan->id;
        $model->e_gudang_id = $modelPermintaan->e_gudang_id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EBarangKeluar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation(): array
    {
        $model = new EBarangKeluar();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDelete(string $id): Response
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the EBarangKeluar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EBarangKeluar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id): EBarangKeluar
    {
        if (($model = EBarangKeluar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return EPermintaan|null
     * @throws NotFoundHttpException
     */
    protected function findModelPermintaan(string $id): EPermintaan
    {
        if (($model = EPermintaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return EPermintaanDetail|null
     * @throws NotFoundHttpException
     */
    protected function findModelPermintaanDetail(string $id): EPermintaanDetail
    {
        if (($model = EPermintaanDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return EBarangMasukDetail|null
     * @throws NotFoundHttpException
     */
    protected function findModelBarangMasukDetail(string $id): EBarangMasukDetail
    {
        if (($model = EBarangMasukDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return EBarangKeluarDetail|null
     * @throws NotFoundHttpException
     */
    protected function findModelDetail(string $id): EBarangKeluarDetail
    {
        if (($model = EBarangKeluarDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }


    /**
     * @param string $permintaandetailid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionLoadBarangMasuk(string $permintaandetailid, string $barangkeluarid): string
    {
        $model = new EBarangKeluar();

        $modelPermintaanDetail = $this->findModelPermintaanDetail($permintaandetailid);
        $providerEBarangMasukDetail = new ActiveDataProvider([
            'query' => $model->loadBarangMasukDetail($modelPermintaanDetail->e_stok_id),
        ]);
        return $this->renderAjax('_load-barang-masuk', [
            'providerEBarangMasukDetail' => $providerEBarangMasukDetail,
            'barangkeluarid' => $barangkeluarid,
            'permintaandetailid' => $modelPermintaanDetail->id
        ]);
    }

    /**
     * @param string $id adalah id barang masuk detail
     * @param string $barangkeluarid
     * @param string $permintaandetailid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCreateDetail(string $id, string $barangkeluarid, string $permintaandetailid)
    {
        $model = new EBarangKeluarDetail();
        $model->e_barang_keluar_id = $barangkeluarid;
        $model->e_permintaan_detail_id = $permintaandetailid;
        $modelBarangMasukDetail = $this->findModelBarangMasukDetail($id);
        $model->e_barang_masuk_detail_id = $modelBarangMasukDetail->id;
        $model->e_stok_id = $modelBarangMasukDetail->e_stok_id;
        $model->e_satuan_id = $modelBarangMasukDetail->e_satuan_id;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->e_barang_keluar_id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        $jumlahbarangMasuk = $modelBarangMasukDetail->jumlah;
        $jumlahPermintaan = $model->ePermintaanDetail->jumlah;
        $model->jumlah = $jumlahbarangMasuk - $modelBarangMasukDetail->hitungQtyYgTelahDikeluarkan();
        if ($jumlahbarangMasuk > $jumlahPermintaan) {
            $model->jumlah = $jumlahPermintaan - $model->ePermintaanDetail->hitungQtyYgTelahDikeluarkan();
        }


        return $this->renderAjax('_form-detail', [
            'model' => $model,
            'modelBarangMasukDetail' => $modelBarangMasukDetail,
        ]);
    }


    /**
     * @param bool $isnew
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouDetailValidation(bool $isnew)
    {
        $model = new EBarangKeluarDetail();
        $model->isNewRecord = $isnew;
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteDetail(string $id): array
    {
        $model = $this->findModelDetail($id);

        $barangKeluarId = $model->e_barang_keluar_id;
        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->delete();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $barangKeluarId])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];

        }
        return $response->data;
    }


    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSubmit(string $id): array
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->submit();
            $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
        }
        return $response->data;
    }

    /**
     * Lists all EBarangMasuk models.
     * @return mixed
     */
    public function actionIndexDetail()
    {
        $searchModel = new EBarangKeluarSearch();
        $dataProvider = $searchModel->searchDetail(Yii::$app->request->queryParams);

        return $this->render('index-detail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionDeflReportForm()
    {
        $model = new DeflReport();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->validate()) {
                $response->data = ['data' => ['url' => Url::to(['defl-report', 'DeflReport' => $model->attributes])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('_form-laporan-penggunaan', [
            'model' => $model
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionDeflReportValidation()
    {
        $model = new DeflReport();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return mixed
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws PdfParserException
     * @throws PdfTypeException
     * @throws MethodNotAllowedHttpException
     */
    public function actionDeflReport()
    {
        try {

            $model = new DeflReport();
            $model->load(Yii::$app->request->queryParams);
//            print_r($model->formatingDeftReport());
//            return;
            if ($model->validate()) {
                $content = $this->renderAjax('_laporan-penggunaan', ['model' => $model, 'datas' => $model->formatingDeftReport()]);
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_CORE,
                    'format' => Pdf::FORMAT_A4,
                    'orientation' => Pdf::ORIENT_PORTRAIT,
                    'destination' => Pdf::DEST_BROWSER,
                    'content' => $content,
                    'cssFile' => '@frontend/web/css/laporanHarian.css',
                    'cssInline' => 'table{font-size:11px; font-family:open sans, tahoma, sans-serif}',
                    'options' => ['title' => Yii::$app->name],
                    'marginTop' => 0,
                    'marginBottom' => '5',
                    'marginLeft' => '5',
                    'marginRight' => '5',
                    'methods' => [
                        'SetFooter' => ['{PAGENO}'],
                    ]
                ]);
                return $pdf->render();
            }
        } catch (MpdfException $e) {
            Yii::info($e->getMessage(), 'exception');
            throw new MpdfException(Yii::t('frontend', 'The requested page does not exist.'));
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

}

<?php

namespace frontend\modules\erp\controllers;

use frontend\modules\erp\models\EBarang;
use frontend\modules\erp\models\EGudang;
use frontend\modules\erp\models\EStok;
use frontend\modules\erp\models\form\EBarangForm;
use frontend\modules\erp\models\search\EBarangSearch;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * BarangController implements the CRUD actions for EBarang model.
 */
class BarangController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EBarang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EBarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EBarang model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);
        $providerEHarga = new ArrayDataProvider([
            'allModels' => $model->eHargas,
        ]);
        $providerEStok = new ArrayDataProvider([
            'allModels' => $model->eStoks,
        ]);
        $dataProviderGudang = new ActiveDataProvider([
            'query' => EGudang::find(),
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerEHarga' => $providerEHarga,
            'providerEStok' => $providerEStok,
            'dataProviderGudang' => $dataProviderGudang
        ]);
    }

    /**
     * Creates a new EBarang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EBarang();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EBarang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new EBarang();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @param int $status
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionChangeAktifStatus(string $id, int $status)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->is_aktif = $status;
            if ($model->save(false)) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
            Yii::info($model->errors, 'exception');
        }
        return $response->data;
    }

    /**
     * Deletes an existing EBarang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the EBarang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EBarang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id)
    {
        if (($model = EBarang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return EGudang|null
     * @throws NotFoundHttpException
     */
    protected function findModelGudang(string $id)
    {
        if (($model = EGudang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $idgudang
     * @param string $idbarang
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionTambahkanKeGudang(string $idgudang, string $idbarang): array
    {
        $model = $this->findModel($idbarang);
        $modelGudang = $this->findModelGudang($idgudang);

        $modelStok = EStok::findOne(['e_barang_id' => $model->id, 'e_gudang_id' => $modelGudang->id]);

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [$model->errors], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            if (!$modelStok) {
                $modelStok = new EStok();
                $modelStok->e_barang_id = $model->id;
                $modelStok->e_gudang_id = $modelGudang->id;
                if ($modelStok->save()) {
                    $response->data = ['data' => ['url' => Url::to(['view', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
                }
            }
        }
        return $response->data;
    }

    /**
     * @param string $gudangid
     * @param string|null $q
     * @return \string[][]
     */
    public function actionGetBarangByGudangForSelect2(string $gudangid, string $q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && !is_null($gudangid)) {
            $out['results'] = EBarang::getBarangByGudangForSelect2($gudangid, $q);
        }
        return $out;
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUploadFile(string $id)
    {
        $model = new EBarangForm();
        $model->scenario = 'upload';
        $modelBarang = $this->findModel($id);
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [], 'status' => false, 'message' => Yii::t('app', 'Gagal Upload Data')];
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->validate()) {
                /** @var UploadedFile $docFile */
                $docFile = $model->doc_file;
                if ($docFile->saveAs($model->upload_dir . $modelBarang->id . '.' . $docFile->extension)) {
                    $modelBarang->gambar = $docFile->name;
                    if ($modelBarang->save()) {
                        $response->data = ['data' => ['url' => Url::to(['view', 'id' => $modelBarang->id])], 'status' => true, 'message' => Yii::t('app', 'Berhasil Upload Data')];
                    }
                }
            }
            return $response->data;
        }
        return $this->renderAjax('_form-upload-file', ['model' => $model, 'modelBarang' => $modelBarang]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadFileFormValidation()
    {
        $model = new EBarangForm();
        $model->scenario = 'upload';
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return \yii\console\Response|Response
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionViewFile(string $id)
    {
        $model = new EBarangForm();
        $modelBarang = $this->findModel($id);
        /** @var UploadedFile $docFile */
        $docFile = $model->doc_file;
        $filePath = Yii::getAlias($model->upload_dir . $modelBarang->id . '.jpg');
        if (file_exists($filePath)) {
            return Yii::$app->response->sendFile($filePath, $modelBarang->id, ['inline' => true]);
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'File Tidak Tersedia'));
    }

    /**
     * @param string $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionGetImage(string $id): string
    {
        $modelStok = EStok::findOne($id);
        if ($modelStok) {
            $model = $this->findModel($modelStok->e_barang_id);
            $modelForm = new EBarangForm();
            $file = $modelForm->upload_dir . $model->id . '.' . $model->getFileExtension();
            return Html::img('@web/' . $modelForm->upload_dir . $model->id . '.' . $model->getFileExtension());
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'File Tidak Tersedia'));
    }

}

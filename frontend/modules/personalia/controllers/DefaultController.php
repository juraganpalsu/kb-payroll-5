<?php

namespace frontend\modules\personalia\controllers;

use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\personalia\models\form\Pegawai;
use kartik\widgets\ActiveForm;
use Yii;
use yii\db\Exception;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `personalia` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return array|mixed|string
     */
    public function actionKaryawanMasukForm()
    {
        $model = new Pegawai();
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $model->load($request->post());
            $redirectUrl = '';
            if ($model->jenis == Pegawai::karyawan_masuk) {
                $redirectUrl = Url::to(['karyawan-masuk', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::karyawan_resign) {
                $redirectUrl = Url::to(['karyawan-resign', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::ulang_tahun) {
                $redirectUrl = Url::to(['ulang-tahun', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::bpjs_tk) {
                $redirectUrl = Url::to(['bpjs-tk', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::bpjs_kes) {
                $redirectUrl = Url::to(['bpjs-kesehatan', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::habis_kontrak) {
                $redirectUrl = Url::to(['habis-kontrak', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::manulife) {
                $redirectUrl = Url::to(['manulife', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::beweekly_factory) {
                $redirectUrl = Url::to(['beweekly', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::jumlah_karyawan) {
                $redirectUrl = Url::to(['jumlah-karyawan', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::masa_kerja) {
                $redirectUrl = Url::to(['masa-kerja', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::assesment) {
                $redirectUrl = Url::to(['assesment', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::pegawai) {
                $redirectUrl = Url::to(['pegawai', 'Pegawai' => $model->attributes]);
            }
            if ($model->jenis == Pegawai::loyalitas) {
                $redirectUrl = Url::to(['loyalitas', 'Pegawai' => $model->attributes]);
            }
            $response->data = [
                'data' => ['url' => $redirectUrl],
                'message' => '',
                'status' => true,
            ];
            return $response->data;
        }

        return $this->render('karyawan-masuk-form', ['model' => $model]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionKaryawanMasukValidation()
    {
        $model = new Pegawai();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionKaryawanMasuk()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            return $model->karyawanMasukExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionKaryawanResign()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            return $model->karyawanResignExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionUlangTahun()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            return $model->ulangTahunExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }


    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionBpjsTk()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            return $model->bpjsExcel($model->formatingDataBpjs(PegawaiBpjs::BPJS_KETENAGAKERJAAN), PegawaiBpjs::BPJS_KETENAGAKERJAAN);
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionBpjsKesehatan()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            return $model->bpjsExcel($model->formatingDataBpjs(PegawaiBpjs::BPJS_KESEHATAN), PegawaiBpjs::BPJS_KESEHATAN);
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionHabisKontrak()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            return $model->habisKontrakExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionManulife()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            return $model->bpjsExcel($model->formatingDataManulife());
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionBeweekly()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            return $model->biweeklyExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionJumlahKaryawan()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
//            print_r($model->formatingDataJumlahKaryawan());
//            return;
            try {
                return $model->jumlahKaryawanExcel();
            } catch (Exception $e) {
                Yii::t($e->getMessage(), 'exception');
            }
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionMasaKerja()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
//            print_r($model->MasaKerjaExcel());
//            return;
            return $model->masaKerjaExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionAssesment()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            //     echo "<pre>";
            //    print_r($model->formatingAssesment());
            //    echo "</pre>";
            //    return;
            return $model->AssesmentExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @return bool
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionPegawai()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
            // echo "<pre>";
            // print_r($model->formatingPegawai());
            // echo "</pre>";
            // return;
            return $model->PegawaiIDExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionLoyalitas()
    {
        $model = new Pegawai();
        $params = Yii::$app->request->queryParams;
        $model->load($params);
        if ($model->validate()) {
//             echo "<pre>";
//             print_r($model->attributes);
//             print_r($model->formatingDataLoyalitas());
//             echo "</pre>";
//             return;
            return $model->loyalitasExcel();
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));

    }
}

<?php

namespace frontend\modules\personalia\controllers;

use frontend\modules\personalia\models\form\MppForm;
use frontend\modules\personalia\models\Mpp;
use frontend\modules\personalia\models\MppAct;
use frontend\modules\personalia\models\search\MppSearch;
use kartik\mpdf\Pdf;
use kartik\widgets\ActiveForm;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * MppController implements the CRUD actions for Mpp model.
 */
class MppController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mpp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MppSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mpp model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Mpp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Mpp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id): Mpp
    {
        if (($model = Mpp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @return string
     */
    public function actionIndexAktual(): string
    {
        $searchModel = new MppSearch();
        $dataProvider = $searchModel->searchAktual(Yii::$app->request->queryParams);

        return $this->render('index-aktual', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @return array
     */
    public function actionUpdateMppAktual(): array
    {
        $model = new MppAct();

        /** @var Request $request */
        $request = Yii::$app->request;
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
        if ($request->isAjax && $request->isPost) {
            $model->saveMpp();
            $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil delete data.'), 'status' => true];
        }
        return $response->data;
    }

    /**
     * @return string
     */
    public function actionPrintReportMpp()
    {
        $model = new MppForm();
        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $model->load($request->post());
            $response->data = ['data' => ['url' => Url::to(['print', 'tahun' => $model->tahun])], 'message' => Yii::t('frontend', 'Berhasil'), 'status' => true];
            return $response->data;
        }
        return $this->renderAjax('_print-report-form', ['model' => $model]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionPrintReportMppValidation(): array
    {
        $model = new MppForm();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param int $tahun
     * @return mixed
     * @throws MpdfException
     * @throws CrossReferenceException
     * @throws PdfParserException
     * @throws PdfTypeException
     * @throws InvalidConfigException
     */
    public function actionPrint(int $tahun)
    {
        $model = new MppAct();
        $report = $model->formatingReport($tahun);
        $content = $this->renderAjax('_print', ['report' => $report, 'tahun' => $tahun]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@frontend/web/css/laporanHarian.css',
            'cssInline' => '',
            'options' => ['title' => Yii::t('frontend', 'OUT ORDER REPORT') . '-' . Yii::$app->name],
            'marginTop' => 5,
            'marginBottom' => 20,
            'marginLeft' => 5,
            'marginRight' => 5,
            'methods' => [
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

}

<?php

namespace frontend\modules\personalia\controllers;

use frontend\modules\personalia\models\form\SuratPeringatanForm;
use frontend\modules\personalia\models\search\SuratPeringatanSearch;
use frontend\modules\personalia\models\SuratPeringatan;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * SuratPeringatanController implements the CRUD actions for SuratPeringatan model.
 */
class SuratPeringatanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SuratPeringatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SuratPeringatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all SuratPeringatan models.
     * @return mixed
     */
    public function actionBcaIndex()
    {
        $searchModel = new SuratPeringatanSearch();
        $dataProvider = $searchModel->searchBca(Yii::$app->request->queryParams);

        return $this->render('index-bca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SuratPeringatan model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new SuratPeringatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SuratPeringatan();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $id
     * @return mixed
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws NotFoundHttpException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function actionPrint(string $id)
    {
        $model = $this->findModel($id);
        $content = $this->renderAjax('_print_sp', ['model' => $model]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@frontend/web/css/surat.css',
            'cssInline' => 'table{font-size:10px}',
            'options' => ['title' => Yii::$app->name],
            'marginTop' => '5',
            'marginBottom' => '5',
            'marginLeft' => '4',
            'marginRight' => '2',
            'methods' => [
                'SetFooter' => [Yii::$app->formatter->asDatetime('now') . '|Page {PAGENO}|'],
            ]
        ]);
//        return $content;

        return $pdf->render();
    }

    /**
     * Updates an existing SuratPeringatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new SuratPeringatan();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing SuratPeringatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the SuratPeringatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return SuratPeringatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuratPeringatan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * @param string $id
     * @return array|mixed|string
     * @throws NotFoundHttpException
     */
    public function actionUploadFile(string $id)
    {
        $model = new SuratPeringatanForm();
        $model->scenario = 'upload';
        $modelSuratPeringatan = $this->findModel($id);
        $request = Yii::$app->request;
        if ($request->isPost && $request->isAjax) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $response->data = ['data' => [], 'status' => false, 'message' => Yii::t('app', 'Gagal Upload Data')];
            $model->doc_file = UploadedFile::getInstance($model, 'doc_file');
            if ($model->validate()) {
                /** @var UploadedFile $docFile */
                $docFile = $model->doc_file;
                if ($docFile->saveAs($model->upload_dir . $modelSuratPeringatan->id . '.' . $docFile->extension)) {
                    $response->data = ['data' => ['url' => Url::to(['index'])], 'status' => true, 'message' => Yii::t('app', 'Berhasil Upload Data')];
                }
            }
            return $response->data;
        }
        return $this->render('_form-upload-file', ['model' => $model, 'modelSuratPeringatan' => $modelSuratPeringatan]);
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionUploadFileFormValidation()
    {
        $model = new SuratPeringatanForm();
        $model->scenario = 'upload';
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return \yii\console\Response|Response
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     */
    public function actionViewFile(string $id)
    {
        $model = new SuratPeringatanForm();
        $modelSuratPeringatan = $this->findModel($id);
        $filePath = Yii::getAlias($model->upload_dir . $modelSuratPeringatan->id . '.pdf');
        if (file_exists($filePath)) {
            return Yii::$app->response->sendFile($filePath, $modelSuratPeringatan->id, ['inline' => true]);
        }
        throw new NotFoundHttpException(Yii::t('frontend', 'File Tidak Tersedia'));
    }
}

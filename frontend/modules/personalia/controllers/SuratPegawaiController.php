<?php

namespace frontend\modules\personalia\controllers;

use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Throwable;
use Yii;
use frontend\modules\personalia\models\search\SuratPegawaiSearch;
use yii\base\InvalidConfigException;
use yii\db\StaleObjectException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Request;
use yii\web\Response;

/**
 * SuratPegawaiController implements the CRUD actions for SuratPegawai model.
 */
class SuratPegawaiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SuratPegawai models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SuratPegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelPerjanjiankerja = new PerjanjianKerja();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelPerjanjiankerja' => $modelPerjanjiankerja,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionMenuSurat()
    {
        $model = new SuratPegawai();
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            $model->load($request->post());
            $redirectUrl = '';
            if ($model->jenis == SuratPegawai::pkwt) {
                $redirectUrl = Url::to(['pkwt', 'SuratPegawai' => $model->attributes]);
            }
            if ($model->jenis == SuratPegawai::sk_acting) {
                $redirectUrl = Url::to(['sk-acting', 'SuratPegawai' => $model->attributes]);
            }
            if ($model->jenis == SuratPegawai::addedum) {
                $redirectUrl = Url::to(['addendum', 'SuratPegawai' => $model->attributes]);
            }
            if ($model->jenis == SuratPegawai::sk_pengakatan) {
                $redirectUrl = Url::to(['sk-pengangkatan', 'SuratPegawai' => $model->attributes]);
            }
            if ($model->jenis == SuratPegawai::sk_promosi_jabatan) {
                $redirectUrl = Url::to(['sk-promosi-jabatan', 'SuratPegawai' => $model->attributes]);
            }
            if ($model->jenis == SuratPegawai::sk_demosi) {
                $redirectUrl = Url::to(['sk-demosi', 'SuratPegawai' => $model->attributes]);
            }
            if ($model->jenis == SuratPegawai::sk_mutasi) {
                $redirectUrl = Url::to(['sk-mutasi', 'SuratPegawai' => $model->attributes]);
            }
            if ($model->jenis == SuratPegawai::sk_aktif) {
                $redirectUrl = Url::to(['sk-aktif', 'SuratPegawai' => $model->attributes]);
            }
            $response->data = [
                'data' => ['url' => $redirectUrl],
                'message' => '',
                'status' => true,
            ];
            return $response->data;
        }
        return $this->renderAjax('menu-surat', ['model' => $model]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionMenuSuratValidation()
    {
        $model = new SuratPegawai();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return \kartik\widgets\ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @return array|mixed|string
     */
    public function actionPkwt()
    {
        $model = new SuratPegawai();
        $model->jenis = SuratPegawai::pkwt;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formPkwt', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionSkActing()
    {
        $model = new SuratPegawai();
        $model->jenis = SuratPegawai::sk_acting;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formSkActing', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionAddendum()
    {
        $model = new SuratPegawai();
        $model->jenis = SuratPegawai::addedum;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formAddendum', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionSkPengangkatan()
    {
        $model = new SuratPegawai();
        $model->jenis = SuratPegawai::sk_pengakatan;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formSkPengakatan', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionSkPromosiJabatan()
    {
        $model = new SuratPegawai();
        $model->jenis = SuratPegawai::sk_promosi_jabatan;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formPromosiJab', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionSkDemosi()
    {
        $model = new SuratPegawai();
        $model->jenis = SuratPegawai::sk_demosi;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formSkDemosi', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionSkMutasi()
    {
        $model = new SuratPegawai();
        $model->jenis = SuratPegawai::sk_mutasi;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formMutasi', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionSkAktif()
    {

        $model = new SuratPegawai();
        $model->jenis = SuratPegawai::sk_aktif;

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formAktif', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single SuratPegawai model.
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the SuratPegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @return SuratPegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuratPegawai::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Creates a new SuratPegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SuratPegawai();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $id
     * @return mixed
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws NotFoundHttpException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function actionPrint(string $id)
    {
        $model = $this->findModel($id);
        $content = '';
        if ($model->jenis == SuratPegawai::pkwt) {
            $content = $this->renderAjax('_print_pkwt', ['model' => $model]);
        }
        if ($model->jenis == SuratPegawai::sk_acting) {
            $content = $this->renderAjax('_print_sk_acting', ['model' => $model]);
        }
        if ($model->jenis == SuratPegawai::addedum) {
            $content = $this->renderAjax('_print_addendum', ['model' => $model]);
        }
        if ($model->jenis == SuratPegawai::sk_pengakatan) {
            $content = $this->renderAjax('_print_sk_pengangkatan', ['model' => $model]);
        }
        if ($model->jenis == SuratPegawai::sk_promosi_jabatan) {
            $content = $this->renderAjax('_print_sk_promosi', ['model' => $model]);
        }
        if ($model->jenis == SuratPegawai::sk_demosi) {
            $content = $this->renderAjax('_print_surat_demosi', ['model' => $model]);
        }
        if ($model->jenis == SuratPegawai::sk_mutasi) {
            $content = $this->renderAjax('_print_surat_mutasi', ['model' => $model]);
        }
        if ($model->jenis == SuratPegawai::sk_aktif) {
            $content = $this->renderAjax('_print_surat_aktif', ['model' => $model]);
        }

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@frontend/web/css/surat_pegawai.css',
            'cssInline' => 'table{font-size:10px}',
            'options' => ['title' => Yii::$app->name],
            'marginTop' => 0,
            'marginBottom' => 0,
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginFooter' => 3,
            'methods' => [
                'SetHTMLFooter' => ['<div class="footer">' . Yii::$app->formatter->asDatetime('now') . ' |Page {PAGENO}|' . '</div>'],
            ]
        ]);
//        return $content;

        return $pdf->render();
    }

    /**
     * Updates an existing SuratPegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new SuratPegawai();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * Deletes an existing SuratPeringatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param string $pegawaiId
     * @param string $tanggal
     * @return array|mixed
     */
    public function actionGetTglMasaKerja(string $pegawaiId)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal.'), 'status' => false];
        if ($request->isAjax && $request->isGet) {
            $model = new SuratPegawai();
            $response->data = ['data' => $model->tglMasaKerja($pegawaiId), 'message' => Yii::t('frontend', 'Berhasil.'), 'status' => true];
        }
        return $response->data;
    }
}

<?php

namespace frontend\modules\personalia\controllers;

use frontend\modules\personalia\models\Appraisal;
use frontend\modules\personalia\models\AppraisalDetail;
use frontend\modules\personalia\models\RefAppraisal;
use frontend\modules\personalia\models\search\AppraisalSearch;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * AppraisalController implements the CRUD actions for Appraisal model.
 */
class AppraisalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Appraisal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $isForMe = false;
        $searchModel = new AppraisalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $isForMe);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'isForMe' => $isForMe
        ]);
    }

    /**
     * Lists all Appraisal models.
     * @return mixed
     */
    public function actionIndexPenilaian()
    {
        $isForMe = true;
        $searchModel = new AppraisalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $isForMe);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'isForMe' => $isForMe
        ]);
    }

    /**
     * Displays a single Appraisal model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(string $id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Appraisal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Appraisal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(string $id)
    {
        if (($model = Appraisal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Creates a new Appraisal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Appraisal();
        $model->scenario = 'header';

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            }
            return $response->data;
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Appraisal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreatePenilaian($id)
    {
        $model = $this->findModel($id);
        $modelAppraisalDetail = new AppraisalDetail();
        $modelAppraisalDetail->appraisal_id = $model->id;
        $refAppraisal = RefAppraisal::find()->andWhere(['kategori' => $model->kategori])->all();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($modelAppraisalDetail->load($request->post())) {
                $return = false;
                if ($model->kategori == Appraisal::STAFF) {
                    $return = $modelAppraisalDetail->getValuePost($request->post());
                }
                if ($model->kategori == Appraisal::MANAGERIAL) {
                    $return = $modelAppraisalDetail->getValuePostManagerial($request->post());
                }
                if ($return) {
                    $response->data = ['data' => ['url' => Url::to(['create-kesimpulan', 'id' => $model->id])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
                } else {
                    $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
                }
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formPenilaian', [
            'model' => $model,
            'modelAppraisalDetail' => $modelAppraisalDetail,
            'refAppraisal' => $refAppraisal,
        ]);
    }

    /**
     * Creates a new Appraisal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreateKesimpulan(string $id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'hasil';
        $refAppraisal = RefAppraisal::find()->andWhere(['kategori' => $model->kategori])->all();

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index-penilaian'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('_formHasil', [
            'model' => $model,
            'refAppraisal' => $refAppraisal,
        ]);
    }

    /**
     * Updates an existing Appraisal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(string $id)
    {
        $model = $this->findModel($id);

        /** @var Request $request */
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            /** @var Response $response */
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $response->data = ['data' => ['url' => Url::to(['index'])], 'message' => Yii::t('frontend', 'Berhasil input data.'), 'status' => true];
            } else {
                $response->data = ['data' => [], 'message' => Yii::t('frontend', 'Gagal input data.'), 'status' => false];
            }
            return $response->data;
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCreateOrUpdateValidation()
    {
        $model = new Appraisal();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }


    /**
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCouValidationPenilaian()
    {
        $model = new AppraisalDetail();
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $request->isPost) {
            $model->load($request->post());
            return ActiveForm::validate($model);
        }
        throw new MethodNotAllowedHttpException(Yii::t('frontend', 'The request does not allowed!'));
    }

    /**
     * @param string $id
     * @return mixed
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws NotFoundHttpException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function actionPrint(string $id)
    {
        $model = $this->findModel($id);
        $print = '';
        if ($model->kategori == Appraisal::STAFF) {
            $print = '_print_appraisal_staff';
        }
        if ($model->kategori == Appraisal::MANAGERIAL) {
            $print = '_print_appraisal_managerial';
        }
        $refAppraisal = RefAppraisal::findAll(['kategori' => $model->kategori]);
        $content = $this->renderAjax($print, [
            'model' => $model,
            'refAppraisal' => $refAppraisal,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@frontend/web/css/surat_pegawai.css',
            'cssInline' => 'table{font-size:10px}',
            'options' => ['title' => Yii::$app->name],
            'marginTop' => '5',
            'marginBottom' => '5',
            'marginLeft' => '4',
            'marginRight' => '2',
            'methods' => [
                'SetFooter' => [Yii::$app->formatter->asDatetime('now') . '|Page {PAGENO}|'],
            ]
        ]);
//        return $content;

        return $pdf->render();
    }

    /**
     * Deletes an existing Appraisal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(string $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}

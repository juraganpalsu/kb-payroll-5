<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefPeraturanPerusahaan */

$this->title = Yii::t('frontend', 'Create Ref Peraturan Perusahaan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Peraturan Perusahaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-peraturan-perusahaan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

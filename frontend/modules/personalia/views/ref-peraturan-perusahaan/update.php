<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefPeraturanPerusahaan */

$this->title = Yii::t('frontend', 'Update {modelClass} ', [
        'modelClass' => 'Ref Peraturan Perusahaan',
    ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Peraturan Perusahaan'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="ref-peraturan-perusahaan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

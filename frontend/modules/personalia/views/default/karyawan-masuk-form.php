<?php

/**
 * Created by PhpStorm.
 * File Name: karyawan-masuk.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 27/02/20
 * Time: 22.02
 */

use common\components\Bulan;
use frontend\modules\personalia\models\form\Pegawai;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model Pegawai */
/* @var $sistemKerja */
/* @var $statusKehadiran */

$this->title = Yii::t('frontend', 'Form Report Personalia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Pegawai'), 'url' => ['/pegawai/pegawai']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<< JS
$(function() {
    $('#laporan-karyawan-masuk-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                console.log(dt);
                let win = window.open(dt.data.url, '_blank');
                win.focus();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);
?>
<div class="laporan-karyawan-masuk">
    <div class="karyawan-masuk-form">

        <?php $form = ActiveForm::begin([
            'id' => 'laporan-karyawan-masuk-form',
            'options' => ['target' => '_blank'],
            'type' => ActiveForm::TYPE_VERTICAL,
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['karyawan-masuk-validation'])
        ]); ?>

        <?= $form->errorSummary($model); ?>

        <?php try { ?>

            <div class="row">
                <div class="col-md-3">
                    <?php
                    echo $form->field($model, 'jenis')->dropDownList($model->_jenis);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <?php
                    echo $form->field($model, 'bulan')->dropDownList(Bulan::_bulan());
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                    echo $form->field($model, 'tahun')->dropDownList(Bulan::_tahun());
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?php
                    echo $form->field($model, 'bisnis_unit')->dropDownList(Pegawai::modelPerjanjianKerja()->_kontrak, ['prompt' => $model->getAttributeLabel('bisnis_unit')]);
                    ?>
                </div>
            </div>

            <hr/>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('frontend', 'Generate'), ['class' => 'btn btn-success btn-flat']) ?>
            </div>

            <?php
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
        <?php ActiveForm::end(); ?>
    </div>


</div>

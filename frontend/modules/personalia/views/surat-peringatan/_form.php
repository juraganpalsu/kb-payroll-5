<?php

use common\components\Status;
use frontend\modules\personalia\models\Pegawai;
use frontend\modules\personalia\models\PegawaiGolongan;
use frontend\modules\personalia\models\PegawaiStruktur;
use frontend\modules\personalia\models\PerjanjianKerja;
use frontend\modules\personalia\models\RefPeraturanPerusahaan;
use frontend\modules\personalia\models\RefSuratPeringatan;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPeringatan */
/* @var $form yii\widgets\ActiveForm */

$js = <<< JS
$(function() {
    $('#form-surat-peringatan').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });    
})
JS;

$this->registerJs($js);
?>

<div class="surat-peringatan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-surat-peringatan',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'ref_surat_peringatan_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(RefSuratPeringatan::find()->orderBy('id')->orderBy('nama ASC')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Ref surat peringatan')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'gmp')->dropDownList(Status::activeInactive(0, true)); ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-8">
                <?php
                if (!$model->isNewRecord) {
                    $model->ref_peraturan_perusahaan_id = explode(',', $model->ref_peraturan_perusahaan_id);
                }
                ?>
                <?= $form->field($model, 'ref_peraturan_perusahaan_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(RefPeraturanPerusahaan::find()->orderBy('id')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Ref Peraturan Perusahaan')],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true
                    ],
                ]); ?>
            </div>

        </div>


        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'mulai_berlaku')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'akhir_berlaku')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'pegawai_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => false,
                    ],
                    'data' => $model->isNewRecord ? [] : [$model->pegawai_id => $model->pegawai->nama_lengkap . '-' . $model->pegawai->nama_panggilan],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, tanggal:$("#suratperingatan-mulai_berlaku").val()};}')
                        ],
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'atasan_pegawai_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => false,
                    ],
                    'data' => $model->isNewRecord ? [] : [$model->atasan_pegawai_id => $model->atasanPegawai->nama_lengkap . '-' . $model->atasanPegawai->nama_panggilan],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, tanggal:$("#suratperingatan-mulai_berlaku").val()};}')
                        ],
                    ],
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'oleh_pegawai_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => false,
                    ],
                    'data' => $model->TtdOleh,
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPeringatan */

$this->title = Yii::t('frontend', 'Create Surat Peringatan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Surat Peringatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-peringatan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

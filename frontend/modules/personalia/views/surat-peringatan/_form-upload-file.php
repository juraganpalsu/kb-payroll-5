<?php
/**
 * Created by PhpStorm.
 * File Name: _form-upload-file.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 26/10/2020
 * Time: 11:52
 */

use frontend\modules\personalia\models\form\SuratPeringatanForm;
use frontend\modules\personalia\models\SuratPeringatan;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/**
 * @var $model SuratPeringatanForm
 * @var $modelSuratPeringatan SuratPeringatan
 */

$this->title = Yii::t('frontend', 'Upload Surat Peringatan {pegawai}', ['pegawai' => $modelSuratPeringatan->pegawai->nama_lengkap]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Upload Surat Peringatan'), 'url' => ['index']];


$jsForm = <<<JS
$(function() {
    $('#create-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        let formData = new FormData(this);
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function (dt) {                
                console.log(dt);
                if(dt.status === true){
                    location.href = dt.data.url;
                }
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($jsForm);
?>
<div class="surat-peringatan-form">
    <div class="surat-peringatan-form">
        <?php $form = ActiveForm::begin([
            'id' => 'create-form',
            'action' => Url::to(['upload-file', 'id' => $modelSuratPeringatan->id]),
            'type' => ActiveForm::TYPE_VERTICAL,
            'formConfig' => ['showErrors' => false],
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['upload-file-form-validation']),
            'fieldConfig' => ['showLabels' => true],
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

        <?= $form->errorSummary($model); ?>

        <?php try { ?>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'doc_file')->widget(FileInput::class, [
                        'options' => [
                            'multiple' => false,
                            'accept' => 'pdf'
                        ],
                        'pluginOptions' => [
                            'previewFileType' => 'any',
                            'showUpload' => false,
                            'initialPreview' => [],
                            'initialPreviewAsData' => true,
                            'initialPreviewConfig' => [],
//                            'deleteUrl' => [],
                            'overwriteInitial' => false,
                            'maxFileSize' => 7000
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('frontend', 'Upload'), ['class' => 'btn btn-success submit-btn']) ?>
            </div>

            <?php
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
        <?php ActiveForm::end(); ?>

    </div>

</div>
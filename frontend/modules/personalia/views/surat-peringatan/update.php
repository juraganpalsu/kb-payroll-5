<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPeringatan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Surat Peringatan',
]) . ' ' . $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Surat Peringatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="surat-peringatan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\personalia\models\search\SuratPeringatanSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Status;
use common\models\Golongan;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\personalia\models\RefPeraturanPerusahaan;
use frontend\modules\personalia\models\RefSuratPeringatan;
use frontend\modules\personalia\models\SuratPeringatan;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$this->title = Yii::t('frontend', 'Surat Peringatan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-peringatan-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'ref_surat_peringatan_id',
            'label' => Yii::t('frontend', 'Ref Surat Peringatan'),
            'value' => function (SuratPeringatan $model) {
                return $model->refSuratPeringatan->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(RefSuratPeringatan::find()->asArray()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Ref Surat Peringatan'), 'id' => 'grid-surat-peringatan-search-ref_surat_peringatan_id']
        ],
        [
            'attribute' => 'ref_peraturan_perusahaan_id',
            'label' => Yii::t('frontend', 'Ref Peraturan Perusahaan'),
            'value' => function (SuratPeringatan $model) {
                return $model->getPeraturanPerusahaanFormated();
            },
            'hidden' => true
        ],
        [
            'attribute' => 'ref_peraturan_perusahaan_id',
            'label' => Yii::t('frontend', 'Ref Peraturan Perusahaan'),
            'value' => function (SuratPeringatan $model) {
                return $model->getPeraturanPerusahaanFormated('<br>', 50);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(RefPeraturanPerusahaan::find()->orderBy('nama ASC')->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Ref Peraturan Perusahaan'), 'id' => 'grid-surat-peringatan-search-ref_peraturan_perusahaan_id'],
            'format' => 'raw'
        ],
        [
            'attribute' => 'gmp',
            'value' => function (SuratPeringatan $model) {
                return $model->gmp_;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::activeInactive(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'GMP')],
        ],
        [
            'label' => Yii::t('frontend', 'Departemen'),
            'value' => function (SuratPeringatan $model) {
                $pegawaiStruktur = $model->pegawaiStruktur;
                return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : '';
            },
            'hidden' => true
        ],
        [
            'label' => Yii::t('frontend', 'Jabatan'),
            'value' => function (SuratPeringatan $model) {
                $pegawaiStruktur = $model->pegawaiStruktur;
                return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->jabatan ? $pegawaiStruktur->struktur->jabatan->nama : '' : '' : '';
            },
            'hidden' => true
        ],
        [
            'label' => Yii::t('frontend', 'Struktur'),
            'value' => function (SuratPeringatan $model) {
                $pegawaiStruktur = $model->pegawaiStruktur;
                return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : '';
            },
            'hidden' => true
        ],
        [
            'attribute' => 'id_pegawai',
            'label' => Yii::t('app', 'Id Pegawai'),
            'value' => function (SuratPeringatan $model) {
                return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (SuratPeringatan $model) {
                return Html::a(Html::tag('strong', $model->pegawai->nama_lengkap), ['print', 'id' => $model->id], ['class' => 'font-weight-bold', 'title' => 'Print Surat', 'data' => ['pjax' => 0], 'target' => '_blank']);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (SuratPeringatan $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $searchModel->modelPerjanjianKerja()->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-resign-search-busines_unit'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'golongan',
            'value' => function (SuratPeringatan $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'mulai_berlaku',
            'value' => function (SuratPeringatan $model) {
                return date('d-m-Y', strtotime($model->mulai_berlaku));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'akhir_berlaku',
            'value' => function (SuratPeringatan $model) {
                return date('d-m-Y', strtotime($model->akhir_berlaku));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'catatan',
            'value' => function (SuratPeringatan $model) {
                return substr($model->catatan, 0, 50) . '...';;
            },
        ],
        [
            'attribute' => 'catatan',
            'value' => function (SuratPeringatan $model) {
                return $model->catatan;
            }, 'hidden' => true
        ],

        [
            'attribute' => 'area_kerja',
            'value' => function (SuratPeringatan $model) {
                if ($pegawaiAreaKerja = $model->pegawai->areaKerja) {
                    return $pegawaiAreaKerja->refAreaKerja->name;
                }
                return '';
            },
//            'hidden' => true
        ],
        [
            'attribute' => 'created_by',
            'value' => function (SuratPeringatan $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            },
        ],
        [
            'attribute' => 'atasan_pegawai_id',
            'value' => function (SuratPeringatan $model) {
                if ($model->atasanPegawai) {
                    return $model->atasanPegawai->nama;
                }
                return '';
            },
        ],
        [
            'attribute' => 'oleh_pegawai_id',
            'value' => function (SuratPeringatan $model) {
                $nama = $model->olehPegawai ? $model->olehPegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {upload-file} {view-file} {delete}',
            'buttons' => [
                'upload-file' => function ($url, SuratPeringatan $model) {
                    return Html::a(Yii::t('frontend', 'Upload'), $url, ['class' => 'btn btn-xs btn-info btn-flat', 'data' => ['header' => Yii::t('frontend', 'Update Sistem Kerja'), 'pjax' => 0], 'target' => '_blank']);
                },
                'view-file' => function ($url, SuratPeringatan $model) {
                    return Html::a(Yii::t('frontend', 'View'), $url, ['class' => 'btn btn-xs btn-warning btn-flat', 'data' => ['header' => Yii::t('frontend', 'Update Sistem Kerja'), 'pjax' => 0], 'target' => '_blank']);
                },
            ],
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
//            'selectedColumns' => [2, 3, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 17, 18],
            'noExportColumns' => [1, 4, 15, 18], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'surat-peringatan' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-surat-peringatan']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        print_r($e->getMessage());
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

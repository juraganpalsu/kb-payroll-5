<?php

use common\components\Bulan;
use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPeringatan;
use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * File Name: _slips.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 21/06/20
 * Time: 23.19
 */

/**
 * @var SuratPeringatan $model
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$createdBy = $model->createdBy->userPegawai;
?>
<div class="slip-gaji-index">
    <table width="200mm" style="border: 2px solid black;">
        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td class="dept" colspan="4" rowspan="4" style="text-align: center">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == 1) {
                        echo Html::img('@frontend/web/pictures/image001.png', ['height' => '60px']);
                    } elseif ($perjanjianKerja->kontrak == 2) {
                        echo Html::img('@frontend/web/pictures/image002.png', ['height' => '60px']);
                    } else {
                        echo Html::img('@frontend/web/pictures/image003.png', ['height' => '60px']);
                    }
                    ?>
                </div>
            </td>
            <td colspan="14" style="  border: 0;">&nbsp;</td>
            <td colspan="6" class="nodok">No. Kode.Dok</td>
        </tr>
        <tr>
            <th rowspan="2" colspan="14" style="font-size: 24pt">
                <?php
                if ($perjanjianKerja->kontrak == 1) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == 2) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                } else {
                    echo 'PT BINA CIPTA ABADI';
                }
                ?>
            </th>
            <td class="nodok" colspan="6">KBU/FO/HCD/17/034</td>
        </tr>
        <tr>
            <td class="nodok" colspan="6">Tingkatan dokument : IV</td>
        </tr>
        <tr>
            <th colspan="14"
                style="font-size: 16pt"><?= preg_replace('/[0-9]+/', '', $model->refSuratPeringatan->nama); ?></th>
            <td class="nodok" colspan="6">Status Revisi : 00</td>
        </tr>
        <tr>
            <th class="dept" colspan="4">HC</th>
            <td colspan="14">&nbsp;</td>
            <td class="nodok" colspan="6">Tgl. Mulai berlaku : 31 Agustus 2017</td>
        </tr>
        <tr>
            <th class="dept" colspan="4">Departement</th>
            <td colspan="14">&nbsp;</td>
            <td class="nodok" colspan="6">Halaman : 1 dari 1</td>
        </tr>

        <tr>
            <td class="top" colspan="26"></td>
        </tr>
        <tr>
            <td colspan="4"><?= Yii::t('frontend', 'Perihal') ?></td>
            <td colspan="12"> : <?= $model->refSuratPeringatan->nama; ?></td>
        </tr>
        <tr>
            <td colspan="4"><?= Yii::t('frontend', 'No') ?></td>
            <td colspan="12"> :
                ....../SP/HCD–<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /
                <?= Helper::integerToRoman(date('m', strtotime($model->mulai_berlaku))) . '/' . date('Y', strtotime($model->mulai_berlaku)) ?></td>
        </tr>
        <br>
        <tr>
            <td colspan="6"><?= Yii::t('frontend', 'Diberikan kepada') ?> :</td>
        </tr>
        <br><br>
        <tr>
            <td colspan="4"><?= Yii::t('frontend', 'NIK') ?></td>
            <td colspan="6">
                : <?= $perjanjianKerja ? str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : ''; ?></td>
        </tr>
        <tr>
            <td colspan="4"><?= Yii::t('frontend', 'Nama Lengkap') ?></td>
            <td colspan="12"> : <?= $model->pegawai->nama_lengkap; ?></td>
        </tr>
        <tr>
            <td colspan="4"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td colspan="12">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
        </tr>
        <tr>
            <td colspan="4"><?= Yii::t('frontend', 'Departement ') ?></td>
            <td colspan="12">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : ''; ?></td>
        </tr>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <tr>
            <td class="content" colspan="24">Karena telah melakukan kesalahan/ pelanggaran seperti tersebut dibawah ini
                :
            </td>
        </tr>
        <br>
        <tr>
            <td class="content" colspan="24"><strong><?= $model->catatan ?></strong></td>
        </tr>
        <br>
        <tr>
            <td class="content" colspan="24">Maka sebagaimana pada Peraturan Perusahaan pasal
                : <?= $model->getPeraturanPerusahaanFormated() ?></td>
        </tr>
        <tr>
            <td class="content" colspan="24"><strong> Maka dikenakan <?= $model->refSuratPeringatan->nama; ?> </strong>
            </td>
        </tr>
        <tr>
            <td class="content" colspan="24">
                Pemberian Surat ini dimaksudkan sebagai tindakan korektif dan pengarahan terhadap kinerja
                saudara, agar dapat diciptakan lingkungan kerja yang tertib aman, teratur serta menjalankan pekerjaan
                dengan penuh tanggung jawab sesuai dengan tugas dan tanggung jawabnya.
                <?php if ($model->refSuratPeringatan->nama !== 'PEMUTUSAN HUBUNGAN KERJA') { ?>
                    <br>Apabila dalam waktu 6 bulan saudara tidak melakukan pelanggaran, baik yang sama maupun yang lainnya
                        sampai dengan tanggal <strong>  <?= Bulan::tanggal($model->akhir_berlaku) ?>
                        .</strong> Maka Surat ini akan dihapuskan dari arsip pribadi saudara.
                    <br>Tetapi apabila dalam jangka waktu yang disepakati ini saudara melakukan suatu pelanggaran lagi, baik
                        yang sama atau yang lainnya, maka saudara akan dikenakan sanksi yang lebih berat.
                    <br>Diharapkan saudara memperbaiki perilaku, etika dan tidak melakukan pelanggaran kembali.
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td class="content" colspan="18"></td>
            <td class="content" colspan="6"><?= Yii::t('frontend', 'Ditetapkan di 	: Tangerang') ?></td>
        </tr>
        <tr>
            <td class="content" colspan="18"></td>
            <td colspan="8"><?= Yii::t('frontend', 'Tanggal 	: ') . Bulan::tanggal($model->mulai_berlaku) ?></td>
        </tr>
        <tr>
            <td colspan="6" style="border-top: 1px solid; text-align: center;" class="border-left">
                <?= Yii::t('frontend', 'Dibuat Oleh') ?>,
                <br><br><br>
            </td>
            <td colspan="1" style="border-top: 1px solid; text-align: center; border-left: 1px solid;"></td>
            <td colspan="10" style="border-top: 1px solid; text-align: center;">
                <?= Yii::t('frontend', 'Diketahui Oleh') ?>,
                <br><br><br>
            </td>
            <td colspan="2"
                style="border-top: 1px solid; text-align: center;">
            </td>
            <td colspan="8"
                style="border-left: 1px solid; border-top: 1px solid; text-align: center; border-right: 1px solid;">
                <?= Yii::t('frontend', 'Yang bersangkutan') ?>
                <br><br><br>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="border-left: 1px; text-align: center; border-bottom: 1px solid; font-size: 10pt"
                class="border-left border-right">
                <br><br><br>
                <strong><?= $model->atasanPegawai ? $model->atasanPegawai->nama : ''; ?></strong><br>
                (<?= $model->atasanPegawai ? $model->atasanPegawaiStruktur->struktur ? $model->atasanPegawaiStruktur->struktur->name : '' : ''; ?>
                )
            </td>
            <td colspan="5" style="text-align: center;  border-left: 1px solid; border-bottom: 1px solid;">
                <br><br><br><br>
                <?= Yii::t('frontend', '(Ka. Dept)') ?>
            </td>
            <td colspan="8"
                style="font-size: 10pt; text-align: center; border-bottom: 1px solid; border-right: 1px solid;">
                <br><br><br>
                <strong><?php
                    $nama = $model->olehPegawai ? $model->olehPegawai->nama : '';
                    $namaTampil = explode(' ', $nama);
                    echo $namaTampil[0] . ' ' . $namaTampil[1];
                    ?></strong><br>
                <?= $model->olehPegawai->pegawaiStruktur->struktur->name ?>
            </td>
            <td colspan="8"
                style="font-size: 10pt; text-align: center; border-right: 1px solid; border-left: 1px; border-bottom: 1px solid;">
                <br><br><br>
                <strong><?= $model->pegawai->nama_lengkap; ?></strong>
            </td>
        </tr>

    </table>
</div>
<?php

use frontend\modules\personalia\models\PenyelesaianKasus;
use frontend\modules\personalia\models\SuratPeringatan;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPeringatan */

$this->title = $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Surat Peringatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-peringatan-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Surat Peringatan') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'ref_surat_peringatan_id',
                'label' => Yii::t('frontend', 'Ref Surat Peringatan'),
                'value' => function (SuratPeringatan $model) {
                    return $model->refSuratPeringatan->nama;
                },
            ],
            [
                'attribute' => 'ref_peraturan_perusahaan_id',
                'label' => Yii::t('frontend', 'Ref Peraturan Perusahaan'),
                'value' => function (SuratPeringatan $model) {
                    return $model->refPeraturanPerusahaan->nama;
                },
            ],
            [
                'attribute' => 'mulai_berlaku',
                'value' => function (SuratPeringatan $model) {
                    return date('d-m-Y', strtotime($model->mulai_berlaku));
                },
            ],
            [
                'attribute' => 'akhir_berlaku',
                'value' => function (SuratPeringatan $model) {
                    return date('d-m-Y', strtotime($model->akhir_berlaku));
                },
            ],
            'gmp_',
            'catatan:ntext',
            [
                'attribute' => 'refSuratPeringatan.nama',
                'label' => Yii::t('frontend', 'Ref Surat Peringatan'),
            ],
            [
                'attribute' => 'perjanjian_kerja_id',
                'label' => Yii::t('frontend', 'Perjanjian Kerja'),
                'value' => function (SuratPeringatan $model) {
                    return $model->perjanjianKerja->namaKontrak . '-' . $model->perjanjianKerja->jenis->nama . '(' . $model->perjanjianKerja->id_pegawai . ')';
                }
            ],
            [
                'attribute' => 'pegawai_golongan_id',
                'label' => Yii::t('frontend', 'Pegawai Golongan'),
                'value' => function (SuratPeringatan $model) {
                    return $model->pegawaiGolongan->golongan->nama;
                }
            ],
            [
                'attribute' => 'pegawai_struktur_id',
                'label' => Yii::t('frontend', 'Pegawai Struktur'),
                'value' => function (SuratPeringatan $model) {
                    return $model->pegawaiStruktur->struktur->nameCostume;
                }
            ],
            ['attribute' => 'lock', 'visible' => false],
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>
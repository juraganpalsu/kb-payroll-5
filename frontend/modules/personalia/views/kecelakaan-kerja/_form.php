<?php

use kartik\checkbox\CheckboxX;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\KecelakaanKerja */
/* @var $form ActiveForm */


$js = <<< JS
$(function() {
    $('#form-kecelakaan-kerja').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });    
})
JS;

$this->registerJs($js);

?>

<div class="kecelakaan-kerja-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-kecelakaan-kerja',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'tanggal')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'jam')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_TIME,
                    'saveFormat' => 'php:H:i:s',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('frontend', 'Choose Jam'),
                            'autoclose' => true
                        ]
                    ]
                ]); ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'klaim_bpjs')->widget(SwitchInput::class, [
                    'pluginOptions' => ['threeState' => false]
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'pegawai_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => false,
                    ],
                    'data' => $model->isNewRecord ? [] : [$model->pegawai_id => $model->namaPegawai],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, tanggal:$("#kecelakaankerja-tanggal").val()};}')
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'berita_acara')->widget(CheckboxX::class, ['pluginOptions' => ['threeState' => false]]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'tahap_satu')->widget(CheckboxX::class, ['pluginOptions' => ['threeState' => false]]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'tahap_dua')->widget(CheckboxX::class, ['pluginOptions' => ['threeState' => false]]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'foto_ktp')->widget(CheckboxX::class, ['pluginOptions' => ['threeState' => false]]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'kartu_bpjs')->widget(CheckboxX::class, ['pluginOptions' => ['threeState' => false]]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'absensi')->widget(CheckboxX::class, ['pluginOptions' => ['threeState' => false]]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <?= $form->field($model, 'lokasi')->textInput(['maxlength' => true, 'placeholder' => 'Lokasi']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'kronologi')->textarea(['rows' => 6]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <?= $form->field($model, 'cedera')->textInput(['maxlength' => true, 'placeholder' => 'Cedera']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'penyebab')->textarea(['rows' => 6]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'preventif')->textarea(['rows' => 6]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>
            </div>
        </div>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

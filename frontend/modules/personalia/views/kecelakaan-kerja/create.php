<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\KecelakaanKerja */

$this->title = Yii::t('frontend', 'Create Kecelakaan Kerja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Kecelakaan Kerja'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kecelakaan-kerja-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

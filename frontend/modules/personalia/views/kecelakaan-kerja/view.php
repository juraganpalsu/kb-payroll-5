<?php

use common\components\Status;
use frontend\modules\personalia\models\KecelakaanKerja;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\KecelakaanKerja */

$this->title = $model->pegawai->nama_lengkap . '-' . $model->pegawai->nama_panggilan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Kecelakaan Kerja'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kecelakaan-kerja-view">

    <div class="row">
        <div class="col-md-3">
            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'tanggal',
                    'value' => function (KecelakaanKerja $model) {
                        return date('d-m-Y', strtotime($model->tanggal));
                    },
                ],
                'jam',
                'lokasi',
                [
                    'attribute' => 'klaim_bpjs',
                    'value' => function (KecelakaanKerja $model) {
                        return Status::activeInactive($model->klaim_bpjs);
                    },
                ],
                [
                    'attribute' => 'berita_acara',
                    'value' => function (KecelakaanKerja $model) {
                        return Status::activeInactive($model->berita_acara);
                    }
                ],
                ['attribute' => 'tahap_satu',
                    'value' => function (KecelakaanKerja $model) {
                        return Status::activeInactive($model->tahap_satu);
                    }
                ],
                ['attribute' => 'tahap_dua',
                    'value' => function (KecelakaanKerja $model) {
                        return Status::activeInactive($model->tahap_dua);
                    }
                ],
                [
                    'attribute' => 'foto_ktp',
                    'value' => function (KecelakaanKerja $model) {
                        return Status::activeInactive($model->foto_ktp);
                    }
                ],
                ['attribute' => 'kartu_bpjs',
                    'value' => function (KecelakaanKerja $model) {
                        return Status::activeInactive($model->kartu_bpjs);
                    }
                ],
                ['attribute' => 'absensi',
                    'value' => function (KecelakaanKerja $model) {
                        return Status::activeInactive($model->absensi);
                    }
                ],
                'kronologi:ntext',
                'cedera',
                'penyebab:ntext',
                'preventif:ntext',
                'catatan:ntext',
                [
                    'attribute' => 'perjanjian_kerja_id',
                    'label' => Yii::t('frontend', 'Perjanjian Kerja'),
                    'value' => function (KecelakaanKerja $model) {
                        return $model->perjanjianKerja->namaKontrak . '-' . $model->perjanjianKerja->jenis->nama . '(' . $model->perjanjianKerja->id_pegawai . ')';
                    }
                ],
                [
                    'attribute' => 'pegawai_golongan_id',
                    'label' => Yii::t('frontend', 'Pegawai Golongan'),
                    'value' => function (KecelakaanKerja $model) {
                        return $model->pegawaiGolongan->golongan->nama;
                    }
                ],
                [
                    'attribute' => 'pegawai_struktur_id',
                    'label' => Yii::t('frontend', 'Pegawai Struktur'),
                    'value' => function (KecelakaanKerja $model) {
                        return $model->pegawaiStruktur->struktur->nameCostume;
                    }
                ],
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $exception) {
                Yii::info($exception->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
</div>

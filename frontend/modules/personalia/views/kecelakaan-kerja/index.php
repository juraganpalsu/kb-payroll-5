<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\personalia\models\search\KecelakaanKerjaSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Status;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\personalia\models\KecelakaanKerja;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;


$this->title = Yii::t('frontend', 'Kecelakaan Kerja');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kecelakaan-kerja-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'id_pegawai',
            'label' => Yii::t('app', 'Id Pegawai'),
            'value' => function (KecelakaanKerja $model) {
                return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (KecelakaanKerja $model) {
                return $model->pegawai->nama_lengkap;
            },
        ],
        [
            'attribute' => 'tanggal',
            'value' => function (KecelakaanKerja $model) {
                return date('d-m-Y', strtotime($model->tanggal));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        //
        [
            'attribute' => 'klaim_bpjs',
            'value' => function (KecelakaanKerja $model) {
                return Status::activeInactive($model->klaim_bpjs);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::activeInactive(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('klaim_bpjs'), 'id' => 'grid-kecelakaan-kerja-search-klaim_bpjs']
        ],
        'jam',
        'lokasi',
        ['attribute' => 'berita_acara',
            'value' => function (KecelakaanKerja $model) {
                return Status::activeInactive($model->berita_acara);
            }, 'hidden' => true],
        ['attribute' => 'tahap_satu',
            'value' => function (KecelakaanKerja $model) {
                return Status::activeInactive($model->tahap_satu);
            }, 'hidden' => true],
        ['attribute' => 'tahap_dua',
            'value' => function (KecelakaanKerja $model) {
                return Status::activeInactive($model->tahap_dua);
            }, 'hidden' => true],
        ['attribute' => 'kronologi', 'hidden' => true],
        ['attribute' => 'cedera', 'hidden' => true],
        ['attribute' => 'penyebab', 'hidden' => true],
        ['attribute' => 'preventif', 'hidden' => true],
        ['attribute' => 'catatan', 'hidden' => true],
        [
            'attribute' => 'perjanjian_kerja_id',
            'label' => Yii::t('frontend', 'Perjanjian Kerja'),
            'value' => function (KecelakaanKerja $model) {
                return $model->perjanjianKerja->namaKontrak . '-' . $model->perjanjianKerja->jenis->nama . '(' . $model->perjanjianKerja->id_pegawai . ')';
            }, 'hidden' => true
        ],
        [
            'attribute' => 'pegawai_golongan_id',
            'label' => Yii::t('frontend', 'Pegawai Golongan'),
            'value' => function (KecelakaanKerja $model) {
                return $model->pegawaiGolongan->golongan->nama;
            }, 'hidden' => true
        ],
        [
            'attribute' => 'pegawai_struktur_id',
            'label' => Yii::t('frontend', 'Pegawai Struktur'),
            'value' => function (KecelakaanKerja $model) {
                return $model->pegawaiStruktur->struktur->nameCostume;
            }, 'hidden' => true
        ],
        [
            'attribute' => 'created_by',
            'value' => function (KecelakaanKerja $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (KecelakaanKerja $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'hidden' => true],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
            'noExportColumns' => [1, 14], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'kecelakaan-kerja' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kecelakaan-kerja']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

</div>

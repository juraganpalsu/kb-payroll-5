<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\KecelakaanKerja */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Kecelakaan Kerja',
    ]) . ' ' . $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Kecelakaan Kerja'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="kecelakaan-kerja-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

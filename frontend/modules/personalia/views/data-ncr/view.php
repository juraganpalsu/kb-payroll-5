<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\DataNcr */

$this->title = $model->lembaga_;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Data Ncr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-ncr-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Data Ncr') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                'tanggal_temuan',
                'lembaga',
                'keterangan_temuan:ntext',
                'tanggal_selesai',
                'keterangan_penyelesaian:ntext',
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $exception) {
                Yii::info($exception->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
</div>

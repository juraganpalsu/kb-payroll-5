<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\DataNcr */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Data Ncr',
    ]) . ' ' . $model->lembaga_;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Data Ncr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lembaga_, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="data-ncr-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\DataNcr */

$this->title = Yii::t('frontend', 'Create Data Ncr');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Data Ncr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-ncr-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

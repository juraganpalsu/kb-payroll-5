<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\KegiatanHr */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Kegiatan Hr',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Kegiatan Hr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="kegiatan-hr-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

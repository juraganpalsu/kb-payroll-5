<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\KegiatanHr */

$this->title = Yii::t('frontend', 'Create Kegiatan Hr');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Kegiatan Hr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kegiatan-hr-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

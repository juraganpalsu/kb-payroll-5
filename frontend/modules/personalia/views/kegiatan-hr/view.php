<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\KegiatanHr */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Kegiatan Hr'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kegiatan-hr-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Kegiatan Hr') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                'jenisKegiatan',
                'nama',
                'dari',
                'sampai',
                'peserta:ntext',
                'lokasi',
                'catatan:ntext',
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $exception) {
                Yii::info($exception->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
</div>

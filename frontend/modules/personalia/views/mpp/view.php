<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Mpp */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Mpp'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpp-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Mpp').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'golongan.id',
            'label' => Yii::t('frontend', 'Golongan'),
        ],
        [
            'attribute' => 'jabatan.id',
            'label' => Yii::t('frontend', 'Jabatan'),
        ],
        [
            'attribute' => 'departemen.id',
            'label' => Yii::t('frontend', 'Departemen'),
        ],
        'keterangan:ntext',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Departemen<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnDepartemen = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'kode',
        'catatan',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->departemen,
        'attributes' => $gridColumnDepartemen    ]);
    ?>
    <div class="row">
        <h4>Golongan<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnGolongan = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'urutan',
        'keterangan:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->golongan,
        'attributes' => $gridColumnGolongan    ]);
    ?>
    <div class="row">
        <h4>Jabatan<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnJabatan = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'kode',
        'keterangan:ntext',
        'created_by_org_chart',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->jabatan,
        'attributes' => $gridColumnJabatan    ]);
    ?>
    
    <div class="row">
<?php
if($providerMppAct->totalCount){
    $gridColumnMppAct = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'jumlah',
            [
                'attribute' => 'golongan.id',
                'label' => Yii::t('frontend', 'Golongan')
            ],
            [
                'attribute' => 'jabatan.id',
                'label' => Yii::t('frontend', 'Jabatan')
            ],
                        [
                'attribute' => 'mppJumlah.id',
                'label' => Yii::t('frontend', 'Mpp Jumlah')
            ],
            'keterangan',
            'created_by_pk',
            'created_by_struktur',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMppAct,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mpp-act']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('frontend', 'Mpp Act')),
        ],
        'export' => false,
        'columns' => $gridColumnMppAct
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMppJumlah->totalCount){
    $gridColumnMppJumlah = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'jumlah',
            [
                'attribute' => 'mppTahun.id',
                'label' => Yii::t('frontend', 'Mpp Tahun')
            ],
                        'keterangan:ntext',
            'created_by_pk',
            'created_by_struktur',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMppJumlah,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mpp-jumlah']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('frontend', 'Mpp Jumlah')),
        ],
        'export' => false,
        'columns' => $gridColumnMppJumlah
    ]);
}
?>

    </div>
</div>

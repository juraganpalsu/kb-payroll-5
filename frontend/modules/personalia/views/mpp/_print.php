<?php

/**
 * Created by PhpStorm.
 * File Name: _print.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 05/01/21
 * Time: 21.05
 */

/**
 * @var array $report
 * @var integer $tahun
 */

$headerWithoutPageBreak = $this->renderAjax('_print-header', ['pagebreak' => false, 'tahun' => $tahun]);
$header = $this->renderAjax('_print-header', ['pagebreak' => true, 'tahun' => $tahun]);
echo $header;
?>

<?php foreach ($report['departemen'] as $mppAct) { ?>
    <tr>
        <td colspan="8" rowspan="<?= count($mppAct['golonganjabatan']) + 1 ?>"><?= $mppAct['detail']['nama'] ?></td>
        <td colspan="16">&nbsp;</td>
    </tr>
    <?php
    $totalMpp = 0;
    $totalAct = 0;
    foreach ($mppAct['golonganjabatan'] as $golongan) {
        $jumlahMpp = $golongan['detail']['jumlahMpp'];
        $jumlahAct = $golongan['detail']['jumlahAct'];
        $selisih = $jumlahMpp - $jumlahAct;
        $totalMpp += $jumlahMpp;
        $totalAct += $jumlahAct;
        ?>
        <tr>
            <td colspan="5"><?= $golongan['detail']['namaGolongan'] ?></td>
            <td colspan="5"><?= $golongan['detail']['namaJabatan'] ?></td>
            <td colspan="3" class="center"><?= $jumlahMpp ?></td>
            <td colspan="3" class="center"><?= $jumlahAct ?></td>
            <td colspan="3" class="center"><?= $selisih ?></td>
        </tr>
    <?php }
    ?>
    <tr>
        <td colspan="18" class="right bold"><?= Yii::t('frontend', 'Jumlah') ?></td>
        <td colspan="3" class="center bold"><?= $totalMpp ?></td>
        <td colspan="3" class="center bold"><?= $totalAct ?></td>
        <td colspan="3" class="center bold"><?= $totalMpp - $totalAct ?></td>
    </tr>
    <?php
} ?>
</table>

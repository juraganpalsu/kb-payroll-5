<?php

/**
 * Created by PhpStorm.
 * File Name: index-aktual.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 22/12/20
 * Time: 22.03
 */


/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\personalia\models\search\MppSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Bulan;
use common\models\Golongan;
use frontend\modules\personalia\models\Departemen;
use frontend\modules\personalia\models\Jabatan;
use frontend\modules\personalia\models\MppAct;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Manpower Planing (MPP)');
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
     $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
     
    $('.update-mpp-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);

?>
    <div class="mpp-index">

        <p>
            <?= Html::a(Yii::t('frontend', 'Print Report'), ['print-report-mpp'], ['data' => ['header' => Yii::t('frontend', 'Print Report MPP')], 'class' => 'btn btn-success btn-flat call-modal-btn']) ?>
        </p>
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'departemen_id',
                'label' => Yii::t('frontend', 'Departemen'),
                'value' => function (MppAct $model) {
                    return $model->mppJumlah->mpp->departemen->nama;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(\frontend\modules\struktur\models\Departemen::find()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Departemen', 'id' => 'grid-mpp-search-departemen_id']
            ],
            [
                'attribute' => 'golongan_id',
                'label' => Yii::t('frontend', 'Golongan'),
                'value' => function (MppAct $model) {
                    return $model->golongan->nama;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Golongan', 'id' => 'grid-mpp-search-golongan_id']
            ],
            [
                'attribute' => 'jabatan_id',
                'label' => Yii::t('frontend', 'Jabatan'),
                'value' => function (MppAct $model) {
                    return $model->jabatan->nama;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(\frontend\modules\struktur\models\Jabatan::find()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Jabatan', 'id' => 'grid-mpp-search-jabatan_id']
            ],
            [
                'attribute' => 'tahun',
                'label' => Yii::t('frontend', 'Tahun'),
                'value' => function (MppAct $model) {
                    return $model->mppJumlah->tahun;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Bulan::_tahun(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Tahun', 'id' => 'grid-mpp-search-tahun']
            ],
            [
                'attribute' => 'mpp_jumlah',
                'label' => Yii::t('frontend', 'Jumlah Mpp'),
                'value' => function (MppAct $model) {
                    return $model->mppJumlah->jumlah;
                }
            ],
            [
                'attribute' => 'jumlah',
                'label' => Yii::t('frontend', 'Jumlah Aktual'),
                'value' => function (MppAct $model) {
                    return $model->jumlah;
                }
            ],
            [
                'attribute' => 'selisih',
                'value' => function (MppAct $model) {
                    $selisih = $model->hitungSelisih();
                    if ($selisih < 0) {
                        return Html::tag('strong', $selisih, ['class' => 'text-orange font-weight-bold']);
                    }
                    if ($selisih > 0) {
                        return Html::tag('strong', $selisih, ['class' => 'text-danger font-weight-bold']);
                    }
                    return $selisih;
                },
                'format' => 'raw'
            ],
            'updated_at'
//        'keterangan:ntext',
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [2, 3, 4, 5, 6],
                'noExportColumns' => [1, 7], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'Penyelesaian-kasus' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mpp']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' => Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['update-mpp-aktual'], ['title' => Yii::t('frontend', 'Update MPP'), 'class' => 'btn btn-success btn-flat update-mpp-btn', 'data' => ['pjax' => 0, 'confirm' => Yii::t('frontend', 'Anda yakin akan memperbaharui data MPP?')]])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
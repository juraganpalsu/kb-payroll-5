<?php

/**
 * Created by PhpStorm.
 * File Name: _print-report-form.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 06/01/21
 * Time: 09.37
 */

use common\components\Bulan;
use frontend\modules\personalia\models\form\MppForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var MppForm $model
 */

$js = <<< JS
    $(function() {
        $('#form-input').on('beforeSubmit', function (event) {
            event.preventDefault();
            let form = $(this);
            let btn = $('.submit-btn');
            btn.button('loading');
            if (form.find('.form-group.has-error').length) {
                btn.button('reset');
                return false;
            }
            btn.button('loading');
            $.post(form.attr('action'), form.serialize()).done(function(dt) {
                console.log(dt);
                if(dt.status === true){
                    //location.href = dt.data.url;
                    var win = window.open(dt.data.url, '_blank');
                    win.focus();
                }
            });
            return false;
        });
    });
JS;

$this->registerJs($js);

?>

<div class="row">
    <div class="col-md-6">

        <?php $form = ActiveForm::begin([
            'id' => 'form-input',
            'action' => Url::to(['print-report-mpp']),
            'type' => ActiveForm::TYPE_VERTICAL,
            'formConfig' => ['showErrors' => false],
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['print-report-mpp-validation']),
            'fieldConfig' => ['showLabels' => true],
        ]); ?>




        <?= $form->errorSummary($model); ?>

        <?php try { ?>

            <div class="row">
                <div class="col-sm-4">
                    <?php
                    echo $form->field($model, 'tahun')->dropDownList(Bulan::_tahun());
                    ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('frontend', 'Submit'), ['class' => 'btn btn-primary']) ?>
            </div>

        <?php } catch (Exception $e) {
            print_r($e->getMessage());
            Yii::info($e->getMessage(), 'exception');
        } ?>

        <?php ActiveForm::end(); ?>

    </div>
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Mpp */

$this->title = Yii::t('frontend', 'Create Mpp');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Mpp'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

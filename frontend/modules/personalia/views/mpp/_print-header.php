<?php

/**
 * Created by PhpStorm.
 * File Name: _print-header.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 05/01/21
 * Time: 21.05
 */

/**
 * @var int $tahun
 */
?>


<div class="slip-gaji-index">
    <table width="200mm">
        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="24" class="bold"><?= Yii::t('frontend', 'PT. KOBE BOGA UTAMA') ?></td>
        </tr>
        <tr>
            <td colspan="24"
                class="bold"><?= Yii::t('frontend', 'MPP KARYAWAN TAHUN {tahun} (DATA PER {tanggal})', ['tahun' => $tahun, 'tanggal' => date('d/m/Y')]) ?></td>
        </tr>
        <tr>
            <td colspan="8" class="center head border-bottom"><?= Yii::t('frontend', 'Departemen') ?></td>
            <td colspan="5" class="center head border-bottom"><?= Yii::t('frontend', 'Golongan') ?></td>
            <td colspan="5" class="center head border-bottom"><?= Yii::t('frontend', 'Jabatan') ?></td>
            <td colspan="3" class="center head border-bottom"><?= Yii::t('frontend', 'Jumlah Mpp') ?></td>
            <td colspan="3" class="center head border-bottom"><?= Yii::t('frontend', 'Jumlah Act') ?></td>
            <td colspan="3" class="center head border-bottom"><?= Yii::t('frontend', 'Selisih') ?></td>
        </tr>

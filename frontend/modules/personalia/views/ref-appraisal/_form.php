<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefAppraisal */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="ref-appraisal-form">
    <div class="row">
        <div class="col-md-6">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'kategori')->dropDownList($model->_kategori) ?>
                </div>
            </div>

            <?= $form->field($model, 'kriteria')->textInput(['placeholder' => 'Kriteria']) ?>

            <?= $form->field($model, 'keterangan_kiri')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'keterangan_kanan')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>


</div>

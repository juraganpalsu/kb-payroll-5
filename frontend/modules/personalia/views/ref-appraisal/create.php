<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefAppraisal */

$this->title = Yii::t('frontend', 'Create Ref Appraisal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Appraisal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-appraisal-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

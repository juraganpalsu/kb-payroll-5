<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefAppraisal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Appraisal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-appraisal-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Ref Appraisal') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            'kategori',
            'kriteria',
            'keterangan_kiri:ntext',
            'keterangan_kanan:ntext',
            'created_by_pk',
            'created_by_struktur',
            ['attribute' => 'lock', 'visible' => false],
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>

    <div class="row">
        <?php
        if ($providerAppraisalDetail->totalCount) {
            $gridColumnAppraisalDetail = [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'id', 'visible' => false],
                'komentar:ntext',
                'nilai',
                [
                    'attribute' => 'appraisal.id',
                    'label' => Yii::t('frontend', 'Appraisal')
                ],
                'created_by_pk',
                'created_by_struktur',
                ['attribute' => 'lock', 'visible' => false],
            ];
            echo Gridview::widget([
                'dataProvider' => $providerAppraisalDetail,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-appraisal-detail']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('frontend', 'Appraisal Detail')),
                ],
                'export' => false,
                'columns' => $gridColumnAppraisalDetail
            ]);
        }
        ?>

    </div>
</div>

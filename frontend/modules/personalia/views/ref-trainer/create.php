<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefTrainer */

$this->title = Yii::t('frontend', 'Create Ref Trainer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Trainer'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-trainer-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

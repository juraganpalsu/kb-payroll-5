<?php

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefTrainer */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Ref Trainer',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Trainer'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="ref-trainer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\personalia\models\search\ResignSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Status;
use frontend\modules\personalia\models\Resign;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Resign');
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Personalia');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resign-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'departemen',
            'value' => function (Resign $model) {
                if ($pegawaiStruktur = $model->pegawaiStruktur) {
                    return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : '';
                }
                return '';
            },
            'hidden' => true
        ],
        [
            'attribute' => 'jabatan',
            'value' => function (Resign $model) {
                if ($pegawaiStruktur = $model->pegawaiStruktur) {
                    return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->jabatan ? $pegawaiStruktur->struktur->jabatan->nama : '' : '' : '';
                }
                return '';
            },
            'hidden' => true
        ],
        [
            'attribute' => 'fungsi_jabatan',
            'value' => function (Resign $model) {
                if ($pegawaiStruktur = $model->pegawaiStruktur) {
                    return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->fungsiJabatan ? $pegawaiStruktur->struktur->fungsiJabatan->nama : '' : '' : '';
                }
                return '';
            },
            'hidden' => true
        ],
        [
            'attribute' => 'id_pegawai',
            'value' => function (Resign $model) {
                return $model->perjanjianKerja->id_pegawai;
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (Resign $model) {
                return $model->pegawai->nama_lengkap;
            },
        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (Resign $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            }
        ],
        [
            'attribute' => 'golongan',
            'value' => function (Resign $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            }
        ],
        [
            'attribute' => 'tanggal',
            'value' => function (Resign $model) {
                return date('d-m-Y', strtotime($model->tanggal));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'jenis_kontrak',
            'value' => function (Resign $model) {
                return $model->perjanjianKerja->jenis->nama;
            },
            'hidden' => true
        ],
        [
            'attribute' => 'area_kerja',
            'value' => function (Resign $model) {
                return $model->areaKerja->refAreaKerja->name;
            },
            'hidden' => true
        ],
        [
            'attribute' => 'tipe',
            'value' => function (Resign $model) {
                return $model->tipe_;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $searchModel->_tipe,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('tipe'), 'id' => 'grid-resign-search-tipe'],
        ],
        'alasan:ntext',
        [
            'attribute' => 'exit_letter',
            'value' => function (Resign $model) {
                return Status::activeInactive($model->exit_letter);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::activeInactive(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('exit_letter'), 'id' => 'grid-resign-search-exit_letter'],
        ],
        [
            'attribute' => 'exit_clearence',
            'value' => function (Resign $model) {
                return Status::activeInactive($model->exit_clearence);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Status::activeInactive(0, true),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('exit_clearence'), 'id' => 'grid-resign-search-exit_clearence'],
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14,15],
            'noExportColumns' => [1, 16], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'Resign' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-resign']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'excetion');
    }
    ?>

</div>
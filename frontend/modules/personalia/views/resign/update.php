<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Resign */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Resign',
    ]) . ' ' . $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Resign'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="resign-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

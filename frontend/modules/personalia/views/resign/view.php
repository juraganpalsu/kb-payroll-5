<?php

use common\components\Status;
use frontend\modules\personalia\models\Resign;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Resign */

$this->title = $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Resign'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resign-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Resign') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) ?>
            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'tanggal',
                    'value' => function (Resign $model) {
                        return date('d-m-Y', strtotime($model->tanggal));
                    }
                ],
                'tipe_',
                'alasan:ntext',
                [
                    'attribute' => 'exit_letter',
                    'value' => function (Resign $model) {
                        return Status::activeInactive($model->exit_letter);
                    }
                ],
                [
                    'attribute' => 'exit_clearence',
                    'value' => function (Resign $model) {
                        return Status::activeInactive($model->exit_clearence);
                    }
                ],
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
</div>

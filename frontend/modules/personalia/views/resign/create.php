<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Resign */

$this->title = Yii::t('frontend', 'Create Resign');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Resign'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resign-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

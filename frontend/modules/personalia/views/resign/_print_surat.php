<?php

use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\Resign;
use yii\helpers\Html;
use common\components\Bulan;

/**
 * @var Resign $model
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
?>
<div class="slip-gaji-index">
    <table width="200mm">
        <tr>
            <td colspan="26" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 16pt; text-decoration:underline; font-weight: bold; ">
                SURAT KETERANGAN
            </td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 12pt; font-weight: bold; ">
                No...../HCD-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->tanggal))) . '/' . date('Y', strtotime($model->tanggal)) ?>
            </td>
        </tr>
        <br><br>
        <br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="22">Yang bertanda tangan dibawah ini atas nama Pimpinan Perusahaan <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                } else {
                    echo 'PT BINA CIPTA ABADI';
                }
                ?> yang beralamat di Jl. Manis Raya No.15 Curug Tangerang, dengan ini menerangkan bahwa :
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'NIK') ?></td>
            <td colspan="6" class="bold">
                : <?= $perjanjianKerja ? str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : ''; ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Nama Lengkap') ?></td>
            <td colspan="12" class="bold"> : <?= $model->pegawai->nama_lengkap; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Tempat/Tgl Lahir ') ?></td>
            <td colspan="12" class="bold">
                : <?= ucwords(strtolower($model->pegawai->tempat_lahir)) . ', ' . Bulan::tanggal($model->pegawai->tanggal_lahir); ?></td>
        </tr>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <br>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="22">
                Adalah karyawan <?php
                if ($perjanjianKerja->kontrak == 1) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == 2) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                } else {
                    echo 'PT BINA CIPTA ABADI';
                }
                ?>
                terhitung sejak tanggal <strong><?= Bulan::tanggal($model->tglAwalMk()); ?></strong> sampai dengan
                tanggal
                <strong><?= Bulan::tanggal($model->tanggal); ?></strong> dan terhitung sejak tanggal
                <strong><?= Bulan::tanggal($model->tanggal . '+1 day'); ?></strong> yang bersangkutan sudah
                tidak bekerja lagi di Perusahaan kami
                dikarenakan <?= $model->tipe != Resign::PENSIUN ? 'telah mengundurkan diri.' : 'telah memasuki usia pensiun.' ?>
            </td>
        </tr>
        <br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="22">
                Selama bekerja pada perusahaan ini yang bersangkutan telah melaksanakan tugas dengan baik, atas jasa dan
                pengabdiannya, kami sampaikan terimakasih.
            </td>
        </tr>
        <br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="22">Demikian surat keterangan ini dibuat dengan sebenarnya, dan agar dapat
                dipergunakan dengan sebagaimana mestinya.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td colspan="10">
                <?= Yii::t('frontend', 'Tangerang, ') . Bulan::tanggal($model->tanggal) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" style="font-size: 11pt">
                <?php
                if ($perjanjianKerja->kontrak == 1) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == 2) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                } else {
                    echo 'PT BINA CIPTA ABADI';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <br><br>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content" style="text-decoration: underline; font-weight: bold;">
                <br><br><br><br>
                <?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content">
                <?= $model->olehPegawaiStruktur ? $model->olehPegawaiStruktur->struktur->name : ''; ?>
            </td>
            <td colspan="2"></td>
        </tr>

    </table>
</div>
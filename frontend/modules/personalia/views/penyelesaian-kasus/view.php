<?php

use frontend\modules\personalia\models\PenyelesaianKasus;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\PenyelesaianKasus */

$this->title = $model->masalah;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Penyelesaian Kasus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penyelesaian-kasus-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Penyelesaian Kasus') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'pegawai_ids',
                    'label' => Yii::t('frontend', 'Pegawai'),
                    'value' => function (PenyelesaianKasus $model) {
                        return $model->namaPegawai;
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'tanggal_kasus',
                    'value' => function (PenyelesaianKasus $model) {
                        return date('d-m-Y', strtotime($model->tanggal_kasus));
                    },
                ],
                'masalah:ntext',
                [
                    'attribute' => 'tanggal_selesai',
                    'value' => function (PenyelesaianKasus $model) {
                        return date('d-m-Y', strtotime($model->tanggal_selesai));
                    },
                ],
                'keterangan:ntext',
//                [
//                    'attribute' => 'perjanjian_kerja_id',
//                    'label' => Yii::t('frontend', 'Perjanjian Kerja'),
//                    'value' => function (PenyelesaianKasus $model) {
//                        return $model->perjanjianKerja->namaKontrak . '-' . $model->perjanjianKerja->jenis->nama . '(' . $model->perjanjianKerja->id_pegawai . ')';
//                    }
//                ],
//                [
//                    'attribute' => 'pegawai_golongan_id',
//                    'label' => Yii::t('frontend', 'Pegawai Golongan'),
//                    'value' => function (PenyelesaianKasus $model) {
//                        return $model->pegawaiGolongan->golongan->nama;
//                    }
//                ],
//                [
//                    'attribute' => 'pegawai_struktur_id',
//                    'label' => Yii::t('frontend', 'Pegawai Struktur'),
//                    'value' => function (PenyelesaianKasus $model) {
//                        return $model->pegawaiStruktur->struktur->nameCostume;
//                    }
//                ],
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $exception) {
                Yii::info($exception->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>

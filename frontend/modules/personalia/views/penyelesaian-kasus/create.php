<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\PenyelesaianKasus */

$this->title = Yii::t('frontend', 'Create Penyelesaian Kasus');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Penyelesaian Kasus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penyelesaian-kasus-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

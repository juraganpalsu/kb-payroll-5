<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\personalia\models\search\PenyelesaianKasusSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\personalia\models\PenyelesaianKasus;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$this->title = Yii::t('frontend', 'Penyelesaian Kasus');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="penyelesaian-kasus-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'pegawai_ids',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (PenyelesaianKasus $model) {
                return $model->namaPegawai;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Pegawai::find()->joinWith('perjanjianKerja')->all(), 'id', 'namaIdPegawai'),
            'filterWidgetOptions' => [
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/pegawai/pegawai/get-pegawai-aktif-for-select2']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term};}')
                    ],
                ],
            ],
            'filterInputOptions' => ['placeholder' => 'Pegawai', 'id' => 'grid-kecelakaan-kerja-search-pegawai_ids'],
            'format' => 'raw'
        ],
        [
            'attribute' => 'tanggal_kasus',
            'value' => function (PenyelesaianKasus $model) {
                return $model->tanggal_kasus ? date('d-m-Y', strtotime($model->tanggal_kasus)) : '';
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        'masalah:ntext',
        [
            'attribute' => 'tanggal_selesai',
            'value' => function (PenyelesaianKasus $model) {
                return $model->tanggal_selesai ? date('d-m-Y', strtotime($model->tanggal_selesai)) : '';
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        'keterangan:ntext',
        [
            'attribute' => 'created_by',
            'value' => function (PenyelesaianKasus $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (PenyelesaianKasus $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6],
            'noExportColumns' => [1, 7], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'Penyelesaian-kasus' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-penyelesaian-kasus']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

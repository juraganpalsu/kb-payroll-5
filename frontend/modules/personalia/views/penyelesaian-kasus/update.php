<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\PenyelesaianKasus */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Penyelesaian Kasus',
    ]) . ' ' . $model->masalah;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Penyelesaian Kasus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->masalah, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="penyelesaian-kasus-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

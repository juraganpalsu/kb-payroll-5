<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\personalia\models\search\TrainingSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Status;
use frontend\modules\personalia\models\RefNamaInstitusi;
use frontend\modules\personalia\models\RefNamaPelatihan;
use frontend\modules\personalia\models\RefTrainer;
use frontend\modules\personalia\models\Training;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('frontend', 'Training');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'jenis_pelatihan',
            'value' => function (Training $model) {
                return $model->jenisPelatihan;
            }
        ],
        [
            'attribute' => 'tanggal_pelaksanaan',
            'value' => function (Training $model) {
                return date('d-m-Y', strtotime($model->tanggal_pelaksanaan));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        'durasi',
        'nilai_tes',
        [
            'attribute' => 'sertifikat',
            'value' => function (Training $model) {
                return Status::activeInactive($model->sertifikat);
            }
        ],
        [
            'attribute' => 'ref_nama_pelatihan_id',
            'label' => Yii::t('frontend', 'Ref Nama Pelatihan'),
            'value' => function (Training $model) {
                return $model->refNamaPelatihan->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(RefNamaPelatihan::find()->asArray()->all(), 'id', 'id'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Ref nama pelatihan', 'id' => 'grid-training-search-ref_nama_pelatihan_id']
        ],
        [
            'attribute' => 'ref_trainer_id',
            'label' => Yii::t('frontend', 'Ref Trainer'),
            'value' => function (Training $model) {
                return $model->refTrainer ? $model->refTrainer->nama : $model->daftarNamaTrainer();
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(RefTrainer::find()->asArray()->all(), 'id', 'id'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Ref trainer', 'id' => 'grid-training-search-ref_trainer_id'],
            'format' => 'raw'
        ],
        [
            'attribute' => 'ref_nama_institusi_id',
            'label' => Yii::t('frontend', 'Ref Nama Institusi'),
            'value' => function (Training $model) {
                return $model->refNamaInstitusi->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(RefNamaInstitusi::find()->asArray()->all(), 'id', 'id'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Ref nama institusi', 'id' => 'grid-training-search-ref_nama_institusi_id']
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10],
            'noExportColumns' => [1, 11], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'training' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-training']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

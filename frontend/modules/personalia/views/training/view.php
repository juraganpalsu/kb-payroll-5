<?php

use common\components\Status;
use frontend\modules\personalia\models\Training;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Training */

$this->title = $model->jenisPelatihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Training'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Training') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'tanggal_pelaksanaan',
                    'value' => function (Training $model) {
                        return date('d-m-Y', strtotime($model->tanggal_pelaksanaan));
                    }
                ],
                'durasi',
                'nilai_tes',
                [
                    'attribute' => 'sertifikat',
                    'value' => function (Training $model) {
                        return Status::activeInactive($model->sertifikat);
                    }
                ],
                [
                    'attribute' => 'refNamaPelatihan.nama',
                    'label' => Yii::t('frontend', 'Ref Nama Pelatihan'),
                ],
                [
                    'attribute' => 'refNamaInstitusi.nama',
                    'label' => Yii::t('frontend', 'Ref Nama Institusi'),
                ],
                [
                    'attribute' => 'ref_trainer_id',
                    'label' => Yii::t('frontend', 'Ref Trainer'),
                    'value' => function (Training $model) {
                        if ($model->jenis_pelatihan == Training::INTERNAL) {
                            return $model->daftarNamaTrainer();
                        }
                        if ($model->jenis_pelatihan == Training::EKSTERNAL) {
                            return $model->refTrainer->nama;
                        }
                        return '';
                    }
                ],
                [
                    'label' => Yii::t('frontend', 'Peserta'),
                    'value' => function (Training $model) {
                        return $model->daftarNamaPegawai();
                    }
                ],
                ['attribute' => 'lock', 'visible' => false],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
</div>

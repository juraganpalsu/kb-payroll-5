<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Training */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Training',
    ]) . ' ' . $model->jenisPelatihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Training'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jenisPelatihan, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="training-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

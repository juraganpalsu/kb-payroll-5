<?php

use common\components\Status;
use frontend\modules\personalia\models\RefNamaInstitusi;
use frontend\modules\personalia\models\RefNamaPelatihan;
use frontend\modules\personalia\models\RefTrainer;
use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Training */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
$(function() {
    $('#form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });    
})
JS;

$this->registerJs($js);
?>

<div class="training-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'jenis_pelatihan')->dropDownList($model->_jenisPelatihan) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'tanggal_pelaksanaan')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ],
//                        'pluginEvents' => [
//                            'changeDate' => 'function(e) { $("#training-peserta").val(null).triger("change")}',
//                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'durasi')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_TIME,
                    'saveFormat' => 'php:H:i:s',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('frontend', 'Choose Durasi'),
                            'autoclose' => true
                        ]
                    ]
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'nilai_tes')->textInput(['placeholder' => 'Nilai Tes']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'sertifikat')->dropDownList(Status::activeInactive(0, true)) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'ref_nama_pelatihan_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(RefNamaPelatihan::find()->orderBy('id')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Ref nama pelatihan')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'ref_nama_institusi_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(RefNamaInstitusi::find()->orderBy('id')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Ref nama institusi')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'ref_trainer_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(RefTrainer::find()->orderBy('id')->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('frontend', 'Choose Ref trainer(Diisi Jika EXTERNAL)')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->hint(Yii::t('frontend', '*) Diisi jika jenis pelatihan EKSTERNAL.'), ['class' => 'text-danger small mark']); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'trainers')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai(Diisi Jika INTERNAL)'),
                        'multiple' => true,
                    ],
                    'data' => $model->isNewRecord ? [] : $model->loadTrainerSaatUpdate(),
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, tanggal:$("#training-tanggal_pelaksanaan").val()};}')
                        ],
                    ],
                ])->hint(Yii::t('frontend', '*)  Diisi jika jenis pelatihan INTERNAL.'), ['class' => 'text-danger small mark']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'pegawai_ids')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => true,
                    ],
                    'data' => $model->isNewRecord ? [] : $model->loadPegawaiSaatUpdate(),
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, tanggal:$("#training-tanggal_pelaksanaan").val()};}')
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>
    <?php ActiveForm::end(); ?>

</div>

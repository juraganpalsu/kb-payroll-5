<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Training */

$this->title = Yii::t('frontend', 'Create Training');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Training'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

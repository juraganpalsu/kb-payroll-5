<?php

use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use yii\helpers\Html;
use common\components\Bulan;

/**
 * @var SuratPegawai $model
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$pegawaiArea = $model->areaKerja;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
?>
<div class="slip-gaji-index">
    <table width="200mm">
        <tr>
            <td colspan="26" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 16pt; text-decoration:underline; font-weight: bold; ">
                SURAT KEPUTUSAN
            </td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 12pt; font-weight: bold; ">
                No.
                <?= substr($model->seq_code, -3) ?>
                /SPT/HC-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->created_at))) . '/' . date('Y', strtotime($model->created_at)) ?>
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="26" class="center" style="font-size: 12pt;">
                Tentang<br>
                <div style="font-size: 14pt; font-weight: bold">Pengangkatan</div>
            </td>
        </tr>
        <br>
        <tr>
            <td colspan="2"></td>
            <td colspan="4">Menimbang :
            </td>
            <td class="text-align-top">1.</td>
            <td class="content" colspan="14">
                Hasil Penilaian Saudara dinilai cukup baik.
            </td>
        </tr>
        <tr>
            <td colspan="6"></td>
            <td class="text-align-top">2.</td>
            <td class="content" colspan="16">
                Evaluasi Direksi berdasarkan Performa Appraisal Saudara
                sebagai <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?>
                cukup baik, dan
                diharapkan mampu mempertahankan serta lebih meningkatkan Performa kerja dikemudian hari.
            </td>
        </tr>
        <tr>
            <td colspan="6"></td>
            <td class="text-align-top">3.</td>
            <td class="content" colspan="18">
                Berdasarkan hal tersebut maka Perusahaan :
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="21"><br>
                Memutuskan : Terhitung mulai tanggal <?= Bulan::tanggal($model->tgl_mulai) ?> <br>
                Mengangkat :
            </td>
        </tr>
        <tr>
            <td colspan="6"></td>
            <td colspan="4" class="bold"><?= Yii::t('frontend', 'NIK') ?></td>
            <td colspan="12" class="bold">
                : <?= $perjanjianKerja ? str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : ''; ?></td>
        </tr>
        <tr>
            <td colspan="6"></td>
            <td colspan="4" class="bold text-align-top"><?= Yii::t('frontend', 'Nama') ?></td>
            <td colspan="12" class="bold"> : <?= $model->pegawai->nama_lengkap; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="6"></td>
            <td colspan="4" class="bold text-align-top"><?= Yii::t('frontend', 'TTL ') ?></td>
            <td colspan="12" class="bold">
                : <?= ucwords(strtolower($model->pegawai->tempat_lahir)) . ', ' . Bulan::tanggal($model->pegawai->tanggal_lahir); ?></td>
        </tr>
        <tr class="bold">
            <td colspan="6"></td>
            <td colspan="4" class="bold text-align-top"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="6"></td>
            <td colspan="4" class="bold text-align-top"><?= Yii::t('frontend', 'Departemen ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : ''; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="6"></td>
            <td colspan="4" class="bold text-align-top"><?= Yii::t('frontend', 'Area Kerja ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiArea ? $pegawaiArea->refAreaKerja->name : ''; ?></td>
        </tr>
        <br><br>
        <tr class="bold">
            <td colspan="2"></td>
            <td class="content" colspan="21">
                Menjadi <strong>KARYAWAN TETAP</strong> <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                }
                ?>. <br>
                Saudara harus menjalankan tugas dan kewajiban sesuai dengan Job Description dan mematuhi semua Peraturan
                Perusahaan.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="21">
                Demikian Surat Keputusan ini dibuat dan diharapkan dengan pengangkatan ini Saudara dapat meningkatkan
                prestasi kerja di masa yang akan datang.
            </td>
        </tr>
        <br> <br>
        <tr>
            <td colspan="2"></td>
            <td colspan="10">
                <?= Yii::t('frontend', 'Tangerang, ') . Bulan::tanggal($model->tgl_mulai) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" style="font-size: 11pt">
                <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <br>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content">
                <br><br><br><br>
                <div style="font-weight: bold;text-decoration: underline;">
                    <?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?>
                </div>
                Direksi
            </td>
        </tr>

    </table>
</div>
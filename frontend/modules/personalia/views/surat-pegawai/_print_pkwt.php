<?php

use common\components\Bulan;
use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use yii\helpers\Html;

/**
 * @var SuratPegawai $model
 */
/* @var $this yii\web\View */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$pegawaiAreaKerja = $model->areaKerja;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
?>
<div>
    <table width="200mm" style="page-break-after: always">
        <tr>
            <td class="font-pkwt" colspan="24" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="24" class="center" style="font-size: 16pt; text-decoration:underline; font-weight: bold; ">
                PERJANJIAN KERJA WAKTU TERTENTU
            </td>
        </tr>
        <tr>
            <td colspan="24" class="center" style="font-size: 16pt; font-weight: bold; ">
                <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                } else {
                    echo 'PT BINA CIPTA ABADI';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="24" class="center" style="font-size: 12pt; font-weight: bold; ">
                No.
                <?= substr($model->seq_code, -3) ?>
                /PKWT/HC-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->created_at))) . '/' . date('Y', strtotime($model->created_at)) ?>
            </td>
        </tr>
        <tr>
            <td class="font-pkwt" colspan="24" style="border-top: 1px solid">
            </td>
        </tr>
        <tr>
            <td class="font-pkwt text-align-top" colspan="12">
                <table>
                    <tr>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">Pada hari ini
                            <strong><?= Helper::idDay($model->tgl_mulai) ?></strong> tanggal
                            <?= Bulan::tanggal($model->perjanjianKerja->mulai_berlaku) ?>.
                            Yang bertanda tangan di bawah ini :
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="7">Nama</td>
                        <td class="bold text-align-top">:</td>
                        <td class="font-pkwt bold"
                            colspan="17"><?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="7">Jabatan</td>
                        <td class="bold text-align-top">:</td>
                        <td class="font-pkwt bold"
                            colspan="17"><?= $model->olehPegawaiStruktur ? $model->olehPegawaiStruktur->struktur->name : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt text-align-top" colspan="7">Alamat</td>
                        <td class="bold text-align-top">:</td>
                        <td class="font-pkwt bold" colspan="17">Kawasan Industri Manis<br>
                            Jl. Manis Raya No. 15 Kel. Kadu Kec. Curug Kab. Tangerang
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">Bertindak untuk dan atas nama PT KOBE BOGA UTAMA, Selanjutnya
                            disebut sebagai
                            <strong>PIHAK PERTAMA.</strong></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="7">Nama</td>
                        <td class="bold text-align-top">:</td>
                        <td class="font-pkwt bold" colspan="17"><?= $model->pegawai->nama_lengkap; ?></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="7">Jenis Kelamin</td>
                        <td class="bold text-align-top">:</td>
                        <td class="font-pkwt bold"
                            colspan="17"><?= ucwords(strtolower($model->pegawai->_jenis_kelamin[$model->pegawai->jenis_kelamin])); ?></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="7">TTL</td>
                        <td class="bold text-align-top">:</td>
                        <td class="font-pkwt bold"
                            colspan="17"><?= ucwords(strtolower($model->pegawai->tempat_lahir)) . ', ' . Bulan::tanggal($model->pegawai->tanggal_lahir); ?></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="7">Nomor KTP</td>
                        <td class="bold text-align-top">:</td>
                        <td class="font-pkwt bold" colspan="17"><?= $model->pegawai->nomor_ktp; ?></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt text-align-top" colspan="7">Alamat</td>
                        <td class="bold text-align-top">:</td>
                        <td class="font-pkwt bold"
                            colspan="17"><?= ucwords(strtolower($model->pegawai->ktp_alamat)) . ' Rt/Rw ' . $model->pegawai->ktp_rt . ' / ' . $model->pegawai->ktp_rw ?>
                            Kel. <?= $model->pegawai->pegawaiKtpDesa ? ucwords(strtolower($model->pegawai->pegawaiKtpDesa->nama)) : '-' ?>
                            Kec. <?= $model->pegawai->pegawaiKtpKecamatan ? ucwords(strtolower($model->pegawai->pegawaiKtpKecamatan->nama)) : '-' ?>
                            Kab. <?= $model->pegawai->pegawaiKtpKabupaten ? ucwords(strtolower($model->pegawai->pegawaiKtpKabupaten->nama)) : '-' ?>
                            Prov. <?= $model->pegawai->pegawaiKtpProvinsi ? ucwords(strtolower($model->pegawai->pegawaiKtpProvinsi->nama)) : '-'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content" colspan="24">Bertindak untuk dan atas nama sendiri, yang
                            selanjutnya
                            disebut sebagai <strong>PIHAK KEDUA.</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content" colspan="24">
                            Dengan ini kedua belah pihak menyatakan telah setuju dan
                            bersepakat untuk mengadakan Perjanjian Kerja Waktu
                            Tertentu (PKWT) dengan ketentuan dan syarat-syarat sebagai
                            berikut:
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PASAL 1</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">RUANG LINGKUP PEKERJAAN DAN WAKTU KERJA</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content" colspan="24"><strong>PIHAK PERTAMA</strong> menyetujui memberikan
                            pekerjaan kepada <strong>PIHAK
                                KEDUA</strong> sebagai Tenaga Kerja Waktu Tertentu,
                            <strong>PIHAK KEDUA</strong> menerima pekerjaan dari <strong>PIHAK PERTAMA</strong> dengan
                            kewajiban dan tugas-tugas sebagai berikut:
                        </td>
                    </tr>
                </table>

            </td>
            <td class="font-pkwt text-align-top" colspan="12">
                <table>
                    <tr>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="7">Jabatan</td>
                        <td class="font-pkwt bold text-align-top">:</td>
                        <td class="font-pkwt bold"
                            colspan="17"><?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="7">Penempatan</td>
                        <td class="font-pkwt bold text-align-top">:</td>
                        <td class="font-pkwt bold"
                            colspan="17"><?= $model->pegawai ? $pegawaiAreaKerja ? ucwords(strtolower($pegawaiAreaKerja->refAreaKerja->name)) : '' : '' ?></td>
                    </tr>
                    <tr>
                        <td class="font-pkwt text-align-top" colspan="12">Bertanggung-jawab kepada :</td>
                        <td class="font-pkwt bold" colspan="12"><?php
                            if ($queryAtasanLangsung = $pegawaiStruktur->struktur->parents(1)->one()) {
                                echo $queryAtasanLangsung->nameCostume;
                            }
                            echo '';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PASAL 2</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">JANGKA WAKTU</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">Perjanjian Kerja Waktu Tertentu ini berlangsung mulai tanggal
                            <?= Bulan::tanggal($model->perjanjianKerja->mulai_berlaku) ?> sampai dengan
                            tanggal <?= $model->perjanjianKerja->akhir_berlaku ? Bulan::tanggal($model->perjanjianKerja->akhir_berlaku) : '' ?>
                            .
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PASAL 3</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PELAKSANAAN TUGAS</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">1.</td>
                        <td class="font-pkwt content" colspan="22">
                            <strong>PIHAK KEDUA</strong> wajib hadir dan bekerja tepat pada waktunya sesuai dengan
                            ketentuan
                            jam kerja yang diatur oleh <strong>PIHAK PERTAMA</strong>, selama waktu kerja wajib berada
                            di tempat kerja
                            dan tidak diperkenankan menerima tamu pribadi tanpa sepengetahuan / seijin Atasan.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td colspan="2" class="font-pkwt content text-align-top">2.</td>
                        <td colspan="22" class="font-pkwt content">
                            <strong>PIHAK KEDUA</strong> dalam melaksanakan tugas wajib tunduk kepada peraturan,
                            petunjuk
                            dan prosedur-prosedur kerja yang berlaku, baik yang telah ada maupun yang akan dibuat
                            kemudian.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td colspan="2" class="font-pkwt content text-align-top">3.</td>
                        <td colspan="22" class="font-pkwt content">
                            <strong>PIHAK KEDUA</strong> berkewajiban memelihara peralatan/ fasilitas kerja yang
                            digunakan.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td colspan="2" class="font-pkwt content text-align-top">4.</td>
                        <td colspan="22" class="font-pkwt content">
                            Kehilangan dan tahu kerusakan fasilitas kerja yang disengaja akan dikenakan sanksi sesuai
                            dengan peraturan
                            perusahaan.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="200mm" style="page-break-after: always">
        <tr>
            <td colspan="24" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td class="font-pkwt text-align-top" colspan="12">
                <table>
                    <tr>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PASAL 4</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PELAKSANAAN TUGAS</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">1.</td>
                        <td class="font-pkwt content" colspan="22">
                            Atas pekerjaan yang dilakukan oleh <strong>PIHAK KEDUA</strong> selama berlakunya secara
                            efektif Perjanjian
                            Kerja Waktu Tertentu ini, Perusahaan akan memberikan kompensasi penghasilan yang diperoleh
                            setiap bulannya seperti yang tercantum dalam lampiran Perjanjian Kerja Waktu Tertentu yang
                            merupakan bagian yang
                            tak terpisahkan dari Perjanjian Kerja Waktu Tertentu ini.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">2.</td>
                        <td class="font-pkwt content" colspan="22">
                            Sehubungan dengan tugas dan tanggungjawab <strong>PIHAK KEDUA</strong> yang waktukerjanya
                            tidak dapat dibatasi sebagaimana
                            waktukerja yang telah ditetapkan pada pasal 1 Ayat 3, maka <strong>PIHAK KEDUA</strong>
                            menyatakan memahami dan menerima
                            bahwa upah yang diperoleh sudah mencakup keseluruhan waktukerja yang dijalani.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">3.</td>
                        <td class="font-pkwt content" colspan="22">
                            <strong>PIHAK KEDUA</strong> tidak berhak menerima pembayaran & fasilitas lainnya dalam
                            bentuk apapun
                            selain dari apa yang telah ditetapkan pada lampiran Perjanjian Kerja Waktu Tertentu yang
                            merupakan bagian yang tidak
                            terpisahkan dari Perjanjian Kerja Waktu Tertentu ini.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PASAL 5</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">HAK DAN KEWAJIBAN</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">1.</td>
                        <td class="font-pkwt content" colspan="22">
                            Apabila <strong>PIHAK KEDUA</strong> tidak masuk bekerja karena sakit, wajib memberitahukan
                            secara tertulis dan disertai dengan
                            Surat Keterangan Dokter.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">2.</td>
                        <td class="font-pkwt content" colspan="22">
                            Apabila <strong>PIHAK KEDUA</strong> tidak masuk bekerja selama 5 (lima) hari berturut-turut
                            tanpa adanya pemberitahuan atau Surat Keterangan
                            Dokter atau alasan lainnya yang sah, maka <strong>PIHAK KEDUA</strong> dianggap telah
                            mengundurkan diri secara sepihak.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">3.</td>
                        <td class="font-pkwt content" colspan="22">
                            Apabila <strong>PIHAK KEDUA</strong> dalam melaksakan pekerjaannya terbukti mengakibatkan
                            kecelakaan atau merugikan <strong>PIHAK KETIGA</strong>
                            (orang lain) mengenai jiwa atau harta yang disebabkan kesalahan ataupun kelalaian <strong>PIHAK
                                KEDUA</strong>,
                        </td>
                    </tr>
                </table>

            </td>
            <td class="font-pkwt" colspan="12" class="text-align-top">
                <table>
                    <tr>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2"></td>
                        <td class="font-pkwt content" colspan="21">
                            maka <strong>PIHAK KEDUA</strong>
                            bertanggungjawab sepenuhnya atas ganti rugi yang wajib diberikan kepada <strong>PIHAK
                                KETIGA</strong> (orang lain) sesuai dengan
                            biaya yang dikeluarkan oleh <strong>PIHAK KETIGA</strong> (orang lain).
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">4.</td>
                        <td class="font-pkwt content" colspan="21">
                            <strong>PIHAK KEDUA</strong> tidak diperkenankan mengikatkan diri secara penuh atau paruh
                            waktu dengan perusahaan atau pihak
                            lain tanpa seizin dari Direksi.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">5.</td>
                        <td class="font-pkwt content" colspan="21">
                            <strong>PIHAK KEDUA</strong> bersedia dipindah-tugaskan kebagian atau departemen lain pada
                            perusahan atau cabang lain sesuai kebuutuhan perusahaan.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">6.</td>
                        <td class="font-pkwt content" colspan="21">
                            <strong>PIHAK KEDUA</strong> bersedia untuk kerja malam / shift jika diperlukan oleh
                            <strong>PIHAK PERTAMA</strong>.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">7.</td>
                        <td class="font-pkwt content" colspan="21">
                            <strong>PIHAK KEDUA</strong> bersedia melakukan pemeriksaan kesehatan seperti yang
                            disyaratkan oleh <strong>PIHAK PERTAMA</strong> dan jika hasil
                            pemeriksaan kesehatan tersebut tidak memenuhi syarat maka Perjanjian Kerja Waktu Tertentu
                            ini secara otomatis dinyatakan batal.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PASAL 6</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">BERAKHIRNYA HUBUNGAN KERJA</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">1.</td>
                        <td class="font-pkwt content" colspan="21">
                            Perjanjian Kerja Waktu Tertentu ini akan gugur dengan sendirinya dengan berakhirnya masa
                            perjanjian
                            dan dapat ditinjau kembali dengan kesepakatan kedua belah pihak.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">2.</td>
                        <td class="font-pkwt content" colspan="21">
                            Perjanjian kerja ini dapat di akhiri secara sepihak oleh <strong>PIHAK PERTAMA</strong>
                            tanpa adanya
                            ganti rugi / uang jasa atau imbalan lainnya kecuali upah sampai dengan tanggal terakhir
                            bekerja,
                            apabila PIHAK KEDUA melakukan perbuatan atau pelanggaran sebagai berikut:
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt content text-align-top" colspan="1">•</td>
                        <td class="font-pkwt content" colspan="20">
                            Penipuan, mencuri/ mencoba mencuri, menggelapkan barang/ uang milik perusahaan atau teman
                            sekerja.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="200mm">
        <tr>
            <td colspan="24" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td class="font-pkwt text-align-top" colspan="12">
                <table>
                    <tr>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt content text-align-top" colspan="1">•</td>
                        <td class="font-pkwt content" colspan="22">
                            Membocorkan rahasia perusahaan Membuat keterangan palsu atau memalsukan dokumen perusahaan.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt content text-align-top" colspan="1">•</td>
                        <td class="font-pkwt content" colspan="22">
                            Membuat kekacauan di tempat kerja.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt content text-align-top" colspan="1">•</td>
                        <td class="font-pkwt content" colspan="22">
                            Melalaikan kewajiban sehingga menimbulkan kerugian bagi perusahaan.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt content text-align-top" colspan="1">•</td>
                        <td class="font-pkwt content" colspan="22">
                            Dinyatakan tidak produktif / tidak mampu oleh PIHAK PERTAMA walaupun telah mendapat
                            pembinaan.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt content text-align-top" colspan="1">•</td>
                        <td class="font-pkwt content" colspan="22">
                            Meninggalkan pekerjaan tanpa seijin atasan walaupun sudah mendapatkan pembinaan sesuai
                            dengan
                            ketentuan yang berlaku.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt content text-align-top" colspan="1">•</td>
                        <td class="font-pkwt content" colspan="22">
                            Pelanggaran lainnya yang diatur dalam perundang-undangan/ PP/ SK Direksi.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">3.</td>
                        <td class="font-pkwt content" colspan="22">
                            Apabila salah satu pihak ingin mengakhiri Perjanjian Kerja Waktu Tertentu diluar
                            dari yang disebutkan pada ayat 1 & 2 diatas, maka harus memberitahukan selambat-lambatnya
                            1 (satu) bulan sebelum perjanjian kerja berakhir. Dan jika terjadi demikian maka
                            masing-masing
                            pihak akan patuh dan melaksanakan hal-hal seperti yang tercantum pada UU No. 13 Tahun 2003
                            tentang
                            Ketenagakerjaan Pasal 62.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PASAL 7</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">MASA EVALUASI</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">1.</td>
                        <td class="font-pkwt content" colspan="22">
                            Jika selama masa Evaluasi, <strong>PIHAK PERTAMA</strong> menilai <strong>PIHAK
                                KEDUA</strong> tidak memenuhi kriteria
                            maka kontrak kerjanya dapat diakhiri melalui surat pemberitahuan tidak lolos masa Evaluasi
                            sekurang-kurangnya 30 hari kerja sebelum hari terakhir bekerja dan <strong>PIHAK
                                KEDUA</strong> sepakat untuk
                            membebaskan <strong>PIHAK PERTAMA</strong> dari kewajiban membayar ganti rugi atau
                            pembayaran dalam bentuk apapun
                            kecuali gaji sampai dengan tanggal terakhir bekerja.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt" colspan="22">
                            <?= Yii::t('frontend', 'Tangerang, ') . Bulan::tanggal($model->tgl_mulai) ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt" colspan="22">
                            <strong>PIHAK PERTAMA</strong><br>
                            <?php
                            if ($perjanjianKerja->kontrak == 1) {
                                echo 'PT KOBE BOGA UTAMA';
                            } elseif ($perjanjianKerja->kontrak == 2) {
                                echo 'PT ARTA DWITUNGGAL ABADI';
                            } else {
                                echo 'PT BINA CIPTA ABADI';
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt" colspan="22" class="content"
                            style="text-decoration: underline; font-weight: bold;">
                            <br><br><br>
                            <?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt content" colspan="22">
                            <?= $model->olehPegawaiStruktur ? $model->olehPegawaiStruktur->struktur->name : ''; ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="font-pkwt text-align-top" colspan="12">
                <table>
                    <tr>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                        <td width="4mm">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">2.</td>
                        <td class="font-pkwt content" colspan="21">
                            Jika <strong>PIHAK KEDUA</strong> ingin mengundurkan diri selama masa evaluasi maka surat
                            pengajuan pengunduran dirinya sudah diketahui
                            atasan dan diterima oleh bagian personalia Human Capital (HC) sekurang-kurangnya 60 hari
                            kerja, sebelum hari terakhir
                            bekerja <strong>PIHAK KEDUA</strong> tetap akan memperoleh gaji sampai dengan tanggal
                            terakhir bekerja.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">3.</td>
                        <td class="font-pkwt content" colspan="21">
                            Jika pengunduran diri <strong>PIHAK KEDUA</strong> tidak sesuai dengan prosedur maka gaji
                            tidak dibayarkan sampai
                            serah-terima dokumen dan kewajiban lainnya diselesaikan.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">PASAL 8</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt bold center" colspan="24">KETENTUAN PENUTUP</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">1.</td>
                        <td class="font-pkwt content" colspan="21">
                            Dalam hal terjadi perselisihan dalam pelaksanaan Perjanjian Kerja Waktu Tertentu kedua belah
                            pihak bersepakat
                            untuk mengupayakan penyelesaian dengan cara musyawarah untuk mufakat. Dalam hal ini tidak
                            tercapai suatu
                            penyelesaian kedua belah pihak sepakat memilih tempat kedudukan hukum yang tetap dan tidak
                            berubah-ubah
                            di Dinas Tenaga Kerja dimana perusahaan berada dan PHI (Pengadilan Hubungan Industrial )
                            setempat.
                        </td>
                        <br><br>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">2.</td>
                        <td class="font-pkwt content" colspan="21">
                            <strong>PIHAK PERTAMA</strong> dan <strong>PIHAK KEDUA</strong> telah membaca dan memahami
                            isi Perjanjian Kerja Waktu Tertentu dalam keadaan
                            sehat jasmani maupun rohani serta ditandatangani kedua belah pihak tanpa adanya unsur
                            paksaan darimanapun juga.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt content text-align-top" colspan="2">3.</td>
                        <td class="font-pkwt content" colspan="20">
                            Perjanjian Kerja Waktu Tertentu ini dibuat rangkap 2 (dua) dan mempunyai kekuatan Hukum yang
                            sama,
                            satu berkas dipegang oleh <strong>PIHAK KEDUA</strong>.
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="24">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt" colspan="22">
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt" colspan="22">
                            <strong>PIHAK KEDUA</strong><br>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt" colspan="22" style="text-decoration: underline; font-weight: bold;">
                            <br><br><br><br>
                            <?= $model->pegawai ? $model->pegawai->nama : ''; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-pkwt" colspan="2"></td>
                        <td class="font-pkwt" colspan="22">
                            Karyawan
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>



<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPegawai */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Surat Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-pegawai-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Surat Pegawai') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('frontend', 'Update'), ['update',], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete',], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            'jenis',
            'tgl_mulai',
            'tgl_akhir',
            'tujuan_surat',
            [
                'attribute' => 'struktur.name',
                'label' => Yii::t('frontend', 'Struktur'),
            ],
            [
                'attribute' => 'golongan.id',
                'label' => Yii::t('frontend', 'Golongan'),
            ],
            'jenis_perjanjian',
            'kontrak',
            'catatan:ntext',
            [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('frontend', 'Pegawai'),
            ],
            [
                'attribute' => 'pegawaiGolongan.id',
                'label' => Yii::t('frontend', 'Pegawai Golongan'),
            ],
            [
                'attribute' => 'perjanjianKerja.id',
                'label' => Yii::t('frontend', 'Perjanjian Kerja'),
            ],
            [
                'attribute' => 'pegawaiStruktur.id',
                'label' => Yii::t('frontend', 'Pegawai Struktur'),
            ],
            'oleh_pegawai_id',
            'oleh_perjanjian_kerja_id',
            'oleh_pegawai_struktur_id',
            'created_by_pk',
            'created_by_struktur',
            ['attribute' => 'lock', 'visible' => false],
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>
    <div class="row">
        <h4>Golongan<?= ' ' . Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnGolongan = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'urutan',
        'keterangan',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->golongan,
        'attributes' => $gridColumnGolongan]);
    ?>
    <div class="row">
        <h4>Pegawai<?= ' ' . Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnPegawai = [
        ['attribute' => 'id', 'visible' => false],
        'nama_lengkap',
        'has_uploaded_to_att',
        'has_created_account',
        'has_downloadded_to_training_app',
        'nama_panggilan',
        'nama_ibu_kandung',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'status_pernikahan',
        'kewarganegaraan',
        'agama',
        'nomor_ktp',
        'nomor_kk',
        'no_telp',
        'no_handphone',
        'alamat_email',
        'ktp_provinsi_id',
        'ktp_kabupaten_kota_id',
        'ktp_kecamatan_id',
        'ktp_desa_id',
        'ktp_rw',
        'ktp_rt',
        'domisili_provinsi_id',
        'domisili_kabupaten_kota_id',
        'domisili_kecamatan_id',
        'domisili_desa_id',
        'domisili_rw',
        'domisili_rt',
        'domisili_alamat',
        'ktp_alamat',
        'catatan:ntext',
        'is_resign',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->pegawai,
        'attributes' => $gridColumnPegawai]);
    ?>
    <div class="row">
        <h4>PegawaiGolongan<?= ' ' . Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnPegawaiGolongan = [
        ['attribute' => 'id', 'visible' => false],
        'aksi',
        [
            'attribute' => 'pegawai.id',
            'label' => Yii::t('frontend', 'Pegawai'),
        ],
        [
            'attribute' => 'golongan.id',
            'label' => Yii::t('frontend', 'Golongan'),
        ],
        'mulai_berlaku',
        'akhir_berlaku',
        'keterangan',
        'created_by_org_chart',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->pegawaiGolongan,
        'attributes' => $gridColumnPegawaiGolongan]);
    ?>
    <div class="row">
        <h4>PegawaiStruktur<?= ' ' . Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnPegawaiStruktur = [
        ['attribute' => 'id', 'visible' => false],
        'aksi',
        [
            'attribute' => 'pegawai.id',
            'label' => Yii::t('frontend', 'Pegawai'),
        ],
        [
            'attribute' => 'struktur.name',
            'label' => Yii::t('frontend', 'Struktur'),
        ],
        'mulai_berlaku',
        'akhir_berlaku',
        'status',
        'keterangan',
        'created_by_org_chart',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->pegawaiStruktur,
        'attributes' => $gridColumnPegawaiStruktur]);
    ?>
    <div class="row">
        <h4>PerjanjianKerja<?= ' ' . Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnPerjanjianKerja = [
        ['attribute' => 'id', 'visible' => false],
        'aksi',
        'jenis_perjanjian',
        'kontrak',
        'id_pegawai',
        'mulai_berlaku',
        'akhir_berlaku',
        'status',
        'dasar_masa_kerja',
        'tanggal_resign',
        [
            'attribute' => 'pegawai.id',
            'label' => Yii::t('frontend', 'Pegawai'),
        ],
        'keterangan',
        'aksi_habis_kontrak',
        'keterangan_habis_kontrak',
        'created_by_org_chart',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->perjanjianKerja,
        'attributes' => $gridColumnPerjanjianKerja]);
    ?>
    <div class="row">
        <h4>Struktur<?= ' ' . Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnStruktur = [
        ['attribute' => 'id', 'visible' => false],
        'root',
        'lft',
        'rgt',
        'lvl',
        'name',
        'icon',
        'icon_type',
        'active',
        'selected',
        'disabled',
        'readonly',
        'visible',
        'collapsed',
        'movable_u',
        'movable_d',
        'movable_l',
        'movable_r',
        'removable',
        'removable_all',
        'child_allowed',
        'kode',
        'jabatan_id',
        'fungsi_jabatan_id',
        'tipe',
        'corporate_id',
        'perusahaan_id',
        'direktur_id',
        'divisi_id',
        'departemen_id',
        'section_id',
        'auto_approve',
        'durasi_auto_approve_min',
        'durasi_auto_approve',
    ];
    echo DetailView::widget([
        'model' => $model->struktur,
        'attributes' => $gridColumnStruktur]);
    ?>
</div>

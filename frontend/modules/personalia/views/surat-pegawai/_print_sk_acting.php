<?php

use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use yii\helpers\Html;
use common\components\Bulan;

/**
 * @var SuratPegawai $model
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
?>
<div class="slip-gaji-index">
    <table width="200mm" style="line-height: 1.1">
        <tr>
            <td colspan="26" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 16pt; text-decoration:underline; font-weight: bold; ">
                SURAT KEPUTUSAN
            </td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 12pt; font-weight: bold; ">
                No.
                <?= substr($model->seq_code, -3) ?>
                /SKP/HC-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->created_at))) . '/' . date('Y', strtotime($model->created_at)) ?>
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="26" class="center">
                Tentang<br>
                <strong>ACTING PROMOSI</strong>
            </td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td colspan="4" class="text-align-top">Menimbang :
            </td>
        </tr>
        <tr>
            <td colspan="4" class="text-align-top">
            </td>
            <td class="text-align-top">1.</td>
            <td class="content" colspan="18">
                Perkembangan Perusahaan dimana semua Sumber Daya Manusia akan ditempatkan pada posisi yang sesuai dengan
                kemampuannya dan kebutuhan Perusahaan.
            </td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td class="text-align-top">2.</td>
            <td class="content" colspan="18">
                Kinerja saudara selama
                menjadi <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?>
                cukup baik.
            </td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td class="text-align-top">3.</td>
            <td class="content" colspan="18">
                Perusahaan membutuhkan <?= $model->struktur ? $model->struktur->name : ''; ?>
            </td>
            <br>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td class="text-align-top">4.</td>
            <td class="content" colspan="18">
                Berdasarkan hal tersebut di atas maka Perusahaan :
            </td>
        </tr>

        <tr>
            <td colspan="1"></td>
            <td colspan="22"><br>
                Memutuskan :
            </td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td colspan="4" class="bold"><?= Yii::t('frontend', 'NIK') ?></td>
            <td colspan="12" class="bold">
                : <?= $perjanjianKerja ? str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : ''; ?></td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td colspan="4" class="bold"><?= Yii::t('frontend', 'Nama') ?></td>
            <td colspan="12" class="bold"> : <?= $model->pegawai->nama_lengkap; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="4"></td>
            <td colspan="4" class="bold"><?= Yii::t('frontend', 'TTL ') ?></td>
            <td colspan="12" class="bold">
                : <?= ucwords(strtolower($model->pegawai->tempat_lahir)) . ', ' . Bulan::tanggal($model->pegawai->tanggal_lahir); ?></td>
        </tr>
        <tr class="bold">
            <td colspan="4"></td>
            <td colspan="4" class="bold text-align-top"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
        </tr>
        <br><br>
        <tr class="bold">
            <td colspan="4"></td>
            <td colspan="19">Terhitung mulai tanggal <?= Bulan::tanggal($model->tgl_mulai) ?> menjalankan masa
                Acting <?= $model->struktur ? $model->struktur->name : ''; ?></td>
        </tr>
        <br>
        <tr class="bold">
            <td colspan="4"></td>
            <td class="text-align-top">1.</td>
            <td class="content" colspan="18">Saudara bertanggung jawab langsung
                kepada <?= $model->struktur ? $model->struktur->parents(1)->one()->name : ''; ?>
            </td>
        </tr>
        <tr class="bold">
            <td colspan="4"></td>
            <td class="text-align-top">2.</td>
            <td class="content" colspan="18">Promosi ini memerlukan percobaan selama <?= $model->intervalMonth() ?>
                bulan (mulai <?= Bulan::tanggal($model->tgl_mulai) . ' s/d ' . Bulan::tanggal($model->tgl_akhir) ?>
                dan bila dipandang perlu akan diperpanjang masa percobaannya selama 3 bulan
                setelah <?= $model->intervalMonth() ?> bulan
                sebelumnya berdasarkan hasil evaluasi.
            </td>
        </tr>
        <tr class="bold">
            <td colspan="4"></td>
            <td class="text-align-top">3.</td>
            <td class="content" colspan="18">Penyesuaian gaji sesuai dengan ketentuan yang berlaku diberikan setelah
                dinyatakan lulus masa percobaan promosinya.
            </td>
        </tr>
        <tr class="bold">
            <td colspan="4"></td>
            <td class="text-align-top">4.</td>
            <td class="content" colspan="18">Diharuskan sdr. <?= $model->pegawai->nama_lengkap; ?> dapat lebih
                meningkatkan kinerjanya dan
                mematuhi peraturan Perusahaan.
            </td>
        </tr>
        <tr class="bold">
            <td colspan="4"></td>
            <td class="text-align-top">5.</td>
            <td class="content" colspan="18">Apabila Anda lulus promosi maka Anda diangkat
                sebagai <?= $model->struktur ? $model->struktur->name : ''; ?>, apabila gagal maka akan dikembalikan ke
                jabatan semula.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td colspan="10">
                <?= Yii::t('frontend', 'Tangerang, ') . Bulan::tanggal($model->tgl_mulai) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" style="font-size: 11pt">
                <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content" style="text-decoration: underline; font-weight: bold;">
                <br><br><br><br>
                <?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content">
                <?= $model->olehPegawaiStruktur ? $model->olehPegawaiStruktur->struktur->name : ''; ?>
            </td>
            <td colspan="2"></td>
        </tr>

    </table>
</div>
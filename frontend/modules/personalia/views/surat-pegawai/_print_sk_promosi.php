<?php

use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use yii\helpers\Html;
use common\components\Bulan;

/**
 * @var SuratPegawai $model
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$pegawaiArea = $model->areaKerja;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
?>
<div class="slip-gaji-index">
    <table width="200mm" style="line-height: 1.1">
        <tr>
            <td colspan="26" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 16pt; text-decoration:underline; font-weight: bold; ">
                SURAT KEPUTUSAN
            </td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 12pt; font-weight: bold; ">
                No.
                <?= substr($model->seq_code, -3) ?>
                /SPJ/HC-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->created_at))) . '/' . date('Y', strtotime($model->created_at)) ?>
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="26" class="center">
                Tentang<br>
                <strong>PROMOSI JABATAN</strong>
            </td>
        </tr>
        <tr>
            <td colspan="26"></td>
        </tr>
        <br>
        <tr>
            <td colspan="2"></td>
            <td colspan="3" class="text-align-top">Mengingat :
            </td>
            <td style="vertical-align: text-top">1.</td>
            <td class="content" colspan="18">
                Perkembangan Perusahaan dimana semua Sumber Daya Manusia akan ditempatkan pada posisi yang sesuai dengan
                kemampuannya dan kebutuhan Perusahaan.
            </td>
            <br>
        </tr>
        <br>
        <tr>
            <td colspan="2"></td>
            <td colspan="3" class="text-align-top font-pkwt">Menimbang:
            </td>
            <td class="text-align-top">1.</td>
            <td class="content" colspan="18">
                Hasil Penilaian Saudara dinilai cukup baik.
            </td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td>2.</td>
            <td class="content" colspan="18">
                Evaluasi Direksi berdasarkan Performa Appraisal Saudara
                sebagai <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?>
                cukup baik, dan
                diharapkan mampu mempertahankan serta lebih meningkatkan Performa kerjanya dikemudian hari.
            </td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td>3.</td>
            <td class="content" colspan="18">
                Berdasarkan hal tersebut maka Perusahaan :
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="22"><br>
                Memutuskan : Terhitung mulai tanggal <?= Bulan::tanggal($model->tgl_mulai) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="4" class="text-align-top">Mengangkat:
            </td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'NIK') ?></td>
            <td colspan="12" class="bold">
                : <?= $perjanjianKerja ? str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : ''; ?></td>
        </tr>
        <tr>
            <td colspan="6"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Nama') ?></td>
            <td colspan="12" class="bold"> : <?= $model->pegawai->nama_lengkap; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="6"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Tempat/tgl lahir ') ?></td>
            <td colspan="12" class="bold">
                : <?= ucwords(strtolower($model->pegawai->tempat_lahir)) . ', ' . Bulan::tanggal($model->pegawai->tanggal_lahir); ?></td>
        </tr>
        <tr class="bold">
            <td colspan="6"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="22"><br>
                Menerangkan :
            </td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td colspan="5" style="border: 1px solid"></td>
            <td colspan="7" style="border: 1px solid; background: #9dc1d3; font-weight: bold">Saat ini :</td>
            <td colspan="7" style="border: 1px solid; background: #9dc1d3; font-weight: bold">Menjadi :</td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td colspan="5" style="border: 1px solid">Fungsi Jabatan</td>
            <td colspan="7"
                style="border: 1px solid"><?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
            <td colspan="7"
                style="border: 1px solid"><?= $model->struktur ? $model->struktur->name : ''; ?></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td colspan="5" style="border: 1px solid">Jabatan</td>
            <td colspan="7"
                style="border: 1px solid"><?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->jabatan ? $pegawaiStruktur->struktur->jabatan->nama : '' : '' : ''; ?></td>
            <td colspan="7"
                style="border: 1px solid"><?= $model->struktur ? $model->struktur->jabatan ? $model->struktur->jabatan->nama : '' : ''; ?></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td colspan="5" style="border: 1px solid">Golongan</td>
            <td colspan="7"
                style="border: 1px solid"><?= $pegawaiGolongan ? ucwords(strtolower($pegawaiGolongan->golongan->nama)) : ''; ?></td>
            <td colspan="7"
                style="border: 1px solid"><?= $model->golongan ? ucwords(strtolower($model->golongan->nama)) : ''; ?></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td colspan="5" style="border: 1px solid">Departement/Divisi</td>
            <td colspan="7"
                style="border: 1px solid"><?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : ''; ?></td>
            <td colspan="7"
                style="border: 1px solid"><?= $model->struktur ? $model->struktur->departemen ? $model->struktur->departemen->nama : $pegawaiStruktur->struktur->departemen : $pegawaiStruktur->struktur->departemen->nama; ?></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td colspan="5" style="border: 1px solid">Area</td>
            <td colspan="7"
                style="border: 1px solid"><?= $pegawaiArea ? ucwords(strtolower($pegawaiArea->refAreaKerja->name)) : ''; ?></td>
            <td colspan="7"
                style="border: 1px solid"><?= $model->refAreaKerja ? ucwords(strtolower($model->refAreaKerja->name)) : ''; ?></td>
        </tr>

        <tr>
            <td colspan="2"></td>
            <td colspan="22"><br>
                Demikian Surat Keputusan ini dibuat untuk dapat dipergunakan seperlunya.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td colspan="10">
                <?= Yii::t('frontend', 'Tangerang, ') . Bulan::tanggal($model->tgl_mulai) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" style="font-size: 11pt">
                <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content" style="text-decoration: underline; font-weight: bold;">
                <br><br><br><br>
                <?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content">
                <?= Yii::t('frontend', 'Direksi') ?>
            </td>
            <td colspan="2"></td>
        </tr>

    </table>
</div>
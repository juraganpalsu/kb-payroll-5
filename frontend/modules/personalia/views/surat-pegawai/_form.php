<?php

use frontend\modules\struktur\models\Struktur;
use kartik\datecontrol\DateControl;
use kartik\tree\TreeViewInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPegawai */
/* @var $form yii\widgets\ActiveForm */

$js = <<< JS
$(function() {
    $('#form-surat-peringatan').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });    
})
JS;

$this->registerJs($js);
?>

<div class="surat-pegawai-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-surat-peringatan',
        'action' => $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'jenis')->textInput(['placeholder' => 'Jenis']) ?>

    <div class="col-md-3">
        <?= $form->field($model, 'tgl_mulai')->widget(DateControl::class, [
            'type' => DateControl::FORMAT_DATE,
            'saveFormat' => 'php:Y-m-d',
            'ajaxConversion' => true,
            'widgetOptions' => [
                'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true
                ]
            ],
        ]); ?>
    </div>

    <div class="col-md-3">
        <?= $form->field($model, 'tgl_akhir')->widget(DateControl::class, [
            'type' => DateControl::FORMAT_DATE,
            'saveFormat' => 'php:Y-m-d',
            'ajaxConversion' => true,
            'widgetOptions' => [
                'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true
                ]
            ],
        ]); ?>
    </div>

    <?= $form->field($model, 'tujuan_surat')->textInput(['maxlength' => true, 'placeholder' => 'Tujuan Surat']) ?>

    <div class="col-md-8">
        <?php
        echo Html::activeLabel($model, 'struktur_id', ['class' => 'control-label has-star', 'style' => ['content' => '']]);
        echo Html::tag('span', Yii::t('frontend', '*'), ['class' => 'text-danger small mark']);
        echo TreeViewInput::widget([
            'model' => $model,
            'attribute' => 'struktur_id',
            'query' => Struktur::find()->addOrderBy('root, lft'),
            'headingOptions' => ['label' => Yii::t('frontend', 'Struktur')],
            'rootOptions' => ['label' => '<i class="fas fa-tree text-success"></i>'],
            'nodeLabel' => function (Struktur $node) {
                return $node->nameCostume;
            },
            'fontAwesome' => true,
            'asDropdown' => true,
            'multiple' => false,
            'options' => ['disabled' => false, 'id' => 'tree-input-struktur_id'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#form-modal")'),
            ],
            'pluginEvents' => [
                "change" => "function() { setValueStrukturHelper(this.value); }",
            ]
        ]);
        echo Html::activeHint($model, 'struktur_id');
        ?>
        <br>
    </div>

    <?= $form->field($model, 'golongan_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\personalia\models\Golongan::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('frontend', 'Choose Golongan')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'jenis_perjanjian')->textInput(['placeholder' => 'Jenis Perjanjian']) ?>

    <?= $form->field($model, 'kontrak')->textInput(['placeholder' => 'Kontrak']) ?>

    <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pegawai_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\personalia\models\Pegawai::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('frontend', 'Choose Pegawai')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'oleh_pegawai_id')->textInput(['maxlength' => true, 'placeholder' => 'Oleh Pegawai']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

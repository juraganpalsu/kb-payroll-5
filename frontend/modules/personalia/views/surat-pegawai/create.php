<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPegawai */

$this->title = Yii::t('frontend', 'Create Surat Pegawai');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Surat Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-pegawai-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formAktif', [
        'model' => $model,
    ]) ?>

</div>

<?php

use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use yii\helpers\Html;
use common\components\Bulan;

/**
 * @var SuratPegawai $model
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
?>
<div class="slip-gaji-index">
    <table width="200mm" style="font-size: 11pt">
        <tr>
            <td colspan="26" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 16pt; text-decoration:underline; font-weight: bold; ">
                ADDENDUM
            </td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 12pt; font-weight: bold; ">
                No.
                <?= substr($model->seq_code, -3) ?>
                /addendum/HC-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->created_at))) . '/' . date('Y', strtotime($model->created_at)) ?>
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="20">Addendum
                No.:<?= substr($model->seq_code, -3) ?>
                /addendum/HC-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->created_at))) . '/' . date('Y', strtotime($model->created_at)) ?>
                , ini dibuat dan di tandatangani di Tangerang oleh dan diantara :
            </td>
        </tr>
        <br>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">1.</td>
            <td colspan="18" class="content" style="line-height: 1.5">
                <strong><?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?></strong>,dalam hal ini bertindak
                sebagai <?= $model->olehPegawaiStruktur ? $model->olehPegawaiStruktur->struktur->name : ''; ?> dan oleh
                karenanya sah bertindak untuk dan atas nama PT. Kobe Boga Utama, selanjutnya dalam perjanjian ini
                disebut sebagai <strong>PIHAK PERTAMA</strong>.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">2.</td>
            <td colspan="18" class="content" style="line-height: 1.5">
                <strong><?= $model->pegawai->nama_lengkap; ?></strong>, Tempat tgl lahir
                : <?= ucwords(strtolower($model->pegawai->tempat_lahir)) . ', ' . Bulan::tanggal($model->pegawai->tanggal_lahir); ?>
                , selanjutnya dalam perjanjian ini disebut sebagai <strong>PIHAK KEDUA</strong>.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="20" style="line-height: 1.5"><strong>PIHAK PERTAMA</strong> dan <strong>PIHAK
                    KEDUA</strong> secara bersama-sama selanjutnya disebut
                sebagai “Para Pihak”. Para Pihak dengan ini terlebih dahulu menerangkan hal-hal sebagai berikut:
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">1.</td>
            <td colspan="18" style="line-height: 1.5">Bahwa, <strong>PIHAK PERTAMA</strong> dan <strong>PIHAK
                    KEDUA</strong> sebelumnya telah saling mengikatkan diri dalam suatu
                hubungan hukum perjanjian kerja waktu tertentu berdasarkan Perjanjian kerja waktu tertentu
                tanggal <?= Bulan::tanggal($model->tgl_mulai) ?>.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">2.</td>
            <td colspan="18" style="line-height: 1.5">Bahwa, dalam Perjanjian kerja waktu tertentu tersebut para pihak
                telah sepakat bahwa
                berakhirnya masa perjanjian kerja waktu tertentu adalah pada
                tanggal <?= Bulan::tanggal($model->perjanjianKerja->akhir_berlaku) ?>.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">3.</td>
            <td colspan="18" style="line-height: 1.5">Bahwa, para pihak sepakat dalam masa perjanjian kerja waktu
                tertentu terhitung mulai
                tanggal
                <?= Bulan::tanggal($model->perjanjianKerja->mulai_berlaku) . ' s/d ' . Bulan::tanggal($model->perjanjianKerja->akhir_berlaku) ?>
                adalah masa
                review dan penilaian kerja <strong>PIHAK PERTAMA</strong> kepada <strong>PIHAK KEDUA</strong> sebagai
                karyawan kontrak.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td colspan="22" style="line-height: 1.5">Berdasarkan uraian tersebut diatas, Para Pihak dengan ini sepakat
                untuk melakukan Addendum
                terhadap Perjanjian
                yang syarat-syarat dan ketentuan-ketentuannya sebagai berikut:
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="22"><strong>Melakukan perubahan Pasal 1 Perjanjian kerja waktu tertentu tentang “Status”sebagai
                    berikut
                    :</strong>
            </td>
        </tr>
        <tr>
            <td colspan="22"></td>
        </tr>
        <br><br><br>
        <tr>
            <td colspan="20"></td>
            <td><small>Paraf</small>(..........)</td>
        </tr>
    </table>
    <table width="200mm" style="line-height: 1.5;">
        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="26" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="22">
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="22">
                Semula:<br>
                Pasal 1 <br>
                Status
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">1.</td>
            <td colspan="18"><strong>PIHAK KEDUA</strong> diterima bekerja oleh <strong>PIHAK PERTAMA</strong>
                sebagai <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?>
                .
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">2.</td>
            <td colspan="18"><strong>PIHAK KEDUA</strong> bekerja terhitung mulai
                tanggal <?= Bulan::tanggal($model->perjanjianKerja->mulai_berlaku) ?>.
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">3.</td>
            <td colspan="18">Status karyawan adalah kontrak terhitung mulai
                tanggal <?= Bulan::tanggal($model->perjanjianKerja->mulai_berlaku) . ' sampai dengan tanggal ' . Bulan::tanggal($model->perjanjianKerja->akhir_berlaku) ?>
                .
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="22">
                Berubah Menjadi:<br>
                Pasal 1 <br>
                Status
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">1.</td>
            <td colspan="18"><strong>PIHAK KEDUA</strong> diterima bekerja oleh <strong>PIHAK PERTAMA</strong>
                sebagai <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?>
                .
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">2.</td>
            <td colspan="18"><strong>PIHAK KEDUA</strong> bekerja terhitung mulai
                tanggal <?= Bulan::tanggal($model->tgl_mulai) ?>.
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="text-align-top">3.</td>
            <td colspan="18">Status karyawan adalah kontrak terhitung mulai
                tanggal <?= Bulan::tanggal($model->tgl_mulai) . ' sampai dengan tanggal ' . Bulan::tanggal($model->tgl_akhir) ?>
                .
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="22">
                Addendum ini mulai berlaku terhitung sejak tanggal sebagaimana disebutkan dalam bagian awal Addendum
                ini.<br>
                Hal-hal lain yang telah diatur dalam Perjanjian yang tidak dilakukan perubahan dalam Addendum ini tetap
                berlaku dan mengikat Para Pihak.<br>
                Demikian Addendum ini dibuat dalam rangkap 2 (dua) bermaterai cukup, masing-masing pihak memperoleh satu
                rangkap yang kesemuanya mempunyai kekuatan hukum dan pembuktian yang sama.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" style="font-size: 11pt">
                <strong>PIHAK PERTAMA</strong>,
            </td>
            <td colspan="4"></td>
            <td colspan="8" style="font-size: 11pt">
                <strong>PIHAK KEDUA</strong>,
            </td>
        </tr>
        <tr>
            <td>
                <br><br>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content">
                <br><br><br>
                <div style="text-decoration: underline; font-weight: bold;"><?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?></div>
                <?= $model->olehPegawaiStruktur ? $model->olehPegawaiStruktur->struktur->name : ''; ?>
            </td>
            <td colspan="4"></td>
            <td colspan="8" class="content">
                <br><br><br>
                <div style="text-decoration: underline; font-weight: bold;"><?= $model->pegawai ? $model->pegawai->nama : ''; ?></div>
                <?= Yii::t('frontend', 'Karyawan') ?>
            </td>
        </tr>

    </table>
</div>
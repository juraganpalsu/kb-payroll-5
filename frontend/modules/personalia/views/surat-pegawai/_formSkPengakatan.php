<?php

use common\models\Golongan;
use common\models\Pegawai;
use frontend\modules\pegawai\models\RefAreaKerja;
use frontend\modules\struktur\models\Struktur;
use kartik\datecontrol\DateControl;
use kartik\tree\TreeViewInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\Module;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPegawai */
/* @var $form yii\widgets\ActiveForm */
/* @var $modelPegawai Pegawai */

$this->title = Yii::t('frontend', 'Surat Keputusan Pengangkatan Tetap');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Surat Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$urlTglMasaKerja = Url::to(['get-tgl-masa-kerja']);
$js = <<< JS
$(function() {
    $('#form-surat-pegawai').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
                // location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);
?>

<div class="surat-pegawai-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-surat-pegawai',
        'action' => Url::to('sk-pengangkatan'),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'tgl_mulai')->label('Terhitung Tgl')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('frontend', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'pegawai_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => false,
                    ],
                    'data' => $model->isNewRecord ? [] : [$model->pegawai_id => $model->pegawai->namaIdPegawai],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, tanggal:$("#suratpegawai-tgl_mulai").val()};}')
                        ],
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'oleh_pegawai_id')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => false,
                    ],
                    'data' => $model->TtdOleh,
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
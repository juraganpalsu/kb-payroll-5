<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\SuratPegawai */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Surat Pegawai',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Surat Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view',]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="surat-pegawai-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

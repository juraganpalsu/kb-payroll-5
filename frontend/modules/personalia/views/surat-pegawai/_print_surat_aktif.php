<?php

use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use yii\helpers\Html;
use common\components\Bulan;

/**
 * @var SuratPegawai $model
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
?>
<div class="slip-gaji-index">
    <table width="200mm" style="line-height: 1.5; font-size: 12pt">
        <tr>
            <td colspan="26" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 16pt; text-decoration:underline; font-weight: bold; ">
                SURAT KETERANGAN
            </td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 12pt; font-weight: bold;">
                No.
                <?= substr($model->seq_code, -3) ?>
                /AK/HC-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->created_at))) . '/' . date('Y', strtotime($model->created_at)) ?>
            </td>
        </tr>
        <br><br>
        <br>
        <tr>
            <td colspan="2"></td>
            <td colspan="22">Yang bertanda tangan dibawah ini :
            </td>
        </tr>
        <br>
        <tr>
            <td colspan="3"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Nama Lengkap') ?></td>
            <td class="bold text-right text-align-top">:</td>
            <td colspan="12" class="bold"><?= $model->olehPegawai->nama_lengkap; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td class="bold text-right text-align-top">:</td>
            <td colspan="12" class="bold">
                <?= $olehPegawaiStruktur ? $olehPegawaiStruktur->struktur ? $olehPegawaiStruktur->struktur->name : '' : ''; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="5" class="bold text-align-top"><?= Yii::t('frontend', 'Alamat ') ?></td>
            <td class="bold text-right text-align-top">:</td>
            <td colspan="12" class="bold">
                Jl. Manis Raya No.15 Curug Kawasan Industri Manis Tangerang
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td colspan="22">Menerangkan bahwa :
            </td>
        </tr>
        <br>
        <tr>
            <td colspan="3"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'NIK') ?></td>
            <td class="bold text-right text-align-top">:</td>
            <td colspan="6" class="bold">
                <?= $perjanjianKerja ? str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : ''; ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Nama Lengkap') ?></td>
            <td class="bold text-right text-align-top">:</td>
            <td colspan="12" class="bold"><?= $model->pegawai->nama_lengkap; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td class="bold text-right text-align-top">:</td>
            <td colspan="12" class="bold">
                <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Departemen ') ?></td>
            <td class="bold text-right text-align-top">:</td>
            <td colspan="12" class="bold">
                <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : ''; ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="5" class="bold"><?= Yii::t('frontend', 'Tanggal Masuk ') ?></td>
            <td class="bold text-right text-align-top">:</td>
            <td colspan="12" class="bold">
                <?= Bulan::tanggal($model->tglMasaKerja()); ?></td>
        </tr>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <br>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td class="content" style="line-height: 1.5" colspan="22">
                Yang bersangkutan adalah benar karyawan
                <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                }
                ?>
                dan sampai surat keterangan ini dibuat masih aktif bekerja pada perusahaan kami.<br>
                Demikian Surat Keterangan ini kami buat untuk keperluan <?= $model->tujuan_surat ?>, agar dapat dipergunakan
                sebagaimana mestinya.
                Atas perhatiannya kami ucapkan terimakasih.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td colspan="10">
                <?= Yii::t('frontend', 'Tangerang, ') . Bulan::tanggal($model->tgl_mulai) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="14">
                <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <br><br>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content" style="text-decoration: underline; font-weight: bold;">
                <br><br><br><br>
                <?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content">
                <?= $model->olehPegawaiStruktur ? $model->olehPegawaiStruktur->struktur->name : ''; ?>
            </td>
            <td colspan="2"></td>
        </tr>

    </table>
</div>
<?php

use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use yii\helpers\Html;
use common\components\Bulan;

/**
 * @var SuratPegawai $model
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
?>
<div class="slip-gaji-index">
    <table width="200mm">
        <tr>
            <td colspan="26" style="padding-left: 0">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                        echo Html::img('@frontend/web/pictures/kop_kobe.png');
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                        echo Html::img('@frontend/web/pictures/kop_ada.png');
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 16pt; text-decoration:underline; font-weight: bold; ">
                SURAT KETERANGAN
            </td>
        </tr>
        <tr>
            <td colspan="26" class="center" style="font-size: 12pt; font-weight: bold; ">
                No.
                <?= substr($model->seq_code, -3) ?>
                /SK-D/HC-<?= $model->perjanjianKerja->kontrak == PerjanjianKerja::KBU || $model->perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU ? 'KBU' : 'ADA' ?>
                /<?= Helper::integerToRoman(date('m', strtotime($model->created_at))) . '/' . date('Y', strtotime($model->created_at)) ?>
            </td>
        </tr>
        <br><br>
        <br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="22">
                Kepada Yth.<br>
                <strong>Sdr. <?= $model->pegawai->nama_lengkap; ?>
                    ( <?= $perjanjianKerja ? str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : ''; ?>
                    ) </strong><br>
                Di tempat.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="22">
                Dengan hormat, <br>
                Sejalan dengan tujuan pengembangan perusahaan dan dalam upaya meningkatan efektivitas,
                serta berdasarkan penilaian dari atasan Saudara, maka terhitung sejak tanggal 21 Juli 2020
                Saudara/i telah Demosi dari:
            </td>
        </tr>
        <br>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Departemen ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : ''; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Golongan ') ?></td>
            <td colspan="12" class="bold">
                : <?= $pegawaiGolongan ? $pegawaiGolongan->golongan->nama : ''; ?></td>
        </tr>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <br>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="22">Demosi menjadi: :
            </td>
        </tr>
        <br>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Jabatan ') ?></td>
            <td colspan="12" class="bold">
                : <?= $model->struktur ? $model->struktur->name : $pegawaiStruktur->struktur->name; ?></td>
        </tr>
        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Departemen ') ?></td>
            <td colspan="12" class="bold">
                : <?= $model->struktur ? $model->struktur->departemen ? $model->struktur->departemen->nama : $pegawaiStruktur->struktur->departemen : $pegawaiStruktur->struktur->departemen->nama; ?></td>
        </tr>

        <tr class="bold">
            <td colspan="3"></td>
            <td colspan="6" class="bold"><?= Yii::t('frontend', 'Golongan ') ?></td>
            <td colspan="12" class="bold">
                : <?= $model->golongan ? $model->golongan->nama : ''; ?></td>
        </tr>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <br>
        <tr>
            <td class="content" colspan="24"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td class="content" colspan="22">
                Semoga perubahan ini akan memperkuat kegigihan Saudara/i dalam meningkatkan
                performa dan pengembangan karir.
            </td>
        </tr>
        <br><br>
        <tr>
            <td colspan="2"></td>
            <td colspan="10">
                <?= Yii::t('frontend', 'Tangerang, ') . Bulan::tanggal($model->tgl_mulai) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" style="font-size: 11pt">
                <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA || $perjanjianKerja->kontrak == PerjanjianKerja::BCA_ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <br><br>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content" style="text-decoration: underline; font-weight: bold;">
                <br><br><br><br>
                <?= $model->olehPegawai ? $model->olehPegawai->nama : ''; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="8" class="content">
                <?= $model->olehPegawaiStruktur ? $model->olehPegawaiStruktur->struktur->name : ''; ?>
            </td>
            <td colspan="2"></td>
        </tr>

    </table>
</div>
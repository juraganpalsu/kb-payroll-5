<?php
/**
 * Created by PhpStorm.
 * File Name: menu-surat.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 13/10/2020
 * Time: 14:57
 */

use common\components\Bulan;
use frontend\modules\personalia\models\form\Pegawai;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model Pegawai */
/* @var $sistemKerja */
/* @var $statusKehadiran */

$this->title = Yii::t('frontend', 'Menu Surat Pegawai');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<< JS
$(function() {
    $('#menu-surat-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                console.log(dt);
                let win = window.open(dt.data.url, '_blank');
                win.focus();
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);
?>
<div class="menu-surat">
    <div class="menu-surat-form">

        <?php $form = ActiveForm::begin([
            'id' => 'menu-surat-form',
            'options' => ['target' => '_blank'],
            'type' => ActiveForm::TYPE_VERTICAL,
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['menu-surat-validation'])
        ]); ?>

        <?= $form->errorSummary($model); ?>

        <?php try { ?>

            <div class="row">
                <div class="col-md-6">
                    <?php
                    echo $form->field($model, 'jenis')->dropDownList($model->_jenis);
                    ?>
                </div>
            </div>

            <hr/>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('frontend', 'Create'), ['class' => 'btn btn-success btn-flat']) ?>
            </div>

            <?php
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
        <?php ActiveForm::end(); ?>
    </div>

</div>
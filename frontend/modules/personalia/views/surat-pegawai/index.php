<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\personalia\models\search\SuratPegawaiSearch */

/**
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $modelPerjanjiankerja PerjanjianKerja
 */

use common\models\Golongan;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPegawai;
use frontend\modules\struktur\models\Struktur;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Url;

$this->title = Yii::t('frontend', 'Surat Pegawai');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
});
JS;

$this->registerJs($js);
?>
    <div class="surat-pegawai-index">

        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'jenis',
                'value' => function (SuratPegawai $model) {
                    return $model->_jenis[$model->jenis];
                },
                'filter' => Html::activeDropDownList($searchModel, 'jenis', $searchModel->_jenis, ['class' => 'form-control', 'prompt' => '*']),
            ],
            [
                'attribute' => 'id_pegawai',
                'value' => function (SuratPegawai $model) {
                    return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'pegawai_id',
                'label' => Yii::t('frontend', 'Pegawai'),
                'value' => function (SuratPegawai $model) {
                    return Html::a(Html::tag('strong', $model->pegawai->nama_lengkap), ['print', 'id' => $model->id], ['class' => 'font-weight-bold', 'title' => 'print', 'data' => ['pjax' => 0], 'target' => '_blank']);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'golongan',
                'value' => function (SuratPegawai $model) {
                    if ($golongan = $model->pegawaiGolongan) {
                        return $golongan->golongan->nama;
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
                'hAlign' => 'center',
                'format' => 'raw'
            ],
            [
                'attribute' => 'busines_unit',
                'value' => function (SuratPegawai $model) {
                    if ($perjanjianKerja = $model->perjanjianKerja) {
                        return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelPerjanjiankerja->_kontrak,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-pegawai-search-busines_unit'],
                'hAlign' => 'center',
                'format' => 'raw'
            ],
            [
                'attribute' => 'tgl_mulai',
                'value' => function (SuratPegawai $model) {
                    return date('d-m-Y', strtotime($model->tgl_mulai));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            [
                'attribute' => 'tgl_akhir',
                'value' => function (SuratPegawai $model) {
                    if (!is_null($model->tgl_akhir)) {
                        return date('d-m-Y', strtotime($model->tgl_akhir));
                    }
                    return '';
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            'tujuan_surat',
            'jenis_perjanjian',
            'kontrak',
            'catatan:ntext',
            [
                'attribute' => 'oleh_pegawai_id',
                'value' => function (SuratPegawai $model) {
                    if ($model->olehPegawai) {
                        return $model->olehPegawai->nama;
                    }
                    return '';
                },
            ],
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{print} {delete}',
                'buttons' => [
                    'print' => function ($url, SuratPegawai $model) {
                        return Html::a(Yii::t('frontend', 'Print'), $url, ['class' => 'btn btn-xs btn-success btn-flat', 'data' => ['pjax' => 0], 'target' => '_blank']);
                    },
                ],
            ],
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
//            'selectedColumns' => [2, 3, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 17, 18],
                'noExportColumns' => [1, 4, 15, 18], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'surat-peringatan' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-surat-peringatan']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['menu-surat'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat call-modal-btn']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>

    </div>
<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false,
        'style' => 'overflow:hidden'
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
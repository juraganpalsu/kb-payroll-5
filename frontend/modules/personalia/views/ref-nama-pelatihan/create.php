<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefNamaPelatihan */

$this->title = Yii::t('frontend', 'Create Ref Nama Pelatihan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Nama Pelatihan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-nama-pelatihan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

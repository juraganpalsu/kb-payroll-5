<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefNamaPelatihan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Ref Nama Pelatihan',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Nama Pelatihan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="ref-nama-pelatihan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

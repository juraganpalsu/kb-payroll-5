<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\personalia\models\search\AppraisalSearch */

/**
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var boolean $isForMe
 */

use common\models\Golongan;
use frontend\modules\personalia\models\Appraisal;
use mdm\admin\components\Helper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('frontend', 'Appraisal');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appraisal-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'kategori',
            'value' => function (Appraisal $model) {
                return $model->_kategori[$model->kategori];
            },
            'filter' => Html::activeDropDownList($searchModel, 'kategori', $searchModel->_kategori, ['class' => 'form-control', 'prompt' => '*']),
        ],
        [
            'attribute' => 'jenis_evaluasi',
            'value' => function (Appraisal $model) {
                return $model->_jenis[$model->jenis_evaluasi];
            },
            'filter' => Html::activeDropDownList($searchModel, 'jenis_evaluasi', $searchModel->_jenis, ['class' => 'form-control', 'prompt' => '*']),
        ],
        [
            'attribute' => 'departemen',
            'value' => function (Appraisal $model) {
                if ($pegawaiStruktur = $model->pegawaiStruktur) {
                    return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : '';
                }
                return '';
            },
            'hidden' => true
        ],
        [
            'attribute' => 'jabatan',
            'value' => function (Appraisal $model) {
                if ($pegawaiStruktur = $model->pegawaiStruktur) {
                    return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->jabatan ? $pegawaiStruktur->struktur->jabatan->nama : '' : '' : '';
                }
                return '';
            },
            'hidden' => true
        ],
        [
            'attribute' => 'fungsi_jabatan',
            'value' => function (Appraisal $model) {
                if ($pegawaiStruktur = $model->pegawaiStruktur) {
                    return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->fungsiJabatan ? $pegawaiStruktur->struktur->fungsiJabatan->nama : '' : '' : '';
                }
                return '';
            },
            'hidden' => true
        ],
        [
            'attribute' => 'id_pegawai',
            'value' => function (Appraisal $model) {
                return $model->perjanjianKerja->id_pegawai;
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('frontend', 'Pegawai'),
            'value' => function (Appraisal $model) use ($isForMe) {
                if ($isForMe) {
                    return Html::a(Html::tag('strong', $model->pegawai->nama_lengkap), ['appraisal/create-penilaian', 'id' => $model->id], ['class' => 'font-weight-bold']);
                }
                return Html::tag('strong', $model->pegawai->nama_lengkap, ['class' => 'font-weight-bold']);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (Appraisal $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $searchModel->modelPerjanjianKerja()->_kontrak,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('busines_unit'), 'id' => 'grid-Appraisal-search-busines_unit'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'golongan',
            'value' => function (Appraisal $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('golongan'), 'id' => 'grid-pegawai-search-golongan'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'tanggal_awal',
            'value' => function (Appraisal $model) {
                return date('d-m-Y', strtotime($model->tanggal_awal));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'tanggal_akhir',
            'value' => function (Appraisal $model) {
                return date('d-m-Y', strtotime($model->tanggal_akhir));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        'total_nilai',
        [
            'attribute' => 'rata_rata',
            'value' => function (Appraisal $model) {
                return $model->rata_rata;
            },
            'format' => ['decimal', 2],
        ],
        [
            'attribute' => 'rekomendasi',
            'value' => function (Appraisal $model) {
                return $model->rekomendasi ? $model->_rekomendasi[$model->rekomendasi] : '';
            },
            'filter' => Html::activeDropDownList($searchModel, 'rekomendasi', $searchModel->_rekomendasi, ['class' => 'form-control', 'prompt' => '*']),
        ],
        'komentar_tambahan:ntext',
        'kesimpulan',
        [
            'attribute' => 'atasan_pegawai_id',
            'value' => function (Appraisal $model) {
                if ($model->atasanPegawai) {
                    return $model->atasanPegawai->nama;
                }
                return '';
            },
        ],
//        [
//            'attribute' => 'atasan_dua_pegawai_id',
//            'value' => function (Appraisal $model) {
//                if ($model->atasanDuaPegawai) {
//                    return $model->atasanDuaPegawai->nama;
//                }
//                return '';
//            },
//        ],
        [
            'attribute' => 'oleh_pegawai_id',
            'value' => function (Appraisal $model) {
                if ($model->olehPegawai) {
                    return $model->olehPegawai->nama;
                }
                return '';
            },
        ],
        [
            'attribute' => 'created_by',
            'value' => function (Appraisal $model) {
                $createdBy = $model->createdBy->userPegawai;
                $nama = $createdBy ? $createdBy->pegawai->nama : '';
                $namaTampil = explode(' ', $nama);
                return $namaTampil[0];
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (Appraisal $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{print} {delete}',
            'buttons' => [
                'print' => function ($url, Appraisal $model) {
                    return Html::a(Yii::t('frontend', 'Print'), $url, ['class' => 'btn btn-xs btn-success btn-flat', 'data' => ['pjax' => 0], 'target' => '_blank']);
                },
                'delete' => function ($url, Appraisal $model) {
                    if (Helper::checkRoute('/personalia/appraisal/delete')) {
                        return Html::a(Yii::t('frontend', 'Delete'), $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan delete data ini ??'), 'method' => 'post']]);
                    }
                    return '';
                },
            ],
        ],
    ];
    ?>
    <?php
    try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
            'noExportColumns' => [1, 16], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'Appraisal' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'options' => ['style' => 'table-layout:fixed;'],
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-appraisal']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        print_r($e->getMessage());
        Yii::info($e->getMessage(), 'excetion');
    }
    ?>

</div>

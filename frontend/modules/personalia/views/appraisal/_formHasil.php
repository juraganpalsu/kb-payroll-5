<?php

use frontend\modules\personalia\models\Appraisal;
use frontend\modules\personalia\models\AppraisalDetail;
use frontend\modules\personalia\models\RefAppraisal;
use kartik\datecontrol\DateControl;
use kartik\rating\StarRating;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Appraisal */
/* @var $modelAppraisalDetails AppraisalDetail */
/* @var $form yii\widgets\ActiveForm */
/**
 * @var ActiveRecord $refAppraisal
 */

$this->title = Yii::t('frontend', 'Create {modelClass}: ', [
        'modelClass' => 'Appraisal',
    ]) . ' ' . $model->pegawai->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Appraisal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$js = <<< JS
$(function() {
    $('#form-appraisal').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });    
})
JS;

$this->registerJs($js);
?>

<div class="appraisal-form">
    <div class="row">
        <div class="col-sm-4">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'id_pegawai',
                    'value' => function (Appraisal $model) {
                        return $model->perjanjianKerja->id_pegawai;
                    },
                ],
                [
                    'attribute' => 'pegawai_id',
                    'label' => Yii::t('frontend', 'Pegawai'),
                    'value' => function (Appraisal $model) {
                        return $model->pegawai->nama_lengkap;
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'fungsi_jabatan',
                    'value' => function (Appraisal $model) {
                        if ($pegawaiStruktur = $model->pegawaiStruktur) {
                            return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->fungsiJabatan ? $pegawaiStruktur->struktur->fungsiJabatan->nama : '' : '' : '';
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => 'jenis_evaluasi',
                    'value' => function (Appraisal $model) {
                        return $model->_jenis[$model->jenis_evaluasi];
                    },
                ],
                [
                    'attribute' => 'golongan',
                    'label' => Yii::t('frontend', 'Golongan'),
                    'value' => function (Appraisal $model) {
                        if ($golongan = $model->pegawaiGolongan) {
                            return $golongan->golongan->nama;
                        }
                        return '';
                    },
                ],

            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            $gridColumn = [
                [
                    'attribute' => 'bisnis_unit',
                    'value' => function (Appraisal $model) {
                        if ($perjanjianKerja = $model->perjanjianKerja) {
                            return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => 'departemen',
                    'value' => function (Appraisal $model) {
                        if ($pegawaiStruktur = $model->pegawaiStruktur) {
                            return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : '';
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => 'masa_evaluasi',
                    'value' => function (Appraisal $model) {
                        return date('d-m-Y', strtotime($model->tanggal_awal)) . ' s/d ' . date('d-m-Y', strtotime($model->tanggal_akhir));
                    },
                ],
                [
                    'attribute' => 'rentang_waktu',
                    'value' => function (Appraisal $model) {
                        return $model->intervalMonth();
                    },
                ],
                [
                    'attribute' => 'due_date',
                    'value' => function (Appraisal $model) {
                        return Html::tag('strong', $model->dueDate(), ['class' => 'text-danger font-weight-bold']);
                    },
                    'format' => 'raw'
                ],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>

    </div>

    <?php if (!empty($model->appraisalDetails)) {
        if ($model->kategori == Appraisal::STAFF) {
            ?>
            <div class="row">
                <div class="col-md-10">

                    <?php
                    /** @var RefAppraisal $item */
                    foreach ($refAppraisal as $item) {
                        ?>
                        <div class="col-md-12 box box-success" style="font-size: 12pt">
                            <div class="col-md-4">
                                <?= $item['keterangan_kiri'] ?>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-12" style="text-align: center; border-bottom: 1px solid">
                                    <?= $item['kriteria'] ?>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-xs-2">
                                        <span>1</span>
                                        <?= Appraisal::getPenilaian($model->id, $item->id) == 1 ? 'X' : '' ?>
                                    </div>
                                    <div class="col-xs-2">
                                        <span>2</span>
                                        <?= Appraisal::getPenilaian($model->id, $item->id) == 2 ? 'X' : '' ?>
                                    </div>
                                    <div class="col-xs-2">
                                        <span>3</span>
                                        <?= Appraisal::getPenilaian($model->id, $item->id) == 3 ? 'X' : '' ?>
                                    </div>
                                    <div class="col-xs-2">
                                        <span>4</span>
                                        <?= Appraisal::getPenilaian($model->id, $item->id) == 4 ? 'X' : '' ?>
                                    </div>
                                    <div class="col-xs-2">
                                        <span>5</span>
                                        <?= Appraisal::getPenilaian($model->id, $item->id) == 5 ? 'X' : '' ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <?= $item['keterangan_kanan'] ?>
                            </div>
                        </div>

                    <?php } ?>


                </div>
            </div>
        <?php }
        if ($model->kategori == Appraisal::MANAGERIAL) {
            ?>
            <div class="row">
                <div class="col-md-10">
                    <?php
                    /** @var RefAppraisal $item */
                    foreach ($refAppraisal as $item) {
                        ?>
                        <div class="col-md-12 box box-success" style="font-size: 12pt">
                            <div class="col-md-8">
                                <?= $item['keterangan_kiri'] ?>
                            </div>

                            <div class="col-md-4">
                                <?php
                                if ($modelAppraisal = Appraisal::getPenilaian($model->id, $item->id, true)) {
                                    echo Html::tag('label', Yii::t('frontend', 'Nilai'), ['class' => 'control-label']);
                                    echo StarRating::widget([
                                        'name' => 'nilai',
                                        'value' => $modelAppraisal->nilai,
                                        'pluginOptions' => [
                                            'readonly' => true,
                                            'showClear' => false,
                                            'showCaption' => false,
                                        ],
                                    ]);
                                    echo Html::tag('label', Yii::t('frontend', 'Komentar'), ['class' => 'control-label']);
                                    echo $modelAppraisal->komentar ? Html::tag('p', $modelAppraisal->komentar) : Html::tag('p', '-');
                                }
                                ?>
                            </div>
                        </div>

                    <?php } ?>


                </div>
            </div>
            <?php
        }
    } ?>

    <div class="row">
        <div class="col-md-6">

            <?php $form = ActiveForm::begin([
                'id' => 'form-appraisal',
                'action' => Url::to(['appraisal/create-kesimpulan', 'id' => $model->id]),
                'type' => ActiveForm::TYPE_VERTICAL,
                'formConfig' => ['showErrors' => false],
                'enableAjaxValidation' => true,
                'validationUrl' => Url::to(['create-or-update-validation']),
                'fieldConfig' => ['showLabels' => true],
            ]); ?>




            <?= $form->errorSummary($model); ?>

            <?php try { ?>

                <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                <div class="row">
                    <div class="col-md-8">
                        <?= $form->field($model, 'rekomendasi')->textInput(['readonly' => true, 'value' => $model->_rekomendasi[$model->rekomendasi]]) ?>
                    </div>
                </div>

                <?= $form->field($model, 'rekomendasi', ['template' => '{input}'])->hiddenInput(); ?>

                <?= $form->field($model, 'kesimpulan')->textInput(['maxlength' => true, 'placeholder' => 'Kesimpulan']) ?>

                <?= $form->field($model, 'komentar_tambahan')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            } ?>

            <?php ActiveForm::end(); ?>

        </div>

        <div class="col-md-4">
            <?php
            $gridColumn = [
                'total_nilai',
                'rata_rata',
                'alpa',
                'izin',
                'terlambat',
                'cuti',
                'sakit',
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn,
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
</div>

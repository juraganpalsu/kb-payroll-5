<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Appraisal */

$this->title = Yii::t('frontend', 'Create Appraisal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Appraisal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appraisal-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

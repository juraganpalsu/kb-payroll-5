<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Appraisal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Appraisal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appraisal-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Appraisal') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            'kategori',
            'jenis_evaluasi',
            'tanggal_awal',
            'tanggal_akhir',
            'alpa',
            'izin',
            'terlambat',
            'cuti',
            'total_nilai',
            'rata_rata',
            'rekomendasi',
            'komentar_tambahan:ntext',
            'kesimpulan',
            [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('frontend', 'Pegawai'),
            ],
            [
                'attribute' => 'perjanjianKerja.id',
                'label' => Yii::t('frontend', 'Perjanjian Kerja'),
            ],
            [
                'attribute' => 'pegawaiGolongan.id',
                'label' => Yii::t('frontend', 'Pegawai Golongan'),
            ],
            [
                'attribute' => 'pegawaiStruktur.id',
                'label' => Yii::t('frontend', 'Pegawai Struktur'),
            ],
            'oleh_pegawai_id',
            'atasan_pegawai_id',
            ['attribute' => 'lock', 'visible' => false],
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * File Name: _print_appraisal_staff.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 12/11/2020
 * Time: 21:51
 */

use common\components\Helper;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\Appraisal;
use frontend\modules\personalia\models\RefAppraisal;
use yii\helpers\Html;
use common\components\Bulan;

/**
 * @var Appraisal $model
 * @var RefAppraisal $refAppraisal
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
?>
<div class="slip-gaji-index">
    <table width="200mm" style="line-height: 1.1; border: 1px solid; font-size: 11pt; page-break-after: always">
        <tr>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
            <td width="8mm">&nbsp;</td>
        </tr>
        <tr>
            <td class="dept" colspan="4" rowspan="4" style="text-align: center">
                <div>
                    <?php
                    if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU) {
                        echo Html::img('@frontend/web/pictures/image001.png', ['height' => '60px']);
                    } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA) {
                        echo Html::img('@frontend/web/pictures/image002.png', ['height' => '60px']);
                    } else {
                        echo Html::img('@frontend/web/pictures/image003.png', ['height' => '60px']);
                    }
                    ?>
                </div>
            </td>
            <td colspan="14" style="  border: 0;">&nbsp;</td>
            <td colspan="6" class="nodok">No. Kode.Dok</td>
        </tr>
        <tr>
            <th rowspan="2" colspan="14" style="font-size: 22pt">
                <?php
                if ($perjanjianKerja->kontrak == PerjanjianKerja::KBU) {
                    echo 'PT KOBE BOGA UTAMA';
                } elseif ($perjanjianKerja->kontrak == PerjanjianKerja::ADA) {
                    echo 'PT ARTA DWITUNGGAL ABADI';
                } else {
                    echo 'PT BINA CIPTA ABADI';
                }
                ?>
            </th>
            <td class="nodok" colspan="6">KBU/FO/HCD/17/034</td>
        </tr>
        <tr>
            <td class="nodok" colspan="6">Tingkatan dokument : IV</td>
        </tr>
        <tr>
            <th colspan="14"
                style="font-size: 16pt">APPRAISAL MANAGERIAL
            </th>
            <td class="nodok" colspan="6">Status Revisi : 00</td>
        </tr>
        <tr>
            <th class="dept" colspan="4">Human Capital</th>
            <td colspan="14">&nbsp;</td>
            <td class="nodok" colspan="6">Tgl. Mulai berlaku : 31 Agustus 2017</td>
        </tr>
        <tr>
            <th class="dept" colspan="4">Departement</th>
            <td colspan="14">&nbsp;</td>
            <td class="nodok" colspan="6">Halaman : 1 dari 1</td>
        </tr>
        <tr>
            <td class="top" colspan="24">&nbsp;</td>
            <br>
        </tr>

<?php
/**
 * Created by PhpStorm.
 * File Name: _print_appraisal_staff.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 12/11/2020
 * Time: 21:51
 */

use common\components\Bulan;
use common\components\Helper;
use frontend\models\Absensi;
use frontend\modules\personalia\models\Appraisal;
use frontend\modules\personalia\models\RefAppraisal;
use frontend\modules\personalia\models\SuratPeringatan;

/**
 * @var Appraisal $model
 * @var RefAppraisal $refAppraisal
 */
$perjanjianKerja = $model->perjanjianKerja;
$pegawaiGolongan = $model->pegawaiGolongan;
$pegawaiStruktur = $model->pegawaiStruktur;
$olehPegawaiStruktur = $model->olehPegawaiStruktur;
$countAllRow = 8;
$row = 0;
$maxRow = 8;

$headerWithoutPageBreak = $this->renderAjax('_header_print_appraisal_staff', ['model' => $model, 'pagebreak' => false]);
$header = $this->renderAjax('_header_print_appraisal_staff', ['model' => $model, 'pagebreak' => true]);
if ($countAllRow < $maxRow) {
    $header = $headerWithoutPageBreak;
}
echo $header;
?>
<tr>
    <td class="top text-align-top" colspan="4"><?= Yii::t('frontend', 'NIK') ?></td>
    <td class="top" colspan="8">
        : <?= $perjanjianKerja ? str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : ''; ?></td>
    <td class="top text-align-top" colspan="4"><?= Yii::t('frontend', 'Departement ') ?></td>
    <td class="top" colspan="8">
        : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : ''; ?>
    </td>
</tr>
<tr>
    <td class="text-align-top" colspan="4"><?= Yii::t('frontend', 'Nama Lengkap') ?></td>
    <td colspan="8"> : <?= $model->pegawai->nama_lengkap; ?></td>
    <td class="text-align-top" colspan="4"><?= Yii::t('frontend', 'Masa Evaluasi') ?></td>
    <td colspan="8">
        : <?= date('d-m-Y', strtotime($model->tanggal_awal)) . ' s/d ' . date('d-m-Y', strtotime($model->tanggal_akhir)) ?></td>
</tr>
<tr>
    <td class="text-align-top" colspan="4"><?= Yii::t('frontend', 'Jabatan ') ?></td>
    <td colspan="8">
        : <?= $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : ''; ?>
    </td>
    <td class="text-align-top" colspan="4"
        style="font-size: 11pt"><?= Yii::t('frontend', 'Tanggal Penilaian') ?></td>
    <td class="text-align-top" colspan="8"> : <?= Bulan::tanggal($model->tanggal_akhir) ?></td>
</tr>
<tr>
    <td class="text-align-top" colspan="4"><?= Yii::t('frontend', 'Jenis Evaluasi ') ?></td>
    <td colspan="8">
        : <?= $model->_jenis[$model->jenis_evaluasi] ?>
    </td>
    <td class="text-align-top" colspan="4"
        style="font-size: 11pt"><?= Yii::t('frontend', 'Jumlah Bulan') ?></td>
    <td colspan="8"> : <?= $model->intervalMonth() ?></td>
</tr>
<?php
/** @var RefAppraisal $item */
foreach ($refAppraisal as $item) {
    ?>
    <tr>
        <td class="top bottom content right text-align-top" rowspan="2" colspan="9">
            <?= $item->keterangan_kiri ?></td>
        <td class="center bottom top text-align-top" colspan="5">
            <?= $item->kriteria ?></td>
        <td class="top bottom content left text-align-top" rowspan="2" colspan="10">
            <?= $item->keterangan_kanan ?>
        </td>
    </tr>
    <tr>
        <td class="nilai-bobot" colspan="1">
            1<br><br><?= Appraisal::getPenilaian($model->id, $item->id) == 1 ? 'X' : '' ?></td>
        <td class="nilai-bobot" colspan="1">
            2<br><br><?= Appraisal::getPenilaian($model->id, $item->id) == 2 ? 'X' : '' ?></td>
        <td class="nilai-bobot" colspan="1">
            3<br><br><?= Appraisal::getPenilaian($model->id, $item->id) == 3 ? 'X' : '' ?></td>
        <td class="nilai-bobot" colspan="1">
            4<br><br><?= Appraisal::getPenilaian($model->id, $item->id) == 4 ? 'X' : '' ?></td>
        <td class="nilai-bobot" colspan="1">
            5<br><br><?= Appraisal::getPenilaian($model->id, $item->id) == 5 ? 'X' : '' ?></td>
    </tr>
    <?php
    $row++;
    $countAllRow--;
    if ($row == $maxRow) {
//        echo "<tr><td colspan='24' style='border: 1px solid white'>&nbsp;</td></tr>";
        echo "</table>";
        if ($countAllRow > $maxRow) {
            echo $header;
        } else {
            echo $headerWithoutPageBreak;
        }
        $row = 0;
    }
} ?>

<tr>
    <td class="top right" colspan="6">Total Nilai</td>
    <td class="top" colspan="12"><?= $model->total_nilai ?></td>
</tr>
<tr>
    <td class="top right" colspan="6">Rata Rata</td>
    <td class="top" colspan="12"><?= $model->rata_rata ?></td>
</tr>
<tr>
    <td class="top" colspan="24">Disiplin, Ketepatan Waktu, dan Kehadiran :</td>
</tr>
<tr>
    <td class="top right" colspan="6">Alpa</td>
    <td class="top" colspan="12"><?= $model->hitungStatusKehadiran($model->pegawai_id, Absensi::ALFA) ?></td>
</tr>
<tr>
    <td class="top right" colspan="6">Izin</td>
    <td class="top" colspan="12"><?= $model->izin ?></td>
</tr>
<tr>
    <td class="top right" colspan="6">Sakit</td>
    <td class="top" colspan="12"><?= $model->izin ?></td>
</tr>
<tr>
    <td class="top right" colspan="6">Cuti</td>
    <td class="top" colspan="12"><?= $model->cuti ?></td>
</tr>
<tr>
    <td class="top right" colspan="6">Terlambat</td>
    <td class="top" colspan="12"><?= $model->terlambat ?></td>
</tr>
<tr>
    <td class="top" colspan="7">KOMENTAR TAMBAHAN :</td>
    <td class="top" colspan="17"><?= $model->komentar_tambahan ?></td>
</tr>
<tr>
    <td class="top" colspan="4">KESIMPULAN :</td>
    <td class="top" colspan="20"><?= $model->kesimpulan; ?></td>
</tr>
<tr>
    <td class="top" colspan="24">Penilaian Keseluruhan dan Rekomendasi :</td>
</tr>
<?php foreach ($model->_rekomendasi as $key => $val) { ?>
    <tr>
        <td colspan="8"></td>
        <td class="top" colspan="2" style="border: 1px solid"><?= $key == $model->rekomendasi ? 'X' : '' ?></td>
        <td colspan="12"><?= $val ?></td>
    </tr>
<?php } ?>
<tr>
    <td colspan="24"><br><br></td>
</tr>
<tr>
    <td colspan="6" style="border-top: 1px solid; text-align: center;" class="border-left">
        <?= Yii::t('frontend', 'Dibuat Oleh') ?>,
        <br><br><br>
    </td>
    <td colspan="1" style="border-top: 1px solid; text-align: center; border-left: 1px solid;"></td>
    <td colspan="10" style="border-top: 1px solid; text-align: center;">
        <?= Yii::t('frontend', 'Diketahui Oleh') ?>,
        <br><br><br>
    </td>
    <td colspan="1"
        style="border-top: 1px solid; text-align: center;">
    </td>
    <td colspan="6"
        style="border-left: 1px solid; border-top: 1px solid; text-align: center; border-right: 1px solid;">
        <?= Yii::t('frontend', 'Disetujui Oleh') ?>
        <br><br><br>
    </td>
</tr>
<tr>
    <td colspan="6"
        style="font-size: 10pt; border-left: 1px; text-align: center; border-bottom: 1px solid; font-size: 10pt"
        class="border-left border-right">
        <br><br><br>
        <strong><?= $model->atasanPegawai ? $model->atasanPegawai->nama : ''; ?></strong><br>
        (<?= $model->atasanPegawaiStruktur ? $model->atasanPegawaiStruktur->struktur ? $model->atasanPegawaiStruktur->struktur->name : '' : ''; ?>
        )
    </td>
    <td colspan="6"
        style="font-size: 10pt; text-align: center;  border-left: 1px solid; border-bottom: 1px solid;">
        <br><br><br>
        <strong><?= $model->pegawai->nama_lengkap; ?></strong><br>
        <?= Yii::t('frontend', '(Karyawan)') ?>
    </td>
    <td colspan="6"
        style="font-size: 10pt; text-align: center; border-bottom: 1px solid; border-right: 1px solid;">
        <br><br><br><br>
        <?= Yii::t('frontend', ' (Ka. Dept)') ?>
    </td>
    <td colspan="6"
        style="font-size: 10pt; text-align: center; border-right: 1px solid; border-left: 1px; border-bottom: 1px solid;">
        <br><br><br>
        <strong><?= $model->olehPegawai->nama_lengkap; ?></strong><br>
        <?= Yii::t('frontend', ' (HR. Dept/ BOD)') ?>
    </td>
</tr>

<tr>
    <td colspan="24">&nbsp;</td>
</tr>
<tr>
    <td colspan="24"><?= Yii::t('frontend', 'History Karyawan') ?></td>
</tr>
<tr>
    <td colspan="20" class="header-history"
        style="background: #f1e507;font-weight: bold;"><?= Yii::t('frontend', 'Daftar Perjanjian Kerja') ?></td>
</tr>
<tr>
    <td colspan="4" class="header-history"><?= Yii::t('frontend', 'Aksi') ?></td>
    <td colspan="4" class="header-history"><?= Yii::t('frontend', 'Jenis Kontrak') ?></td>
    <td colspan="4" class="header-history"><?= Yii::t('frontend', 'Bisnis Unit') ?></td>
    <td colspan="4" class="header-history"><?= Yii::t('frontend', 'Mulai') ?></td>
    <td colspan="4" class="header-history"><?= Yii::t('frontend', 'Akhir') ?></td>
</tr>
<?php foreach ($model->pegawai->perjanjianKerjas as $perjanjianKerja) { ?>
    <tr>
        <td colspan="4"
            class="detail-history"><?= frontend\modules\pegawai\Module::getAksi($perjanjianKerja->aksi); ?></td>
        <td colspan="4" class="detail-history">
            <?php
            if ($perjanjianKerja->pegawaiStrukturs) {
                echo $perjanjianKerja->jenis->nama;
            }
            ?>
        </td>
        <td colspan="4" class="detail-history">
            <?php
            if ($perjanjianKerja->pegawaiStrukturs) {
                echo $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
            }
            ?>
        </td>
        <td colspan="4" class="detail-history"><?= Helper::idDate($perjanjianKerja->mulai_berlaku); ?></td>
        <td colspan="4"
            class="detail-history"><?= is_null($perjanjianKerja->akhir_berlaku) ? '' : Helper::idDate($perjanjianKerja->akhir_berlaku) ?></td>
    </tr>
<?php } ?>
<tr>
    <td colspan="24">&nbsp;</td>
</tr>
<tr>
    <td colspan="16" class="header-history"
        style="background: #f81e09; font-weight: bold; color: cornsilk "><?= Yii::t('frontend', 'Daftar Surat Peringatan') ?></td>
</tr>
<tr>
    <td colspan="8" class="header-history"><?= Yii::t('frontend', 'Jenis') ?></td>
    <td colspan="4" class="header-history"><?= Yii::t('frontend', 'Mulai') ?></td>
    <td colspan="4" class="header-history"><?= Yii::t('frontend', 'Akhir') ?></td>
</tr>
<?php /** @var SuratPeringatan $suratPeringatan */
foreach ($model->pegawai->suratPeringatans as $suratPeringatan) { ?>
    <tr>
        <td colspan="8" class="detail-history"><?= $suratPeringatan->refSuratPeringatan->nama ?></td>
        <td colspan="4" class="detail-history"><?= Helper::idDate($suratPeringatan->mulai_berlaku); ?></td>
        <td colspan="4"
            class="detail-history"><?= is_null($suratPeringatan->akhir_berlaku) ? '' : Helper::idDate($suratPeringatan->akhir_berlaku) ?></td>
    </tr>
<?php } ?>
</table>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\search\AppraisalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-appraisal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'kategori')->checkbox() ?>

    <?= $form->field($model, 'jenis_evaluasi')->checkbox() ?>

    <?= $form->field($model, 'tanggal_awal')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('frontend', 'Choose Tanggal Awal'),
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'tanggal_akhir')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('frontend', 'Choose Tanggal Akhir'),
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?php /* echo $form->field($model, 'alpa')->textInput(['placeholder' => 'Alpa']) */ ?>

    <?php /* echo $form->field($model, 'izin')->textInput(['placeholder' => 'Izin']) */ ?>

    <?php /* echo $form->field($model, 'terlambat')->textInput(['placeholder' => 'Terlambat']) */ ?>

    <?php /* echo $form->field($model, 'cuti')->textInput(['placeholder' => 'Cuti']) */ ?>

    <?php /* echo $form->field($model, 'total_nilai')->textInput(['placeholder' => 'Total Nilai']) */ ?>

    <?php /* echo $form->field($model, 'rata_rata')->textInput(['placeholder' => 'Rata Rata']) */ ?>

    <?php /* echo $form->field($model, 'rekomendasi')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'komentar_tambahan')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'kesimpulan')->textInput(['maxlength' => true, 'placeholder' => 'Kesimpulan']) */ ?>

    <?php /* echo $form->field($model, 'pegawai_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\personalia\models\Pegawai::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('frontend', 'Choose Pegawai')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'perjanjian_kerja_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\personalia\models\PerjanjianKerja::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('frontend', 'Choose Perjanjian kerja')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'pegawai_golongan_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\personalia\models\PegawaiGolongan::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('frontend', 'Choose Pegawai golongan')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'pegawai_struktur_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\personalia\models\PegawaiStruktur::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('frontend', 'Choose Pegawai struktur')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'oleh_pegawai_id')->textInput(['maxlength' => true, 'placeholder' => 'Oleh Pegawai']) */ ?>

    <?php /* echo $form->field($model, 'oleh_perjanjian_kerja_id')->textInput(['maxlength' => true, 'placeholder' => 'Oleh Perjanjian Kerja']) */ ?>

    <?php /* echo $form->field($model, 'oleh_pegawai_struktur_id')->textInput(['maxlength' => true, 'placeholder' => 'Oleh Pegawai Struktur']) */ ?>

    <?php /* echo $form->field($model, 'atasan_pegawai_id')->textInput(['maxlength' => true, 'placeholder' => 'Atasan Pegawai']) */ ?>

    <?php /* echo $form->field($model, 'atasan_pegawai_struktur_id')->textInput(['maxlength' => true, 'placeholder' => 'Atasan Pegawai Struktur']) */ ?>

    <?php /* echo $form->field($model, 'created_by_pk')->textInput(['maxlength' => true, 'placeholder' => 'Created By Pk']) */ ?>

    <?php /* echo $form->field($model, 'created_by_struktur')->textInput(['placeholder' => 'Created By Struktur']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('frontend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

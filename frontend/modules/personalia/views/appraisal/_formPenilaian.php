<?php

use frontend\modules\personalia\models\Appraisal;
use frontend\modules\personalia\models\AppraisalDetail;
use frontend\modules\personalia\models\RefAppraisal;
use kartik\rating\StarRating;
use kartik\widgets\ActiveForm;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\Appraisal */
/* @var $modelAppraisalDetail AppraisalDetail */
/* @var $refAppraisal ActiveRecord */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('frontend', 'Create {modelClass}: ', [
        'modelClass' => 'Appraisal',
    ]) . ' ' . $model->pegawai->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Appraisal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$js = <<< JS
$(function() {
    $('#form-appraisal').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });    
})
JS;

$this->registerJs($js);
?>

<div class="appraisal-form">
    <div class="row">
        <div class="col-sm-4">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'attribute' => 'id_pegawai',
                    'value' => function (Appraisal $model) {
                        return $model->perjanjianKerja->id_pegawai;
                    },
                ],
                [
                    'attribute' => 'pegawai_id',
                    'label' => Yii::t('frontend', 'Pegawai'),
                    'value' => function (Appraisal $model) {
                        return $model->pegawai->nama_lengkap;
                    },
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'fungsi_jabatan',
                    'value' => function (Appraisal $model) {
                        if ($pegawaiStruktur = $model->pegawaiStruktur) {
                            return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->fungsiJabatan ? $pegawaiStruktur->struktur->fungsiJabatan->nama : '' : '' : '';
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => 'jenis_evaluasi',
                    'value' => function (Appraisal $model) {
                        return $model->_jenis[$model->jenis_evaluasi];
                    },
                ],
                [
                    'attribute' => 'golongan',
                    'label' => Yii::t('frontend', 'Golongan'),
                    'value' => function (Appraisal $model) {
                        if ($golongan = $model->pegawaiGolongan) {
                            return $golongan->golongan->nama;
                        }
                        return '';
                    },
                ],

            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            $gridColumn = [
                [
                    'attribute' => 'bisnis_unit',
                    'value' => function (Appraisal $model) {
                        if ($perjanjianKerja = $model->perjanjianKerja) {
                            return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => 'departemen',
                    'value' => function (Appraisal $model) {
                        if ($pegawaiStruktur = $model->pegawaiStruktur) {
                            return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : '';
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => 'masa_evaluasi',
                    'value' => function (Appraisal $model) {
                        return date('d-m-Y', strtotime($model->tanggal_awal)) . ' s/d ' . date('d-m-Y', strtotime($model->tanggal_akhir));
                    },
                ],
                [
                    'attribute' => 'rentang_waktu',
                    'value' => function (Appraisal $model) {
                        return $model->intervalMonth();
                    },
                ],
                [
                    'attribute' => 'due_date',
                    'value' => function (Appraisal $model) {
                        return Html::tag('strong', $model->dueDate(), ['class' => 'text-danger font-weight-bold']);
                    },
                    'format' => 'raw'
                ],
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>

    </div>
    <?php
    if (empty($model->appraisalDetails)) {
        if ($model->kategori == Appraisal::STAFF) {
            ?>
            <div class="row">
                <div class="col-md-10">

                    <?php $form = ActiveForm::begin([
                        'id' => 'form-appraisal',
                        'action' => Url::to(['appraisal/create-penilaian', 'id' => $modelAppraisalDetail->appraisal_id]),
                        'type' => ActiveForm::TYPE_VERTICAL,
                        'formConfig' => ['showErrors' => false],
                        'enableAjaxValidation' => true,
                        'validationUrl' => Url::to(['cou-validation-penilaian']),
                        'fieldConfig' => ['showLabels' => true],
                    ]); ?>

                    <?= $form->errorSummary($modelAppraisalDetail); ?>

                    <?php try { ?>

                        <?= $form->field($modelAppraisalDetail, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
                        <?= $form->field($modelAppraisalDetail, 'appraisal_id', ['template' => '{input}'])->hiddenInput(); ?>

                        <?php
                        /** @var RefAppraisal $item */
                        foreach ($refAppraisal as $item) {
                            ?>
                            <div class="col-md-12 box box-success" style="font-size: 12pt">
                                <div class="col-md-4">
                                    <?= $item['keterangan_kiri'] ?>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-12" style="text-align: center; border-bottom: 1px solid">
                                        <?= $item['kriteria'] ?>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-xs-2">
                                            <span>1</span>
                                            <?= Html::radio('nilai-' . $item->id, false, ['value' => 1]) ?>
                                        </div>
                                        <div class="col-xs-2">
                                            <span>2</span>
                                            <?= Html::radio('nilai-' . $item->id, false, ['value' => 2]) ?>
                                        </div>
                                        <div class="col-xs-2">
                                            <span>3</span>
                                            <?= Html::radio('nilai-' . $item->id, false, ['value' => 3]) ?>
                                        </div>
                                        <div class="col-xs-2">
                                            <span>4</span>
                                            <?= Html::radio('nilai-' . $item->id, false, ['value' => 4]) ?>
                                        </div>
                                        <div class="col-xs-2">
                                            <span>5</span>
                                            <?= Html::radio('nilai-' . $item->id, false, ['value' => 5]) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <?= $item['keterangan_kanan'] ?>
                                </div>
                            </div>

                        <?php } ?>


                        <div class="form-group">
                            <?= Html::submitButton($modelAppraisalDetail->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $modelAppraisalDetail->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                    <?php } catch (Exception $e) {
                        Yii::info($e->getMessage(), 'exception');
                    } ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
            <?php
        }
        if ($model->kategori == Appraisal::MANAGERIAL) {
            ?>
            <div class="row">
                <div class="col-md-10">

                    <?php $form = ActiveForm::begin([
                        'id' => 'form-appraisal',
                        'action' => Url::to(['appraisal/create-penilaian', 'id' => $modelAppraisalDetail->appraisal_id]),
                        'type' => ActiveForm::TYPE_VERTICAL,
                        'formConfig' => ['showErrors' => false],
                        'enableAjaxValidation' => true,
                        'validationUrl' => Url::to(['cou-validation-penilaian']),
                        'fieldConfig' => ['showLabels' => true],
                    ]); ?>

                    <?= $form->errorSummary($modelAppraisalDetail); ?>

                    <?php try { ?>

                        <?= $form->field($modelAppraisalDetail, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
                        <?= $form->field($modelAppraisalDetail, 'appraisal_id', ['template' => '{input}'])->hiddenInput(); ?>

                        <?php
                        /** @var RefAppraisal $item */
                        foreach ($refAppraisal as $item) {
                            ?>
                            <div class="col-md-12 box box-success" style="font-size: 12pt">
                                <div class="col-md-8">
                                <strong><?= $item['kriteria'] ?></strong><br>
                                    <?= $item['keterangan_kiri'] ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    echo $form->field($modelAppraisalDetail, 'nilai[' . $item->id . ']')->widget(StarRating::class,
                                        [
                                            'pluginOptions' => [
                                                'step' => 1,
                                                'showCaption' => false,
                                            ]
                                        ]);

                                    echo $form->field($modelAppraisalDetail, 'komentar[' . $item->id . ']')->textarea()
                                    ?>
                                </div>
                            </div>

                        <?php } ?>


                        <div class="form-group">
                            <?= Html::submitButton($modelAppraisalDetail->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $modelAppraisalDetail->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                    <?php } catch (Exception $e) {
                        print_r($e->getMessage());
                        Yii::info($e->getMessage(), 'exception');
                    } ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
            <?php
        }
    } else { ?>
        <div class="box-body">
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
                Penilaian sudah terisi dan data tidak bisa ubah, Terimakasih.
            </div>
        </div>
    <?php } ?>
</div>

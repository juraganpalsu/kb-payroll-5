<?php


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefNamaInstitusi */

$this->title = Yii::t('frontend', 'Create Ref Nama Institusi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Nama Institusi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-nama-institusi-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

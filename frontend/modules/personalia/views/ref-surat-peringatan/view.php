<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefSuratPeringatan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Surat Peringatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-surat-peringatan-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('frontend', 'Ref Surat Peringatan').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'catatan:ntext',
        'created_by_pk',
        'created_by_struktur',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerSuratPeringatan->totalCount){
    $gridColumnSuratPeringatan = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'mulai_berlaku',
            'akhir_berlaku',
            'gmp',
            'catatan:ntext',
                        'ref_peraturan_perusahaan_id',
            [
                'attribute' => 'perjanjianKerja.id',
                'label' => Yii::t('frontend', 'Perjanjian Kerja')
            ],
            [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('frontend', 'Pegawai')
            ],
            [
                'attribute' => 'pegawaiGolongan.id',
                'label' => Yii::t('frontend', 'Pegawai Golongan')
            ],
            [
                'attribute' => 'pegawaiStruktur.id',
                'label' => Yii::t('frontend', 'Pegawai Struktur')
            ],
            'created_by_pk',
            'created_by_struktur',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSuratPeringatan,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-surat-peringatan']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('frontend', 'Surat Peringatan')),
        ],
        'export' => false,
        'columns' => $gridColumnSuratPeringatan
    ]);
}
?>

    </div>
</div>

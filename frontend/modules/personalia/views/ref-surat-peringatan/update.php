<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefSuratPeringatan */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Ref Surat Peringatan',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Surat Peringatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="ref-surat-peringatan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

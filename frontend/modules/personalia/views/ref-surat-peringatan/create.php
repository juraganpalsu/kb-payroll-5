<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\personalia\models\RefSuratPeringatan */

$this->title = Yii::t('frontend', 'Create Ref Surat Peringatan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Ref Surat Peringatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-surat-peringatan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

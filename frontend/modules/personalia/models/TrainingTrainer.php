<?php

namespace frontend\modules\personalia\models;

use Yii;
use \frontend\modules\personalia\models\base\TrainingTrainer as BaseTrainingTrainer;

/**
 * This is the model class for table "training_trainer".
 */
class TrainingTrainer extends BaseTrainingTrainer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['training_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id'], 'required'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'training_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

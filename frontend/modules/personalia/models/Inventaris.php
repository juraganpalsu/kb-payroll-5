<?php

namespace frontend\modules\personalia\models;

use Yii;
use \frontend\modules\personalia\models\base\Inventaris as BaseInventaris;

/**
 * This is the model class for table "inventaris".
 */
class Inventaris extends BaseInventaris
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_diterima', 'ref_inventaris_id', 'pegawai_id'], 'required'],
            [['tanggal_diterima', 'tanggal_dikembalikan', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'ref_inventaris_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

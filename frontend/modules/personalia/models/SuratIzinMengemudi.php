<?php

namespace frontend\modules\personalia\models;

use Yii;
use \frontend\modules\personalia\models\base\SuratIzinMengemudi as BaseSuratIzinMengemudi;

/**
 * This is the model class for table "surat_izin_mengemudi".
 */
class SuratIzinMengemudi extends BaseSuratIzinMengemudi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor', 'ref_surat_izin_mengemudi_id', 'pegawai_id'], 'required'],
            [['nomor', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'ref_surat_izin_mengemudi_id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

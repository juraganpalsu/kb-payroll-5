<?php

namespace frontend\modules\personalia\models;

use frontend\modules\personalia\models\base\MppJumlah as BaseMppJumlah;
use Throwable;
use yii\db\StaleObjectException;

/**
 * This is the model class for table "mpp_jumlah".
 */
class MppJumlah extends BaseMppJumlah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jumlah', 'tahun', 'mpp_id'], 'required'],
            [['tahun', 'jumlah', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'mpp_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['tahun', 'unique', 'targetAttribute' => ['tahun', 'mpp_id']]
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes): void
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $modelMppAct = new MppAct();
            $modelMppAct->mpp_jumlah_id = $this->id;
            $modelMppAct->mpp_id = $this->mpp_id;
            $modelMppAct->jabatan_id = $this->mpp->jabatan_id;
            $modelMppAct->golongan_id = $this->mpp->golongan_id;
            $modelMppAct->jumlah = 0;
            if ($modelMppAct->save()) {
                $modelMppAct->saveMpp();
            }
        }
    }

    /**
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function beforeDelete(): bool
    {
        if ($modelMppAct = MppAct::findOne(['mpp_jumlah_id' => $this->id])) {
            $modelMppAct->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (!$insert) {
            $modelHistory = new MppJumlahHistory();
            $modelHistory->setAttributes($this->oldAttributes);
            $modelHistory->mpp_jumlah_id = $this->id;
            $modelHistory->save();
        }
        return parent::beforeSave($insert);
    }

}

<?php

namespace frontend\modules\personalia\models;

use frontend\modules\personalia\models\base\Mpp as BaseMpp;

/**
 * This is the model class for table "mpp".
 */
class Mpp extends BaseMpp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['golongan_id', 'jabatan_id', 'departemen_id'], 'required'],
            [['golongan_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['jabatan_id', 'departemen_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['jabatan_id', 'golongan_id'], 'unique', 'targetAttribute' => ['golongan_id', 'jabatan_id', 'departemen_id']]
        ];
    }

}

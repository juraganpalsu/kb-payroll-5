<?php

namespace frontend\modules\personalia\models;

use frontend\modules\pegawai\models\Pegawai;
use Yii;
use \frontend\modules\personalia\models\base\PenyelesaianKasus as BasePenyelesaianKasus;

/**
 * This is the model class for table "penyelesaian_kasus".
 *
 * @property string $namaPegawai
 * @property array $pegawai_ids
 */
class PenyelesaianKasus extends BasePenyelesaianKasus
{

    public $pegawai_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_kasus', 'masalah', 'pegawai_ids'], 'required'],
            [['tanggal_kasus', 'tanggal_selesai', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['masalah', 'keterangan'], 'string'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_ids', 'pegawaiAktifValidation'],
            [['tanggal_selesai'], 'akhirBerlakuValidation']
        ];
    }

    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->tanggal_kasus) {
            $this->addError($attribute, Yii::t('frontend', 'Tanggal Selesai Minimum Adalah {tanggal}', ['tanggal' => $this->tanggal_kasus]));
        }
    }

    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $error = '';
        foreach ($this->pegawai_ids as $pegawai_id) {
            $modelPegawai = Pegawai::findOne($pegawai_id);
            if ($modelPegawai && !empty($this->tanggal_kasus)) {
                if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_kasus)) {
                    $error .= Yii::t('frontend', '{pegawai} sedang tidak dalam perjanjian kerja aktif.', ['pegawai' => $modelPegawai->nama]) . ' - ';
                }
                if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal_kasus)) {
                    $error .= Yii::t('frontend', 'Golongan aktif untuk {pegawai} tidak ditemukan.', ['pegawai' => $modelPegawai->nama]) . ' - ';
                }
                if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_kasus)) {
                    $error .= Yii::t('frontend', 'Struktur aktif untuk {pegawai} tidak ditemukan.', ['pegawai' => $modelPegawai->nama]) . ' - ';
                }
            }
        }
        if (!empty($error)) {
            $this->addError($attribute, $error);
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if (!$insert) {
            PenyelesaianKasusDetail::deleteAll(['penyelesaian_kasus_id' => $this->id]);
        }
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        foreach ($this->pegawai_ids as $pegawai_id) {
            $modelPegawai = Pegawai::findOne($pegawai_id);
            $modelDetail = new PenyelesaianKasusDetail();
            $modelDetail->pegawai_id = $modelPegawai->id;
            if ($perjanjianKerja = $modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_kasus)) {
                $modelDetail->perjanjian_kerja_id = $perjanjianKerja->id;
            }
            if ($golongan = $modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal_kasus)) {
                $modelDetail->pegawai_golongan_id = $golongan->id;
            }
            if ($struktur = $modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_kasus)) {
                $modelDetail->pegawai_struktur_id = $struktur->id;
            }
            $modelDetail->penyelesaian_kasus_id = $this->id;
            $modelDetail->save();
        }
        return true;
    }

    /**
     * @return string
     */
    public function getNamaPegawai()
    {
        $nama = '';
        foreach ($this->penyelesaianKasusDetails as $penyelesaianKasusDetail) {
            if ($penyelesaianKasusDetail->pegawai) {
                if ($perjanjianKerja = $penyelesaianKasusDetail->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_kasus)) {
                    $nama .= $perjanjianKerja->pegawai->nama_lengkap . '-' . $perjanjianKerja->id_pegawai . '<br />';
                }
            }
        }
        return $nama;
    }

    /**
     * @return array
     */
    public function loadPegawaiSaatUpdate()
    {
        $details = [];
        foreach ($this->penyelesaianKasusDetails as $trainingDetail) {
            $details[$trainingDetail->pegawai_id] = $trainingDetail->pegawai->nama_lengkap . '-' . $trainingDetail->perjanjianKerja->id_pegawai;
        }
        $this->pegawai_ids = array_keys($details);
        return $details;
    }
}

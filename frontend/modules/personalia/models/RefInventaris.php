<?php

namespace frontend\modules\personalia\models;

use Yii;
use \frontend\modules\personalia\models\base\RefInventaris as BaseRefInventaris;

/**
 * This is the model class for table "ref_inventaris".
 */
class RefInventaris extends BaseRefInventaris
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama'], 'required'],
            [['catatan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 225],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

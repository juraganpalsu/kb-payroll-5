<?php

namespace frontend\modules\personalia\models;

use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use frontend\models\Absensi;
use frontend\models\User;
use frontend\modules\izin\models\IzinJenis;
use frontend\modules\izin\models\IzinTanpaHadir;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\search\AppraisalSearch;
use Yii;
use \frontend\modules\personalia\models\base\Appraisal as BaseAppraisal;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "appraisal".
 * @property array $_kategori
 * @property array $_jenis
 * @property array $_rekomendasi
 * @property array $TtdOleh
 */
class Appraisal extends BaseAppraisal
{
    const STAFF = 1;
    const MANAGERIAL = 2;

    public $_kategori = [self::STAFF => 'STAFF', self::MANAGERIAL => 'MANAGERIAL'];

    const PROMOSI = 1;
    const HABIS_KONTRAK = 2;

    public $_jenis = [self::PROMOSI => 'PROMOSI', self::HABIS_KONTRAK => 'HABIS KONTRAK'];

    const DIATAS_RATA_RATA = 1;
    const CUKUP = 2;
    const DIBAWAH_STANDAR_MAJU = 3;
    const DIBAWAH_STANDAR = 4;

    public $_rekomendasi = [self::DIATAS_RATA_RATA => 'Diatas Rata Rata', self::CUKUP => 'Cukup', self::DIBAWAH_STANDAR_MAJU => 'Dibawah Standar Namun Ada Potensi Kemajuan', self::DIBAWAH_STANDAR => 'Dibawah Standar'];

    public $nilai;
    public $ref_appraisal_id;
    public $appraisal_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ref_appraisal_id', 'appraisal_id', 'tanggal_awal', 'tanggal_akhir', 'kesimpulan', 'pegawai_id', 'oleh_pegawai_id', 'atasan_pegawai_id'], 'required'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['alpa', 'izin', 'terlambat', 'cuti', 'sakit', 'total_nilai', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['rata_rata'], 'number'],
            [['komentar_tambahan'], 'string'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'atasan_pegawai_id', 'atasan_pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['kategori', 'jenis_evaluasi'], 'string', 'max' => 1],
            [['kesimpulan'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['pegawai_id'], 'pegawaiAktifValidation'],
//            [['pegawai_id'], 'pegawaiDoubleValidation'],
            [['tanggal_akhir'], 'akhirBerlakuValidation']
        ];
    }

    /**
     * @return array|array[]
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['header'] = ['kategori', 'jenis_evaluasi', 'tanggal_awal', 'tanggal_akhir', 'pegawai_id', 'oleh_pegawai_id', 'atasan_pegawai_id'];
        $scenarios['penilaian'] = ['nilai', 'ref_appraisal_id', 'appraisal_id', 'komentar'];
        $scenarios['hasil'] = ['kesimpulan', 'rekomendasi', 'komentar_tambahan'];
        return $scenarios;
    }

    /**
     * @return array
     */
    public function getTtdOleh()
    {
        $query = Pegawai::find()
            ->andWhere(['IN', 'id', ['105f989c3f2211e9b9d41831bfb9b9e1', '583642363f2111e9b9d41831bfb9b9e1', 'a45441a8414511e9b9d41831bfb9b9e1']])->all();
        $datas = [];
        /** @var Pegawai $data */
        foreach ($query as $data) {
            $datas[$data->id] = $data->nama;
        }
        return $datas;
    }

    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->tanggal_awal) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->tanggal_awal]));
        }
    }

    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tanggal_awal)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_awal)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal_awal)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_awal)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function pegawaiDoubleValidation(string $attribute)
    {
        $tanggal_awal = $this->tanggal_awal;
        $tanggal_akhir = $this->tanggal_akhir;
        if (!empty($tanggal_awal) && !empty($tanggal_akhir) && !empty($this->pegawai)) {
            $query = self::find()
                ->andWhere(['pegawai_id' => $this->$attribute])
                ->andWhere('\'' . $tanggal_awal . '\' BETWEEN tanggal_awal AND tanggal_akhir OR \'' . $tanggal_akhir . '\' BETWEEN tanggal_awal AND tanggal_akhir OR tanggal_awal BETWEEN \'' . $tanggal_awal . '\' AND \'' . $tanggal_akhir . '\' OR tanggal_akhir BETWEEN \'' . $tanggal_awal . '\' AND \'' . $tanggal_akhir . '\' OR ( \'' . $tanggal_awal . '\' <= tanggal_awal AND \'' . $tanggal_akhir . '\' >= tanggal_akhir) OR ( \'' . $tanggal_awal . '\' >= tanggal_awal AND \'' . $tanggal_akhir . '\' <= tanggal_akhir)')
                ->count();
            if ($query) {
                $this->addError($attribute, Yii::t('frontend', 'Terdapat tanggal yg sama.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_awal)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal_awal)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal_awal)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        if ($perjanjianKerja = $this->olehPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_awal)) {
            $this->oleh_perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($struktur = $this->olehPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_awal)) {
            $this->oleh_pegawai_struktur_id = $struktur->id;
        }
        if ($struktur = $this->atasanPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_akhir)) {
            $this->atasan_pegawai_struktur_id = $struktur->id;
        }
//        $this->alpa = $this->hitungStatusKehadiran($this->pegawai_id, Absensi::ALFA);
        $this->izin = $this->hitungIjinTanpahadir($this->pegawai_id, 'ITH');
        $this->cuti = $this->hitungStatusKehadiran($this->pegawai_id, Absensi::CUTI);
        $this->sakit = $this->hitungIjinTanpahadir($this->pegawai_id, 'SD') + $this->hitungIjinTanpahadir($this->pegawai_id, 'IS');
        $this->terlambat = $this->hitungJumlahTelat($this->pegawai_id);
        return true;
    }

    /**
     * @return PerjanjianKerja
     */
    public function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @param string $pegawaiId
     * @return ActiveQuery
     */
    public function queryAbsensi(string $pegawaiId)
    {
        $tanggalAwal = new DateTime($this->tanggal_awal);
        $tanggalAkhir = new DateTime($this->tanggal_akhir);
        $tanggalAwal->modify('+2 day');
        $tanggalAkhir->modify('-60 day');

        $query = Absensi::find();
        $query->andWhere(['BETWEEN', 'absensi.tanggal', $tanggalAwal->format('Y-m-d'), $tanggalAkhir->format('Y-m-d')]);
        $query->andWhere(['absensi.pegawai_id' => $pegawaiId]);
        $query->orderBy('absensi.tanggal ASC');
        return $query;
    }

    /**
     * @param string $pegawaiId
     * @param int $statusKehadiran
     * @return bool|int|string|null
     */
    public function hitungStatusKehadiran(string $pegawaiId, int $statusKehadiran = 0)
    {
        $query = $this->queryAbsensi($pegawaiId);
        if ($statusKehadiran > 0) {
            $query->andWhere(['absensi.status_kehadiran' => $statusKehadiran]);
        }
        return $query->count();
    }

    /**
     * @param string $pegawaiId
     * @return bool|int|string|null
     */
    public function hitungJumlahTelat(string $pegawaiId)
    {
        $query = $this->queryAbsensi($pegawaiId);
        return $query->andWhere(['>', 'telat', 0])->count();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function intervalMonth()
    {
        $date1 = new DateTime($this->tanggal_awal);
        $date2 = new DateTime($this->tanggal_akhir);
        $date2->modify('+1 day');

        $interval = $date2->diff($date1);

        return $interval->format('%y Tahun, %m Bulan');
    }

    /**
     * @return string
     * @throws Exception
     */
    public function dueDate()
    {
        $date2 = new DateTime($this->tanggal_akhir);
        $date2->modify('-2 weeks');
        return $date2->format('d-M-Y');
    }

    /**
     * @param string $pegawaiId
     * @param string $jenis id dari table izin_jenis
     * @param bool $asArray
     * @return array|false|int
     */
    public function hitungIjinTanpahadir(string $pegawaiId, string $jenis, bool $asArray = false)
    {
        $query = IzinTanpaHadir::find();
        $jenis = IzinJenis::getConfig($jenis);
        $query->andWhere(['pegawai_id' => $pegawaiId, 'izin_jenis_id' => $jenis->id]);
        $tanggalAwal = $this->tanggal_awal;
        $tanggalAkhir = $this->tanggal_akhir;
        $query->andWhere('\'' . $tanggalAwal . '\' BETWEEN izin_tanpa_hadir.tanggal_mulai AND izin_tanpa_hadir.tanggal_selesai OR \'' . $tanggalAkhir . '\' BETWEEN izin_tanpa_hadir.tanggal_mulai AND izin_tanpa_hadir.tanggal_selesai OR izin_tanpa_hadir.tanggal_mulai BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR izin_tanpa_hadir.tanggal_selesai BETWEEN \'' . $tanggalAwal . '\' AND \'' . $tanggalAkhir . '\' OR ( \'' . $tanggalAwal . '\' <= izin_tanpa_hadir.tanggal_mulai AND \'' . $tanggalAkhir . '\' >= izin_tanpa_hadir.tanggal_selesai) OR ( \'' . $tanggalAwal . '\' >= izin_tanpa_hadir.tanggal_mulai AND \'' . $tanggalAkhir . '\' <= izin_tanpa_hadir.tanggal_selesai)');
        $dataIjin = $query->all();
        $jumlahIjin = 0;
        $tanggalIzin = [];
        if (!empty($dataIjin)) {
            /** @var IzinTanpaHadir $ijinTanpaHadir */
            foreach ($dataIjin as $ijinTanpaHadir) {
                try {
                    $tglMulai = $ijinTanpaHadir->tanggal_mulai;
                    if ($ijinTanpaHadir->tanggal_mulai < $tanggalAwal) {
                        $tglMulai = $tanggalAwal;
                    }
                    $tglAkhir = $ijinTanpaHadir->tanggal_selesai;
                    if ($ijinTanpaHadir->tanggal_selesai > $tanggalAkhir) {
                        $tglAkhir = $tanggalAkhir;
                    }
                    $mulai = new DateTime($tglMulai);
                    $akhir = new DateTime($tglAkhir);
                    $akhir = $akhir->modify('+1 day');
                    $jumlahIjin += $mulai->diff($akhir)->days;
                    $period = new DatePeriod($mulai, new DateInterval('P1D'), $akhir);
                    foreach ($period as $date) {
                        $tanggalIzin[] = $date->format('Y-m-d');
                    }
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        }
        if ($asArray) {
            return ['jumlahIzin' => $jumlahIjin, 'tanggalIzin' => $tanggalIzin];
        }
        return $jumlahIjin;
    }

    /**
     * @param string $appraisalId
     * @param string $refAppraisalId
     * @param bool $asModel
     * @return AppraisalDetail|int|null
     */
    public static function getPenilaian(string $appraisalId, string $refAppraisalId, bool $asModel = false)
    {
        $model = AppraisalDetail::findOne(['appraisal_id' => $appraisalId, 'ref_appraisal_id' => $refAppraisalId]);
        if ($asModel) {
            return $model;
        }
        if ($model) {
            return $model->nilai;
        }
        return 0;
    }

    /**
     * @return int
     */
    public static function jumlahAppraisal(): int
    {
        if (User::pegawai()) {
            $modelSplSearch = new AppraisalSearch();
            $count = $modelSplSearch->searchAppraisal(false)->count();
            if ($count > 0) {
                return $count;
            }
        }
        return 0;
    }

}
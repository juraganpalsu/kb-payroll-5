<?php

namespace frontend\modules\personalia\models;

use frontend\modules\personalia\models\base\MppJumlahHistory as BaseMppJumlahHistory;

/**
 * This is the model class for table "mpp_jumlah_history".
 */
class MppJumlahHistory extends BaseMppJumlahHistory
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['mpp_id', 'mpp_jumlah_id'], 'required'],
            [['tahun', 'jumlah', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'mpp_id', 'mpp_jumlah_id'], 'string', 'max' => 36],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

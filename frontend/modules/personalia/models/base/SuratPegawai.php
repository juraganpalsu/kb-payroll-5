<?php

namespace frontend\modules\personalia\models\base;

use common\components\CreatedByBehavior;
use common\models\Golongan;
use frontend\models\User;
use frontend\modules\pegawai\models\AreaKerja;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\RefAreaKerja;
use frontend\modules\struktur\models\Struktur;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "surat_pegawai".
 *
 * @property string $id
 * @property integer $seq_code
 * @property integer $jenis
 * @property string $tgl_mulai
 * @property string $tgl_akhir
 * @property string $tujuan_surat
 * @property integer $struktur_id
 * @property integer $ref_area_kerja_id
 * @property integer $golongan_id
 * @property integer $jenis_perjanjian
 * @property integer $kontrak
 * @property string $catatan
 * @property string $pegawai_id
 * @property string $pegawai_golongan_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_struktur_id
 * @property string $area_kerja_id
 * @property string $oleh_pegawai_id
 * @property string $oleh_perjanjian_kerja_id
 * @property string $oleh_pegawai_struktur_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Golongan $golongan
 * @property Pegawai $pegawai
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 * @property Struktur $struktur
 * @property AreaKerja $areaKerja
 * @property RefAreaKerja $refAreaKerja
 *
 * @property Pegawai $olehPegawai
 * @property PegawaiStruktur $olehPegawaiStruktur
 * @property PerjanjianKerja $olehPerjanjianKerja
 */
class SuratPegawai extends ActiveRecord
{
    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0])->orderBy('created_at DESC');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat_pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'pegawai_golongan_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'area_kerja_id'], 'required'],
            [['seq_code', 'jenis', 'ref_area_kerja_id', 'struktur_id', 'golongan_id', 'jenis_perjanjian', 'kontrak', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tgl_mulai', 'tgl_akhir', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['id', 'pegawai_id', 'pegawai_golongan_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['tujuan_surat'], 'string', 'max' => 225],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'seq_code' => Yii::t('frontend', 'Seq Code'),
            'jenis' => Yii::t('frontend', 'Jenis'),
            'tgl_mulai' => Yii::t('frontend', 'Tgl Mulai'),
            'tgl_akhir' => Yii::t('frontend', 'Tgl Akhir'),
            'tujuan_surat' => Yii::t('frontend', 'Tujuan Surat'),
            'struktur_id' => Yii::t('frontend', 'Struktur ID'),
            'golongan_id' => Yii::t('frontend', 'Golongan ID'),
            'jenis_perjanjian' => Yii::t('frontend', 'Jenis Perjanjian'),
            'kontrak' => Yii::t('frontend', 'Kontrak'),
            'catatan' => Yii::t('frontend', 'Catatan'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'oleh_pegawai_id' => Yii::t('frontend', 'Oleh Pegawai ID'),
            'oleh_perjanjian_kerja_id' => Yii::t('frontend', 'Oleh Perjanjian Kerja ID'),
            'oleh_pegawai_struktur_id' => Yii::t('frontend', 'Oleh Pegawai Struktur ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAreaKerja()
    {
        return $this->hasOne(AreaKerja::class, ['id' => 'area_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRefAreaKerja()
    {
        return $this->hasOne(RefAreaKerja::class, ['id' => 'ref_area_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGolongan()
    {
        return $this->hasOne(Golongan::class, ['id' => 'golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStruktur()
    {
        return $this->hasOne(Struktur::class, ['id' => 'struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'oleh_pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'oleh_pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'oleh_perjanjian_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace frontend\modules\personalia\models\base;

use common\components\CreatedByBehavior;
use common\models\Pegawai;
use frontend\models\User;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "appraisal".
 *
 * @property string $id
 * @property integer $kategori
 * @property integer $jenis_evaluasi
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property integer $alpa
 * @property integer $izin
 * @property integer $terlambat
 * @property integer $cuti
 * @property integer $sakit
 * @property integer $total_nilai
 * @property double $rata_rata
 * @property integer $rekomendasi
 * @property string $komentar_tambahan
 * @property string $kesimpulan
 * @property integer $total_nilai_atasan_dua
 * @property double $rata_rata_atasan_dua
 * @property integer $rekomendasi_atasan_dua
 * @property string $komentar_tambahan_atasan_dua
 * @property string $kesimpulan_atasan_dua
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $oleh_pegawai_id
 * @property string $oleh_perjanjian_kerja_id
 * @property string $oleh_pegawai_struktur_id
 * @property string $atasan_pegawai_id
 * @property string $atasan_pegawai_struktur_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 *
 * @property string $atasan_dua_pegawai_id
 * @property string $atasan_dua_perjanjian_kerja_id
 * @property string $atasan_dua_pegawai_struktur_id
 * @property string $atasan_dua_pegawai_golongan_id
 *
 * @property Pegawai $atasanDuaPegawai
 * @property PerjanjianKerja $atasanDuaPerjanjianKerja
 * @property PegawaiStruktur $atasanDuaPegawaiStruktur
 * @property PegawaiGolongan $atasanDuaPegawaiGolongan
 *
 *
 * @property Pegawai $pegawai
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 *
 * @property Pegawai $olehPegawai
 * @property PegawaiStruktur $olehPegawaiStruktur
 * @property PerjanjianKerja $olehPerjanjianKerja
 * @property Pegawai $atasanPegawai
 * @property PegawaiStruktur $atasanPegawaiStruktur
 * @property \frontend\modules\personalia\models\AppraisalDetail[] $appraisalDetails
 * @property \frontend\modules\personalia\models\AppraisalDetail[] $appraisalDetailsAtasanSatu
 * @property \frontend\modules\personalia\models\AppraisalDetail[] $appraisalDetailsAtasanDua
 * @property User $createdBy
 *
 */
class Appraisal extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appraisal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_awal', 'tanggal_akhir', 'kesimpulan', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'required'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['alpa', 'izin', 'terlambat', 'cuti', 'sakit', 'total_nilai', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['rata_rata'], 'number'],
            [['komentar_tambahan'], 'string'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'atasan_pegawai_id', 'atasan_pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['kategori', 'jenis_evaluasi'], 'string', 'max' => 1],
            [['kesimpulan'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'kategori' => Yii::t('frontend', 'Kategori'),
            'jenis_evaluasi' => Yii::t('frontend', 'Jenis Evaluasi'),
            'tanggal_awal' => Yii::t('frontend', 'Tanggal Awal'),
            'tanggal_akhir' => Yii::t('frontend', 'Tanggal Akhir'),
            'alpa' => Yii::t('frontend', 'Alpa'),
            'izin' => Yii::t('frontend', 'Izin'),
            'terlambat' => Yii::t('frontend', 'Terlambat'),
            'cuti' => Yii::t('frontend', 'Cuti'),
            'sakit' => Yii::t('frontend', 'Sakit'),
            'total_nilai' => Yii::t('frontend', 'Total Nilai'),
            'rata_rata' => Yii::t('frontend', 'Rata Rata'),
            'rekomendasi' => Yii::t('frontend', 'Rekomendasi'),
            'komentar_tambahan' => Yii::t('frontend', 'Komentar Tambahan'),
            'kesimpulan' => Yii::t('frontend', 'Kesimpulan'),
            'total_nilai_atasan_dua' => Yii::t('frontend', 'Total Nilai Atasan Dua'),
            'rata_rata_atasan_dua' => Yii::t('frontend', 'Rata Rata Atasan Dua'),
            'rekomendasi_atasan_dua' => Yii::t('frontend', 'Rekomendasi Atasan Dua'),
            'komentar_tambahan_atasan_dua' => Yii::t('frontend', 'Komentar Tambahan Atasan Dua'),
            'kesimpulan_atasan_dua' => Yii::t('frontend', 'Kesimpulan Atasan Dua'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'oleh_pegawai_id' => Yii::t('frontend', 'Oleh Pegawai ID'),
            'oleh_perjanjian_kerja_id' => Yii::t('frontend', 'Oleh Perjanjian Kerja ID'),
            'oleh_pegawai_struktur_id' => Yii::t('frontend', 'Oleh Pegawai Struktur ID'),
            'atasan_pegawai_id' => Yii::t('frontend', 'Atasan Satu Pegawai'),
            'atasan_pegawai_struktur_id' => Yii::t('frontend', 'Atasan Pegawai Struktur ID'),
            'atasan_dua_pegawai_id' => Yii::t('frontend', 'Atasan Dua Pegawai'),
            'atasan_dua_perjanjian_kerja_id' => Yii::t('frontend', 'Atasan Dua Perjanjian Kerja'),
            'atasan_dua_pegawai_struktur_id' => Yii::t('frontend', 'Atasan Dua Pegawai Struktur'),
            'atasan_dua_pegawai_golongan_id' => Yii::t('frontend', 'Atasan Dua Pegawai Golongan'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAppraisalDetails()
    {
        return $this->hasMany(\frontend\modules\personalia\models\AppraisalDetail::class, ['appraisal_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAppraisalDetailsAtasanSatu()
    {
        $pegawaiId = 0;
        if ($pegawai = User::pegawai()) {
            $pegawaiId = $pegawai->id;
        }
        return $this->hasMany(\frontend\modules\personalia\models\AppraisalDetail::class, ['appraisal_id' => 'id'])->joinWith('appraisal')->andWhere(['appraisal.atasan_pegawai_id' => $pegawaiId, 'appraisal_detail.atasan_pegawai_id' => $pegawaiId]);
    }

    /**
     * @return ActiveQuery
     */
    public function getAppraisalDetailsAtasanDua()
    {
        $pegawaiId = 0;
        if ($pegawai = User::pegawai()) {
            $pegawaiId = $pegawai->id;
        }
        return $this->hasMany(\frontend\modules\personalia\models\AppraisalDetail::class, ['appraisal_id' => 'id'])->joinWith('appraisal')->andWhere(['appraisal.atasan_dua_pegawai_id' => $pegawaiId, 'appraisal_detail.atasan_pegawai_id' => $pegawaiId]);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'oleh_pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'oleh_pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'oleh_perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAtasanPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'atasan_pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAtasanPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'atasan_pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRefAppraisal()
    {
        return $this->hasOne(\frontend\modules\personalia\models\RefAppraisal::class, ['id' => 'ref_appraisal_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAtasanDuaPegawai(): ActiveQuery
    {
        return $this->hasOne(Pegawai::class, ['id' => 'atasan_dua_pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAtasanDuaPerjanjianKerja(): ActiveQuery
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'atasan_dua_perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAtasanDuaPegawaiStruktur(): ActiveQuery
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'atasan_dua_pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAtasanDuaPegawaiGolongan(): ActiveQuery
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'atasan_dua_pegawai_golongan_id']);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0])->orderBy('created_at DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

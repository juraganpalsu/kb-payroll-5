<?php

namespace frontend\modules\personalia\models\base;

use common\components\CreatedByBehavior;
use common\components\UUIDBehavior;
use common\models\Golongan;
use frontend\models\User;
use frontend\modules\struktur\models\Jabatan;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "mpp_act".
 *
 * @property string $id
 * @property integer $jumlah
 * @property integer $golongan_id
 * @property string $jabatan_id
 * @property string $mpp_id
 * @property string $mpp_jumlah_id
 * @property string $keterangan
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\personalia\models\Mpp $mpp
 * @property \frontend\modules\personalia\models\MppJumlah $mppJumlah
 * @property Golongan $golongan
 * @property Jabatan $jabatan
 */
class MppAct extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'golongan_id', 'jabatan_id', 'mpp_id', 'mpp_jumlah_id'], 'required'],
            [['jumlah', 'golongan_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'mpp_id', 'mpp_jumlah_id'], 'string', 'max' => 36],
            [['jabatan_id', 'created_by_pk'], 'string', 'max' => 32],
            [['keterangan'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mpp_act';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'jumlah' => Yii::t('frontend', 'Jumlah'),
            'golongan_id' => Yii::t('frontend', 'Golongan ID'),
            'jabatan_id' => Yii::t('frontend', 'Jabatan ID'),
            'mpp_id' => Yii::t('frontend', 'Mpp ID'),
            'mpp_jumlah_id' => Yii::t('frontend', 'Mpp Jumlah ID'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMpp()
    {
        return $this->hasOne(\frontend\modules\personalia\models\Mpp::class, ['id' => 'mpp_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMppJumlah()
    {
        return $this->hasOne(\frontend\modules\personalia\models\MppJumlah::class, ['id' => 'mpp_jumlah_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGolongan()
    {
        return $this->hasOne(Golongan::class, ['id' => 'golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getJabatan()
    {
        return $this->hasOne(Jabatan::class, ['id' => 'jabatan_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0])->orderBy(self::tableName() . '.created_at DESC');
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

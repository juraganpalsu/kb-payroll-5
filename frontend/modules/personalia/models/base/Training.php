<?php

namespace frontend\modules\personalia\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "training".
 *
 * @property string $id
 * @property integer $jenis_pelatihan
 * @property string $tanggal_pelaksanaan
 * @property string $durasi
 * @property integer $nilai_tes
 * @property integer $sertifikat
 * @property string $ref_nama_pelatihan_id
 * @property string $ref_trainer_id
 * @property string $ref_nama_institusi_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\modules\personalia\models\RefNamaInstitusi $refNamaInstitusi
 * @property \frontend\modules\personalia\models\RefNamaPelatihan $refNamaPelatihan
 * @property \frontend\modules\personalia\models\RefTrainer $refTrainer
 * @property \frontend\modules\personalia\models\TrainingDetail[] $trainingDetails
 * @property \frontend\modules\personalia\models\TrainingTrainer[] $trainingTrainers
 */
class Training extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_pelaksanaan', 'durasi', 'nilai_tes', 'ref_nama_pelatihan_id', 'ref_trainer_id', 'ref_nama_institusi_id'], 'required'],
            [['jenis_pelatihan', 'nilai_tes', 'sertifikat', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal_pelaksanaan', 'durasi', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'ref_nama_pelatihan_id', 'ref_trainer_id', 'ref_nama_institusi_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'jenis_pelatihan' => Yii::t('frontend', 'Jenis Pelatihan'),
            'tanggal_pelaksanaan' => Yii::t('frontend', 'Tanggal Pelaksanaan'),
            'durasi' => Yii::t('frontend', 'Durasi'),
            'nilai_tes' => Yii::t('frontend', 'Nilai Tes'),
            'sertifikat' => Yii::t('frontend', 'Sertifikat'),
            'ref_nama_pelatihan_id' => Yii::t('frontend', 'Ref Nama Pelatihan ID'),
            'ref_trainer_id' => Yii::t('frontend', 'Ref Trainer ID'),
            'ref_nama_institusi_id' => Yii::t('frontend', 'Ref Nama Institusi ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getRefNamaInstitusi()
    {
        return $this->hasOne(\frontend\modules\personalia\models\RefNamaInstitusi::class, ['id' => 'ref_nama_institusi_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRefNamaPelatihan()
    {
        return $this->hasOne(\frontend\modules\personalia\models\RefNamaPelatihan::class, ['id' => 'ref_nama_pelatihan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRefTrainer()
    {
        return $this->hasOne(\frontend\modules\personalia\models\RefTrainer::class, ['id' => 'ref_trainer_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTrainingDetails()
    {
        return $this->hasMany(\frontend\modules\personalia\models\TrainingDetail::class, ['training_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTrainingTrainers()
    {
        return $this->hasMany(\frontend\modules\personalia\models\TrainingTrainer::class, ['training_id' => 'id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}
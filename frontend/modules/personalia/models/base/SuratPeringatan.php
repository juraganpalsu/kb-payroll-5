<?php

namespace frontend\modules\personalia\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "surat_peringatan".
 *
 * @property string $id
 * @property string $mulai_berlaku
 * @property string $akhir_berlaku
 * @property integer $gmp
 * @property string $catatan
 * @property string $ref_surat_peringatan_id
 * @property string $ref_peraturan_perusahaan_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_id
 * @property string $pegawai_golongan_id
 * @property string $pegawai_struktur_id
 * @property string $oleh_pegawai_id
 * @property string $oleh_perjanjian_kerja_id
 * @property string $oleh_pegawai_struktur_id
 * @property string $atasan_pegawai_id
 * @property string $atasan_pegawai_struktur_id
 * @property string $created_at
 * @property integer $created_by
 * @property User $createdBy
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Pegawai $pegawai
 * @property PegawaiGolongan $pegawaiGolongan
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 * @property \frontend\modules\personalia\models\RefSuratPeringatan $refSuratPeringatan
 * @property \frontend\modules\personalia\models\RefPeraturanPerusahaan $refPeraturanPerusahaan
 *
 * @property Pegawai $olehPegawai
 * @property PegawaiStruktur $olehPegawaiStruktur
 * @property PerjanjianKerja $olehPerjanjianKerja
 * @property Pegawai $atasanPegawai
 * @property PegawaiStruktur $atasanPegawaiStruktur
 */
class SuratPeringatan extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mulai_berlaku', 'akhir_berlaku', 'ref_surat_peringatan_id', 'perjanjian_kerja_id', 'pegawai_id', 'pegawai_golongan_id', 'pegawai_struktur_id'], 'required'],
            [['mulai_berlaku', 'akhir_berlaku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'ref_surat_peringatan_id', 'perjanjian_kerja_id', 'pegawai_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'atasan_pegawai_id', 'atasan_pegawai_struktur_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat_peringatan';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'mulai_berlaku' => Yii::t('frontend', 'Mulai Berlaku'),
            'akhir_berlaku' => Yii::t('frontend', 'Akhir Berlaku'),
            'gmp' => Yii::t('frontend', 'GMP'),
            'catatan' => Yii::t('frontend', 'Catatan'),
            'ref_surat_peringatan_id' => Yii::t('frontend', 'Ref Surat Peringatan ID'),
            'ref_peraturan_perusahaan_id' => Yii::t('frontend', 'Ref Peraturan Perusahaan ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_id' => Yii::t('frontend', 'Pegawai ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Pegawai Golongan ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'oleh_pegawai_id' => Yii::t('frontend', 'TTD Oleh Pegawai'),
            'oleh_perjanjian_kerja_id' => Yii::t('frontend', 'Oleh Perjanjian Kerja ID'),
            'oleh_pegawai_struktur_id' => Yii::t('frontend', 'Oleh Pegawai Struktur ID'),
            'atasan_pegawai_id' => Yii::t('frontend', 'Atasan Pegawai'),
            'atasan_pegawai_struktur_id' => Yii::t('frontend', 'Oleh Pegawai Struktur ID'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRefSuratPeringatan()
    {
        return $this->hasOne(\frontend\modules\personalia\models\RefSuratPeringatan::class, ['id' => 'ref_surat_peringatan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRefPeraturanPerusahaan()
    {
        return $this->hasOne(\frontend\modules\personalia\models\RefPeraturanPerusahaan::class, ['id' => 'ref_peraturan_perusahaan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'oleh_pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'oleh_pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOlehPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'oleh_perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAtasanPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'atasan_pegawai_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAtasanPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'atasan_pegawai_struktur_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0])->orderBy('created_at DESC');
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}


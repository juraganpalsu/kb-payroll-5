<?php

namespace frontend\modules\personalia\models;

use Yii;
use \frontend\modules\personalia\models\base\DataNcr as BaseDataNcr;

/**
 * This is the model class for table "data_ncr".
 *
 *
 * @property array $_lembaga
 * @property string $lembaga_
 */
class DataNcr extends BaseDataNcr
{

    const ISO = 1;
    const PIZZA_HUT = 2;
    const SSP = 3;
    const BPOM = 4;
    const AIB = 5;
    const BPJS_TK = 6;
    const BPJS_KES = 7;
    const DISNAKER = 8;

    public $_lembaga = [self::ISO => 'ISO', self::PIZZA_HUT => 'PIZZA HUT', self::SSP => 'SSP', self::BPOM => 'BPOM', self::AIB => 'AIB',
        self::BPJS_TK => 'BPJS TK', self::BPJS_KES => 'BPJS Kes', self::DISNAKER => 'DISNAKER'
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_temuan', 'lembaga', 'keterangan_temuan'], 'required'],
            [['tanggal_temuan', 'tanggal_selesai', 'keterangan_penyelesaian', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan_temuan', 'keterangan_penyelesaian'], 'string'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['lembaga'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return mixed
     */
    public function getLembaga_()
    {
        return $this->_lembaga[$this->lembaga];
    }

}

<?php

namespace frontend\modules\personalia\models;

use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\personalia\models\base\Training as BaseTraining;

/**
 * This is the model class for table "training".
 *
 * @property array $_jenisPelatihan
 * @property string $jenisPelatihan
 * @property array $pegawai_ids
 * @property array $trainers
 */
class Training extends BaseTraining
{

    const INTERNAL = 1;
    const EKSTERNAL = 2;

    public $_jenisPelatihan = [self::INTERNAL => 'INTERNAL', self::EKSTERNAL => 'EKSTERNAL'];

    public $pegawai_ids;
    public $trainers;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_pelatihan', 'pegawai_ids', 'tanggal_pelaksanaan', 'durasi', 'nilai_tes', 'ref_nama_pelatihan_id', 'ref_nama_institusi_id'], 'required'],
            [['jenis_pelatihan', 'nilai_tes', 'sertifikat', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['ref_trainer_id', 'trainers', 'tanggal_pelaksanaan', 'durasi', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'ref_nama_pelatihan_id', 'ref_trainer_id', 'ref_nama_institusi_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     *
     * @return string jenis pelatihan
     */
    public function getJenisPelatihan()
    {
        return $this->_jenisPelatihan[$this->jenis_pelatihan];
    }

    /**
     * @return bool|void
     */
    public function beforeValidate()
    {
        parent::beforeValidate();
        if ($this->jenis_pelatihan == self::INTERNAL) {
            $this->ref_trainer_id = '-';
        }

        if ($this->jenis_pelatihan == self::EKSTERNAL) {
            $this->trainers = [];
        }

        return true;
    }

    /**
     *
     * Hapus semua isinya, masukkan yg baru
     *
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if (!$insert) {
            TrainingDetail::deleteAll(['training_id' => $this->id]);
            TrainingTrainer::deleteAll(['training_id' => $this->id]);
        }
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->pegawai_ids) {
            foreach ($this->pegawai_ids as $trainer) {
                $modelPegawai = Pegawai::findOne($trainer);
                if ($modelPegawai) {
                    $perjanjianKerja = $modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_pelaksanaan);
                    $golongan = $modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal_pelaksanaan);
                    $struktur = $modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_pelaksanaan);
                    if ($perjanjianKerja && $golongan && $struktur) {
                        $modelDetail = new TrainingDetail();
                        $modelDetail->training_id = $this->id;
                        $modelDetail->pegawai_id = $modelPegawai->id;
                        $modelDetail->perjanjian_kerja_id = $perjanjianKerja->id;
                        $modelDetail->pegawai_golongan_id = $golongan->id;
                        $modelDetail->pegawai_struktur_id = $struktur->id;
                        $modelDetail->save();
                    }
                }
            }
        }
        if ($this->trainers) {
            foreach ($this->trainers as $trainer) {
                $modelPegawai = Pegawai::findOne($trainer);
                if ($modelPegawai) {
                    $perjanjianKerja = $modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal_pelaksanaan);
                    $golongan = $modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal_pelaksanaan);
                    $struktur = $modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal_pelaksanaan);
                    if ($perjanjianKerja && $golongan && $struktur) {
                        $modelDetail = new TrainingTrainer();
                        $modelDetail->training_id = $this->id;
                        $modelDetail->pegawai_id = $modelPegawai->id;
                        $modelDetail->perjanjian_kerja_id = $perjanjianKerja->id;
                        $modelDetail->pegawai_golongan_id = $golongan->id;
                        $modelDetail->pegawai_struktur_id = $struktur->id;
                        $modelDetail->save();
                    }
                }
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function loadPegawaiSaatUpdate()
    {
        $details = [];
        foreach ($this->trainingDetails as $trainingDetail) {
            $details[$trainingDetail->pegawai_id] = $trainingDetail->pegawai->nama_lengkap . '-' . $trainingDetail->perjanjianKerja->id_pegawai;
        }
        $this->pegawai_ids = array_keys($details);
        return $details;
    }


    /**
     * @return string
     */
    public function daftarNamaPegawai()
    {
        return implode(', ', $this->loadPegawaiSaatUpdate());
    }

    /**
     * @return array
     */
    public function loadTrainerSaatUpdate()
    {
        $trainers = [];
        foreach ($this->trainingTrainers as $trainingTrainer) {
            $trainers[$trainingTrainer->pegawai_id] = $trainingTrainer->pegawai->nama_lengkap . '-' . $trainingTrainer->perjanjianKerja->id_pegawai;
        }
        $this->trainers = array_keys($trainers);
        return $trainers;
    }

    /**
     * @return string
     */
    public function daftarNamaTrainer()
    {
        return implode(', ', $this->loadTrainerSaatUpdate());
    }

}

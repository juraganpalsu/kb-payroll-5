<?php

namespace frontend\modules\personalia\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\personalia\models\KegiatanHr;

/**
 * frontend\modules\personalia\models\search\KegiatanHrSearch represents the model behind the search form about `frontend\modules\personalia\models\KegiatanHr`.
 */
class KegiatanHrSearch extends KegiatanHr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama', 'dari', 'sampai', 'peserta', 'lokasi', 'catatan', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['jenis_kegiatan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KegiatanHr::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'jenis_kegiatan' => $this->jenis_kegiatan,
        ]);

        if (!empty($this->dari)) {
            $tanggal = explode('s/d', $this->dari);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'dari', $awal, $akhir]);
        }

        if (!empty($this->sampai)) {
            $tanggal = explode('s/d', $this->sampai);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'sampai', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'peserta', $this->peserta])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

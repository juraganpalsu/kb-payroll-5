<?php

namespace frontend\modules\personalia\models\search;

use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\SuratPeringatan;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\personalia\models\search\SuratPeringatanSearch represents the model behind the search form about `frontend\modules\personalia\models\SuratPeringatan`.
 */
class SuratPeringatanSearch extends SuratPeringatan
{
    /**
     * @var int
     */
    public $id_pegawai;

    public $busines_unit;

    public $golongan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pegawai', 'mulai_berlaku', 'akhir_berlaku', 'busines_unit', 'gmp', 'catatan', 'ref_surat_peringatan_id', 'ref_peraturan_perusahaan_id', 'perjanjian_kerja_id', 'pegawai_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['id_pegawai', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'golongan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SuratPeringatan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $perjanjianKerja) {
                $perjanjianKerja->andWhere(['kontrak' => $this->busines_unit]);
            }]);
        }

        if (!empty($this->mulai_berlaku)) {
            $tanggal = explode('s/d', $this->mulai_berlaku);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'surat_peringatan.mulai_berlaku', $awal, $akhir]);
        }

        if (!empty($this->akhir_berlaku)) {
            $tanggal = explode('s/d', $this->akhir_berlaku);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'surat_peringatan.akhir_berlaku', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['ref_surat_peringatan_id' => $this->ref_surat_peringatan_id])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['like', 'ref_peraturan_perusahaan_id', $this->ref_peraturan_perusahaan_id])
            ->andFilterWhere(['gmp' => $this->gmp]);

        $query->orderBy('created_at DESC');

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchBca(array $params)
    {
        $query = SuratPeringatan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        $query->joinWith('perjanjianKerja');
        $query->andWhere(['IN', 'perjanjian_kerja.kontrak', [PerjanjianKerja::BCA_ADA, PerjanjianKerja::BCA_KBU]]);


        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->mulai_berlaku)) {
            $tanggal = explode('s/d', $this->mulai_berlaku);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'surat_peringatan.mulai_berlaku', $awal, $akhir]);
        }

        if (!empty($this->akhir_berlaku)) {
            $tanggal = explode('s/d', $this->akhir_berlaku);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'surat_peringatan.akhir_berlaku', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['ref_surat_peringatan_id' => $this->ref_surat_peringatan_id])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['ref_peraturan_perusahaan_id' => $this->ref_peraturan_perusahaan_id])
            ->andFilterWhere(['gmp' => $this->gmp]);

        $query->orderBy('created_at DESC');

        return $dataProvider;
    }
}

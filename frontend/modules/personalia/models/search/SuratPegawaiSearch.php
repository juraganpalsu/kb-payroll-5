<?php

namespace frontend\modules\personalia\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\personalia\models\SuratPegawai;
use yii\db\ActiveQuery;

/**
 * frontend\modules\personalia\models\search\SuratPegawaiSearch represents the model behind the search form about `frontend\modules\personalia\models\SuratPegawai`.
 * @property array $_jenis
 */
class SuratPegawaiSearch extends SuratPegawai
{
    public $id_pegawai;

    public $busines_unit;

    public $golongan;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tgl_mulai', 'tgl_akhir', 'tujuan_surat', 'catatan', 'pegawai_id', 'pegawai_golongan_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['id_pegawai', 'busines_unit', 'golongan', 'jenis', 'struktur_id', 'golongan_id', 'jenis_perjanjian', 'kontrak', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SuratPegawai::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['LIKE', 'perjanjian_kerja.id_pegawai', $this->id_pegawai]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith('perjanjianKerja');
            $query->andWhere(['perjanjian_kerja.kontrak' => $this->busines_unit]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        if (!empty($this->tgl_mulai)) {
            $tanggal = explode('s/d', $this->tgl_mulai);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tgl_mulai', $awal, $akhir]);
        }

        if (!empty($this->tgl_akhir)) {
            $tanggal = explode('s/d', $this->tgl_akhir);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tgl_akhir', $awal, $akhir]);
        }

        $query->andFilterWhere([
            'jenis' => $this->jenis,
            'struktur_id' => $this->struktur_id,
            'golongan_id' => $this->golongan_id,
            'jenis_perjanjian' => $this->jenis_perjanjian,
            'kontrak' => $this->kontrak,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'created_by_struktur' => $this->created_by_struktur,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'tujuan_surat', $this->tujuan_surat])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);

        return $dataProvider;
    }
}

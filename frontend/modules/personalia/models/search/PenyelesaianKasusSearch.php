<?php

namespace frontend\modules\personalia\models\search;

use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\personalia\models\PenyelesaianKasus;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\personalia\models\search\PenyelesaianKasusSearch represents the model behind the search form about `frontend\modules\personalia\models\PenyelesaianKasus`.
 */
class PenyelesaianKasusSearch extends PenyelesaianKasus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_kasus', 'masalah', 'tanggal_selesai', 'keterangan', 'pegawai_ids', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PenyelesaianKasus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if (!empty($this->tanggal_kasus)) {
            $tanggal = explode('s/d', $this->tanggal_kasus);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'penyelesaian_kasus.tanggal_kasus', $awal, $akhir]);
        }

        if (!empty($this->tanggal_selesai)) {
            $tanggal = explode('s/d', $this->tanggal_selesai);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'penyelesaian_kasus.tanggal_selesai', $awal, $akhir]);
        }


        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'masalah', $this->masalah])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        if (!empty($this->pegawai_ids)) {
            if ($modelPegawai = Pegawai::findOne($this->pegawai_ids)) {
                if ($perjanjianKerja = $modelPegawai->perjanjianKerja) {
                    $query->joinWith(['penyelesaianKasusDetails' => function (ActiveQuery $penyelesaianKasusDetail) use ($perjanjianKerja) {
                        $penyelesaianKasusDetail->andWhere(['penyelesaian_kasus_detail.perjanjian_kerja_id' => $perjanjianKerja->id]);
                    }]);
                }
            }
        }

        return $dataProvider;
    }
}

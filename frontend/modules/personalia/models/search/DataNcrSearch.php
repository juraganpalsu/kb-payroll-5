<?php

namespace frontend\modules\personalia\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\personalia\models\DataNcr;

/**
 * frontend\modules\personalia\models\search\DataNcrSearch represents the model behind the search form about `frontend\modules\personalia\models\DataNcr`.
 */
class DataNcrSearch extends DataNcr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_temuan', 'lembaga', 'keterangan_temuan', 'tanggal_selesai', 'keterangan_penyelesaian', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataNcr::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->tanggal_temuan)) {
            $tanggal = explode('s/d', $this->tanggal_temuan);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal_temuan', $awal, $akhir]);
        }

        if (!empty($this->tanggal_selesai)) {
            $tanggal = explode('s/d', $this->tanggal_selesai);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal_selesai', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'lembaga', $this->lembaga])
            ->andFilterWhere(['like', 'keterangan_temuan', $this->keterangan_temuan])
            ->andFilterWhere(['like', 'keterangan_penyelesaian', $this->keterangan_penyelesaian])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\personalia\models\search;

use frontend\modules\personalia\models\KecelakaanKerja;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\personalia\models\search\KecelakaanKerjaSearch represents the model behind the search form about `frontend\modules\personalia\models\KecelakaanKerja`.
 */
class KecelakaanKerjaSearch extends KecelakaanKerja
{
    /**
     * @var int
     */
    public $id_pegawai;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','id_pegawai', 'tanggal', 'lokasi', 'kronologi', 'cedera', 'penyebab', 'preventif', 'klaim_bpjs', 'berita_acara', 'tahap_satu', 'tahap_dua', 'catatan', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['klaim_bpjs','id_pegawai', 'berita_acara', 'tahap_satu', 'tahap_dua', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KecelakaanKerja::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }


        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'kecelakaan_kerja.tanggal', $awal, $akhir]);
        }        

        $query->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'kronologi', $this->kronologi])
            ->andFilterWhere(['like', 'cedera', $this->cedera])
            ->andFilterWhere(['like', 'penyebab', $this->penyebab])
            ->andFilterWhere(['like', 'preventif', $this->preventif])
            ->andFilterWhere(['klaim_bpjs' => $this->klaim_bpjs])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['like', 'catatan', $this->catatan]);
            

        return $dataProvider;
    }
}

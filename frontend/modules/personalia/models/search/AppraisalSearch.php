<?php

namespace frontend\modules\personalia\models\search;

use common\components\Status;
use frontend\models\User;
use frontend\modules\personalia\models\Appraisal;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\personalia\models\search\AppraisalSearch represents the model behind the search form about `frontend\modules\personalia\models\Appraisal`.
 */
class AppraisalSearch extends Appraisal
{
    /**
     * @var int
     */
    public $id_pegawai;

    public $busines_unit;

    public $golongan;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'id_pegawai', 'busines_unit', 'kategori', 'jenis_evaluasi', 'tanggal_awal', 'tanggal_akhir', 'rekomendasi', 'komentar_tambahan', 'kesimpulan', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'atasan_pegawai_id', 'atasan_dua_pegawai_id', 'atasan_pegawai_struktur_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['id_pegawai', 'alpa', 'izin', 'terlambat', 'cuti', 'total_nilai', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'golongan'], 'integer'],
            [['rata_rata'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param bool $isForMe
     * @return ActiveDataProvider
     */
    public function search(array $params, bool $isForMe = false): ActiveDataProvider
    {
        $query = Appraisal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $perjanjianKerja) {
                $perjanjianKerja->andWhere(['kontrak' => $this->busines_unit]);
            }]);
        }

        if (!empty($this->tanggal_awal)) {
            $tanggal = explode('s/d', $this->tanggal_awal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'appraisal.tanggal_awal', $awal, $akhir]);
        }

        if (!empty($this->tanggal_akhir)) {
            $tanggal = explode('s/d', $this->tanggal_akhir);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'appraisal.tanggal_akhir', $awal, $akhir]);
        }

        $query->andFilterWhere([
            'alpa' => $this->alpa,
            'izin' => $this->izin,
            'terlambat' => $this->terlambat,
            'cuti' => $this->cuti,
            'total_nilai' => $this->total_nilai,
            'rata_rata' => $this->rata_rata,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'jenis_evaluasi', $this->jenis_evaluasi])
            ->andFilterWhere(['like', 'rekomendasi', $this->rekomendasi])
            ->andFilterWhere(['like', 'komentar_tambahan', $this->komentar_tambahan])
            ->andFilterWhere(['like', 'kesimpulan', $this->kesimpulan])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);

        if (!empty($this->atasan_pegawai_id)) {
            $query->joinWith(['atasanPegawai' => function (ActiveQuery $query) {
                $query->alias('atasanPegawai');
                $query->andWhere(['LIKE', 'atasanPegawai.nama_lengkap', $this->atasan_pegawai_id]);
                $query->andWhere(['atasanPegawai.deleted_by' => 0]);
            }]);
        }
//        if (!empty($this->atasan_dua_pegawai_id)) {
//            $query->joinWith(['atasanDuaPegawai' => function (ActiveQuery $query) {
//                $query->alias('atasanDuaPegawai');
//                $query->andWhere(['LIKE', 'atasanDuaPegawai.nama_lengkap', $this->atasan_dua_pegawai_id]);
//                $query->andWhere(['atasanDuaPegawai.deleted_by' => 0]);
//            }]);
//        }

        if (!empty($this->oleh_pegawai_id)) {
            $query->joinWith(['olehPegawai' => function (ActiveQuery $query) {
                $query->alias('olehPegawai');
                $query->andWhere(['LIKE', 'olehPegawai.nama_lengkap', $this->oleh_pegawai_id]);
                $query->andWhere(['olehPegawai.deleted_by' => 0]);
            }]);
        }

        if ($isForMe) {
            if ($pegawai = User::pegawai()) {
                $query->andWhere(['atasan_pegawai_id' => $pegawai->id]);
            } else {
                $query->andWhere(['atasan_pegawai_id' => 0]);
            }
        }

        $query->orderBy('created_at DESC');

        return $dataProvider;
    }

    /**
     * Mencari data yg belum dilakukan approval bedasarkan user login
     *
     * @param bool $asActiveDataProvider
     * @return ActiveDataProvider|ActiveQuery
     */
    public function searchAppraisal(bool $asActiveDataProvider = true)
    {
        $query = Appraisal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andWhere(['total_nilai' => Status::DEFLT]);

        $modelPegawai = User::pegawai();
        $query->andWhere(['total_nilai' => Status::DEFLT, 'atasan_pegawai_id' => $modelPegawai->id]);

        $query->orderBy('appraisal.created_at DESC');

        if ($asActiveDataProvider) {
            return $dataProvider;
        }
        return $query;
    }
}
<?php

namespace frontend\modules\personalia\models\search;

use frontend\modules\personalia\models\Training;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\personalia\models\search\TrainingSearch represents the model behind the search form about `frontend\modules\personalia\models\Training`.
 */
class TrainingSearch extends Training
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal_pelaksanaan', 'durasi', 'ref_nama_pelatihan_id', 'ref_trainer_id', 'ref_nama_institusi_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['jenis_pelatihan', 'nilai_tes', 'sertifikat', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Training::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'jenis_pelatihan' => $this->jenis_pelatihan,
            'durasi' => $this->durasi,
            'nilai_tes' => $this->nilai_tes,
            'sertifikat' => $this->sertifikat,
            'lock' => $this->lock,
        ]);

        if (!empty($this->tanggal_pelaksanaan)) {
            $tanggal = explode('s/d', $this->tanggal_pelaksanaan);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'tanggal_pelaksanaan', $awal, $akhir]);
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'ref_nama_pelatihan_id', $this->ref_nama_pelatihan_id])
            ->andFilterWhere(['like', 'ref_trainer_id', $this->ref_trainer_id])
            ->andFilterWhere(['like', 'ref_nama_institusi_id', $this->ref_nama_institusi_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\personalia\models\search;

use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\Resign;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * frontend\modules\personalia\models\search\ResignSearch represents the model behind the search form about `frontend\modules\personalia\models\Resign`.
 *
 * @property integer $busines_unit
 */
class ResignSearch extends Resign
{

    public $busines_unit;

    public $golongan;

    /**
     * @var int
     */
    public $id_pegawai;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pegawai', 'tanggal', 'alasan', 'pegawai_id', 'busines_unit', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'created_at', 'created_by_pk', 'updated_at', 'deleted_at'], 'safe'],
            [['tipe', 'id_pegawai', 'exit_letter', 'exit_clearence', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'golongan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Resign::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['pegawai' => function (ActiveQuery $q) {
        }]);

        if (!empty($this->id_pegawai)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $q) {
                $q->andWhere(['perjanjian_kerja.id_pegawai' => $this->id_pegawai]);
            }]);
        }

        if (!empty($this->golongan)) {
            $query->joinWith('pegawaiGolongan');
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan]);
        }

        $query->andFilterWhere([
            'tipe' => $this->tipe,
            'exit_letter' => $this->exit_letter,
            'exit_clearence' => $this->exit_clearence,
        ]);

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'resign.tanggal', $awal, $akhir]);
        }

        if (!empty($this->busines_unit)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $perjanjianKerja) {
                $perjanjianKerja->andWhere(['kontrak' => $this->busines_unit]);
            }]);
        }


        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'alasan', $this->alasan])
            ->andFilterWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchBca(array $params)
    {
        $query = Resign::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('perjanjianKerja');
        $query->andWhere(['IN', 'perjanjian_kerja.kontrak', [PerjanjianKerja::BCA_ADA, PerjanjianKerja::BCA_KBU]]);

        $query->andFilterWhere([
            'tipe' => $this->tipe,
            'exit_letter' => $this->exit_letter,
            'exit_clearence' => $this->exit_clearence,
        ]);

        if (!empty($this->pegawai_id)) {
                $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                        $query->andWhere(['like', 'pegawai.nama_lengkap', $this->pegawai_id]);
                }]);
        }

        if (!empty($this->tanggal)) {
            $tanggal = explode('s/d', $this->tanggal);
            $awal = date('Y-m-d', strtotime($tanggal[0]));
            $akhir = date('Y-m-d', strtotime($tanggal[1]));
            $query->andWhere(['BETWEEN', 'resign.tanggal', $awal, $akhir]);
        }


        $query->andFilterWhere(['like', 'resign.alasan', $this->alasan]);
        
            

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\personalia\models\search;

use frontend\modules\personalia\models\Mpp;
use frontend\modules\personalia\models\MppAct;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * frontend\modules\personalia\models\search\MppSearch represents the model behind the search form about `frontend\modules\personalia\models\Mpp`.
 */

/**
 * Class MppSearch
 * @package frontend\modules\personalia\models\search
 *
 * @property integer $tahun
 */
class MppSearch extends Mpp
{

    public $tahun;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'jabatan_id', 'departemen_id', 'keterangan', 'deleted_at'], 'safe'],
            [['golongan_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'tahun'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Mpp::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'golongan_id' => $this->golongan_id,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'jabatan_id', $this->jabatan_id])
            ->andFilterWhere(['like', 'departemen_id', $this->departemen_id])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_by_pk', $this->created_by_pk]);

        return $dataProvider;
    }

    public function searchAktual(array $params): ActiveDataProvider
    {
        $query = MppAct::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith('mppJumlah');
        $query->joinWith('mpp');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'mpp.departemen_id' => $this->departemen_id,
            'mpp_act.jabatan_id' => $this->jabatan_id,
            'mpp_act.golongan_id' => $this->golongan_id,
            'mpp_jumlah.tahun' => $this->tahun,
        ]);

        $query->orderBy('mpp_act.created_at DESC');

        return $dataProvider;
    }
}

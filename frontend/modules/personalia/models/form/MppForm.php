<?php


namespace frontend\modules\personalia\models\form;


use Yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * File Name: MppForm.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 06/01/21
 * Time: 09.33
 */

/**
 * Class MppForm
 * @package frontend\modules\personalia\models\form
 *
 * @property int $tahun
 */
class MppForm extends Model
{
    public $tahun;

    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            [['tahun'], 'required'],
            [['tahun'], 'integer'],
        ];
    }

    /**
     * @return array[]
     */
    public function attributeLabels(): array
    {
        return [
            'tahun' => Yii::t('frontend', 'Tahun')
        ];
    }
}
<?php

namespace frontend\modules\personalia\models\form;

use common\components\Helper;
use common\components\Status;
use common\models\Golongan;
use frontend\modules\pegawai\models\PegawaiAsuransiLain;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiPendidikan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\PerjanjianKerjaJenis;
use frontend\modules\pegawai\Module;
use frontend\modules\personalia\models\Resign;
use frontend\modules\struktur\models\Struktur;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\DataReader;
use yii\db\Query;


/**
 * Created by PhpStorm.
 * File Name: Pegawai.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 27/02/20
 * Time: 21.49
 */


/**
 * Class Pegawai
 *
 * @property integer $jenis
 * @property integer $bulan
 * @property integer $tahun
 * @property integer $bisnis_unit
 * @property array $_jenis
 *
 *
 *
 *
 *
 */
class Pegawai extends Model
{

    public $jenis;
    public $bulan;
    public $tahun;
    public $bisnis_unit;

    const karyawan_masuk = 1;
    const karyawan_resign = 2;
    const ulang_tahun = 3;
    const bpjs_tk = 4;
    const bpjs_kes = 5;
    const bulanan_hrd = 6;
    const habis_kontrak = 7;
    const jumlah_karyawan = 8;
    const beweekly_factory = 9;
    const manulife = 10;
    const masa_kerja = 11;
    const assesment = 12;
    const pegawai = 13;
    const loyalitas = 14;


    public $_jenis = [
        self::karyawan_masuk => 'Karyawan Masuk',
        self::karyawan_resign => 'Karyawan Resign',
        self::ulang_tahun => 'Ulang Tahun',
        self::bpjs_tk => 'BPJS TK',
        self::bpjs_kes => 'BPJS KESEHATAN',
        self::bulanan_hrd => 'Bulanan HRD',
        self::habis_kontrak => 'Habis Kontrak',
        self::jumlah_karyawan => 'Jumlah Karyawan',
        self::beweekly_factory => 'Biweekly Factory',
        self::manulife => 'Manulife',
        self::masa_kerja => 'Masa Kerja',
        self::assesment => 'Assesment',
        self::pegawai => 'Pegawai',
        self::loyalitas => 'Loyalitas'
    ];


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['jenis', 'bulan', 'tahun'], 'required'],
            [['jenis', 'bulan', 'tahun', 'bisnis_unit'], 'integer'],

        ];
    }

    /**
     * @return array
     */
    public function attrbuteLabels()
    {
        return [
            'jenis' => Yii::t('frontend', 'Jenis'),
            'bulan' => Yii::t('frontend', 'Bulan'),
            'tahun' => Yii::t('frontend', 'Tahun'),
            'bisnis_unit' => Yii::t('frontend', 'Bisnis Unit')
        ];
    }

    /**
     * @return PerjanjianKerja
     */
    public static function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @return array
     */
    public function formatingDataKaryawanMasuk()
    {
        $query = PerjanjianKerja::find()
            ->andWhere(['dasar_masa_kerja' => Status::YA, 'month(mulai_berlaku)' => $this->bulan, 'year(mulai_berlaku)' => $this->tahun]);
        if (!empty($this->bisnis_unit)) {
            $query->andWhere(['kontrak' => $this->bisnis_unit]);
        }
        $queryPerjanjianKerjas = $query->all();
        $karyawanMasuk = [];
        try {
            /** @var PerjanjianKerja $perjanjianKerja */
            foreach ($queryPerjanjianKerjas as $perjanjianKerja) {
                if (!$perjanjianKerja->pegawai) {
                    continue;
                }
                $struktur = $perjanjianKerja->pegawai->getStrukturTerakhir();
                $golongan = $perjanjianKerja->pegawai->getGolonganTerakhir();
                $area = $perjanjianKerja->pegawai->getAreaKerjaTerakhir();
                $akunBank = '';
                $nomorRekening = '';
                if ($pegawaiBank = $perjanjianKerja->pegawai->pegawaiBank) {
                    $akunBank = $pegawaiBank->namaBank;
                    $nomorRekening = $pegawaiBank->nomer_rekening;
                }
                /** @var Struktur $queryAtasanLangsung */
                $queryAtasanLangsung = $struktur ? $struktur->struktur->parents(1)->one() : '';
                $namaAtasanLangsung = '';
                if ($queryAtasanLangsung) {
                    $modelPegawaiStruktur = new PegawaiStruktur();
                    $modelPegawaiStruktur->struktur_id = $queryAtasanLangsung->id;
                    $queryPegawaiStruktur = $modelPegawaiStruktur->pegawaiBerdasarTanggal();
                    if ($queryPegawaiStruktur) {
                        $namaAtasanLangsung .= '( ';
                        /** @var PegawaiStruktur $pegawaiStruktur */
                        foreach ($queryPegawaiStruktur as $pegawaiStruktur) {
                            if ($pegawaiStruktur->pegawai) {
                                $namaAtasanLangsung .= $pegawaiStruktur->pegawai->nama . ', ';
                            }
                        }
                        $namaAtasanLangsung .= ' )';
                    }
                }


                $queryHistoryPerjanjianKerja = PerjanjianKerja::find()->andWhere(['pegawai_id' => $perjanjianKerja->pegawai_id])->orderBy('mulai_berlaku ASC')->all();
                $historyPerjanjianKerja = array_map(function (PerjanjianKerja $perjanjianKerja) {
                    $tanggal = date('d-m-Y', strtotime($perjanjianKerja->mulai_berlaku));
                    if ($perjanjianKerja->akhir_berlaku) {
                        $tanggal = date('d-m-Y', strtotime($perjanjianKerja->mulai_berlaku)) . ' s/d ' . date('d-m-Y', strtotime($perjanjianKerja->akhir_berlaku));
                    }
                    return $perjanjianKerja->jenis->nama . '(' . $tanggal . ')';
                }, $queryHistoryPerjanjianKerja);
                $karyawanMasuk[] = [
                    'id' => $perjanjianKerja->id,
                    'departemen' => $struktur ? $struktur->struktur->departemen->nama : '',
                    'jabatan' => $struktur ? $struktur->struktur->jabatan->nama : '',
                    'fungsiJabatan' => $struktur ? $struktur->struktur->fungsiJabatan->nama : '',
                    'idPegawai' => str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                    'nama' => $perjanjianKerja->pegawai->nama,
                    'tempat_lahir' => $perjanjianKerja->pegawai->tempat_lahir,
                    'tanggal_lahir' => date('d-m-Y', strtotime($perjanjianKerja->pegawai->tanggal_lahir)),
                    'umur' => $perjanjianKerja->pegawai->getUsia(),
                    'jenis_kelamin' => $perjanjianKerja->pegawai->getJenisKelamin(),
                    'golongan' => $golongan ? $golongan->golongan->nama : '',
                    'atasanLangsung' => $struktur ? $queryAtasanLangsung ? $queryAtasanLangsung->name . $namaAtasanLangsung : '' : '',
                    'bisnisUnit' => $perjanjianKerja->namaKontrak,
                    'jenisKontrak' => $perjanjianKerja->jenis->nama,
                    'awalKontrak' => $perjanjianKerja->mulai_berlaku ? date('d-m-Y', strtotime($perjanjianKerja->mulai_berlaku)) : '',
                    'akhirKontrak' => $perjanjianKerja->akhir_berlaku ? date('d-m-Y', strtotime($perjanjianKerja->akhir_berlaku)) : '',
                    'historyKontrak' => $historyPerjanjianKerja ? implode(',', $historyPerjanjianKerja) : '',
                    'area' => $area ? $area->refAreaKerja->name : '',
                    'akun_bank' => $akunBank,
                    'no_rekening' => $nomorRekening,
                    'npwp' => $perjanjianKerja->pegawai->pegawaiNpwp ? $perjanjianKerja->pegawai->pegawaiNpwp->nomor : '',
                ];
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $karyawanMasuk;
    }

    /**
     * @return bool
     */
    public function karyawanMasukExcel()
    {

        $queryPerjanjianKerja = $this->formatingDataKaryawanMasuk();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(12);
            $activeSheet->getColumnDimension('B')->setWidth(10);
            $activeSheet->getColumnDimension('C')->setWidth(15);
            $activeSheet->getColumnDimension('D')->setWidth(10);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(5);
            $activeSheet->getColumnDimension('I')->setWidth(10);
            $activeSheet->getColumnDimension('J')->setWidth(10);
            $activeSheet->getColumnDimension('K')->setWidth(30);
            $activeSheet->getColumnDimension('L')->setWidth(8);
            $activeSheet->getColumnDimension('M')->setWidth(12);
            $activeSheet->getColumnDimension('N')->setWidth(10);
            $activeSheet->getColumnDimension('O')->setWidth(10);
            $activeSheet->getColumnDimension('P')->setWidth(30);
            $activeSheet->getColumnDimension('Q')->setWidth(15);
            $activeSheet->getColumnDimension('R')->setWidth(10);
            $activeSheet->getColumnDimension('S')->setWidth(12);
            $activeSheet->getColumnDimension('T')->setWidth(12);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'Departemen'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Jabatan'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Id Pegawai'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Nama'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Tempat Lahir'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Tanggal Lahir'))->getStyle('G1')->applyFromArray($center, $headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Umur'))->getStyle('H1')->applyFromArray($center, $headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Jenis Kelamin'))->getStyle('I1')->applyFromArray($center, $headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Golongan'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', Yii::t('frontend', 'Atasan Langsung'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('N1', Yii::t('frontend', 'Awal Kontrak'))->getStyle('N1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('O1', Yii::t('frontend', 'Akhir Kontrak'))->getStyle('O1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('P1', Yii::t('frontend', 'History Kontrak'))->getStyle('P1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('Q1', Yii::t('frontend', 'Area'))->getStyle('Q1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('R1', Yii::t('frontend', 'Akun Bank'))->getStyle('R1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('S1', Yii::t('frontend', 'No Rek Bank'))->getStyle('S1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('T1', Yii::t('frontend', 'NPWP'))->getStyle('T1')->applyFromArray($headerStyle);

            $row = 2;

            foreach ($queryPerjanjianKerja as $dataPegawai) {
                $activeSheet->setCellValue('A' . $row, $dataPegawai['departemen'])->getStyle('A' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['jabatan'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['fungsiJabatan'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['idPegawai'])->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['nama'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['tempat_lahir'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['tanggal_lahir'])->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['umur'])->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['jenis_kelamin'])->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('J' . $row, $dataPegawai['golongan'])->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('K' . $row, $dataPegawai['atasanLangsung'])->getStyle('K' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('L' . $row, $dataPegawai['bisnisUnit'])->getStyle('L' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('M' . $row, $dataPegawai['jenisKontrak'])->getStyle('M' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('N' . $row, $dataPegawai['awalKontrak'])->getStyle('N' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('O' . $row, $dataPegawai['akhirKontrak'])->getStyle('O' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('P' . $row, $dataPegawai['historyKontrak'])->getStyle('P' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('Q' . $row, $dataPegawai['area'])->getStyle('Q' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('R' . $row, $dataPegawai['akun_bank'])->getStyle('R' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('S' . $row, $dataPegawai['no_rekening'])->getStyle('S' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('T' . $row, $dataPegawai['npwp'])->getStyle('T' . $row)->applyFromArray(array_merge($center, $borderInside));


                $row++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'T' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'T' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanKaryawanMasuk-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }


    /**
     * @return array
     */
    public function formatingDataKaryawanResign()
    {
        $query = Resign::find()
            ->andWhere(['month(resign.tanggal)' => $this->bulan, 'year(resign.tanggal)' => $this->tahun]);
        if (!empty($this->bisnis_unit)) {
            $query->joinWith(['perjanjianKerja' => function (ActiveQuery $query) {
                $query->andWhere(['kontrak' => $this->bisnis_unit]);
            }]);
        }

        $queryResign = $query->all();
        $karyawanResign = [];
        /** @var Resign $resign */
        foreach ($queryResign as $resign) {
            if ($dasarMasaKerja = $resign->perjanjianKerja->getPerjanjianDasar()) {
                $tanggalmk = $dasarMasaKerja->mulai_berlaku ? Helper::idDate($dasarMasaKerja->mulai_berlaku) : '';
            } else {
                continue;
            }

            $karyawanResign[] = [
                'id' => $resign->id,
                'departemen' => $resign->pegawaiStruktur->struktur->departemen->nama,
                'jabatan' => $resign->pegawaiStruktur->struktur->jabatan->nama,
                'fungsiJabatan' => $resign->pegawaiStruktur->struktur->fungsiJabatan->nama,
                'struktur' => $resign->pegawaiStruktur->struktur->name,
                'idPegawai' => str_pad($resign->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                'nama' => $resign->pegawai->nama,
                'tanggal' => Helper::idDate($resign->tanggal),
                'tanggalLahir' => Helper::idDate($resign->pegawai->tanggal_lahir),
                'tanggalMk' => $tanggalmk,
                'golongan' => $resign->pegawaiGolongan->golongan->nama,
                'bisnisUnit' => $resign->perjanjianKerja->namaKontrak,
                'jenisKontrak' => $resign->perjanjianKerja->jenis->nama,
                'tipe' => $resign->tipe_,
                'alasan' => $resign->alasan,
                'area' => $resign->areaKerja->refAreaKerja->name

            ];
        }

        return $karyawanResign;
    }

    /**
     * @return bool
     */
    public function karyawanResignExcel()
    {

        $dataKaryawanResign = $this->formatingDataKaryawanResign();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(15);
            $activeSheet->getColumnDimension('B')->setWidth(10);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(8);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(10);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(10);
            $activeSheet->getColumnDimension('J')->setWidth(15);
            $activeSheet->getColumnDimension('K')->setWidth(15);
            $activeSheet->getColumnDimension('L')->setWidth(15);
            $activeSheet->getColumnDimension('M')->setWidth(15);
            $activeSheet->getColumnDimension('N')->setWidth(20);
            $activeSheet->getColumnDimension('O')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'Departemen'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Jabatan'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Struktur'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Id Pegawai'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Nama'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Tanggal Lahir'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Golongan'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', Yii::t('frontend', 'Tanggal MK'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', Yii::t('frontend', 'Tanggal Resign'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', Yii::t('frontend', 'Tipe'))->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('N1', Yii::t('frontend', 'Alasan'))->getStyle('N1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('O1', Yii::t('frontend', 'Area'))->getStyle('O1')->applyFromArray($headerStyle);

            $row = 2;

            foreach ($dataKaryawanResign as $dataPegawai) {
                $activeSheet->setCellValue('A' . $row, $dataPegawai['departemen'])->getStyle('A' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['jabatan'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['fungsiJabatan'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['struktur'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['idPegawai'])->getStyle('E' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['nama'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['tanggalLahir'])->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['bisnisUnit'])->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['golongan'])->getStyle('I' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('J' . $row, $dataPegawai['jenisKontrak'])->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('K' . $row, $dataPegawai['tanggalMk'])->getStyle('K' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('L' . $row, $dataPegawai['tanggal'])->getStyle('L' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('M' . $row, $dataPegawai['tipe'])->getStyle('M' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('N' . $row, $dataPegawai['alasan'])->getStyle('N' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('O' . $row, $dataPegawai['area'])->getStyle('O' . $row)->applyFromArray(array_merge($borderInside));


                $row++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'O' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'O' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanKaryawanResign-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }


    /**
     * @return array
     */
    public function formatingDataUlangTahun()
    {
        $query = \frontend\modules\pegawai\models\Pegawai::find();
        $query->joinWith('perjanjianKerja')
            ->andWhere(['month(`pegawai`.`tanggal_lahir`)' => $this->bulan]);
        if (!empty($this->bisnis_unit)) {
            $query->andWhere(['kontrak' => $this->bisnis_unit]);
        }
        $query->orderBy('pegawai.nama_lengkap ASC');
        $perjanjianKerjaQuery = $query->all();
        try {
            $ulangTahun = [];
            /** @var \frontend\modules\pegawai\models\Pegawai $pegawai */
            foreach ($perjanjianKerjaQuery as $pegawai) {
//                $pegawai = $perjanjianKerja->pegawai;
                $perjanjianKerja = $pegawai->perjanjianKerja;
                $struktur = $pegawai->pegawaiStruktur;
                $golongan = $pegawai->pegawaiGolongan;
                if ($perjanjianKerja) {
                    $ulangTahun[$pegawai->id] = [
                        'id' => $pegawai->id,
                        'departemen' => $struktur ? $struktur->struktur ? $struktur->struktur->departemen ? $struktur->struktur->departemen->nama : '' : '' : '',
                        'jabatan' => $struktur ? $struktur->struktur ? $struktur->struktur->jabatan ? $struktur->struktur->jabatan->nama : '' : '' : '',
                        'fungsiJabatan' => $struktur ? $struktur->struktur ? $struktur->struktur->fungsiJabatan ? $struktur->struktur->fungsiJabatan->nama : '' : '' : '',
                        'idPegawai' => str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                        'nama' => $pegawai->nama,
                        'golongan' => $golongan ? $golongan->golongan ? $golongan->golongan->nama : '' : '',
                        'bisnisUnit' => $perjanjianKerja->namaKontrak,
                        'jenisKontrak' => $perjanjianKerja->jenis->nama,
                        'tanggal_lahir' => date('d-m-Y', strtotime($pegawai->tanggal_lahir)),
                        'usia' => $pegawai->getUsia(),
                    ];
                }
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return $ulangTahun;
    }


    /**
     * @return bool
     */
    public function ulangTahunExcel()
    {

        $formatingDataUlangTahun = $this->formatingDataUlangTahun();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(10);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(15);
            $activeSheet->getColumnDimension('J')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'Departemen'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Jabatan'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Id Pegawai'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Nama'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Golongan'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Tanggal Lahir'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Usia'))->getStyle('J1')->applyFromArray($headerStyle);

            $row = 2;

            foreach ($formatingDataUlangTahun as $dataPegawai) {
                $activeSheet->setCellValue('A' . $row, $dataPegawai['departemen'])->getStyle('A' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['jabatan'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['fungsiJabatan'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['idPegawai'])->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['nama'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['bisnisUnit'])->getStyle('F' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['golongan'])->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['jenisKontrak'])->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['tanggal_lahir'])->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('J' . $row, $dataPegawai['usia'])->getStyle('J' . $row)->applyFromArray(array_merge($borderInside));


                $row++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'J' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'J' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanKaryawanUlangTahun-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }


    /**
     * @param int $jenis
     * @return array
     */
    public function formatingDataBpjs(int $jenis = PegawaiBpjs::BPJS_KETENAGAKERJAAN)
    {
        $query = PegawaiBpjs::find()
            ->andWhere(['month(tanggal_kepesertaan)' => $this->bulan, 'year(tanggal_kepesertaan)' => $this->tahun, 'jenis' => $jenis]);
        if (!empty($this->bisnis_unit)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                $query->joinWith(['perjanjianKerja' => function (ActiveQuery $query) {
                    $query->andWhere(['kontrak' => $this->bisnis_unit]);
                }]);
            }]);
        }
        $queryBpjs = $query->all();
        $dataBpjs = [];
        try {
            /** @var PegawaiBpjs $pegawaiBpjs */
            foreach ($queryBpjs as $pegawaiBpjs) {
                if ($pegawaiBpjs->pegawai) {
                    $perjanjianKerja = $pegawaiBpjs->pegawai->perjanjianKerja;
                    $struktur = $pegawaiBpjs->pegawai->pegawaiStruktur;
                    $golongan = $pegawaiBpjs->pegawai->pegawaiGolongan;
                    if ($perjanjianKerja) {
                        $dataBpjs[] = [
                            'id' => $pegawaiBpjs->id,
                            'departemen' => $struktur ? $struktur->struktur->departemen->nama : '',
                            'jabatan' => $struktur ? $struktur->struktur->jabatan->nama : '',
                            'fungsiJabatan' => $struktur ? $struktur->struktur->fungsiJabatan->nama : '',
                            'idPegawai' => str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                            'nama' => $pegawaiBpjs->pegawai->nama,
                            'golongan' => $golongan ? $golongan->golongan->nama : '',
                            'bisnisUnit' => $perjanjianKerja->namaKontrak,
                            'jenisKontrak' => $perjanjianKerja->jenis->nama,
                            'tanggal_kepesertaan' => Helper::idDate($pegawaiBpjs->tanggal_kepesertaan),
                            'nomor' => $pegawaiBpjs->nomor,
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return $dataBpjs;
    }


    /**
     * @param array $datas
     * @param int $jenis
     * @return bool
     */
    public function bpjsExcel(array $datas, int $jenis = 0)
    {
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(10);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(15);
            $activeSheet->getColumnDimension('J')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'Departemen'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Jabatan'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Id Pegawai'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Nama'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Golongan'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Tanggal Kepesertaan'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Nomor'))->getStyle('J1')->applyFromArray($headerStyle);

            $row = 2;

            foreach ($datas as $dataPegawai) {
                $activeSheet->setCellValue('A' . $row, $dataPegawai['departemen'])->getStyle('A' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['jabatan'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['fungsiJabatan'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['idPegawai'])->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['nama'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['bisnisUnit'])->getStyle('F' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['golongan'])->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['jenisKontrak'])->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['tanggal_kepesertaan'])->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('J' . $row, $dataPegawai['nomor'])->getStyle('J' . $row)->applyFromArray(array_merge($borderInside));


                $row++;
            }

            $jenisString = 'Manulife';
            if ($jenis > 0) {
                $jenisStringObj = new PegawaiBpjs();
                $jenisString = 'BPJS-';
                $jenisString .= $jenisStringObj->_jenis[$jenis];
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'J' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'J' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanKaryawan-' . $jenisString . '-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }


    /**
     * @return array
     */
    public function formatingDataHabisKontrak()
    {
        $query = PerjanjianKerja::find()->andWhere(['month(akhir_berlaku)' => $this->bulan, 'year(akhir_berlaku)' => $this->tahun])->orderBy('akhir_berlaku ASC');
        if (!empty($this->bisnis_unit)) {
            $query->andWhere(['kontrak' => $this->bisnis_unit]);
        }
        $queryPerjanjianKerja = $query->all();
        $dataPerjanjianKerja = [];
        try {
            /** @var PerjanjianKerja $perjanjianKerja */
            foreach ($queryPerjanjianKerja as $perjanjianKerja) {
                if ($perjanjianKerja->pegawai) {
                    $struktur = $perjanjianKerja->pegawai->pegawaiStruktur;
                    $golongan = $perjanjianKerja->pegawai->pegawaiGolongan;
                    $area = $perjanjianKerja->pegawai->areaKerja;
                    /** @var Struktur $queryAtasanLangsung */
                    $queryAtasanLangsung = $struktur ? $struktur->struktur->parents(1)->one() : '';
                    $namaAtasanLangsung = [];
                    if ($queryAtasanLangsung) {
                        $modelPegawaiStruktur = new PegawaiStruktur();
                        $modelPegawaiStruktur->struktur_id = $queryAtasanLangsung->id;
                        $queryPegawaiStruktur = $modelPegawaiStruktur->pegawaiBerdasarTanggal();
                        if ($queryPegawaiStruktur) {
                            /** @var PegawaiStruktur $pegawaiStruktur */
                            foreach ($queryPegawaiStruktur as $pegawaiStruktur) {
                                $namaAtasanLangsung[] = $pegawaiStruktur->pegawai ? $pegawaiStruktur->pegawai->nama : '-';
                            }
                        }
                    }
                    $queryHistoryPerjanjianKerja = PerjanjianKerja::find()->andWhere(['pegawai_id' => $perjanjianKerja->pegawai_id])->orderBy('mulai_berlaku ASC')->all();
                    $historyPerjanjianKerja = array_map(function (PerjanjianKerja $perjanjianKerja) {
                        return $perjanjianKerja->jenis->nama . '(' . date('d-m-Y', strtotime($perjanjianKerja->mulai_berlaku)) . ' s/d ' . date('d-m-Y', strtotime($perjanjianKerja->akhir_berlaku)) . ')';
                    }, $queryHistoryPerjanjianKerja);
                    $dataPerjanjianKerja[] = [
                        'id' => $perjanjianKerja->id,
                        'departemen' => $struktur ? $struktur->struktur->departemen->nama : '',
                        'jabatan' => $struktur ? $struktur->struktur->jabatan->nama : '',
                        'fungsiJabatan' => $struktur ? $struktur->struktur->fungsiJabatan->nama : '',
                        'idPegawai' => str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                        'nama' => $perjanjianKerja->pegawai->nama,
                        'golongan' => $golongan ? $golongan->golongan->nama : '',
                        'atasanLangsung' => $struktur ? $queryAtasanLangsung ? $queryAtasanLangsung->name . '(' . implode(',', $namaAtasanLangsung) . ')' : '' : '',
                        'bisnisUnit' => $perjanjianKerja->namaKontrak,
                        'jenisKontrak' => $perjanjianKerja->jenis->nama,
                        'awalKontrak' => Helper::idDate($perjanjianKerja->mulai_berlaku),
                        'akhirKontrak' => Helper::idDate($perjanjianKerja->akhir_berlaku),
                        'historyKontrak' => $historyPerjanjianKerja ? implode(',', $historyPerjanjianKerja) : '',
                        'area' => $area ? $area->refAreaKerja->name : '',
                    ];
                }
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return $dataPerjanjianKerja;
    }

    /**
     * @return bool
     */
    public function habisKontrakExcel()
    {

        $dataHabisKontrak = $this->formatingDataHabisKontrak();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(10);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(30);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(15);
            $activeSheet->getColumnDimension('J')->setWidth(15);
            $activeSheet->getColumnDimension('K')->setWidth(15);
            $activeSheet->getColumnDimension('L')->setWidth(30);
            $activeSheet->getColumnDimension('M')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'Departemen'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Jabatan'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Id Pegawai'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Nama'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Golongan'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Atasan Langsung'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Awal Kontrak'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', Yii::t('frontend', 'Akhir Kontrak'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', Yii::t('frontend', 'History Kontrak'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', Yii::t('frontend', 'Area'))->getStyle('M1')->applyFromArray($headerStyle);

            $row = 2;

            foreach ($dataHabisKontrak as $dataPegawai) {
                $activeSheet->setCellValue('A' . $row, $dataPegawai['departemen'])->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['jabatan'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['fungsiJabatan'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['idPegawai'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['nama'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['golongan'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['atasanLangsung'])->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['bisnisUnit'])->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['jenisKontrak'])->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('J' . $row, $dataPegawai['awalKontrak'])->getStyle('J' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('K' . $row, $dataPegawai['akhirKontrak'])->getStyle('K' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('L' . $row, $dataPegawai['historyKontrak'])->getStyle('L' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('M' . $row, $dataPegawai['area'])->getStyle('M' . $row)->applyFromArray(array_merge($borderInside));

                $row++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'M' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'M' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanKaryawanHabisKontrak-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }


    /**
     * @return array
     */
    public function formatingDataManulife()
    {
        $query = PegawaiAsuransiLain::find()->andWhere(['month(tanggal_kepesertaan)' => $this->bulan, 'year(tanggal_kepesertaan)' => $this->tahun])->orderBy('tanggal_kepesertaan ASC');
        if (!empty($this->bisnis_unit)) {
            $query->joinWith(['pegawai' => function (ActiveQuery $query) {
                $query->joinWith(['perjanjianKerja' => function (ActiveQuery $query) {
                    $query->andWhere(['kontrak' => $this->bisnis_unit]);
                }]);
            }]);
        }
        $queryManulife = $query->all();
        $dataManulife = [];
        try {
            /** @var PegawaiAsuransiLain $asuransiLain */
            foreach ($queryManulife as $asuransiLain) {
                if ($asuransiLain->pegawai) {
                    $perjanjianKerja = $asuransiLain->pegawai->perjanjianKerja;
                    $struktur = $asuransiLain->pegawai->pegawaiStruktur;
                    $golongan = $asuransiLain->pegawai->pegawaiGolongan;
                    if ($perjanjianKerja) {
                        $dataManulife[] = [
                            'id' => $asuransiLain->id,
                            'departemen' => $struktur ? $struktur->struktur->departemen->nama : '',
                            'jabatan' => $struktur ? $struktur->struktur->jabatan->nama : '',
                            'fungsiJabatan' => $struktur ? $struktur->struktur->fungsiJabatan->nama : '',
                            'idPegawai' => str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                            'nama' => $asuransiLain->pegawai->nama,
                            'golongan' => $golongan ? $golongan->golongan->nama : '',
                            'bisnisUnit' => $perjanjianKerja->namaKontrak,
                            'jenisKontrak' => $perjanjianKerja->jenis->nama,
                            'tanggal_kepesertaan' => Helper::idDate($asuransiLain->tanggal_kepesertaan),
                            'nomor' => $asuransiLain->nomor,
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $dataManulife;
    }

    /**
     * @return array
     */
    public function formatingDataBiweekly()
    {
        $model = new PerjanjianKerja();
        $tanggalString = $this->tahun . '-' . $this->bulan . '-01';
        $tanggal = date('Y-m-d', strtotime($tanggalString));
        $pegawaiQuery = $model->perjanjianKerjaAktifBerdasarBulan($tanggal);
        try {
            $karyawanResign = [];

            /** @var PerjanjianKerja $perjanjianKerja */
            foreach ($pegawaiQuery as $perjanjianKerja) {

                if ($perjanjianKerja) {
                    if (!empty($this->bisnis_unit)) {
                        if ($perjanjianKerja->kontrak != $this->bisnis_unit) {
                            continue;
                        }
                    }
                    if ($pegawai = $perjanjianKerja->pegawai) {
                        $struktur = $pegawai->pegawaiStruktur;
                        $departemen = '';
                        $section = '';
                        $jabatan = '';
                        $fungsiJabatan = '';
                        if ($struktur) {
                            if ($objStruktur = $struktur->struktur) {
                                if ($objDepartemen = $objStruktur->departemen) {
                                    $departemen = $objDepartemen->nama;
                                }
                                if ($objSection = $objStruktur->section) {
                                    $section = $objSection->nama;
                                }
                                if ($objJabatan = $objStruktur->jabatan) {
                                    $jabatan = $objJabatan->nama;
                                }
                                if ($objFungsiJabatan = $objStruktur->fungsiJabatan) {
                                    $fungsiJabatan = $objFungsiJabatan->nama;
                                }
                            }
                        }
                        $pegawaiGolongan = $pegawai->pegawaiGolongan;
                        $golongan = '';
                        if ($pegawaiGolongan) {
                            if ($objGolongan = $pegawaiGolongan->golongan) {
                                $golongan = $objGolongan->nama;
                            }
                        }
                        $pegawaiSistemKerja = $pegawai->pegawaiSistemKerja;
                        $costCenter = $pegawai->costCenter;

                        /** @var Struktur $queryAtasanLangsung */
                        $queryAtasanLangsung = $struktur ? $struktur->struktur->parents(1)->one() : '';
                        $namaAtasanLangsung = '';
                        if ($queryAtasanLangsung) {
                            $modelPegawaiStruktur = new PegawaiStruktur();
                            $modelPegawaiStruktur->struktur_id = $queryAtasanLangsung->id;
                            $queryPegawaiStruktur = $modelPegawaiStruktur->pegawaiBerdasarTanggal();
                            if ($queryPegawaiStruktur) {
                                $namaAtasanLangsung .= '( ';
                                /** @var PegawaiStruktur $pegawaiStruktur */
                                foreach ($queryPegawaiStruktur as $pegawaiStruktur) {
                                    if ($pegawaiStruktur->pegawai) {
                                        $namaAtasanLangsung .= $pegawaiStruktur->pegawai->nama . ', ';
                                    }
                                }
                                $namaAtasanLangsung .= ' )';
                            }
                        }
                        $namaPendidikanTerakhir = '';
                        if ($pendidikanTerakhir = PegawaiPendidikan::pendidikanTerakhir($pegawai->id)) {
                            $namaPendidikanTerakhir = $pendidikanTerakhir->jenjang_;
                        }
                        $area = $pegawai->areaKerja;
                        $gedungString = '';
                        $wilayahString = '';
                        if ($area) {
                            $gedung = $area->refAreaKerja->parents(1)->one();
                            if ($gedung) {
                                $gedungString = $gedung->name;
                                $wilayah = $area->refAreaKerja->parents(2)->one();
                                if ($wilayah) {
                                    $wilayahString = $wilayah->name;
                                }
                            }

                            $factory = $area->refAreaKerja->parents(3)->one();
                            if ($factory) {
                                if ($factory->name != 'FACTORY') {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                        $karyawanResign[] = [
                            'id' => $perjanjianKerja->id,
                            'departemen' => $departemen,
                            'section' => $section,
                            'jabatan' => $jabatan,
                            'fungsiJabatan' => $fungsiJabatan,
                            'idPegawai' => str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                            'nama' => $pegawai->nama,
                            'golongan' => $golongan,
                            'bisnisUnit' => $perjanjianKerja->namaKontrak,
                            'jenisKontrak' => $perjanjianKerja->jenis->nama,
                            'awalKontrak' => Helper::idDate($perjanjianKerja->mulai_berlaku),
                            'akhirKontrak' => date('d-m-Y', strtotime($perjanjianKerja->akhir_berlaku)),
                            'area' => $area->refAreaKerja ? $area->refAreaKerja->name : '',
                            'usia' => $pegawai->getUsia(),
                            'grupKerja' => $pegawaiSistemKerja ? $pegawaiSistemKerja->sistemKerja->nama : '',
                            'gedung' => $gedungString,
                            'atasan' => $namaAtasanLangsung,
                            'costCenterAreaKerja' => $costCenter ? $costCenter->costCenterTemplate->area_kerja : '',
                            'costCenterKode' => $costCenter ? $costCenter->costCenterTemplate->kode : '',
                            'jenisKelamin' => $pegawai->jenisKelamin,
                            'wilayah' => $wilayahString,
                            'pendidikanTerakhir' => $namaPendidikanTerakhir,
                            // 'pendidikanTerakhir' => $pegawai->getPendidikanTerakhir() ? $pegawai->getPendidikanTerakhir()['jenjang'] : ''
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return $karyawanResign;
    }


    /**
     * @return bool
     */
    public function biweeklyExcel()
    {

        $dataHabisKontrak = $this->formatingDataBiweekly();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(4);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(15);
            $activeSheet->getColumnDimension('J')->setWidth(15);
            $activeSheet->getColumnDimension('K')->setWidth(15);
            $activeSheet->getColumnDimension('L')->setWidth(15);
            $activeSheet->getColumnDimension('M')->setWidth(15);
            $activeSheet->getColumnDimension('N')->setWidth(15);
            $activeSheet->getColumnDimension('O')->setWidth(15);
            $activeSheet->getColumnDimension('P')->setWidth(15);
            $activeSheet->getColumnDimension('Q')->setWidth(15);
            $activeSheet->getColumnDimension('R')->setWidth(15);
            $activeSheet->getColumnDimension('S')->setWidth(15);
            $activeSheet->getColumnDimension('T')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'No'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Departemen'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Sub Dept'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Jabatan'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Id Pegawai'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Nama'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Golongan'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', Yii::t('frontend', 'Area Kerja'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', Yii::t('frontend', 'Grup Kerja'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', Yii::t('frontend', 'Gedung'))->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('N1', Yii::t('frontend', 'Atasan'))->getStyle('N1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('O1', Yii::t('frontend', 'Area Kerja Cost Center'))->getStyle('O1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('P1', Yii::t('frontend', 'Cost Center'))->getStyle('P1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('Q1', Yii::t('frontend', 'Jenis Kelamin'))->getStyle('Q1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('R1', Yii::t('frontend', 'Wilayah'))->getStyle('R1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('S1', Yii::t('frontend', 'Tanggal Masuk'))->getStyle('S1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('T1', Yii::t('frontend', 'Pendidikan'))->getStyle('T1')->applyFromArray($headerStyle);

            $row = 2;
            $no = 1;


            foreach ($dataHabisKontrak as $dataPegawai) {

                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['departemen'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['section'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['jabatan'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['fungsiJabatan'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['idPegawai'])->getStyle('F' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['nama'])->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['bisnisUnit'])->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['golongan'])->getStyle('I' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('J' . $row, $dataPegawai['jenisKontrak'])->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('K' . $row, $dataPegawai['area'])->getStyle('K' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('L' . $row, $dataPegawai['grupKerja'])->getStyle('L' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('M' . $row, $dataPegawai['gedung'])->getStyle('M' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('N' . $row, $dataPegawai['atasan'])->getStyle('N' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('O' . $row, $dataPegawai['costCenterAreaKerja'])->getStyle('O' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('P' . $row, $dataPegawai['costCenterKode'])->getStyle('P' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('Q' . $row, $dataPegawai['jenisKelamin'])->getStyle('Q' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('R' . $row, $dataPegawai['wilayah'])->getStyle('R' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('S' . $row, $dataPegawai['awalKontrak'])->getStyle('S' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('T' . $row, $dataPegawai['pendidikanTerakhir'])->getStyle('T' . $row)->applyFromArray(array_merge($center, $borderInside));
                $row++;
                $no++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'T' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'T' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanBiweeklyFactory-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function formatingDataJumlahKaryawan()
    {
        $query = new Query();
        $query->select(['p.id as id', 'pk.id_pegawai', 'p.nama_lengkap', 'CONCAT(p.nama_lengkap, \' - \', pk.id_pegawai) AS text', 'pk.kontrak as bu', 'pk.dasar_masa_kerja as dasarMasaKerja',
            'CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) AS pkth', 'CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) AS psth',
            'p.jenis_kelamin as jenisKelamin', 's.departemen_id as departemen_id', 'd.nama as dnama'])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'pegawai_struktur ps', 'p.id = ps.pegawai_id AND ((CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(ps.akhir_berlaku),month(ps.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND ps.akhir_berlaku is null))')
            ->join('LEFT JOIN', 'struktur s', 's.id = ps.struktur_id')
            ->join('LEFT JOIN', 'departemen d', 'd.id = s.departemen_id')
            ->join('LEFT JOIN', 'perjanjian_kerja pk', 'p.id = pk.pegawai_id AND ((CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(pk.akhir_berlaku),month(pk.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND pk.akhir_berlaku is null))');
        $query->andWhere(['p.deleted_by' => 0, 'ps.deleted_by' => 0, 'pk.deleted_by' => 0]);
        $command = $query->createCommand();

        $ju = [];
        foreach ($command->queryAll() as $item) {
            $queryPerjanjianKerja = PerjanjianKerja::find()
                ->joinWith(['pegawai' => function (ActiveQuery $query) use ($item) {
                    $query->join('LEFT JOIN', 'pegawai_struktur ps', 'pegawai.id = ps.pegawai_id AND ((CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(ps.akhir_berlaku),month(ps.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND ps.akhir_berlaku is null))');
                    $query->join('LEFT JOIN', 'struktur', 'struktur.id = ps.struktur_id');
                    $query->andWhere(['struktur.departemen_id' => $item['departemen_id']]);
                }]);
            $queryKaryawanMasuk = $queryPerjanjianKerja->andWhere(['perjanjian_kerja.dasar_masa_kerja' => Status::YA, 'month(perjanjian_kerja.mulai_berlaku)' => $this->bulan, 'year(perjanjian_kerja.mulai_berlaku)' => $this->tahun])->count();

            $queryKaryawanResign = $queryPerjanjianKerja->andWhere(['month(perjanjian_kerja.tanggal_resign)' => $this->bulan, 'year(perjanjian_kerja.tanggal_resign)' => $this->tahun])->count();

            $ju['departemen'][$item['departemen_id']]['detail'] = ['namaDepartemen' => $item['dnama'], 'masuk' => $queryKaryawanMasuk, 'resign' => $queryKaryawanResign];
            $ju['departemen'][$item['departemen_id']]['bu'][$item['bu']]['jk'][$item['jenisKelamin']][] = $item['text'];
        }
        return $ju;
    }


    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function jumlahKaryawanExcel()
    {

        $dataHabisKontrak = $this->formatingDataJumlahKaryawan();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

//        $center = [
//            'alignment' => [
//                'horizontal' => Alignment::HORIZONTAL_CENTER,
//            ]
//        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri'
            ));
        $fontsum = [
            'font' => [
                'bold' => true,
                'size' => 12,
                'name' => 'Calibri',
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(20);
            $activeSheet->getColumnDimension('B')->setWidth(5);
            $activeSheet->getColumnDimension('C')->setWidth(5);
            $activeSheet->getColumnDimension('D')->setWidth(5);
            $activeSheet->getColumnDimension('E')->setWidth(5);
            $activeSheet->getColumnDimension('F')->setWidth(5);
            $activeSheet->getColumnDimension('G')->setWidth(5);
            $activeSheet->getColumnDimension('H')->setWidth(5);
            $activeSheet->getColumnDimension('I')->setWidth(5);
            $activeSheet->getColumnDimension('J')->setWidth(8);
            $activeSheet->getColumnDimension('K')->setWidth(8);

            $activeSheet->mergeCells('A1:A2')->setCellValue('A1', Yii::t('frontend', 'Departemen'))->getStyle('A1:A2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('B1:C1')->setCellValue('B1', Yii::t('frontend', 'KBU'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B2', Yii::t('frontend', 'L'))->getStyle('B2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C2', Yii::t('frontend', 'P'))->getStyle('C2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('D1:E1')->setCellValue('D1', Yii::t('frontend', 'ADA'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D2', Yii::t('frontend', 'L'))->getStyle('D2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E2', Yii::t('frontend', 'P'))->getStyle('E2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('F1:G1')->setCellValue('F1', Yii::t('frontend', 'BCA KBU'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F2', Yii::t('frontend', 'L'))->getStyle('F2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G2', Yii::t('frontend', 'P'))->getStyle('G2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('H1:I1')->setCellValue('H1', Yii::t('frontend', 'BCA ADA'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H2', Yii::t('frontend', 'L'))->getStyle('H2')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I2', Yii::t('frontend', 'P'))->getStyle('I2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('J1:J2')->setCellValue('J1', Yii::t('frontend', 'Masuk'))->getStyle('J1:J2')->applyFromArray($headerStyle);
            $activeSheet->mergeCells('K1:K2')->setCellValue('K1', Yii::t('frontend', 'Keluar'))->getStyle('K1:K2')->applyFromArray($headerStyle);

            $row = 3;

            $totalMasuk = 0;
            $totalKeluar = 0;

            foreach ($dataHabisKontrak['departemen'] as $dataPegawai) {
                $masuk = $dataPegawai['detail']['masuk'];
                $totalMasuk += $masuk;
                $keluar = $dataPegawai['detail']['resign'];
                $totalKeluar += $keluar;
                $activeSheet->setCellValue('A' . $row, $dataPegawai['detail']['namaDepartemen'])->getStyle('A' . $row)->applyFromArray(array_merge($borderInside));
                if (isset($dataPegawai['bu'][PerjanjianKerja::KBU])) {
                    $bu = $dataPegawai['bu'][PerjanjianKerja::KBU];
                    $jk = $bu['jk'];
                    if (isset($jk[\frontend\modules\pegawai\models\Pegawai::LAKI_LAKI])) {
                        $activeSheet->setCellValue('B' . $row, count($jk[\frontend\modules\pegawai\models\Pegawai::LAKI_LAKI]))->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                    }
                    if (isset($jk[\frontend\modules\pegawai\models\Pegawai::PEREMPUAN])) {
                        $activeSheet->setCellValue('C' . $row, count($jk[\frontend\modules\pegawai\models\Pegawai::PEREMPUAN]))->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                    }
                }
                if (isset($dataPegawai['bu'][PerjanjianKerja::ADA])) {
                    $bu = $dataPegawai['bu'][PerjanjianKerja::ADA];
                    $jk = $bu['jk'];
                    if (isset($jk[\frontend\modules\pegawai\models\Pegawai::LAKI_LAKI])) {
                        $activeSheet->setCellValue('D' . $row, count($jk[\frontend\modules\pegawai\models\Pegawai::LAKI_LAKI]))->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                    }
                    if (isset($jk[\frontend\modules\pegawai\models\Pegawai::PEREMPUAN])) {
                        $activeSheet->setCellValue('E' . $row, count($jk[\frontend\modules\pegawai\models\Pegawai::PEREMPUAN]))->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                    }
                }
                if (isset($dataPegawai['bu'][PerjanjianKerja::BCA_KBU])) {
                    $bu = $dataPegawai['bu'][PerjanjianKerja::BCA_KBU];
                    $jk = $bu['jk'];
                    if (isset($jk[\frontend\modules\pegawai\models\Pegawai::LAKI_LAKI])) {
                        $activeSheet->setCellValue('F' . $row, count($jk[\frontend\modules\pegawai\models\Pegawai::LAKI_LAKI]))->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                    }
                    if (isset($jk[\frontend\modules\pegawai\models\Pegawai::PEREMPUAN])) {
                        $activeSheet->setCellValue('G' . $row, count($jk[\frontend\modules\pegawai\models\Pegawai::PEREMPUAN]))->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                    }
                }
                if (isset($dataPegawai['bu'][PerjanjianKerja::BCA_ADA])) {
                    $bu = $dataPegawai['bu'][PerjanjianKerja::BCA_ADA];
                    $jk = $bu['jk'];
                    if (isset($jk[\frontend\modules\pegawai\models\Pegawai::LAKI_LAKI])) {
                        $activeSheet->setCellValue('H' . $row, count($jk[\frontend\modules\pegawai\models\Pegawai::LAKI_LAKI]))->getStyle('H' . $row)->applyFromArray(array_merge($borderInside));
                    }
                    if (isset($jk[\frontend\modules\pegawai\models\Pegawai::PEREMPUAN])) {
                        $activeSheet->setCellValue('I' . $row, count($jk[\frontend\modules\pegawai\models\Pegawai::PEREMPUAN]))->getStyle('I' . $row)->applyFromArray(array_merge($borderInside));
                    }
                }
                $activeSheet->setCellValue('J' . $row, $masuk > 0 ? $masuk : '')->getStyle('J' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('K' . $row, $keluar > 0 ? $keluar : '')->getStyle('K' . $row)->applyFromArray(array_merge($borderInside));
                $row++;
            }


            $rowd = $row - 1;
            $activeSheet->setCellValue('A' . $row, Yii::t('frontend', 'Total'))->getStyle('A' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('B' . $row, '=SUM(B3:B' . $rowd . ')')->getStyle('B' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('C' . $row, '=SUM(C3:C' . $rowd . ')')->getStyle('C' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('D' . $row, '=SUM(D3:D' . $rowd . ')')->getStyle('D' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('E' . $row, '=SUM(E3:E' . $rowd . ')')->getStyle('E' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('F' . $row, '=SUM(F3:F' . $rowd . ')')->getStyle('F' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('G' . $row, '=SUM(G3:G' . $rowd . ')')->getStyle('G' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('H' . $row, '=SUM(H3:H' . $rowd . ')')->getStyle('H' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('I' . $row, '=SUM(I3:I' . $rowd . ')')->getStyle('I' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('J' . $row, $totalMasuk > 0 ? $totalMasuk : '')->getStyle('J' . $row)->applyFromArray($fontsum);
            $activeSheet->setCellValue('K' . $row, $totalKeluar ? $totalKeluar : '')->getStyle('K' . $row)->applyFromArray($fontsum);
//            $row += 1;
            $activeSheet->getStyle('A1:' . 'K' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'K' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanJumlahKaryawan-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }

    /**
     * @return array
     */
    public function formatingMasaKerja(): array
    {
        $model = new PerjanjianKerja();
        $tanggal = $this->tahun . '-' . $this->bulan . '-01';
        $pegawaiQuery = $model->perjanjianKerjaAktifBerdasarBulan($tanggal);
        try {
//            $a = 0;
            $dataMasaKerjas = [];

            /** @var PerjanjianKerja $perjanjianKerja */
            foreach ($pegawaiQuery as $perjanjianKerja) {

                if ($perjanjianKerja) {
                    if (!empty($this->bisnis_unit)) {
                        if ($perjanjianKerja->kontrak != $this->bisnis_unit) {
                            continue;
                        }
                    }
                    if ($pegawai = $perjanjianKerja->pegawai) {
                        $pegawaiStruktur = $pegawai->pegawaiStruktur;
                        $departemen = '';
                        $jabatan = '';
                        $fungsiJabatan = '';
                        if ($pegawaiStruktur) {
                            if ($objStruktur = $pegawaiStruktur->struktur) {
                                if ($objDepartemen = $objStruktur->departemen) {
                                    $departemen = $objDepartemen->nama;
                                }
                                if ($objJabatan = $objStruktur->jabatan) {
                                    $jabatan = $objJabatan->nama;
                                }
                                if ($objFungsiJabatan = $objStruktur->fungsiJabatan) {
                                    $fungsiJabatan = $objFungsiJabatan->nama;
                                }
                            }
                        }
                        $pegawaiGolongan = $pegawai->pegawaiGolongan;
                        $golongan = '';
                        if ($pegawaiGolongan) {
                            if ($objGolongan = $pegawaiGolongan->golongan) {
                                $golongan = $objGolongan->nama;
                            }
                        }

                        if ($dasarMasaKerja = $perjanjianKerja->getPerjanjianDasar()) {
                            $masaKerja = $dasarMasaKerja->hitungMasaKerja('', $tanggal)['tahun'];
                            $tanggalmk = $dasarMasaKerja->mulai_berlaku ? date('d-m-Y', strtotime($dasarMasaKerja->mulai_berlaku)) : '';
                            $dasarTahun = Helper::idDate($dasarMasaKerja->mulai_berlaku);
                            $dasarBulan = Helper::idDate($dasarMasaKerja->mulai_berlaku);
                            if ($this->tahun == $dasarTahun && $this->bulan == $dasarBulan) {
                                continue;
                            }
                        } else {
                            continue;
                        }

                        /** @var Struktur $queryAtasanLangsung */
                        $queryAtasanLangsung = $pegawaiStruktur ? $pegawaiStruktur->struktur->parents(1)->one() : '';
                        $namaAtasanLangsung = '';
                        if ($queryAtasanLangsung) {
                            $modelPegawaiStruktur = new PegawaiStruktur();
                            $modelPegawaiStruktur->struktur_id = $queryAtasanLangsung->id;
                            $queryPegawaiStruktur = $modelPegawaiStruktur->pegawaiBerdasarTanggal();
                            if ($queryPegawaiStruktur) {
                                /** @var PegawaiStruktur $pegawaiStrukturM */
                                foreach ($queryPegawaiStruktur as $pegawaiStrukturM) {
                                    if ($pegawaiStrukturM->pegawai) {
                                        $namaAtasanLangsung .= $pegawaiStrukturM->pegawai->nama . ', ';
                                    }
                                }
                            }
                        }

                        $area = $pegawai->areaKerja;
//                        $a++;
//                        if ($a == 30) {
//                            break;
//                        }
                        $dataMasaKerjas[] = [
                            'id' => $perjanjianKerja->id,
                            'departemen' => $departemen,
                            'jabatan' => $jabatan,
                            'fungsiJabatan' => $fungsiJabatan,
                            'idPegawai' => str_pad($perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                            'nama' => $pegawai->nama,
                            'golongan' => $golongan,
                            'bisnisUnit' => $perjanjianKerja->namaKontrak,
                            'jenisKontrak' => $perjanjianKerja->jenis->nama,
                            'awalKontrak' => Helper::idDate($perjanjianKerja->mulai_berlaku),
                            'akhirKontrak' => !empty($perjanjianKerja->akhir_berlaku) ? Helper::idDate($perjanjianKerja->akhir_berlaku) : '',
                            'area' => $area ? $area->refAreaKerja->name : '',
                            'MasaKerja' => $masaKerja,
                            'Tanggalmk' => $tanggalmk,

                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return $dataMasaKerjas;
    }


    /**
     * @return bool
     */
    public function masaKerjaExcel()
    {

        $dataHabisKontrak = $this->formatingMasaKerja();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri',
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(4);
            $activeSheet->getColumnDimension('B')->setWidth(15);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(10);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(10);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(15);
            $activeSheet->getColumnDimension('J')->setWidth(12);
            $activeSheet->getColumnDimension('K')->setWidth(12);
            $activeSheet->getColumnDimension('L')->setWidth(15);
            $activeSheet->getColumnDimension('M')->setWidth(15);
            $activeSheet->getColumnDimension('N')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'No'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Departemen'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Id Pegawai'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Nama'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Golongan'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Awal Kontrak'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', Yii::t('frontend', 'Akhir Kontrak'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', Yii::t('frontend', 'Area Kerja'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', Yii::t('frontend', 'Tanggal Dasar MK'))->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('N1', Yii::t('frontend', 'Masa Kerja(Sesuai Dasar MK)'))->getStyle('N1')->applyFromArray($headerStyle);

            $row = 2;
            $no = 1;


            foreach ($dataHabisKontrak as $dataPegawai) {

                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['departemen'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['jabatan'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['fungsiJabatan'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['idPegawai'])->getStyle('E' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['nama'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['bisnisUnit'])->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['golongan'])->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['jenisKontrak'])->getStyle('I' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('J' . $row, $dataPegawai['awalKontrak'])->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('K' . $row, $dataPegawai['akhirKontrak'])->getStyle('K' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('L' . $row, $dataPegawai['area'])->getStyle('L' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('M' . $row, $dataPegawai['Tanggalmk'])->getStyle('M' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('N' . $row, $dataPegawai['MasaKerja'])->getStyle('N' . $row)->applyFromArray(array_merge($center, $borderInside));
                $row++;
                $no++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'N' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'N' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanMasaKerja-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }


    /**
     * @return array
     */
    public function formatingAssesment()
    {
        $assesments = [];
        try {
            $query = new Query();
            $query->select(['p.id as id', 'ps.aksi as aksips', 'pk.aksi as aksipk', 'pg.aksi as aksipg', 'pk.id_pegawai as id_pegawai', 'p.nama_lengkap as nama', 'CONCAT(p.nama_lengkap, \' - \', pk.id_pegawai) AS text', 'pk.kontrak as bu', 'pk.jenis_perjanjian as kontrak_id',
                'ps.mulai_berlaku as tglps', 'pk.mulai_berlaku as tglpk', 'pg.mulai_berlaku as tglpg', 'CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) AS pkth', 'CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) AS psth',
                'd.nama as departemen', 'pg.golongan_id as golongan', 'j.nama as jabatan', 'fj.nama as fungsiJabatan', 'd.nama as dnama', 'ps.aksi as aksiStruk', 'pk.aksi as aksiPer', 'pg.aksi as aksiGol', 'pk.kontrak as bisnisUnitId', 'pg.golongan_id as golongan_id'])
                ->from('pegawai p')
                ->join('LEFT JOIN', 'pegawai_struktur ps', 'p.id = ps.pegawai_id AND ((CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(ps.akhir_berlaku),month(ps.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND ps.akhir_berlaku is null))')
                ->join('LEFT JOIN', 'struktur s', 's.id = ps.struktur_id')
                ->join('LEFT JOIN', 'departemen d', 'd.id = s.departemen_id')
                ->join('LEFT JOIN', 'jabatan j', 'j.id = s.jabatan_id')
                ->join('LEFT JOIN', 'fungsi_jabatan fj', 'fj.id = s.fungsi_jabatan_id')
                // ->join('LEFT JOIN', 'perjanjian_kerja_jenis pkj', 'pkj.id = pk.jenis_perjanjian')
                ->join('LEFT JOIN', 'perjanjian_kerja pk', 'p.id = pk.pegawai_id AND ((CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(pk.akhir_berlaku),month(pk.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND pk.akhir_berlaku is null))')
                ->join('LEFT JOIN', 'pegawai_golongan pg', 'p.id = pg.pegawai_id AND ((CONCAT(year(pg.mulai_berlaku),month(pg.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(pg.mulai_berlaku),month(pg.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pg.mulai_berlaku),month(pg.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(pg.akhir_berlaku),month(pg.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pg.mulai_berlaku),month(pg.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND pg.akhir_berlaku is null))');
            $query->orWhere(['and', ['ps.aksi' => [1, 2, 3, 4], 'month(ps.mulai_berlaku)' => $this->bulan, 'year(ps.mulai_berlaku)' => $this->tahun]]);
            $query->orWhere(['and', ['pk.aksi' => [1, 2, 3], 'month(pk.mulai_berlaku)' => $this->bulan, 'year(pk.mulai_berlaku)' => $this->tahun]]);
            $query->orWhere(['and', ['pg.aksi' => [1, 2], 'month(pg.mulai_berlaku)' => $this->bulan, 'year(pg.mulai_berlaku)' => $this->tahun]]);
            $query->andWhere(['p.deleted_by' => 0, 'ps.deleted_by' => 0, 'pk.deleted_by' => 0, 'pg.deleted_by' => 0]);
            $command = $query->createCommand()->queryAll();

            $modelPerjanjianKerja = new PerjanjianKerja();
            foreach ($command as $pegawai) {
                $golongan = Golongan::findOne($pegawai['golongan_id']);
                $pegawaiGolongan = ['golongan' => $golongan->nama];
                $bisnisUnit = ['bisnisUnit' => $modelPerjanjianKerja->_kontrak[$pegawai['bisnisUnitId']]];
                $kontrak = PerjanjianKerjaJenis::findOne($pegawai['kontrak_id']);
                $jenisKontrak = ['jenisKontrak' => $kontrak->nama];
                $assesments[] = array_merge($pegawai, $bisnisUnit, $pegawaiGolongan, $jenisKontrak);
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $assesments;
    }


    /**
     * @return bool
     */
    public function AssesmentExcel()
    {

        $dataHabisKontrak = $this->formatingAssesment();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri',
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(4);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('C')->setWidth(10);
            $activeSheet->getColumnDimension('D')->setWidth(15);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(8);
            $activeSheet->getColumnDimension('I')->setWidth(15);
            $activeSheet->getColumnDimension('J')->setWidth(15);
            $activeSheet->getColumnDimension('K')->setWidth(15);
            $activeSheet->getColumnDimension('L')->setWidth(15);
            $activeSheet->getColumnDimension('M')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'No'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Departemen'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Jabatan'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Fungsi Jabatan'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Id Pegawai'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Nama'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Golongan'))->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', Yii::t('frontend', 'Aksi Perjanjian Kerja'))->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', Yii::t('frontend', 'Tgl Awal'))->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', Yii::t('frontend', 'Aksi Struktur'))->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', Yii::t('frontend', 'Tgl Awal'))->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('N1', Yii::t('frontend', 'Aksi Golongan'))->getStyle('N1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('O1', Yii::t('frontend', 'Tgl Awal'))->getStyle('O1')->applyFromArray($headerStyle);

            $row = 2;
            $no = 1;


            foreach ($dataHabisKontrak as $dataPegawai) {

                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['departemen'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['jabatan'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['fungsiJabatan'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['id_pegawai'])->getStyle('E' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['nama'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['bisnisUnit'])->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['golongan'])->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('I' . $row, $dataPegawai['jenisKontrak'])->getStyle('I' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('J' . $row, Module::getAksi($dataPegawai['aksipk']))->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('K' . $row, $dataPegawai['aksipk'] != 0 ? date('d-m-Y', strtotime($dataPegawai['tglpk'])) : '-')->getStyle('K' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('L' . $row, Module::getAksi($dataPegawai['aksips']))->getStyle('L' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('M' . $row, $dataPegawai['aksips'] != 0 ? date('d-m-Y', strtotime($dataPegawai['tglps'])) : '-')->getStyle('M' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('N' . $row, Module::getAksi($dataPegawai['aksipg']))->getStyle('N' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('O' . $row, $dataPegawai['aksipg'] != 0 ? date('d-m-Y', strtotime($dataPegawai['tglpg'])) : '-')->getStyle('O' . $row)->applyFromArray(array_merge($center, $borderInside));
                $row++;
                $no++;
            }


            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'O' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'O' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanAssesment-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }

    /**
     * @return array|DataReader
     * @throws \yii\db\Exception
     */
    public function formatingPegawai()
    {
        $query = new Query();
        $query->select(['p.id as id', 'pk.id as pk_id', 'ps.id as struktur_id', 'pk.id_pegawai as id_pegawai', 'p.nama_lengkap as nama', 'pk.kontrak as bu', 'pk.jenis_perjanjian as jenisKontrak',
            'ps.mulai_berlaku as tglps', 'pk.mulai_berlaku as tglpk', 'pg.mulai_berlaku as tglpg', 'CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) AS pkth', 'CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) AS psth',
            'd.nama as departemen', 'pg.id as golongan_id', 'j.nama as jabatan', 'fj.nama as fungsiJabatan', 'd.nama as dnama', 'ps.aksi as aksiStruk', 'pk.aksi as aksiPer', 'pg.aksi as aksiGol', 'pk.kontrak as bisnisUnitId'])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'pegawai_struktur ps', 'p.id = ps.pegawai_id AND ((CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(ps.akhir_berlaku),month(ps.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(ps.mulai_berlaku),month(ps.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND ps.akhir_berlaku is null))')
            ->join('LEFT JOIN', 'struktur s', 's.id = ps.struktur_id')
            ->join('LEFT JOIN', 'departemen d', 'd.id = s.departemen_id')
            ->join('LEFT JOIN', 'jabatan j', 'j.id = s.jabatan_id')
            ->join('LEFT JOIN', 'fungsi_jabatan fj', 'fj.id = s.fungsi_jabatan_id')
            // ->join('LEFT JOIN', 'perjanjian_kerja_jenis pkj', 'pkj.id = pk.jenis_perjanjian')
            ->join('LEFT JOIN', 'perjanjian_kerja pk', 'p.id = pk.pegawai_id AND ((CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(pk.akhir_berlaku),month(pk.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pk.mulai_berlaku),month(pk.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND pk.akhir_berlaku is null))')
            ->join('LEFT JOIN', 'pegawai_golongan pg', 'p.id = pg.pegawai_id AND ((CONCAT(year(pg.mulai_berlaku),month(pg.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND CONCAT(year(pg.mulai_berlaku),month(pg.mulai_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pg.mulai_berlaku),month(pg.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\'AND CONCAT(year(pg.akhir_berlaku),month(pg.akhir_berlaku)) >= \'' . $this->tahun . $this->bulan . '\') OR (CONCAT(year(pg.mulai_berlaku),month(pg.mulai_berlaku)) <=\'' . $this->tahun . $this->bulan . '\' AND pg.akhir_berlaku is null))');
        $query->andWhere(['p.deleted_by' => 0, 'ps.deleted_by' => 0, 'pk.deleted_by' => 0, 'pg.deleted_by' => 0]);
        return $query->createCommand()->queryAll();

    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function PegawaiIDExcel()
    {

        $dataHabisKontrak = $this->formatingPegawai();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri',
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(4);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('C')->setWidth(30);
            $activeSheet->getColumnDimension('D')->setWidth(30);
            $activeSheet->getColumnDimension('E')->setWidth(30);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(15);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'No'))->getStyle('S1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Pegawai ID'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Perjanjian Kerja ID'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Golongan ID'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Struktur ID'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'ID Pegawai'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Nama'))->getStyle('F1')->applyFromArray($headerStyle);

            $row = 2;
            $no = 1;


            foreach ($dataHabisKontrak as $dataPegawai) {

                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['id'])->getStyle('B' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['pk_id'])->getStyle('C' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['golongan_id'])->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['struktur_id'])->getStyle('E' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['id_pegawai'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['nama'])->getStyle('G' . $row)->applyFromArray(array_merge($borderInside));
                $row++;
                $no++;
            }


            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'L' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'L' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanPegawaiID-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }


    /**
     * @return array
     */
    public function formatingDataLoyalitas(): array
    {
        $tanggal = $this->tahun . '-' . $this->bulan . '-01';
        $lastDayOnThisMonth = date('Y-m-t', strtotime($tanggal));
        $query = PerjanjianKerja::find()->select(['TIMESTAMPDIFF(YEAR, mulai_berlaku, \'' . $lastDayOnThisMonth . '\') AS difference', 'id']);
        $query->andWhere(['dasar_masa_kerja' => Status::YA, 'MONTH(mulai_berlaku)' => $this->bulan]);
        if (!empty($this->bisnis_unit)) {
            $query->andWhere(['kontrak' => $this->bisnis_unit]);
        }
        $query->having(['IN', 'difference', [5, 10, 15]])->orderBy('difference ASC');
        $query = $query->all();
        $datas = [];
        foreach ($query as $perjanjianKerjaQuery) {
            $modelPerjanjianKerja = PerjanjianKerja::findOne($perjanjianKerjaQuery['id']);
            if ((date('m', strtotime($modelPerjanjianKerja->mulai_berlaku)) != date('m', strtotime($lastDayOnThisMonth))) || $modelPerjanjianKerja->pegawai->is_resign == Status::YA) {
                continue;
            }
            $pegawaiGolongan = '';
            if ($golongan = $modelPerjanjianKerja->pegawai->pegawaiGolongan) {
                $pegawaiGolongan = $golongan->golongan->nama;
            }
            $pegawaiStruktur = '';
            if ($struktur = $modelPerjanjianKerja->pegawai->pegawaiStruktur) {
                $pegawaiStruktur = $struktur->struktur->name;
            }
            $datas[$modelPerjanjianKerja->pegawai_id] = [
                'idPegawai' => str_pad($modelPerjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT),
                'nama' => $modelPerjanjianKerja->pegawai->nama,
                'mulai' => Helper::idDate($modelPerjanjianKerja->mulai_berlaku),
                'akhir' => $lastDayOnThisMonth,
                'lamaBekerja' => $perjanjianKerjaQuery['difference'],
                'bisnisUnit' => $modelPerjanjianKerja->namaKontrak,
                'golongan' => $pegawaiGolongan,
                'struktur' => $pegawaiStruktur
            ];
        }
        return $datas;
    }

    /**
     * @return bool
     */
    public function loyalitasExcel()
    {
        $dataLoyalitasPegawai = $this->formatingDataLoyalitas();

        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $outLineBottomBorder = [
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => Border::BORDER_THIN,
                ),
            ),
        ];

        $borderInside = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                ],
            ],
        ];

        $font = array(
            'font' => array(
                'size' => 9,
                'name' => 'Calibri',
            ));


        try {
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(4);
            $activeSheet->getColumnDimension('B')->setWidth(10);
            $activeSheet->getColumnDimension('C')->setWidth(20);
            $activeSheet->getColumnDimension('D')->setWidth(20);
            $activeSheet->getColumnDimension('E')->setWidth(20);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(10);
            $activeSheet->getColumnDimension('H')->setWidth(10);

            $activeSheet->setCellValue('A1', Yii::t('frontend', 'No'))->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', Yii::t('frontend', 'Pegawai ID'))->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', Yii::t('frontend', 'Bisnis Unit'))->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', Yii::t('frontend', 'Golongan'))->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', Yii::t('frontend', 'Struktur'))->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', Yii::t('frontend', 'Nama'))->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', Yii::t('frontend', 'Lama Bekerja'))->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', Yii::t('frontend', 'Mulai'))->getStyle('H1')->applyFromArray($headerStyle);

            $row = 2;
            $no = 1;

            foreach ($dataLoyalitasPegawai as $dataPegawai) {

                $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('B' . $row, $dataPegawai['idPegawai'])->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('C' . $row, $dataPegawai['bisnisUnit'])->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('D' . $row, $dataPegawai['golongan'])->getStyle('D' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('E' . $row, $dataPegawai['struktur'])->getStyle('E' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('F' . $row, $dataPegawai['nama'])->getStyle('F' . $row)->applyFromArray(array_merge($borderInside));
                $activeSheet->setCellValue('G' . $row, $dataPegawai['lamaBekerja'])->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                $activeSheet->setCellValue('H' . $row, $dataPegawai['mulai'])->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                $row++;
                $no++;
            }

            $row = $row - 1;
            $activeSheet->getStyle('A1:' . 'H' . $row)->applyFromArray($outLineBottomBorder);
            $activeSheet->getStyle('A1:' . 'H' . $row)->applyFromArray($font);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=LaporanLoyalitas-' . $this->bulan . '-' . $this->tahun . '.xls');
            header('Cache-Control: max-age=0');
            try {
                ob_end_clean();
                $Excel_writer->save('php://output');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }
}
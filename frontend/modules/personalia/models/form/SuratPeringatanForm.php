<?php
/**
 * Created by PhpStorm.
 * File Name: SuratPeringatanForm.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 26/10/2020
 * Time: 11:45
 */

namespace frontend\modules\personalia\models\form;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class SuratPeringatanForm
 * @property string $upload_dir;
 * @property string $doc_file;
 * @property string $id;
 * @package frontend\modules\personalia\models\form
 *
 */
class SuratPeringatanForm extends Model
{
    public $upload_dir = 'uploads/surat-peringatan/';

    public $doc_file;

    public $id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_file'], 'safe'],
            [['doc_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'maxFiles' => 1],
        ];
    }

    /**
     * @return array|array[]
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['upload'] = ['doc_file'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'doc_file' => Yii::t('frontend', 'File'),
        ];
    }
}
<?php

namespace frontend\modules\personalia\models;

use Exception;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\personalia\models\base\MppAct as BaseMppAct;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mpp_act".
 */
class MppAct extends BaseMppAct
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['golongan_id', 'jabatan_id', 'mpp_id', 'mpp_jumlah_id'], 'required'],
            [['jumlah', 'golongan_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'mpp_id', 'mpp_jumlah_id'], 'string', 'max' => 36],
            [['jabatan_id', 'created_by_pk'], 'string', 'max' => 32],
            [['keterangan'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param array $params
     * @return array|ActiveRecord|null
     */
    private function mppJumlah(array $params)
    {
        $year = date('Y');
        return MppJumlah::find()
            ->joinWith(['mpp' => function (ActiveQuery $query) use ($params) {
                $query->andWhere(['golongan_id' => $params['golongan'], 'jabatan_id' => $params['jabatan'], 'departemen_id' => $params['departemen']]);
            }])
            ->andWhere(['tahun' => $year])
            ->one();
    }

    /**
     * @return array
     */
    public function collectingMpp(): array
    {
        $queryPegawai = Pegawai::find()
            ->joinWith('perjanjianKerja')
            ->joinWith('pegawaiStruktur')
            ->joinWith('pegawaiGolongan')
            ->all();

        $data = [];
        /** @var Pegawai $pegawai */
        foreach ($queryPegawai as $pegawai) {
            $struktur = $pegawai->pegawaiStruktur->struktur;
            $mppJumlah = $this->mppJumlah(['departemen' => $struktur->departemen_id, 'jabatan' => $struktur->jabatan_id, 'golongan' => $pegawai->pegawaiGolongan->golongan_id]);
            if ($pegawai->pegawaiGolongan && $mppJumlah) {
                $data[$mppJumlah['id']]['departemen'][$struktur->departemen_id]['detail'] = ['nama' => $struktur->departemen->nama, 'mppId' => $mppJumlah['mpp_id']];
                $data[$mppJumlah['id']]['departemen'][$struktur->departemen_id]['jabatan'][$struktur->jabatan_id]['detail'] = ['nama' => $struktur->jabatan->nama];
                $data[$mppJumlah['id']]['departemen'][$struktur->departemen_id]['jabatan'][$struktur->jabatan_id]['golongan'][$pegawai->pegawaiGolongan->golongan_id]['detail'] = ['nama' => $pegawai->pegawaiGolongan->golongan->nama];
                $data[$mppJumlah['id']]['departemen'][$struktur->departemen_id]['jabatan'][$struktur->jabatan_id]['golongan'][$pegawai->pegawaiGolongan->golongan_id]['pegawai'][$pegawai->id] = [
                    'namaPegawai' => $pegawai->nama,
                    'namaJabatan' => $struktur->jabatan->nama,
                    'namaGolongan' => $pegawai->pegawaiGolongan->golongan->nama
                ];
            }
        }
        return $data;
    }

    /**
     * Menyimpan data mpp
     */
    public function saveMpp(): void
    {
        try {
            foreach ($this->collectingMpp() as $mppJumlahId => $mppNow) {
                foreach ($mppNow['departemen'] as $keyDepartemen => $departeman) {
                    foreach ($departeman['jabatan'] as $keyJabatan => $jabatan) {
                        foreach ($jabatan['golongan'] as $keyGolongan => $golongan) {
                            $mppAct = new MppAct();
                            $mppAct->mpp_jumlah_id = $mppJumlahId;
                            $cek = MppAct::findOne(['mpp_jumlah_id' => $mppAct->mpp_jumlah_id]);
                            if ($cek) {
                                $mppAct = $cek;
                            }
                            $mppAct->mpp_id = $departeman['detail']['mppId'];
                            $mppAct->jabatan_id = $keyJabatan;
                            $mppAct->golongan_id = $keyGolongan;
                            $mppAct->jumlah = count($golongan['pegawai']);
                            $mppAct->save();
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * @param int $tahun
     * @return array
     */
    public function formatingReport(int $tahun): array
    {
        $query = MppAct::find()->joinWith(['mppJumlah' => function (ActiveQuery $query) use ($tahun) {
            $query->andWhere(['tahun' => $tahun]);
        }])->all();

        $datas = [];
        /** @var MppAct $mppAct */
        foreach ($query as $mppAct) {
            $datas['departemen'][$mppAct->mpp->departemen_id]['detail'] = ['nama' => $mppAct->mpp->departemen->nama];
//            $datas['departemen'][$mppAct->mpp->departemen_id]['golongan'][$mppAct->golongan_id]['detail'] = ['nama' => $mppAct->golongan->nama];
            $datas['departemen'][$mppAct->mpp->departemen_id]['golonganjabatan'][$mppAct->golongan_id . $mppAct->jabatan_id]['detail'] = [
                'namaJabatan' => $mppAct->jabatan->nama, 'jumlahAct' => $mppAct->jumlah, 'jumlahMpp' => $mppAct->mppJumlah->jumlah,
                'namaGolongan' => $mppAct->golongan->nama,
            ];
        }

        return $datas;
    }

    /**
     * @return int
     */
    public function hitungSelisih(): int
    {
        return $this->mppJumlah->jumlah - $this->jumlah;

    }

}

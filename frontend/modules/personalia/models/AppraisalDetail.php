<?php

namespace frontend\modules\personalia\models;

use frontend\modules\personalia\models\base\AppraisalDetail as BaseAppraisalDetail;

/**
 * This is the model class for table "appraisal_detail".
 */
class AppraisalDetail extends BaseAppraisalDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nilai', 'ref_appraisal_id', 'appraisal_id'], 'required'],
            [['komentar'], 'string'],
            [['nilai', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'ref_appraisal_id', 'appraisal_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $model = Appraisal::findOne(['id' => $this->appraisal_id]);
        $model->total_nilai = $this->totalNilai();
        $model->rata_rata = $this->rataRata();
        if ($model->rata_rata) {
            $model->rekomendasi = $this->rekomendasiNilai();
        }
        $model->save(false);
        return true;
    }

    /**
     * @return int
     */
    public function totalNilai()
    {
        $query = self::find()->andWhere(['appraisal_id' => $this->appraisal_id]);
        return (int)$query->sum('nilai');
    }

    /**
     * @return float
     */
    public function rataRata()
    {
        $average = 0;
        $total = $this->totalNilai();
        if ($total) {
            $count = self::find()->andWhere(['appraisal_id' => $this->appraisal_id])->count();
            $average = $total / $count;
        }
        return (float)$average;
    }

    /**
     * @return int
     */
    public function rekomendasiNilai()
    {
        $rekomendasi = 0;
        if ($this->rataRata() >= 4) {
            $rekomendasi = Appraisal::DIATAS_RATA_RATA;
        }
        if (($this->rataRata() >= 3.5) and ($this->rataRata() < 4)) {
            $rekomendasi = Appraisal::CUKUP;
        }
        if (($this->rataRata() >= 3) and ($this->rataRata() < 3.5)) {
            $rekomendasi = Appraisal::DIBAWAH_STANDAR_MAJU;
        }
        if ($this->rataRata() < 3) {
            $rekomendasi = Appraisal::DIBAWAH_STANDAR;
        }
        return $rekomendasi;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        parent::beforeValidate();
        if (is_string($this->nilai)) {
            $this->nilai = (int)$this->nilai;
        }

        return true;
    }

    /**
     * @param array $post
     * @return bool
     */
    public function getValuePost(array $post): bool
    {
        if (!empty($post)) {
            foreach ($post as $key => $value) {
                $exp = explode('-', $key);
                $datas = [];
                if ($exp[0] == 'nilai') {
                    $datas = [
                        'appraisal_id' => $this->appraisal_id,
                        'ref_appraisal_id' => $exp[1],
                        'nilai' => $value
                    ];
                }
                if (!empty($datas)) {
                    $model = new AppraisalDetail();
                    $model->setAttributes($datas);
                    $model->save();
                }
            }
            return true;
        }
        return false;
    }

    public function getValuePostManagerial(array $post): bool
    {
        if (!empty($post)) {
            foreach ((array)$this->nilai as $key => $value) {
                $datas = [
                    'appraisal_id' => $this->appraisal_id,
                    'ref_appraisal_id' => $key,
                    'nilai' => $value,
                    'komentar' => $this->komentar[$key]
                ];
                if (!empty($datas)) {
                    $model = new AppraisalDetail();
                    $model->setAttributes($datas);
                    $model->save();
                }
            }
            return true;
        }
        return false;
    }
}
<?php

namespace frontend\modules\personalia\models;

use DateTime;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\base\SuratPegawai as BaseSuratPegawai;
use Yii;
use yii\db\DataReader;
use yii\db\Exception;

/**
 * This is the model class for table "surat_pegawai".
 * @property array $TtdOleh
 * @property array $_jenis
 */
class SuratPegawai extends BaseSuratPegawai
{
    const pkwt = 1;
    const sk_acting = 2;
    const addedum = 3;
    const sk_pengakatan = 4;
    const sk_promosi_jabatan = 5;
    const sk_demosi = 6;
    const sk_mutasi = 7;
    const sk_aktif = 8;

    public $_jenis = [
        self::pkwt => 'PERJANJIAN KERJA WAKTU TERTENTU',
        self::sk_acting => 'SURAT KEPUTUSAN - ACTING PROMOSI',
        self::addedum => 'ADDENDUM',
        self::sk_pengakatan => 'SURAT KEPUTUSAN - PENGANGKATAN',
        self::sk_promosi_jabatan => 'SURAT KEPUTUSAN - PROMOSI JABATAN',
        self::sk_demosi => 'SURAT KETERANGAN - DEMOSI',
        self::sk_mutasi => 'SURAT KETERANGAN - MUTASI',
        self::sk_aktif => 'SURAT KETERANGAN - AKTIF',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'tgl_mulai'], 'required'],
            [['seq_code', 'jenis', 'ref_area_kerja_id', 'struktur_id', 'golongan_id', 'jenis_perjanjian', 'kontrak', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tgl_mulai', 'tgl_akhir', 'created_at', 'updated_at', 'deleted_at', 'pegawai_golongan_id', 'perjanjian_kerja_id', 'pegawai_struktur_id'], 'safe'],
            [['catatan'], 'string'],
            [['id', 'pegawai_id', 'pegawai_golongan_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'area_kerja_id', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['tujuan_surat'], 'string', 'max' => 225],
            [['lock', 'ref_area_kerja_id', 'struktur_id', 'golongan_id', 'jenis_perjanjian', 'kontrak'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'checkNpwpForPkwt']
        ];
    }

    /**
     * @param string $attribute
     */
    public function checkNpwpForPkwt(string $attribute)
    {
        if ($this->jenis == self::pkwt) {
            if ($pegawai = $this->pegawai) {
                if ($npwp = $pegawai->pegawaiNpwp) {
                    if (empty($npwp->nomor)) {
                        $this->addError($attribute, Yii::t('frontend', 'NPWP belum diisi.'));
                    }
                }else{
                    $this->addError($attribute, Yii::t('frontend', 'NPWP belum diisi.'));
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getTtdOleh()
    {
        $query = Pegawai::find()->andWhere(['IN', 'id', ['105f989c3f2211e9b9d41831bfb9b9e1', '583642363f2111e9b9d41831bfb9b9e1', 'a45441a8414511e9b9d41831bfb9b9e1']])->all();
        $datas = [];
        /** @var Pegawai $data */
        foreach ($query as $data) {
            $datas[$data->id] = $data->nama;
        }
        return $datas;
    }

    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tgl_mulai)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tgl_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tgl_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tgl_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getAreaKerjaUntukTanggal($this->tgl_mulai)) {
                $this->addError($attribute, Yii::t('frontend', 'Area kerka aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($insert) {
            $this->seq_code = $this->generateSeqCode();
        }
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tgl_mulai)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tgl_mulai)) {
            $this->pegawai_golongan_id = $golongan->id;
//            $this->golongan_id = $golongan->golongan_id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tgl_mulai)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        if ($areaKerja = $this->pegawai->getAreaKerjaUntukTanggal($this->tgl_mulai)) {
            $this->area_kerja_id = $areaKerja->id;
//            $this->ref_area_kerja_id = $areaKerja->ref_area_kerja_id;
        }
        if ($perjanjianKerja = $this->olehPegawai->getPerjanjianKerjaUntukTanggal($this->tgl_mulai)) {
            $this->oleh_perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($struktur = $this->olehPegawai->getPegawaiStrukturUntukTanggal($this->tgl_mulai)) {
            $this->oleh_pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @return PerjanjianKerja
     */
    public function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function tglMasaKerja()
    {
        $m = PerjanjianKerja::getPerjanjianDasarStatic($this->pegawai_id);
        if ($m) {
            return $m->mulai_berlaku;
        }
        return '';
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function intervalMonth()
    {
        $date1 = new DateTime($this->tgl_mulai);
        $date2 = new DateTime($this->tgl_akhir);
        $date2->modify('+1 day');

        $interval = $date2->diff($date1);

        return $interval->format('%m');
    }

    /**
     * @return false|string|DataReader|null
     * @throws Exception
     */
    public function generateSeqCode()
    {
        $query = $this->getDb()->createCommand("SELECT max(seq_code) FROM {$this->tableName()} where seq_code like \"{$this->jenis}" . date('y') . "%\"")->queryScalar();
        $seqCode = $this->jenis . date('y') . '001';
        if ($query) {
            $seqCode = $query + 1;
        }
        return $seqCode;
        //SELECT CONCAT(2, date_format(curdate(), '%y'),IF(CONCAT(2,SUBSTRING(MAX(seq_code),2,2)) = CONCAT(2,date_format(curdate(), '%y')),LPAD(SUBSTRING(MAX(seq_code),4,3)+1,3,'0'), '001')) FROM `hris-aws-20200917`.surat_pegawai
        #return $this->getDb()->createCommand("SELECT CONCAT({$this->jenis}, date_format(curdate(), '%y'),IF(MAX(CONCAT({$this->jenis},SUBSTRING(seq_code,2,2))) = CONCAT({$this->jenis},date_format(curdate(), '%y')),LPAD(MAX(SUBSTRING(seq_code,4,3))+1,3,'0'), '001')) FROM {$this->tableName()}")->queryScalar();
    }
}

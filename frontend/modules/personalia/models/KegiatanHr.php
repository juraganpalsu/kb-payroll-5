<?php

namespace frontend\modules\personalia\models;

use frontend\modules\personalia\models\base\KegiatanHr as BaseKegiatanHr;

/**
 * This is the model class for table "kegiatan_hr".
 *
 *
 * @property array $_jenis_kegiatan
 * @property string $jenisKegiatan
 */
class KegiatanHr extends BaseKegiatanHr
{

    const INTERNAL = 1;
    const EKSTERNAL = 2;

    public $_jenis_kegiatan = [self::INTERNAL => 'INTERNAL', self::EKSTERNAL => 'EKSTERNAL'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_kegiatan', 'nama', 'dari', 'sampai', 'peserta', 'lokasi'], 'required'],
            [['jenis_kegiatan', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['dari', 'sampai', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['peserta', 'catatan'], 'string'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama', 'lokasi'], 'string', 'max' => 225],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return mixed
     */
    public function getJenisKegiatan()
    {
        return $this->_jenis_kegiatan[$this->jenis_kegiatan];
    }

}

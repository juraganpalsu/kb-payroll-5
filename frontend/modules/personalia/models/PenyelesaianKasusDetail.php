<?php

namespace frontend\modules\personalia\models;

use Yii;
use \frontend\modules\personalia\models\base\PenyelesaianKasusDetail as BasePenyelesaianKasusDetail;

/**
 * This is the model class for table "penyelesaian_kasus_detail".
 */
class PenyelesaianKasusDetail extends BasePenyelesaianKasusDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'penyelesaian_kasus_id'], 'required'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'penyelesaian_kasus_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

<?php

namespace frontend\modules\personalia\models;

use Yii;
use \frontend\modules\personalia\models\base\RefAppraisal as BaseRefAppraisal;

/**
 * This is the model class for table "ref_appraisal".
 * @property array $_kategori
 */
class RefAppraisal extends BaseRefAppraisal
{
    const STAFF = 1;
    const MANAGERIAL = 2;

    public $_kategori = [self::STAFF => 'STAFF', self::MANAGERIAL => 'MANAGERIAL'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kategori', 'kriteria', 'keterangan_kiri'], 'required'],
            [['keterangan_kiri', 'keterangan_kanan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['kategori', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['kriteria'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

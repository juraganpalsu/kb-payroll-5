<?php

namespace frontend\modules\personalia\models;

use frontend\modules\pegawai\models\Pegawai;
use Yii;
use \frontend\modules\personalia\models\base\KecelakaanKerja as BaseKecelakaanKerja;

/**
 * This is the model class for table "kecelakaan_kerja".
 *
 * @property string $namaPegawai
 */
class KecelakaanKerja extends BaseKecelakaanKerja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'jam', 'pegawai_id', 'klaim_bpjs'], 'required'],
//            [['tanggal', 'jam', 'lokasi', 'kronologi', 'cedera', 'penyebab', 'preventif', 'pegawai_id'], 'required'],
            [['tanggal', 'jam', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['kronologi', 'penyebab', 'preventif', 'catatan'], 'string'],
            [['klaim_bpjs', 'berita_acara', 'tahap_satu', 'tahap_dua', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lokasi', 'cedera'], 'string', 'max' => 225],
            [['klaim_bpjs', 'berita_acara', 'tahap_satu', 'tahap_dua', 'foto_ktp', 'kartu_bpjs', 'absensi', 'lock'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'pegawaiAktifValidation']
        ];
    }
    //

    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tanggal)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        return true;
    }


    /**
     * @return string
     */
    public function getNamaPegawai()
    {
        $nama = '';
        if ($this->pegawai) {
            if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
                $nama = $perjanjianKerja->pegawai->nama_lengkap . '-' . $perjanjianKerja->id_pegawai;
            }
        }
        return $nama;
    }
}

<?php

namespace frontend\modules\personalia\models;

use common\components\Status;
use DateTime;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\base\SuratPeringatan as BaseSuratPeringatan;
use Yii;
use yii\db\DataReader;
use yii\db\Exception;

/**
 * This is the model class for table "surat_peringatan".
 *
 * @property string $gmp_
 * @property array $TtdOleh
 */
class SuratPeringatan extends BaseSuratPeringatan
{

    const SP1 = 4;
    const SP2 = 5;
    const SP3 = 6;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mulai_berlaku', 'akhir_berlaku', 'ref_surat_peringatan_id', 'ref_peraturan_perusahaan_id', 'pegawai_id', 'gmp',], 'required'],
            [['mulai_berlaku', 'akhir_berlaku', 'perjanjian_kerja_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_at', 'updated_at', 'deleted_at', 'atasan_pegawai_id'], 'safe'],
            [['catatan'], 'string'],
            [['gmp', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id', 'ref_surat_peringatan_id', 'perjanjian_kerja_id', 'pegawai_id', 'pegawai_golongan_id', 'pegawai_struktur_id', 'created_by_pk', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'atasan_pegawai_id', 'atasan_pegawai_struktur_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'pegawaiAktifValidation'],
            [['akhir_berlaku'], 'akhirBerlakuValidation'],
            [['ref_peraturan_perusahaan_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function getTtdOleh()
    {
        $query = Pegawai::find()->andWhere(['IN', 'id', ['105f989c3f2211e9b9d41831bfb9b9e1', '583642363f2111e9b9d41831bfb9b9e1', 'a45441a8414511e9b9d41831bfb9b9e1', '388d32bab22211ebb71d0050568c1db4']])->all();
        $datas = [];
        /** @var Pegawai $data */
        foreach ($query as $data) {
            $datas[$data->id] = $data->nama;
        }
        return $datas;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        parent::beforeValidate();
        $this->ref_peraturan_perusahaan_id = implode(',', (array)$this->ref_peraturan_perusahaan_id);
        return true;
    }

    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->mulai_berlaku) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->mulai_berlaku]));
        }
    }

    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->mulai_berlaku)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($this->mulai_berlaku)) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->mulai_berlaku)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->mulai_berlaku)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->mulai_berlaku)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->mulai_berlaku)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->mulai_berlaku)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        if ($perjanjianKerja = $this->olehPegawai->getPerjanjianKerjaUntukTanggal($this->mulai_berlaku)) {
            $this->oleh_perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($struktur = $this->olehPegawai->getPegawaiStrukturUntukTanggal($this->mulai_berlaku)) {
            $this->oleh_pegawai_struktur_id = $struktur->id;
        }
        if ($struktur = $this->atasanPegawai->getPegawaiStrukturUntukTanggal($this->mulai_berlaku)) {
            $this->atasan_pegawai_struktur_id = $struktur->id;
        }

        if (is_array($this->ref_peraturan_perusahaan_id)) {
            $this->ref_peraturan_perusahaan_id = implode(',', (array)$this->ref_peraturan_perusahaan_id);
        }
        return true;
    }

    /**
     * @return array|mixed
     */
    public function getGmp_()
    {
        return Status::activeInactive($this->gmp);
    }

    /**
     * @return PerjanjianKerja
     */
    public function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @return false|string|DataReader|null
     * @throws Exception
     */
    public function generateSeqCode()
    {
        //SELECT CONCAT(2, date_format(curdate(), '%y'),IF(CONCAT(2,SUBSTRING(MAX(seq_code),2,2)) = CONCAT(2,date_format(curdate(), '%y')),LPAD(SUBSTRING(MAX(seq_code),4,3)+1,3,'0'), '001')) FROM `hris-aws-20200917`.surat_pegawai
        return $this->getDb()->createCommand("SELECT CONCAT({ date_format(curdate(), '%y'),IF(CONCAT(SUBSTRING(MAX(seq_code),2,2)) = CONCAT(date_format(curdate(), '%y')),LPAD(SUBSTRING(MAX(seq_code),4,3)+1,3,'0'), '001')) FROM {$this->tableName()}")->queryScalar();
    }

    /**
     * @param string $glue
     * @param int $maxString
     * @return string
     */
    public function getPeraturanPerusahaanFormated(string $glue = ',', int $maxString = 0)
    {
        $ref = explode(',', $this->ref_peraturan_perusahaan_id);
        $queryPeraturan = RefPeraturanPerusahaan::find()->select('nama')->andWhere(['IN', 'id', $ref])->column();
        if ($maxString > 0) {
            $queryPeraturan = array_map(function ($data) use ($maxString) {
                return substr($data, 0, $maxString);
            }, $queryPeraturan);
        }
        return implode($glue, (array)$queryPeraturan);
    }

    /**
     * @param int $id
     * @return RefSuratPeringatan|null
     */
    public static function getModelRef(int $id): RefSuratPeringatan
    {
        return RefSuratPeringatan::findOne($id);
    }

    /**
     * @param int $spId
     * @param int $month
     * @param int $year
     * @return int
     */
    private function queryDashboard(int $spId, int $month, int $year): int
    {
        return (int)SuratPeringatan::find()->select('ref_surat_peringatan_id, COUNT(*)')->andWhere(['month(mulai_berlaku)' => $month, 'year(mulai_berlaku)' => $year, 'ref_surat_peringatan_id' => $spId])->count();
    }

    /**
     * @return array
     */
    public function formatingDataDashboard(): array
    {
        $now = new DateTime();
        $nowMonth = $now->format('m');
        $nowMonthString = $now->format('M');
        $nowYear = $now->format('Y');
        $previous = $now->modify('-1 month');
        $previousMonth = $previous->format('m');
        $previousString = $previous->format('M');
        $previousYear = $previous->format('Y');
        $first = $previous->modify('-1 month');
        $firstMonth = $first->format('m');
        $firstMonthString = $first->format('M');
        $firstYear = $first->format('Y');
        $categories = [$firstMonthString, $previousString, $nowMonthString];
        $series = [
            [
                'name' => self::getModelRef(self::SP1)->nama,
                'data' => [
                    $this->queryDashboard(self::SP1, $firstMonth, $firstYear),
                    $this->queryDashboard(self::SP2, $firstMonth, $firstYear),
                    $this->queryDashboard(self::SP3, $firstMonth, $firstYear)
                ],
            ],
            [
                'name' => self::getModelRef(self::SP2)->nama,
                'data' => [
                    $this->queryDashboard(self::SP1, $previousMonth, $previousYear),
                    $this->queryDashboard(self::SP2, $previousMonth, $previousYear),
                    $this->queryDashboard(self::SP3, $previousMonth, $previousYear)
                ],
            ],
            [
                'name' => self::getModelRef(self::SP3)->nama,
                'data' => [
                    $this->queryDashboard(self::SP1, $nowMonth, $nowYear),
                    $this->queryDashboard(self::SP2, $nowMonth, $nowYear),
                    $this->queryDashboard(self::SP3, $nowMonth, $nowYear)
                ],
            ]
        ];
        return ['categories' => $categories, 'series' => $series];
    }

}
<?php

namespace frontend\modules\personalia\models;

use frontend\modules\pegawai\models\AreaKerja;
use frontend\modules\pegawai\models\Pegawai;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\personalia\models\base\Resign as BaseResign;
use Yii;

/**
 * This is the model class for table "resign".
 *
 * @property array $_tipe
 * @property string $tipe_
 * @property array $TtdOleh
 *
 *
 *
 * @property string $namaPegawai
 *
 */
class Resign extends BaseResign
{

    const KEMAUAN_SENDIRI = 1;
    const PHK = 2;
    const PENSIUN = 3;
    const PROMOSI = 4;
    const MUTASI = 5;
    const DEMOSI = 6;

    public $_tipe = [self::KEMAUAN_SENDIRI => 'Kemauan Sendiri', self::PHK => 'PHK', self::PENSIUN => 'Pensiun',
        self::PROMOSI => 'Promosi', self::MUTASI => 'Mutasi', self::DEMOSI => 'Demosi'
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'pegawai_id', 'tipe'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['tipe', 'exit_letter', 'exit_clearence', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['alasan'], 'string'],
            [['id', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'area_kerja_id', 'oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['oleh_pegawai_id', 'oleh_perjanjian_kerja_id', 'oleh_pegawai_struktur_id', 'lock'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_id', 'pegawaiAktifValidation']
        ];
    }

    /**
     * @return mixed
     */
    public function getTipe_()
    {
        return $this->_tipe[$this->tipe];
    }

    /**
     * @return array
     */
    public function getTtdOleh()
    {
        $query = Pegawai::find()->andWhere(['IN', 'id', ['105f989c3f2211e9b9d41831bfb9b9e1', '583642363f2111e9b9d41831bfb9b9e1', 'a45441a8414511e9b9d41831bfb9b9e1']])->all();
        $datas = [];
        /** @var Pegawai $data */
        foreach ($query as $data) {
            $datas[$data->id] = $data->nama;
        }
        return $datas;
    }

    /**
     * @param string $attribute
     */
    public function pegawaiAktifValidation(string $attribute)
    {
        $modelPegawai = Pegawai::findOne($this->$attribute);
        if ($modelPegawai && !empty($this->tanggal)) {
            $perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal);
            if ($perjanjianKerja) {
                if (self::findOne(['perjanjian_kerja_id' => $perjanjianKerja->id])) {
                    $this->addError($attribute, Yii::t('frontend', 'Perjanjian kerja ini sudah berakhir(resign).'));
                }
            }
            if (!$perjanjianKerja) {
                $this->addError($attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getAreaKerjaUntukTanggal($this->tanggal)) {
                $this->addError($attribute, Yii::t('frontend', 'Area kerka aktif tidak ditemukan.'));
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
            $this->perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($golongan = $this->pegawai->getPegawaiGolonganUntukTanggal($this->tanggal)) {
            $this->pegawai_golongan_id = $golongan->id;
        }
        if ($struktur = $this->pegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
            $this->pegawai_struktur_id = $struktur->id;
        }
        if ($areaKerja = $this->pegawai->getAreaKerjaUntukTanggal($this->tanggal)) {
            $this->area_kerja_id = $areaKerja->id;
        }
        if ($perjanjianKerja = $this->olehPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
            $this->oleh_perjanjian_kerja_id = $perjanjianKerja->id;
        }
        if ($struktur = $this->olehPegawai->getPegawaiStrukturUntukTanggal($this->tanggal)) {
            $this->oleh_pegawai_struktur_id = $struktur->id;
        }
        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->updateKomponenPegawai();

        return true;
    }

    /**
     * Set Status komponen pegawai
     */
    public function updateKomponenPegawai()
    {
        if ($this->perjanjianKerja) {
            $modelPerjanjianKerja = PerjanjianKerja::findOne($this->perjanjian_kerja_id);
            $modelPerjanjianKerja->tanggal_resign = $this->tanggal;
            if (empty($modelPerjanjianKerja->akhir_berlaku)) {
                $modelPerjanjianKerja->akhir_berlaku = $this->tanggal;
            }
            $modelPerjanjianKerja->save(false);
        }

//        $dasarMasaKerja = PerjanjianKerja::getPerjanjianDasarStatic($this->pegawai_id);
//        if ($dasarMasaKerja) {
//            $dasarMasaKerja->dasar_masa_kerja = Status::TIDAK;
//            $dasarMasaKerja->save(false);
//        }

        if ($this->pegawaiStruktur) {
            $modelStruktur = PegawaiStruktur::findOne($this->pegawai_struktur_id);
            $modelStruktur->akhir_berlaku = $this->tanggal;
            $modelStruktur->save();
        }

        if ($this->pegawaiGolongan) {
            $modelPegawaiGolongan = PegawaiGolongan::findOne($this->pegawai_golongan_id);
            $modelPegawaiGolongan->akhir_berlaku = $this->tanggal;
            $modelPegawaiGolongan->save();
        }

        if ($this->areaKerja) {
            $modelAreaKerja = AreaKerja::findOne($this->area_kerja_id);
            $modelAreaKerja->akhir_berlaku = $this->tanggal;
            $modelAreaKerja->save();
        }
    }

    /**
     * @return string
     */
    public function getNamaPegawai()
    {
        $nama = '';
        if ($this->pegawai) {
            if ($perjanjianKerja = $this->pegawai->getPerjanjianKerjaUntukTanggal($this->tanggal)) {
                $nama = $perjanjianKerja->pegawai->nama_lengkap . '-' . $perjanjianKerja->id_pegawai;
            }
        }
        return $nama;
    }

    /**
     * @return PerjanjianKerja
     */
    public function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    public function tglAwalMk()
    {
        $m = PerjanjianKerja::getPerjanjianDasarStatic($this->pegawai_id);
        if ($m) {
            return $m->mulai_berlaku;
        }
        return '';
    }

}

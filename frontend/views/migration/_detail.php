<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Migration */

?>
<div class="migration-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->version) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'version',
        'apply_time:datetime',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
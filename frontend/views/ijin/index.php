<?php

use common\models\Divisi;
use common\models\Ijin;
use common\models\Pegawai;
use common\models\Unit;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\IjinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ijin');
$this->params['breadcrumbs'][] = $this->title;


$js = <<<JS
$(function() {
    $('.search-button').click(function(){
	    $('.search-form').toggle(1000);
	    return false;
    });
  
    $('.btn-delete-multiple').on('click', function(e) {
        e.preventDefault();
        krajeeDialog.confirm("Are you really sure you want to do this?", function (result) {
            if (result) {
                let btn = $('.btn-delete-multiple');
                btn.button('loading');
                let keys = $('#grid-ijin').yiiGridView('getSelectedRows');
                $.post(btn.attr('href'), {ids : keys}).done(function(dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
                return false;
            }
        });
    });
});
JS;

$this->registerJs($js);
?>
<div class="ijin-index">

    <p>
        <?= Html::a(Yii::t('frontend', 'Delete'), ['delete-multiple'], ['class' => 'btn btn-success btn-danger btn-flat btn-delete-multiple']) ?>
    </p>

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'id_pegawai',
            'label' => Yii::t('app', 'Id Pegawai'),
            'value' => function (Ijin $model) {
                $modelPegawai = $model->pegawai;
                return $modelPegawai->getPerjanjianKerjaUntukTanggal($model->tanggal) ? str_pad($modelPegawai->getPerjanjianKerjaUntukTanggal($model->tanggal)->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
            },
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('app', 'Pegawai'),
            'value' => function (Ijin $model) {
                return $model->pegawai->nama;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Pegawai::find()->all(), 'id', 'nama_'),
            'filterWidgetOptions' => [
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/pegawai/pegawai/get-pegawai-for-select2']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) {
                            return {q:params.term}; 
                        }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(a) { return a.text; }'),
                    'templateSelection' => new JsExpression('function (a) { return a.text; }'),
                ]
            ],
            'filterInputOptions' => ['placeholder' => 'Pegawai', 'id' => 'grid-ijin-search-pegawai_id']
        ],
        [
            'attribute' => 'tanggal',
            'label' => Yii::t('app', 'Tanggal'),
            'value' => function (Ijin $model) {
                return date('d-m-Y', strtotime($model->tanggal));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'tipe_',
            'label' => Yii::t('app', 'Tipe'),
            'width' => '20%',
            'value' => function ($model) {
                return $model->tipe_;
            },
            'filter' => $searchModel->_tipe,
        ],
        [
            'attribute' => 'shift',
            'label' => Yii::t('app', 'Shift'),
            'width' => '10%',
            'value' => function (Ijin $model) {
                return $model->shift_;
            },
            'filter' => $searchModel->_shift(),
        ],
        'keterangan',
        ['attribute' => 'lock', 'hidden' => true],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}'
        ],
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'headerOptions' => ['class' => 'kartik-sheet-style'],
        ],
    ];
    ?>
    <?php try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9],
            'noExportColumns' => [1, 10], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'ijin' . date('dmYHis')
        ]);
        echo GridView::widget([
            'id' => 'grid-ijin',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ijin']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ijin */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Ijin',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ijin'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ijin-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

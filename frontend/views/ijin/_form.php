<?php

use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ijin */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="ijin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'tipe')->dropDownList($model->_tipe, ['placeholder' => 'Tipe']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'tanggal')->widget(DateControl::classname(), [
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                    ],
                ]
            ]); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'pegawai_ids')->widget(Select2::className(), [
                'options' => [
                    'placeholder' => Yii::t('app', '-- Pilih Pegawai'),
                    'multiple' => true,
                ],
                'data' => $model->pegawai_exist,
                'maintainOrder' => true,
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/pegawai/pegawai/get-pegawai-for-select2']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) {
                            return {q:params.term}; 
                        }')
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true, 'placeholder' => 'Keterangan']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'shift')->dropDownList($model->_shift(), ['prompt' => '-']) ?>
        </div>
    </div>
    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <hr>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

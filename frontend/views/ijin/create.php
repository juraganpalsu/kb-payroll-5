<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Ijin */

$this->title = Yii::t('app', 'Create Ijin');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ijin'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ijin-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

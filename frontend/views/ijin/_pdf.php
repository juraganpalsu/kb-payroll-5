<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ijin */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ijin'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ijin-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Ijin').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'tipe',
        'tanggal',
        [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('app', 'Pegawai')
        ],
        'keterangan',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ijin */

$this->title = $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ijin'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ijin-view">

    <div class="row">
        <div class="col-sm-12">

            <?php // Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger pull-right btn-flat',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumn = [
                'tipe_',
                'tanggal',
                [
                    'attribute' => 'pegawai.nama',
                    'label' => Yii::t('app', 'Pegawai'),
                ],
                'keterangan',
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]);
            ?>
        </div>
    </div>
</div>
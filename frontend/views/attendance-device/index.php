<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\AttendanceDeviceSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('frontend', 'Attendance Device');
$this->params['breadcrumbs'][] = $this->title;
$actionGetDevice = Url::to(['get-device']);
$search = <<<JS
    $('.btn-get-device').click(function(){
        var btn = $(this);
        btn.button('loading');
        $.get('$actionGetDevice').done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.reload();
            }
            btn.button('reset');
        });
        return false;
    });
JS;

$this->registerJs($search);
?>
<div class="attendance-device-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id'],
        'name',
        'notes:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    ?>
    <?php
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-attendance-device']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-success btn-flat btn-get-device', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                '{export}',
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AttendanceDevice */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Attendance Device',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Attendance Device'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="attendance-device-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

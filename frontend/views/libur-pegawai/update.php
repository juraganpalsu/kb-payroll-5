<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\LiburPegawai */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Libur Pegawai',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Libur Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="libur-pegawai-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

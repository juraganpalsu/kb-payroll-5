<?php

use common\models\Divisi;
use common\models\LiburPegawai;
use common\models\SistemKerja;
use common\models\Unit;
use frontend\models\Pegawai;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\LiburPegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Libur Pegawai');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function() {
    $('.search-button').click(function(){
	    $('.search-form').toggle(1000);
	    return false;
    });
  
    $('.get-month').on('click', function(e) {
        e.preventDefault();
        var btn = $(this);
        var month = btn.data('month');
        var url = btn.data('url');
        $.post(url, {month : month})
        .done(function(dt) {
            var win = window.open(dt.url);
            win.focus();
            return true;
        }).fail(function() {
            console.log('Gagal')
        })
    });
    
    $('.btn-delete-multiple').on('click', function(e) {
        e.preventDefault();
        krajeeDialog.confirm("Are you really sure you want to do this?", function (result) {
            if (result) {
                var btn = $('.btn-delete-multiple');
                btn.button('loading');
                var keys = $('#grid-libur-pegawai').yiiGridView('getSelectedRows');
                $.post(btn.attr('href'), {ids : keys}).done(function(dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.reload();
                    }
                    btn.button('reset');
                });
                return false;
            }
        });
    });
});
JS;

$this->registerJs($js);
?>
<div class="libur-pegawai-index">

    <div class="row">
        <div class="col-sm-12">
            <?= Html::a(Yii::t('frontend', 'Upload'), ['upload-libur'], ['class' => 'btn btn-success btn-flat']) ?>
            <?php
            try {
                echo ButtonDropdown::widget([
                    'label' => Yii::t('frontend', 'Download Template Libur'),
                    'options' => [
                        'class' => 'btn btn-warning btn-flat',
                    ],
                    'dropdown' => [
                        'items' => $searchModel->monthDropdown
                    ],
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'excetion');
            }
            ?>
            <?= Html::a(Yii::t('frontend', 'Delete'), ['delete-multiple'], ['class' => 'btn btn-success btn-danger btn-flat btn-delete-multiple']) ?>
        </div>
    </div>
    <p>
        <?php // Html::a(Yii::t('app', 'Download File'), ['create'], ['class' => 'btn btn-warning']) ?>
        <?php // Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?php //  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'idfp',
            'label' => Yii::t('app', 'Idfp'),
            'value' => function (LiburPegawai $model) {
                return $model->pegawai->getPerjanjianKerjaUntukTanggal($model->tanggal) ? $model->pegawai->getPerjanjianKerjaUntukTanggal($model->tanggal)->id_pegawai : '';
            },
            'format' => 'html'
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('app', 'Pegawai'),
            'value' => function (LiburPegawai $model) {
                return $model->pegawai ? $model->pegawai->nama : '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(Pegawai::find()->all(), 'id', 'nama_'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Pegawai', 'id' => 'grid-libur-pegawai-search-pegawai_id']
        ],
        [
            'attribute' => 'tanggal',
            'label' => Yii::t('app', 'Tanggal'),
            'value' => function (LiburPegawai $model) {
                return date('d-m-Y', strtotime($model->tanggal));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'sistem_kerja',
            'width' => '20%',
            'value' => function (LiburPegawai $model) {
                $text = '';
                if (!is_null($model->pegawai->pegawaiSistemKerja)) {
                    $text = $model->pegawai->pegawaiSistemKerja->sistemKerja->nama;
                }
                return $text;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('sistem_kerja'), 'id' => 'grid-pegawai-search-sistem_kerja'],
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'created_by',
            'value' => function (LiburPegawai $model) {
                if (!is_null($createdBy = $model->createdBy)) {
                    $nama = $createdBy->userPegawai ? $createdBy->userPegawai->pegawai->nama : '';
                    $namaTampil = explode(' ', $nama);
                    return $namaTampil[0];
                }
                return '';
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function (LiburPegawai $model) {
                return date('d-m-Y H:m', strtotime($model->created_at));
            },
        ],
        ['attribute' => 'lock', 'hidden' => true],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}'
        ],
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'headerOptions' => ['class' => 'kartik-sheet-style'],
        ],
    ];
    ?>

    <?php try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6],
            'noExportColumns' => [1, 7], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'Libur-pegawai' . date('dmYHis')
        ]);
        echo GridView::widget([
            'id' => 'grid-libur-pegawai',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-libur-pegawai']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                [
                    'content' => $export
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

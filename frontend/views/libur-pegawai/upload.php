<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 7/25/17
 * Time: 3:19 PM
 *
 * @var $model \frontend\models\LiburPegawai
 */

use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$this->title = 'Upload Libur Pegawai';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Libur Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin([
                'id' => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>

            <div class="row">
                <div class="col-sm-6">
                    <?php
                    echo $form->field($model, 'month')->dropDownList($model->_month, ['id' => 'month']);
                    ?>
                </div>
            </div>

            <?php
            echo $form->field($model, 'doc_file')->widget(FileInput::classname(), [
                'options' => ['multiple' => false],
                'pluginOptions' => [
                    'previewFileType' => 'image',
                    'uploadUrl' => Url::to(['/libur-pegawai/upload-libur']),
                    'allowedFileExtensions' => ['xls', 'xlsx'],
                    'uploadExtraData' => new JsExpression("function (previewId, index) {
                        return {
                            month: $(\"#month :selected\").val(),
                            };
                        }"),
                ],
//                'pluginEvents' => [
//                    'fileuploaded' => 'function(event, data){ var win = window.open(data.response.data.url); win.focus();}'
//                ]
            ]);
            ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

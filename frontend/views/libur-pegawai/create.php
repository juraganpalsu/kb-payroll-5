<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\LiburPegawai */

$this->title = Yii::t('app', 'Create Libur Pegawai');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Libur Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libur-pegawai-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

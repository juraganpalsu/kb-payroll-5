<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\SplDetail */

$this->title = Yii::t('app', 'Create Spl Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spl Detail'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spl-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

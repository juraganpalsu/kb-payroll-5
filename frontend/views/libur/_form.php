<?php

use kartik\datecontrol\DateControl;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Libur */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="libur-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="row">
        <div class="col-sm-3">
            <?=
            $form->field($model, 'jenis')->widget(Select2::className(), [
                'data' =>$model->_jenis_libur,
                'options' => ['placeholder' => Yii::t('app', '--Pilih Jenis')],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ])
            ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'tanggal')->widget(DateControl::classname(), [
                'widgetOptions' => [
                    'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                    ],
                ]
            ]); ?>
        </div>
    </div>
    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6, 'style' => 'width:50%']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <hr>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Reset'), Yii::$app->request->url, ['class' => 'btn btn-warning btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\LiburSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-libur-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'jenis')->textInput(['placeholder' => 'Jenis']) ?>

    <?= $form->field($model, 'tanggal')->widget(DateControl::classname(), [
        'widgetOptions' => [
            'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
            'pluginOptions' => [
                'todayHighlight' => true,
                'autoclose' => true,
            ],
        ]
    ]); ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Libur */

$this->title = Yii::t('app', 'Create Libur');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liburs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libur-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\GantiHari */
/* @var $providerGantiHariDetail \yii\data\ArrayDataProvider */

$this->title = $model->keterangan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ganti Hari'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ganti-hari-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Ganti Hari') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php
            $gridColumn = [
                'jumlah_jam',
                'keterangan:ntext',
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]);
            ?>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12">
            <?php
            if ($providerGantiHariDetail->totalCount) {
                $gridColumnGantiHariDetail = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'hidden' => true],
                    [
                        'attribute' => 'pegawai.nama',
                        'label' => Yii::t('app', 'Pegawai')
                    ],
                    ['attribute' => 'lock', 'hidden' => true],
                ];
                echo Gridview::widget([
                    'dataProvider' => $providerGantiHariDetail,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ganti-hari-detail']],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Ganti Hari Detail') . ' ' . $this->title),
                    ],
                    'columns' => $gridColumnGantiHariDetail
                ]);
            }
            ?>
        </div>
    </div>
</div>
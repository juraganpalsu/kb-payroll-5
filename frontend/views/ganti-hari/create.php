<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\GantiHari */

$this->title = Yii::t('app', 'Create Ganti Hari');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ganti Hari'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ganti-hari-create well text-primary">

    <h3><?= Html::encode($this->title) ?></h3>
    <hr>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

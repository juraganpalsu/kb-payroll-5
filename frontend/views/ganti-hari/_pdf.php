<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\GantiHari */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ganti Hari'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ganti-hari-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Ganti Hari').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'jumlah_jam',
        'keterangan:ntext',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\GantiHari */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Ganti Hari',
    ]) . ' ' . $model->keterangan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ganti Hari'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->keterangan, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ganti-hari-update well text-primary">

    <h3><?= Html::encode($this->title) ?></h3>
    <hr/>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model frontend\models\GantiHari */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="ganti-hari-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <div class="row">
        <div class="col-md-2">
            <?php
            $disabled = [];
            if (!$model->isNewRecord) {
                $disabled = ['disabled' => 'disabled'];
            }
            ?>
            <?= $form->field($model, 'jumlah_jam')->textInput(array_merge(['placeholder' => 'Jumlah Jam'], $disabled)) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'pegawai_ids')->widget(\kartik\select2\Select2::className(), [
                'options' => [
                    'placeholder' => Yii::t('app', '-- Pilih Pegawai'),
                    'multiple' => true,
                ],
                'data' => $model->pegawai_exist,
                'maintainOrder' => true,
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/spl/get-pegawai-for-select2']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) {
                            var template = $("#spl-spl_template_id").val();
                            if(template === ""){
                                alert("Isi SPL Template Terlebih Dahulu");
                            }
                            return {q:params.term, z:template}; 
                        }')
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <hr/>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

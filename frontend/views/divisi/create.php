<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Divisi */

$this->title = 'Create Divisi';
$this->params['breadcrumbs'][] = ['label' => 'Divisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="divisi-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

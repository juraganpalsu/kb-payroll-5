<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Divisi */

$this->title = 'Update Divisi: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Divisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="divisi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

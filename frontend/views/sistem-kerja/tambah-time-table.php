<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 31/10/2018
 * Time: 22:30
 */

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\SistemKerjaTimeTable */
/* @var $form yii\widgets\ActiveForm */


$js = <<< JS
$(function() {
    $('#form-time-table').on('beforeSubmit', function (event) {
        event.preventDefault();
        var form = $(this);
        var btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['tambah-time-table', 'skid' => $model->sistem_kerja_id]) : Url::to(['update-time-table', 'id' => $model->id]);

?>

<div class="tambah-time-table-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-time-table',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['form-time-table-validation', 'skid' => $model->sistem_kerja_id]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php
    try {
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (InvalidConfigException $e) {
    }
    ?>

    <div class="row">
        <div class="col-md-8">
            <?php
            $initialValue = $model->isNewRecord ? '' :         $initialValue = $model->timeTable->nama . ' - ' . $model->timeTable->masuk . ' - ' . $model->timeTable->pulang;
            echo $form->field($model, 'time_table_id')->widget(Select2::classname(), [
                'initValueText' => $initialValue,
                'options' => ['placeholder' => 'Search for a time table ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/time-table/list-select2']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ]
                ],
            ]);
            ?>
        </div>
    </div>

    <?php try {
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (InvalidConfigException $e) {
    } ?>


    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

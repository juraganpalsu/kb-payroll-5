<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 31/10/2018
 * Time: 22:30
 */

use common\components\Hari;
use common\models\SistemKerja;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PolaJadwalLibur */
/* @var $form yii\widgets\ActiveForm */


$js = <<< JS
$(function() {
    $('#form-pola-libur').on('beforeSubmit', function (event) {
        event.preventDefault();
        var form = $(this);
        var btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['buat-pola', 'skid' => $model->sistem_kerja_id]) : Url::to(['update-pola', 'id' => $model->id]);

?>

<div class="sistem-kerja-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-pola-libur',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['buat-pola-validation', 'skid' => $model->sistem_kerja_id]),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php
    try {
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (InvalidConfigException $e) {
    }
    ?>

    <?php if ($model->sistemKerja->jenis_libur == SistemKerja::POLA) { ?>
        <div class="row">
            <div class="col-sm-4">
                <?php try {
                    echo $form->field($model, 'masuk')->dropDownList(range(0, 6));
                } catch (InvalidConfigException $e) {
                } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?php try {
                    echo $form->field($model, 'libur')->dropDownList(range(0, 6));
                } catch (InvalidConfigException $e) {
                } ?>
            </div>
        </div>

    <?php } else { ?>

        <div class="row">
            <div class="col-md-8">
                <?php
                echo $form->field($model, 'hari')->widget(Select2::classname(), [
                    'value' => $model->hari,
                    'data' => Hari::_hari,
                    'options' => ['placeholder' => Yii::t('app', 'Pilih hari...'), 'multiple' => true,],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

    <?php } ?>

    <?php try {
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (InvalidConfigException $e) {
    } ?>


    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

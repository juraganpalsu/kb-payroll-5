<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 31/10/2018
 * Time: 21:55
 */


/**
 * @var SistemKerja $model
 * @var View $this
 */

use common\models\SistemKerja;
use common\models\SistemKerjaTimeTable;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\web\View;

try {
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'time_table_id',
            'value' => function (SistemKerjaTimeTable $model) {
                return $model->timeTable->nama . ' - ' . $model->timeTable->masuk . ' - ' . $model->timeTable->pulang;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, SistemKerjaTimeTable $model) {
                    return Html::a(Yii::t('frontend', 'Update'), ['update-time-table', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Update Pola Libur')]]);
                },
                'delete' => function ($url, SistemKerjaTimeTable $model) {
                    return Html::a('Delete', ['delete-time-table', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat btn-delete', 'data' => ['confirm' => Yii::t('frontend', 'Are ou sure you want to delete this item?')]]);
                }
            ],
        ],
    ];
    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->sistemKerjaTimeTables,
        'pagination' => [
            'pageSize' => -1
        ]
    ]);
    echo GridView::widget([
        'id' => 'sistem-kerja-time-table-grid',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'sistem-kerja-time-table-grid-pjax']],
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
        ],
        'beforeHeader' => [
            [
                'columns' => [
                    [
                        'content' => Yii::t('frontend', 'Time Table'),
                        'options' => [
                            'colspan' => 5,
                            'class' => 'text-center warning',
                        ]
                    ],
                ],
                'options' => ['class' => 'skip-export']
            ]
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
        'tableOptions' => ['class' => 'small']
    ]);
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
}
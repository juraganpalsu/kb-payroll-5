<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SistemKerjaSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

/**
 * @var string $click
 */

use common\models\SistemKerja;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Sistem Kerja');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function ($) {
    $('$click').click();
    
    $('.btn-add-or-update-sistem-kerja').click(function(e){
        e.preventDefault();
        var idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $("#kv-pjax-container-sistem-kerja").on("pjax:success", function() {
        $('.btn-add-or-update-sistem-kerja').click(function(e){
            e.preventDefault();
            var idModal = $('#form-modal');
            idModal.find('.modal-title').text($(this).data('header'));
            idModal.modal('show')
               .find('#modelContent')
               .load($(this).attr('href'));
            return false;
        });
    });
});
JS;

$this->registerJs($js);

?>
<div class="sistem-kerja-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function () {
                return GridView::ROW_COLLAPSED;
            },
            'enableRowClick' => true,
            'detailUrl' => Url::to(['show-configuration']),
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        'nama',
        'hari_kerja_aktif',
        [
            'attribute' => 'hari_keenam_setengah',
            'value' => function (SistemKerja $model) {
                return $model->hariKeenamSetengahHari;
            },
            'filter' => $searchModel->_enamHariKerja,
        ],
        [
            'attribute' => 'jenis_libur',
            'value' => function (SistemKerja $model) {
                return $model->jenisLibur;
            },
            'filter' => $searchModel->_jenisLibur
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url) {
                    return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-primary btn-flat btn-add-or-update-sistem-kerja', 'data' => ['header' => Yii::t('frontend', 'Update Sistem Kerja')]]);
                },
                'delete' => function ($url) {
                    return Html::a('Delete', $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                }
            ],
        ],
    ];
    ?>
    <?php try {
        echo GridView::widget([
            'id' => 'sistem-kerja-grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'rowOptions' => function (SistemKerja $model) {
                return ['id' => 'kv-grid-sistem-kerja-row-' . $model->id];
            },
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-sistem-kerja']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat btn-add-or-update-sistem-kerja', 'data' => ['header' => Yii::t('frontend', 'Tambah Sistem Kerja')]]) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                '{export}',
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

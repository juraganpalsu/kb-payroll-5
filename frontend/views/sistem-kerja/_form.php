<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\SistemKerja */
/* @var $form yii\widgets\ActiveForm */


$js = <<< JS
$(function() {
    $('#form-sistem-kerja').on('beforeSubmit', function (event) {
        event.preventDefault();
        var form = $(this);
        var btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]);

?>

<div class="sistem-kerja-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-sistem-kerja',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try {
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (InvalidConfigException $e) {
    } ?>

    <div class="row">
        <div class="col-sm-5">
            <?php try {
                echo $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']);
            } catch (InvalidConfigException $e) {
            } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?php try {
                echo $form->field($model, 'hari_kerja_aktif', ['addon' => ['append' => ['content' => Yii::t('frontend', 'hari')]]])->textInput(['placeholder' => $model->getAttributeLabel('hari_kerja_aktif'), 'type' => 'number']);
            } catch (InvalidConfigException $e) {
            } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php try {
                echo $form->field($model, 'hari_keenam_setengah')->checkbox();
            } catch (InvalidConfigException $e) {
            } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'jenis_libur')->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                    'handleWidth' => 60,
                    'onText' => 'Pola',
                    'offText' => 'Hari'
                ]
            ])->label(Yii::t('frontend', 'Libur Berdasarkan')) ?>
        </div>
    </div>

    <?php try {
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (InvalidConfigException $e) {
    } ?>


    <hr>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

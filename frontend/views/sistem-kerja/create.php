<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SistemKerja */

$this->title = Yii::t('app', 'Create Sistem Kerja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sistem Kerja'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sistem-kerja-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

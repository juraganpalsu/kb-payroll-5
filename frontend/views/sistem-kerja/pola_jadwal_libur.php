<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 31/10/2018
 * Time: 21:55
 */

/**
 * @var SistemKerja $model
 * @var View $this
 */

use common\models\PolaJadwalLibur;
use common\models\SistemKerja;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\web\View;

try {
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'masuk',
            'visible' => ($model->jenis_libur == SistemKerja::POLA)
        ],
        [
            'attribute' => 'libur',
            'visible' => ($model->jenis_libur == SistemKerja::POLA)
        ],
        [
            'attribute' => 'hari_',
            'visible' => ($model->jenis_libur == SistemKerja::HARI)
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, PolaJadwalLibur $model) {
                    return Html::a(Yii::t('frontend', 'Update'), ['update-pola', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-flat buat-pola-btn call-modal-btn', 'data' => ['header' => Yii::t('frontend', 'Update Pola Libur')]]);
                },
                'delete' => function ($url, PolaJadwalLibur $model) {
                    return Html::a('Delete', ['delete-pola', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger btn-flat btn-delete', 'data' => ['confirm' => Yii::t('frontend', 'Are ou sure you want to delete this item?')]]);

                }
            ],
        ],
    ];
    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->polaJadwalLiburs,
        'pagination' => [
            'pageSize' => -1
        ]
    ]);
    echo GridView::widget([
        'id' => 'pola-jadwal-libur-grid',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'pola-jadwal-libur-grid-pjax']],
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
        ],
        'beforeHeader' => [
            [
                'columns' => [
                    [
                        'content' => Yii::t('frontend', 'Pola Jadwal Libur'),
                        'options' => [
                            'colspan' => 5,
                            'class' => 'text-center warning',
                        ]
                    ],
                ],
                'options' => ['class' => 'skip-export']
            ]
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
        'tableOptions' => ['class' => 'small']
    ]);
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
}
<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 30/10/2018
 * Time: 22:48
 */

use common\models\SistemKerja;
use yii\helpers\Html;
use yii\web\View;


/**
 *
 * @var View $this
 * @var SistemKerja $model
 *
 */


$js = <<<JS
$(function ($) {
    $('.call-modal-btn').click(function(e){
        e.preventDefault();
        var idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete').click(function(e) {
        e.preventDefault();
        var btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);
?>

<div class="row">
    <div class="col-sm-6">
        <p>
            <?php
            $show = true;
            if ($model->jenis_libur == SistemKerja::HARI && !empty($model->polaJadwalLiburs)) {
                $show = false;
            }
            if ($show) {
                echo Html::a(Yii::t('frontend', 'Buat Pola'), ['buat-pola', 'skid' => $model->id],
                    [
                        'class' => 'btn btn-xs btn-success buat-pola-btn btn-flat call-modal-btn',
                        'data' => ['header' => Yii::t('frontend', 'Buat Pola Libur')]
                    ]
                );
            }
            ?>
        </p>
        <?php echo $this->renderAjax('pola_jadwal_libur', ['model' => $model]) ?>
    </div>
    <div class="col-sm-6">
        <p>
            <?php
            echo Html::a(Yii::t('frontend', 'Tambahkan Time Table'), ['tambah-time-table', 'skid' => $model->id],
                [
                    'class' => 'btn btn-xs btn-success tambah-time-table-btn btn-flat call-modal-btn',
                    'data' => ['header' => Yii::t('frontend', 'Tambahkan Time Table')]
                ]
            );
            ?>
        </p>
        <?php echo $this->renderAjax('time_table', ['model' => $model]) ?>
    </div>
</div>

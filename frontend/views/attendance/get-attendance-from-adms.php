<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 11/19/17
 * Time: 8:13 PM
 */

/**
 * @var \frontend\models\Attendance $model
 */

use kartik\daterange\DateRangePicker;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$this->title = 'Get Attendance From Adms';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo Html::a(Yii::t('frontend', 'Get From Machine'), ['get-attendance-from-machine'], ['class' => 'btn btn-primary btn-flat']);
echo Html::a(Yii::t('frontend', 'Get From Adms'), ['get-attendance-from-adms'], ['class' => 'btn btn-primary btn-flat', 'disabled' => 'disabled']);
?>
<div class="get-attendance-from-machine">
    <hr/>
    <div class="alert alert-warning" role="alert">
        <?= Yii::t('frontend', '*)Hanya mendownload ulang data yg gagal disimpan karena belum terdaftar ID PEGAWAI nya, daftarkan ID PEGAWAI nya terlebih dahulu jika belum.') ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?php if (Yii::$app->getSession()->hasFlash('berhasil')):
                ?>
                <div class="alert alert-success" role="alert">
                    <?= Yii::$app->getSession()->getFlash('berhasil') ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-12">
            <?php if (Yii::$app->getSession()->hasFlash('gagal')):
                ?>
                <div class="alert alert-danger" role="alert">
                    <?= Yii::$app->getSession()->getFlash('gagal') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'get-attendance-from-adms',
    ]);
    ?>
    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'date', [
                'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                'options' => ['class' => 'drp-container form-group']
            ])->widget(DateRangePicker::class, [
                'convertFormat' => true,
                'useWithAddon' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'pegawai_ids')->widget(Select2::class, [
                'options' => [
                    'placeholder' => Yii::t('app', '-- Pilih Pegawai'),
                    'multiple' => true,
                ],
                'data' => [],
                'maintainOrder' => true,
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/pegawai/pegawai/get-pegawai-for-select2']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) {
                            return {q:params.term}; 
                        }')
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <hr/>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Get Data'), ['class' => 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

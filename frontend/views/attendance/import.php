<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 8/2/17
 * Time: 11:28 PM
 */

/**
 * @var $model \frontend\models\Attendance
 */

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Import Attendance';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-att">
    <hr>
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin([
                'id' => 'import-att-form',
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>

            <?php
            echo $form->field($model, 'file_att')->widget(FileInput::classname(), [
                'options' => ['multiple' => false],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'previewFileType' => 'image',
                    'uploadUrl' => Url::to(['/attendance/upload-file']),
                    'allowedFileExtensions' => ['txt', 'xls', 'xlsx', 'csv'],
                    'uploadExtraData' => [
                    ],
                ],
//                'pluginEvents' => [
//                    'fileuploaded' => 'function(event, data){ var win = window.open(data.response.data.url); win.focus();}'
//                ]
            ]);
            ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 11/19/17
 * Time: 8:13 PM
 */

use kartik\daterange\DateRangePicker;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Get Attendance From Machine';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo Html::a(Yii::t('frontend', 'Get From Machine'), ['get-attendance-from-machine'], ['class' => 'btn btn-primary btn-flat', 'disabled' => 'disabled']);
echo Html::a(Yii::t('frontend', 'Get From Adms'), ['get-attendance-from-adms'], ['class' => 'btn btn-primary btn-flat']);

?>
<div class="get-attendance-from-machine">
    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <?php if (Yii::$app->getSession()->hasFlash('berhasil')):
                ?>
                <div class="alert alert-success" role="alert">
                    <?= Yii::$app->getSession()->getFlash('berhasil') ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-12">
            <?php if (Yii::$app->getSession()->hasFlash('gagal')):
                ?>
                <div class="alert alert-danger" role="alert">
                    <?= Yii::$app->getSession()->getFlash('gagal') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>


    <?php $form = ActiveForm::begin([
        'id' => 'get-attendance-from-machine',
    ]);
    ?>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'date', [
                'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                'options' => ['class' => 'drp-container form-group']
            ])->widget(DateRangePicker::class, [
                'convertFormat' => true,
                'useWithAddon' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5">
            <?=
            $form->field($model, 'ip')->widget(yii\widgets\MaskedInput::className(), [
                'options' => ['class' => 'form-control'],
                'clientOptions' => [
                    'alias' => 'ip'
                ],
            ]);
            ?>
        </div>
    </div>

    <hr/>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Get Data'), ['class' => 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

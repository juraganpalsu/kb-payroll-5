<?php

use common\models\Attendance;
use common\models\AttendanceDevice;
use common\models\Divisi;
use common\models\Unit;
use frontend\models\Pegawai;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use function GuzzleHttp\Psr7\str;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\AttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Attendance');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="attendance-index">
    <p>
        <?php
        if (Helper::checkRoute('/attendance/import-attendance')) {
            echo Html::a(Yii::t('app', 'Impor Attendance'), ['import-attendance'], ['class' => 'btn btn-success btn-flat']);
        }
        ?>
        <?php
        if (Helper::checkRoute('/attendance/get-attendance-from-machine')) {
            echo Html::a(Yii::t('app', 'Get Attendance'), ['get-attendance-from-machine'], ['class' => 'btn btn-success btn-flat']);
        }
        ?>
        <?= Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button btn-flat']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'idfp',
            'value' => function ($model) {
                return str_pad($model->idfp, 8, '0', STR_PAD_LEFT);
            }
        ],
        [
            'attribute' => 'pegawai_id',
            'label' => Yii::t('app', 'Pegawai'),
            'value' => function (Attendance $model) {
                return $model->pegawai->nama;
            },
        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (Attendance $model) {
                $tanggal = date('Y-m-d', strtotime($model->check_time));
                if ($perjanjianKerja = $model->pegawai->getPerjanjianKerjaUntukTanggal($tanggal)) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            },
        ],
        [
            'attribute' => 'sistem_kerja',
            'value' => function (Attendance $model) {
                $tanggal = date('Y-m-d', strtotime($model->check_time));
                return $model->pegawai->getPegawaiSistemKerjaUntukTanggal($tanggal) ? $model->pegawai->getPegawaiSistemKerjaUntukTanggal($tanggal)->sistemKerja->nama : '';
            },
            'hidden' => true
        ],
        [
            'attribute' => 'tanggal',
            'label' => Yii::t('app', 'Tanggal'),
            'value' => function (Attendance $model) {
                return date('d-m-Y', strtotime($model->check_time));
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'name' => 'range_tanggal',
                'value' => '',
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => ['format' => 'd-m-Y',
                        'separator' => ' s/d ',
                    ],
                    'opens' => 'left'
                ]
            ],
        ],
        [
            'attribute' => 'jam',
            'value' => function (Attendance $model) {
                return date('H:i:s', strtotime($model->check_time));
            }
        ],
        [
            'attribute' => 'device_id',
            'value' => function (Attendance $model) {
                return $model->attendanceDevice ? $model->attendanceDevice->name : '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => ArrayHelper::map(AttendanceDevice::find()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('device_id'), 'id' => 'grid-attendance-search-device_id']
        ],
//        'check_time',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    ?>

    <?php try {
        $export = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10],
            'noExportColumns' => [1, 11], //id dan lock
            'dropdownOptions' => [
                'class' => 'btn btn-secondary'
            ],
            'filename' => 'attendance' . date('dmYHis')
        ]);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-attendance']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        $export . Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>


</div>

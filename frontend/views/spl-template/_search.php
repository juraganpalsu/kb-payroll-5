<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\SplTemplateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-spl-template-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'sistem_kerja')->checkbox() ?>

    <?= $form->field($model, 'kategori')->checkbox() ?>

    <?= $form->field($model, 'masuk_minimum')->widget(\kartik\widgets\TimePicker::className()); ?>

    <?php /* echo $form->field($model, 'masuk')->widget(\kartik\widgets\TimePicker::className()); */ ?>

    <?php /* echo $form->field($model, 'masuk_maksimum')->widget(\kartik\widgets\TimePicker::className()); */ ?>

    <?php /* echo $form->field($model, 'pulang_minimum')->widget(\kartik\widgets\TimePicker::className()); */ ?>

    <?php /* echo $form->field($model, 'pulang')->widget(\kartik\widgets\TimePicker::className()); */ ?>

    <?php /* echo $form->field($model, 'pulang_maksimum')->widget(\kartik\widgets\TimePicker::className()); */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

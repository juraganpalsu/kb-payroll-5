<?php

/**
 * Created by PhpStorm.
 * File Name: index-detail.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 11/09/19
 * Time: 21.52
 */


use common\models\SplTemplate;
use common\models\SplTemplateDetail;
use kartik\grid\GridView;
use yii\helpers\Html;


/**
 * @var SplTemplate $model
 */


$js = <<<JS
$(function ($) {
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.btn-delete').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    btn.button('reset');
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);

?>
<div class="row">
    <div class="col-sm-2">
        <?php
        echo Html::a(Yii::t('frontend', 'Tambahkan Template'), ['tambahkan-template', 'stid' => $model->id],
            [
                'class' => 'btn btn-xs btn-success btn-flat btn-call-modal',
                'data' => ['header' => Yii::t('frontend', 'Tambahkan SPL Template')]
            ]
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-10">
        <?php
        try {
            $gridColumn = [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => Yii::t('frontend', 'SPL Template'),
                    'attribute' => 'spl_template_detail_id',
                    'value' => function (SplTemplateDetail $model) {
                        return $model->splTemplateDetail->nama . ' - ' . $model->splTemplateDetail->masuk . ' - ' . $model->splTemplateDetail->pulang;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{remove-detail-template}',
                    'buttons' => [
                        'remove-detail-template' => function ($url) {
                            return Html::a('Delete', $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete', 'data' => ['confirm' => Yii::t('frontend', 'Are ou sure you want to delete this item?')]]);
                        }
                    ],
                ],
            ];
            echo GridView::widget([
                'id' => 'spl-template-detail-grid',
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'spl-template-detail-grid-pjax']],
                'export' => [
                    'label' => 'Page',
                    'fontAwesome' => true,
                ],
                'beforeHeader' => [
                    [
                        'columns' => [
                            [
                                'content' => Yii::t('frontend', 'SPL Template Detail'),
                                'options' => [
                                    'colspan' => 5,
                                    'class' => 'text-center warning',
                                ]
                            ],
                        ],
                        'options' => ['class' => 'skip-export']
                    ]
                ],
                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'hover' => true,
                'showPageSummary' => false,
                'persistResize' => false,
                'tableOptions' => ['class' => 'small']
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
</div>


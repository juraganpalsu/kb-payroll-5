<?php

use common\components\Status;
use common\models\SplTemplate;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\SplTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


/**
 * @var string $click
 */

$this->title = Yii::t('app', 'Spl Template');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function ($) {
    $('$click').click();
});
JS;

$this->registerJs($js);
?>
<div class="spl-template-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'nama',
        [
            'attribute' => 'kategori_',
            'label' => Yii::t('app', 'Kategori'),
            'width' => '10%',
            'value' => function ($model) {
                return $model->kategori_;
            },
            'filter' => $searchModel->_kategori,
        ],
        [
            'attribute' => 'shift_',
            'label' => Yii::t('app', 'Shift'),
            'width' => '10%',
            'value' => function ($model) {
                return $model->shift_;
            },
            'filter' => $searchModel->_shift,
        ],
        'istirahat',
        'masuk_minimum',
        'masuk',
        'toleransi_masuk',
        'masuk_maksimum',
        'pulang_minimum',
        'pulang',
        'pulang_maksimum',
        ['attribute' => 'lock', 'hidden' => true],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>

    <?php try {
        echo GridView::widget([
            'id' => 'spl-template-grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'rowOptions' => function (SplTemplate $model) {
                return ['id' => 'kv-grid-spl-template-row-' . $model->id];
            },
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl-template']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                '{export}',
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>
<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
<div id='modelContent' class="well"></div>
<?php
Modal::end();
?>

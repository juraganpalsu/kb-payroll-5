<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\SplTemplate */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spl Template'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spl-template-view">

    <div class="row">
        <div class="col-sm-9">

        </div>
        <div class="col-sm-3">

            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumn = [
//        ['attribute' => 'id', 'hidden' => true],
                'nama',
                'kategori_',
                'shift_',
                'masuk_minimum',
                'masuk',
                'toleransi_masuk',
                'masuk_maksimum',
                'istirahat',
                'pulang_minimum',
                'pulang',
//        ['attribute' => 'lock', 'hidden' => true],
                'pulang_maksimum',
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            /** @var \yii\data\ActiveDataProvider $providerSpl */
            if ($providerSpl->totalCount) {
                $gridColumnSpl = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'hidden' => true],
                    'tanggal',
                    [
                        'attribute' => 'splTemplate.nama',
                        'label' => Yii::t('app', 'Spl Template')
                    ],
                    'keterangan',
                    ['attribute' => 'lock', 'hidden' => true],
                ];
                try {
                    echo Gridview::widget([
                        'dataProvider' => $providerSpl,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl']],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Spl') . ' ' . $this->title),
                        ],
                        'columns' => $gridColumnSpl
                    ]);
                } catch (Exception $e) {
                }
            }
            ?>
        </div>
    </div>
</div>
<?php

use kartik\widgets\SwitchInput;
use kartik\widgets\TimePicker;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\SplTemplate */
/* @var $form kartik\widgets\ActiveForm */
/** @var $sistem_kerja array */

?>

<div class="spl-template-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>
            </div>
            <div class="col-md-4">
                <?php
//                echo $form->field($model, 'sebagai_header')->widget(SwitchInput::class, [
//                    'pluginOptions' => [
//                        'handleWidth' => 50,
//                        'onText' => Yii::t('frontend', 'Ya'),
//                        'offText' => Yii::t('frontend', 'Tidak')
//                    ]
//                ])
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'kategori')->dropDownList($model->_kategori) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'tunjangan_shift')->dropDownList($model->_shift) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'istirahat')->textInput(['maxlength' => true, 'placeholder' => 'istirahat']) ?>
                <?php //$form->field($model, 'jumlah_jam_lembur')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('jumlah_jam_lembur')]) ?>
            </div>
        </div>
        <div class=" row">
            <div class=" col-md-3">
                <?=
                $form->field($model, 'masuk_minimum')->widget(TimePicker::class, [
                    'pluginOptions' => [
                        'showSeconds' => true,
                        'showMeridian' => false,
                        'minuteStep' => 5,
                        'secondStep' => 30,
                        'defaultTime' => false,
                        'modalBackdrop' => true,
                    ]
                ]);
                ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'masuk')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'masuk_maksimum')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-2">
                <?= $form->field($model, 'toleransi_masuk', ['addon' => ['append' => ['content' => 'menit']]])->textInput(['type' => 'number']); ?>
            </div>
        </div>


        <div class=" row">
            <div class=" col-md-3">
                <?= $form->field($model, 'pulang_minimum')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'pulang')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'pulang_maksimum')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-2">
                <?= $form->field($model, 'toleransi_pulang', ['addon' => ['append' => ['content' => 'menit']]])->textInput(['type' => 'number']); ?>
            </div>
        </div>

        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-danger btn-flat']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

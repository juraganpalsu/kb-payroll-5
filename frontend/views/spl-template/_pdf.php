<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\SplTemplate */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spl Template'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spl-template-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Spl Template').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'nama',
        'sistem_kerja',
        'kategori',
        'masuk_minimum',
        'masuk',
        'masuk_maksimum',
        'pulang_minimum',
        'pulang',
        'pulang_maksimum',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnSpl = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'tanggal',
        [
                'attribute' => 'splTemplate.id',
                'label' => Yii::t('app', 'Spl Template')
        ],
        'keterangan',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSpl,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-spl']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Spl').' '. $this->title),
        ],
        'columns' => $gridColumnSpl
    ]);
?>
    </div>
</div>

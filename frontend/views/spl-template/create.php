<?php


/* @var $this yii\web\View */
/* @var $model frontend\models\SplTemplate */

$this->title = Yii::t('app', 'Create Spl Template');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spl Template'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spl-template-create">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>

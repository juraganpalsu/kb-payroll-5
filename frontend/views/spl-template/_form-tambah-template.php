<?php

/**
 * Created by PhpStorm.
 * File Name: _form-tambah-template.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 11/09/19
 * Time: 22.09
 */


use common\models\SplTemplate;
use common\models\SplTemplateDetail;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;


/**
 * @var SplTemplateDetail $model
 */

$js = <<< JS
$(function() {
    $('#form-time-table').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

$action = Url::to(['tambahkan-template', 'stid' => $model->spl_template_id]);
?>

<div class="tambah-time-table-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-time-table',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['tambahkan-template-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?php echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-8">
                <?php
                $initialValue = $model->isNewRecord ? '' : $initialValue = $model->splTemplate->nama . ' - ' . $model->splTemplate->masuk . ' - ' . $model->splTemplate->pulang;
                echo $form->field($model, 'spl_template_detail_id')->widget(Select2::class, [
                    'initValueText' => $initialValue,
                    'options' => ['placeholder' => Yii::t('frontend', 'Search for a time table ...')],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 2,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/spl-template/list-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>

        <?php echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <?php echo $form->field($model, 'spl_template_id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Add') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat submit-btn' : 'btn btn-primary btn-flat submit-btn']) ?>
        </div>

        <?php

    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>
    <?php ActiveForm::end(); ?>

</div>


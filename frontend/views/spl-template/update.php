<?php

/* @var $this yii\web\View */
/* @var $model frontend\models\SplTemplate */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Spl Template',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spl Template'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="spl-template-update">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>

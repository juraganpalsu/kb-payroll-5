<?php

/**
 * @var string $content
 */

use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo Html::encode($this->title);
                } else {
                    echo Inflector::camel2words(
                        Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== Yii::$app->id) ? '<small>Module</small>' : '';
                }
                ?>
            </h1>
        <?php } ?>

        <?php
        try {
            echo Breadcrumbs::widget(
                [
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]
            );
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'excepion');
        }
        ?>
    </section>

    <section class="content">
        <?php
        try {
            echo Alert::widget();
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'excepion');
        }
        ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= date('Y') ?>.</strong> All rights reserved.
    <span style="color: white">built by jiwanndaru[at]gmail[dot]com</span>
</footer>

<div class='control-sidebar-bg'></div>
<?php

use common\models\Spl;
use dmstr\widgets\Menu;
use frontend\modules\erp\models\EPermintaan;
use frontend\modules\personalia\models\Appraisal;
use mdm\admin\components\Helper;

?>
<aside class="main-sidebar" style="position: fixed">

    <section class="sidebar">

        <?php
        try {
            echo Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        ['label' => Yii::t('frontend', 'HRIS Menu'), 'options' => ['class' => 'header']],
                        [
                            'label' => Yii::t('frontend', 'Setting'),
                            'icon' => 'wrench',
                            'url' => '#',
                            'visible' => Helper::checkRoute('/time-table/index') || Helper::checkRoute('/sistem-kerja/index') || Helper::checkRoute('/spl-template/index') || Helper::checkRoute('/unit/index') || Helper::checkRoute('/divisi/index') || Helper::checkRoute('/attendance-device/index'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'Jam Kerja'), 'icon' => 'circle-o', 'visible' => Helper::checkRoute('/time-table/index') || Helper::checkRoute('/sistem-kerja/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Time Table'), 'icon' => 'circle-o', 'url' => ['/time-table'], 'visible' => Helper::checkRoute('/time-table/index')],
                                        ['label' => Yii::t('frontend', 'Sistem Kerja'), 'icon' => 'circle-o', 'url' => ['/sistem-kerja'], 'visible' => Helper::checkRoute('/sistem-kerja/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'SPL'), 'icon' => 'circle-o', 'visible' => Helper::checkRoute('/attendance/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'SPL Template'), 'icon' => 'circle-o', 'url' => ['/spl-template'], 'visible' => Helper::checkRoute('/spl-template/index')],
                                        ['label' => Yii::t('frontend', 'SPL Alasan'), 'icon' => 'circle-o', 'url' => ['/spl/spl-alasan'], 'visible' => Helper::checkRoute('/spl/spl-alasan')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Payroll'), 'icon' => 'circle-o', 'visible' => Helper::checkRoute('/masterdata/p-tunjangan-anak-config/index') || Helper::checkRoute('/masterdata/p-tunjangan-loyalitas-config/index') || Helper::checkRoute('/masterdata/master-uang-shift/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Payroll Periode'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-periode/index'], 'visible' => Helper::checkRoute('/payroll/payroll-periode/index')],
                                        ['label' => Yii::t('frontend', 'Tunjangan Anak'), 'icon' => 'circle-o', 'url' => ['/masterdata/p-tunjangan-anak-config'], 'visible' => Helper::checkRoute('/masterdata/ptunjangan-anak-config/index')],
                                        ['label' => Yii::t('frontend', 'Tunjangan Loyalitas'), 'icon' => 'circle-o', 'url' => ['/masterdata/p-tunjangan-loyalitas-config'], 'visible' => Helper::checkRoute('/masterdata/ptunjangan-loyalitas-config/index')],
                                        ['label' => Yii::t('frontend', 'Uang Shift'), 'icon' => 'circle-o', 'url' => ['/masterdata/master-uang-shift'], 'visible' => Helper::checkRoute('/masterdata/master-uang-shift/index')],
                                        ['label' => Yii::t('frontend', 'THR'), 'icon' => 'circle-o', 'url' => ['/payroll/thr-setting'], 'visible' => Helper::checkRoute('/payroll/thr-setting/index')],
                                        ['label' => Yii::t('frontend', 'PTKP'), 'icon' => 'circle-o', 'url' => ['/payroll/ptkp-setting'], 'visible' => Helper::checkRoute('/payroll/ptkp-setting/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Struktur'), 'icon' => 'circle-o', 'url' => '#', 'visible' => Helper::checkRoute('/struktur/corporate/index') || Helper::checkRoute('/struktur/departemen/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Struktur'), 'icon' => 'align-left', 'url' => ['/struktur/struktur'], 'visible' => Helper::checkRoute('/struktur/struktur/index')],
                                        ['label' => Yii::t('frontend', 'Parameter'), 'icon' => 'cloud-upload',
                                            'items' => [
                                                ['label' => Yii::t('frontend', 'Corporate'), 'icon' => 'check', 'url' => ['/struktur/corporate'], 'visible' => Helper::checkRoute('/struktur/corporate/index')],
                                                ['label' => Yii::t('frontend', 'Perusahaan'), 'icon' => 'check', 'url' => ['/struktur/perusahaan'], 'visible' => Helper::checkRoute('/struktur/perusahaan/index')],
                                                ['label' => Yii::t('frontend', 'Direktur'), 'icon' => 'check', 'url' => ['/struktur/direktur'], 'visible' => Helper::checkRoute('/struktur/direktur/index')],
                                                ['label' => Yii::t('frontend', 'Divisi'), 'icon' => 'check', 'url' => ['/struktur/divisi'], 'visible' => Helper::checkRoute('/struktur/divisi/index')],
                                                ['label' => Yii::t('frontend', 'Departemen'), 'icon' => 'check', 'url' => ['/struktur/departemen'], 'visible' => Helper::checkRoute('/struktur/departemen/index')],
                                                ['label' => Yii::t('frontend', 'Section'), 'icon' => 'check', 'url' => ['/struktur/section'], 'visible' => Helper::checkRoute('/struktur/section/index')],
                                                ['label' => Yii::t('frontend', 'Jabatan'), 'icon' => 'check', 'url' => ['/struktur/jabatan'], 'visible' => Helper::checkRoute('/struktur/jabatan/index')],
                                                ['label' => Yii::t('frontend', 'Fungsi Jabatan'), 'icon' => 'check', 'url' => ['/struktur/fungsi-jabatan'], 'visible' => Helper::checkRoute('/struktur/fungsi-jabatan/index')],
                                            ]
                                        ],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Personalia'), 'icon' => 'circle-o', 'url' => '#', 'visible' => Helper::checkRoute('/personalia/ref-surat-peringatan/index') || Helper::checkRoute('/personalia/ref-peraturan-perusahaan/index') || Helper::checkRoute('/personalia/ref-appraisal/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Jenis Surat Peringatan'), 'icon' => 'circle-o', 'url' => ['/personalia/ref-surat-peringatan'], 'visible' => Helper::checkRoute('/personalia/ref-surat-peringatan/index')],
                                        ['label' => Yii::t('frontend', 'Peraturan Perusahaan'), 'icon' => 'circle-o', 'url' => ['/personalia/ref-peraturan-perusahaan'], 'visible' => Helper::checkRoute('/personalia/ref-peraturan-perusahaan/index')],
                                        ['label' => Yii::t('frontend', 'Ref Appraisal'), 'icon' => 'circle-o', 'url' => ['/personalia/ref-appraisal'], 'visible' => Helper::checkRoute('/personalia/ref-appraisal/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Training'), 'icon' => 'circle-o', 'url' => '#', 'visible' => Helper::checkRoute('/personalia/ref-nama-pelatihan/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Nama Pelatihan'), 'icon' => 'circle-o', 'url' => ['/personalia/ref-nama-pelatihan'], 'visible' => Helper::checkRoute('/personalia/ref-nama-pelatihan/index')],
                                        ['label' => Yii::t('frontend', 'Nama Institusi'), 'icon' => 'circle-o', 'url' => ['/personalia/ref-nama-institusi'], 'visible' => Helper::checkRoute('/personalia/ref-nama-institusi/index')],
                                        ['label' => Yii::t('frontend', 'Trainer'), 'icon' => 'circle-o', 'url' => ['/personalia/ref-trainer'], 'visible' => Helper::checkRoute('/personalia/ref-trainer/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Pegawai'), 'icon' => 'circle-o', 'url' => '#', 'visible' => Helper::checkRoute('/cost-center-template/index') || Helper::checkRoute('/pegawai/ref-area-kerja/index')
                                    || Helper::checkRoute('/libur/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Cost Center'), 'icon' => 'circle-o', 'url' => ['/cost-center-template'], 'visible' => Helper::checkRoute('/cost-center-template/index')],
                                        ['label' => Yii::t('frontend', 'Area Kerja'), 'icon' => 'circle-o', 'url' => ['/pegawai/ref-area-kerja'], 'visible' => Helper::checkRoute('/pegawai/ref-area-kerja/index')],
                                        ['label' => Yii::t('frontend', 'Libur Nasional'), 'icon' => 'circle-o', 'url' => ['/libur'], 'visible' => Helper::checkRoute('/libur/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Inventory'), 'icon' => 'circle-o', 'url' => '#', 'visible' => Helper::checkRoute('/erp/satuan/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Satuan'), 'icon' => 'circle-o', 'url' => ['/erp/satuan'], 'visible' => Helper::checkRoute('/erp/satuan/index')],
                                        ['label' => Yii::t('frontend', 'Barang'), 'icon' => 'circle-o', 'url' => ['/erp/barang'], 'visible' => Helper::checkRoute('/erp/barang/index')],
                                        ['label' => Yii::t('frontend', 'Gudang'), 'icon' => 'circle-o', 'url' => ['/erp/gudang'], 'visible' => Helper::checkRoute('/erp/gudang/index')],
                                        ['label' => Yii::t('frontend', 'Approver'), 'icon' => 'circle-o', 'url' => ['/erp/setting-approve'], 'visible' => Helper::checkRoute('/erp/setting-approve/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Attendance Device'), 'icon' => 'circle-o', 'url' => ['/attendance-device'], 'visible' => Helper::checkRoute('/attendance-device/index')],
                                ['label' => Yii::t('frontend', 'Cuti'), 'icon' => 'circle-o', 'url' => ['/izin/cuti-conf'], 'visible' => Helper::checkRoute('/izin/cuti-conf/index')],
                            ],
                        ],
                        [
                            'label' => Yii::t('frontend', 'Referensi'),
                            'icon' => 'navicon',
                            'url' => '#',
                            'visible' => Helper::checkRoute('/pegawai/pegawai/index') || Helper::checkRoute('/ijin/index') || Helper::checkRoute('/perubahan-status-absensi/index')
                                || Helper::checkRoute('/payroll/pinjaman/index-finance') || Helper::checkRoute('/izin/cuti/index') || Helper::checkRoute('/site/dashboard-control-data')
                                || Helper::checkRoute('/payroll/pinjaman/index-payroll') || Helper::checkRoute('/personalia/surat-pegawai/index')
                                || Helper::checkRoute('/payroll/payroll-bpjs/index') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-satu') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu')
                                || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-dua') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-tiga') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-empat')
                                || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-tiga-empat-gt') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu') || Helper::checkRoute('/payroll/payroll-bpjs/index-ada')
                                || Helper::checkRoute('/payroll/payroll-bpjs/index-bca'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'Dashboard Control Data'), 'icon' => 'circle-o', 'url' => ['/site/dashboard-control-data'], 'visible' => Helper::checkRoute('/site/dashboard-control-data')],
                                ['label' => Yii::t('frontend', 'Pegawai'), 'icon' => 'circle-o', 'url' => ['/pegawai/pegawai'], 'visible' => Helper::checkRoute('/pegawai/pegawai/index')],
                                ['label' => Yii::t('frontend', 'Personalia'), 'icon' => 'circle-o', 'url' => '#', 'visible' => Helper::checkRoute('/personalia/kecelakaan-kerja/index')
                                    || Helper::checkRoute('/personalia/kegiatan-hr/index') || Helper::checkRoute('/personalia/data-ncr/index') || Helper::checkRoute('/personalia/surat-pegawai/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Surat Pegawai'), 'icon' => 'circle-o', 'url' => ['/personalia/surat-pegawai'], 'visible' => Helper::checkRoute('/personalia/surat-pegawai/index')],
                                        ['label' => Yii::t('frontend', 'Surat Peringatan'), 'icon' => 'circle-o', 'url' => ['/personalia/surat-peringatan'], 'visible' => Helper::checkRoute('/personalia/surat-peringatan/index')],
                                        ['label' => Yii::t('frontend', 'Resign'), 'icon' => 'circle-o', 'url' => ['/personalia/resign'], 'visible' => Helper::checkRoute('/personalia/resign/index')],
                                        ['label' => Yii::t('frontend', 'Appraisal'), 'icon' => 'circle-o', 'url' => ['/personalia/appraisal'], 'visible' => Helper::checkRoute('/personalia/appraisal/index')],
                                        ['label' => Yii::t('frontend', 'MPP'), 'icon' => 'circle-o', 'url' => ['/personalia/mpp/index-aktual'], 'visible' => Helper::checkRoute('/personalia/mpp/index-aktual')],
                                        ['label' => Yii::t('frontend', 'Kecelakaan Kerja'), 'icon' => 'circle-o', 'url' => ['/personalia/kecelakaan-kerja'], 'visible' => Helper::checkRoute('/personalia/kecelakaan-kerja/index')],
                                        ['label' => Yii::t('frontend', 'Kegiatan HR'), 'icon' => 'circle-o', 'url' => ['/personalia/kegiatan-hr'], 'visible' => Helper::checkRoute('/personalia/kegiatan-hr/index')],
                                        ['label' => Yii::t('frontend', 'Data NCR'), 'icon' => 'circle-o', 'url' => ['/personalia/data-ncr'], 'visible' => Helper::checkRoute('/personalia/data-ncr/index')],
                                        ['label' => Yii::t('frontend', 'Peyelesaian Kasus'), 'icon' => 'circle-o', 'url' => ['/personalia/penyelesaian-kasus'], 'visible' => Helper::checkRoute('/personalia/penyelesaian-kasus/index')],

                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Training'), 'icon' => 'circle-o', 'url' => '#', 'visible' => Helper::checkRoute('/personalia/training/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Training'), 'icon' => 'circle-o', 'url' => ['/personalia/training'], 'visible' => Helper::checkRoute('/personalia/training/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Payroll'), 'icon' => 'circle-o', 'visible' => Helper::checkRoute('/payroll/insentif-tidak-tetap/index') || Helper::checkRoute('/payroll/pinjaman/index-finance') || Helper::checkRoute('/payroll/payroll-diluar-proses/index')
                                    || Helper::checkRoute('/payroll/pinjaman/index-payroll') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-satu') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-dua') || Helper::checkRoute('/payroll/payroll-bpjs/index')
                                    || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-tiga') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-empat') || Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-tiga-empat-gt') || Helper::checkRoute('/payroll/payroll-bpjs/index-ada'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Insentif Tidak Tetap'), 'icon' => 'circle-o', 'url' => ['/payroll/insentif-tidak-tetap/index'], 'visible' => Helper::checkRoute('/payroll/insentif-tidak-tetap/index')],
                                        ['label' => Yii::t('frontend', 'Rapel/Kekurangan Upah'), 'icon' => 'circle-o', 'url' => ['/payroll/rapel/index'], 'visible' => Helper::checkRoute('/payroll/rapel/index')],
                                        ['label' => Yii::t('frontend', 'Pinjaman Finance'), 'icon' => 'circle-o', 'url' => ['/payroll/pinjaman/index-finance'], 'visible' => Helper::checkRoute('/payroll/pinjaman/index-finance')],
                                        ['label' => Yii::t('frontend', 'Potongan Upah'), 'icon' => 'circle-o', 'url' => ['/payroll/pinjaman/index-payroll'], 'visible' => Helper::checkRoute('/payroll/pinjaman/index-payroll')],
                                        ['label' => Yii::t('frontend', 'Payroll Diluar Proses'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-diluar-proses'], 'visible' => Helper::checkRoute('/payroll/payroll-diluar-proses/index')],
                                        ['label' => Yii::t('frontend', 'Payroll Component'), 'icon' => 'circle-o', 'url' => ['/masterdata/default/index-component'], 'visible' => Helper::checkRoute('/masterdata/default/hr-manager')],
                                        ['label' => Yii::t('frontend', 'BPJS KBU'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bpjs/index-kbu'], 'visible' => Helper::checkRoute('/payroll/payroll-bpjs/index-kbu')],
                                        ['label' => Yii::t('frontend', 'BPJS ADA'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bpjs/index-ada'], 'visible' => Helper::checkRoute('/payroll/payroll-bpjs/index-ada')],
                                        ['label' => Yii::t('frontend', 'BPJS BCA'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bpjs/index-bca'], 'visible' => Helper::checkRoute('/payroll/payroll-bpjs/index-bca')],
                                        ['label' => Yii::t('frontend', 'BPJS (KBU-SATU)'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bpjs/index-kbu-satu'], 'visible' => Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-satu')],
                                        ['label' => Yii::t('frontend', 'BPJS (KBU-DUA)'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bpjs/index-kbu-dua'], 'visible' => Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-dua')],
                                        ['label' => Yii::t('frontend', 'BPJS (KBU-TIGA)'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bpjs/index-kbu-tiga'], 'visible' => Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-tiga')],
                                        ['label' => Yii::t('frontend', 'BPJS (KBU-EMPAT)'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bpjs/index-kbu-empat'], 'visible' => Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-empat')],
                                        ['label' => Yii::t('frontend', 'BPJS (TIGA EMPAT GT)'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bpjs/index-kbu-tiga-empat-gt'], 'visible' => Helper::checkRoute('/payroll/payroll-bpjs/index-kbu-tiga-empat-gt')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Izin'), 'icon' => 'circle-o', 'url' => '#', 'visible' => Helper::checkRoute('/izin/cuti/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Jenis Izin'), 'icon' => 'circle-o', 'url' => ['/izin/izin-jenis'], 'visible' => Helper::checkRoute('/izin/izin-jenis/index')],
                                        ['label' => Yii::t('frontend', 'Izin Tanpa Hadir'), 'icon' => 'circle-o', 'url' => ['/izin/izin-tanpa-hadir'], 'visible' => Helper::checkRoute('/izin/izin-tanpa-hadir/index')],
                                        ['label' => Yii::t('frontend', 'Izin Terlambat'), 'icon' => 'circle-o', 'url' => ['/izin/izin-terlambat'], 'visible' => Helper::checkRoute('/izin/izin-terlambat/index')],
                                        ['label' => Yii::t('frontend', 'Izin Keluar/Pulang'), 'icon' => 'circle-o', 'url' => ['/izin/izin-keluar'], 'visible' => Helper::checkRoute('/izin/izin-keluar/index')],
                                        ['label' => Yii::t('frontend', 'Absensi Tidak Lengkap'), 'icon' => 'circle-o', 'url' => ['/izin/absensi-tidak-lengkap'], 'visible' => Helper::checkRoute('/izin/absensi-tidak-lengkap/index')],
                                        ['label' => Yii::t('frontend', 'Setengah Hari'), 'icon' => 'circle-o', 'url' => ['/izin/setengah-hari'], 'visible' => Helper::checkRoute('/izin/setengah-hari/index')],
                                        ['label' => Yii::t('frontend', 'Cuti'), 'icon' => 'circle-o', 'url' => ['/izin/cuti'], 'visible' => Helper::checkRoute('/izin/cuti/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Perubahan Absensi'), 'icon' => 'circle-o', 'url' => ['/perubahan-status-absensi'], 'visible' => Helper::checkRoute('/perubahan-status-absensi/index')],
                            ],
                        ],
                        [
                            'label' => Yii::t('frontend', 'Proses'),
                            'icon' => 'sun-o',
                            'url' => '#',
                            'visible' => Helper::checkRoute('/absensi/index') || Helper::checkRoute('/attendance/index') || Helper::checkRoute('/spl/index') || Helper::checkRoute('/libur-pegawai/index') || Helper::checkRoute('/spl/spl/index-lembur-insentif'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'Absensi'), 'icon' => 'group', 'url' => ['/absensi'], 'visible' => Helper::checkRoute('/absensi/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Data Absensi'), 'icon' => 'hand-pointer-o', 'url' => ['/absensi'], 'visible' => Helper::checkRoute('/absensi/index')],
                                        ['label' => Yii::t('frontend', 'Laporan Absensi'), 'icon' => 'file-o', 'url' => ['/absensi/laporan-form'], 'visible' => Helper::checkRoute('/absensi/laporan-form')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Attendance'), 'icon' => 'user', 'visible' => Helper::checkRoute('/attendance/index'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Finger'), 'icon' => 'hand-pointer-o', 'url' => ['/attendance'], 'visible' => Helper::checkRoute('/attendance/index')],
                                        ['label' => Yii::t('frontend', 'Beatroute'), 'icon' => 'mobile-phone', 'url' => ['/attendance/index-beatroute'], 'visible' => Helper::checkRoute('/attendance/index')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'SPL'), 'icon' => 'clock-o', 'visible' => Helper::checkRoute('/spl/spl/index') || Helper::checkRoute('/spl/spl/index-lembur-insentif'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Data SPL'), 'icon' => 'hand-pointer-o', 'url' => ['/spl/spl/index'], 'visible' => Helper::checkRoute('/spl/spl/index')],
                                        ['label' => Yii::t('frontend', 'Data Insentif'), 'icon' => 'hand-pointer-o', 'url' => ['/spl/spl/index-lembur-insentif'], 'visible' => Helper::checkRoute('/spl/spl/index-lembur-insentif')],
                                        ['label' => Yii::t('frontend', 'Semua Data'), 'icon' => 'mobile-phone', 'url' => ['/spl/spl/index-hr'], 'visible' => Helper::checkRoute('/spl/spl/index-hr')],
                                        ['label' => Yii::t('frontend', 'Accounting'), 'icon' => 'mobile-phone', 'url' => ['/spl/spl/index-acc'], 'visible' => Helper::checkRoute('/spl/spl/index-acc')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Libur Pegawai'), 'icon' => 'circle-o', 'url' => ['/libur-pegawai'], 'visible' => Helper::checkRoute('/libur-pegawai/index')],

                                ['label' => Yii::t('frontend', 'Laporan'), 'icon' => 'file-o', 'url' => ['/personalia/default/karyawan-masuk-form'], 'visible' => Helper::checkRoute('/personalia/default/karyawan-masuk'),
                                ],

                            ],
                        ],
                        ['label' => Yii::t('frontend', 'Payroll'), 'icon' => 'money', 'visible' => Helper::checkRoute('/payroll/gaji-borongan/index') || Helper::checkRoute('/payroll/proses-gaji/index-bca')
                            || Helper::checkRoute('/payroll/proses-gaji/index-kbu-nol') || Helper::checkRoute('/payroll/proses-gaji/index-kbu-satu')
                            || Helper::checkRoute('/payroll/proses-gaji/index-kbu-dua') || Helper::checkRoute('/payroll/proses-gaji/index-kbu-tiga') || Helper::checkRoute('/payroll/proses-gaji/index-kbu-tiga-empat-gt') || Helper::checkRoute('/payroll/proses-gaji/index-kbu-empat')
                            || Helper::checkRoute('/payroll/proses-gaji/index-kbu-lima') || Helper::checkRoute('/payroll/proses-gaji/index-bca-satu') || Helper::checkRoute('/payroll/proses-gaji/index-bca-dua')
                            || Helper::checkRoute('/payroll/proses-gaji/index-bca-tiga') || Helper::checkRoute('/payroll/proses-gaji/index-bca-ada') || Helper::checkRoute('/payroll/gaji-pph21/index'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'Borongan'), 'icon' => 'circle-o', 'url' => ['/payroll/gaji-borongan'], 'visible' => Helper::checkRoute('/payroll/gaji-borongan/index')],
                                ['label' => Yii::t('frontend', 'THR'), 'icon' => 'circle-o', 'visible' => Helper::checkRoute('/payroll/thr/index') || Helper::checkRoute('/payroll/thr/index-bca')
                                    || Helper::checkRoute('/payroll/thr/index-kbu-nol') || Helper::checkRoute('/payroll/thr/index-kbu-satu')
                                    || Helper::checkRoute('/payroll/thr/index-kbu-dua') || Helper::checkRoute('/payroll/thr/index-kbu-tiga') || Helper::checkRoute('/payroll/thr/index-kbu-tiga-empat-gt') || Helper::checkRoute('/payroll/thr/index-kbu-empat')
                                    || Helper::checkRoute('/payroll/thr/index-kbu-lima') || Helper::checkRoute('/payroll/thr/index-bca-satu') || Helper::checkRoute('/payroll/thr/index-bca-dua')
                                    || Helper::checkRoute('/payroll/thr/index-bca-tiga') || Helper::checkRoute('/payroll/thr/index-bca-ada'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'KBU/ADA'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index'], 'visible' => Helper::checkRoute('/payroll/thr/index')],
                                        ['label' => Yii::t('frontend', 'KBU-SATU'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-kbu-satu'], 'visible' => Helper::checkRoute('/payroll/thr/index-kbu-satu')],
                                        ['label' => Yii::t('frontend', 'KBU-DUA'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-kbu-dua'], 'visible' => Helper::checkRoute('/payroll/thr/index-kbu-dua')],
                                        ['label' => Yii::t('frontend', 'KBU-TIGA'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-kbu-tiga'], 'visible' => Helper::checkRoute('/payroll/thr/index-kbu-tiga')],
                                        ['label' => Yii::t('frontend', 'KBU-EMPAT'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-kbu-empat'], 'visible' => Helper::checkRoute('/payroll/thr/index-kbu-empat')],
                                        ['label' => Yii::t('frontend', 'KBU-LIMA-UP'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-kbu-lima'], 'visible' => Helper::checkRoute('/payroll/thr/index-kbu-lima')],
                                        ['label' => Yii::t('frontend', 'KBU-TIGA-EMPAT'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-kbu-tiga-empat-gt'], 'visible' => Helper::checkRoute('/payroll/thr/index-kbu-tiga-empat-gt')],
                                        ['label' => Yii::t('frontend', 'BCA'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-bca'], 'visible' => Helper::checkRoute('/payroll/thr/index-bca')],
                                        ['label' => Yii::t('frontend', 'BCA-SATU'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-bca-satu'], 'visible' => Helper::checkRoute('/payroll/thr/index-bca-satu')],
                                        ['label' => Yii::t('frontend', 'BCA-DUA'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-bca-dua'], 'visible' => Helper::checkRoute('/payroll/thr/index-bca-dua')],
                                        ['label' => Yii::t('frontend', 'BCA-TIGA'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-bca-tiga'], 'visible' => Helper::checkRoute('/payroll/thr/index-bca-tiga')],
                                        ['label' => Yii::t('frontend', 'BCA-ADA'), 'icon' => 'circle-o', 'url' => ['/payroll/thr/index-bca-ada'], 'visible' => Helper::checkRoute('/payroll/thr/index-bca-ada')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Bonus'), 'icon' => 'circle-o', 'url' => ['/payroll/payroll-bonus'], 'visible' => Helper::checkRoute('/payroll/bonus/index')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-KBU'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-KBU-NOL'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-kbu-nol'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-kbu-nol')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-KBU-SATU'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-kbu-satu'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-kbu-satu')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-KBU-DUA'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-kbu-dua'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-kbu-dua')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-KBU-TIGA'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-kbu-tiga'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-kbu-tiga')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-KBU-EMPAT'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-kbu-empat'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-kbu-empat')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-KBU-LIMA-UP'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-kbu-lima'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-kbu-lima')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-KBU-TIGA-EMPAT'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-kbu-tiga-empat-gt'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-kbu-tiga-empat-gt')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-BCA'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-bca'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-bca')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-BCA-SATU'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-bca-satu'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-bca-satu')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-BCA-DUA'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-bca-dua'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-bca-dua')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-BCA-TIGA'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-bca-tiga'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-bca-tiga')],
                                ['label' => Yii::t('frontend', 'Proses Gaji-BCA-ADA'), 'icon' => 'circle-o', 'url' => ['/payroll/proses-gaji/index-bca-ada'], 'visible' => Helper::checkRoute('/payroll/proses-gaji/index-bca-ada')],
                                ['label' => Yii::t('frontend', 'Gaji PPH21'), 'icon' => 'circle-o', 'url' => ['/payroll/gaji-pph21/index'], 'visible' => Helper::checkRoute('/payroll/gaji-pph21/index')],
                            ]
                        ],
                        ['label' => Yii::t('frontend', 'Inventory'), 'icon' => 'tv', 'visible' => Helper::checkRoute('/erp/barang-masuk/index'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'Stock'), 'icon' => 'database', 'url' => ['/erp/stok'], 'visible' => Helper::checkRoute('/erp/stok/index')],
                                ['label' => Yii::t('frontend', 'Stock In'), 'icon' => 'toggle-right', 'url' => ['/erp/barang-masuk'], 'visible' => Helper::checkRoute('/erp/barang-masuk/index')],
                                ['label' => Yii::t('frontend', 'Stock Out'), 'icon' => 'toggle-left', 'url' => ['/erp/barang-keluar'], 'visible' => Helper::checkRoute('/erp/barang-keluar/index')],
                                ['label' => Yii::t('frontend', 'Permintaan'), 'icon' => 'pencil-square-o', 'url' => ['/erp/permintaan'], 'visible' => Helper::checkRoute('/erp/permintaan/index')],
                                ['label' => Yii::t('frontend', 'Data P2B'), 'icon' => 'pencil-square-o', 'url' => ['/erp/stok/index-pp'], 'visible' => Helper::checkRoute('/erp/stok/index-pp')],
                            ]
                        ],
                        ['label' => Yii::t('frontend', 'WSC'), 'icon' => 'tv', 'visible' => Helper::checkRoute('/wsc/wsc/index'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'My Index'), 'icon' => 'calendar-o', 'url' => ['/wsc/wsc'], 'visible' => Helper::checkRoute('/wsc/wsc/index')],
                                ['label' => Yii::t('frontend', 'Index Tagging Me'), 'icon' => 'calendar-o', 'url' => ['/wsc/wsc/index-tagging-me'], 'visible' => Helper::checkRoute('/wsc/wsc/index-tagging-me')],
                                ['label' => Yii::t('frontend', 'Index My Team'), 'icon' => 'users', 'url' => ['/wsc/wsc/index-my-team'], 'visible' => Helper::checkRoute('/wsc/wsc/index-my-team')],
                                ['label' => Yii::t('frontend', 'Index All'), 'icon' => 'users', 'url' => ['/wsc/wsc/index-all'], 'visible' => Helper::checkRoute('/wsc/wsc/index-all')],
                            ]
                        ],
                        [
                            'label' => Yii::t('frontend', 'Approval'),
                            'icon' => 'check-square-o',
                            'url' => '#',
                            'visible' => Helper::checkRoute('/spl/approval'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'SPL'), 'icon' => 'clock-o', 'url' => ['/spl/spl/approval'],
                                    'template' => '<a href="{url}">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-yellow">' . Spl::jumlahSpl() . '</small></span></a>',
                                    'visible' => true],
                                ['label' => Yii::t('frontend', 'Permintaan Barang'), 'icon' => 'clock-o', 'url' => ['/erp/permintaan/approval'],
                                    'template' => '<a href="{url}">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-yellow">' . EPermintaan::countApprovalStatic() . '</small></span></a>',
                                    'visible' => true],
                            ],
                        ],
                        [
                            'label' => Yii::t('frontend', 'ESS'),
                            'icon' => 'check-square-o',
                            'url' => '#',
                            'visible' => Helper::checkRoute('/attendance/index-ess') || Helper::checkRoute('/absensi/index-ess') || Helper::checkRoute('/spl/spl/index-ess') || Helper::checkRoute('/erp/permintaan/index'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'Attendance'), 'icon' => 'user', 'url' => ['/attendance/index-ess'], 'visible' => Helper::checkRoute('/attendance/index-ess'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'My Attendance'), 'icon' => 'hand-pointer-o', 'url' => ['/attendance/index-ess'], 'visible' => Helper::checkRoute('/attendance/index-ess')],
                                        ['label' => Yii::t('frontend', 'Attendance Team'), 'icon' => 'file-o', 'url' => ['/attendance/index-team'], 'visible' => Helper::checkRoute('/attendance/index-team')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'Absensi'), 'icon' => 'user', 'url' => ['/absensi/index-ess'], 'visible' => Helper::checkRoute('/absensi/index-ess'),
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'My Absensi'), 'icon' => 'hand-pointer-o', 'url' => ['/absensi/index-ess'], 'visible' => Helper::checkRoute('/absensi/index-ess')],
                                        ['label' => Yii::t('frontend', 'Absensi Team'), 'icon' => 'file-o', 'url' => ['/absensi/index-absen-team'], 'visible' => Helper::checkRoute('/absensi/index-absen-team')],
                                    ]
                                ],
                                ['label' => Yii::t('frontend', 'My Payroll'), 'icon' => 'user', 'url' => ['/payroll/default/my-payroll'], 'visible' => Helper::checkRoute('/payroll/default/my-payroll')],
                                ['label' => Yii::t('frontend', 'SPKL'), 'icon' => 'user', 'url' => ['/spl/spl/index-ess'], 'visible' => Helper::checkRoute('/spl/spl/index-ess')],
                                ['label' => Yii::t('frontend', 'Permintaan Barang'), 'icon' => 'pencil-square-o', 'url' => ['/erp/permintaan'], 'visible' => Helper::checkRoute('/erp/permintaan/index')],
                                ['label' => Yii::t('frontend', 'Appraisal'), 'icon' => 'circle-o', 'url' => ['/personalia/appraisal/index-penilaian'],
                                    'template' => '<a href="{url}">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-yellow">' . Appraisal::jumlahAppraisal() . '</small></span></a>',
                                    'visible' => true],
                            ],
                        ],
                        [
                            'label' => Yii::t('frontend', 'BCA'),
                            'icon' => 'check-square-o',
                            'url' => '#',
                            'visible' => Helper::checkRoute('/pegawai/pegawai/bca-index') || Helper::checkRoute('/personalia/resign/bca-index'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'Pegawai'), 'icon' => 'user', 'url' => ['/pegawai/pegawai/bca-index'], 'visible' => Helper::checkRoute('/pegawai/pegawai/bca-index')],
                                ['label' => Yii::t('frontend', 'Surat Peringatan'), 'icon' => 'user', 'url' => ['/personalia/surat-peringatan/bca-index'], 'visible' => Helper::checkRoute('/personalia/surat-peringatan/bca-index')],
                                ['label' => Yii::t('frontend', 'Resign'), 'icon' => 'user', 'url' => ['/personalia/resign/bca-index'], 'visible' => Helper::checkRoute('/personalia/resign/bca-index')],
                            ],
                        ],
                        ['label' => Yii::t('frontend', 'Administrator'), 'icon' => 'cogs', 'visible' => Helper::checkRoute('/user/admin/index'),
                            'items' => [
                                ['label' => Yii::t('frontend', 'User Management'), 'icon' => 'users',
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'All User'), 'icon' => 'user', 'url' => ['/user/admin/index']],
                                        ['label' => Yii::t('frontend', 'Create User'), 'icon' => 'user-plus', 'url' => ['/user/admin/index']],
                                    ],
                                ],
                                ['label' => Yii::t('frontend', 'RBAC'), 'icon' => 'check-circle',
                                    'items' => [
                                        ['label' => Yii::t('frontend', 'Assigment'), 'icon' => 'american-sign-language-interpreting', 'url' => ['/admin/assignment']],
                                        ['label' => Yii::t('frontend', 'Role'), 'icon' => 'grav', 'url' => ['/admin/role']],
                                        ['label' => Yii::t('frontend', 'Permission'), 'icon' => 'blind', 'url' => ['/admin/permission']],
                                        ['label' => Yii::t('frontend', 'Route'), 'icon' => 'compress', 'url' => ['/admin/route']],
                                    ],
                                ],
                            ]
                        ],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>

    </section>

</aside>

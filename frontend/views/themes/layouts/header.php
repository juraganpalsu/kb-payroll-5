<?php

use common\models\Spl;
use frontend\models\User;
use frontend\modules\erp\models\EPermintaan;
use frontend\modules\personalia\models\Appraisal;
use frontend\modules\wsc\models\search\WscSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $content string */

/** @var User $user */
$user = Yii::$app->user->identity;
if (!$user) {
    return Yii::$app->controller->redirect(['/']);
}
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo', 'style' => ['position' => 'fixed']]) ?>

    <nav class="navbar navbar-fixed-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <?php
                $spl = Spl::jumlahSpl();
                $permintaanBarang = EPermintaan::countApprovalStatic();
                $appraisal = Appraisal::jumlahAppraisal();
                $wscMyActiveActivity = WscSearch::countMyActiveActivity();
                $wscTaggingMe = WscSearch::countTaggingMe();
                $jumlah = $spl + $appraisal + $wscMyActiveActivity + $wscTaggingMe;
                ?>
                <!-- Notifications Menu -->
                <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning"><?= $jumlah; ?>
                        </span>
                    </a>
                    <?php
                    if ($jumlah) {
                        ?>
                        <ul class="dropdown-menu">
                            <li class="header">
                                You have <?= $jumlah; ?> notifications
                            </li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <?php
                                    if ($spl) { ?>
                                        <li>
                                            <a href="<?= Url::to('/spl/spl/approval') ?>">
                                                <i class="fa fa-clock-o text-red"></i> <?= $spl ?> SPKL Belum
                                                Approve
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php
                                    if ($permintaanBarang) { ?>
                                        <li>
                                            <a href="<?= Url::to('/spl/spl/approval') ?>">
                                                <i class="fa fa-clock-o text-red"></i> <?= $permintaanBarang ?> Permintaan Barang Belum
                                                Approve
                                            </a>
                                        </li>
                                    <?php } ?>
<!--                                    --><?php
//                                    if ($appraisal) { ?>
<!--                                        <li>-->
<!--                                            <a href="--><?//= Url::to('/personalia/appraisal/index-penilaian') ?><!--">-->
<!--                                                <i class="fa fa-users text-green"></i> --><?//= $appraisal ?>
<!--                                                Appraisal Belum Dinilai-->
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                    --><?php //} ?>
                                    <?php
                                    if ($wscMyActiveActivity) { ?>
                                        <li>
                                            <a href="<?= Url::to('/wsc/wsc') ?>">
                                                <i class="fa fa-book text-green"></i>
                                                <?= Yii::t('frontend', 'Anda memiliki {activity} data WSC', ['activity' => $wscMyActiveActivity]) ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php
                                    if ($wscTaggingMe) { ?>
                                        <li>
                                            <a href="<?= Url::to('/wsc/wsc/index-tagging-me') ?>">
                                                <i class="fa fa-book text-green"></i>
                                                <?= Yii::t('frontend', 'Ada {tag} WSC yg menandai anda', ['tag' => $wscTaggingMe]) ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        </ul>
                    <?php } ?>
                </li>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?= Html::tag('span', '', ['class' => 'user-image', 'data-letters' => $user->initialName]); ?>
                        <span class="hidden-xs"><?= $user->username; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!--User image-->
                        <li class="user-header">
                            <?php // Html::img('/images/person2.png', ['class' => 'img-circle', 'alt' => Yii::t('frontend', 'User Image')]); ?>
                            <!--<p data-letters="MN"> My Name</p>-->
                            <?= Html::tag('span', '', ['class' => 'img-circle', 'data-letters-big' => $user->initialName]); ?>
                            <p>
                                <?= $user->username; ?>
                                <small><?= Yii::t('frontend', 'Last Login At : ') . date('d-m-Y', $user->last_login_at); ?></small>
                            </p>
                        </li>
                        <!--Menu Body-->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <?= Html::a(Yii::t('frontend', 'Account'), ['/user/settings/account']) ?>
                            </div>
                            <div class="col-xs-4 text-center">
                                <?php // Html::a(Yii::t('frontend', 'Profile'), ['/user/settings/profile']) ?>
                            </div>
                            <div class="col-xs-4 text-center">
                                <?= Html::a(Yii::t('frontend', 'Network'), ['/user/settings/networks']) ?>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?php
                                if ($user->pegawai) {
                                    echo Html::a(Yii::t('frontend', 'Profile'), ['/pegawai/pegawai/view', 'id' => $user->pegawai->id], ['class' => 'btn btn-default btn-flat']);
                                } else {
                                    echo Html::a(Yii::t('frontend', 'Profile'), '#', ['class' => 'btn btn-danger']);
                                }
                                ?>
                            </div>
                            <div class="pull-right">
                                <?=
                                Html::a(
                                    'Sign out', ['/user/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                )
                                ?>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
<div style="height: 50px"></div>
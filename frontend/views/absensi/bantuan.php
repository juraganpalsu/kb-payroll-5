<?php
/**
 * Created by PhpStorm.
 * File Name: bantuan.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 27/02/2021
 * Time: 22:05
 */

/* @var $modelPegawai \frontend\modules\pegawai\models\Pegawai */

$this->title = Yii::t('app', 'Bantuan');
$this->params['breadcrumbs'][] = $this->title;

?>

<div>
    <div class="box box-solid col-md-6">
        <div class="box-header with-border">
            <i class="fa fa-info"></i>

            <h3 class="box-title">Deskripsi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <dl>
                <dt>Hallo, <?= $modelPegawai->nama_panggilan ?></dt>
                <dd>Kami akan menjelaskan terkait aturan yang ada pada absensi</dd>
                <dt>Untuk pengecekan dilakukan H+1</dt>
                <dt>Penyebab absensi, <?= $modelPegawai->nama_panggilan ?> Undifined kemungkinan karena sbb :</dt>
                <dd>1. Absensi tidak lengkap hanya ada salah satu attendance masuk/pulang</dd>
                <dd>2. SPKL Belum di approve / salah pembuatan SPKL</dd>
                <dd>3. Jam kerja tidak sesuai dengan sistem kerja</dd>
                <dd>dll</dd>
                <dt>Penyebab absensi, <?= $modelPegawai->nama_panggilan ?> Alfa kemungkinan karena sbb :</dt>
                <dd>1. Anda benar tidak masuk</dd>
                <dd>2. Jika anda merasa izin, kemungkinan surat izin belum diinput bisa menghubungi PIC Terkait sesuai
                    Gol dan Bisnis Unit
                </dd>
                <dd>3. Jika anda hanya absensi menggunakan Beatroute kemungkinan Beatroute belum diupload oleh tim
                    payroll, upload Beatroute dilakukan perminggu atau
                    H+7 setelah Anda absen
                </dd>
                <dd>dll</dd>
            </dl>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered">
                    <tr class="success">
                        <th scope="col">Bisnis Unit</th>
                        <th scope="col">Golongan</th>
                        <th scope="col">PIC Pengecekan</th>
                        <th scope="col">Kontak W.A PIC</th>
                    </tr>
                    <tr>
                        <th scope="row">KBU</th>
                        <td>1</td>
                        <td>Sipa/Lula</td>
                        <td rowspan="5">
                            1.Sipa = 0812-1887-7019<br>
                            2.Lula = 0857-2208-7490<br>
                            3.Alka = 0853-1108-6692<br>
                            4.Dias = 0858-9282-0041<br>
                            5. Resi = 0858-1063-0401
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">KBU/ADA</th>
                        <td>2-3,5</td>
                        <td>Lula</td>
                    </tr>
                    <tr>
                        <th scope="row">KBU</th>
                        <td>4,6,7</td>
                        <td>Sipa</td>
                    </tr>
                    <tr>
                        <th scope="row">BCA KBU</th>
                        <td>1-3</td>
                        <td>Resi/ Alka/ Dias</td>
                    </tr>
                    <tr>
                        <th scope="row">BCA ADA</th>
                        <td>2-3</td>
                        <td>Alka/ Dias</td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

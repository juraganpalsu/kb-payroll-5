<?php


/* @var $this yii\web\View */
/* @var $model frontend\models\Absensi */

$this->title = Yii::t('app', 'Proses Absensi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Absensi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.alert {
    overflow-y: scroll;
    max-height: 400px;
}
CSS;

$this->registerCss($css);

?>
<div class="absensi-proses">

    <?php if (Yii::$app->session->hasFlash('info-proses')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i><?= Yii::t('frontend', 'Info Proses!'); ?></h4>
            <?= Yii::$app->session->getFlash('info-proses') ?>
        </div>
    <?php endif; ?>

    <?= $this->render('_formProses', [
        'model' => $model,
    ]) ?>

</div>

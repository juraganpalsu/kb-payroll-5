<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Absensi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Absensi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absensi-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Absensi').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'masuk',
        'pulang',
        'tanggal',
        [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('app', 'Pegawai')
        ],
        'time_table',
        'status_kehadiran',
        'jumlah_jam_lembur',
        ['attribute' => 'lock', 'hidden' => true],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>

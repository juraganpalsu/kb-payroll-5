<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 30/12/2018
 * Time: 16:11
 */

/**
 * @var Absensi $model
 */

use frontend\models\Absensi;
use frontend\models\Attendance;
use kartik\grid\GridView;

$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    ['attribute' => 'id', 'hidden' => true],
    [
        'attribute' => 'idfp',
        'label' => Yii::t('app', 'Id Pegawai'),
        'value' => function (Attendance $model) {
            return str_pad($model->idfp, 8, '0', STR_PAD_LEFT);
        },
        'format' => 'html'
    ],
    [
        'attribute' => 'pegawai_id',
        'label' => Yii::t('app', 'Pegawai'),
        'value' => function (Attendance $model) {
            return $model->pegawai->nama;
        }
    ],
    [
        'attribute' => 'tanggal',
        'label' => Yii::t('app', 'Tanggal'),
        'value' => function (Attendance $model) {
            return date('d-m-Y', strtotime($model->check_time));
        },
    ],
    [
        'attribute' => 'jam',
        'value' => function (Attendance $model) {
            return date('H:i:s', strtotime($model->check_time));
        }
    ],
    ['attribute' => 'lock', 'hidden' => true],
];
?>

<?php try {
    echo GridView::widget([
        'dataProvider' => $model->dataProviderAttendance(),
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'attendance-grid-pjax']],
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
        ],
        'beforeHeader' => [
            [
                'columns' => [
                    [
                        'content' => Yii::t('frontend', 'Attendance'),
                        'options' => [
                            'colspan' => 5,
                            'class' => 'text-center warning',
                        ]
                    ],
                ],
                'options' => ['class' => 'skip-export']
            ]
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
        'tableOptions' => ['class' => 'small']
    ]);
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
} ?>

<?php

/**
 * Created by PhpStorm.
 * File Name: _form-proses-per-golongan.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 23/07/20
 * Time: 23.02
 */

use common\models\Golongan;
use frontend\modules\payroll\models\PayrollBpjs;
use frontend\modules\queue\models\AntrianProsesAbsensi;
use kartik\datecontrol\DateControl;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/**
 * @var $model frontend\models\Absensi
 * @var $dataProvider ActiveDataProvider
 */

$this->title = Yii::t('frontend', 'Proses Absensi Per Golongan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Absensi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<< JS
$(function() {
    $('#form-proses').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
     
    $('.btn-delete').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
})
JS;

$this->registerJs($js);


$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    ['attribute' => 'id', 'hidden' => true],
    [
        'attribute' => 'tanggal_awal',
        'value' => function (AntrianProsesAbsensi $model) {
            return date('d-m-Y', strtotime($model->tanggal_awal));
        },
    ],
    [
        'attribute' => 'tanggal_akhir',
        'value' => function (AntrianProsesAbsensi $model) {
            return date('d-m-Y', strtotime($model->tanggal_akhir));
        },
    ],
    [
        'attribute' => 'golongan_id',
        'value' => function (AntrianProsesAbsensi $model) {
            return $model->golongan->nama;
        },
    ],
    [
        'attribute' => 'bisnis_unit',
        'value' => function (AntrianProsesAbsensi $model) {
            return $model->bisnis_unit ? AntrianProsesAbsensi::getBisnisUnit()[$model->bisnis_unit] : '';
        },
    ],
    [
        'attribute' => 'status',
        'value' => function (AntrianProsesAbsensi $model) {
            return $model->_status[$model->status];
        }
    ],
    [
        'attribute' => 'created_by',
        'value' => function (AntrianProsesAbsensi $model) {
            return $model->createdBy->username;
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{delete-queue-proses}',
        'buttons' => [
            'delete-queue-proses' => function ($url, AntrianProsesAbsensi $model) {
                if ($model->status == AntrianProsesAbsensi::DIBUAT) {
                    return Html::a('Delete', $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                }
                return '';
            }
        ],
    ],
];
?>

<?php try {
    if ($dataProvider->count) {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'queue-grid-pjax']],
            'export' => [
                'label' => 'Page',
                'fontAwesome' => true,
            ],
            'beforeHeader' => [
                [
                    'columns' => [
                        [
                            'content' => Yii::t('frontend', 'Antrian Proses Absensi'),
                            'options' => [
                                'colspan' => 8,
                                'class' => 'text-center warning',
                            ]
                        ],
                    ],
                    'options' => ['class' => 'skip-export']
                ]
            ],
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'hover' => true,
            'showPageSummary' => false,
            'persistResize' => false,
            'tableOptions' => ['class' => 'small']
        ]);
    }
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
} ?>

<div class="absensi-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-proses',
        'action' => Url::to(['proses-per-golongan']),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['form-proses-per-golongan-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'tanggal_awal')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'tanggal_akhir')->widget(DateControl::class, [
                    'widgetOptions' => [
                        'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'golongan_id')->dropDownList(ArrayHelper::map(Golongan::find()->orderBy('id')->all(), 'id', 'nama')) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'bisnis_unit')->dropDownList(PayrollBpjs::modelPerjanjianKerja()->_kontrak, ['prompt' => '---']) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('frontend', 'Proses'), ['class' => 'btn btn-success btn-flat submit-btn']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

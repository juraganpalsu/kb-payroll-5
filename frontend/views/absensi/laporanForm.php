<?php

use common\models\Golongan;
use frontend\models\form\AbsensiForm;
use frontend\modules\pegawai\models\PerjanjianKerjaJenis;
use frontend\modules\queue\models\AntrianLaporanAbsensi;
use frontend\modules\queue\models\AntrianProsesAbsensi;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model AntrianLaporanAbsensi */
/* @var $sistemKerja */
/**
 * @var $statusKehadiran
 * @var $dataProvider ActiveDataProvider
 */

$this->title = Yii::t('app', 'Form Laporan Absensi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Absensi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$js = <<< JS
$(function() {
    $('#laporan-absensi-form').on('beforeSubmit', function (event) {
        event.preventDefault();
        var form = $(this);
        var btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            if(dt.status === true){
                location.href = dt.data.url;
            }
        });
        return false;
    });
    
    $('.btn-delete').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
})
JS;

$this->registerJs($js);


$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    ['attribute' => 'id', 'hidden' => true],
    [
        'attribute' => 'tanggal_awal',
        'value' => function (AntrianLaporanAbsensi $model) {
            return date('d-m-Y', strtotime($model->tanggal_awal));
        },
    ],
    [
        'attribute' => 'tanggal_akhir',
        'value' => function (AntrianLaporanAbsensi $model) {
            return date('d-m-Y', strtotime($model->tanggal_akhir));
        },
    ],
    [
        'attribute' => 'golongan_id',
        'value' => function (AntrianLaporanAbsensi $model) {
            return $model->golongan->nama;
        },
    ],
    [
        'attribute' => 'bisnis_unit',
        'value' => function (AntrianLaporanAbsensi $model) {
            return $model->bisnis_unit ? AntrianProsesAbsensi::getBisnisUnit()[$model->bisnis_unit] : '';
        },
    ],
    [
        'attribute' => 'jenis_perjanjian',
        'value' => function (AntrianLaporanAbsensi $model) {
            return $model->jenis_perjanjian ? $model->jenisPerjanjian->nama : '';
        },
    ],
    [
        'attribute' => 'jenis_laporan',
        'value' => function (AntrianLaporanAbsensi $model) {
            return $model->_jenisLaporan[$model->jenis_laporan];
        },
    ],
    [
        'attribute' => 'status',
        'value' => function (AntrianLaporanAbsensi $model) {
            return AntrianLaporanAbsensi::_status($model->status);
        }
    ],
    [
        'attribute' => 'created_by',
        'value' => function (AntrianLaporanAbsensi $model) {
            return $model->createdBy->username;
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{download-queue-laporan}{delete-queue-laporan}',
        'buttons' => [
            'download-queue-laporan' => function ($url, AntrianLaporanAbsensi $model) {
                if ($model->status == AntrianProsesAbsensi::SELESAI) {
                    return Html::a(Yii::t('frontend', 'Download'), $url, ['class' => 'btn btn-xs btn-success btn-flat']);
                }
                return '';
            },
            'delete-queue-laporan' => function ($url, AntrianLaporanAbsensi $model) {
                if ($model->status == AntrianProsesAbsensi::DIBUAT || $model->status == AntrianProsesAbsensi::SELESAI) {
                    return Html::a('Delete', $url, ['class' => 'btn btn-xs btn-danger btn-flat btn-delete', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);
                }
                return '';
            },
        ],
    ],
];
?>

<?php try {
    if ($dataProvider->count) {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'pjax' => false,
            'pjaxSettings' => ['options' => ['id' => 'queue-grid-pjax']],
            'export' => [
                'label' => 'Page',
                'fontAwesome' => true,
            ],
            'beforeHeader' => [
                [
                    'columns' => [
                        [
                            'content' => Yii::t('frontend', 'Antrian Laporan Absensi(Antrian akan dihapus otomatis pada jam 12 malam.)'),
                            'options' => [
                                'colspan' => 10,
                                'class' => 'text-center warning',
                            ]
                        ],
                    ],
                    'options' => ['class' => 'skip-export']
                ]
            ],
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'hover' => true,
            'showPageSummary' => false,
            'persistResize' => false,
            'tableOptions' => ['class' => 'small']
        ]);
    }
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
} ?>

<div class="laporan-absensi">
    <div class="absensi-form">

        <?php $form = ActiveForm::begin([
            'id' => 'laporan-absensi-form',
            'options' => ['target' => '_blank'],
            'type' => ActiveForm::TYPE_VERTICAL,
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['/absensi/laporan-validation'])
        ]); ?>

        <?= $form->errorSummary($model); ?>

        <?php try { ?>

            <div class="row">
                <div class="col-md-7">
                    <?php
                    echo $form->field($model, 'golongan_id')->widget(Select2::class, [
                        'options' => ['placeholder' => Yii::t('frontend', 'Select Golongan')],
                        'data' => ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'),
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <?php
                    echo $form->field($model, 'bisnis_unit')->dropDownList(AbsensiForm::modelPerjanjianKerja()->_kontrak, ['prompt' => Yii::t('frontend', 'Semua')]);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php
                    echo $form->field($model, 'jenis_perjanjian')->dropDownList(ArrayHelper::map(PerjanjianKerjaJenis::find()->all(), 'id', 'nama'), ['prompt' => Yii::t('frontend', 'Semua')]);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <?php
                    try {
                        echo $form->field($model, 'status_kehadiran')->dropDownList($statusKehadiran);
                    } catch (InvalidConfigException $e) {
                        Yii::info($e->getMessage(), 'exception');
                    }
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <?php
                    try {
                        echo $form->field($model, 'jenis_laporan')->dropDownList($model->_jenisLaporan);
                    } catch (InvalidConfigException $e) {
                        Yii::info($e->getMessage(), 'exception');
                    }
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'tanggal_awal')->widget(DateControl::class, [
                        'widgetOptions' => [
                            'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true,
                            ],
                        ]
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'tanggal_akhir')->widget(DateControl::class, [
                        'widgetOptions' => [
                            'options' => ['placeholder' => Yii::t('app', 'Pilih Tanggal')],
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true,
                            ],
                        ]
                    ]); ?>
                </div>
            </div>

            <hr/>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success btn-flat', 'id' => 'laporan_absensi-btn']) ?>
            </div>

            <?php
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
        <?php ActiveForm::end(); ?>
    </div>


</div>

<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 8/28/17
 * Time: 10:04 PM
 */

/** @var array $datas */
/** @var \frontend\models\form\AbsensiForm $model */
?>

<?php
$header = $this->renderAjax('_headerLaporan', ['model' => $model, 'datas' => $datas]);
$height = 4;
$row = 1;
$rowHeader = 6;
$maxLengthName = 30;
$totalHeight = 0;
$countPeriod = 1;
$maxRow = 60;
echo $header;

$countHeight = $height;
foreach ($datas as $keyData => $data) {
    ?>
    <tr>
        <td colspan="24" height="14px"
            style="border: 1px solid black; height: <?php echo $height; ?>mm "><?php echo '<strong>' . date('d-m-Y', strtotime($keyData)) . '</strong>';
            $row++ ?></td>
    </tr>
    <?php foreach ($data as $d) { ?>
        <tr>
            <td colspan="2" class="center dotted"
                style="border-left: 1px solid  black; border-bottom: 1px dotted  black; text-align: center; height: <?php echo $height; ?>mm"><?php echo str_pad($d['idfp'], 8, '0', STR_PAD_LEFT); ?></td>
            <td colspan="6" class="dotted"
                style="border-left: 1px solid  black; border-bottom: 1px dotted  black; padding-left: 2mm"><?php echo $d['nama']; ?></td>
            <td colspan="2" class="center dotted"
                style="border-left: 1px solid  black; border-bottom: 1px dotted  black; text-align: center; height: <?php echo $height; ?>mm"><?php echo $d['status_kehadiran']; ?></td>
            <td colspan="3" class="dotted"
                style="border-left: 1px solid  black; border-bottom: 1px dotted  black; text-align: center;"><?php echo !empty($d['masuk']) ? date('d-m-Y H:i:s', strtotime($d['masuk'])) : ''; ?></td>
            <td colspan="3" class="center dotted"
                style="border-left: 1px solid  black; border-bottom: 1px dotted  black; text-align: center;"><?php echo !empty($d['pulang']) ? date('d-m-Y H:i:s', strtotime($d['pulang'])) : ''; ?></td>
            <td colspan="6" class="center dotted"
                style="border-left: 1px solid  black; border-bottom: 1px dotted  black; text-align: center;"><?php echo $d['time_table'] ?? ''; ?></td>
            <td colspan="2" class="dotted"
                style="border-left: 1px solid  black; border-bottom: 1px dotted  black; text-align: center;"><?php echo $d['jam_lembur'] > 0 ? $d['jam_lembur'] : ''; ?></td>
        </tr>
        <?php

        if ($maxRow == $row) {
            ?>
            </table>
            <div style="height: 2px"></div>
            <?php
            echo $header;
            $row = 0;
        }
        $row++;
    }
}
?>
</table>
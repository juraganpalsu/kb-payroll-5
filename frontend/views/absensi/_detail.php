<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Absensi */

?>
<div class="absensi-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'hidden' => true],
            'masuk',
            'pulang',
            'tanggal',
            [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('app', 'Pegawai'),
            ],
            'time_table',
            'status_kehadiran',
            'jumlah_jam_lembur',
            ['attribute' => 'lock', 'hidden' => true],
        ];
        try {
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
</div>
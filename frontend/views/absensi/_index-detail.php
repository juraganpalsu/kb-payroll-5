<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 30/12/2018
 * Time: 16:08
 */

use common\models\Absensi;

/**
 * @var Absensi $model
 *
 */
?>

<div class="row">
    <div class="col-sm-6">
        <?php echo $this->renderAjax('_attendance', ['model' => $model]) ?>
    </div>
    <div class="col-sm-6">
        <?php echo $this->renderAjax('_spl', ['model' => $model]) ?>
    </div>
</div>


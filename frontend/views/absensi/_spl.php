<?php

/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 24/1/2019
 * Time: 23:11
 */

use frontend\models\Absensi;
use yii\widgets\DetailView;


/**
 * @var Absensi $model
 */

$gridColumn1 = [
    'nama',
    'kategori_',
    'shift_',
//    'jumlah_jam_lembur',
    'masuk_minimum',
    'masuk',
    'masuk_maksimum',
    'pulang_minimum',
    'pulang',
    'pulang_maksimum',
];

if ($model->splTemplate) {
    ?>
    <strong><?= Yii::t('frontend', 'Spl Template') ?></strong>
    <div class="row">
        <div class="col-sm-6">
            <?php
            try {
                echo DetailView::widget([
                    'model' => $model->splTemplate,
                    'attributes' => $gridColumn1
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
<?php } ?>
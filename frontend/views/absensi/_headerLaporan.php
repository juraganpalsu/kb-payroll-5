<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 8/28/17
 * Time: 10:30 PM
 */


/** @var \frontend\models\form\AbsensiForm $model */

$height = 4;
?>

<table style="border-collapse: collapse; page-break-after: always; border: 1px solid black">
    <!--    <tr>-->
    <!--        <td width="8mm">1&nbsp;</td>-->
    <!--        <td width="8mm">2&nbsp;</td>-->
    <!--        <td width="8mm">3&nbsp;</td>-->
    <!--        <td width="8mm">4&nbsp;</td>-->
    <!--        <td width="8mm">5&nbsp;</td>-->
    <!--        <td width="8mm">6&nbsp;</td>-->
    <!--        <td width="8mm">7&nbsp;</td>-->
    <!--        <td width="8mm">8&nbsp;</td>-->
    <!--        <td width="8mm">9&nbsp;</td>-->
    <!--        <td width="8mm">10&nbsp;</td>-->
    <!--        <td width="8mm">11&nbsp;</td>-->
    <!--        <td width="8mm">12&nbsp;</td>-->
    <!--        <td width="8mm">13&nbsp;</td>-->
    <!--        <td width="8mm">14&nbsp;</td>-->
    <!--        <td width="8mm">15&nbsp;</td>-->
    <!--        <td width="8mm">16&nbsp;</td>-->
    <!--        <td width="8mm">17&nbsp;</td>-->
    <!--        <td width="8mm">18&nbsp;</td>-->
    <!--        <td width="8mm">19&nbsp;</td>-->
    <!--        <td width="8mm">20&nbsp;</td>-->
    <!--        <td width="8mm">21&nbsp;</td>-->
    <!--        <td width="8mm">22&nbsp;</td>-->
    <!--        <td width="8mm">23&nbsp;</td>-->
    <!--        <td width="8mm">24&nbsp;</td>-->
    <!--    </tr>-->
    <tr>
        <td class=" bdrBottom" width="16mm" colspan="2"
            style=" border-bottom: 1px solid black; border-top: 1px solid black; vertical-align: top; border-left: 1px solid black;">
            <div style=" margin-right: 10px;"><?php echo 'Logo'; ?></div>
        </td>
        <td width="56mm" colspan="7"
            style=" border-bottom: 1px solid black; border-top: 1px solid black; vertical-align: top; padding-left: 3mm;">
            <div><strong>Kobe Boga Utama</strong><br/>
                Jakartan <br/>
                Indonesia
            </div>
        </td>
        <td width="112mm" colspan="15" style=" border-bottom: 1px solid black; border-top: 1px solid black;">
            <h3><strong>LAPORAN ABSENSI</strong></h3>
        </td>
    </tr>
    <tr>
        <td height="15px" colspan="2"
            style=" border-left: 1px solid black; border-bottom: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo 'Sistem Kerja '; ?>
        </td>
        <td colspan="10" style=" border-right: 1px solid black; border-bottom: 1px solid black">
            <?php echo ': <strong>' . $model->getSistemKerja()[$model->sistem_kerja] . '</strong>'; ?>
        </td>
        <td height="15px" colspan="2"
            style=" border-left: 1px solid black; border-bottom: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo 'Status Kehadiran '; ?>
        </td>
        <td colspan="10" style=" border-right: 1px solid black; border-bottom: 1px solid black">
            <?php echo ': <strong>' . $model->getStatusKehadiran()[$model->status_kehadiran] . '</strong>'; ?>
        </td>
    </tr>
    <tr>
        <td height="15px" colspan="2"
            style=" border-left: 1px solid black; border-bottom: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo 'Printed '; ?>
        </td>
        <td colspan="10" style=" border-right: 1px solid black; border-bottom: 1px solid black">
            <?php echo ': <strong>' . date('d-M-Y H:i') . '</strong>'; ?>
        </td>
        <td height="15px" colspan="2"
            style=" border-left: 1px solid black; border-bottom: 1px solid black; height: <?php echo $height; ?>mm ">
            <?php echo 'Periode '; ?>
        </td>
        <td colspan="10" style=" border-right: 1px solid black; border-bottom: 1px solid black">
            <?php echo ': ' . date('d-m-Y', strtotime($model->tanggal_awal)) . ' s/d ' . date('d-m-Y', strtotime($model->tanggal_akhir)) ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Idfp
        </td>
        <td colspan="6" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Nama
        </td>
        <td colspan="2" class="center head"
            style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
            Status
        </td>
        <?php if ($model->status_kehadiran == \frontend\models\Absensi::ERROR) { ?>
            <td colspan="14" class="center head"
                style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
                Attendance
            </td>
        <?php } else { ?>
            <td colspan="3" class="center head"
                style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
                Masuk
            </td>
            <td colspan="3" class="center head"
                style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
                Pulang
            </td>
            <td colspan="6" class="center head"
                style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
                Time Table
            </td>
            <td colspan="2" class="center head"
                style="text-align: center; background-color: #EEEEEE; border-left: 1px solid  black; border-bottom: 1px solid  black; height: <?php echo $height; ?>mm ">
                Jumlah Lembur
            </td>
        <?php } ?>
    </tr>

<?php

use frontend\models\form\AbsensiForm;
use kartik\daterange\DateRangePicker;
use kartik\dialog\Dialog;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model AbsensiForm */
/* @var $form yii\widgets\ActiveForm */

try {
    echo Dialog::widget();
} catch (Exception $e) {
}


$js = <<< JS
$(function() {
    
    post_proses = function(form, btn){
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
    };
    
    $('#form-proses').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        
        let pid = $('#absensiform-pegawai_ids');
        btn.button('loading');
        if(pid.val() == ''){
            krajeeDialog.confirm("Are you really sure you want to do this?", function (result) {
                if (result) {
                    post_proses(form, btn);
                }else{
                    location.reload();
                }
            });
            return false;
        }else {
            post_proses(form, btn);
            return false;
        }
    });
})
JS;

$this->registerJs($js);
?>

<div class="absensi-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-proses',
        'action' => Url::to(['proses']),
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['form-proses-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>
        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'tanggal', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                    'options' => ['class' => 'drp-container form-group']
                ])->widget(DateRangePicker::class, [
                    'convertFormat' => true,
                    'useWithAddon' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'pegawai_ids')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('app', '-- Pilih Pegawai'),
                        'multiple' => true,
                    ],
                    'data' => [],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) {
                            return {q:params.term}; 
                        }')
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <hr>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Proses'), ['class' => 'btn btn-success btn-flat submit-btn']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

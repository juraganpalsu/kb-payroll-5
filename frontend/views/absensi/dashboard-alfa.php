<?php

/**
 * Created by PhpStorm.
 * File Name: dashboard-alfa.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-07-25
 * Time: 4:29 PM
 */


/* @var $this yii\web\View */
/* @var $searchModel StrukturSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Helper;
use frontend\modules\struktur\models\search\StrukturSearch;
use frontend\modules\struktur\models\Struktur;
use kartik\grid\GridView;

$hMinSatu = date('Y-m-d', strtotime('-1 day'));
$hMinEmpat = date('Y-m-d', strtotime('-4 day'));
$this->title = 'Dashboard Alfa ' . Helper::idDate($hMinEmpat) . ' s.d ' . Helper::idDate($hMinSatu);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="divisi-index">
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'name',
            'value' => function (Struktur $model) {
                return $model->nameCostume;
            }
        ],
        [
            'label' => Yii::t('frontend', 'Jumlah'),
            'value' => function (Struktur $model) {
                return StrukturSearch::getDataAlfaPerDepartemen($model->id);
            }
        ],


        ['attribute' => 'lock', 'visible' => false],
    ];
    ?>
    <?php try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-struktur']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                '{export}',
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        print_r($e->getMessage());
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>
<?php

use common\components\Status;
use frontend\models\Absensi;
use frontend\models\Attendance;
use frontend\models\Spl;
use frontend\models\TimeTable;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\AbsensiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Absensi');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(function ($) {    
    $('.btn-call-modal').click(function(e){
        e.preventDefault();
        let idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $('.search-button').click(function(){
	    $('.search-form').toggle(1000);
	        return false;
    });
});
JS;

$this->registerJs($js);
?>
    <div class="absensi-index">
        <p>
            <?php
            if (Helper::checkRoute('/absensi/proses-per-golongan')) {
                try {
                    echo ButtonDropdown::widget([
                        'label' => Yii::t('frontend', 'Proses Absensi'),
                        'options' => [
                            'class' => 'btn btn-success btn-flat',
                        ],
                        'dropdown' => [
                            'items' => [
                                ['label' => Yii::t('frontend', 'Per Golongan'), 'url' => Url::to(['/absensi/proses-per-golongan']), 'linkOptions' => ['target' => '_blank']],
                                ['label' => Yii::t('frontend', 'Per Karyawan'), 'url' => Url::to(['/absensi/proses']), 'linkOptions' => ['target' => '_blank']]
                            ]
                        ],
                    ]);
                } catch (Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
            ?>
            <?php
            if (Helper::checkRoute('/absensi/laporan-form')) {
                echo Html::a(Yii::t('app', 'Laporan Absensi'), ['laporan-form'], ['class' => 'btn btn-primary btn-flat', 'data' => ['header' => Yii::t('frontend', 'Laporan Absensi')]]);
                echo ' ' . Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button btn-flat']);
                echo ' ' . Html::a(Yii::t('app', 'Index Absensi All'), ['index-all'], ['class' => 'btn btn-warning btn-flat', 'target' => '_blank']);
            }
            ?>
            <?php
            if (Helper::checkRoute('/absensi/index-ess')) {
                echo Html::a(Yii::t('app', 'Absen My Team'), ['index-absen-team'], ['class' => 'btn btn-success btn-flat']);
                echo ' ' . Html::a(Yii::t('app', 'Bantuan'), ['ess-bantuan'], ['class' => 'btn btn-primary btn-flat']);
            }
            ?>
        </p>
        <div class="search-form" style="display:none">
            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'hidden' => true],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',
                'value' => function () {
                    return GridView::ROW_COLLAPSED;
                },
                'enableRowClick' => true,
                'detailUrl' => Url::to(['show-detail']),
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true
            ],
            [
                'attribute' => 'id_pegawai',
                'label' => Yii::t('app', 'Id Pegawai'),
                'value' => function (Absensi $model) {
                    return $model->perjanjianKerja ? str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '';
                },
            ],
            [
                'attribute' => 'pegawai_id',
                'label' => Yii::t('app', 'Pegawai'),
                'value' => function (Absensi $model) {
                    return $model->pegawai ? $model->pegawai->nama : '';
                },
                'format' => 'raw'
            ],
//            [
//                'attribute' => 'departemen',
//                'value' => function (Absensi $model) {
//                    $pegawaiStruktur = $model->pegawaiStruktur;
//                    return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : '';
//                },
//                'hidden' => true
//            ],
//            [
//                'attribute' => 'struktur',
//                'value' => function (Absensi $model) {
//                    $pegawaiStruktur = $model->pegawaiStruktur;
//                    return $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : '';
//                },
//                'hidden' => true
//            ],
            [
                'attribute' => 'busines_unit',
                'value' => function (Absensi $model) {
                    if ($perjanjianKerja = $model->perjanjianKerja) {
                        return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                    }
                    return '';
                },
            ],
//            [
//                'attribute' => 'golongan_id',
//                'value' => function (Absensi $model) {
//                    if ($golongan = $model->pegawaiGolongan) {
//                        return $golongan->golongan->nama;
//                    }
//                    return '';
//                },
//                'hidden' => true
//            ],
//            [
//                'attribute' => 'sistem_kerja',
//                'value' => function (Absensi $model) {
//                    return $model->pegawai->getPegawaiSistemKerjaUntukTanggal($model->tanggal) ? $model->pegawai->getPegawaiSistemKerjaUntukTanggal($model->tanggal)->sistemKerja->nama : '';
//                },
//                'hidden' => true
//            ],
//            'perjanjian_kerja_id',
//            'pegawai_struktur_id',
            [
                'attribute' => 'tanggal',
                'label' => Yii::t('app', 'Tanggal'),
                'value' => function (Absensi $model) {
                    return date('d-m-Y', strtotime($model->tanggal));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ],
            ],
            [
                'attribute' => 'masuk',
                'value' => function (Absensi $model) {
                    if ($model->masuk) {
                        $jamMasuk = date('d-m-Y H:i:s', strtotime($model->masuk));
//                        if ($telatMasuk = $model->hitungTelatMasuk()) {
//                            $jamMasuk .= Html::tag('strong', ' (' . $telatMasuk . ')', ['class' => 'text-danger']);
//                        }
                        return $jamMasuk;
                    }
                    return '';
                },
                'format' => 'raw'
            ],
            [

                'attribute' => 'pulang',
                'value' => function (Absensi $model) {
                    if ($model->pulang) {
                        $jamPulang = date('d-m-Y H:i:s', strtotime($model->pulang));
//                        if ($pulangCepat = $model->hitungPulangCepat()) {
//                            $jamPulang .= Html::tag('strong', ' (' . $pulangCepat . ')', ['class' => 'text-danger']);
//                        }
                        return $jamPulang;
                    }
                    return '';
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'shift_search',
                'label' => Yii::t('app', 'Shift'),
                'width' => '10%',
                'value' => function (Absensi $model) {
                    return $model->time_table_id ? $model->timeTable : '' ? $model->timeTable->shift_ : '';
                },
//            'filter' => $searchModel->shift_,
                'filter' => Html::activeDropDownList($searchModel, 'shift_search', $searchModel->shift_, ['class' => 'form-control', 'prompt' => 'Select Category']),
            ],
            [
                'label' => Yii::t('app', 'Telat'),
                'width' => '10%',
                'value' => function (Absensi $model) {
                    return $model->telat;
                },
            ],
            [
                'attribute' => 'spl_id',
                'label' => Yii::t('app', 'SPL'),
                'width' => '10%',
                'value' => function (Absensi $model) {
                    if ($model->spl_id > 0) {
                        //antisipasi SPL dihapus
                        if ($model->splRel) {
                            return $model->splRel->splTemplate->nama;
                        }
                        return '';
                    } else {
                        if ($model->pegawai) {
                            $modelSpl = new Spl();
                            $getSpl = $modelSpl->getSplByPegawai($model->tanggal, $model->pegawai);
                            if ($getSpl) {
                                if ($getSpl->status_approval == Status::APPROVED && $model->status_kehadiran == Absensi::ERROR) {
                                    return Html::tag('span', $getSpl->splTemplate->nama, ['style' => 'color:red']);
                                } elseif ($getSpl->status_approval == Status::REJECTED && $model->status_kehadiran == Absensi::ERROR) {
                                    return Html::tag('span', $getSpl->splTemplate->nama, ['style' => 'color:orange']);
                                } elseif ($getSpl->status_approval == Status::DEFLT && $model->status_kehadiran == Absensi::ERROR) {
                                    return Html::tag('span', $getSpl->splTemplate->nama, ['style' => 'color:green']);
                                }
                            }
                        }
                        return '';
                    }
                },
                'format' => 'html'
//            'filter' => $searchModel->shift_,
            ],
//        'tanggal',
            [
                'attribute' => 'time_table_id',
                'value' => function (Absensi $model) {
                    return !$model->time_table_id ? '' : $model->timeTable ? $model->timeTable->nama : '';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(TimeTable::find()->all(), 'id', 'namaSistemKerja'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Time Table', 'id' => 'grid-absensi-search-time_table_id']
            ],
            [
                'attribute' => 'status_kehadiran',
                'value' => function (Absensi $model) {
                    return $model->statusKehadiran_;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->_status_kehadiran,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Time Table', 'id' => 'grid-absensi-search-status_kehadiran']
            ],
//            [
//                'attribute' => 'attendance',
//                'value' => function (Absensi $model) {
//                    $provider = array_map(function (Attendance $x) {
//                        return date('d-m-Y H:i:s', strtotime($x->check_time));
//                    }, $model->dataProviderAttendance(false));
//                    return implode("\n", $provider);
//                },
//                'format' => 'raw',
//                'hidden' => true
//            ],
            [
                'attribute' => 'jumlah_jam_lembur',
                'value' => function (Absensi $model) {
                    return $model->jamLembur();
                },
            ],
//            [
//                'attribute' => 'uml',
//                'label' => 'UML',
//                'value' => function (Absensi $model) {
//                    $pengali = $model->hitungPengaliUangMakanLembur();
//                    return $pengali ? 'UML' . $pengali : '';
//                },
//                'hidden' => true
//            ],
//            [
//                'attribute' => 'area_kerja',
//                'value' => function (Absensi $model) {
//                    return $model->pegawai->getAreaKerjaUntukTanggal($model->tanggal) ? $model->pegawai->getAreaKerjaUntukTanggal($model->tanggal)->refAreaKerja->name : '';
//                },
//                'hidden' => true
//            ],
            ['attribute' => 'lock', 'hidden' => true],
//        [
//            'class' => 'yii\grid\ActionColumn',
//        ],
        ];
        ?>
        <?php try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
                'noExportColumns' => [1, 2, 20], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'absensi' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-absensi']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        } ?>
    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
<?php

use common\models\Golongan;
use \common\models\base\Absensi;
use common\models\SistemKerja;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\modules\pegawai\models\form\PegawaiForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\AbsensiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-absensi-search">

    <?php $form = ActiveForm::begin([
        'action' => [Yii::$app->controller->action->id],
        'method' => 'get',
    ]); ?>

    <?php try { ?>

        <div class="row">
            <div class="col-md-5">
                <?php
                echo $form->field($model, 'sistem_kerja')->widget(Select2::class, [
                    'data' => ArrayHelper::map(SistemKerja::find()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => $model->getAttributeLabel('sistem_kerja'), 'multiple' => false],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?php
                echo $form->field($model, 'busines_unit')->dropDownList(Absensi::modelPerjanjianKerja()->_kontrak, ['prompt' => '*']);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?php
                echo $form->field($model, 'golongan_id')->dropDownList(ArrayHelper::map(Golongan::find()->all(), 'id', 'nama'), ['prompt' => '*']);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?php
                echo $form->field($model, 'tanggal', [
                    'addon' => ['prepend' => ['content' => '<i class="fa fa-calendar"></i>']],
                    'options' => ['class' => 'drp-container form-group', 'id' => 'ads']
                ])->widget(DateRangePicker::class, [
                    'options' => [
                        'id' => 'absensisearch-tanggal-form',
                        'class' => 'form-control',
                    ],
                    'convertFormat' => true,
                    'useWithAddon' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' s/d ',
                        ],
                        'opens' => 'left'
                    ]
                ]); ?>
            </div>
        </div>

        <hr/>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    }
    ?>


    <?php ActiveForm::end(); ?>

</div>

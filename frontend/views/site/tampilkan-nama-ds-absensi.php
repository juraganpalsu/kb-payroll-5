<?php

/**
 * Created by PhpStorm.
 * File Name: tampilkan-nama-ds-absensi.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 26/10/20
 * Time: 22.08
 */

/**
 *
 * @var ActiveDataProvider $dataProvider
 * @var DsAbsensi $modelDsAbsensi Ds
 *
 *
 */

use common\components\Status;
use frontend\models\Absensi;
use frontend\modules\dashboard\models\DsAbsensi;
use frontend\modules\pegawai\models\Pegawai;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

?>


<?php try {
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'id_pegawai',
            'value' => function (Pegawai $model) {
                return $model->perjanjianKerja ? Html::tag('strong', str_pad($model->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT), ['class' => 'font-weight-bold']) : '';
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'nama_lengkap',
            'value' => function (Pegawai $model) {
                return Html::a(Html::tag('strong', $model->nama_lengkap), ['view', 'id' => $model->id], ['class' => 'text-danger font-weight-bold']);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'golongan',
            'value' => function (Pegawai $model) {
                if ($golongan = $model->pegawaiGolongan) {
                    return $golongan->golongan->nama;
                }
                return '';
            },
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'jenis_kontrak',
            'value' => function (Pegawai $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->jenis->nama;
                }
                return '';
            },
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'busines_unit',
            'value' => function (Pegawai $model) {
                if ($perjanjianKerja = $model->perjanjianKerja) {
                    return $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak];
                }
                return '';
            },
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'struktur',
            'value' => function (Pegawai $model) {
                if ($struktur = $model->pegawaiStruktur) {
                    return $struktur->struktur->nameCostume;
                }
                return '';
            }
        ],
        [
            'attribute' => 'area_kerja',
            'value' => function (Pegawai $model) {
                if ($areaKerja = $model->areaKerja) {
                    return $areaKerja->refAreaKerja->name;
                }
                return '';
            }
        ],
        [
            'attribute' => 'sistem_kerja',
            'width' => '20%',
            'value' => function (Pegawai $model) {
                $text = '';
                if (!is_null($model->pegawaiSistemKerja)) {
                    $text = $model->pegawaiSistemKerja->sistemKerja->nama;
                }
                return $text;
            },
            'hAlign' => 'center',
            'format' => 'raw'
        ],
        [
            'attribute' => 'is_resign',
            'value' => function (Pegawai $model) {
                return Status::activeInactive($model->is_resign);
            },
        ],
    ];

    $modelAbsensi = new Absensi();
    if ($dataProvider->count) {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'queue-grid-pjax']],
            'export' => [
                'label' => 'Page',
                'fontAwesome' => true,
            ],
            'beforeHeader' => [
                [
                    'columns' => [
                        [
                            'content' => Yii::t('frontend', 'Daftar karyawan {status} pada {tanggal}', ['status' => $modelAbsensi->_status_kehadiran[$modelDsAbsensi->status_kehadiran], 'tanggal' => date('d-m-Y', strtotime($modelDsAbsensi->dsPerTanggal->tanggal))]),
                            'options' => [
                                'colspan' => 10,
                                'class' => 'text-center warning',
                            ]
                        ],
                    ],
                    'options' => ['class' => 'skip-export']
                ]
            ],
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'hover' => true,
            'showPageSummary' => false,
            'persistResize' => false,
            'tableOptions' => ['class' => 'small']
        ]);
    }
} catch (Exception $e) {
    Yii::info($e->getMessage(), 'exception');
} ?>


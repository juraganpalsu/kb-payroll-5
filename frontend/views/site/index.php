<?php

/* @var $this yii\web\View */

/**
 * @var array $jenisKelamin
 * @var \common\models\Pegawai $modelPegawai
 */

use mdm\admin\components\Helper;
use yii\helpers\Html;

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= Yii::$app->name; ?>!</h1>
        <?php
        if (Helper::checkRoute('site/dashboard-hr')) {
            echo Html::a(Yii::t('frontend', 'Dashboard'), ['dashboard-hr'], ['class' => 'btn btn-success btn-flat']);
        }
        ?>
    </div>
    <div class="col-md-4">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <li class="icon fa fa-birthday-cake"></li>
                    Today's Birthday
                    <li class="icon fa fa-birthday-cake"></li>
                </h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                Hallo, sahabat Kobe. Happy Birthday to : <br>
                <?php
                foreach ($modelPegawai->birthdayPegawai() as $item) {
                    echo "<li class='icon fa fa-birthday-cake'></li> <b>" . $item['nama'] . " (" . $item['departemen'] . ")</b><br/>";
                }
                ?>
                Sehat selalu dan bahagia selalu. Aamiin
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

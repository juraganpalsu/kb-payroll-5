<?php
/**
 * Created by PhpStorm.
 * File Name: dashboard-control-data.php
 * User: bobi -> bobi[.]arip[at]gmail[.]com
 * Date: 03/09/2020
 * Time: 17:16
 */

/* @var $searchModel PegawaiSearch */
/* @var $pegawaiTanpaMasaKerja yii\data\ActiveDataProvider */
/* @var $pegawaiTanpaMasterData yii\data\ActiveDataProvider */

/* @var $pegawaiHabisKontrak yii\data\ActiveDataProvider */

/* @var $pegawaiTanpaSistemKerja yii\data\ActiveDataProvider */


use frontend\models\search\PegawaiSearch;
use yii\helpers\Url;

$this->title = Yii::t('frontend', 'Dashboard Control Data')
?>

<!-- ./col -->
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-light-blue">
        <div class="inner">
            <h3><?= $pegawaiTanpaSistemKerja->totalCount ?></h3>

            <strong>Pegawai Aktif</strong>
            <p><strong>Tanpa Sistem Kerja</strong></p>
        </div>
        <div class="icon">
            <i class="fa fa-calendar-times-o"></i>
        </div>
        <a href="<?= Url::to(['/pegawai/pegawai/pegawai-aktif-tanpa-sistem-kerja']) ?>" class="small-box-footer">
            Detail Data
            <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-3 col-xs-6" xmlns="http://www.w3.org/1999/html">
    <!-- small box -->
    <div class="small-box bg-green">
        <div class="inner">
            <h3><?= $pegawaiTanpaMasaKerja->totalCount ?></sup></h3>

            <strong>Pegawai Aktif</strong>
            <p><strong>Tanpa Dasar Masa Kerja</strong></p>
        </div>
        <div class="icon">
            <i class="fa fa-users"></i>
        </div>
        <a href="<?= Url::to(['/pegawai/pegawai/pegawai-aktif-tanpa-dasar-masa-kerja']) ?>" class="small-box-footer">Detail
            Data <i
                    class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?= $pegawaiTanpaMasterData->totalCount ?></h3>

            <strong>Pegawai Aktif</strong>
            <p><strong>Tanpa Master Data Payroll</strong></p>
        </div>
        <div class="icon">
            <i class="fa fa-money"></i>
        </div>
        <a href="<?= Url::to(['/pegawai/pegawai/pegawai-aktif-tanpa-master-data']) ?>" class="small-box-footer">
            Detail Data
            <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
        <div class="inner">
            <h3><?= $pegawaiHabisKontrak->totalCount ?></h3>

            <strong>Pegawai Aktif</strong>
            <p><strong>Habis Kontrak</strong></p>
        </div>
        <div class="icon">
            <i class="fa fa-file"></i>
        </div>
        <a href="<?= Url::to(['/pegawai/default/habis-kontrak']) ?>" class="small-box-footer">Detail Data <i
                    class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>


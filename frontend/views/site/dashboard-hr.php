<?php

/**
 * Created by PhpStorm.
 * File Name: dashboard-hr.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 27/10/19
 * Time: 18.53
 */

use frontend\modules\pegawai\models\Pegawai;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * @var array $jenisKelamin
 * @var array $pegawaiPerGolongan
 * @var array $pegawaiPerBusinesUnit
 * @var array $pegawaiPerJenisKontrak
 * @var array $dsAkhirKontrak
 * @var array $dashboardAbsensi
 * @var array $dsSuratPeringatan
 * @var array $dsJumlahPegawaiPerdepartemen
 * @var array $dsTurnOver
 * @var array $dsInOut
 * @var array $dsRangeAge
 *
 */


$lakiLaki = (int)$jenisKelamin[Pegawai::LAKI_LAKI];
$perempuan = (int)$jenisKelamin[Pegawai::PEREMPUAN];

$this->title = Yii::t('frontend', 'Dashboard HR');

$urlTo = Url::to(['/site/tampilkan-nama?param=']);

$js = <<< JS
$(function() {
    tampilkanNama = function(th) {
      console.log(th.category, th.y, th.series.name);
      let win = window.open('$urlTo' + th.category + '*' + th.y + '*' + th.series.name, '_blank');
      win.focus();
    }
});
JS;

$this->registerJs($js);

?>
<div class="row">
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'pie'
                    ],
                    'title' => [
                        'text' => Yii::t('frontend', 'Jumlah karyawan berdasarkan jenis kelamin')
                    ],
                    'tooltip' => [
                        'pointFormat' => '{series.name}: <b>{point.y}</b>'
                    ],
                    'plotOptions' => [
                        'pie' => [
                            'allowPointSelect' => true,
                            'cursor' => 'pointer',
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '<b>{point.name}</b>: {point.y}'
                            ],
                            'showInLegend' => true
                        ]
                    ],
                    'series' => [[
                        'name' => 'Brands',
                        'colorByPoint' => true,
                        'data' => [
                            [
                                'name' => 'Perempuan',
                                'y' => $perempuan,
//                                'sliced' => true,
//                                'selected' => true
                            ],
                            [
                                'name' => 'Laki-laki',
                                'y' => $lakiLaki
                            ],
                        ]
                    ]]
                ],
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting',
                    'themes/grid'
                ],
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'pie'
                    ],
                    'title' => [
                        'text' => Yii::t('frontend', 'Jumlah karyawan berdasarkan Bisnis Unit')
                    ],
                    'tooltip' => [
                        'pointFormat' => '{series.name}: <b>{point.y}</b>'
                    ],
                    'plotOptions' => [
                        'pie' => [
                            'allowPointSelect' => true,
                            'cursor' => 'pointer',
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '<b>{point.name}</b>: {point.y}'
                            ],
                            'showInLegend' => true
                        ]
                    ],
                    'series' => [
                        [
                            'name' => 'Brands',
                            'colorByPoint' => true,
                            'data' => $pegawaiPerJenisKontrak
                        ]
                    ]
                ],
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting',
                    'themes/grid'
                ],
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'pie'
                    ],
                    'title' => [
                        'text' => Yii::t('frontend', 'Jumlah karyawan berdasarkan Golongan')
                    ],
                    'tooltip' => [
                        'pointFormat' => '{series.name}: <b>{point.y}</b>'
                    ],
                    'plotOptions' => [
                        'pie' => [
                            'allowPointSelect' => true,
                            'cursor' => 'pointer',
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '<b>{point.name}</b>: {point.y}'
                            ],
                            'showInLegend' => true
                        ]
                    ],
                    'series' => [
                        [
                            'name' => 'Brands',
                            'colorByPoint' => true,
                            'data' => $pegawaiPerGolongan
                        ]
                    ]
                ],
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting',
                    'themes/grid'
                ],
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>

</div>
<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>
<div class="row">
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'column'
                    ],
                    'title' => [
                        'text' => Yii::t('frontend', 'Jumlah karyawan berdasarkan Jenis Kontrak')
                    ],
                    'tooltip' => [
                        'pointFormat' => '{series.name}: <b>{point.y}</b>'
                    ],
                    'xAxis' => [
                        'type' => 'category'
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => Yii::t('frontend', 'Jumlah karyawan')
                        ]
                    ],
                    'plotOptions' => [
                        'series' => [
                            'borderWidth' => 0,
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y}'
                            ]
                        ]
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('frontend', 'Jenis Kontrak'),
                            'data' => $pegawaiPerBusinesUnit
                        ]
                    ]
                ],
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting',
                    'themes/grid'
                ],
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'column'
                    ],
                    'title' => [
                        'text' => Yii::t('frontend', 'Kontrak Yg Berakhir pada {tanggal}', ['tanggal' => date('m-Y', strtotime('+ 2 months'))])
                    ],
                    'tooltip' => [
                        'pointFormat' => '{series.name}: <b>{point.y}</b>'
                    ],
                    'xAxis' => [
                        'type' => 'category'
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => Yii::t('frontend', 'Jumlah karyawan')
                        ]
                    ],
                    'plotOptions' => [
                        'series' => [
                            'borderWidth' => 0,
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y}'
                            ]
                        ]
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('frontend', 'Jenis Kontrak'),
                            'data' => $dsAkhirKontrak
                        ]
                    ]
                ],
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting',
                    'themes/grid'
                ],
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'column',
                    ],
                    'plotOptions' => [
                        'series' => [
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y}'
                            ],
                            'cursor' => 'pointer',
                            'point' => [
                                'events' => [
                                    'click' => new JsExpression('function () { tampilkanNama(this);}')
                                ]
                            ]
                        ]
                    ],
                    'title' => ['text' => Yii::t('frontend', 'Status Kehadiran')],
                    'xAxis' => [
                        'categories' => array_values($dashboardAbsensi['departemen'])
                    ],
                    'yAxis' => [
                        'title' => ['text' => Yii::t('frontend', 'Jumlah Karyawan')]
                    ],
                    'series' => array_values($dashboardAbsensi['status_kehadiran'])
                ]
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>
<div class="row">
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'column',
                    ],
                    'plotOptions' => [
                        'series' => [
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y}'
                            ],
                            'cursor' => 'pointer',
                            'point' => [
                                'events' => [
//                                    'click' => new JsExpression('function () { tampilkanNama(this);}')
                                ]
                            ]
                        ]
                    ],
                    'title' => ['text' => Yii::t('frontend', 'Surat Peringatan')],
                    'xAxis' => [
                        'categories' => $dsSuratPeringatan['categories'],
                        'crosshair' => true
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('frontend', 'Jumlah Karyawan')
                        ]
                    ],
                    'series' => $dsSuratPeringatan['series']
                ]
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'column'
                    ],
                    'title' => [
                        'text' => Yii::t('frontend', 'Jumlah karyawan Per Departemen')
                    ],
                    'tooltip' => [
                        'pointFormat' => '{series.name}: <b>{point.y}</b>'
                    ],
                    'xAxis' => [
                        'type' => 'category'
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => Yii::t('frontend', 'Jumlah karyawan')
                        ]
                    ],
                    'plotOptions' => [
                        'series' => [
                            'borderWidth' => 0,
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y}'
                            ]
                        ]
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('frontend', 'Departement'),
                            'data' => $dsJumlahPegawaiPerdepartemen
                        ]
                    ]
                ],
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting',
                    'themes/grid'
                ],
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'line'
                    ],
                    'title' => [
                        'text' => Yii::t('frontend', 'Turn Over Rate')
                    ],
                    'tooltip' => [
                        'pointFormat' => '{series.name}: <b>{point.y}</b>'
                    ],
                    'xAxis' => [
                        'type' => 'category'
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => Yii::t('frontend', 'Jumlah karyawan(%)')
                        ]
                    ],
                    'plotOptions' => [
                        'series' => [
                            'borderWidth' => 0,
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y}'
                            ]
                        ]
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('frontend', 'Bulan'),
                            'data' => $dsTurnOver
                        ]
                    ]
                ],
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting',
                    'themes/grid'
                ],
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">&nbsp;</div>
</div>
<div class="row">
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'column',
                    ],
                    'plotOptions' => [
                        'series' => [
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y}'
                            ],
                            'cursor' => 'pointer',
                        ]
                    ],
                    'title' => ['text' => Yii::t('frontend', 'Masuk/Resign')],
                    'xAxis' => [
                        'categories' => $dsInOut['categories'],
                        'crosshair' => true
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('frontend', 'Jumlah Karyawan')
                        ]
                    ],
                    'series' => $dsInOut['series']
                ]
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
    <div class="col-md-4">
        <?php
        try {
            echo Highcharts::widget([
                'options' => [
                    'chart' => [
                        'plotBackgroundColor' => null,
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'type' => 'column',
                    ],
                    'plotOptions' => [
                        'series' => [
                            'dataLabels' => [
                                'enabled' => true,
                                'format' => '{point.y}'
                            ],
                            'cursor' => 'pointer',
                            'point' => [
                                'events' => [
//                                    'click' => new JsExpression('function () { tampilkanNama(this);}')
                                ]
                            ]
                        ]
                    ],
                    'title' => ['text' => Yii::t('frontend', 'Range Usia')],
//                    'xAxis' => [
//                        'categories' => $dsRangeAge['categories'],
//                        'crosshair' => true
//                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('frontend', 'Jumlah Karyawan')
                        ]
                    ],
                    'series' => $dsRangeAge['series']
                ]
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>
    </div>
</div>
<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\CostCenterTemplateSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\components\Status;
use common\models\CostCenterTemplate;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('frontend', 'Cost Center Template');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function ($) {
    $('.btn-add-or-update-cost-center-template').click(function(e){
        e.preventDefault();
        var idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $("#kv-pjax-container-cost-center-template").on("pjax:success", function() {
        $('.btn-add-or-update-cost-center-template').click(function(e){
            e.preventDefault();
            var idModal = $('#form-modal');
            idModal.find('.modal-title').text($(this).data('header'));
            idModal.modal('show')
               .find('#modelContent')
               .load($(this).attr('href'));
            return false;
        });
    });
    
    $('.change-aktif-status-btn').click(function(e) {
        e.preventDefault();
        let btn = $(this);
        krajeeDialog.confirm(btn.data('confirm'), function(result) {
            if(result){
                btn.button('loading');
                $.post(btn.attr('href'))
                .done(function (dt) {
                    console.log(dt);
                    if(dt.status === true){
                        location.href = dt.data.url;
                    }
                    btn.button('reset');
                });
            }else{
                btn.button('reset');
            }
        });
        return false;
    });
});
JS;

$this->registerJs($js);

?>
    <div class="cost-center-template-index">
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'kode',
            'area_kerja',
            'keterangan:ntext',
            [
                'attribute' => 'is_aktif',
                'value' => function (CostCenterTemplate $model) {
                    if ($model->is_aktif) {
                        return Html::a(Status::activeInactive(Status::YA), ['change-aktif-status', 'id' => $model->id, 'status' => Status::TIDAK], ['class' => 'btn btn-success btn-flat btn-xs btn-block change-aktif-status-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merubah status <strong>{costcenter}</strong> ?', ['costcenter' => $model->getKodeArea()])]]);
                    }
                    return Html::a(Status::activeInactive(Status::TIDAK), ['change-aktif-status', 'id' => $model->id, 'status' => Status::YA], ['class' => 'btn btn-warning btn-flat btn-xs btn-block change-aktif-status-btn', 'data' => ['confirm' => Yii::t('frontend', 'Anda yakin akan merubah status <strong>{costcenter}</strong> ?', ['costcenter' => $model->getKodeArea()])]]);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Status::activeInactive(0, true),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => $searchModel->getAttributeLabel('is_aktif'), 'id' => 'grid-pegawai-search-is_aktif'],
                'format' => 'raw'
            ],
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-primary btn-flat btn-add-or-update-cost-center-template', 'data' => ['header' => Yii::t('frontend', 'Update Cost Center')]]);
                    },
                    'delete' => function ($url) {
                        return Html::a('Delete', $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                    }
                ],
            ],
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'selectedColumns' => [1, 2, 3, 4],
                'noExportColumns' => [1, 5], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'cost-center-template' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-cost-center-template']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat btn-add-or-update-cost-center-template', 'data' => ['header' => Yii::t('frontend', 'Tambah Cost Center')]]) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
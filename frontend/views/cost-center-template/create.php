<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CostCenterTemplate */

$this->title = Yii::t('frontend', 'Create Cost Center Template');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Cost Center Template'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-center-template-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

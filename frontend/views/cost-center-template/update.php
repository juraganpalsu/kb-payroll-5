<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CostCenterTemplate */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
        'modelClass' => 'Cost Center Template',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Cost Center Template'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="cost-center-template-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\CostCenterTemplate */
/* @var $form kartik\widgets\ActiveForm */

$js = <<< JS
$(function() {
    $('#form-cost-center-template').on('beforeSubmit', function (event) {
        event.preventDefault();
        var form = $(this);
        var btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]);

?>

<div class="cost-center-template-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-cost-center-template',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>
    <?= $form->errorSummary($model); ?>

    <?php
    try {
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (InvalidConfigException $e) {
    } ?>

    <div class="row">
        <div class="col-sm-5">
            <?php try {
                echo $form->field($model, 'kode')->textInput(['placeholder' => 'Kode']);
            } catch (InvalidConfigException $e) {
            } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <?php try {
                echo $form->field($model, 'area_kerja')->textInput(['maxlength' => true, 'placeholder' => 'Area Kerja']);
            } catch (InvalidConfigException $e) {
            } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <?php try {
                echo $form->field($model, 'keterangan')->textarea(['rows' => 6]);
            } catch (InvalidConfigException $e) {
            } ?>
        </div>
    </div>

    <?php try {
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (InvalidConfigException $e) {
    } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
/* @var $this \yii\web\View */

/* @var $content string */

use mdm\admin\components\Helper;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
                'innerContainerOptions' => ['style' => 'width: 100%']
            ]);
            $menuItems = [
//        ['label' => 'Home', 'url' => ['/site/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']],
            ];
            if (Yii::$app->user->isGuest) {
//        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => Yii::t('app', 'Proses'),
                    'items' => [
                        ['label' => Yii::t('app', 'Absensi'), 'url' => ['/absensi']],
                        ['label' => Yii::t('app', 'Attendance'), 'url' => ['/attendance']],
                    ]
                ];
                $menuItems[] = [
                    'label' => Yii::t('app', 'Konfigurasi'),
                    'items' => [
                        ['label' => Yii::t('app', 'Pegawai'), 'url' => ['/pegawai']],
                        ['label' => Yii::t('app', 'Time Table'), 'url' => ['/time-table']],
                        ['label' => Yii::t('app', 'SPL'), 'url' => ['/spl']],
                        ['label' => Yii::t('app', 'SPL Template'), 'url' => ['/spl-template']],
                        ['label' => Yii::t('app', 'Libur Pegawai'), 'url' => ['/libur-pegawai']],
                        ['label' => Yii::t('app', 'Libur'), 'url' => ['/libur']],
                        ['label' => Yii::t('app', 'Ijin'), 'url' => ['/ijin']]
                    ]
                ];
                $menuItems[] = [
                    'label' => Yii::t('app', 'Admin'), 'visible' => Yii::$app->user->can('/admin/route/index'),
                    'items' => [
                        ['label' => Yii::t('app', 'Route'), 'url' => ['/admin/route']],
                        ['label' => Yii::t('app', 'Role'), 'url' => ['/admin/role']],
                        ['label' => Yii::t('app', 'Permission'), 'url' => ['/admin/permission']],
                        ['label' => Yii::t('app', 'Assigmnent'), 'url' => ['/admin/assignment']],
                        ['label' => Yii::t('app', 'User'), 'url' => ['/user']]
                    ]
                ];
                $menuItems[] = [
                    'label' => '(' . Yii::$app->user->identity->username . ')',
                    'items' => [
                        ['label' => Yii::t('app', 'Akun'), 'url' => ['/user/my-account'], 'visible' => Helper::checkRoute('/user/my-account')],
                        [
                            'label' => Yii::t('app', 'Logout'),
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']
                        ],
                    ]
                ];
            }
            try {
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => $menuItems,
                ]);
            } catch (Exception $e) {
                
            }
            NavBar::end();
            ?>

            <div class="container" style="width: 100%">
                <?php
                try {
                    echo Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]);
                } catch (Exception $e) {
                    
                }
                ?>
<?= Alert::widget() ?>
<?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container" style="width: 100%">
                <p class="pull-left">&copy; <?= Yii::$app->name . ' ' . date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

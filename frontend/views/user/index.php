<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'email:email',

            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    $options = ['class' => 'btn btn-xs btn-success btn-block'];
                    $teks = Yii::t('app', 'Un Block');
                    if ($model->status == User::STATUS_ACTIVE) {
                        $teks = Yii::t('app', 'Block');
                        Html::removeCssClass($options, 'btn btn-xs btn-danger btn-block');
                        Html::addCssClass($options, 'btn btn-xs btn-danger btn-block');
                    }
                    return Html::a($teks, ['block', 'id' => $model->id], array_merge($options, ['data' => ['method' => 'post', 'confirm' => 'Are you sure?']]));
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
</div>

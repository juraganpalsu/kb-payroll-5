<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\TimeTable */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Time Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-table-view">

    <div class="row">
        <div class="col-sm-12">

            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat pull-right']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat pull-right',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $gridColumn = [
                'nama',
                'shift_',
                'masuk_minimum',
                'masuk',
                'masuk_maksimum',
                'toleransi_masuk',
                'pulang_minimum',
                'pulang',
                'pulang_maksimum',
                'toleransi_pulang'
            ];
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
            ?>
        </div>
    </div>
</div>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TimeTable */
/** @var $dataHari array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Time Table',
    ]) . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Time Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="time-table-update">

    <?= $this->render('_form', [
        'dataHari' => $dataHari,
        'model' => $model,
    ]) ?>

</div>

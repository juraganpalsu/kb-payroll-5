<?php

use common\models\TimeTable;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\TimeTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Time Tables');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="time-table-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'nama',
        [
            'attribute' => 'shift_search',
            'label' => Yii::t('app', 'Shift'),
            'width' => '10%',
            'value' => function (TimeTable $model) {
                return $model->shift_;
            },
            'filter' => $searchModel->_shift,
        ],
        'masuk_minimum',
        'masuk',
        'masuk_maksimum',
        'toleransi_masuk',
        'pulang_minimum',
        'pulang',
        'pulang_maksimum',
        'toleransi_pulang',
        ['attribute' => 'lock', 'hidden' => true],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?php try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-time-table']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                '{export}',
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>


</div>

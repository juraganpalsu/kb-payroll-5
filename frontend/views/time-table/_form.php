<?php

use kartik\widgets\TimePicker;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TimeTable */
/* @var $form kartik\widgets\ActiveForm */
/** @var $dataHari array */

/** @var $dataHari array */
?>

<div class="time-table-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?php try { ?>

        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'shift')->dropDownList($model->_shift) ?>
            </div>
        </div>

        <div class=" row">
            <div class=" col-md-3">
                <?=
                $form->field($model, 'masuk_minimum')->widget(TimePicker::class, [
                    'pluginOptions' => [
                        'showSeconds' => true,
                        'showMeridian' => false,
                        'minuteStep' => 5,
                        'secondStep' => 30,
                        'defaultTime' => false,
                        'modalBackdrop' => true,
                    ]
                ]);
                ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'masuk')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'masuk_maksimum')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-2">
                <?= $form->field($model, 'toleransi_masuk', ['addon' => ['append' => ['content' => 'menit']]])->textInput(['type' => 'number']); ?>
            </div>
        </div>

        <div class=" row">
            <div class=" col-md-3">
                <?= $form->field($model, 'pulang_minimum')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'pulang')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-3">
                <?= $form->field($model, 'pulang_maksimum')->widget(TimePicker::class, ['pluginOptions' => ['showSeconds' => true, 'showMeridian' => false, 'minuteStep' => 5, 'secondStep' => 30, 'defaultTime' => false]]); ?>
            </div>
            <div class=" col-md-2">
                <?= $form->field($model, 'toleransi_pulang', ['addon' => ['append' => ['content' => 'menit']]])->textInput(['type' => 'number']); ?>
            </div>
        </div>


        <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-danger btn-flat']) ?>
        </div>

    <?php } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TimeTable */
/** @var $dataHari array */

$this->title = Yii::t('app', 'Create Time Table');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Time Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-table-create">

    <?= $this->render('_form', [
        'dataHari' => $dataHari,
        'model' => $model,
    ]) ?>

</div>

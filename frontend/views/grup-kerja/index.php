<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\GrupKerjaSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\Divisi;
use common\models\GrupKerja;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Grup Kerja';
?>
<div class="grup-kerja-index">

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'unit_id',
            'label' => 'Unit',
            'value' => function(GrupKerja $model){
                $divisi = $model->divisi;
                return isset($divisi->unit->nama) ? $divisi->unit->nama : '-';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Unit::find()->asArray()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Unit', 'id' => 'grid-grup-kerja-search-unit_id']
        ],
        [
            'attribute' => 'divisi_id',
            'label' => 'Divisi',
            'value' => function (GrupKerja $model) {
                return isset($model->divisi->nama) ? $model->divisi->nama : '-';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Divisi::find()->asArray()->all(), 'id', 'nama'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Divisi', 'id' => 'grid-grup-kerja-search-divisi_id']
        ],
        'nama',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}'
        ],
    ];
    ?>
    <?php try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumn,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-grup-kerja']],
            'export' => [
                'fontAwesome' => true,
            ],
            'toolbar' => [
                [
                    'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'title' => Yii::t('frontend', 'Create'), 'class' => 'btn btn-success btn-flat']) . ' ' .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default btn-flat', 'title' => Yii::t('frontend', 'Reset Grid')])
                ],
                '{export}',
            ],
            'panel' => [
                'heading' => false,
                'before' => '{summary}',
            ],
            'striped' => true,
            'responsive' => true,
            'hover' => true,
        ]);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

</div>

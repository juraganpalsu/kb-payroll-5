<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GrupKerja */

$this->title = 'Update Grup Kerja: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Grup Kerja', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="grup-kerja-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\GrupKerja */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Grup Kerja', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grup-kerja-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Grup Kerja'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'divisi.id',
            'label' => 'Divisi',
        ],
        'nama',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Divisi<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnDivisi = [
        ['attribute' => 'id', 'visible' => false],
        'unit_id',
        'nama',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->divisi,
        'attributes' => $gridColumnDivisi    ]);
    ?>
</div>

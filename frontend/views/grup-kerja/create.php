<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GrupKerja */

$this->title = 'Create Grup Kerja';
$this->params['breadcrumbs'][] = ['label' => 'Grup Kerja', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grup-kerja-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

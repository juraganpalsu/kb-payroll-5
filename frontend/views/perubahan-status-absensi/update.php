<?php

use common\models\Absensi;

/* @var $this yii\web\View */
/**
 * @var $model common\models\PerubahanStatusAbsensi
 * @var $modelAbsensi Absensi
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Perubahan Status Absensi',
    ]) . ' ' . date('d-m-Y', strtotime($model->tanggal)) . ' - ' . $model->keterangan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perubahan Status Absensi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="perubahan-status-absensi-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelAbsensi' => $modelAbsensi
    ]) ?>

    <br>

</div>

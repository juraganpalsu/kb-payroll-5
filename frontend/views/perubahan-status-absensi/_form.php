<?php

use common\models\Absensi;
use kartik\datecontrol\DateControl;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\PerubahanStatusAbsensi */
/**
 * @var $form yii\widgets\ActiveForm
 *
 * @var $modelAbsensi Absensi
 */


$js = <<< JS
$(function() {
    $('#form-perubahan-absensi').on('beforeSubmit', function (event) {
        event.preventDefault();
        let form = $(this);
        let btn = $('.submit-btn');
        btn.button('loading');
        if (form.find('.form-group.has-error').length) {
            btn.button('reset');
            return false;
        }
        btn.button('loading');
        $.post(form.attr('action'), form.serialize()).done(function(dt) {
            console.log(dt);
            if(dt.status === true){
                location.href = dt.data.url;
            }
            btn.button('reset');
        });
        return false;
    });
})
JS;

$this->registerJs($js);

$action = $model->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->id]);

?>

<div class="perubahan-status-absensi-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-perubahan-absensi',
        'action' => $action,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['showErrors' => false],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['create-or-update-validation']),
        'fieldConfig' => ['showLabels' => true],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?php try {
        echo $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'tanggal')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('app', 'Choose Tanggal'),
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'masuk')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_TIME,
                    'saveFormat' => 'php:H:i:s',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('app', 'Choose Masuk'),
                            'autoclose' => true
                        ]
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'tanggal_pulang')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('app', 'Choose Tanggal Pulang'),
                            'autoclose' => true
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'pulang')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_TIME,
                    'saveFormat' => 'php:H:i:s',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => Yii::t('app', 'Choose Pulang'),
                            'autoclose' => true
                        ]
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'pegawai_ids')->widget(Select2::class, [
                    'options' => [
                        'placeholder' => Yii::t('frontend', '-- Pilih Pegawai'),
                        'multiple' => false,
                    ],
                    'data' => $model->pegawai_exist,
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/pegawai/pegawai/get-pegawai-berdasar-tanggal-for-select2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, tanggal:$("#perubahanstatusabsensi-tanggal").val()};}')
                        ],
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">


        </div>

        <div class="row">
            <div class="col-sm-6">
                <?php echo $form->field($model, 'status_kehadiran')->dropDownList(array_diff_key($modelAbsensi->_status_kehadiran, [Absensi::IJIN => '', Absensi::CUTI => '', Absensi::ERROR => '']), ['placeholder' => $model->getAttributeLabel('status_kehadiran')]); ?>
            </div>
            <div class="col-md-6">
                <?php
                echo $form->field($model, 'time_table_id')->widget(DepDrop::class, [
                    'data' => [],
                    'pluginOptions' => [
                        'depends' => ['perubahanstatusabsensi-pegawai_ids'],
                        'placeholder' => 'Select...',
                        'url' => Url::to(['/pegawai/pegawai/get-time-table-depdrop'])
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?php echo $form->field($model, 'jumlah_jam_lembur')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('jumlah_jam_lembur')]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php echo $form->field($model, 'keterangan')->textarea(['rows' => 6]); ?>
            </div>
        </div>

        <?php
        echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);
    } catch (Exception $e) {
        Yii::info($e->getMessage(), 'exception');
    } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

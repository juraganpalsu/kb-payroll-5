<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PerubahanStatusAbsensiSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

/**
 * @var $click string
 * @var $modelAbsensi Absensi
 */

use common\models\PerubahanStatusAbsensi;
use frontend\models\Absensi;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Perubahan Status Absensi');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$(function ($) {
    $('$click').click();
    
    $('.btn-add-or-update-perubahan-absensi').click(function(e){
        e.preventDefault();
        var idModal = $('#form-modal');
        idModal.find('.modal-title').text($(this).data('header'));
        idModal.modal('show')
           .find('#modelContent')
           .load($(this).attr('href'));
        return false;
    });
    
    $("#kv-pjax-container-perubahan-status-absensi").on("pjax:success", function() {
        $('.btn-add-or-update-perubahan-absensi').click(function(e){
            e.preventDefault();
            var idModal = $('#form-modal');
            idModal.find('.modal-title').text($(this).data('header'));
            idModal.modal('show')
               .find('#modelContent')
               .load($(this).attr('href'));
            return false;
        });
    });
});
JS;

$this->registerJs($js);

?>
    <div class="perubahan-status-absensi-index">
        <?php
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'idfp',
                'value' => function (PerubahanStatusAbsensi $model) {
                    return $model->getIdPegawaiUntukTanggal($model->tanggal);
                },
            ],
            [
                'attribute' => 'nama',
                'value' => function (PerubahanStatusAbsensi $model) {
                    return $model->nama;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'tanggal',
                'value' => function (PerubahanStatusAbsensi $model) {
                    return date('d-m-Y', strtotime($model->tanggal));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => ['format' => 'Y/m/d']
                    ]
                ],
            ],
            [
                'attribute' => 'masuk',
                'value' => function (PerubahanStatusAbsensi $model) {
                    return $model->masuk ? $model->masuk : '';
                },
            ],
            [
                'attribute' => 'tanggal_pulang',
                'value' => function (PerubahanStatusAbsensi $model) {
                    return date('d-m-Y', strtotime($model->tanggal_pulang));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'name' => 'range_tanggal',
                    'value' => '',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => ['format' => 'Y/m/d']
                    ]
                ],
            ],
            [
                'attribute' => 'pulang',
                'value' => function (PerubahanStatusAbsensi $model) {
                    return $model->pulang ? $model->pulang : '';
                },
            ],
            [
                'attribute' => 'status_kehadiran',
                'value' => function (PerubahanStatusAbsensi $model) use ($modelAbsensi) {
                    return $modelAbsensi->_status_kehadiran[$model->status_kehadiran];
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelAbsensi->_status_kehadiran,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => Yii::t('frontend', 'Status Kehadiran'), 'id' => 'grid-perubahan-status-absensi-search-status_kehadiran']
            ],
            [
                'attribute' => 'time_table_id',
                'value' => function (PerubahanStatusAbsensi $model) {
                    if ($model->timeTable) {
                        return $model->timeTable->nama;
                    }
                    return '';
                }
            ],
            'jumlah_jam_lembur',
            'keterangan:ntext',
            [
                'attribute' => 'created_by',
                'value' => function (PerubahanStatusAbsensi $model) {
                    $createdBy = $model->createdBy->userPegawai;
                    $nama = $createdBy ? $createdBy->pegawai->nama : '';
                    $namaTampil = explode(' ', $nama);
                    return $namaTampil[0];
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function (PerubahanStatusAbsensi $model) {
                    return date('d-m-Y H:m', strtotime($model->created_at));
                },
            ],
            ['attribute' => 'lock', 'visible' => false],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(Yii::t('frontend', 'Update'), $url, ['class' => 'btn btn-xs btn-primary btn-flat btn-add-or-update-perubahan-absensi', 'data' => ['header' => Yii::t('frontend', 'Update Perubahan Absensi')]]);
                    },
                    'delete' => function ($url) {
                        return Html::a('Delete', $url, ['class' => 'btn btn-xs btn-danger btn-flat', 'data' => ['confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'), 'method' => 'post']]);

                    }
                ],
            ],
        ];
        ?>
        <?php
        try {
            $export = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
//                'selectedColumns' => [2, 3, 4, 5, 6, 7, 8, 9, 10],
                'noExportColumns' => [1], //id dan lock
                'dropdownOptions' => [
                    'class' => 'btn btn-secondary'
                ],
                'filename' => 'perubahan-absensi' . date('dmYHis')
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-perubahan-status-absensi']],
                'export' => [
                    'fontAwesome' => true,
                ],
                'toolbar' => [
                    [
                       'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                                'data' => ['pjax' => 0, 'header' => Yii::t('frontend', 'Buat Perubahan Absensi')],
                                'title' => Yii::t('frontend', 'Create'),
                                'class' => 'btn btn-success btn-flat btn-add-or-update-perubahan-absensi']) . ' ' .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                                'data' => ['pjax' => 0],
                                'title' => Yii::t('frontend', 'Reset Grid'),
                                'class' => 'btn btn-default btn-flat'])
                    ],
                    [
                        'content' => $export
                    ],
                ],
                'panel' => [
                    'heading' => false,
                    'before' => '{summary}',
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        ?>

    </div>

<?php
Modal::begin([
    'options' => [
        'id' => 'form-modal',
        'tabindex' => false
    ],
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4 class="modal-title text-primary"></h4>',
]);
?>
    <div id='modelContent' class="well"></div>
<?php
Modal::end();
?>
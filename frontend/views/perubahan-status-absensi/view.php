<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\PerubahanStatusAbsensi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perubahan Status Absensi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perubahan-status-absensi-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Perubahan Status Absensi').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'urut',
        'tanggal',
        'masuk',
        'pulang',
        'status_kehadiran',
        'jumlah_jam_lembur',
        'keterangan:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerPerubahanStatusAbsensiDetail->totalCount){
    $gridColumnPerubahanStatusAbsensiDetail = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'urut',
                        [
                'attribute' => 'pegawai.id',
                'label' => Yii::t('app', 'Pegawai')
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPerubahanStatusAbsensiDetail,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-perubahan-status-absensi-detail']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Perubahan Status Absensi Detail')),
        ],
        'export' => false,
        'columns' => $gridColumnPerubahanStatusAbsensiDetail
    ]);
}
?>

    </div>
</div>

<?php

use common\models\Absensi;

/* @var $this yii\web\View */
/**
 * @var $model common\models\PerubahanStatusAbsensi
 * @var $modelAbsensi Absensi
 */


$this->title = Yii::t('app', 'Create Perubahan Status Absensi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perubahan Status Absensi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perubahan-status-absensi-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelAbsensi' => $modelAbsensi
    ]) ?>
    <hr>

</div>

<?php


namespace backend\modules\v1\controllers;


use dektrium\user\Finder;
use dektrium\user\models\LoginForm;
use frontend\modules\pegawai\models\UserPegawai;
use mdm\admin\components\AccessControl;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * File Name: SecurityController.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/17/2021
 * Time: 11:52 AM
 */
class SecurityController extends Controller
{

    public $modelClass = 'frontend\modules\v1\models\User';

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];
        return $behaviors;
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function actionValidate()
    {
        if (Yii::$app->request->isPost) {
            $loginForm = [
                'login-form' => [
                    'login' => Yii::$app->request->post('username'),
                    'password' => Yii::$app->request->post('password'),
                    'rememberMe' => 0
                ]
            ];
            $model = Yii::createObject(LoginForm::class);
            if ($model->load($loginForm) && $model->validate()) {
                $finder = Yii::createObject(Finder::class);
                if ($modelUser = $finder->findUserByUsernameOrEmail($model->login)) {
                    if ($userPegawai = UserPegawai::findOne(['user_id' => $modelUser->id])) {
                        $namaDepartemen = '';
                        $namaJabatan = '';
                        $namaStruktur = '';
                        if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                            if ($struktur = $pegawaiStruktur->struktur) {
                                if ($departemen = $struktur->departemen) {
                                    $namaDepartemen = $departemen->nama;
                                }
                                if ($jabatan = $struktur->jabatan) {
                                    $namaJabatan = $jabatan->nama;
                                }
                                $namaStruktur = $struktur->nameCostume;
                            }
                        }
                        $data = [
                            'username' => $modelUser->username,
                            'employeename' => $userPegawai->pegawai->nama,
                            'token' => $modelUser->auth_key,
                            'noktp' => $userPegawai->pegawai->nomor_ktp,
                            'departement' => $namaDepartemen,
                            'jabatan' => $namaJabatan,
                            'struktur' => $namaStruktur,
                        ];
                        return ['status' => true, 'message' => Yii::t('backend', 'Login Berhasil'), 'data' => $data];
                    }
                }
            }
        }
        return ['status' => false, 'message' => Yii::t('backend', 'Gagal Login.'), 'data' => []];
    }


}
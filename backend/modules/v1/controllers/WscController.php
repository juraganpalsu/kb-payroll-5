<?php


namespace backend\modules\v1\controllers;


use backend\controllers\WscSController;
use backend\modules\v1\models\Wsc;
use frontend\modules\pegawai\models\UserPegawai;
use Yii;

/**
 * Created by PhpStorm.
 * File Name: WscController.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/24/2021
 * Time: 23:34 AM
 */
class WscController extends WscSController
{

    /**
     * @return Wsc
     */
    protected static function newModel(): Wsc
    {
        return new Wsc();
    }

    /**
     * @return array
     */
    public function actionStartWsc()
    {
        if (Yii::$app->request->isPost) {
            $date = Yii::$app->request->post('date');
            if ($userPegawai = UserPegawai::findOne(['user_id' => Yii::$app->user->id])) {
                if (!empty($date) && $modelPegawai = $userPegawai->pegawai) {
                    return self::newModel()->startWsc($modelPegawai, $date);
                }
                return ['status' => false, 'message' => Yii::t('backend', 'Invalid Data'), 'data' => [$date]];
            }
            return ['status' => false, 'message' => Yii::t('backend', 'No User Linked'), 'data' => []];
        }
        return ['status' => false, 'message' => Yii::t('backend', 'Can\'t saving data'), 'data' => []];
    }

}
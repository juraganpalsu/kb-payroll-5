<?php


namespace backend\modules\v1\controllers;


use backend\modules\v1\models\Pegawai;
use mdm\admin\components\AccessControl;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * File Name: PegawaiController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 21/05/20
 * Time: 17.39
 */
class PegawaiController extends Controller
{

    public $modelClass = 'backend\modules\v1\models\Pegawai';

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];
        return $behaviors;
    }

    /**
     * @return Pegawai
     */
    public static function model(): Pegawai
    {
        return new Pegawai();
    }

    /**
     *
     * http://{dns}/v1/pegawai/terbaru
     *
     * @return array
     */
    public function actionDownload(int $jumlah = 10)
    {
        $model = new Pegawai();
        $downloadPegawai = $model->downloadPegawai($jumlah);
        if (!empty($downloadPegawai)) {
            return ['status' => true, 'message' => Yii::t('frontend', 'Data Pegawai'), 'data' => $downloadPegawai];
        }
        return ['status' => false, 'message' => Yii::t('frontend', 'Tidak ada data baru'), 'data' => []];
    }


    /**
     * @param int $noktp
     * @return array
     */
    public function actionInfoPegawai(int $noktp)
    {
        $model = Pegawai::findOne(['nomor_ktp' => $noktp]);
        if ($model) {
            $dataPegawai = self::model()->checkPegawaiIsActive($model->id);
            if ($dataPegawai) {
                return ['status' => true, 'message' => Yii::t('frontend', 'Pegawai Aktif'), 'data' => ['status' => true, 'nama' => $model->nama]];
            }
            return ['status' => true, 'message' => Yii::t('frontend', 'Pegawai Tidak Aktif'), 'data' => ['status' => false, 'nama' => $model->nama]];
        }
        return ['status' => false, 'message' => Yii::t('frontend', 'Data tidak ditemukan'), 'data' => []];
    }


}
<?php

namespace backend\modules\v1\models;

use yii\db\ActiveRecord;

/**
 * Created by PhpStorm.
 * File Name: Pegawai.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 21/05/20
 * Time: 17.40
 */
class Pegawai extends \frontend\modules\pegawai\models\Pegawai
{
    const UNDOWNLOADED = 0;
    const DOWNLOADED = 1;

    /**
     * @param int $limit
     * @return array
     */
    public function downloadPegawai(int $limit = 10)
    {
        $query = Pegawai::find()->andWhere(['has_downloadded_to_training_app' => Pegawai::UNDOWNLOADED])
            ->limit($limit)
            ->orderBy('created_at DESC')
            ->all();
        $dataPegawai = [];
        if ($query) {
            /** @var Pegawai $pegawai */
            foreach ($query as $pegawai) {
                $id_pegawai = '';
                $bisnisUnit = '';
                $golongan = '';
                $departemen = '';
                if ($perjanjianKerja = $pegawai->perjanjianKerja) {
                    $bisnisUnit = $perjanjianKerja->namaKontrak;
                    $id_pegawai = $perjanjianKerja->id_pegawai;
                }
                if ($pegawaiGolongan = $pegawai->pegawaiGolongan) {
                    $golongan = $pegawaiGolongan->golongan->nama;
                }
                if ($pegawaiStruktur = $pegawai->pegawaiStruktur) {
                    if ($struktur = $pegawaiStruktur->struktur) {
                        if ($departemenModel = $struktur->departemen) {
                            $departemen = $departemenModel->nama;
                        }
                    }
                }
                $dataPegawai[] = [
                    'id' => $pegawai->id,
                    'id_pegawai' => $id_pegawai,
                    'nama' => $pegawai->nama_lengkap,
                    'bisnis_unit' => $bisnisUnit,
                    'departemen' => $departemen,
                    'golongan' => $golongan
                ];
            }
        }
        return $dataPegawai;
    }

    /**
     * @param string $id
     * @return array|ActiveRecord|null
     */
    public function checkPegawaiIsActive(string $id)
    {
        return self::find()
            ->joinWith('perjanjianKerja')
            ->andWhere(['pegawai.id' => $id])
            ->andWhere(['OR', ['perjanjian_kerja.tanggal_resign' => null], ['>', 'perjanjian_kerja.mulai_berlaku', date('Y-m-d')]])
            ->one();
    }
}
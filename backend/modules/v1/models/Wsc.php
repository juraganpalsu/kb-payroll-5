<?php


namespace backend\modules\v1\models;


use Yii;

/**
 * Created by PhpStorm.
 * File Name: Wsc.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/24/2021
 * Time: 23:39 AM
 */
class Wsc extends \backend\models\Wsc
{

    /**
     * @param \frontend\modules\pegawai\models\Pegawai $modelPegawai
     * @param string $date
     * @return array
     */
    public function startWsc(\frontend\modules\pegawai\models\Pegawai $modelPegawai, string $date)
    {
        $return = ['status' => false, 'message' => Yii::t('backend', 'There s no WSC Schedule'), 'data' => []];
        $model = self::findOne(['pegawai_id' => $modelPegawai->id, 'tanggal' => $date]);
        if ($model) {
            if ($model->is_started) {
                $return = ['status' => true, 'message' => Yii::t('backend', 'Already Started'), 'data' => ['keterangan' => $model->keterangan]];
            } else {
                $model->is_started = true;
                if ($model->save(false)) {
                    $return = ['status' => true, 'message' => Yii::t('backend', 'Starting WSC'), 'data' => ['keterangan' => $model->keterangan]];
                } else {
                    $return = ['status' => false, 'message' => Yii::t('backend', 'Error Occurred, Please Contact Administrator'), 'data' => []];
                }
            }
        }
        return $return;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 12/12/17
 * Time: 11:08 AM
 */

namespace backend\controllers;


use backend\models\Attendance;
use mdm\admin\components\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class AttendanceBackController extends Controller
{
    /**
     * @var $modelClass Attendance
     */
//    public $modelClass = 'backend\models\Attendance';

    /**
     * @return array behavoirs
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];
        return $behaviors;
    }

    /**
     * @return Attendance
     */
    private static function model()
    {
        return new Attendance();
    }


}
<?php


namespace backend\controllers;


use backend\models\WscLocation;
use frontend\modules\pegawai\models\UserPegawai;
use mdm\admin\components\AccessControl;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * File Name: WscSController.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/17/2021
 * Time: 7:00 AM
 */
class WscSController extends Controller
{
    public $modelClass = 'frontend\modules\wsc\models\Wsc';

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];
        return $behaviors;
    }


    /**
     * @param int $noktp
     * @return array
     */
    public function actionCreateLocation(): array
    {
        $model = new WscLocation();
        Yii::info('Starting create data wsc ....', 'create-wsc');
        if (Yii::$app->request->isPost) {
            $wscDatas = Yii::$app->request->post('data');
            if ($userPegawai = UserPegawai::findOne(['user_id' => Yii::$app->user->id])) {
                if (!empty($wscDatas) && is_array($wscDatas) && $modelPegawai = $userPegawai->pegawai) {
                    if ($model->createLocation($modelPegawai, $wscDatas)) {
                        Yii::info(count($wscDatas) . ' data wsc berhasil diinput', 'create-wsc');
                        return ['status' => true, 'message' => Yii::t('backend', 'Terdapat data disimpan'), 'data' => ['yourid' => $wscDatas['id']]];
                    }
                    return ['status' => false, 'message' => Yii::t('backend', 'Tidak ada data disimpan'), 'data' => ['yourid' => $wscDatas['id']]];
                }
            }
        }
        return ['status' => false, 'message' => Yii::t('backend', 'Tidak ada data disimpan'), 'data' => []];

    }

}
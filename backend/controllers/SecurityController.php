<?php


namespace backend\controllers;


/**
 * Created by PhpStorm.
 * File Name: SecurityController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 21/05/20
 * Time: 18.04
 */

use dektrium\user\controllers\SecurityController as BaseSecurityController;
use dektrium\user\Module;
use yii\web\Response;

/**
 * Controller that manages user authentication process.
 *
 * @property Module $module
 *
 */
class SecurityController extends BaseSecurityController
{
    /**
     * @return string|Response
     */
    public function actionLogin()
    {
        return $this->render('@backend/modules/views/security/login');
    }


}
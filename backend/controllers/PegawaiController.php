<?php

/**
 * Created with love at Jan 15, 2018
 * @author jiwa
 */

namespace backend\controllers;

use backend\models\Pegawai;
use mdm\admin\components\AccessControl;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Description of PegawaiController
 *
 */
class PegawaiController extends Controller
{

    public $modelClass = 'frontend\modules\pegawai\models\Pegawai';

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];
        return $behaviors;
    }

    /**
     * @return Pegawai
     */
    public static function model()
    {
        return new Pegawai();
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function actionIndex()
    {
        $query = Pegawai::find();
        $this->model()->load(Yii::$app->request->queryParams);

        $query->andFilterWhere(['like', 'id', $this->model()->id])
            ->andFilterWhere(['like', 'nama', $this->model()->nama]);

        return $query->asArray()->all();
    }

    public function actionTerbaru()
    {

    }

}

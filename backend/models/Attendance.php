<?php

/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 12/20/17
 * Time: 2:20 PM
 */

namespace backend\models;

use frontend\models\Attendance as AttendanceFrontend;
use frontend\models\Pegawai;


class Attendance extends AttendanceFrontend
{
    /**
     * Menyimpan attendance dari api
     * disini validasinya data exist berdasarkan id, krn menggunakan uuid
     * Jika data berhasil disimpan, maka id akan di kembalikan untuk menandai bahwa data telah tersimpan
     *
     * @param array $attendances data attendance yang akan disimpan
     * @return array id yang berhasil disimpan
     */
    public function saveAttendanceFromApi(array $attendances)
    {
        $ids = [];
        $result = ['status' => false, 'data' => '', 'message' => \Yii::t('backend', 'Gagal menyimpan data.')];
        foreach ($attendances as $attendance) {
            $model = new Attendance();
            $model->scenario = 'save-attendance-from-api';
            $model->detachBehavior('uuid');
            $model->setAttributes($attendance);
            if (self::findOne($model->id)) {
                continue;
            }
            $getPegawai = Pegawai::findOne(['idfp' => $model->idfp]);
            if (!$getPegawai) {
                continue;
            }
            $model->pegawai_id = $getPegawai->id;
            if ($model->save()) {
                array_push($ids, $model->id);
            }
        }
        if (!empty($ids)) {
            $result = ['status' => true, 'data' => $ids, 'message' => \Yii::t('backend', 'Berhasil menyimpan data.')];
        }
        return $result;
    }
}
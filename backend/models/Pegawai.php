<?php

/**
 * Created with love at Jan 15, 2018
 * @author jiwa
 */

namespace backend\models;

use yii\data\ActiveDataProvider;

/**
 * Description of Pegawai
 *
 */
class Pegawai extends \frontend\modules\pegawai\models\Pegawai
{

    public function search($params)
    {
        $query = Pegawai::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idfp' => $this->idfp,
            'sistem_kerja' => $this->sistemKerja_,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'nama', $this->nama]);

        return $dataProvider;
    }

}

<?php


namespace backend\models;


/**
 * Created by PhpStorm.
 * File Name: WscLocation.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/17/2021
 * Time: 7:36 AM
 */
class WscLocation extends \frontend\modules\wsc\models\WscLocation
{

    /**
     * @return array
     */
    public function scenarios(): array
    {
        $scenarios = parent::scenarios();
        $scenarios['create-from-service'] = ['latitude', 'longitude', 'time'];
        return $scenarios;
    }


    /**
     * @param \frontend\modules\pegawai\models\Pegawai $modelPegawai
     * @return Wsc|null
     */
    public function getWscSchedule(\frontend\modules\pegawai\models\Pegawai $modelPegawai)
    {
        return Wsc::findOne(['pegawai_id' => $modelPegawai->id, 'tanggal' => date('Y-m-d', strtotime($this->time))]);
    }


    /**
     * @param \frontend\modules\pegawai\models\Pegawai $modelPegawai
     * @param array $attributes
     * @return bool
     */
    public function createLocation(\frontend\modules\pegawai\models\Pegawai $modelPegawai, array $attributes): bool
    {
        $this->scenario = 'create-from-service';
        $this->setAttributes($attributes);
        if ($this->validate() && $wscSchedule = $this->getWscSchedule($modelPegawai)) {
            $this->wsc_id = $wscSchedule->id;
            if (self::findOne(['time' => $this->time, 'wsc_id' => $this->wsc_id])) {
                return false;
            }
            return $this->save();
        }
        return false;
    }

}
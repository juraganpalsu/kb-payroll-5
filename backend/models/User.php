<?php

namespace backend\models;

use dektrium\user\models\User as BaseUser;

/**
 * Created by PhpStorm.
 * File Name: User.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 21/05/20
 * Time: 18.01
 */
class User extends BaseUser
{

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token, 'blocked_at' => null]);
    }
}
<?php


namespace common\components;


use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Created by PhpStorm.
 * File Name: UUIDBehavior.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 15/10/20
 * Time: 21.41
 */
class UUIDBehavior extends Behavior
{
    /**
     * Field/Column yang akan diisi UUID
     * Default -> id
     * @var string
     */
    public $column = 'id';

    /**
     * Override event()
     * memasukkan method beforeSave() kedalam komponen ActiveRecord::EVENT_BEFORE_INSERT
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
        ];
    }

    /**
     * set beforeSave() -> UUID data
     */
    public function beforeSave(): void
    {
        $this->owner->{$this->column} = $this->owner->getDb()->createCommand("SELECT UUID()")->queryScalar();
    }
}
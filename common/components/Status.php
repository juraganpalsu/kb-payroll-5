<?php

/**
 * Created by PhpStorm
 *
 * File Name: Status.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 12/06/19
 * Time: 14:33
 */


namespace common\components;

/**
 * Class Status
 * @package common\components
 *
 * @property array $_status
 * @property array $_finishingStatus
 * @property array $_approval
 * @property array $activeInactive
 *
 */
class Status
{
    const OPEN = 1;
    const SUBMITED = 2;
    const APPROVED = 3;
    const REJECTED = 4;
    const FINISHED = 5;
    const CLOSED = 6;
    const CANCELED = 7;
    const REVISED = 8;

    /**
     * @var array status
     */
    public $_status = [
        self::OPEN => 'OPEN',
        self::SUBMITED => 'SUBMITED',
        self::APPROVED => 'APPROVED',
        self::REJECTED => 'REJECTED',
        self::FINISHED => 'FINISHED',
        self::CLOSED => 'CLOSED',
        self::CANCELED => 'CANCELED',
        self::REVISED => 'REVISED',
    ];

    const DEFLT = 0;
    const APPROVE = 1;
    const REJECT = 2;
    const SKIP = 3;
    const CANCEL = 4;

    public $_approval = [self::DEFLT => '', self::APPROVE => 'APPROVE', self::REJECT => 'REJECT', self::SKIP => 'SKIP', self::CANCEL => 'CANCEL'];

    const YA = 1;
    const TIDAK = 0;

    public $activeInactive = [
        self::YA => 'Ya',
        self::TIDAK => 'TIDAK'
    ];

    const dibuat = 1;
    const diproses = 2;
    const selesai = 3;

    public $_finishingStatus = [self::dibuat => 'DIBUAT', self::diproses => 'DIPROSSES', self::selesai => 'SELESAI'];

    /**
     * @param int $status
     * @param bool $asArray
     * @return mixed
     */
    public static function statuses(int $status = 0, $asArray = false)
    {
        $model = new self();
        if ($asArray) {
            return $model->_status;
        }
        return $model->_status[$status];
    }


    /**
     * @param int $status
     * @param bool $asArray
     * @return mixed
     */
    public static function approvalStatus(int $status = 0, $asArray = false)
    {
        $model = new self();
        if ($asArray) {
            return $model->_approval;
        }
        return $model->_approval[$status];
    }

    /**
     * @param int $status
     * @param bool $asArray
     * @return array|mixed
     */
    public static function activeInactive(int $status = 0, $asArray = false)
    {
        $model = new self();
        if ($asArray) {
            return $model->activeInactive;
        }
        return $model->activeInactive[$status];
    }

    /**
     * @param int $status
     * @param bool $asArray
     * @return array|mixed
     */
    public static function finishingStatus(int $status = 0, $asArray = false)
    {
        $model = new self();
        if ($asArray) {
            return $model->_finishingStatus;
        }
        return $model->_finishingStatus[$status];
    }

}
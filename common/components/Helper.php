<?php

/**
 * Created by PhpStorm
 *
 * File Name: Helper.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 23/07/19
 * Time: 23:45
 */


namespace common\components;


use Yii;

class Helper
{

    const TIDAK_AKTIF = 0;
    const AKTIF = 1;

    const bulan_satu_tahun = 12;
    const hari_satu_minggu = 7;


    const indonesianDay = [
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jum\'at',
        'Sat' => 'Sabtu',
        'Sun' => 'Minggu'
    ];


    /**
     * @return string[]
     */
    public static function statuses(): array
    {
        return [self::AKTIF => 'AKTIF', self::TIDAK_AKTIF => 'TIDAK AKTIF'];
    }


    /**
     * @param int $status
     * @return string
     */
    public static function getStatus(int $status): string
    {
        return self::statuses()[$status];
    }

    /**
     * @param int $integer
     * @return string
     */
    public static function integerToRoman(int $integer): string
    {
        // Memastikan yg dikirim adalah angka
        $integer = intval($integer);
        $result = '';

        // Daftar angka romawi.
        $lookup = [
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1
        ];

        foreach ($lookup as $roman => $value) {
            $matches = intval($integer / $value);
            $result .= str_repeat($roman, $matches);
            $integer = $integer % $value;
        }
        return $result;
    }


    /**
     * @param int $value
     * @param int $percent
     * @return float
     */
    public static function percent(int $value, int $percent): float
    {
        if ($value == 0 || $percent == 0) {
            return 0;
        }
        return $value * ($percent / 100);
    }


    /**
     * @param string $date
     * @return false|string
     */
    public static function idDate(string $date = ''): string
    {
        if (empty($date)) {
            return '';
        }
        return date('d-m-Y', strtotime($date));
    }


    /**
     * @param string $dateTime
     * @return false|string
     */
    public static function idDateTime(string $dateTime = ''): string
    {
        if (empty($dateTime)) {
            return '';
        }
        return date('d-m-Y H:i:s', strtotime($dateTime));
    }

    /**
     * @param float $data
     * @param int $decimal
     * @return string
     */
    public static function asDecimal(float $data, int $decimal = 0): string
    {
        return Yii::$app->formatter->asDecimal($data, $decimal);
    }

    /**
     * @param string $date
     * @return string
     */
    public static function idDay(string $date = ''): string
    {
        if (empty($date)) {
            $date = date('Y-m-d');
        }
        $day = date('D', strtotime($date));
        return self::indonesianDay[$day];
    }


}
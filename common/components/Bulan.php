<?php


namespace common\components;


use DateTime;
use Exception;
use yii\base\Component;

/**
 * Created by PhpStorm.
 * File Name: Bulan.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 27/02/20
 * Time: 22.13
 */

/**
 * Class Bulan
 * @package common\components
 *
 * @property array $_bulan
 */
class Bulan extends Component
{
    const jan = 1;
    const feb = 2;
    const mar = 3;
    const apr = 4;
    const mei = 5;
    const jun = 6;
    const jul = 7;
    const agu = 8;
    const sep = 9;
    const okt = 10;
    const nov = 11;
    const des = 12;

    public $_bulan = [
        self::jan => 'Januari',
        self::feb => 'Februari',
        self::mar => 'Maret',
        self::apr => 'April',
        self::mei => 'Mei',
        self::jun => 'Juni',
        self::jul => 'Juli',
        self::agu => 'Agustus',
        self::sep => 'September',
        self::okt => 'Oktober',
        self::nov => 'November',
        self::des => 'Desember',
    ];

    /**
     * @param int $bulan
     * @return array|mixed
     */
    public static function _bulan(int $bulan = 0)
    {
        $obj = new self();
        if ($bulan) {
            return $obj->_bulan[$bulan];
        }
        return $obj->_bulan;
    }

    /**
     * @return array
     */
    public static function _tahun()
    {
        $tahunSekarang = date('Y') + 1;
        return array_combine(range($tahunSekarang, 2000), range($tahunSekarang, 2000));
    }

    /**
     * @param string $tanggal
     * @return string
     * @throws Exception
     */
    public static function tanggal(string $tanggal)
    {
        $dateTime = new DateTime($tanggal);
        $bulanIndonsia = self::_bulan($dateTime->format('m'));
        return $dateTime->format('d') . ' ' . $bulanIndonsia . ' ' . $dateTime->format('Y');
    }
}
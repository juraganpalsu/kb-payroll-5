<?php


namespace common\components;

use yii\db\ActiveRecord;
use yii\base\Behavior;


/**
 * Created by PhpStorm.
 * File Name: DefaultPKBehavior.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 03/04/20
 * Time: 22.03
 */
class DefaultPKBehavior extends Behavior
{

    public $perjanjian_kerja_id = 'perjanjian_kerja_id';
    public $pegawai_golongan_id = 'pegawai_golongan_id';
    public $pegawai_struktur_id = 'pegawai_struktur_id';

    public $values = [
        'perjanjian_kerja_id' => 0,
        'pegawai_golongan_id' => 0,
        'pegawai_struktur_id' => 0
    ];


    /**
     * Set sebelum minyimpan data
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave'
        ];
    }


    /**
     * set beforeSave()
     */
    public function beforeSave()
    {
        if ($this->values['perjanjian_kerja_id']) {
            $this->owner->{$this->perjanjian_kerja_id} = call_user_func($this->values['perjanjian_kerja_id']);
        }
        if ($this->values['pegawai_golongan_id']) {
            $this->owner->{$this->pegawai_golongan_id} = call_user_func($this->values['pegawai_golongan_id']);
        }
        if ($this->values['pegawai_struktur_id']) {
            $this->owner->{$this->pegawai_struktur_id} = call_user_func($this->values['pegawai_struktur_id']);
        }
    }

}
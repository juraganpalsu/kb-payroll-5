<?php

namespace common\components;

use yii\base\Component;
use yii\base\InvalidParamException;
use yii\helpers\Json;

/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 9/1/17
 * Time: 9:07 PM
 */
class Hari extends Component
{
    //Hari dalam format Internasional
    const mon = 'Mon';
    const tue = 'Tue';
    const wed = 'Wed';
    const thu = 'Thu';
    const fri = 'Fri';
    const sat = 'Sat';
    const sun = "Sun";

    //Hari dalam Indonesia
    const senin = 'Senin';
    const selasa = 'Selasa';
    const rabu = 'Rabu';
    const kamis = 'Kamis';
    const jumat = 'Jum \'at';
    const sabtu = 'Sabtu';
    const minggu = 'Minggu';


    const _hari = [
        self::mon => self::senin,
        self::tue => self::selasa,
        self::wed => self::rabu,
        self::thu => self::kamis,
        self::fri => self::jumat,
        self::sat => self::sabtu,
        self::sun => self::minggu
    ];

    /**
     * @param string $hariInt Hari dalam form International -> Mon, Tue etc
     *
     * @return mixed Hari dalam format Indonesia jika ditemukan, jika tidak akan return exception
     */
    public static function getHariIndonesia(string $hariInt)
    {
        if (isset(self::_hari[$hariInt])) {
            return self::_hari[$hariInt];
        }
        throw new InvalidParamException('Parameter yg diinput salah.');
    }

    /**
     * @param array $hariInt Hari dalam format internationan
     * @return array data hari dalam format Indonesia
     *
     */
    public static function getHarisIndonesia(array $hariInt)
    {
        $hari = self::_hari;
        $datas = [];
        array_walk($hariInt, function ($d) use (&$datas, $hari) {
            $datas[$d] = $hari[$d];
        });
        return $datas;
    }

}
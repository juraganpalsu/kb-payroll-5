<?php


namespace common\components;


use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

/**
 * Created by PhpStorm.
 * File Name: GenerateSeqCode.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 27/09/19
 * Time: 10.02
 *
 * @property string $seqCodeAttribute
 * @property string $value
 *
 */
class GenerateSeqCode extends AttributeBehavior
{
    /**
     * @var string the attribute that will receive current seq code value.
     */
    public $seqCodeAttribute = 'seq_code';


    /**
     * {@inheritdoc}
     *
     */
    public $value;


    /**
     * {@inheritdoc}
     *
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => $this->seqCodeAttribute,
            ];
        }
    }

    /**
     * {@inheritdoc}
     *
     */
    protected function getValue($event)
    {
        return $this->generateSeqCode();
    }


    /**
     * @return int|string
     */
    public function generateSeqCode()
    {
        $owner = $this->owner;
        return $owner->getDb()->createCommand("SELECT CONCAT(date_format(curdate(), '%y%m%d'),IF(SUBSTRING(MAX($this->seqCodeAttribute),1,6) = date_format(curdate(), '%y%m%d'),LPAD(SUBSTRING(MAX($this->seqCodeAttribute),7,4)+1,4,'0'), '0001')) FROM {$owner->tableName()}")->queryScalar();
    }


}
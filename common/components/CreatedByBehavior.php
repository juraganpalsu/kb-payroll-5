<?php

/**
 * Created by PhpStorm
 *
 * File Name: CreatedByBehavior.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 17/07/19
 * Time: 23:28
 */


namespace common\components;


use yii\base\Behavior;
use yii\db\ActiveRecord;

class CreatedByBehavior extends Behavior
{

    /**
     * Relasinya ke table perjanjian kerja
     * created_by_pk = created_by_pegawai_perjanjian_kerja
     * @var string
     */
    public $byPegawaiAttribute = 'created_by_pk';
    public $byStrukturAttribute = 'created_by_struktur';

    public $values = [
        'byPegawai' => 0,
        'byStruktur' => 0
    ];

    /**
     * Set sebelum minyimpan data
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave'
        ];
    }


    /**
     * set beforeSave()
     */
    public function beforeSave()
    {
        $this->owner->{$this->byPegawaiAttribute} = call_user_func($this->values['byPegawai']);
        $this->owner->{$this->byStrukturAttribute} = call_user_func($this->values['byStruktur']);
    }
}
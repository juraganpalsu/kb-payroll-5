<?php


namespace common\components;


use frontend\modules\pegawai\models\Pegawai;
use Yii;
use yii\validators\Validator;

/**
 * Created by PhpStorm.
 * File Name: PegawaiAktifValidation.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 03/04/20
 * Time: 21.33
 */
class PegawaiAktifValidation extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        $modelPegawai = Pegawai::findOne($model->pegawai_id);
        if ($modelPegawai && !empty($model->$attribute)) {
            if (!$modelPegawai->getPerjanjianKerjaUntukTanggal($model->$attribute)) {
                $this->addError($model, $attribute, Yii::t('frontend', 'Pegawai sedang tidak dalam perjanjian kerja aktif.'));
            }
            if (!$modelPegawai->getPegawaiGolonganUntukTanggal($model->$attribute)) {
                $this->addError($model, $attribute, Yii::t('frontend', 'Golongan aktif tidak ditemukan.'));
            }
            if (!$modelPegawai->getPegawaiStrukturUntukTanggal($model->$attribute)) {
                $this->addError($model, $attribute, Yii::t('frontend', 'Struktur aktif tidak ditemukan.'));
            }
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jiwa
 * Date: 3/2/17
 * Time: 10:51 AM
 */

namespace common\components;


use Yii;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Request;

/**
 * Class ApiRequest
 * @package common\components
 *
 * @property array $ca_service
 * @property string $access_token
 * @property string $method
 * @property array $options
 * @property boolean $useCertificates
 */
class ApiRequest
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     * Sertifikat
     */
    public $ca_service = [
        'sslVerifyPeer' => false,
        'sslCafile' => ''
    ];

    /**
     *
     * @var string
     * Sebagai password API
     * Defaultnya adalah app key pada project-payroll
     */
    public $access_token;

    const POST_METHOD = 'post';
    const GET_METHOD = 'get';

    /**
     * @var string
     *
     * Default method adalah post
     *
     * Pada umumnya jika menggukan method post adalah untuk mengirim data
     * Jangan lupa untuk mengeset data nya dengan menggukan ->setData()
     *
     * Jika akan melakukan get, maka $method harus di set ->method = ApiRequest::GET_METHOD
     * Tapi tidak harus mengeset data
     */
    public $method = self::POST_METHOD;


    /**
     *
     * @var array
     * Digunakan untuk memberi option pengiriman atau pengambailan data seperti halnya include certificate
     * Dapat diisi oleh optoin2 yang terdapat pada CURL
     *
     */
    public $options = [];

    /**
     * @var bool
     *
     * Jika akan menggukan certicate maka harus di set true, jika tidak maka certificate tidak terbaca
     *
     */
    public $useCertificates = false;

    /**
     * @var string untuk menampung resquest
     */
    public $request;


    /**
     * ApiRequest constructor.
     * @param $apiHost
     */
    public function __construct($apiHost)
    {
        $this->access_token = isset(Yii::$app->params['app-key']) ? Yii::$app->params['app-key'] : '';

        $this->client = new Client(['baseUrl' => $apiHost, 'transport' => 'yii\httpclient\CurlTransport']);
    }

    /**
     * @return string|Request
     * @throws InvalidConfigException
     */
    public function createRequest()
    {
        $this->request = $this->client->createRequest();
        $this->request->setHeaders(['Authorization' => 'Bearer ' . $this->access_token]);
        $this->request->addHeaders(['content-type' => 'application/json'])
            ->setMethod($this->method);
        if ($this->useCertificates) {
            $this->options = array_merge($this->options, $this->ca_service);
        }
        if (!empty($this->options)) {
            $this->request->setOptions($this->options);
        }
        return $this->request;
    }

}
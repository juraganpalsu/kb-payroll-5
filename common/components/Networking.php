<?php
/**
 * Created by PhpStorm.
 * User: Yohanes
 * Date: 27-May-16
 * Time: 10:02 AM
 */

namespace common\components;


class Networking
{

    /**
     *
     * return true jika os windows
     * false jika os linux atau selain windows
     *
     * @return boolean
     */
    static function isWinOS()
    {

        if (strncasecmp(PHP_OS, 'WIN', 3) == 0) {
            return true;
        }
        return false;
    }

    /**
     *
     * ping host api
     * dilakukan sebelum sinkronisasi
     * -n = windows
     * -c = linux
     *
     * @param string $host
     * @param integer $timeout
     * @return boolean
     */
    static function ping($host, $timeout = 3)
    {
        $os = '-n';
        if (!Networking::isWinOS()) {
            $os = '-c';
        }

        exec("ping $os 1 -w $timeout " . $host, $output, $result);

        if ($result == 0) {
            return true;
        }
        return false;
    }
}
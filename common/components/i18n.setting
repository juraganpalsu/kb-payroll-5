Skip to content

i18n with Yii 2 Advanced Template

In this post, I will show you my workflow for internationalization of
Yii based projects.

We will configure sane paths, logically dividing the frontend and
backend. We will also use the *yii *cli tool to generate the translation
files for us. Let’s get started.

 1. Create *messages* directory inside the *common* directory.
 2. Create a file called *i18n.php* inside *common/config* directory.
 3. Paste the following block of code inside ***i18n.php.***
    PHP
    <?php
    return [
        'sourcePath' => __DIR__. '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR,
        'languages' => ['pt-PT'], //Add languages to the array for the language files to be generated.
        'translator' => 'Yii::t',
        'sort' => false,
        'removeUnused' => false,
        'only' => ['*.php'],
        'except' => [
            '.svn',
            '.git',
            '.gitignore',
            '.gitkeep',
            '.hgignore',
            '.hgkeep',
            '/messages',
            '/vendor',
        ],
        'format' => 'php',
        'messagePath' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'messages',
        'overwrite' => true,
    ];


      * Make sure to add all required languages to *‘languages’ *array.
        In the above example I have added Portuguese.
 4. Add the i18n component to your *common/main.php* configuration as
    follows:
    'components' => [
        ...
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        ...
    ],


 5. You can now:
      * Set the language in common configuration i.e.
        'language'=>'pt-PT' inside *common/main.php*.
      * Set the language at runtime i.e. Yii::$app->language='pt-PT'
      * See the Yii 2.0 Guide on Internationalization
        <http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html>
        for more details on setting a language.
 6. In your frontend and backend code, use the following method to
    create i18n supported strings respectively.

    Yii::t('frontend', 'Translatable String');


    Yii::t('backend', 'Translatable String');

 7. Run  yii message/extract @common/config/i18n.php to generate the
    translation files inside common/messages.
 8. Yii message will generate the translation files as follows:
    common/
      ...
      messages/
        pt-PT/
          backend.php
          frontend.php
      ...


 9. The files frontend.php and backend.php are now ready to edit the
    Portuguese translations :)
<?php

namespace common\models;

use common\components\Status;
use common\models\base\SplTemplate as BaseSplTemplate;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "spl_template".
 * @property mixed $kategori_
 * @property mixed $kategoriSingkatan
 * @property mixed $sistemKerja_
 * @property mixed $namaKomplit
 * @property float|int $jumlah
 * @property float|int $jumlahKonversi
 *
 * @property array $_kategori
 * @property array $_kategoriSingkatan
 *
 */
class SplTemplate extends BaseSplTemplate
{

    /**
     * Jenis lembur
     * Lembur sebelum jam kerja dimulai
     */
    const LEMBUR_AWAL = 1;

    /**
     * Lembur yang dilakukan setelah jam kerja berakhir
     */
    const LEMBUR_AKHIR = 2;

    /**
     * Lembur yang dilakukan pada hari libur atau tidak dibarengi dengan jam kerja setelah nya ataupun sebelumnya
     */
    const LEMBUR_KHUSUS = 3;

    /**
     * @var array menampilkan jenis lembur
     */
    public $_kategori = [self::LEMBUR_AWAL => 'Lembur Awal', self::LEMBUR_AKHIR => 'Lembur Akhir', self::LEMBUR_KHUSUS => 'Lembur Khusus'];
    public $_kategoriSingkatan = [self::LEMBUR_AWAL => 'AW', self::LEMBUR_AKHIR => 'AK', self::LEMBUR_KHUSUS => 'KH'];

    const NON_SHIFT = 0;
    const SHIFT_SATU = 1;
    const SHIFT_DUA = 2;
    const SHIFT_TIGA = 3;

    public $_shift = [self::NON_SHIFT => 'Non Shift', self::SHIFT_SATU => 'Shift Satu', self::SHIFT_DUA => 'Shift Dua', self::SHIFT_TIGA => 'Shift Tiga',];


    const AS_HEADER = 1;
    const NO_HEADER = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
            [
                [['nama', 'masuk', 'pulang'], 'required'],
                [['toleransi_masuk', 'toleransi_pulang', 'sistem_kerja', 'sebagai_header', 'kategori', 'tunjangan_shift', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
                [['istirahat', 'jumlah_jam_lembur'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
                [['masuk_minimum', 'masuk', 'masuk_maksimum', 'jam_istirahat_minimum', 'jam_istirahat', 'jam_istirahat_maksimum', 'masuk_istirahat_minimum', 'masuk_istirahat', 'masuk_istirahat_maksimum', 'pulang_minimum', 'pulang', 'pulang_maksimum', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
                [['nama'], 'string', 'max' => 225],
                [['sebagai_header', 'lock'], 'default', 'value' => 0],
                [['lock'], 'mootensai\components\OptimisticLockValidator'],
                [['masuk_minimum', 'masuk_maksimum'], 'required', 'when' => function ($model) {
                    return $model->kategori == self::LEMBUR_AWAL;
                }, 'enableClientValidation' => false],
                [['pulang_minimum', 'pulang_maksimum'], 'required', 'when' => function ($model) {
                    return $model->kategori == self::LEMBUR_AKHIR;
                }, 'enableClientValidation' => false],
            ];
    }

    /**
     * @return mixed
     */
    public function getKategori_()
    {
        return $this->_kategori[$this->kategori];
    }

    /**
     * @return mixed
     */
    public function getKategoriSingkatan()
    {
        return $this->_kategoriSingkatan[$this->kategori];
    }


    /**
     * @return array
     */
    public function getSistemKerja_()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getShift_()
    {
        return $this->_shift[$this->tunjangan_shift];
    }

    /**
     * @return array|mixed
     */
    public function getSebagaiHeader()
    {
        return Status::activeInactive($this->sebagai_header);
    }

    /**
     * @return string
     */
    public function getNamaKomplit()
    {
        return $this->nama . ', ' . $this->kategori_ . ' - ' . $this->masuk . ' s/d ' . $this->pulang;
    }

    /**
     * Merubah iputan yg sebelumnya tanda "," menjadi "."
     * agar inputan setengah terbaca
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->istirahat = str_replace(",", ".", $this->istirahat);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $attr = ['tunjangan_shift' => Yii::t('app', 'Shift')];
        return array_merge(parent::attributeLabels(), $attr);
    }

    /**
     * Menampilkan jumlah jam lembur
     *
     * @return float|int
     */
    public function getJumlah()
    {
        $masuk = strtotime($this->masuk);
        $pulang = strtotime($this->pulang);

        $jumlah = abs($pulang - $masuk) / 3600;
        if ($masuk > $pulang) {
            $jumlah = abs(($masuk - $pulang) / 3600 - 24);
        }
        return $jumlah - $this->istirahat;
    }

    /**
     * Menghitung jumlah jam lembur setelah dikonversi
     * Contoh :
     *  untuk lembur khusus dikali 2
     *  untuk lembur awal dan akhir dikali 2 - 0.5
     *
     * @return float|int
     */
    public function getJumlahKonversi()
    {
        if ($this->kategori == self::LEMBUR_KHUSUS) {
            return round($this->jumlah * 2, 1);
        }
        return round(($this->jumlah * 2) - 0.5, 1);
    }

    /**
     * @param null $q
     * @param string $status
     * @return array|ActiveRecord[]
     */
    public function listSelect2($q = null, $status = '*')
    {
        $query = self::find()->select(['id', 'concat(`nama`,\'-\',`masuk`,\'-\',`pulang`) as text'])
            ->andWhere(['like', 'concat(nama, masuk, pulang)', $q])->limit(20);
        if ($status == self::NO_HEADER) {
            $query->andWhere(['sebagai_header' => self::NO_HEADER]);
        }
        if ($status == self::AS_HEADER) {
            $query->andWhere(['sebagai_header' => self::AS_HEADER]);
        }
        $data = $query->asArray()->all();
        return $data;
    }

    /**
     * @return array
     */
    public function getAllTemplate()
    {
        $queryDetail = SplTemplateDetail::find()
            ->select('spl_template_detail_id')
            ->andWhere(['spl_template_id' => $this->id])
            ->column();
        $allTamplate = [$this->id];
        if ($queryDetail) {
            $allTamplate = array_merge($queryDetail, $allTamplate);
        }
        $queryAllTemplate = SplTemplate::find()
            ->andWhere(['IN', 'id', $allTamplate])
            ->orderBy('masuk ASC')
            ->all();
        $splTemplate = [];
        foreach ($queryAllTemplate as $template) {
            $splTemplate[$template->id] = $template;
        }
        return $splTemplate;
    }

}

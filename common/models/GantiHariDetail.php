<?php

namespace common\models;

use common\models\base\GantiHariDetail as BaseGantiHariDetail;

/**
 * This is the model class for table "ganti_hari_detail".
 */
class GantiHariDetail extends BaseGantiHariDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pegawai_id', 'ganti_hari_id'], 'required'],
            [['ganti_hari_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['pegawai_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}

<?php

namespace common\models;

use common\models\base\CostCenterTemplate as BaseCostCenterTemplate;

/**
 * This is the model class for table "cost_center_template".
 *
 * @property string $kodeArea
 */
class CostCenterTemplate extends BaseCostCenterTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'area_kerja'], 'required'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['kode'], 'string', 'max' => 11],
            [['area_kerja'], 'string', 'max' => 45],
            [['lock', 'is_aktif'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return string
     */
    public function getKodeArea()
    {
        return $this->kode . ' - ' . $this->area_kerja;
    }

}

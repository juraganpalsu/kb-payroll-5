<?php

namespace common\models;

use common\models\base\SplTemplateDetail as BaseSplTemplateDetail;

/**
 * This is the model class for table "spl_template_detail".
 */
class SplTemplateDetail extends BaseSplTemplateDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['spl_template_id', 'spl_template_detail_id'], 'required'],
            [['spl_template_id', 'spl_template_detail_id', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id'], 'string', 'max' => 45],
            [['created_by_pk'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
//            ['spl_template_detail_id', 'templateDetailValidation']
        ];
    }

//    public function templateDetailValidation(string $attribute)
//    {
//        $queryTemplate = SplTemplate::findOne($this->spl_template_id);
//        $queryTemplateDetail = SplTemplate::findOne($this->$attribute);
//        if ($queryTemplate && $queryTemplateDetail) {
//            if ($queryTemplate->kategori == SplTemplate::LEMBUR_AWAL) {
//
//            }
//
//            if ($queryTemplate->kategori == SplTemplate::LEMBUR_AKHIR) {
//
//            }
//        }
//    }

}

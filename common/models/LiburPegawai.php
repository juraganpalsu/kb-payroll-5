<?php

namespace common\models;

use common\models\base\LiburPegawai as BaseLiburPegawai;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "libur_pegawai".
 * @property mixed monthString
 */
class LiburPegawai extends BaseLiburPegawai
{

    /**
     * @var int bulan
     */
    public $month;

    public $upload_dir = 'uploads/';

    public $doc_file;

    /**
     * @var string maksimal hari dalan satu bulan
     */
//    private $_28 = 28;
    private $_29 = 29;
    private $_30 = 30;
    private $_31 = 31;


    /**
     * Return bulan dalam bentuk array atau sting
     */
    const MONTH_STRING = 1;
    const MONTH_ARRAY = 2;

    public $_month = [1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month', 'pegawai_id', 'sistem_kerja_id', 'tanggal', 'doc_file'], 'required'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['sistem_kerja_id', 'month', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['pegawai_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['tanggal', 'pegawai_id'], 'unique', 'targetAttribute' => ['tanggal', 'pegawai_id']]
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $schenarios = parent::scenarios();
        $schenarios['download-file'] = ['month'];
        $schenarios['upload-file'] = ['doc_file', 'month'];
        $schenarios['create-libur'] = ['pegawai_id', 'tanggal', 'sistem_kerja_id'];
        return $schenarios;
    }

    /**
     * @return array
     */
    public function getMonthDropdown()
    {
        $months = [];
        foreach ($this->_month as $k => $month) {
            array_push($months, [
                'label' => $month,
                'url' => Url::to(['/libur-pegawai/get-month']),
                'options' => ['class' => 'get-month', 'data' => ['month' => $k, 'url' => Url::to(['/libur-pegawai/get-month'])]]
            ]);
        }
        return $months;
    }

    /**
     * @return mixed
     */
    public function getMonthString()
    {
        return $this->_month[$this->month];
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadFile()
    {
        $spreadsheet = new Spreadsheet();
        $Excel_writer = new Xls($spreadsheet);
        $days = cal_days_in_month(CAL_GREGORIAN, $this->month, date('Y'));
        $spreadsheet->setActiveSheetIndex(0);
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->mergeCells('A1:A3')->setCellValue('A1', 'NO')
            ->mergeCells('B1:B3')->setCellValue('B1', 'Idfp')
            ->mergeCells('C1:C3')->setCellValue('C1', 'Nama');
        $activeSheet->mergeCells('D1:AH1')->setCellValue('D1', 'Periode Bulan ' . $this->monthString);
        foreach ($this->getcolumnrange('D', $this->maxColumn($days)) as $k => $col) {
            $day = $k + 1;
            $activeSheet->setCellValue($col . '3', $day);
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=TemplateLibur_' . $this->monthString . date('Y') . '.xls');
        header('Cache-Control: max-age=0');
        try {
            ob_end_clean();
            $Excel_writer->save('php://output');
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }

        return true;
    }

    /**
     * @param int $col
     * @return string
     */
    private function maxColumn(int $col)
    {
        $maxColumn = 'AE';
        if ($col == $this->_29) {
            $maxColumn = 'AF';
        } elseif ($col == $this->_30) {
            $maxColumn = 'AG';
        } elseif ($col == $this->_31) {
            $maxColumn = 'AH';
        }
        return $maxColumn;
    }


    /**
     * @param $min
     * @param $max
     * @return array
     */
    private function getcolumnrange($min, $max)
    {
        $pointer = strtoupper($min);
        $output = array();
        while ($this->positionalComparison($pointer, strtoupper($max)) <= 0) {
            array_push($output, $pointer);
            $pointer++;
        }
        return $output;
    }

    /**
     * @param $a
     * @param $b
     * @return int
     */
    private function positionalComparison($a, $b)
    {
        $a1 = $this->stringToIntValue($a);
        $b1 = $this->stringToIntValue($b);
        if ($a1 > $b1) return 1;
        else if ($a1 < $b1) return -1;
        else return 0;
    }

    /**
     * @param $str
     * @return int
     * e.g. A=1 - B=2 - Z=26 - AA=27 - CZ=104 - DA=105 - ZZ=702 - AAA=703
     */
    private function stringToIntValue($str)
    {
        $amount = 0;
        $strarra = array_reverse(str_split($str));

        for ($i = 0; $i < strlen($str); $i++) {
            $amount += (ord($strarra[$i]) - 64) * pow(26, $i);
        }
        return $amount;
    }

    /**
     * Upload file
     *
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            /** @var UploadedFile $docFile */
            $docFile = $this->doc_file;
            $docFile->saveAs($this->upload_dir . $docFile->baseName . '.' . $docFile->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function extractLiburFromExcel()
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->upload_dir . $this->doc_file->name);
        $maxRow = $spreadsheet->getActiveSheet()->getHighestRow('B');
        $maxColumn = $spreadsheet->getActiveSheet()->getHighestColumn('3');
        $startRow = 4;
        $datas = [];
        foreach ($spreadsheet->getActiveSheet()->getRowIterator($startRow, $maxRow) as $row) {
            $idfp = $spreadsheet->getActiveSheet()->getCell('B' . $row->getRowIndex())->getValue();
            $date = 1;
            foreach ($spreadsheet->getActiveSheet()->getColumnIterator('D', $maxColumn) as $column) {
                $libur = $spreadsheet->getActiveSheet()->getCell($column->getColumnIndex() . $row->getRowIndex())->getValue();
                if ($libur == 'L') {
                    $datas[$idfp][] = $date;
                }
                $date++;
            }
        }
        return $datas;
    }

    /**
     * @param array $datas
     * @return int jumlah data yg berhasil disimpan
     */
    public function createLibur(array $datas)
    {
        $count = 0;
        if (!empty($datas)) {
            foreach ($datas as $idfp => $data) {
                $perjanjianKerja = PerjanjianKerja::findOne(['id_pegawai' => $idfp]);
                foreach ($data as $dates) {
                    $date = date('Y') . '-' . $this->month . '-' . $dates;
                    if ($perjanjianKerja) {
                        $model = new LiburPegawai();
                        $model->scenario = 'create-libur';
                        $model->pegawai_id = $perjanjianKerja->pegawai_id;
                        $model->tanggal = $date;
                        $model->sistem_kerja_id = $perjanjianKerja->pegawai->getPegawaiSistemKerjaUntukTanggal($date)->sistem_kerja_id;
                        $cek = self::findOne(['tanggal' => $date, 'pegawai_id' => $perjanjianKerja->pegawai_id]);
                        if ($cek) {
                            $model = $cek;
                        }
                        if ($model->save()) {
                            $count++;
                        }
                    }
                }
            }
        }
        return $count;
    }

    /**
     * Cek hari libur sesua dengan sistem kerja
     *
     * @param Pegawai $pegawai
     * @param string $tanggal
     * @return bool
     */
    public function cekLiburPegawai(Pegawai $pegawai, string $tanggal)
    {
        $day = date('D', strtotime($tanggal));
        $libur = false;

        $sisteKerja = $pegawai->getPegawaiSistemKerjaUntukTanggal($tanggal);
        if ($sisteKerja) {
            $sistemKerjaConfig = SistemKerja::getConfig($sisteKerja->sistem_kerja_id);

            if ($sistemKerjaConfig->jenis_libur == SistemKerja::POLA) {
                $query = self::findOne(['pegawai_id' => $pegawai->id, 'tanggal' => $tanggal]);
                if ($query) {
                    $libur = true;
                }
            }

            if ($sistemKerjaConfig->jenis_libur == SistemKerja::HARI) {
                if (in_array($day, $sistemKerjaConfig->hariLibur)) {
                    $libur = true;
                }
            }
        }

        return $libur;
    }

    /**
     * @param Pegawai $pegawai
     * @param string $tanggal hari ini atau hari yg di cek
     *
     * @return bool
     */
    public static function cekBesokLibur(Pegawai $pegawai, string $tanggal)
    {
        $besok = date('Y-m-d', strtotime($tanggal . ' +1 day'));
        $model = new self();
        return $model->cekLiburPegawai($pegawai, $besok);

    }

}

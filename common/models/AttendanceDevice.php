<?php

namespace common\models;

use common\components\ApiRequest;
use common\models\base\AttendanceDevice as BaseAttendanceDevice;
use Yii;
use yii\base\InvalidConfigException;
use yii\httpclient\Response;

/**
 * This is the model class for table "attendance_device".
 */
class AttendanceDevice extends BaseAttendanceDevice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['notes'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['id'], 'string', 'max' => 45],
            [['name'], 'string', 'max' => 225]
        ];
    }

    /**
     * @return array|mixed
     * @throws \yii\httpclient\Exception
     */
    public function getDevice()
    {

        $data = [];
        $adms = Yii::$app->params['adms'];
//        if (Networking::ping($adms['host'])) {
        Yii::info('Succesfull pinging..', 'download-device');
        $apiRequest = new ApiRequest($adms['url']);
        $apiRequest->options = [
            'proxy' => 'tcp://192.1.2.222:3128', // use a Proxy
            'timeout' => 5, // set timeout to 5 seconds for the case server is not responding
        ];
        $apiRequest->useCertificates = true;
        $apiRequest->ca_service = [
            'sslVerifyPeer' => false,
            'sslCafile' => '@common/certificates/serviceadms_hc_kbu_com.crt'
        ];
        $apiRequest->access_token = $adms['token'];
        $apiRequest->method = ApiRequest::GET_METHOD;
        try {
            Yii::info('Start downloading..', 'download-device');
            /** @var Response $responseApi */
            $responseApi = $apiRequest->createRequest()
                ->setUrl($adms['url'] . '/v1/iclock/get-device')
                ->send();

            if ($responseApi->isOk) {
                $data = $responseApi->data;
            } else {
                Yii::info('Gagal curl download from adms.', 'exception');
                Yii::info($responseApi->data, 'exception');
            }
        } catch (InvalidConfigException $e) {
            Yii::info($e->getMessage(), 'download-device');
        }
        Yii::info('Downloading finish..', 'download-device');
        return $data;
    }

    /**
     * @param array $device
     * @return bool
     */
    public function saveDevice(array $device)
    {
        $model = new AttendanceDevice();
        $model->id = $device['id'];
        $check = self::findOne($device['id']);
        if ($check) {
            $model = $check;
        }
        $model->name = $device['name'];
        $model->notes = $device['notes'];
        return $model->save();
    }

}

<?php

namespace common\models;

use common\models\base\PerubahanStatusAbsensi as BasePerubahanStatusAbsensi;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "perubahan_status_absensi".
 *
 * @property array $pegawai_ids
 * @property array $pegawai_exist
 *
 *
 * @property string $nama
 * @property string $idfp
 */
class PerubahanStatusAbsensi extends BasePerubahanStatusAbsensi
{

    public $pegawai_ids;
    public $pegawai_exist = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_ids', 'tanggal', 'tanggal_pulang', 'status_kehadiran', 'keterangan'], 'required'],
            [['urut', 'status_kehadiran', 'time_table_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal', 'tanggal_pulang', 'masuk', 'pulang', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id'], 'string', 'max' => 32],
            [['jumlah_jam_lembur'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['pegawai_ids', 'cekPegawaiExist']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['pegawai_ids'] = Yii::t('app', 'Pegawai');
        return $labels;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($insert) {
            $this->urut = $this->generateUrut();
            if (empty($this->masuk)) {
                $this->masuk = date('H:i:s');
            }
            if (empty($this->pulang)) {
                $this->pulang = date('H:i:s');
            }
            if (in_array($this->status_kehadiran, [Absensi::ALFA])) {
                $this->masuk = null;
                $this->pulang = null;
                $this->jumlah_jam_lembur = 0;
            }
        }
        if ($this->masuk == '00:00:00') {
            $this->masuk = null;
        }
        if ($this->pulang == '00:00:00') {
            $this->pulang = null;
        }
        return true;
    }

    /**
     * @return int|string
     */
    private function generateUrut()
    {
        $countRows = (new Query())
            ->select(['id'])
            ->from(static::tableName())
            ->count();
        return $countRows + 1;
    }


    /**
     * Cek apakah seorang pegawai telah dibuatkan Perubahan Status
     * @param $attribute
     */
    public function cekPegawaiExist($attribute)
    {
        $query = PerubahanStatusAbsensiDetail::find()
            ->joinWith(['perubahanStatusAbsensi' => function (ActiveQuery $query) {
                $query->andWhere(['tanggal' => $this->tanggal]);
            }])
            ->andWhere('pegawai_id =:pegawai_id', [':pegawai_id' => $this->pegawai_ids])
            ->one();
        if ($query) {
            $this->addError($attribute, Yii::t('frontend', 'Perubahan Telah diabuat.'));
        }
    }


    /**
     * @return array
     */
    public function createDetail()
    {
        $savedDetail = [];
        $perubahanStatusAbsensiDetail = new PerubahanStatusAbsensiDetail();
        $perubahanStatusAbsensiDetail->pegawai_id = $this->pegawai_ids;
        $perubahanStatusAbsensiDetail->perubahan_status_absensi_id = $this->id;

        if ($perubahanStatusAbsensiDetail->save()) {
            $this->createOrUpdateAbsensi($perubahanStatusAbsensiDetail);
            array_push($savedDetail, $perubahanStatusAbsensiDetail->pegawai_id);
        }
        return $savedDetail;
    }

    /**
     * @return string
     */
    public function getNama()
    {
        $datas = array_map(function (PerubahanStatusAbsensiDetail $dt) {
            return $dt->pegawai ? $dt->pegawai->nama : '-';
        }, $this->perubahanStatusAbsensiDetails);
        return implode('<br />', $datas);
    }


    /**
     * @param string $tanggal
     * @return string
     */
    public function getIdPegawaiUntukTanggal(string $tanggal)
    {
        $datas = array_map(function (PerubahanStatusAbsensiDetail $dt) use ($tanggal) {
            if ($pegawai = $dt->pegawai) {
                if ($perjanjianKerja = $pegawai->getPerjanjianKerjaUntukTanggal($tanggal)) {
                    return $perjanjianKerja->id_pegawai;
                }
            }
            return '-';
        }, $this->perubahanStatusAbsensiDetails);
        return implode('<br />', $datas);
    }


    /**
     *
     * Melakukan perubahan pada data absensi sesuai dengan perubahannya
     *
     * @param PerubahanStatusAbsensiDetail $modelPerubahanAbsensiDetail
     */
    private function createOrUpdateAbsensi(PerubahanStatusAbsensiDetail $modelPerubahanAbsensiDetail)
    {
        $modelAbsensi = new Absensi();
        $modelPerubahanAbsensi = $modelPerubahanAbsensiDetail->perubahanStatusAbsensi;
        $modelAbsensi->pegawai_id = $modelPerubahanAbsensiDetail->pegawai_id;
        $modelAbsensi->tanggal = $modelPerubahanAbsensi->tanggal;

        if ($check = Absensi::findOne(['pegawai_id' => $modelAbsensi->pegawai_id, 'tanggal' => $modelAbsensi->tanggal])) {
            $modelAbsensi = $check;
        }

        $modelAbsensi->status_kehadiran = $modelPerubahanAbsensi->status_kehadiran;
        if (in_array($modelPerubahanAbsensi->status_kehadiran, [Absensi::ALFA])) {
            $modelAbsensi->masuk = null;
            $modelAbsensi->pulang = null;
            $modelAbsensi->time_table_id = null;
            $modelAbsensi->jumlah_jam_lembur = 0;
            $modelAbsensi->telat = 0;
        } else {
            $modelAbsensi->time_table_id = $modelPerubahanAbsensi->time_table_id;
            $modelAbsensi->masuk = $modelPerubahanAbsensi->tanggal . ' ' . $modelPerubahanAbsensi->masuk;
            $modelAbsensi->pulang = $modelPerubahanAbsensi->tanggal_pulang . ' ' . $modelPerubahanAbsensi->pulang;
            $modelAbsensi->jumlah_jam_lembur = $modelPerubahanAbsensi->jumlah_jam_lembur;
        }
        if (is_null($modelPerubahanAbsensi->masuk)) {
            $modelAbsensi->masuk = null;
        }
        if (is_null($modelPerubahanAbsensi->pulang)) {
            $modelAbsensi->pulang = null;
        }
        $modelAbsensi->save();
    }

    /**
     * @param string $pegawai_id
     * @param string $tanggal
     * @return array|ActiveRecord|null
     */
    public static function getPerubahanAbsensiByPegawaiDanTanggal(string $pegawai_id, string $tanggal)
    {
        $query = PerubahanStatusAbsensi::find()
            ->joinWith(['perubahanStatusAbsensiDetails' => function (ActiveQuery $x) use ($pegawai_id) {
                $x->andWhere(['pegawai_id' => $pegawai_id]);
            }])->andWhere(['tanggal' => $tanggal]);
        return $query->one();
    }

}

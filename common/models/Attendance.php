<?php

namespace common\models;

use common\models\base\Attendance as BaseAttendance;
use DateTime;
use frontend\modules\pegawai\models\PerjanjianKerja;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "attendance".
 * @property UploadedFile $file_att
 * @property string $upload_dir
 * @property integer $timeFileUploaded
 * @property string $date
 * @property array $pegawai_ids
 *
 */
class Attendance extends BaseAttendance
{

    public $file_att;
    public $upload_dir = 'uploads/attendance/';

    public $timeFileUploaded;

    public $ip;
    public $date;

    //basename file attendance
    const SOLUTION = 'SOLUTION';
    const HMTAS = 'HMTAS';
    const BEATROUTE = 'BEATROUTE';
    const SFA_DEVICE_ID = 'SFA';

    public $pegawai_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
            [
                [['pegawai_ids', 'pegawai_id', 'check_time', 'idfp', 'file_att', 'ip', 'date'], 'required'],
                [['pegawai_ids', 'device_id', 'file_att', 'check_time', 'created_at', 'updated_at', 'deleted_at', 'ip'], 'safe'],
                [['idfp', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
                [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id'], 'string', 'max' => 32],
                [['lock'], 'default', 'value' => '0'],
                [['lock'], 'mootensai\components\OptimisticLockValidator']
            ];
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['import-attendance'] = ['file_att'];
        $scenario['save-attendance'] = ['pegawai_id', 'check_time', 'idfp'];
        $scenario['save-attendance-from-api'] = ['pegawai_id', 'check_time', 'idfp'];
        $scenario['get-attendance-from-machine'] = ['ip', 'date'];
        $scenario['get-attendance-from-adms'] = ['pegawai_ids', 'date'];
        return $scenario;
    }


    /**
     * Upload file
     *
     * @return bool
     */
    public function upload()
    {
        $this->timeFileUploaded = time();
        if ($this->validate()) {
            $modUpload = $this->file_att;
            $modUpload->saveAs($this->upload_dir . $this->file_att->baseName . '-' . $this->timeFileUploaded . '.' . $this->file_att->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Menyimpan data attendance dari file txt
     *
     * @return int
     */
    public function getAttContent()
    {
        $file = $this->upload_dir . $this->file_att->baseName . '-' . $this->timeFileUploaded . '.' . $this->file_att->extension;
        $handle = fopen($file, "r");
        $scs = 0;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $a = preg_split('/\s+/', $line);
                $checkTime = $a[7] . ' ' . $a[8];
                if ($this->saveAttendance($a[1], $checkTime)) {
                    continue;
                }
                $scs++;
            }

            fclose($handle);
        }
        return $scs;
    }

    /**
     * @return bool|int
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function getAttContentXls()
    {
        $file = $this->upload_dir . $this->file_att->baseName . '-' . $this->timeFileUploaded . '.' . $this->file_att->extension;
        if (!file_exists($file)) {
            return false;
        }

        ini_set('memory_limit', '8192M');

        $source = explode('_', $this->file_att->baseName);

        $scs = 0;
        if (strtoupper($source[0]) == self::SOLUTION) {
            $scs = $this->getAttContentXlsSolution($file);
        }
        if (strtoupper($source[0]) == self::HMTAS) {
            $scs = $this->getAttContentXlsHmtas($file);
        }
        if (strtoupper($source[0]) == self::BEATROUTE) {
            $scs = $this->getAttContentXlsBeatroute($file);
        }
        if (strtoupper($source[0]) == self::SFA_DEVICE_ID) {
            $scs = $this->getAttContentXlsSfa($file);
        }
        ini_set('memory_limit', '1024M');

        return $scs;
    }

    /**
     * @param string $file
     * @return int
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function getAttContentXlsSolution(string $file)
    {
        $scs = 0;
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        try {
            $excel = $reader->load($file);
            $maxRow = $excel->getActiveSheet()->getHighestRow('A');
            foreach ($excel->getActiveSheet()->getRowIterator(2, $maxRow) as $row) {
                $idfp = $excel->getActiveSheet()->getCell('A' . $row->getRowIndex())->getValue();
                try {
                    $checkTime = new DateTime($excel->getActiveSheet()->getCell('D' . $row->getRowIndex())->getValue());

                    $checkTime = $checkTime->format('Y-m-d H:i:s');
                    if (!$this->saveAttendance($idfp, $checkTime)) {
                        continue;
                    }
                    $scs++;
                } catch (\Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exceptoin');
        }
        return $scs;
    }

    /**
     * @param string $file
     * @return int
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function getAttContentXlsHmtas(string $file)
    {
        $scs = 0;
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        try {
            $excel = $reader->load($file);
            $maxRow = $excel->getActiveSheet()->getHighestRow('A');
            foreach ($excel->getActiveSheet()->getRowIterator(1, $maxRow) as $row) {
                $idfp = $excel->getActiveSheet()->getCell('A' . $row->getRowIndex())->getValue();
                $date = $excel->getActiveSheet()->getCell('B' . $row->getRowIndex())->getValue();
                $time = $excel->getActiveSheet()->getCell('C' . $row->getRowIndex())->getValue();

                if (!is_numeric($idfp) || empty($date) || empty($time)) {
                    continue;
                }

                try {
                    $checkTime = new DateTime($date . ' ' . $time);
                    $checkTime = $checkTime->format('Y-m-d H:i:s');
                    if (!$this->saveAttendance($idfp, $checkTime)) {
                        continue;
                    }
                    $scs++;
                } catch (\Exception $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $scs;
    }

    /**
     * @param string $file
     * @return int
     */
    public function getAttContentXlsBeatroute(string $file)
    {
        $scs = 0;
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        try {
            $excel = $reader->load($file);
            $maxRow = $excel->getActiveSheet()->getHighestRow('A');
            foreach ($excel->getActiveSheet()->getRowIterator(1, $maxRow) as $row) {
                $idfp = $excel->getActiveSheet()->getCell('A' . $row->getRowIndex())->getValue();

                if (!is_numeric($idfp) || empty($idfp)) {
                    continue;
                }

                $date = $excel->getActiveSheet()->getCell('J' . $row->getRowIndex())->getValue();
                $date = new DateTime($date);
                $date = $date->format('Y-m-d');

                $timeFirst = $excel->getActiveSheet()->getCell('K' . $row->getRowIndex())->getValue();
                $timeFirst = new DateTime($timeFirst);
                $timeFirst = $timeFirst->format('H:i:s');

                $timeLast = $excel->getActiveSheet()->getCell('L' . $row->getRowIndex())->getValue();
                $timeLast = new DateTime($timeLast);
                $timeLast = $timeLast->format('H:i:s');

                if (!empty($timeFirst)) {
                    $checkTime = new DateTime($date . ' ' . $timeFirst);
                    $checkTime = $checkTime->format('Y-m-d H:i:s');
                    if ($this->saveAttendance($idfp, $checkTime, self::BEATROUTE)) {
                        $scs++;
                    }
                }

                if (!empty($timeLast)) {
                    $checkTime = new DateTime($date . ' ' . $timeLast);
                    $checkTime = $checkTime->format('Y-m-d H:i:s');
                    if ($this->saveAttendance($idfp, $checkTime, self::BEATROUTE)) {
                        $scs++;
                    }
                }

            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exceptoin');
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exceptoin');
        }
        return $scs;
    }

    /**
     * @param string $file
     * @return int
     */
    public function getAttContentXlsSfa(string $file)
    {
        $scs = 0;
        $reader = new Xls();
        $reader->setReadDataOnly(true);
        try {
            $excel = $reader->load($file);
            $maxRow = $excel->getActiveSheet()->getHighestRow('A');
            foreach ($excel->getActiveSheet()->getRowIterator(1, $maxRow) as $row) {
                $idfp = $excel->getActiveSheet()->getCell('A' . $row->getRowIndex())->getValue();

                if (!is_numeric($idfp) || empty($idfp)) {
                    continue;
                }

                $date = $excel->getActiveSheet()->getCell('J' . $row->getRowIndex())->getValue();
                $date = new DateTime($date);
                $date = $date->format('Y-m-d');

                $timeFirst = $excel->getActiveSheet()->getCell('K' . $row->getRowIndex())->getValue();
                $timeFirst = new DateTime($timeFirst);
                $timeFirst = $timeFirst->format('H:i:s');

                $timeLast = $excel->getActiveSheet()->getCell('L' . $row->getRowIndex())->getValue();
                $timeLast = new DateTime($timeLast);
                $timeLast = $timeLast->format('H:i:s');

                if (!empty($timeFirst)) {
                    $checkTime = new DateTime($date . ' ' . $timeFirst);
                    $checkTime = $checkTime->format('Y-m-d H:i:s');
                    if ($this->saveAttendance($idfp, $checkTime, self::SFA_DEVICE_ID)) {
                        $scs++;
                    }
                }

                if (!empty($timeLast)) {
                    $checkTime = new DateTime($date . ' ' . $timeLast);
                    $checkTime = $checkTime->format('Y-m-d H:i:s');
                    if ($this->saveAttendance($idfp, $checkTime, self::SFA_DEVICE_ID)) {
                        $scs++;
                    }
                }

            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exceptoin');
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exceptoin');
        }
        return $scs;
    }

    /**
     * Menyimpan data attendance
     *
     * @param int $idpegawai
     * @param string $checkTime
     * @param string $deviceId
     * @return Attendance|false
     */
    public function saveAttendance(int $idpegawai, string $checkTime, string $deviceId = '0')
    {
        if (!$this->validateDate($checkTime)) {
            return false;
        }

        /** @var PerjanjianKerja $perjanjianKerja */
        $perjanjianKerja = PerjanjianKerja::find()->andWhere(['id_pegawai' => $idpegawai])->orderBy('created_at DESC')->one();
        if ($perjanjianKerja) {
            $cekAttendance = Attendance::find()->andWhere('idfp =:id_pegawai AND check_time =:ct', [
                ':id_pegawai' => $idpegawai,
                ':ct' => $checkTime
            ])->one();
            if ($cekAttendance) {
                return false;
            }
            $newAtt = new Attendance();
            $newAtt->scenario = 'save-attendance';
            $newAtt->pegawai_id = $perjanjianKerja->pegawai_id;
            $newAtt->idfp = $idpegawai;
            $newAtt->check_time = $checkTime;
            $newAtt->device_id = $deviceId;
            $newAtt->save();
            return $newAtt;
        }
        return false;
    }

    /**
     * Cek apakah format tanggal benar
     *
     * @param string $date
     * @param string $format
     * @return bool true if valid format(vice versa)
     */
    public function validateDate(string $date, string $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * @param Pegawai $pegawai
     * @param int $dari waktu masuk Y-m-d H:i:s yg sudah dikonversi ke format integer
     * @param int $sampai waktu pulang Y-m-d H:i:s yg sudah dikonversi ke format integer
     * @return array|ActiveRecord[]
     */
    public function getAttendances(Pegawai $pegawai, int $dari, int $sampai)
    {
        $dari = date('Y-m-d H:i:s', $dari);
        $sampai = date('Y-m-d H:i:s', $sampai);

        $getAttendance = self::find();
        $getAttendance->andWhere('pegawai_id =:pegawai', [
            ':pegawai' => $pegawai->id
        ]);

        if ($masterData = $pegawai->masterDataDefaults) {
            if ($masterData->beatroute) {
                $getAttendance->andWhere(['IN', 'device_id', [self::BEATROUTE, self::SFA_DEVICE_ID]]);
            }
        }

        $getAttendance->andWhere(['BETWEEN', 'check_time', $dari, $sampai])
            ->orderBy('check_time ASC');

        return $getAttendance->all();

    }

    /**
     *
     * Mengecek tanggal, apakah tanggal tersebut diantara tanggal yg ditentukan
     *
     * @param string $dateTobeCheck tanggal yg akan di cek
     * @return bool
     */
    public function checkDateRange(string $dateTobeCheck)
    {
        $date = explode('s/d', $this->date);
        $start = strtotime($date[0]);
        $end = strtotime($date[1]);
        $dateTobeCheck = strtotime($dateTobeCheck);
        return $start <= $dateTobeCheck && $end >= $dateTobeCheck;
    }


}

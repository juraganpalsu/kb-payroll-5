<?php

namespace common\models;

use common\models\base\Pegawai as BasePegawai;
use frontend\modules\pegawai\models\AreaKerja;
use frontend\modules\pegawai\models\CostCenter;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiKeluarga;
use frontend\modules\pegawai\models\PegawaiKontakDarurat;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pegawai".
 * @property mixed $sistemKerja_
 *
 * @property string $nama_
 * @property string $nama
 *
 *
 * @property PegawaiSistemKerja $pegawaiSistemKerja
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PerjanjianKerja $perjanjianKerja
 * @property PegawaiGolongan $pegawaiGolongan
 * @property CostCenter $costCenter
 * @property AreaKerja $areaKerja
 * @property string $tanggal
 *
 */
class Pegawai extends BasePegawai
{

    public $tanggal = '';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_lengkap', 'nama_panggilan', 'nama_ibu_kandung', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'status_pernikahan', 'kewarganegaraan', 'agama', 'nomor_ktp', 'no_handphone', 'nomor_kk', 'alamat_email','gol_darah'], 'required'],
            [['nama_file', 'tanggal_lahir', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['has_uploaded_to_att', 'has_created_account', 'has_downloadded_to_training_app', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'ess_status'], 'integer'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama_lengkap', 'nama_ibu_kandung', 'alamat_email'], 'string', 'max' => 225],
            [['alamat_email'], 'email'],
            [['nama_panggilan', 'tempat_lahir', 'kewarganegaraan', 'nomor_kk'], 'string', 'max' => 45],
            [['jenis_kelamin','gol_darah'], 'string', 'max' => 1],
            [['status_pernikahan', 'agama'], 'string', 'max' => 2],
            [['nomor_ktp'], 'string', 'max' => 16],
            [['no_telp', 'no_handphone'], 'string', 'max' => 13],
            [['is_resign', 'lock', 'has_uploaded_to_att', 'has_created_account', 'has_downloadded_to_training_app'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['ktp_provinsi_id', 'ktp_kabupaten_kota_id', 'ktp_kecamatan_id', 'ktp_desa_id', 'domisili_provinsi_id', 'domisili_kabupaten_kota_id', 'domisili_kecamatan_id', 'domisili_desa_id'], 'required'],
            [['ktp_provinsi_id', 'ktp_kabupaten_kota_id', 'ktp_kecamatan_id', 'ktp_desa_id', 'domisili_provinsi_id', 'domisili_kabupaten_kota_id', 'domisili_kecamatan_id', 'domisili_desa_id'], 'integer'],
            [['domisili_alamat', 'ktp_alamat'], 'required'],
            [['domisili_alamat', 'ktp_alamat', 'catatan'], 'string'],
            [['ktp_rw', 'ktp_rt', 'domisili_rw', 'domisili_rt'], 'required'],
            [['ktp_rw', 'ktp_rt', 'domisili_rw', 'domisili_rt'], 'string', 'max' => 3],
            [['nomor_ktp'], 'validasiNoKtp'],
        ];
    }

    /**
     * Validasi white space
     * @param $attribute
     */
    public function validateWhitespace($attribute)
    {
        if (preg_match(' / \s / ', $this->$attribute) == 1) {
            $this->addError($attribute, Yii::t('frontend', 'Tidak diperkenankan terdapat spasi . '));
        }
    }

    /**
     * @param string $attribute
     */
    public function validasiNoKtp(string $attribute)
    {
        $query = Pegawai::find()->andWhere(['nomor_ktp' => $this->$attribute]);
        if ($this->isNewRecord) {
            if (!empty($query->one())) {
                $this->addError($attribute, Yii::t('frontend', 'Maaf, nomor KTP telah terdaftar.'));
            }
        } else {
            if (!empty($query->one())) {
                if (count($query->all()) > 1) {
                    $this->addError($attribute, Yii::t('frontend', 'Maaf, nomor KTP telah terdaftar.'));
                } else {
                    $pegawai = $query->one();
                    if ($pegawai['id'] != $this->id) {
                        $this->addError($attribute, Yii::t('frontend', 'Maaf, nomor KTP telah terdaftar.'));
                    }
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getNama_()
    {
        return $this->nama_lengkap;
    }

    /**
     * @return string
     */
    public function getNama()
    {
        return $this->nama_lengkap;
    }

    /**
     * @return array
     */
    public function getTimeTableDepdrop()
    {
        $timeTable = [];
        $sistemKerja = $this->pegawaiSistemKerja;
        if ($sistemKerja) {
            $sistemKerjaTimeTables = $sistemKerja->sistemKerja->sistemKerjaTimeTables;
            if ($sistemKerjaTimeTables) {
                foreach ($sistemKerjaTimeTables as $sistemKerjas) {
                    $timeTable[] = ['id' => $sistemKerjas->timeTable->id, 'name' => $sistemKerjas->timeTable->nama];
                }
            }
        }
        return $timeTable;
    }


    /**
     *
     * Pegawai sistem kerja saat ini
     *
     * @return ActiveQuery
     */
    public function getPegawaiSistemKerja()
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        return $this->hasOne(PegawaiSistemKerja::class, ['pegawai_id' => 'id'])->andWhere([PegawaiSistemKerja::tableName() . '.deleted_by' => 0])
            ->andWhere('(pegawai_sistem_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_sistem_kerja.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'pegawai_sistem_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_sistem_kerja.akhir_berlaku is null )');

    }

    /**
     * @param string $tanggal
     * @return array|PegawaiSistemKerja|ActiveRecord|null
     */
    public function getPegawaiSistemKerjaUntukTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            return $this->pegawaiSistemKerja;
        }
        $query = PegawaiSistemKerja::find()->andWhere(['pegawai_id' => $this->id])->andWhere('((\'' . $tanggal . '\' >= pegawai_sistem_kerja.mulai_berlaku and \'' . $tanggal . '\' <= pegawai_sistem_kerja.akhir_berlaku) OR (\'' . $tanggal . '\' >= pegawai_sistem_kerja.mulai_berlaku and pegawai_sistem_kerja.akhir_berlaku is null))')->one();
        return $query;
    }


    /**
     *
     * Pegawai Struktur saat ini
     *
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        $tanggal = date('Y-m-d');

        return $this->hasOne(PegawaiStruktur::class, ['pegawai_id' => 'id'])->andWhere([PegawaiStruktur::tableName() . '.deleted_by' => 0])
            ->andWhere('(pegawai_struktur.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_struktur.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'pegawai_struktur.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_struktur.akhir_berlaku is null )');
    }

    /**
     * @param string $tanggal
     * @return array|PegawaiStruktur|ActiveRecord|null
     */
    public function getPegawaiStrukturUntukTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            return $this->pegawaiStruktur;
        }

        return PegawaiStruktur::find()->andWhere(['pegawai_id' => $this->id])
            ->andWhere('(pegawai_struktur.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_struktur.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'pegawai_struktur.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_struktur.akhir_berlaku is null )')->one();
    }

    /**
     *
     * Perjanjian kerja pegawai saat ini
     *
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        $tanggal = date('Y-m-d');
        return $this->hasOne(PerjanjianKerja::class, ['pegawai_id' => 'id'])->andWhere([PerjanjianKerja::tableName() . '.deleted_by' => 0])
            ->andWhere('(perjanjian_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND perjanjian_kerja.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'perjanjian_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND perjanjian_kerja.akhir_berlaku is null )');
    }

    /**
     * @param string $tanggal
     * @return array|PerjanjianKerja|ActiveRecord|null
     */
    public function getPerjanjianKerjaUntukTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            return $this->perjanjianKerja;
        }
        return PerjanjianKerja::find()->andWhere(['pegawai_id' => $this->id])
            ->andWhere('(perjanjian_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND perjanjian_kerja.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'perjanjian_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND perjanjian_kerja.akhir_berlaku is null )')->one();
    }


    /**
     *
     * Golongan pegawai saat ini
     *
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        $tanggal = date('Y-m-d');
        return $this->hasOne(PegawaiGolongan::class, ['pegawai_id' => 'id'])->andWhere([PegawaiGolongan::tableName() . '.deleted_by' => 0])
            ->andWhere('(pegawai_golongan.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_golongan.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'pegawai_golongan.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_golongan.akhir_berlaku is null )');
    }

    /**
     * @param string $tanggal
     * @return array|PegawaiGolongan|ActiveRecord|null
     */
    public function getPegawaiGolonganUntukTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            return $this->pegawaiGolongan;
        }
        return PegawaiGolongan::find()->andWhere(['pegawai_id' => $this->id])
            ->andWhere('(pegawai_golongan.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_golongan.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'pegawai_golongan.mulai_berlaku <=\'' . $tanggal . '\' AND pegawai_golongan.akhir_berlaku is null )')->one();
    }

    /**
     *
     * Cost Center pegawai saat ini
     *
     * @return ActiveQuery
     */
    public function getCostCenter()
    {
        $tanggal = date('Y-m-d');
        return $this->hasOne(CostCenter::class, ['pegawai_id' => 'id'])->andWhere([CostCenter::tableName() . '.deleted_by' => 0])
            ->andWhere('(cost_center.mulai_berlaku <=\'' . $tanggal . '\' AND cost_center.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'cost_center.mulai_berlaku <=\'' . $tanggal . '\' AND cost_center.akhir_berlaku is null )');
    }

    /**
     * @param string $tanggal
     * @return array|CostCenter|ActiveRecord|null
     */
    public function getCostCenterUntukTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            return $this->costCenter;
        }
        return CostCenter::find()->andWhere(['pegawai_id' => $this->id])
            ->andWhere('(cost_center.mulai_berlaku <=\'' . $tanggal . '\' AND cost_center.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'cost_center.mulai_berlaku <=\'' . $tanggal . '\' AND cost_center.akhir_berlaku is null )')->one();
    }

    /**
     *
     * Area Kerja pegawai saat ini
     *
     * @return ActiveQuery
     */
    public function getAreaKerja()
    {
        $tanggal = date('Y-m-d');
        return $this->hasOne(AreaKerja::class, ['pegawai_id' => 'id'])->andWhere([AreaKerja::tableName() . '.deleted_by' => 0])
            ->andWhere('(area_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND area_kerja.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'area_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND area_kerja.akhir_berlaku is null )');
    }

    /**
     * @param string $tanggal
     * @return array|AreaKerja|ActiveRecord|null
     */
    public function getAreaKerjaUntukTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            return $this->areaKerja;
        }
        return AreaKerja::find()->andWhere(['pegawai_id' => $this->id])
            ->andWhere('(area_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND area_kerja.akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'area_kerja.mulai_berlaku <=\'' . $tanggal . '\' AND area_kerja.akhir_berlaku is null )')->one();
    }

    /**
     * @return int
     */
    public function getJumlahAnak()
    {
        $query = PegawaiKeluarga::find()
            ->andWhere(['pegawai_id' => $this->id, 'hubungan' => PegawaiKeluarga::ANAK])->count();
        return (int)$query;
    }

    /**
     * @return int
     */
    public function getJumlahKk()
    {
        $query = PegawaiKeluarga::find()
            ->andWhere(['pegawai_id' => $this->id])->count();
        return (int)$query;
    }

    /**
     * @return int
     */
    public function getJumlahDarurat()
    {
        $query = PegawaiKontakDarurat::find()
            ->andWhere(['pegawai_id' => $this->id])->count();
        return (int)$query;
    }

    /**
     *
     * Mengakomodir karyawan yg resign diantara tanggal awal dan akhir periode payroll
     * (hanya bekerja beberapa hari saja)
     *
     * @param string $tanggalAwal
     * @param string $tanggalAKhir
     * @return array|ActiveRecord|null
     */
    public function getPerjanjianKerjaResign(string $tanggalAwal, string $tanggalAKhir)
    {
        return PerjanjianKerja::find()->andWhere(['pegawai_id' => $this->id])
            ->andWhere(['BETWEEN', 'tanggal_resign', $tanggalAwal, $tanggalAKhir])
            ->orderBy('mulai_berlaku DESC')->one();
    }


    /**
     *
     * Mengakomodir karyawan yg resign diantara tanggal awal dan akhir periode payroll
     * (hanya bekerja beberapa hari saja)
     *
     * @param string $tanggalAwal
     * @param string $tanggalAKhir
     * @return array|PegawaiGolongan|ActiveRecord|null
     */
    public function getPegawaiGolonganResign(string $tanggalAwal, string $tanggalAKhir)
    {
        return PegawaiGolongan::find()->andWhere(['pegawai_id' => $this->id])
            ->andWhere(['BETWEEN', 'mulai_berlaku', $tanggalAwal, $tanggalAKhir])
            ->orderBy('mulai_berlaku DESC')->one();
    }


    /**
     *
     * Mengakomodir karyawan yg resign diantara tanggal awal dan akhir periode payroll
     * (hanya bekerja beberapa hari saja)
     *
     * @param string $tanggalAwal
     * @param string $tanggalAKhir
     * @return array|ActiveRecord|null
     */
    public function getPegawaiStrukturResign(string $tanggalAwal, string $tanggalAKhir)
    {
        return PegawaiStruktur::find()->andWhere(['pegawai_id' => $this->id])
            ->andWhere(['BETWEEN', 'mulai_berlaku', $tanggalAwal, $tanggalAKhir])
            ->orderBy('mulai_berlaku DESC')->one();
    }

    /**
     *
     * Untuk yg join ke table pegawai menggunakan baseModel, maka harus menyertakan ->andWhere([Pegawai::tableName() . '.deleted_by' => 0])
     * Agar yg sudah dihapus tetap terfilter
     *
     * @return ActiveQuery
     *
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

}

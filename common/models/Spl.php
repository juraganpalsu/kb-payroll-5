<?php

namespace common\models;

use common\components\Status;
use common\models\base\Spl as BaseSpl;
use DateTime;
use frontend\models\search\SplSearch;
use frontend\models\status\SplApproval;
use frontend\models\status\SplDetailApproval;
use frontend\models\status\SplStatus;
use frontend\models\User;
use frontend\modules\struktur\models\Helper;
use Throwable;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Query;

/**
 * This is the model class for table "spl".
 *
 *
 *
 * @property string $nama
 * @property string $idPegawai
 * @property string $sistemKerja
 * @property string $golongan
 *
 * @property array $pegawai_ids
 * @property array $pegawai_exist
 *
 * @property SplApproval $approvalByUser
 * @property boolean $showDeleteBtn
 * @property boolean $showReviseBtn
 * @property array $_kategori
 *
 */
class Spl extends BaseSpl
{


    const SPL_APPROVAL_MODUL_ID = 1;

    public $pegawai_ids;
    public $pegawai_exist = [];

    public $showDeleteBtn = false;
    public $showReviseBtn = false;

    const SCENARIO_STANDAR = 'standar';
    const SCENARIO_INSENTIF = 'insentif';

    const LEMBUR_BIASA = 1;
    const LEMBUR_INSENTIF = 2;

    public $_kategori = [self::LEMBUR_BIASA => 'LEMBUR BIASA', self::LEMBUR_INSENTIF => 'LEMBUR INSENTIF'];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'spl_template_id', 'spl_alasan_id','keterangan'], 'required'],
            [['seq_code', 'kategori', 'spl_template_id', 'divisi', 'last_status', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['kategori', 'klaim_quaker', 'tanggal', 'pegawai_ids', 'created_at', 'updated_at', 'deleted_at', 'is_planing', 'is_revision'], 'safe'],
            [['keterangan'], 'string'],
            [['id', 'spl_alasan_id', 'cost_center_id', 'spl_id', 'created_by_pk'], 'string', 'max' => 32],
            [['klaim_quaker', 'lock', 'unit', 'jabatan', 'status_approval', 'cost_center_id'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['tanggal', 'validasiTanggal', 'on' => self::SCENARIO_STANDAR],
            [['kategori'], 'default', 'value' => self::LEMBUR_BIASA]
        ];
    }

    /**
     * @return array|array[]
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_STANDAR] = ['tanggal', 'spl_template_id', 'spl_alasan_id', 'klaim_quaker', 'keterangan'];
        $scenarios[self::SCENARIO_INSENTIF] = ['tanggal', 'spl_template_id', 'spl_alasan_id', 'klaim_quaker', 'keterangan'];
        return $scenarios;
    }


    /**
     * @return string
     */
    public function getSeqCode()
    {
        return 'SPL-' . $this->seq_code;
    }

    /**
     * @param string $attribute
     */
    public function validatePegawai(string $attribute)
    {
        if (count($this->cekPegawaiExist()) == 0) {
            $this->addError($attribute, Yii::t('frontend', 'Semua pegawai telah dibuatkan SPL.'));
        }
    }

    /**
     * Validasi tanggal
     *
     * @param string $attribute
     */
    public function validasiTanggal(string $attribute)
    {
        $error = $this->validasiTanggalHelper();
        if (!empty($error)) {
            $this->addError($attribute, $error);
        }
    }

    /**
     * @return string
     */
    public function validasiTanggalHelper()
    {
        $error = '';
        try {
            if ($this->is_planing) {
                $hPlus7 = new DateTime('+6 days');
                $tglSpl = new DateTime($this->tanggal);
                if ($tglSpl < $hPlus7) {
                    $error = Yii::t('frontend', 'Tanggal harus lebih besar lagi.');
                }
            } else {
                //jika dadakan
                if (!$this->is_revision) {
                    //jika bukan revisi -> yg revisi tidak divalidasi
                    $tanggalSpl = new DateTime($this->tanggal);
                    //ternyata walapun hanya nanpung, tapi tetap ditambahkan (+1)
                    $hPlus = $tanggalSpl->modify('+1 day');
                    if ($tanggalSpl->format('D') == 'Sat') {
                        //perlakuan untuk hari jum'at
                        $hPlus = $tanggalSpl->modify('+3 day');
                    }
                    if ($tanggalSpl->format('D') == 'Sun') {
                        //perlakuan untuk hari sabtu
                        $hPlus = $tanggalSpl->modify('+2 day');
                    }
                    $maxSpl = new DateTime($hPlus->format('Y-m-d') . ' ' . '08:59:59');
                    $saatIni = new DateTime();
                    if ($this->kategori == self::LEMBUR_BIASA) {
                        if ($this->isNewRecord) {
                            if (($saatIni->format('YmdHis') > $maxSpl->format('YmdHis'))) {
                                $error = Yii::t('frontend', 'Tanggal tidak boleh lebih besar dari {tanggal}.', ['tanggal' => $maxSpl->format('d-m-Y H:i:s')]);
                            }
                        } else {
                            if (($saatIni->format('YmdHis') > $maxSpl->format('YmdHis')) && in_array($this->last_status, [Status::OPEN])) {
                                $error = Yii::t('frontend', 'Tanggal tidak boleh lebih besar dari {tanggal}.', ['tanggal' => $maxSpl->format('d-m-Y H:i:s')]);
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return $error;
    }

    /**
     * @return bool|string
     */
    public function generateSeqCode()
    {
        if ($this->isNewRecord) {
            $date = date('ymd');
            $pad = str_pad(1, 3, 0, STR_PAD_LEFT);
            $seqCode = $date . $pad;
            /** @var Spl $lastRow */
            $lastRow = self::find()->select('seq_code')->where('seq_code LIKE \'' . $date . '%\'')->orderBy('seq_code DESC')->one();
            if ($lastRow) {
                $lastSeqCode = substr($lastRow->seq_code, 6, 3) + 1;
                $seqCode = $date . str_pad($lastSeqCode, 3, 0, STR_PAD_LEFT);
            }
            return $seqCode;
        }
        return false;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($insert) {
            $this->seq_code = $this->generateSeqCode();
        }
        return true;
    }


    /**
     * @param bool $insert true if isnewrecord, false if update
     *
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->createStatus(Status::OPEN);
        }
    }


    /**
     * Status yang akan disimpan
     *
     * @param $status
     */
    public function createStatus($status)
    {
        $modeStatus = new SplStatus();
        $modeStatus->status = $status;
        $modeStatus->spl_id = $this->id;
        if ($modeStatus->save()) {
            $this->updateLastStatus($status);
        }
    }


    /**
     *
     * @param int $status Status terakhir yg diterima
     */
    public function updateLastStatus(int $status)
    {
        $model = self::findOne($this->id);
        $model->last_status = $status;
        if ($model->save(false)) {
            if (in_array($model->last_status, [Status::FINISHED, Status::APPROVED, Status::REJECTED])) {
                $model->prosesUlangAbsensi();
            }
        }
    }


    /**
     *
     * User melakukan submit Purchase Requisition
     * - Membuat status purchase requisitions
     * - Membuat status purchase requisition products
     * - Mengirim notifikasi
     * - Membuat daftar user approval
     *
     * @return bool
     */
    public function submit()
    {
        try {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $getUserApprovals = Helper::getRuteApproval($user->id, self::SPL_APPROVAL_MODUL_ID);

            //cek apakah terdapat user approval untuk modul ini
            if (!empty($getUserApprovals)) {
                $this->createStatus(Status::SUBMITED);
                $this->createStatusDetail(Status::SUBMITED);

                $this->createApprovals();
                $this->createApprovalDetails();
            } else {
                $this->createStatusDetail(Status::FINISHED);
                $this->createStatus(Status::FINISHED);
            }

            return true;
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return false;
    }

    /**
     * Menampilkan status terakhir dalam bentuk string
     *
     * @return string
     */
    public function getLastStatus()
    {
        return Status::statuses($this->last_status) ?? '';
    }

    /**
     * Membuat/memperbaharui status 'detail' -> relasi dibawahnya
     *
     * @param $status
     */
    public function createStatusDetail($status)
    {
        array_map(function (SplDetail $x) use ($status) {
            $x->createStatus($status);
        }, $this->splDetails);
    }


    /**
     * Menyimpan data kedalam model table approval
     * -user approval level 0
     * --user approval level 1
     * ---user approval level 2
     * ---dst
     */
    public function createApprovals()
    {
        $approvals = Helper::getRuteApproval(Yii::$app->user->id, self::SPL_APPROVAL_MODUL_ID);
        if (!empty($approvals)) {
            $parentLevel = 0;
            $parentId = '0';
            $oldParentId = '0';
            $urutan = 1;
            foreach ($approvals as $approval) {
                $splApproval = new SplApproval();
                $splApproval->pegawai_id = $approval['pegawai_id'];
                $splApproval->perjanjian_kerja_id = $approval['perjanjian_kerja_id'];
                $splApproval->pegawai_struktur_id = $approval['pegawai_struktur_id'];
                $splApproval->level = $approval['level'];
                $splApproval->spl_id = $this->id;
                if ($parentId == '0') {
                    $splApproval->urutan = $urutan;
                    $splApproval->spl_approval_id = $parentId;
                } else {
                    if ($parentLevel == $splApproval->level) {
                        $splApproval->urutan = $urutan;
                        $splApproval->spl_approval_id = $oldParentId;
                    } else {
                        $urutan++;
                        $splApproval->urutan = $urutan;
                        $splApproval->spl_approval_id = $parentId;
                    }
                }
                if ($splApproval->save()) {
                    $parentId = $splApproval->id;
                    $oldParentId = $splApproval->spl_approval_id;
                    $parentLevel = $splApproval->level;
                }
            }
        }
    }

    /**
     * Menyimpan data kedalam model table approval
     * --detail
     * ---user approval level 0
     * ---user approval level 1
     * ---user approval level 2
     * ---dst
     */
    public function createApprovalDetails()
    {
        foreach ($this->splDetails as $splDetail) {
            $approvals = Helper::getRuteApproval(Yii::$app->user->id, self::SPL_APPROVAL_MODUL_ID);
            if (!empty($approvals)) {
                $parentLevel = 0;
                $parentId = '0';
                $oldParentId = '0';
                $urutan = 1;
                foreach ($approvals as $approval) {
                    $detailApproval = new SplDetailApproval();
                    $detailApproval->pegawai_id = $approval['pegawai_id'];
                    $detailApproval->perjanjian_kerja_id = $approval['perjanjian_kerja_id'];
                    $detailApproval->pegawai_struktur_id = $approval['pegawai_struktur_id'];
                    $detailApproval->level = $approval['level'];
                    $detailApproval->spl_detail_id = $splDetail->id;
                    if ($parentId == '0') {
                        $detailApproval->urutan = $urutan;
                        $detailApproval->spl_detail_approval_id = $parentId;
                    } else {
                        if ($parentLevel == $detailApproval->level) {
                            $detailApproval->urutan = $urutan;
                            $detailApproval->spl_detail_approval_id = $oldParentId;
                        } else {
                            $urutan++;
                            $detailApproval->urutan = $urutan;
                            $detailApproval->spl_detail_approval_id = $parentId;
                        }
                    }
                    if ($detailApproval->save()) {
                        $parentId = $detailApproval->id;
                        $oldParentId = $detailApproval->spl_detail_approval_id;
                        $parentLevel = $detailApproval->level;
                    }
                }
            }
        }
    }

    /**
     * Malakukan check approval semua detail, apakah telah dilakukan aksi approval atau belum
     *
     * Jika semua product telah dilakukan approval oleh user approval, maka return false, karena sudah tidak ada status Status::DEFLT
     * Jika ada walaupun hanya satu item yg belum di approval, maka return true, karena masih ada status Status::DEFLT
     *
     *
     * @param int $status status yang akan dicek, secara default adalah status Status::DEFLT,
     * ini untuk mengecek apakah semua item telah dilakukan approval atau belum
     * Jika diisi paramnya dengan Status::APPROVE, maka akan cek apakah ada salah satu product item yg di approve
     * Jika ada satu item yg sesuai dengan param, maka return true, sebaliknya false
     *
     * @return bool
     */
    public function checkStatusApprovalByUser(int $status = Status::DEFLT)
    {
        $approvalTypes = [];
        array_map(function (SplDetail $x) use (&$approvalTypes) {
            if ($x->approvalByUser) {
                $approvalTypes[] = $x->approvalByUser->tipe;
            }
        }, $this->splDetails);
        if (in_array($status, $approvalTypes)) {
            return true;
        }
        return false;
    }

    /**
     * Cek status approval pada suatu detail
     *
     * @param int $status
     * @return bool true jika ada yg sesuai dengan param, sebaliknya false
     */
    public function checkStatusApprovals(int $status = Status::REJECT)
    {
        $model = self::findOne($this->id);
        $rejects = [];
        array_map(function (SplDetail $x) use (&$rejects, $status) {
            $rejects[] = $x->checkStatusApprovals($status);
        }, $model->splDetails);
        if (in_array(true, $rejects)) {
            return true;
        }
        return false;
    }

    /**
     * Melakukan aproval items
     *
     * @param int $status
     * @param string $comment
     */
    public function approval(int $status, string $comment = '')
    {
        array_map(function (SplDetail $x) use ($status, $comment) {
            $x->approval($status, $comment);
        }, $this->splDetails);
    }

    /**
     * User pembuat pR akan mendapat notifikasi saat PR di approve
     *
     * @return ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Mengambil item yg telah di APPROVE dan atau item yg tidak menggunakan approval
     *
     * @return array|SplDetail[]
     */
    public function getDetailApproved()
    {
        return array_filter($this->splDetails, function (SplDetail $detail) {
            return (in_array($detail->last_status, [Status::APPROVED, Status::FINISHED, Status::CLOSED]));
        });
    }

    /**
     * @return int jumlah PR dengan status
     */
    public function countSubmited()
    {
        return count(array_filter(Spl::find()->all(), function (Spl $x) {
            return ($x->last_status == Status::SUBMITED);
        }));
    }

    /**
     * Check apakah semua detail telah selesai
     *
     * @return bool
     */
    public function checkAllItemHasBeenClosed()
    {
        $status = [];
        array_map(function (SplDetail $detail) use (&$status) {
            $status[] = $detail->last_status ?? 0;
        }, $this->splDetails);
        $status = array_unique($status);
        if (count($status) == 1 && array_shift($status) == Status::CLOSED) {
            return true;
        }
        return false;
    }


    /**
     * @return array|ActiveRecord[]
     */
    public function getSplTemplateForDropDown()
    {
        $query = SplTemplate::find()->all();
        $datas = [];
        /** @var SplTemplate $data */
        foreach ($query as $data) {
            $datas[$data->id] = $data->nama . ', ' . $data->kategori_ . ' - ' . $data->masuk . ' s/d ' . $data->pulang;
        }
        return $datas;
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function getTemplateInsentifForDropDown()
    {
        $query = SplTemplate::find()->andWhere(['like', 'nama', 'Insentif'])->all();
        $datas = [];
        /** @var SplTemplate $data */
        foreach ($query as $data) {
            $datas[$data->id] = $data->nama . ', ' . $data->kategori_ . ' - ' . $data->masuk . ' s/d ' . $data->pulang;
        }
        return $datas;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'pegawai_ids' => Yii::t('app', 'Pegawai')
        ]);
    }

    /**
     * Cek apakah seorang pegawai telah dibuatkan SPL
     * Jika telah dibuatkan SPL, maka cek apakah SPL awal atau akhir, jika ditemukan tipe yg sama, maka tidak disimpan
     *
     * update 20-02-2019 Tidak bisa double SPL dalam satu hari, karena engine tidak mendukung
     *
     */
    public function cekPegawaiExist()
    {
        $model = SplDetail::find()
            ->joinWith(['spl' => function (ActiveQuery $spl) {
                $spl->andWhere(['tanggal' => $this->tanggal]);
            }])
            ->andWhere(['IN', 'pegawai_id', $this->pegawai_ids])
            ->all();
        $exist = [];
        $submit = $this->pegawai_ids;
        if ($model) {
            /** @var SplDetail $detail */
            foreach ($model as $detail) {
                if ($detail->last_status != Status::REJECTED) {
                    array_push($exist, $detail->pegawai_id);
                }
            }
            $this->pegawai_ids = array_diff($submit, $exist);
        }
        return $this->pegawai_ids;
    }


    /**
     * @return array
     */
    public function createDetail()
    {
        $savedDetail = [];
        foreach ($this->pegawai_ids as $detail) {
            if ($modelPegawai = \frontend\modules\pegawai\models\Pegawai::findOne($detail)) {
                $modelPerjanjianKerja = $modelPegawai->getPerjanjianKerjaUntukTanggal($this->tanggal);
                $modelPegawaiStruktur = $modelPegawai->getPegawaiStrukturUntukTanggal($this->tanggal);
                if ($modelPerjanjianKerja && $modelPegawaiStruktur) {
                    $splDetail = new SplDetail();
                    $splDetail->pegawai_id = $modelPegawai->id;
                    $splDetail->pegawai_struktur_id = $modelPegawaiStruktur->id;
                    $splDetail->perjanjian_kerja_id = $modelPerjanjianKerja->id;
                    $splDetail->spl_id = $this->id;
                    if ($splDetail->save()) {
                        array_push($savedDetail, $splDetail->pegawai_id);
                    }
                }
            }
        }
        return $savedDetail;
    }

    /**
     * @return string
     */
    public function getNama()
    {
        $datas = array_map(function (SplDetail $dt) {
            return $dt->pegawaiStruktur ? $dt->pegawaiStruktur->pegawai->nama . '[' . $dt->pegawaiStruktur->struktur->nameCostume . ']' : '';
        }, $this->splDetails);
        return implode("<br>", $datas);
    }

    /**
     * @return string
     */
    public function getIdPegawai()
    {
        $datas = array_map(function (SplDetail $dt) {
            return $dt->perjanjianKerja ? $dt->perjanjianKerja->id_pegawai : '';
        }, $this->splDetails);
        return implode("<br>", $datas);
    }

    /**
     * @param string $tanggal
     * @return string
     */
    public function getGolongan(string $tanggal)
    {
        $datas = array_map(function (SplDetail $dt) use ($tanggal) {
            if ($pegawai = $dt->pegawai) {
                if ($golongan = $pegawai->getPegawaiGolonganUntukTanggal($tanggal)) {
                    return $golongan->golongan->nama;
                }
            }
            return '';
        }, $this->splDetails);
        return implode("\n", $datas);
    }

    /**
     * @return string
     */
    public function getSistemKerja()
    {
        $datas = array_map(function (SplDetail $dt) {
            return $dt->pegawai ? $dt->pegawai->getPegawaiSistemKerjaUntukTanggal($dt->spl->tanggal) ? $dt->pegawai->getPegawaiSistemKerjaUntukTanggal($dt->spl->tanggal)->sistemKerja->nama : '' : '';
        }, $this->splDetails);
        return implode("\n", $datas);
    }

    /**
     * @param string $tanggal tanggal spl
     * @param Pegawai $pegawai pegawai yang berada pada SplDetail
     *
     * @param array $approvalType
     * @return array|ActiveRecord[]|Spl
     */
    public function getSplByPegawai(string $tanggal, Pegawai $pegawai, array $approvalType = [Status::DEFLT])
    {
        $querySpl = self::find()
            ->andWhere('tanggal =:tanggal', [':tanggal' => $tanggal]);

        $querySpl->joinWith(['splDetails' => function (ActiveQuery $q) use ($pegawai, $approvalType) {
            $q->andWhere(['IN', 'spl_detail.last_status', $approvalType]);
            $q->andWhere('spl_detail.pegawai_id =:pegawai_id', [':pegawai_id' => $pegawai->id]);
        }]);
        return $querySpl->one();
    }


    /**
     * @return array
     * @throws Exception
     */
    public function getPegawaiSelect2()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $query = new Query();
        $query->select(['p.id as id', 'pk.id_pegawai', 'p.nama_lengkap', 'CONCAT(p.nama_lengkap, \' - \', pk.id_pegawai) AS text'])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'pegawai_struktur ps', 'p.id = ps.pegawai_id AND ((ps.mulai_berlaku <=\'' . date('Y-m-d') . '\' AND ps.akhir_berlaku >= \'' . date('Y-m-d') . '\'' . ') OR (' . 'ps.mulai_berlaku <=\'' . date('Y-m-d') . '\' AND ps.akhir_berlaku is null)) AND ps.struktur_id IN (' . implode(', ', $user->getStrukturBawahan()) . ')')
            ->join('LEFT JOIN', 'perjanjian_kerja pk', 'p.id = pk.pegawai_id AND ((pk.mulai_berlaku <=\'' . date('Y-m-d') . '\' AND pk.akhir_berlaku >= \'' . date('Y-m-d') . '\'' . ') OR (' . 'pk.mulai_berlaku <=\'' . date('Y-m-d') . '\' AND pk.akhir_berlaku is null))');
//            ->where('CONCAT( p.nama_lengkap, pk.id_pegawai) LIKE \'%' . $queryString . '%\'');
        $query->andWhere(['p.deleted_by' => 0, 'ps.deleted_by' => 0, 'pk.deleted_by' => 0]);
        $query->limit(20);
        $command = $query->createCommand();
        return $command->queryAll();
    }

    /**
     * @return array
     */
    public function getListPegawai()
    {
        $available = [];
        $assigned = [];
        try {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $query = new Query();
            $query->select(['p.id as id', 'pk.id_pegawai', 'p.nama_lengkap', 'CONCAT(p.nama_lengkap, \' - \', pk.id_pegawai) AS text'])
                ->from('pegawai p')
                ->join('LEFT JOIN', 'pegawai_struktur ps', 'p.id = ps.pegawai_id AND ((ps.mulai_berlaku <=\'' . date('Y-m-d') . '\' AND ps.akhir_berlaku >= \'' . date('Y-m-d') . '\'' . ') OR (' . 'ps.mulai_berlaku <=\'' . date('Y-m-d') . '\' AND ps.akhir_berlaku is null)) AND ps.struktur_id IN (' . implode(', ', $user->getStrukturBawahan()) . ')')
                ->join('LEFT JOIN', 'perjanjian_kerja pk', 'p.id = pk.pegawai_id AND ((pk.mulai_berlaku <=\'' . date('Y-m-d') . '\' AND pk.akhir_berlaku >= \'' . date('Y-m-d') . '\'' . ') OR (' . 'pk.mulai_berlaku <=\'' . date('Y-m-d') . '\' AND pk.akhir_berlaku is null))');
            $query->andWhere(['p.deleted_by' => 0, 'ps.deleted_by' => 0, 'pk.deleted_by' => 0, 'is_resign' => 0]);
            $command = $query->createCommand();

            foreach ($command->queryAll() as $item) {
                $available[$item['id']] = $item['text'];
            }

            $assigned = [];
            foreach ($this->splDetails as $detail) {
                $assigned[$detail->pegawai_id] = $detail->pegawai->namaIdPegawai;
                unset($available[$detail->pegawai_id]);
            }


        } catch (Exception $e) {
        }
        return [
            'available' => $available,
            'assigned' => $assigned,
        ];
    }


    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalByUser()
    {
        return $this->hasOne(SplApproval::class, ['spl_id' => 'id'])->andWhere(['spl_approval.pegawai_id' => User::pegawai()->id]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function prosesUlangAbsensi()
    {
        $pegawaiIds = [];
        foreach ($this->splDetails as $detail) {
            $pegawaiIds[] = $detail->pegawai_id;
        }
        $prosesInfo = [];
        if (!empty($pegawaiIds)) {
            $modelAbsensi = new Absensi();
            $prosesInfo = $modelAbsensi->prosesData($this->tanggal, $this->tanggal, null, $pegawaiIds);

        }
        return $prosesInfo;
    }


    /**
     * @param string $komentar
     */
    public function createApproval(string $komentar = '')
    {
        if (!$this->checkStatusApprovalByUser()) {
            if ($this->checkStatusApprovalByUser(Status::APPROVE)) {
                $this->approvalByUser->createApproval(Status::APPROVE);
            } else {
                $this->approvalByUser->createApproval(Status::REJECT, $komentar);
            }
        }
    }

    /**
     * @return int
     */
    public static function jumlahSpl(): int
    {
        if (User::pegawai()) {
            $modelSplSearch = new SplSearch();
            $count = $modelSplSearch->searchApproval(false)->count();
            if ($count > 0) {
                return $count;
            }
        }
        return 0;
    }


    /**
     * @return bool
     */
    public function setAsRevisi()
    {
        try {
            $this->createStatusDetail(Status::REVISED);
            $this->createStatus(Status::REVISED);
            return true;
        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return false;
    }

    /**
     *
     */
    public function buatRevisiDetail()
    {
        $splDetailOld = self::findOne($this->spl_id);
        foreach ($splDetailOld->splDetails as $splDetail) {
            if ($splDetail->last_status == Status::REVISED) {
                $modelDetail = new SplDetail();
                $modelDetail->pegawai_id = $splDetail->pegawai_id;
                $modelDetail->spl_id = $this->id;
                $modelDetail->perjanjian_kerja_id = $splDetail->perjanjian_kerja_id;
                $modelDetail->pegawai_struktur_id = $splDetail->pegawai_struktur_id;
                if (empty($modelDetail->cekPegawaiTelahDibuatkanSpl($splDetail->spl->tanggal))) {
                    $modelDetail->save();
                }
            }
        }
    }

    /**
     * @param array $pegawais
     * @return int
     */
    public function createDetails(array $pegawais)
    {
        $savedDetail = [];
        $tanggal = $this->tanggal;
        foreach ($pegawais as $pegawaiId) {
            if ($modelPegawai = \frontend\modules\pegawai\models\Pegawai::findOne($pegawaiId)) {
                $modelPerjanjianKerja = $modelPegawai->getPerjanjianKerjaUntukTanggal($tanggal);
                $modelPegawaiStruktur = $modelPegawai->getPegawaiStrukturUntukTanggal($tanggal);
                if ($modelPerjanjianKerja && $modelPegawaiStruktur) {
                    $modelDetail = SplDetail::find()->joinWith(['spl' => function (ActiveQuery $query) use ($tanggal) {
                        $query->andWhere(['tanggal' => $tanggal]);
                    }])->andWhere(['pegawai_id' => $pegawaiId])->all();
                    if (empty($modelDetail)) {
                        $splDetail = new SplDetail();
                        $splDetail->pegawai_id = $modelPegawai->id;
                        $splDetail->pegawai_struktur_id = $modelPegawaiStruktur->id;
                        $splDetail->perjanjian_kerja_id = $modelPerjanjianKerja->id;
                        $splDetail->spl_id = $this->id;
                        if ($splDetail->save()) {
                            array_push($savedDetail, $splDetail->pegawai_id);
                        }
                    }
                }
            }
        }
        return count($savedDetail);
    }

    /**
     * @param array $pegawais
     * @return int
     */
    public function deleteDetails(array $pegawais)
    {
        $deletedDetail = [];
        foreach ($pegawais as $pegawaiId) {
            $modelDetail = SplDetail::findOne(['spl_id' => $this->id, 'pegawai_id' => $pegawaiId]);
            if ($modelDetail) {
                try {
                    if ($modelDetail->delete()) {
                        $deletedDetail[] = $pegawaiId;
                    }
                } catch (Throwable $e) {
                    Yii::info($e->getMessage(), 'exception');
                }
            }
        }
        return count($deletedDetail);
    }

}
<?php

namespace common\models;

use common\models\base\PerubahanStatusAbsensiDetail as BasePerubahanStatusAbsensiDetail;

/**
 * This is the model class for table "perubahan_status_absensi_detail".
 */
class PerubahanStatusAbsensiDetail extends BasePerubahanStatusAbsensiDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['perubahan_status_absensi_id', 'pegawai_id'], 'required'],
            [['urut', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'perubahan_status_absensi_id', 'pegawai_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        if ($insert) {
            $this->urut = $this->generateUrut();
        }
        return true;
    }

    /**
     * @return int|string
     */
    private function generateUrut()
    {
        $countRows = (new \yii\db\Query())
            ->select(['id'])
            ->from(static::tableName())
            ->count();
        return $countRows + 1;
    }

}

<?php

namespace common\models;

use common\models\base\SistemKerjaTimeTable as BaseSistemKerjaTimeTable;

/**
 * This is the model class for table "sistem_kerja_time_table".
 */
class SistemKerjaTimeTable extends BaseSistemKerjaTimeTable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sistem_kerja_id', 'time_table_id'], 'required'],
            [['sistem_kerja_id', 'time_table_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            ['time_table_id', 'unique', 'targetAttribute' => ['sistem_kerja_id', 'time_table_id'], 'message' => '{attribute} sudah ada.']
        ];
    }

}

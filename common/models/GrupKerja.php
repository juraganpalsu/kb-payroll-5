<?php

namespace common\models;

use common\models\base\GrupKerja as BaseGrupKerja;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "grup_kerja".
 *
 * @property integer $unit_id
 */
class GrupKerja extends BaseGrupKerja
{

    public $unit_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['divisi_id', 'nama'], 'required'],
            [['divisi_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['nama'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * Mencari grup kerja menggunakan dep drop
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getGrupKerjaDepdrop()
    {
        $query = self::find()
            ->select('grup_kerja.id, grup_kerja.nama AS name, grup_kerja.divisi_id, divisi.unit_id')
            ->joinWith(['divisi' => function ($divisi) {
                /** @var ActiveQuery $divisi */
                $divisi->andWhere(['unit_id' => $this->unit_id]);
            }]);
        if (!is_null($this->divisi_id)) {
            $query->andWhere(['divisi_id' => $this->divisi_id]);
        }

        return $query->asArray()->all();
    }

}

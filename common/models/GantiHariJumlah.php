<?php

namespace common\models;

use common\models\base\GantiHariJumlah as BaseGantiHariJumlah;

/**
 * This is the model class for table "ganti_hari_jumlah".
 */
class GantiHariJumlah extends BaseGantiHariJumlah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pegawai_id'], 'required'],
            [['jumlah_hutang'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['pegawai_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}

<?php

namespace common\models;

use common\models\base\Libur as BaseLibur;

/**
 * This is the model class for table "libur".
 */
class Libur extends BaseLibur
{

    //jenis libur
    const LIBUR_NASIONAL = 1;
    const CUTI_BERSAMA = 2;

    public $_jenis_libur = [self::LIBUR_NASIONAL => 'Libur Nasional', self::CUTI_BERSAMA => 'Cuti Bersama'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
            [
                [['jenis', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
                [['tanggal'], 'required'],
                [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
                [['keterangan'], 'string'],
                [['lock'], 'default', 'value' => '0'],
                [['lock'], 'mootensai\components\OptimisticLockValidator']
            ];
    }


    /**
     * @return mixed
     */
    public function getJenisLibur()
    {
        return $this->_jenis_libur[$this->jenis];
    }


    /**
     * Cek apakah tanggal merah atau cuti bersama
     *
     * @param string $tanggal
     * @param int|string $jenisLibur
     * @return bool
     */
    public static function cekLiburByJenis($tanggal, $jenisLibur = '')
    {
        $getLibur = Libur::find()
            ->andWhere('tanggal =:tanggal', [
                ':tanggal' => $tanggal
            ]);
        if (!empty($jenisLibur)) {
            $getLibur->andWhere('jenis =:jenis', [':jenis' => $jenisLibur]);
        }

        $getLibur = $getLibur->one();
        if ($getLibur) {
            return true;
        }
        return false;
    }


}

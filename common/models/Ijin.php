<?php

namespace common\models;

use common\models\base\Ijin as BaseIjin;

/**
 * This is the model class for table "ijin".
 *
 *
 *
 * @property array $_tipe
 * @property string $tipe_
 * @property string $shift_
 */
class Ijin extends BaseIjin
{


    public $pegawai_ids;
    public $pegawai_exist = [];

    const SURAT_DOKTER = 1;
    const IJIN = 2;
    const CUTI = 3;
    const DISPENSASI = 4;
    const SETENGAH_HARI = 5;
    const OFF = 6;
    const TUGAS_KANTOR = 7;
    const CUTI_MELAHIRKAN = 8;
    const IZIN_SAKIT = 9;
    const IZIN_KELUAR = 10;
    const TUGAS_LUAR_KOTA = 11;
    const MATI_LAMPU = 12;
    const ATL = 13;
    const TUKAR_LIBUR = 14;
    const IZIN_TERLAMBAT = 15;
    const BERITA_ACARA = 16;

    public $_tipe = [self::SURAT_DOKTER => 'Surat Dokter', self::IJIN => 'Ijin Tidak Dibayar', self::CUTI => 'Cuti',
        self::DISPENSASI => 'Dispensasi', self::SETENGAH_HARI => 'Setengah Hari', self::OFF => 'Off', self::TUGAS_KANTOR => 'Tugas Kantor',
        self::CUTI_MELAHIRKAN => 'Cuti Melahirkan', self::IZIN_SAKIT => 'Izin Sakit', self::IZIN_KELUAR => 'Izin Keluar',
        self::TUGAS_LUAR_KOTA => 'Tugas Luar Kota', self::MATI_LAMPU => 'Mati Lampu', self::ATL => 'ATL', self::TUKAR_LIBUR => 'Tukar Libur',
        self::IZIN_TERLAMBAT => 'Izin Terlambat', self::BERITA_ACARA => 'Berita Acara'];


    /**
     * @inheritdoct
     */
    public function rules()
    {
        return
            [
                [['tipe', 'tanggal', 'pegawai_ids'], 'required'],
                [['shift', 'tipe', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
                [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
                [['pegawai_id'], 'string', 'max' => 32],
                [['keterangan'], 'string', 'max' => 45],
                [['lock'], 'default', 'value' => 0],
                [['lock'], 'mootensai\components\OptimisticLockValidator']
            ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['newmodel'] = ['tipe', 'tanggal', 'keterangan', 'shift'];
        return $scenario;
    }

    /**
     * @return array|mixed tipe ijin
     *
     */
    public function getTipe_()
    {
        return $this->_tipe[$this->tipe];
    }

    /**
     * @return string
     */
    public function getShift_()
    {
        return is_null($this->shift) ? '-' : $this->_shift()[$this->shift];
    }

    /**
     * @return array
     */
    public function _shift()
    {
        $modelTimeTable = new TimeTable();
        return $modelTimeTable->_shift;
    }


    /**
     * Create ijin
     */
    public function create()
    {
        $savedPegawais = [];
        foreach ($this->pegawai_ids as $pegawai_id) {
            $check = Ijin::findOne(['pegawai_id' => $pegawai_id, 'tanggal' => $this->tanggal]);
            if (!$check) {
                $newModel = new Ijin();
                $newModel->scenario = 'newmodel';
                $newModel->attributes = $this->attributes;
                $newModel->pegawai_id = $pegawai_id;
                if ($newModel->save()) {
                    $savedPegawais[] = $newModel->pegawai_id;
                }
            }
        }
        if ($savedPegawais) {
            $this->prosesData($savedPegawais);
        }
    }

    /**
     * @param array $pegawaiids
     */
    public function prosesData(array $pegawaiids)
    {
        $modelAbsensi = new Absensi();
        try {
            $modelAbsensi->prosesData($this->tanggal, $this->tanggal, null, $pegawaiids);
        } catch (\Exception $e) {
            \Yii::info($e->getMessage(), 'exception');
        }
    }


}

<?php

namespace common\models;

use common\components\Hari;
use common\models\base\PolaJadwalLibur as BasePolaJadwalLibur;
use yii\helpers\Json;

/**
 * This is the model class for table "pola_jadwal_libur".
 */
class PolaJadwalLibur extends BasePolaJadwalLibur
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sistem_kerja_id'], 'required'],
            [['sistem_kerja_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['hari', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['masuk', 'libur'], 'integer', 'max' => 6],
//            [['hari'], 'string', 'max' => 45],
            [['masuk', 'libur', 'lock'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function beforeValidate()
    {
        return $this->hari = Json::encode($this->hari);
    }

    public function getHari_()
    {
        $hari = Json::decode($this->hari);
        if (!empty($hari)) {
            return implode(', ', Hari::getHarisIndonesia($hari));
        }
        return '';
    }

}

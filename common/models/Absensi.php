<?php

namespace common\models;

use common\components\Status;
use common\models\base\Absensi as BaseAbsensi;
use DateInterval;
use DateTime;
use Exception;
use frontend\modules\izin\models\AbsensiTidakLengkap;
use frontend\modules\izin\models\Cuti;
use frontend\modules\izin\models\IzinJenis;
use frontend\modules\izin\models\IzinTanpaHadir;
use frontend\modules\izin\models\SetengahHari;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "absensi".
 * @property mixed $ijin
 * @property mixed $statusKehadiran_
 *
 * @property array $_status_kehadiran
 *
 * @property SistemKerja $sistemKerja
 */
class Absensi extends BaseAbsensi
{
    /**sys
     * Status Kehadiran
     */
    const ALFA = 1;
    const MASUK = 2;
    const IJIN = 3;
    const LIBUR = 4;
    const ERROR = 5;
    const CUTI = 6;

    public $check_time = ['masuk' => '', 'pulang' => ''];

    /**
     * @var array status kehadiran
     */
    public $_status_kehadiran = [
        self::ALFA => 'Alfa',
        self::MASUK => 'Masuk',
        self::IJIN => 'Ijin',
        self::LIBUR => 'Libur',
        self::ERROR => 'Undefined',
        self::CUTI => 'Cuti',
    ];

    //public $_pengaliUangMakanLembur;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
            [
                [['masuk', 'telat', 'istirahat', 'masuk_istirahat', 'pulang', 'tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
                [['tanggal', 'pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id'], 'required'],
                [['time_table_id', 'telat', 'spl_template_id', 'pegawai_sistem_kerja_id', 'status_kehadiran', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
                [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_golongan_id', 'cost_center_id', 'spl_id'], 'safe'],
                [['jumlah_jam_lembur'], 'number'],
                [['lock', 'telat'], 'default', 'value' => '0'],
                [['lock'], 'mootensai\components\OptimisticLockValidator'],
                [['tanggal', 'pegawai_id'], 'unique', 'targetAttribute' => ['tanggal', 'pegawai_id']]
            ];
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws Exception
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        $this->telat = $this->hitungTelat();
        return true;
    }

    /**
     * @return int
     */
    public function kategoriSpl()
    {
        $kategori = 0;
        $splTemplate = $this->splTemplate;
        if ($splTemplate) {
            $kategori = $splTemplate->kategori;
        }
        return $kategori;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function hitungTelat()
    {
        $telat = 0;
        // Hitung telat masuk dan ada SPKL Lembur Akhir
        if ($this->status_kehadiran == Absensi::MASUK && $this->kategoriSpl() == 0 || $this->kategoriSpl() == splTemplate::LEMBUR_AKHIR) {
            if ($timeTable = $this->timeTable) {
                //cek masuk masuk dan masuk maksimumnya apakah beda hari
//                if ($timeTable->masuk < $timeTable->masuk_maksimum) {
                $timeTableMasuk = strtotime($this->tanggal . ' ' . $timeTable->masuk);
                $aktualMasuk = strtotime($this->masuk);
                if ($aktualMasuk > $timeTableMasuk) {
                    //Rounds a number down to the nearest integer
                    $telatDariMasuk = floor(($aktualMasuk - $timeTableMasuk) / 60);
                    if ($telatDariMasuk > $timeTable->toleransi_masuk) {
                        $telat = $telatDariMasuk - $timeTable->toleransi_masuk;
                    }
                    if ($telatDariMasuk == $timeTable->toleransi_masuk) {
                        $telat = 1;
                    }
                }
            }
        }
        // Hitung telat masuk dan ada SPKL Lembur Khusus dan Lembur Awal
        if ($this->status_kehadiran == Absensi::MASUK && $this->kategoriSpl() == splTemplate::LEMBUR_KHUSUS || $this->kategoriSpl() == splTemplate::LEMBUR_AWAL) {
            if ($splTemplate = $this->splTemplate) {
                //cek masuk masuk dan masuk maksimumnya apakah beda hari
//                if ($splTemplate->masuk < $splTemplate->masuk_maksimum) {
                $splTemplateMasuk = strtotime($this->tanggal . ' ' . $splTemplate->masuk);
                $aktualMasuk = strtotime($this->masuk);
                if ($aktualMasuk > $splTemplateMasuk) {
                    $telatDariMasuk = floor(($aktualMasuk - $splTemplateMasuk) / 60);
                    if ($telatDariMasuk > $splTemplate->toleransi_masuk) {
                        $telat = $telatDariMasuk - $splTemplate->toleransi_masuk;
                    }
                    if ($telatDariMasuk == $splTemplate->toleransi_masuk) {
                        $telat = 1;
                    }
                }
//                }
            }
        }
        return $telat;
    }

    /**
     * @return array|mixed
     */
    public function getStatusKehadiran_()
    {
        if (!empty($this->_status_kehadiran)) {
            if ($this->status_kehadiran == Absensi::IJIN) {
                $izinTanpaHadir = $this->getIzinTanpaHadir();
                if ($izinTanpaHadir) {
                    return $izinTanpaHadir;
                }
                $absensiTidakLengkap = $this->getIzinAbsensiTidakLengkap();
                if ($absensiTidakLengkap) {
                    return $absensiTidakLengkap;
                }
                $setengahHari = $this->getIzinSetengahHari();
                if ($setengahHari) {
                    return $setengahHari;
                }
            }
            return $this->_status_kehadiran[$this->status_kehadiran];
        }
        return $this->_status_kehadiran;
    }

    /**
     * Helper -> agar pemanggilannya lebih simple.
     *
     * @param string $tanggal
     * @param Pegawai $pegawai
     * @param array $approvalType
     * @return array|ActiveRecord[]|Spl
     */
    public static function getSpl(string $tanggal, Pegawai $pegawai, array $approvalType = [Status::FINISHED, Status::APPROVED])
    {
        $model = new Spl();
        return $model->getSplByPegawai($tanggal, $pegawai, $approvalType);
    }

    /**
     * * Memproses data staf HO
     * -tanggal
     * --pegaawai
     * ---time table
     * ----presensi
     *
     * @param string $tanggal_awal
     * @param string $tanggal_akhir
     * @param null $sistemKerja
     * @param array $pegawai_ids
     *
     * @return array
     * @throws Exception
     */
    public function prosesData(string $tanggal_awal, string $tanggal_akhir, $sistemKerja = null, array $pegawai_ids = [])
    {
        $tanggal_awal = new DateTime($tanggal_awal);
        $tanggal_akhir = new DateTime($tanggal_akhir);
        $timeTable = new TimeTable();
        $info = [];
        while ($tanggal_awal <= $tanggal_akhir) {

            $tanggal = $tanggal_awal->format('Y-m-d');
//            echo "++++++++++++++++++++++++++++++++ \n";
//            echo "Proses untuk tanggal $tanggal \n";
//            echo "++++++++++++++++++++++++++++++++ \n";

            $getPegawais = Pegawai::find();

            if (!empty($pegawai_ids)) {
                $getPegawais->andWhere(['IN', 'id', $pegawai_ids]);
            }
            $getPegawais = $getPegawais->all();
            /** @var Pegawai $pegawai */
            foreach ($getPegawais as $pegawai) {

                $pegawaiSistemKerja = $pegawai->getPegawaiSistemKerjaUntukTanggal($tanggal);

                //cek apakah punya sistem kerja yang masih aktif
                if (is_null($pegawaiSistemKerja)) {
                    continue;
                }

                $perjanjianKerja = $pegawai->getPerjanjianKerjaUntukTanggal($tanggal);

                //cek apakah pegawai mempunyai perjanjian kerja aktif
                if (is_null($perjanjianKerja)) {
                    continue;
                }

                $pegawaiStruktur = $pegawai->getPegawaiStrukturUntukTanggal($tanggal);

                //cek apakah ada struktur yg masih aktif
                if (is_null($pegawaiStruktur)) {
                    continue;
                }

                $pegawaiCostCenter = $pegawai->getCostCenterUntukTanggal($tanggal);

                $costCenterId = '0';
                if (!is_null($pegawaiCostCenter)) {
                    $costCenterId = $pegawaiCostCenter->id;
                }

                $pegawaiGolongan = $pegawai->getPegawaiGolonganUntukTanggal($tanggal);

                $pegawaiGolonganId = '0';
                if (!is_null($pegawaiGolongan)) {
                    $pegawaiGolonganId = $pegawaiGolongan->id;
                }

                //cek apakah hanya akan memproses sistem kerja tertentu
                if (!is_null($sistemKerja) && $sistemKerja != $pegawaiSistemKerja->sistem_kerja_id) {
                    continue;
                }


                if (PerubahanStatusAbsensi::getPerubahanAbsensiByPegawaiDanTanggal($pegawai->id, $tanggal)) {
                    continue;
                }

                /** @var Pegawai $pegawai */
                $model = new Absensi();
                $model->tanggal = $tanggal;
                $model->pegawai_id = $pegawai->id;
                $cekModel = self::findOne(['tanggal' => $tanggal, 'pegawai_id' => $pegawai->id]);
                if ($cekModel) {
                    $model = $cekModel;
                }
                $model->perjanjian_kerja_id = $perjanjianKerja->id;
                $model->pegawai_struktur_id = $pegawaiStruktur->id;
                $model->cost_center_id = $costCenterId;
                $model->pegawai_golongan_id = $pegawaiGolonganId;
                $model->pegawai_sistem_kerja_id = $pegawaiSistemKerja->id;
                $model->spl_id = '0';
                $model->time_table_id = '';
                $model->masuk = '';
                $model->pulang = '';
                $model->jumlah_jam_lembur = '';


//                $spl = self::getSpl($model->tanggal, $pegawai, ApprovalType::DEFLT);
//
//                //Jika ada SPL dan spl belum diapprove, maka status langsung error
//                if ($spl && $spl->status_approval == ApprovalType::DEFLT) {
//                    $model->status_kehadiran = Absensi::ERROR;
//                    if ($model->save()) {
//                        continue;
//                    }
//                }

                $spl = self::getSpl($model->tanggal, $pegawai);

                $ijin = null;

                $queryIzinTanpaHadir = IzinTanpaHadir::find()
                    ->andWhere(['pegawai_id' => $pegawai->id])
                    ->andWhere('\'' . $model->tanggal . '\' BETWEEN tanggal_mulai AND tanggal_selesai OR tanggal_mulai = \'' . $model->tanggal . '\' OR tanggal_selesai = \'' . $model->tanggal . '\'')
                    ->one();

                if ($queryIzinTanpaHadir) {
                    $ijin = $queryIzinTanpaHadir;
                }

                $queryIzinAbsensiTidakLengkap = AbsensiTidakLengkap::find()->andWhere(['pegawai_id' => $pegawai->id, 'tanggal' => $model->tanggal])->one();
                if ($queryIzinAbsensiTidakLengkap) {
                    $ijin = $queryIzinAbsensiTidakLengkap;
                }

                $queryIzinSetengahHari = SetengahHari::find()->andWhere(['pegawai_id' => $pegawai->id, 'tanggal' => $model->tanggal])->one();
                if ($queryIzinSetengahHari) {
                    $ijin = $queryIzinSetengahHari;
                }

                $cuti = Cuti::find()
                    ->andWhere(['pegawai_id' => $pegawai->id])
                    ->andWhere(['batal' => Status::TIDAK])
                    ->andWhere('\'' . $model->tanggal . '\' BETWEEN tanggal_mulai AND tanggal_selesai OR tanggal_mulai = \'' . $model->tanggal . '\' OR tanggal_selesai = \'' . $model->tanggal . '\'')
                    ->one();

//                if ($pegawai->sistem_kerja == TimeTable::LIMA_HARI_KERJA) {
                $timeTables = $timeTable->getTimeTableBySistemKerja($pegawaiSistemKerja->sistem_kerja_id);
//                $setCheckTime = [];
                $modelLibur = new LiburPegawai();
                $cekLiburPegawai = $modelLibur->cekLiburPegawai($pegawai, $model->tanggal);
                $cekLibur = Libur::cekLiburByJenis($model->tanggal);

                //************************** Sementara, sebelum ada solusi sistem ************************
                //Pegawai borongan tidak tanggal merah, semua tanggal dianggap sama
                if ($pegawaiGolongan->golongan->id == Golongan::NOL) {
                    $cekLibur = false;
                }
                //****************************************************************************************

                //Cek apakan hari libur pegawai atau libur nasional/cuti bersama
                if (($cekLiburPegawai || $cekLibur)) {
                    if ($spl) {
                        //spl yg digunakan hanya untuk hari libur / tanggal merah saja
                        $setCheckTime = $model->setCheckTimeKhusus($pegawai, $spl);
                    } else {
                        $setCheckTime = $model->setCheckTime($pegawai, $timeTables);
                    }
                } else {
                    $setCheckTime = $model->setCheckTime($pegawai, $timeTables, $spl);
                }

//                echo "--Pegawai $pegawai->nama, idfp $pegawai->idfp \n";
//                echo $pegawai->sistemKerja_ . "\n";
//                print_r($setCheckTime);
//                var_dump(($cekLibur || $cekLiburPegawai));
//                print_r($ijin->attributes);
                $model->setStatusAbsensi($setCheckTime, ($cekLibur || $cekLiburPegawai), $spl, $ijin, $cuti);
//                $model->hitungJumlahJamLembur($spl);
//                print_r($model->attributes);


                if ($model->save()) {
                    $info['tanggal'][$model->tanggal]['status_kehadiran'][$model->statusKehadiran_][] = [
                        'id' => $model->pegawai_id,
                        'nama' => $model->pegawai->nama
                    ];
                }

//                var_dump($model->save());
//                print_r($model->errors);
            }
            $tanggal_awal->add(new DateInterval('P1D'));
        }
        return $info;
    }

    /**
     * @param array $setCheckTimes
     * @param bool $libur
     * @param Spl|null $spl
     * @param ActiveRecord|null $ijin
     * @param ActiveRecord|null $cuti
     * @throws Exception
     * @internal param Pegawai $pegawai
     */
    public function setStatusAbsensi(array $setCheckTimes, bool $libur, Spl $spl = null, ActiveRecord $ijin = null, ActiveRecord $cuti = null)
    {
        if (!empty($setCheckTimes)) {
//            print_r($setCheckTimes);
            $this->status_kehadiran = self::ERROR;
            if ($libur) {
                $this->status_kehadiran = self::LIBUR;
                if ($spl) {
                    $setCheckTime = $setCheckTimes[key($setCheckTimes)];
                    if (isset($setCheckTime['masuk']) && isset($setCheckTime['pulang'])) {
                        //hitung jam lembur
                        $khusus = false;
                        if ($spl->splTemplate->kategori == SplTemplate::LEMBUR_KHUSUS) {
                            $khusus = true;
                        }
                        $setCheckTime['jamLembur'] = $this->hitungJumlahJamLembur($spl, $setCheckTime['pulang'], $khusus);
                        $this->setAttributeMasuk(0, $setCheckTime, $spl->id, $spl->spl_template_id);
                    } else {
                        $this->status_kehadiran = self::ERROR;
                    }
                } else {
                    foreach ($setCheckTimes as $timeTableId => $setCheckTime) {
                        if (isset($setCheckTime['masuk']) && isset($setCheckTime['pulang'])) {
                            $setCheckTime['jamLembur'] = 0;
                            $this->setAttributeMasuk(0, $setCheckTime, 0);
                        }
                    }
                    //tetap libur, walaupun attendancenya sesuai
                    $this->status_kehadiran = self::LIBUR;
                }
            }
            //batal per 25032019
//            elseif ($ijin) {
//                //jika ada ijin, maka ijin didahulukan
//                $this->status_kehadiran = self::IJIN;
//            }
            else {
//                echo "masuk sini \n";
                $this->status_kehadiran = self::ERROR;
                foreach ($setCheckTimes as $timeTableId => $setCheckTime) {
                    if (isset($setCheckTime['masuk']) && isset($setCheckTime['pulang'])) {
                        $splId = 0;
                        $splTemplateId = 0;
                        $setCheckTime['jamLembur'] = 0;
                        if ($spl) {
                            if ($this->validateSpl($timeTableId, $spl)) {
                                //hitung jumlah jam lembur
                                $setCheckTime['jamLembur'] = $this->hitungJumlahJamLembur($spl, $setCheckTime['pulang']);
                                $splId = $spl->id;
                                $splTemplateId = $spl->spl_template_id;
                            }
                        }
                        $this->setAttributeMasuk($timeTableId, $setCheckTime, $splId, $splTemplateId);
                    }
                }
                if ($libur) {
                    $this->status_kehadiran = self::LIBUR;
                    $this->jumlah_jam_lembur = 0;
                } elseif ($ijin) {
                    //jika ada ijin, maka ijin didahulukan
                    $this->status_kehadiran = self::IJIN;
                    $this->jumlah_jam_lembur = 0;
                } elseif ($cuti) {
                    $this->status_kehadiran = self::CUTI;
                    $this->jumlah_jam_lembur = 0;
                }
            }
        } else {
            if ($spl) {
                $this->status_kehadiran = self::ERROR;
            } else {
                if ($libur) {
                    $this->status_kehadiran = self::LIBUR;
                } elseif ($ijin) {
                    $this->status_kehadiran = self::IJIN;
                } elseif ($cuti) {
                    $this->status_kehadiran = self::CUTI;
                } else {
                    $this->status_kehadiran = self::ALFA;
                }
            }
        }
    }

    /**
     * Memvalidasi SPL yg sesuai dengan timetable yg sedang diproses
     *
     * @param int $timeTableId
     * @param Spl $spl
     * @return bool
     */
    public function validateSpl(int $timeTableId, Spl $spl)
    {
        $modelTimeTable = TimeTable::findOne($timeTableId);
        if ($modelTimeTable) {
            if ($spl->splTemplate->kategori == SplTemplate::LEMBUR_AWAL) {
                return $modelTimeTable->masuk == $spl->splTemplate->pulang_maksimum;
            }
            if ($spl->splTemplate->kategori == SplTemplate::LEMBUR_AKHIR) {
                return $modelTimeTable->pulang == $spl->splTemplate->masuk_minimum;
            }
        }
        return false;
    }

    /**
     * Helper untuk set data jika status kehadirannya masuk
     * Karena kalau masuk ada beberapa attribute yg harus harus diset, tidak seperti alfa dan error yg hanya set status
     * kehadiran saja
     *
     * @param int $timeTableId
     * @param array $checkTime
     * @param int $splId
     * @param int $splTemplateId
     */
    protected function setAttributeMasuk(int $timeTableId, array $checkTime, $splId = 0, $splTemplateId = 0)
    {
        $this->time_table_id = $timeTableId;
        $this->spl_id = $splId;
        $this->spl_template_id = $splTemplateId;
        $this->masuk = $checkTime['masuk'];
        $this->pulang = $checkTime['pulang'];
        $this->status_kehadiran = self::MASUK;
        $this->jumlah_jam_lembur = $checkTime['jamLembur'];
    }

    /**
     * @param Pegawai $pegawai
     * @param array|TimeTable $timeTables
     * @param Spl|null $spl
     * @return array
     */
    public function setCheckTime(Pegawai $pegawai, array $timeTables, Spl $spl = null)
    {
        $setCheckTime = [];
        /** @var TimeTable $timeTable */
        foreach ($timeTables as $timeTable) {
            //mengambil konfigurasi sistem kerja
            $cekSistemKerjaConfig = SistemKerja::getConfig($pegawai->getPegawaiSistemKerjaUntukTanggal($this->tanggal)->sistem_kerja_id);
            if (is_null($cekSistemKerjaConfig)) {
                continue;
            }

            //cek apakah hari keenam setengah hari?
            if ($cekSistemKerjaConfig->hari_keenam_setengah) {
                //jika besok tidak libur dan jam kerja lebih kecil dari pada 8 jam, maka continue
                if (!LiburPegawai::cekBesokLibur($pegawai, $this->tanggal) && $timeTable->hitungJamKerja() < TimeTable::NORMAL_JAM_KERJA) {
                    continue;
                }

                //jika besok libur dan time table lebih besar dari pada 8 jam, maka continue
                if (LiburPegawai::cekBesokLibur($pegawai, $this->tanggal) && $timeTable->hitungJamKerja() >= TimeTable::NORMAL_JAM_KERJA) {
                    continue;
                }
            }

            //Percobaan validasi SPL
            //time table yg jam masuk atau pulangnya tidak sesuai dengan SPL maka tidak akan diproses
            if ($spl && !$this->validateSpl($timeTable->id, $spl)) {
                continue;
            }

//            print_r($timeTable->attributes);
//            print_r($untukHari);
//            if($timeTables
//            echo "Time Table :  $timeTable->nama \n";
//            $setCheckTime[$timeTable->id] = ['nama' => $timeTable->nama];
            $attendances = $this->getAttendances($pegawai, $timeTable, $spl);
//            print_r($attendances);
            if (!empty($attendances)) {
                //menyimpan sementara data attendance
                $checkTimesMasuk = [];
                $checkTimesPulang = [];
                /** @var Attendance $attendance */
                foreach ($attendances as $attendance) {
//                    print_r($attendance->attributes);

                    //masuk kondisi standar
                    $masukMin = strtotime($this->tanggal . ' ' . $timeTable->masuk_minimum);

                    //jika jam masuk dan masuk minimumnya beda hari
                    //contoh masuk minimum jam 23.00 hari sebelumnya dan masuk jam 00.30
                    if (strtotime($timeTable->masuk_minimum) > strtotime($timeTable->masuk)) {
                        $masukMin = date('Y-m-d', strtotime($this->tanggal . '-1 day'));
                        $masukMin = strtotime($masukMin . ' ' . $timeTable->masuk_minimum);
                    }

                    //jika jam masuk dan masuk minimumnya beda hari
                    //contoh masuk minimumnya jam 23.00 hari sebelumnya dan pulang jam 08.00
                    //atau masuk minimumnya 16.00 hari sebelumnya dan pulang maksimum pukul 00.59
//                    if (strtotime($timeTable->masuk_minimum) > strtotime($timeTable->pulang) || strtotime($timeTable->masuk_minimum) > strtotime($timeTable->pulang_maksimum)) {
//                        $masukMin = date('Y-m-d', strtotime($this->tanggal . '-1 day'));
//                        $masukMin = strtotime($masukMin . ' ' . $timeTable->masuk_minimum);
//                    }

                    //kondisi standar masuk maksimum
                    //biasa nya beda hanya beberapa menit dari jam masuk, yg artinya masih tanggal yang sama
                    $masukMax = strtotime($this->tanggal . ' ' . $timeTable->masuk_maksimum);

                    //kondisi masuk jam 23.55 dam masuk maksimum jam 00.30
                    if (strtotime($timeTable->masuk) > strtotime($timeTable->masuk_maksimum)) {
                        $masukMax = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
                        $masukMax = strtotime($masukMax . ' ' . $timeTable->masuk_maksimum);
                    }

                    //kondisi standar pulang minimum
                    //biasa jam pulang minimum sampe dengan maksimum menggunakan jam yang sama
                    //contoh pulang minimun 00.00, pulang 00.00 dan pulang maksimum 00.59
                    $pulangMin = strtotime($this->tanggal . ' ' . $timeTable->pulang_minimum);

                    //contoh pulang minimun 22.00, pulang 22.00 dan pulang maksimum 23.00
                    $pulangMax = strtotime($this->tanggal . ' ' . $timeTable->pulang_maksimum);

                    //contoh pulang minimun 23.00, pulang 00.59
                    if (strtotime($timeTable->pulang_minimum) > strtotime($timeTable->pulang)) {
                        $pulangTemp = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
                        $pulangMin = strtotime($pulangTemp . ' ' . $timeTable->pulang_minimum);
                        //kondisi pulang minimum dan pulang maksimumnya beda hari
                        //contoh : jika pulang minimun 23.30, pulang 23.30 dan pulang maksimum 00.29 di hari berikutnya
                        if (strtotime($timeTable->pulang_minimum) > strtotime($timeTable->pulang_maksimum)) {
                            $pulangMin = strtotime($this->tanggal . ' ' . $timeTable->pulang_minimum);
                        }
                        $pulangMax = strtotime($pulangTemp . ' ' . $timeTable->pulang_maksimum);
                    } elseif (strtotime($timeTable->pulang) > strtotime($timeTable->pulang_maksimum)) {
                        //contoh pulang minimun 23.00, pulang 23.00 dan pulang maksimum 00.59
                        $pulangMax = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
                        $pulangMax = strtotime($pulangMax . ' ' . $timeTable->pulang_maksimum);
                    } elseif (strtotime($timeTable->masuk) > strtotime($timeTable->pulang)) {
                        $pulangTemp = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
                        $pulangMin = strtotime($pulangTemp . ' ' . $timeTable->pulang_minimum);
                        //kondisi pulang minimum dan pulang maksimumnya beda hari
                        //contoh : jika pulang minimun 23.30, pulang 23.30 dan pulang maksimum 00.29 di hari berikutnya
                        if (strtotime($timeTable->pulang_minimum) > strtotime($timeTable->pulang_maksimum)) {
                            $pulangMin = strtotime($this->tanggal . ' ' . $timeTable->pulang_minimum);
                        }
                        $pulangMax = strtotime($pulangTemp . ' ' . $timeTable->pulang_maksimum);
                    }

                    //cek jika ada spl -> karena jika ada maka masuk min atau pulang max bisa berubah, sesuai dengan spl nya
                    //lembur awal atau lembur akhir
                    if ($spl) {
                        $cekSpl = $this->cekSpl($spl, $timeTable);
                        $masukMin = $cekSpl['masukMin'] == false ? $masukMin : $cekSpl['masukMin'];
                        $masukMax = $cekSpl['masukMax'] == false ? $masukMax : $cekSpl['masukMax'];
                        $pulangMin = $cekSpl['pulangMin'] == false ? $pulangMin : $cekSpl['pulangMin'];
                        $pulangMax = $cekSpl['pulangMax'] == false ? $pulangMax : $cekSpl['pulangMax'];
                    }

                    $checkTime = strtotime($attendance->check_time);

//                    echo "masuk minimum : " . date('Y-m-d H:i:s', $masukMin) . " dan masuk maxnya :" . date('Y-m-d H:i:s', $masukMax) . " \n";
//                    echo "pulang minimum : " . date('Y-m-d H:i:s', $pulangMin) . " dan pulang maxnya :" . date('Y-m-d H:i:s', $pulangMax) . " \n";

                    //cek attendancenya masuk ke shift yg mana
                    //set jam masuk
                    if ($checkTime >= $masukMin && $checkTime <= $masukMax) {
//                        $setCheckTime[$timeTable->id]['masuk'][] = $checkTime;
                        $setCheckTime[$timeTable->id]['masuk'][] = date('Y-m-d H:i:s', $checkTime);
                    }

                    //set jam pulang
                    if ($checkTime >= $pulangMin && $checkTime <= $pulangMax) {
//                        $setCheckTime[$timeTable->id]['pulang'][] = $checkTime;
                        $setCheckTime[$timeTable->id]['pulang'][] = date('Y-m-d H:i:s', $checkTime);
                    }

//                    echo "Check time 1 : $attendance->check_time \n";
//                    echo "Check time 2 :" . strtotime($attendance->check_time) . " \n";
//                    echo "Check time :" . date('Y-m-d H:i:s', $checkTime) . " \n";
//                    echo "Check time 4 : $checkTime \n";
                }
                if (isset($setCheckTime[$timeTable->id]['masuk'])) {
                    $checkTimesMasuk = $setCheckTime[$timeTable->id]['masuk'];
                    $setCheckTime[$timeTable->id]['masuk'] = min($setCheckTime[$timeTable->id]['masuk']);
                }
                if (isset($setCheckTime[$timeTable->id]['pulang'])) {
                    $checkTimesPulang = $setCheckTime[$timeTable->id]['pulang'];
                    $setCheckTime[$timeTable->id]['pulang'] = max($setCheckTime[$timeTable->id]['pulang']);
                }

                //telegram #Koreksi2 26.02.2019
                //Diambil yg pertama ketemu, attendance selanjutnya tidak dihiraukan
                if (isset($setCheckTime[$timeTable->id]['masuk']) && isset($setCheckTime[$timeTable->id]['pulang'])) {
                    $atts = array_merge($checkTimesMasuk, $checkTimesPulang);
                    //menandai attendance
                    $this->flagingAttendance($pegawai, $atts);
                    break;
                }
            }
        }
//        print_r($setCheckTime);
        return $setCheckTime;
    }


    /**
     * Untuk menandai bahwa attendance 'itu' telah diproses
     * Masih perlu bnyak pengembangan, sampai saat ini hanya menandai saja, dan tandanya tidak digunakan untuk apapun
     * Hanya attendance yg komplit(masuk dan pulangnya) ditemukan
     *
     * @param Pegawai $pegawai
     * @param array $attendance
     */
    private function flagingAttendance(Pegawai $pegawai, array $attendance)
    {
        Attendance::updateAll(['status' => 1], ['AND', ['pegawai_id' => $pegawai->id], ['IN', 'check_time', $attendance]]);
    }

    /**
     * @param Spl $spl
     * @param TimeTable $timeTable
     * @return array
     */
    public function cekSpl(Spl $spl, TimeTable $timeTable)
    {
        $masukMin = 0;
        $masukMax = 0;
        $pulangMin = 0;
        $pulangMax = 0;
        $jumlahJamLembur = 0;
        //jika splnya lembur awal
        if ($spl->splTemplate->kategori == SplTemplate::LEMBUR_AWAL) {

            //Spl pulang harus sama dengan time table masuk
            //contoh lembur shift 1 time table -> masuk min 06:00, masuk 07:00, masuk mak 07:10
            //SPl -> pulang min 07:00, pulang 07:00, pulang mak 07:10
            if ($spl->splTemplate->pulang == $timeTable->masuk) {
                //masuk min lembur kondisi standar lembur awal
                $masukMin = strtotime($this->tanggal . ' ' . $spl->splTemplate->masuk_minimum);
                $masukMax = strtotime($this->tanggal . ' ' . $spl->splTemplate->masuk_maksimum);

                //cek mulai dan akhir lembur kondisi standar
//                $masukLembur = $this->tanggal . ' ' . $spl->splTemplate->masuk;
//                $akhirLembur = $this->tanggal . ' ' . $spl->splTemplate->pulang;

                //jika jam masuk minimumnya lebih kecil dibanding jam masuk spl nya
                //contoh -> jam masuk pukul 01.00 tapi lembur awal minimumnya jam 22.00 hari sebelumnya
                if (strtotime($timeTable->masuk) < strtotime($spl->splTemplate->masuk_minimum)) {
                    $tanggalTempMasuk = date('Y-m-d', strtotime($this->tanggal . '-1 day'));
                    $masukMin = strtotime($tanggalTempMasuk . ' ' . $spl->splTemplate->masuk_minimum);
                }

                //jika jam masuk maksimumnya lebih kecil dibanding jam masuk spl nya
                //contoh -> jam masuk pukul 01.00 tapi lembur awal maksimumnya jam 23.00 hari sebelumnya
                if (strtotime($timeTable->masuk) < strtotime($spl->splTemplate->masuk_maksimum)) {
                    $tanggalTempMasuk = date('Y-m-d', strtotime($this->tanggal . '-1 day'));
                    $masukMax = strtotime($tanggalTempMasuk . ' ' . $spl->splTemplate->masuk_maksimum);
                }
            }
        }
        //jika splnya lembur akhir
        if ($spl->splTemplate->kategori == SplTemplate::LEMBUR_AKHIR) {

            //Spl masuk harus sama dengan time table pulang
            //contoh lembur shift 3 time table -> pulang min 06:00, pulang 07:00, pulang mak 07:10
            //SPl -> masuk min 07:00, masuk 07:00, masuk mak 07:59
            if ($spl->splTemplate->masuk == $timeTable->pulang) {
                //pulang maksimum lembur kondisi standar
                $pulangMin = strtotime($this->tanggal . ' ' . $spl->splTemplate->pulang_minimum);
                $pulangMax = strtotime($this->tanggal . ' ' . $spl->splTemplate->pulang_maksimum);

                //cek mulai dan akhir lembur kondisi standar
//                $masukLembur = $this->tanggal . ' ' . $spl->splTemplate->masuk;
                //akhir lembur menggunakan jam pulang
//                $akhirLembur = $this->tanggal . ' ' . $spl->splTemplate->pulang;

                //Jika masuk lebih besar dari pada jam pulang lembur minimum
                //contoh masuk jam 16.00 pulang jam 00.00 tapi ada spl sampe jam 02.00 hari berikutnya
                if (strtotime($timeTable->masuk) > strtotime($spl->splTemplate->pulang_minimum)) {
                    $pulangMaxTemp = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
                    $pulangMin = strtotime($pulangMaxTemp . ' ' . $spl->splTemplate->pulang_minimum);
                }

                //Jika masuk lebih besar dari pada jam pulang lembur maksimum
                //contoh masuk jam 16.00 pulang jam 00.00 tapi ada spl sampe jam 02.00 hari berikutnya
                if (strtotime($timeTable->masuk) > strtotime($spl->splTemplate->pulang_maksimum)) {
                    $pulangMaxTemp = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
                    $pulangMax = strtotime($pulangMaxTemp . ' ' . $spl->splTemplate->pulang_maksimum);
                }
            }
        }

        //jika masuk lembur dan akhir lembur tidak kosong, maka cek jumlah jam lembur
        //hitung jam lembur pindah ke akhir bagian, karena methode penghitungan lembur berubah
        //metode pengghitungan lembur yg baru menggunakan terakhir absen untuk meghitung jumlah jam lemburnya
//        if (!empty($masukLembur) && !empty($akhirLembur)) {
//            $jumlahJamLembur = $this->hitungJumlahJamLembur($spl, $masukLembur, $akhirLembur, $spl->splTemplate->istirahat);
//        }

        return ['masukMin' => $masukMin, 'masukMax' => $masukMax, 'pulangMin' => $pulangMin, 'pulangMax' => $pulangMax, 'jamLembur' => $jumlahJamLembur];
    }

    /**
     *
     * Menghitung jumlah jam lembur
     * Dengan asumsi perhitungan jam lembrnya adalah jam pertama dikali 1,5 dan jam berikutnya dikali 2
     * Dengan minimal lembur 1 jam
     *
     * @param Spl $spl
     * @param string|null $akhir
     * @param bool $khusus
     *
     *
     * @return float|int jumlah jam lembur, dengan minimal 1,5 perlu pengembangan jika ingin lebih kecil dari itu.
     * @throws Exception
     */
    public function hitungJumlahJamLembur(Spl $spl, string $akhir = null, $khusus = false)
    {
        $awal = $spl->tanggal . ' ' . $spl->splTemplate->masuk;

        //jika jam pulang spl pada esok hari, maka tanggal akhir nya ditambah satu hari
        $tanggalAkhir = $spl->tanggal;
        if (strtotime($spl->splTemplate->masuk) > strtotime($spl->splTemplate->pulang)) {
            $tanggalAkhir = date('Y-m-d', strtotime($tanggalAkhir . '+1 day'));
        } else if (!is_null($akhir) && $spl->splTemplate->tunjangan_shift == SplTemplate::SHIFT_TIGA && $spl->splTemplate->kategori == SplTemplate::LEMBUR_AKHIR) {
            //jika lembur akhir shift 3
            $tanggalAkhirTemp = explode(' ', $akhir)[0];
            if (strtotime($tanggalAkhirTemp) > strtotime($spl->tanggal)) {
                $tanggalAkhir = date('Y-m-d', strtotime($tanggalAkhir . '+1 day'));
            }
        }

        $akhirTemp = $tanggalAkhir . ' ' . $spl->splTemplate->pulang;

        //jika pegawai absen melebihi dari jam pulang, maka yang di pakai untuk jam pulang adalah jam pulang spl
        if (strtotime($akhir) > strtotime($akhirTemp)) {
            $akhir = $akhirTemp;
        } else {
            $akhir = explode(' ', $akhir);
            $tanggalAkhirTemp = $akhir[0];
            $jamAkhirTemp = explode(':', $akhir[1]);
            $menitTemp = '00';
            if ($jamAkhirTemp[1] >= 30) {
                $menitTemp = '30';
            }
            $akhir = $tanggalAkhirTemp . ' ' . $jamAkhirTemp[0] . ':' . $menitTemp . ':' . $jamAkhirTemp[2];
        }

        //jam istirahat menggunakan jam yg diinput secara manual
        $jamIstirahat = $spl->splTemplate->istirahat;
//        echo 'Jumlah jam istirahat ' . $jamIstirahat . "\n";
        $datetime1 = new DateTime($awal);
        $datetime2 = new DateTime($akhir);
        $interval = $datetime1->diff($datetime2);
        /** @var int $jam */
        $jam = $interval->format('%H');
        $menit = $interval->format('%I') / 60;
        $menit = round($menit * 2) / 2;
        $jamLembur = $jam + $menit - $jamIstirahat;

        if ($spl->kategori == Spl::LEMBUR_INSENTIF) {
            return $jamLembur;
        }
//        echo "Jam lebur masih utuh $jamLembur \n";

        $jamPertama = 1;
        $pengaliJamPertama = $khusus ? 2 : 1.5;
        $jumlahJamLembur = $jamPertama * $pengaliJamPertama;
        if ($jamLembur > $jamPertama) {
            $jamBerikutnya = ($jamLembur - $jamPertama) * 2;
            $jamPertama = $jamPertama * $pengaliJamPertama;
            $jumlahJamLembur = $jamPertama + $jamBerikutnya;
        }
//        echo "Jumlah jam lembur $jumlahJamLembur \n";

        return $jumlahJamLembur;
    }

    /**
     * @return float|int
     * @throws Exception
     */
    public function hitungJumlahJam()
    {
        if (!$this->timeTable) {
            return 0;
        }

        $awal = $this->tanggal . ' ' . $this->timeTable->masuk;
        if (strtotime($this->masuk) > strtotime($awal)) {
            $awal = explode(' ', $this->masuk);
            $tanggalAwalTemp = $awal[0];
            $jamAwalTemp = explode(':', $awal[1]);
            $menitTemp = '00';
            if ($jamAwalTemp[1] >= 30) {
                $menitTemp = '30';
            }
            $awal = $tanggalAwalTemp . ' ' . $jamAwalTemp[0] . ':' . $menitTemp . ':' . $jamAwalTemp[2];
        }

        //jika jam pulang spl pada esok hari, maka tanggal akhir nya ditambah satu hari
        $tanggalAkhir = $this->tanggal;
        if (strtotime($this->timeTable->masuk) > strtotime($this->timeTable->pulang)) {
            $tanggalAkhir = date('Y-m-d', strtotime($tanggalAkhir . '+1 day'));
        }
        $akhir = $tanggalAkhir . ' ' . $this->timeTable->pulang;

        //jika pegawai absen melebihi dari jam pulang, maka yang di pakai untuk jam pulang adalah jam pulang timetable
        if (strtotime($this->pulang) < strtotime($akhir)) {
            $akhir = explode(' ', $this->pulang);
            $tanggalAkhirTemp = $akhir[0];
            $jamAkhirTemp = explode(':', $akhir[1]);
            $menitTemp = '00';
            if ($jamAkhirTemp[1] >= 30) {
                $menitTemp = '30';
            }
            $akhir = $tanggalAkhirTemp . ' ' . $jamAkhirTemp[0] . ':' . $menitTemp . ':' . $jamAkhirTemp[2];
        }

        $datetime1 = new DateTime($awal);
        $datetime2 = new DateTime($akhir);
        $interval = $datetime1->diff($datetime2);
        /** @var int $jam */
        $jam = $interval->format('%H');
        $menit = $interval->format('%I') / 60;
        $menit = round($menit * 2) / 2;
        return $jam + $menit;
//        return $awal . ' ' . $akhir;
    }

    /**
     * @param Pegawai $pegawai
     * @param TimeTable $timeTable
     * @param Spl|null $spl
     * @return array|ActiveRecord[]
     */
    public function getAttendances(Pegawai $pegawai, TimeTable $timeTable, Spl $spl = null)
    {
        $modelAttendance = new Attendance();
        //masuk kondisi standar
        $masukMin = strtotime($this->tanggal . ' ' . $timeTable->masuk_minimum);

        //jika jam masuk dan masuk minimumnya beda hari
        //contoh masuk minimum jam 23.00 hari sebelumnya dan masuk maksimum jam 00.30
        if (strtotime($timeTable->masuk_minimum) > strtotime($timeTable->masuk_maksimum)) {
            //hanya untuk memperjelas, H-1 dipake hanya jika masuk minimum lebih besar dari pada masuk
            //contoh 23.00 -> 00.01 -> 00.59
            //Tidak berlaku jika seperti ini : 23.00 -> 23.59 -> 00.59
            if (strtotime($timeTable->masuk_minimum) > strtotime($timeTable->masuk)) {
                $masukMin = date('Y-m-d', strtotime($this->tanggal . '-1 day'));
                $masukMin = strtotime($masukMin . ' ' . $timeTable->masuk_minimum);
            }
        }

        //pulang kondisi setandar
        $pulangMax = strtotime($this->tanggal . ' ' . $timeTable->pulang_maksimum);

        //jika jam pulang minimumnya lebih besar dibanding pulang maksimumnya
        //contoh : pulang minimunya jam 23.00, sedangkan pulang keesokan harinya jam 00.30
        if (strtotime($timeTable->pulang_minimum) > strtotime($timeTable->pulang)) {
            $pulangTemp = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
            $pulangMax = strtotime($pulangTemp . ' ' . $timeTable->pulang_maksimum);
        } elseif (strtotime($timeTable->pulang) > strtotime($timeTable->pulang_maksimum)) {
            //contoh pulang minimun 23.00, pulang 23.00 dan pulang maksimum 00.59
            $pulangMax = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
            $pulangMax = strtotime($pulangMax . ' ' . $timeTable->pulang_maksimum);
        } elseif (strtotime($timeTable->masuk) > strtotime($timeTable->pulang)) {
            $pulangTemp = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
            $pulangMax = strtotime($pulangTemp . ' ' . $timeTable->pulang_maksimum);
        }


        //cek jika ada spl -> karena jika ada maka masuk min atau pulang max bisa berubah, sesuai dengan spl nya
        //lembur awal atau lembur akhir
        if ($spl) {
            $cekSpl = $this->cekSpl($spl, $timeTable);
            $masukMin = $cekSpl['masukMin'] == false ? $masukMin : $cekSpl['masukMin'];
            $pulangMax = $cekSpl['pulangMax'] == false ? $pulangMax : $cekSpl['pulangMax'];
        }

        //Percobaan pengambilan absensi kemarin sebagai dasar pengambilan attendance hari ini
        //kasuk setelah pulang shift 3, langsung masuk shift 2
        $kemarin = date('Y-m-d', strtotime($this->tanggal . '-1 day'));
        $absensiKemarin = $this->getAbsensi($kemarin, $this->pegawai_id, Absensi::MASUK);
        /** @var Absensi $absensiKemarin */
        if ($absensiKemarin) {
            $pulangKemarin = strtotime($absensiKemarin->pulang);
            //jika time table masuk min(lebih tepat masuk max) atau pulang max nya lebih kecil dari pada jam pulang kmaren, maka akan langsung return kosong
            //atau tidak dianggap ada attendance
            if (($masukMin <= $pulangKemarin) || ($pulangMax <= $pulangKemarin)) {
                return [];
            }
        }


//        echo "masuk min : " . date('Y-m-d H:i:s', $masukMin) . "\n";
//        echo "pulang max :" . date('Y-m-d H:i:s', $pulangMax) . "\n";
        return $modelAttendance->getAttendances($pegawai, $masukMin, $pulangMax);
    }

    /**
     * @param string $tanggal
     * @param string $pegawai_id
     * @param int $statusKehadiran
     * @param string $timeTable
     * @return array|null|ActiveRecord
     */
    public function getAbsensi(string $tanggal, string $pegawai_id, $statusKehadiran = 0, $timeTable = '')
    {
        $query = self::find()
            ->andWhere('tanggal =:tanggal AND pegawai_id =:pegawai', [':tanggal' => $tanggal, ':pegawai' => $pegawai_id]);
        if ($statusKehadiran > 0) {
            $query->andWhere('status_kehadiran =:sk', [':sk' => $statusKehadiran]);
        }

        if (!empty($timeTable)) {
            $query->andWhere('time_table_id =:tti', [':tti' => $timeTable]);
        }

        return $query->one();
    }

    /**
     * Seperti halnya checktime yg diaatas, akan tetapi check time khusus tidak membutuhkan time table
     * untuk validasi data attendance nya berdasarkan SPL khusus
     * SPL khusus adalah spl yg tidak terhubung dengan time table
     *
     * @param Pegawai $pegawai
     * @param Spl $spl
     * @return array
     */
    public function setCheckTimeKhusus(Pegawai $pegawai, Spl $spl)
    {
        $setCheckTime = [];

        //Pastikan yang diinput adalah SPL khusus
        if ($spl->splTemplate->kategori == SplTemplate::LEMBUR_KHUSUS) {

            //Ambil data attendances
            $attendances = $this->getAttendancesKhusus($pegawai, $spl);
            if (!empty($attendances)) {
                /** @var Attendance $attendance */
                foreach ($attendances as $attendance) {


                    //masuk kondisi standar
                    //masuk minimum tidak ada setting, karena masuk minimum pasti adalah tanggal SPL itu sendiri 22012019
                    $masukMin = strtotime($this->tanggal . ' ' . $spl->splTemplate->masuk_minimum);
//                    $masukLembur = $this->tanggal . ' ' . $spl->splTemplate->masuk;

                    //kondisi standar masuk maksimum
                    //biasa nya beda hanya beberapa menit dari jam masuk, yg artinya masih tanggal yang sama
                    //kecuali nanti ada yg masuk jam 23.55 dam masuk maksimum jam 00.05, ini akan memerlukan perlakuan khusus
                    $masukMax = strtotime($this->tanggal . ' ' . $spl->splTemplate->masuk_maksimum);

                    if (strtotime($spl->splTemplate->masuk_minimum) > strtotime($spl->splTemplate->masuk_maksimum) || strtotime($spl->splTemplate->masuk_minimum) > strtotime($spl->splTemplate->masuk_maksimum)) {
                        $masukMax = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
//                        $masukLembur = $masukMin . ' ' . $spl->splTemplate->masuk;
                        $masukMax = strtotime($masukMax . ' ' . $spl->splTemplate->masuk_maksimum);
                    }


                    //kondisi standar pulang minimum
                    //biasa jam pulang minimum sampe dengan maksimum menggunakan jam yang sama
                    //contoh pulang minimun 00.00, pulang 00.00 dan pulang maksimum 00.59
                    //belum support jika pulang minimun 23.30, pulang 23.30 dan pulang maksimum 00.29 di hari berikutnya
                    $pulangMin = strtotime($this->tanggal . ' ' . $spl->splTemplate->pulang_minimum);
                    $pulangMax = strtotime($this->tanggal . ' ' . $spl->splTemplate->pulang_maksimum);
//                    $akhirLembur = $this->tanggal . ' ' . $spl->splTemplate->pulang;

                    //jika masuk dan pulangnya beda hari(pulang di hari esok)
                    if (strtotime($spl->splTemplate->masuk) > strtotime($spl->splTemplate->pulang) || strtotime($spl->splTemplate->pulang_minimum) > strtotime($spl->splTemplate->pulang_maksimum)) {
                        $pulangTemp = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
                        $pulangMin = strtotime($pulangTemp . ' ' . $spl->splTemplate->pulang_minimum);
                        //untuk support jika pulang minimun 23.30, pulang 23.30 dan pulang maksimum 00.29 di hari berikutnya
                        if (strtotime($spl->splTemplate->pulang_minimum) > strtotime($spl->splTemplate->pulang_maksimum)) {
                            $pulangMin = strtotime($this->tanggal . ' ' . $spl->splTemplate->pulang_minimum);
                        }
                        $pulangMax = strtotime($pulangTemp . ' ' . $spl->splTemplate->pulang_maksimum);
//                        $akhirLembur = $pulangTemp . ' ' . $spl->splTemplate->pulang;
                    }

                    //Menghitung jumlah jam lembur
//                    $jumlahJamLembur = $this->hitungJumlahJamLembur($spl, $masukLembur, $akhirLembur, $spl->splTemplate->istirahat, true);

                    $checkTime = strtotime($attendance->check_time);

                    //set jam masuk
                    if ($checkTime >= $masukMin && $checkTime <= $masukMax) {
//                        $setCheckTime[$spl->splTemplate->id]['masuk'][] = $checkTime;
                        $setCheckTime[$spl->splTemplate->id]['masuk'][] = date('Y-m-d H:i:s', $checkTime);
                    }

                    //set jam pulang
                    if ($checkTime >= $pulangMin && $checkTime <= $pulangMax) {
//                        $setCheckTime[$spl->splTemplate->id]['pulang'][] = $checkTime;
                        $setCheckTime[$spl->splTemplate->id]['pulang'][] = date('Y-m-d H:i:s', $checkTime);
                    }

//                    echo "Check time 1 : $attendance->check_time \n";
//                    echo "Check time 2 :" . strtotime($attendance->check_time) . " \n";
//                    echo "Check time :" . date('Y-m-d H:i:s', $checkTime) . " \n";
//                    echo "Check time 4 : $checkTime \n";
                }
                if (isset($setCheckTime[$spl->splTemplate->id]['masuk'])) {
                    $setCheckTime[$spl->splTemplate->id]['masuk'] = max($setCheckTime[$spl->splTemplate->id]['masuk']);
                }
                if (isset($setCheckTime[$spl->splTemplate->id]['pulang'])) {
                    $setCheckTime[$spl->splTemplate->id]['pulang'] = max($setCheckTime[$spl->splTemplate->id]['pulang']);
                }
//                print_r($setCheckTime);
            }
        }
        return $setCheckTime;
    }

    /**
     * Begitupun dengan get attendance khusus, tidak membutuhkan time table
     * Mengambil data attendance nya pun berdasarkan SPL khusus yang diiput
     *
     * @param Pegawai $pegawai
     * @param Spl $spl
     * @return array|ActiveRecord[]
     *
     */
    public function getAttendancesKhusus(Pegawai $pegawai, Spl $spl)
    {
        $modelAttendance = new Attendance();

        //masuk kondisi standar
        $masukMin = strtotime($this->tanggal . ' ' . $spl->splTemplate->masuk_minimum);

        //jika jam masuk dan masuk minimumnya beda hari
        //contoh masuk jam 00.00 dan masuk minimum jam 23.00 hari sebelumnya
        if (strtotime($spl->splTemplate->masuk_minimum) > strtotime($spl->splTemplate->masuk)) {
            $masukMin = date('Y-m-d', strtotime($this->tanggal . '-1 day'));
            $masukMin = strtotime($masukMin . ' ' . $spl->splTemplate->masuk_minimum);
        }

        //pulang kondisi setandar
        $pulangMax = strtotime($this->tanggal . ' ' . $spl->splTemplate->pulang_maksimum);


        //jika jam pulang minimumnya lebih besar dibanding pulang maksimumnya
        //contoh : pulang minimunya jam 23.00, sedangkan pulang maksimumnya keesokan harinya jam 00.30 -> trial
        if (strtotime($spl->splTemplate->pulang_minimum) > strtotime($spl->splTemplate->pulang_maksimum)) {
            $pulangMax = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
            $pulangMax = strtotime($pulangMax . ' ' . $spl->splTemplate->pulang_maksimum);
        }


        //jika  jam masuk lebih besar dibanding jam pulang
        //contoh : masuk jam 23.00 pulang jam 7 pagi keesokan harinya
        if (strtotime($spl->splTemplate->masuk) > strtotime($spl->splTemplate->pulang)) {
            $pulangMax = date('Y-m-d', strtotime($this->tanggal . '+1 day'));
            $pulangMax = strtotime($pulangMax . ' ' . $spl->splTemplate->pulang_maksimum);
        }

//        echo "masuk min : " . date('Y-m-d H:i:s', $masukMin) . "\n";
//        echo "pulang max :" . date('Y-m-d H:i:s', $pulangMax) . "\n";
        return $modelAttendance->getAttendances($pegawai, $masukMin, $pulangMax);
    }

    /**
     * @return bool|string
     */
    public function getIzinTanpaHadir()
    {
        $query = IzinTanpaHadir::find()
            ->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere('\'' . $this->tanggal . '\' BETWEEN tanggal_mulai AND tanggal_selesai OR tanggal_mulai = \'' . $this->tanggal . '\' OR tanggal_selesai = \'' . $this->tanggal . '\'')
            ->one();

        /** @var IzinTanpaHadir $query */
        if ($query) {
            $modelTipeIzin = IzinJenis::findOne($query['izin_jenis_id']);
            return $this->_status_kehadiran[$this->status_kehadiran] . ' -> ' . $modelTipeIzin->nama;
        }
        return false;
    }

    /**
     * @return bool|string
     */
    public function getIzinAbsensiTidakLengkap()
    {
        $query = AbsensiTidakLengkap::find()->andWhere(['pegawai_id' => $this->pegawai_id, 'tanggal' => $this->tanggal])->one();

        /** @var AbsensiTidakLengkap $query */
        if ($query) {
            return $this->_status_kehadiran[$this->status_kehadiran] . ' -> ' . AbsensiTidakLengkap::alias . $query->shift;
        }
        return false;
    }

    /**
     * @return bool|string
     */
    public function getIzinSetengahHari()
    {
        $query = SetengahHari::find()->andWhere(['pegawai_id' => $this->pegawai_id, 'tanggal' => $this->tanggal])->one();

        /** @var SetengahHari $query */
        if ($query) {
            return $this->_status_kehadiran[$this->status_kehadiran] . ' -> ' . SetengahHari::alias . $query->shift;
        }
        return false;
    }


    /**
     *
     * Menghitung jumlah pengali uang makan lembur
     * Ketentuan :
     *  jika lembur khusus, penghitungan di mulai dari 12 jam pertama
     *      lembur 12 jam -> dapat 1
     *      lembur 16 jam -> dapat 2, dan seterusnya kelipatan 4
     *  jika lembur biasa, maka di mulai dari 4 jam pertama, dan berikutnya kelipatan 4
     *
     * @return int
     */
    public function hitungPengaliUangMakanLembur()
    {
        $nilaiDasarLemburBiasa = 7.5;
        $nilaiDasarLemburKhusus = 22;

        $x = 0;
        if ($this->status_kehadiran == self::MASUK && $this->jamLembur() >= $nilaiDasarLemburBiasa) {
            $getSpl = self::getSpl($this->tanggal, $this->pegawai);
            if ($getSpl) {
                $nilaiDasar = $getSpl->splTemplate->kategori == SplTemplate::LEMBUR_KHUSUS ? $nilaiDasarLemburKhusus : $nilaiDasarLemburBiasa;
                $this->hitungPengaliUangMakanHelper($nilaiDasar, $this->jamLembur(), $x);
            }
        }

        return $x;
    }

    /**
     * n adalah kelipatan penghitungannya
     * @param int $nilaiDasar berdasarkan jenis lembur, jika lembur khusus = 22, lembur biasa = 7.5
     * @param int $jumlahJamLembur jumlah jam lembur yang akan dihitung jumlah uang makannya
     * @param int|null $x hasil perhitungan pengali uang makan lembur
     */
    private function hitungPengaliUangMakanHelper(int $nilaiDasar, int $jumlahJamLembur, int &$x = null)
    {
        $n = 8;
        $jumlahJamLembur -= $nilaiDasar;
        if ($jumlahJamLembur >= 0) {
            $this->hitungPengaliUangMakanHelper($n, $jumlahJamLembur, $x);
            $x++;
        }
    }


    /**
     *
     * Per hari memungkinkan sistem kerjanya berbeda-beda, karena sistem kerja dapat berubah sewaktu2
     *
     * @return SistemKerja|string
     */
    public function getSistemKerja()
    {
        $modelPegawai = $this->pegawai;
        return $modelPegawai->getPegawaiSistemKerjaUntukTanggal($this->tanggal) ? $modelPegawai->getPegawaiSistemKerjaUntukTanggal($this->tanggal)->sistemKerja : '';
    }

    /**
     * @param array $info
     * @return string
     */
    public static function formatInfoProses(array $info)
    {
        $message = '';
        if (!empty($info)) {
            $message = '<ul>';
            foreach ($info['tanggal'] as $keyT => $tanggal) {
                $message .= '<li>' . date('d-m-Y', strtotime($keyT));
                $message .= '<ul>';
                foreach ($tanggal['status_kehadiran'] as $keySk => $statusKehadiran) {
                    $message .= '<li>' . $keySk;
                    $message .= '<ul>';
                    foreach ($statusKehadiran as $pegawai) {
                        $message .= '<li>' . Html::a($pegawai['nama'], [
                                '/absensi', 'AbsensiSearch' => ['pegawai_id' => $pegawai['id'], 'tanggal' => $keyT . 's/d' . $keyT]
                            ], ['target' => '_blank']) . '</li>';
                    }
                    $message .= '</ul>';
                    $message .= '</li>';
                }
                $message .= '</ul>';
                $message .= '</li>';
            }
            $message .= '</ul>';
        }
        return $message;
    }


    /**
     * @param float $jumlahJamLembur
     * @param int $jumlahTelat
     * @param false $isInsentif
     * @return float|int
     */
    public static function konversiPotonganLemburKarenaTelat(float $jumlahJamLembur, int $jumlahTelat, $isInsentif = false)
    {
        $potongan = 0;
        if (($jumlahTelat > 0) && ($jumlahTelat <= 30)) {
            $potongan = 1;
        }
        if (($jumlahTelat > 30) && ($jumlahTelat <= 60)) {
            $potongan = $isInsentif ? 1 : 2;
        }
        if (($jumlahTelat > 60) && ($jumlahTelat <= 90)) {
            $potongan = $isInsentif ? 2 : 3;
        }
        if (($jumlahTelat > 90) && ($jumlahTelat <= 120)) {
            $potongan = $isInsentif ? 2 : 4;
        }
        if (($jumlahTelat > 120) && ($jumlahTelat <= 150)) {
            $potongan = $isInsentif ? 3 : 5;
        }
        if (($jumlahTelat > 150) && ($jumlahTelat <= 180)) {
            $potongan = $isInsentif ? 3 : 6;
        }
        if (($jumlahTelat > 180)) {
            $potongan = $isInsentif ? 4 : 7;
        }
        return $jumlahJamLembur - $potongan;
    }

    /**
     * @return float|int
     */
    public function jamLembur()
    {
        $jamLembur = (float)$this->jumlah_jam_lembur;
        $telat = $this->telat;
        if ($jamLembur > 0 && $telat > 0 && $this->splRel) {
            return self::konversiPotonganLemburKarenaTelat($jamLembur, $telat, $this->splRel->kategori == Spl::LEMBUR_INSENTIF);
        }
        return $jamLembur;
    }


}
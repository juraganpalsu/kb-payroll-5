<?php

namespace common\models;

use common\models\base\PegawaiSistemKerja as BasePegawaiSistemKerja;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pegawai_sistem_kerja".
 */
class PegawaiSistemKerja extends BasePegawaiSistemKerja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mulai_berlaku', 'pegawai_id', 'sistem_kerja_id'], 'required'],
            [['mulai_berlaku', 'akhir_berlaku', 'mulai_libur', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['sistem_kerja_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['pegawai_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => 0],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['mulai_berlaku'], 'mulaiBerlakuValidation'],
            [['akhir_berlaku'], 'akhirBerlakuValidation']
        ];
    }

    /**
     * @param $attribute
     */
    public function akhirBerlakuValidation($attribute)
    {
        if ($this->$attribute < $this->mulai_berlaku) {
            $this->addError($attribute, Yii::t('frontend', 'Akhir Berlaku Minimum Adalah {tanggal}', ['tanggal' => $this->mulai_berlaku]));
        }
    }

    /**
     * @param string $attribute
     */
    public function mulaiBerlakuValidation(string $attribute)
    {
        /** @var self $query */
        $query = self::find()->andWhere(['>=', 'akhir_berlaku', $this->$attribute])->andWhere(['pegawai_id' => $this->pegawai_id])->one();
        if ($query) {
            if (($query->id != $this->id) && $this->isNewRecord) {
                $this->addError($attribute, Yii::t('frontend', 'Tanggal telah terpakai.'));
            }
        }
    }


    /**
     * @param int $sistemKerja
     */
    public function nonAkatifkanSistemKerjaLama(int $sistemKerja)
    {
        $query = PegawaiSistemKerja::findOne($sistemKerja);
        if ($query) {
            $query->akhir_berlaku = date('Y-m-d', strtotime($this->mulai_berlaku . ' -1 day'));
            $query->save();
        }
    }

    /**
     * @param string $tanggal
     * @return array|ActiveRecord|null
     */
    public function sistemKerjaBerdasarTanggal(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        return PegawaiSistemKerja::find()->andWhere(['pegawai_id' => $this->pegawai_id])
            ->andWhere('(mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku >= \'' . $tanggal . '\'' . ') OR (' . 'mulai_berlaku <=\'' . $tanggal . '\' AND akhir_berlaku is null )')
            ->one();
    }

    /**
     * @param string $tanggal
     * @return bool
     */
    public function isAktif(string $tanggal = '')
    {
        if (empty($tanggal)) {
            $tanggal = date('Y-m-d');
        }
        /** @var SistemKerja $sistemKerjaBerdasarTanggal */
        if ($sistemKerjaBerdasarTanggal = $this->sistemKerjaBerdasarTanggal($tanggal)) {
            if ($sistemKerjaBerdasarTanggal->id == $this->id) {
                return true;
            }
        }
        return false;
    }

}

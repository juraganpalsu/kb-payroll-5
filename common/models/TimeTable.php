<?php

namespace common\models;

use common\models\base\TimeTable as BaseTimeTable;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "time_table".
 *
 * @property array $_shift
 * @property string $shift_
 */
class TimeTable extends BaseTimeTable
{

    const NON_SHIFT = 0;
    const SHIFT_SATU = 1;
    const SHIFT_DUA = 2;
    const SHIFT_TIGA = 3;

    public $_shift = [self::NON_SHIFT => 'NON SHIFT', self::SHIFT_SATU => 'SHIFT SATU', self::SHIFT_DUA => 'SHIFT DUA', self::SHIFT_TIGA => 'SHIFT TIGA'];

    const NORMAL_JAM_KERJA = 8;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
            [
                [['nama', 'masuk_minimum', 'masuk', 'masuk_maksimum', 'toleransi_masuk', 'pulang_minimum', 'pulang', 'pulang_maksimum'], 'required'],
                [['masuk_minimum', 'masuk', 'masuk_maksimum', 'istirahat_minimum', 'istirahat', 'istirahat_maksimum', 'masuk_istirahat_minimum', 'masuk_istirahat', 'masuk_istirahat_maksimum', 'pulang_minimum', 'pulang', 'pulang_maksimum', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
                [['shift', 'sistem_kerja', 'toleransi_masuk', 'toleransi_pulang', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
                [['nama'], 'string', 'max' => 45],
                [['lock', 'untuk_hari', 'toleransi_masuk', 'toleransi_pulang', 'sistem_kerja'], 'default', 'value' => 0],
                [['lock'], 'mootensai\components\OptimisticLockValidator']
            ];
    }


    /**
     * Get shift
     *
     * @return string
     */
    public function getShift_()
    {
        return $this->_shift[$this->shift];
    }

    /**
     *
     * Mengambil time table berdasarkan sistem kerja
     *
     * @param int $sistemKerja integer sistem kerja
     * @return array|ActiveRecord[]
     */
    public function getTimeTableBySistemKerja(int $sistemKerja)
    {
        $query = SistemKerja::findOne($sistemKerja);
        return $query->timeTables;
    }

    /**
     * @return string
     */
    public function getNamaSistemKerja()
    {
        return $this->nama;
    }

    /**
     *
     * Menghitung jumlah jam kerja dalam satu hari
     *
     * @return float|int
     */
    public function hitungJamKerja()
    {
        $masuk = strtotime($this->masuk) == strtotime('midnight') ? strtotime($this->masuk) - 3600 : strtotime($this->masuk);
        $pulang = strtotime($this->pulang) == strtotime('midnight') ? strtotime($this->pulang) + 3600 : strtotime($this->pulang);
        $hasil = ($pulang - $masuk) / 3600;
        if ($masuk > $pulang) {
            $hasil = (((24 * 3600) + $pulang) - $masuk) / 3600;
        }
        return $hasil;

    }


}

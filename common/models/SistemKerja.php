<?php

namespace common\models;

use common\models\base\SistemKerja as BaseSistemKerja;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "sistem_kerja".
 *
 * @property array $_enamHariKerja
 * @property array $_jenisLibur
 *
 * @property string $hariKeenamSetengahHari
 * @property string $jenisLibur;
 * @property array $hariLibur
 */
class SistemKerja extends BaseSistemKerja
{

    const TIDAK = 0;
    const YA = 1;

    const HARI = 0;
    const POLA = 1;

    /**
     * @var array Apakah hari keenam setengah hari
     */
    public $_enamHariKerja = [self::TIDAK => 'TIDAK', self::YA => 'YA'];


    /**
     * @var array Jenis libur
     */
    public $_jenisLibur = [self::HARI => 'HARI', self::POLA => 'POLA'];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'hari_kerja_aktif'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['hari_kerja_aktif', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['nama'], 'string', 'max' => 45],
            [['hari_keenam_setengah', 'jenis_libur'], 'integer'],
            [['hari_kerja_aktif', 'hari_keenam_setengah', 'jenis_libur', 'lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @return mixed|string Ya atau tidak
     */
    public function getHariKeenamSetengahHari()
    {
        return $this->_enamHariKerja[$this->hari_keenam_setengah];
    }

    /**
     * @return mixed|string Jenis Libur
     */
    public function getJenisLibur()
    {
        return $this->_jenisLibur[$this->jenis_libur];
    }

    /**
     * @param int $id
     * @return SistemKerja|null
     */
    public static function getConfig(int $id)
    {
        return self::findOne($id);
    }

    /**
     * @return array|mixed
     */
    public function getHariLibur()
    {
        $hari = [];
        if ($this->jenis_libur == self::HARI) {
            $polaJadwalLibur = $this->polaJadwalLiburHari;
            if (!empty($polaJadwalLibur)) {
                $hari = Json::decode($polaJadwalLibur->hari);
            }
        }
        return $hari;
    }

    /**
     * @param null $q
     * @return array|ActiveRecord[]
     */
    public function listSelect2($q = null)
    {
        return self::find()->select(['id', 'nama as text'])
            ->andWhere(['like', 'nama', $q])->limit(20)->asArray()->all();
    }

}

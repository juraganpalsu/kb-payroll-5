<?php

namespace common\models;

use common\models\base\GantiHari as BaseGantiHari;

/**
 * This is the model class for table "ganti_hari".
 */
class GantiHari extends BaseGantiHari
{

    public $pegawai_ids;
    public $pegawai_exist = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
            [
                [['pegawai_ids', 'jumlah_jam'], 'required'],
                [['jumlah_jam'], 'number'],
                [['keterangan'], 'string'],
                [['pegawai_id', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
                [['created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
                [['lock'], 'default', 'value' => '0'],
                [['lock'], 'mootensai\components\OptimisticLockValidator']
            ];
    }

    /**
     * Merubah iputan yg sebelumnya tanda "," menjadi "."
     * agar inputan setengah terbaca
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->jumlah_jam = str_replace(",", ".", $this->jumlah_jam);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Menambah pegawai
     *
     * @param string $pegawai_id
     * @return bool
     */
    public function createDetail(string $pegawai_id)
    {
        $modelDetail = new GantiHariDetail();
        $modelDetail->pegawai_id = $pegawai_id;
        $modelDetail->ganti_hari_id = $this->id;
        if ($modelDetail->save()) {
            return $this->kalkulasiHutang($modelDetail);
        }
        return false;
    }

    /**
     * Menghapus pegawai
     *
     * @param string $pegawai_id
     * @return bool|false|int
     */
    public function deleteDetail(string $pegawai_id)
    {
        $query = GantiHariDetail::findOne(['ganti_hari_id' => $this->id, 'pegawai_id' => $pegawai_id]);
        if ($query) {
            if ($query->delete()) {
                return $this->kalkulasiHutang($query, '-');
            }
        }
        return false;
    }

    /**
     * Menghitung jumlah total hutang hari(dalam hitungan jam)
     *
     * @param GantiHariDetail $gantiHariDetail
     * @param string $aksi
     * @return bool
     */
    public function kalkulasiHutang(GantiHariDetail $gantiHariDetail, string $aksi = '+')
    {
        $model = new GantiHariJumlah();
        $model->jumlah_hutang = $gantiHariDetail->gantiHari->jumlah_jam;
        $model->pegawai_id = $gantiHariDetail->pegawai_id;
        $cek = GantiHariJumlah::findOne(['pegawai_id' => $gantiHariDetail->pegawai_id]);
        if ($cek) {
            $model = $cek;
            if ($aksi == '+') {
                $model->jumlah_hutang += $gantiHariDetail->gantiHari->jumlah_jam;
            } else {
                $model->jumlah_hutang -= $gantiHariDetail->gantiHari->jumlah_jam;

            }
        }
        return $model->save();
    }

    public function kurangiHutang()
    {

    }


}

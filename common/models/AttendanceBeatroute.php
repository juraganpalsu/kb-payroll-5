<?php

namespace common\models;

use common\models\base\AttendanceBeatroute as BaseAttendanceBeatroute;

/**
 * This is the model class for table "attendance_beatroute".
 *
 *
 * @property array $_status
 */
class AttendanceBeatroute extends BaseAttendanceBeatroute
{

    const P = 'P';
    const A = 'A';

    public $_status = [self::P => 'P', self::A => 'A'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emp_role_id', 'user_br_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['date'], 'required'],
            [['date', 'duration', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['status'], 'string', 'max' => 5],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

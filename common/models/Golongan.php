<?php

namespace common\models;

use common\models\base\Golongan as BaseGolongan;

/**
 * This is the model class for table "golongan".
 */
class Golongan extends BaseGolongan
{

    const NOL = 1;
    const SATU_H = 2;
    const DUA = 3;
    const TIGA = 4;
    const EMPAT = 5;
    const LIMA = 6;
    const ENAM = 7;
    const SATU_B = 8;
    const TUJUH = 15;
    const TIGA_GT = 16;
    const EMPAT_GT = 17;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'urutan'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['urutan', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['nama'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

}

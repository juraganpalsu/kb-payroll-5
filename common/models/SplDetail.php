<?php

namespace common\models;

use common\components\Status;
use common\models\base\SplDetail as BaseSplDetail;
use frontend\models\status\SplDetailApproval;
use frontend\models\status\SplDetailStatus;
use frontend\models\User;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "spl_detail".
 *
 *
 * @property SplDetailApproval $approvalByUser
 * @property SplDetailApproval $rootApproval
 *
 */
class SplDetail extends BaseSplDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['perjanjian_kerja_id', 'pegawai_struktur_id', 'pegawai_id', 'spl_id'], 'required'],
            [['last_status', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan_approval'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'pegawai_id', 'spl_id', 'perjanjian_kerja_id', 'pegawai_struktur_id', 'created_by_pk'], 'string', 'max' => 32],
            [['lock', 'tipe_approval'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->createStatus(Status::OPEN);
        }
    }

    /**
     * @return ActiveDataProvider
     */
    public function dataProviderAttendance()
    {
        $query = Attendance::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->where([
            'idfp' => $this->perjanjianKerja->id_pegawai,
        ]);

        $kemarin = date('Y-m-d', strtotime($this->spl->tanggal . '-1 day'));
        $besok = date('Y-m-d', strtotime($this->spl->tanggal . '+1 day'));

        $query->andWhere(['BETWEEN', 'date(check_time)', $kemarin, $besok]);

        $query->orderBy('check_time ASC');

        return $dataProvider;
    }

    /**
     * Membuat status
     *
     * @param $status
     */
    public function createStatus($status)
    {
        $modeStatus = new SplDetailStatus();
        $modeStatus->status = $status;
        $modeStatus->spl_detail_id = $this->id;
        if ($modeStatus->save()) {
            $this->updateLastStatus($status);
        }
    }


    /**
     *
     * @param int $status Status terakhir yg diterima
     */
    public function updateLastStatus(int $status)
    {
        $model = self::findOne($this->id);
        if ($status == Status::REVISED) {
            if (in_array($model->last_status, [Status::FINISHED, Status::APPROVED])) {
                $model->last_status = $status;
            }
        } else {
            $model->last_status = $status;
        }
        if ($model->save()) {
            if ($model->last_status == Status::CLOSED) {
                $parent = $this->spl;
                if ($parent->checkAllItemHasBeenClosed()) {
                    $parent->createStatus(Status::CLOSED);
                }
            }
        }
    }

    /**
     * Menampilkan status terakhir dalam bentuk string
     *
     * @return string
     */
    public function getLastStatus()
    {
        return Status::statuses($this->last_status) ?? '';
    }

    /**
     * Melakukan aproval product
     *
     * @param int $status
     * @param string $comment
     */
    public function approval(int $status, string $comment = '')
    {
        if ($this->approvalByUser) {
            $this->approvalByUser->approval($status, $comment);
        }
    }

    /**
     * Tambah relasi ke detail approval tetapi ditambah dengan filter user_id
     * Relasi Approval berdasarkan user login
     *
     * @return ActiveQuery
     */
    public function getApprovalByUser()
    {
        return $this->hasOne(SplDetailApproval::class, ['spl_detail_id' => 'id'])->andWhere(['spl_detail_approval.pegawai_id' => User::pegawai()->id]);
    }

    /**
     *
     * Relasi ke table approval -> mecari root dari sebuah approval
     *
     * @return ActiveQuery
     */
    public function getRootApprovals()
    {
        return $this->hasOne(SplDetailApproval::class, ['spl_detail_id' => 'id'])->andWhere(['spl_detail_approval.spl_detail_approval_id' => 0]);
    }

    /**
     * Cek semua status status approval
     *
     * @param int $status default Status::REJECT
     *
     * @return bool true jika ada status yg sesuai dengan param, sebaliknya false
     */
    public function checkStatusApprovals(int $status = Status::REJECT)
    {
        $rejects = [];
        array_map(function (SplDetailApproval $x) use (&$rejects) {
            $rejects[] = $x->tipe;
        }, $this->splDetailApprovals);

        if (in_array($status, $rejects)) {
            return true;
        }
        return false;
    }

    /**
     * @param string $tanggal
     * @return array|ActiveRecord[]
     */
    public function cekPegawaiTelahDibuatkanSpl(string $tanggal)
    {
        return self::find()->joinWith(['spl' => function (ActiveQuery $spl) use ($tanggal) {
            $spl->andWhere(['tanggal' => $tanggal]);
        }])->andWhere(['spl_detail.pegawai_id' => $this->pegawai_id])->andWhere(['IN', 'spl_detail.last_status', [Status::OPEN, Status::SUBMITED, Status::APPROVED, Status::FINISHED, Status::CLOSED]])->one();
    }

}

<?php

namespace common\models\base;

use mootensai\relation\RelationTrait;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "spl_template".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $sistem_kerja
 * @property integer $sebagai_header
 * @property integer $kategori
 * @property double $jumlah_jam_lembur
 * @property double $istirahat
 * @property integer $tunjangan_shift
 * @property string $masuk_minimum
 * @property string $masuk
 * @property integer $toleransi_masuk
 * @property string $masuk_maksimum
 * @property string $jam_istirahat_minimum
 * @property string $jam_istirahat
 * @property string $jam_istirahat_maksimum
 * @property string $masuk_istirahat_minimum
 * @property string $masuk_istirahat
 * @property string $masuk_istirahat_maksimum
 * @property string $pulang_minimum
 * @property string $pulang
 * @property integer $toleransi_pulang
 * @property string $pulang_maksimum
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\models\Spl[] $spls
 * @property \common\models\SplTemplateDetail[] $splTemplateDetail
 */
class SplTemplate extends ActiveRecord
{

    use RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['toleransi_masuk', 'toleransi_pulang','sistem_kerja', 'kategori', 'tunjangan_shift', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['istirahat', 'jumlah_jam_lembur'], 'number'],
            [['masuk_minimum', 'masuk', 'masuk_maksimum', 'jam_istirahat_minimum', 'jam_istirahat', 'jam_istirahat_maksimum', 'masuk_istirahat_minimum', 'masuk_istirahat', 'masuk_istirahat_maksimum', 'pulang_minimum', 'pulang', 'pulang_maksimum', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['nama'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spl_template';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'sistem_kerja' => Yii::t('app', 'Sistem Kerja'),
            'sebagai_header' => Yii::t('app', 'Sebagai Header'),
            'kategori' => Yii::t('app', 'Kategori'),
            'istirahat' => Yii::t('app', 'Jumlah Jam Istirahat'),
            'jumlah_jam_lembur' => Yii::t('app', 'Jumlah Jam Lebur'),
            'masuk_minimum' => Yii::t('app', 'Masuk Minimum'),
            'masuk' => Yii::t('app', 'Masuk'),
            'toleransi_masuk' => Yii::t('frontend', 'Toleransi Masuk'),
            'masuk_maksimum' => Yii::t('app', 'Masuk Maksimum'),
            'pulang_minimum' => Yii::t('app', 'Pulang Minimum'),
            'pulang' => Yii::t('app', 'Pulang'),
            'toleransi_pulang' => Yii::t('frontend', 'Toleransi Pulang'),
            'pulang_maksimum' => Yii::t('app', 'Pulang Maksimum'),
            'lock' => Yii::t('app', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSpls()
    {
        return $this->hasMany(\frontend\models\Spl::class, ['spl_template_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplTemplateDetail()
    {
        return $this->hasMany(\common\models\SplTemplateDetail::class, ['spl_template_id' => 'id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where(['spl_template.deleted_by' => null])->orWhere(['spl_template.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace common\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "sistem_kerja".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $hari_keenam_setengah
 * @property integer $jenis_libur
 * @property integer $hari_kerja_aktif
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \common\models\PolaJadwalLibur[] $polaJadwalLiburs
 * @property \common\models\PolaJadwalLibur $polaJadwalLiburHari
 * @property \common\models\SistemKerjaTimeTable[] $sistemKerjaTimeTables
 * @property \common\models\TimeTable[] $timeTables
 */
class SistemKerja extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['hari_kerja_aktif', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['nama'], 'string', 'max' => 45],
            [['hari_keenam_setengah', 'jenis_libur'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sistem_kerja';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'hari_keenam_setengah' => Yii::t('app', 'Hari Keenam Setengah'),
            'jenis_libur' => Yii::t('app', 'Jenis Libur'),
            'hari_kerja_aktif' => Yii::t('app', 'Hari Kerja Aktif'),
            'lock' => Yii::t('app', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPolaJadwalLiburs()
    {
        return $this->hasMany(\common\models\PolaJadwalLibur::class, ['pola_jadwal_libur.sistem_kerja_id' => 'id'])->andWhere(['pola_jadwal_libur.deleted_by' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getPolaJadwalLiburHari()
    {
        return $this->hasOne(\common\models\PolaJadwalLibur::class, ['pola_jadwal_libur.sistem_kerja_id' => 'id'])->andWhere(['pola_jadwal_libur.deleted_by' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getSistemKerjaTimeTables()
    {
        return $this->hasMany(\common\models\SistemKerjaTimeTable::class, ['sistem_kerja_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTimeTables()
    {
        return $this->hasMany(\common\models\TimeTable::class, ['id' => 'time_table_id'])->via('sistemKerjaTimeTables')->orderBy('masuk ASC');
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where(['sistem_kerja.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

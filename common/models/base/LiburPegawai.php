<?php

namespace common\models\base;

use frontend\models\User;
use mootensai\relation\RelationTrait;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "libur_pegawai".
 *
 * @property integer $id
 * @property string $tanggal
 * @property string $pegawai_id
 * @property integer $sistem_kerja_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \frontend\models\Pegawai $pegawai
 * @property \common\models\SistemKerja $sistemKerja
 * @property User $createdBy
 */
class LiburPegawai extends ActiveRecord
{

    use RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['sistem_kerja_id','created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['pegawai_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'libur_pegawai';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'pegawai_id' => Yii::t('app', 'Pegawai ID'),
            'sistem_kerja_id' => Yii::t('app', 'Sistem Kerja ID'),
            'lock' => Yii::t('app', 'Lock'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(\frontend\models\Pegawai::className(), ['id' => 'pegawai_id'])->andWhere([\common\models\Pegawai::tableName() . '.deleted_by' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSistemKerja()
    {
        return $this->hasOne(\common\models\SistemKerja::className(), ['id' => 'sistem_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where(['libur_pegawai.deleted_by' => null])->orWhere(['libur_pegawai.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveQuery;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "pegawai_sistem_kerja".
 *
 * @property integer $id
 * @property string $mulai_berlaku
 * @property string $akhir_berlaku
 * @property string $mulai_libur
 * @property string $keterangan
 * @property string $pegawai_id
 * @property integer $sistem_kerja_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \common\models\Pegawai $pegawai
 * @property \common\models\SistemKerja $sistemKerja
 */
class PegawaiSistemKerja extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mulai_berlaku', 'pegawai_id', 'sistem_kerja_id'], 'required'],
            [['mulai_berlaku', 'akhir_berlaku', 'mulai_libur', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['sistem_kerja_id', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['pegawai_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai_sistem_kerja';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mulai_berlaku' => Yii::t('app', 'Mulai Berlaku'),
            'akhir_berlaku' => Yii::t('app', 'Akhir Berlaku'),
            'mulai_libur' => Yii::t('app', 'Mulai Libur'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'pegawai_id' => Yii::t('app', 'Pegawai ID'),
            'sistem_kerja_id' => Yii::t('app', 'Sistem Kerja ID'),
            'lock' => Yii::t('app', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(\common\models\Pegawai::className(), ['id' => 'pegawai_id'])->andWhere([\common\models\Pegawai::tableName() . '.deleted_by' => 0]);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSistemKerja()
    {
        return $this->hasOne(\common\models\SistemKerja::className(), ['id' => 'sistem_kerja_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where(['pegawai_sistem_kerja.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}


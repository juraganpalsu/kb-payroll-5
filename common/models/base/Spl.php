<?php

namespace common\models\base;

use common\components\CreatedByBehavior;
use common\components\Status;
use frontend\models\status\SplApproval;
use frontend\models\status\SplStatus;
use frontend\models\User;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\spl\models\SplAlasan;
use frontend\modules\struktur\models\Struktur;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "spl".
 *
 * @property string $id
 * @property integer $seq_code
 * @property string $tanggal
 * @property integer $spl_template_id
 * @property integer $klaim_quaker
 * @property integer $jumlah_konversi
 * @property integer $unit
 * @property integer $divisi
 * @property integer $jabatan
 * @property string $keterangan
 * @property integer $kategori
 * @property string $spl_alasan_id
 * @property string $cost_center_id
 * @property string $spl_id
 * @property integer $last_status
 * @property boolean $is_planing
 * @property boolean $is_revision
 * @property integer $status_approval
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \common\models\SplTemplate $splTemplate
 * @property SplDetail[] $splDetails
 * @property SplDetail[] $splDetailsApproved
 * @property SplStatus[] $splStatuses
 * @property SplApproval[] $splApprovals
 * @property SplApproval $splApproval
 * @property User $createdBy
 * @property Struktur $createdByStruktur
 * @property PerjanjianKerja $createdByPk
 * @property SplAlasan $splAlasan
 * @property \common\models\CostCenterTemplate $costCenter
 * @property \common\models\Spl $splChild
 * @property \common\models\Spl $splParent
 *
 */
class Spl extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'spl_template_id'], 'required'],
            [['seq_code', 'kategori', 'spl_template_id', 'divisi', 'last_status', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal', 'created_at', 'updated_at', 'deleted_at', 'is_planing', 'is_revision'], 'safe'],
            [['keterangan'], 'string'],
            [['id', 'spl_alasan_id', 'cost_center_id', 'created_by_pk', 'spl_id'], 'string', 'max' => 32],
            [['unit', 'jabatan'], 'string', 'max' => 1],
            [['status_approval'], 'string', 'max' => 2],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spl';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('frontend', 'ID'),
            'seq_code' => Yii::t('frontend', 'Seq Code'),
            'tanggal' => Yii::t('frontend', 'Tanggal'),
            'klaim_quaker' => Yii::t('frontend', 'Klaim Quaker'),
            'spl_template_id' => Yii::t('frontend', 'Spkl Template ID'),
            'jumlah_konversi' => Yii::t('frontend', 'Jumlah'),
            'unit' => Yii::t('frontend', 'Unit'),
            'divisi' => Yii::t('frontend', 'Divisi'),
            'jabatan' => Yii::t('frontend', 'Jabatan'),
            'keterangan' => Yii::t('frontend', 'Keterangan Alasan Lembur'),
            'kategori' => Yii::t('frontend', 'Kategori'),
            'spl_alasan_id' => Yii::t('frontend', 'Kategori Alasan'),
            'cost_center_id' => Yii::t('frontend', 'Cost Center'),
            'spl_id' => Yii::t('frontend', 'SPL ID'),
            'last_status' => Yii::t('frontend', 'Last Status'),
            'is_planing' => Yii::t('frontend', 'Terencana'),
            'is_revision' => Yii::t('frontend', 'Is Revision'),
            'status_approval' => Yii::t('frontend', 'Status Approval'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'jumlah_jam' => Yii::t('frontend', 'Jumlah Jam'),
            'lock' => Yii::t('frontend', 'Lock'),
        );
    }

    /**
     * @return ActiveQuery
     */
    public function getSplTemplate()
    {
        return $this->hasOne(\common\models\SplTemplate::class, ['id' => 'spl_template_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplDetails()
    {
        return $this->hasMany(\common\models\SplDetail::class, ['spl_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplDetailsApproved()
    {
        return $this->hasMany(\common\models\SplDetail::class, ['spl_id' => 'id'])->andWhere(['last_status' => Status::APPROVED]);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplStatuses()
    {
        return $this->hasMany(SplStatus::class, ['spl_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplApprovals()
    {
        return $this->hasMany(SplApproval::class, ['spl_id' => 'id'])->orderBy('spl_approval.level ASC');
    }

    /**
     * @return ActiveQuery
     */
    public function getSplApproval()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        return $this->hasOne(SplApproval::class, ['spl_id' => 'id'])->andWhere(['spl_approval.pegawai_id' => $user->pegawai->id]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedByPk()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'created_by_pk']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedByStruktur()
    {
        return $this->hasOne(Struktur::class, ['id' => 'created_by_struktur']);
    }


    /**
     * @return ActiveQuery
     */
    public function getSplAlasan()
    {
        return $this->hasOne(SplAlasan::class, ['id' => 'spl_alasan_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getCostCenter()
    {
        return $this->hasOne(\common\models\CostCenterTemplate::class, ['id' => 'cost_center_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplParent()
    {
        return $this->hasOne(\common\models\Spl::class, ['spl_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplChild()
    {
        return $this->hasOne(\common\models\Spl::class, ['id' => 'spl_id']);
    }


    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace common\models\base;

use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use mootensai\relation\RelationTrait;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "attendance".
 *
 * @property string $id
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_struktur_id
 * @property string $check_time
 * @property integer $idfp
 * @property integer $uploaded
 * @property string $created_at
 * @property string $device_id
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \common\models\Pegawai $pegawai
 * @property PerjanjianKerja $perjanjianKerja
 * @property PegawaiStruktur $pegawaiStruktur
 * @property \common\models\AttendanceDevice $attendanceDevice
 */
class Attendance extends ActiveRecord
{

    use RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'check_time', 'idfp'], 'required'],
            [['device_id', 'check_time', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['uploaded', 'idfp', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id'], 'string', 'max' => 32],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attendance';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pegawai_id' => Yii::t('app', 'Pegawai ID'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'check_time' => Yii::t('app', 'Check Time'),
            'idfp' => Yii::t('app', 'Idfp'),
            'uploaded' => Yii::t('app', 'Uploaded'),
            'device_id' => Yii::t('app', 'Device ID'),
            'lock' => Yii::t('app', 'Lock'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(\common\models\Pegawai::class, ['id' => 'pegawai_id'])->andWhere([\common\models\Pegawai::tableName() . '.deleted_by' => 0]);
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getAttendanceDevice()
    {
        return $this->hasOne(\common\models\AttendanceDevice::class, ['id' => 'device_id'])->andWhere([\common\models\AttendanceDevice::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where(['attendance.deleted_by' => null])->orWhere(['attendance.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace common\models\base;

use frontend\models\User;
use mootensai\behaviors\UUIDBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "perubahan_status_absensi".
 *
 * @property string $id
 * @property integer $urut
 * @property string $tanggal
 * @property string $tanggal_pulang
 * @property string $masuk
 * @property string $pulang
 * @property integer $status_kehadiran
 * @property integer $time_table_id
 * @property string $jumlah_jam_lembur
 * @property string $keterangan
 * @property integer $shift
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \common\models\PerubahanStatusAbsensiDetail[] $perubahanStatusAbsensiDetails
 * @property \common\models\TimeTable $timeTable
 * @property User $createdBy
 */
class PerubahanStatusAbsensi extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
     * This function helps \mootensai\relation\RelationTrait runs faster
     * @return array relation names of this model
     */
    public function relationNames()
    {
        return [
            'perubahanStatusAbsensiDetails'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanggal', 'tanggal_pulang', 'status_kehadiran'], 'required'],
            [['urut', 'status_kehadiran', 'shift', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['tanggal', 'tanggal_pulang', 'masuk', 'pulang', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['id'], 'string', 'max' => 32],
            [['jumlah_jam_lembur'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perubahan_status_absensi';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'urut' => Yii::t('app', 'Urut'),
            'tanggal' => Yii::t('app', 'Tanggal Masuk'),
            'tanggal_pulang' => Yii::t('app', 'Tanggal Pulang'),
            'masuk' => Yii::t('app', 'Masuk'),
            'pulang' => Yii::t('app', 'Pulang'),
            'status_kehadiran' => Yii::t('app', 'Status Kehadiran'),
            'jumlah_jam_lembur' => Yii::t('app', 'Jumlah Jam Lembur'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'shift' => Yii::t('app', 'Shift'),
            'lock' => Yii::t('app', 'Lock'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerubahanStatusAbsensiDetails()
    {
        return $this->hasMany(\common\models\PerubahanStatusAbsensiDetail::className(), ['perubahan_status_absensi_id' => 'id'])->andWhere(['perubahan_status_absensi_detail.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }


    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeTable()
    {
        return $this->hasOne(TimeTable::class, ['id' => 'time_table_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

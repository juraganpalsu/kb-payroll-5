<?php

namespace common\models\base;

use frontend\modules\erp\models\EGudangPegawai;
use frontend\modules\indonesia\models\IndonesiaDesa;
use frontend\modules\indonesia\models\IndonesiaKabupatenKota;
use frontend\modules\indonesia\models\IndonesiaKecamatan;
use frontend\modules\indonesia\models\IndonesiaProvinsi;
use frontend\modules\masterdata\models\MasterDataDefault;
use frontend\modules\masterdata\models\PGajiPokok;
use frontend\modules\masterdata\models\PInsentifTetap;
use frontend\modules\masterdata\models\PPremiHadir;
use frontend\modules\masterdata\models\PSewaMobil;
use frontend\modules\masterdata\models\PSewaMotor;
use frontend\modules\masterdata\models\PSewaRumah;
use frontend\modules\masterdata\models\PTunjaganLainTetap;
use frontend\modules\masterdata\models\PTunjanganJabatan;
use frontend\modules\masterdata\models\PTunjanganKost;
use frontend\modules\masterdata\models\PTunjanganPulsa;
use frontend\modules\masterdata\models\PUangMakan;
use frontend\modules\masterdata\models\PUangTransport;
use frontend\modules\pegawai\models\AkunBeatroute;
use frontend\modules\pegawai\models\AreaKerja;
use frontend\modules\pegawai\models\CostCenter;
use frontend\modules\pegawai\models\PegawaiAsuransiLain;
use frontend\modules\pegawai\models\PegawaiBank;
use frontend\modules\pegawai\models\PegawaiBpjs;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiKeluarga;
use frontend\modules\pegawai\models\PegawaiKontakDarurat;
use frontend\modules\pegawai\models\PegawaiNpwp;
use frontend\modules\pegawai\models\PegawaiPendidikan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use frontend\modules\pegawai\models\UserPegawai;
use frontend\modules\personalia\models\SuratPeringatan;
use frontend\modules\personalia\models\Training;
use mootensai\behaviors\UUIDBehavior;
use mootensai\relation\RelationTrait;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "pegawai".
 *
 * @property string $id
 * @property string $nama
 * @property string $nama_lengkap
 * @property string $nama_panggilan
 * @property string $nama_ibu_kandung
 * @property integer $jenis_kelamin
 * @property integer $gol_darah
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $status_pernikahan
 * @property string $kewarganegaraan
 * @property integer $agama
 * @property string $nomor_ktp
 * @property string $nomor_kk
 * @property string $no_telp
 * @property string $no_handphone
 * @property string $alamat_email
 * @property integer $ktp_provinsi_id
 * @property integer $ktp_kabupaten_kota_id
 * @property integer $ktp_kecamatan_id
 * @property integer $ktp_desa_id
 * @property string $ktp_rw
 * @property string $ktp_rt
 * @property string $ktp_alamat
 * @property integer $domisili_provinsi_id
 * @property integer $domisili_kabupaten_kota_id
 * @property integer $domisili_kecamatan_id
 * @property integer $domisili_desa_id
 * @property string $domisili_rw
 * @property string $domisili_rt
 * @property string $domisili_alamat
 * @property string $pegawai_id
 * @property string $catatan
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 * @property integer $has_uploaded_to_att
 * @property integer $has_created_account
 * @property integer $is_resign
 * @property integer $has_downloadded_to_training_app
 * @property integer $ess_status
 *
 * @property UserPegawai $userPegawai
 * @property \frontend\models\Absensi[] $absensis
 * @property \frontend\models\Attendance[] $attendances
 * @property \frontend\models\SplDetail[] $splDetails
 * @property \common\models\PegawaiSistemKerja[] $pegawaiSistemKerjas
 * @property PegawaiStruktur[] $pegawaiStrukturs
 * @property PerjanjianKerja[] $perjanjianKerjas
 * @property AkunBeatroute[] $akunBeatroutes
 * @property PegawaiGolongan[] $pegawaiGolongans
 * @property CostCenter[] $costCenters
 * @property AreaKerja[] $areaKerjas
 * @property PegawaiNpwp $pegawaiNpwp
 * @property PegawaiAsuransiLain $pegawaiAsuransiLain
 * @property PegawaiAsuransiLains[] $pegawaiAsuransiLains
 * @property PegawaiPendidikan[] $pegawaiPendidikan
 * @property PegawaiKeluarga[] $pegawaiKeluarga
 * @property PegawaiBpjs[] $pegawaiBpjs
 * @property PegawaiKontakDarurat[] $pegawaiKontakDarurat
 * @property PegawaiBank $pegawaiBank
 * @property IndonesiaProvinsi $pegawaiKtpProvinsi
 * @property IndonesiaKabupatenKota $pegawaiKtpKabupaten
 * @property IndonesiaKecamatan $pegawaiKtpKecamatan
 * @property IndonesiaDesa $pegawaiKtpDesa
 * @property IndonesiaProvinsi $pegawaiDomisiliProvinsi
 * @property IndonesiaKabupatenKota $pegawaiDomisiliKabupaten
 * @property IndonesiaKecamatan $pegawaiDomisiliKecamatan
 * @property IndonesiaDesa $pegawaiDomisiliDesa
 * @property SuratPeringatan $suratPeringatans
 * @property Training $trainings
 * @property MasterDataDefault $masterDataDefaults
 * @property PGajiPokok[] $pGajiPokoks
 * @property PInsentifTetap[] $pInsentifTetaps
 * @property PPremiHadir[] $pPremiHadirs
 * @property PSewaMobil[] $pSewaMobils
 * @property PSewaMotor[] $pSewaMotors
 * @property PSewaRumah[] $pSewaRumahs
 * @property PTunjaganLainTetap[] $pTunjaganLainTetaps
 * @property PTunjanganJabatan[] $pTunjanganJabatans
 * @property PTunjanganKost[] $pTunjanganKosts
 * @property PTunjanganPulsa[] $pTunjanganPulsas
 * @property PUangMakan[] $pUangMakans
 * @property PUangTransport[] $pUangTransports
 * @property EGudangPegawai[] $eGudangPegawais
 *
 */
class Pegawai extends ActiveRecord
{

    use RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_lengkap', 'nama_panggilan', 'nama_ibu_kandung', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'status_pernikahan', 'kewarganegaraan', 'agama', 'nomor_ktp', 'no_handphone'], 'required'],
            [['tanggal_lahir', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['catatan'], 'string'],
            [['created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock', 'has_downloadded_to_training_app'], 'integer'],
            [['ktp_provinsi_id', 'ktp_kabupaten_kota_id', 'ktp_kecamatan_id', 'ktp_desa_id', 'domisili_provinsi_id', 'domisili_kabupaten_kota_id', 'domisili_kecamatan_id', 'domisili_desa_id'], 'integer'],
            [['domisili_alamat', 'ktp_alamat', 'catatan'], 'string'],
            [['ktp_rw', 'ktp_rt', 'domisili_rw', 'domisili_rt'], 'string', 'max' => 3],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['nama_lengkap', 'nama_ibu_kandung', 'alamat_email'], 'string', 'max' => 225],
            [['nama_panggilan', 'tempat_lahir', 'kewarganegaraan', 'nomor_kk'], 'string', 'max' => 45],
            [['jenis_kelamin', 'gol_darah'], 'string', 'max' => 1],
            [['status_pernikahan', 'agama'], 'string', 'max' => 2],
            [['nomor_ktp'], 'string', 'max' => 16],
            [['no_telp', 'no_handphone'], 'string', 'max' => 13],
            [['lock', 'has_uploaded_to_att', 'has_created_account'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai';
    }


    /**
     * @return string
     */
    public function getNama()
    {
        return $this->nama_lengkap;
    }


    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'has_uploaded_to_att' => Yii::t('app', 'Has Uploaded To Att'),
            'has_created_account' => Yii::t('app', 'Has Created Account'),
            'nama_panggilan' => Yii::t('frontend', 'Nama Panggilan'),
            'nama_ibu_kandung' => Yii::t('frontend', 'Nama Ibu Kandung'),
            'jenis_kelamin' => Yii::t('frontend', 'Jenis Kelamin'),
            'tempat_lahir' => Yii::t('frontend', 'Tempat Lahir'),
            'tanggal_lahir' => Yii::t('frontend', 'Tanggal Lahir'),
            'status_pernikahan' => Yii::t('frontend', 'Status Pernikahan'),
            'kewarganegaraan' => Yii::t('frontend', 'Kewarganegaraan'),
            'agama' => Yii::t('frontend', 'Agama'),
            'nomor_ktp' => Yii::t('frontend', 'Nomor KTP'),
            'nomor_kk' => Yii::t('frontend', 'Nomor Kartu Keluarga'),
            'no_telp' => Yii::t('frontend', 'No Telp'),
            'no_handphone' => Yii::t('frontend', 'No Handphone'),
            'alamat_email' => Yii::t('frontend', 'Alamat Email Pribadi'),
            'lock' => Yii::t('app', 'Lock'),
            'ktp_provinsi_id' => Yii::t('frontend', 'Provinsi'),
            'ktp_kabupaten_kota_id' => Yii::t('frontend', 'Kabupaten Kota'),
            'ktp_kecamatan_id' => Yii::t('frontend', 'Kecamatan'),
            'ktp_desa_id' => Yii::t('frontend', 'Desa'),
            'ktp_rw' => Yii::t('frontend', 'Rw'),
            'ktp_rt' => Yii::t('frontend', 'Rt'),
            'ktp_alamat' => Yii::t('frontend', 'Alamat'),
            'domisili_provinsi_id' => Yii::t('frontend', 'Provinsi'),
            'domisili_kabupaten_kota_id' => Yii::t('frontend', 'Kabupaten Kota'),
            'domisili_kecamatan_id' => Yii::t('frontend', 'Kecamatan'),
            'domisili_desa_id' => Yii::t('frontend', 'Desa'),
            'domisili_rw' => Yii::t('frontend', 'Rw'),
            'domisili_rt' => Yii::t('frontend', 'Rt'),
            'domisili_alamat' => Yii::t('frontend', 'Alamat'),
            'is_resign' => Yii::t('frontend', 'Resign'),
            'has_downloadded_to_training_app' => Yii::t('frontend', 'Has Downloaded'),
            'ess_status' => Yii::t('frontend', 'Ess Status')
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAbsensis()
    {
        return $this->hasMany(\frontend\models\Absensi::class, ['pegawai_id' => 'id'])->orderBy('id DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getAttendances()
    {
        return $this->hasMany(\frontend\models\Attendance::class, ['pegawai_id' => 'id'])->orderBy('id DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getGantiHariDetails()
    {
        return $this->hasMany(\frontend\models\GantiHariDetail::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGantiHariJumlahs()
    {
        return $this->hasMany(\frontend\models\GantiHariJumlah::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplDetails()
    {
        return $this->hasMany(\frontend\models\SplDetail::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiSistemKerjas()
    {
        return $this->hasMany(\common\models\PegawaiSistemKerja::class, ['pegawai_id' => 'id'])->andWhere([\common\models\PegawaiSistemKerja::tableName() . '.deleted_by' => 0])->orderBy(\common\models\PegawaiSistemKerja::tableName() . '.mulai_berlaku DESC');
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiStrukturs()
    {
        return $this->hasMany(PegawaiStruktur::class, ['pegawai_id' => 'id'])->andWhere([PegawaiStruktur::tableName() . '.deleted_by' => 0])->orderBy(PegawaiStruktur::tableName() . '.mulai_berlaku DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerjas()
    {
        return $this->hasMany(PerjanjianKerja::class, ['pegawai_id' => 'id'])->andWhere([PerjanjianKerja::tableName() . '.deleted_by' => 0])->orderBy(PerjanjianKerja::tableName() . '.mulai_berlaku DESC');
    }


    /**
     * @return ActiveQuery
     */
    public function getAkunBeatroutes()
    {
        return $this->hasMany(AkunBeatroute::class, ['pegawai_id' => 'id'])->andWhere([AkunBeatroute::tableName() . '.deleted_by' => 0])->orderBy(AkunBeatroute::tableName() . '.mulai_berlaku DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getCostCenters()
    {
        return $this->hasMany(CostCenter::class, ['pegawai_id' => 'id'])->andWhere([CostCenter::tableName() . '.deleted_by' => 0])->orderBy(CostCenter::tableName() . '.mulai_berlaku DESC');
    }


    /**
     * @return ActiveQuery
     */
    public function getAreaKerjas()
    {
        return $this->hasMany(AreaKerja::class, ['pegawai_id' => 'id'])->andWhere([AreaKerja::tableName() . '.deleted_by' => 0])->orderBy(AreaKerja::tableName() . '.mulai_berlaku DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongans()
    {
        return $this->hasMany(PegawaiGolongan::class, ['pegawai_id' => 'id'])->andWhere([PegawaiGolongan::tableName() . '.deleted_by' => 0])->orderBy(PegawaiGolongan::tableName() . '.mulai_berlaku DESC');
    }


    /**
     * @return ActiveQuery
     */
    public function getUserPegawai()
    {
        return $this->hasOne(UserPegawai::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiNpwp()
    {
        return $this->hasOne(PegawaiNpwp::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiAsuransiLain()
    {
        return $this->hasOne(PegawaiAsuransiLain::class, ['pegawai_id' => 'id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiAsuransiLains()
    {
        return $this->hasMany(PegawaiAsuransiLain::class, ['pegawai_id' => 'id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiPendidikan()
    {
        return $this->hasMany(PegawaiPendidikan::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiKeluarga()
    {
        return $this->hasMany(PegawaiKeluarga::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiBpjs()
    {
        return $this->hasMany(PegawaiBpjs::class, ['pegawai_id' => 'id'])->andWhere(['pegawai_keluaraga_id' => null]);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiKontakDarurat()
    {
        return $this->hasMany(PegawaiKontakDarurat::class, ['pegawai_id' => 'id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiBank()
    {
        return $this->hasOne(PegawaiBank::class, ['pegawai_id' => 'id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiKtpProvinsi()
    {
        return $this->hasOne(IndonesiaProvinsi::class, ['id' => 'ktp_provinsi_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiKtpKabupaten()
    {
        return $this->hasOne(IndonesiaKabupatenKota::class, ['id' => 'ktp_kabupaten_kota_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiKtpKecamatan()
    {
        return $this->hasOne(IndonesiaKecamatan::class, ['id' => 'ktp_kecamatan_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiKtpDesa()
    {
        return $this->hasOne(IndonesiaDesa::class, ['id' => 'ktp_desa_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiDomisiliProvinsi()
    {
        return $this->hasOne(IndonesiaProvinsi::class, ['id' => 'domisili_provinsi_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiDomisiliKabupaten()
    {
        return $this->hasOne(IndonesiaKabupatenKota::class, ['id' => 'domisili_kabupaten_kota_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiDomisiliKecamatan()
    {
        return $this->hasOne(IndonesiaKecamatan::class, ['id' => 'domisili_kecamatan_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPegawaiDomisiliDesa()
    {
        return $this->hasOne(IndonesiaDesa::class, ['id' => 'domisili_desa_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getSuratPeringatans()
    {
        return $this->hasMany(SuratPeringatan::class, ['pegawai_id' => 'id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(Training::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMasterDataDefaults()
    {
        return $this->hasOne(MasterDataDefault::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPGajiPokoks()
    {
        return $this->hasMany(PGajiPokok::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPInsentifTetaps()
    {
        return $this->hasMany(PInsentifTetap::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPPremiHadirs()
    {
        return $this->hasMany(PPremiHadir::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPSewaMobils()
    {
        return $this->hasMany(PSewaMobil::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPSewaMotors()
    {
        return $this->hasMany(PSewaMotor::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPSewaRumahs()
    {
        return $this->hasMany(PSewaRumah::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjaganLainTetaps()
    {
        return $this->hasMany(PTunjaganLainTetap::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganJabatans()
    {
        return $this->hasMany(PTunjanganJabatan::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganKosts()
    {
        return $this->hasMany(PTunjanganKost::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPTunjanganPulsas()
    {
        return $this->hasMany(PTunjanganPulsa::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPUangMakans()
    {
        return $this->hasMany(PUangMakan::class, ['pegawai_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPUangTransports()
    {
        return $this->hasMany(PUangTransport::class, ['pegawai_id' => 'id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getEGudangPegawais()
    {
        return $this->hasMany(EGudangPegawai::class, ['pegawai_id' => 'id']);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

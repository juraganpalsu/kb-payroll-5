<?php

namespace common\models\base;

use mootensai\relation\RelationTrait;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "time_table".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $shift
 * @property string $untuk_hari
 * @property string $masuk_minimum
 * @property string $masuk
 * @property integer $toleransi_masuk
 * @property string $masuk_maksimum
 * @property string $istirahat_minimum
 * @property string $istirahat
 * @property string $istirahat_maksimum
 * @property string $masuk_istirahat_minimum
 * @property string $masuk_istirahat
 * @property string $masuk_istirahat_maksimum
 * @property string $pulang_minimum
 * @property string $pulang
 * @property integer $toleransi_pulang
 * @property string $pulang_maksimum
 * @property integer $sistem_kerja
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 */
class TimeTable extends ActiveRecord
{

    use RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'untuk_hari', 'masuk_minimum', 'masuk', 'masuk_maksimum', 'pulang_minimum', 'pulang', 'pulang_maksimum'], 'required'],
            [['masuk_minimum', 'masuk', 'masuk_maksimum', 'istirahat_minimum', 'istirahat', 'istirahat_maksimum', 'masuk_istirahat_minimum', 'masuk_istirahat', 'masuk_istirahat_maksimum', 'pulang_minimum', 'pulang', 'pulang_maksimum', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['shift', 'sistem_kerja', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['nama', 'untuk_hari', 'toleransi_masuk', 'toleransi_pulang'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'time_table';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'shift' => Yii::t('app', 'Shift'),
            'untuk_hari' => Yii::t('app', 'Untuk Hari'),
            'masuk_minimum' => Yii::t('app', 'Masuk Minimum'),
            'masuk' => Yii::t('app', 'Masuk'),
            'toleransi_masuk' => Yii::t('app', 'Toleransi Masuk'),
            'masuk_maksimum' => Yii::t('app', 'Masuk Maksimum'),
            'istirahat_minimum' => Yii::t('app', 'Istirahat Minimum'),
            'istirahat' => Yii::t('app', 'Istirahat'),
            'istirahat_maksimum' => Yii::t('app', 'Istirahat Maksimum'),
            'masuk_istirahat_minimum' => Yii::t('app', 'Masuk Istirahat Minimum'),
            'masuk_istirahat' => Yii::t('app', 'Masuk Istirahat'),
            'masuk_istirahat_maksimum' => Yii::t('app', 'Masuk Istirahat Maksimum'),
            'pulang_minimum' => Yii::t('app', 'Pulang Minimum'),
            'pulang' => Yii::t('app', 'Pulang'),
            'toleransi_pulang' => Yii::t('app', 'Toleransi Pulang'),
            'pulang_maksimum' => Yii::t('app', 'Pulang Maksimum'),
            'sistem_kerja' => Yii::t('app', 'Sistem Kerja'),
            'lock' => Yii::t('app', 'Lock'),
        ];
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where(['time_table.deleted_by' => null])->orWhere(['time_table.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

<?php

namespace common\models\base;

use common\components\CreatedByBehavior;
use frontend\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "cost_center_template".
 *
 * @property string $id
 * @property integer $kode
 * @property string $area_kerja
 * @property string $keterangan
 * @property integer $is_aktif
 * @property string $created_at
 * @property integer $created_by
 * @property string $created_by_pk
 * @property integer $created_by_struktur
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 */
class CostCenterTemplate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['kode', 'created_by', 'created_by_struktur', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id', 'created_by_pk'], 'string', 'max' => 32],
            [['area_kerja'], 'string', 'max' => 45],
            [['lock', 'is_aktif'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cost_center_template';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'kode' => Yii::t('frontend', 'Kode'),
            'area_kerja' => Yii::t('frontend', 'Area Kerja'),
            'keterangan' => Yii::t('frontend', 'Keterangan'),
            'is_aktif' => Yii::t('frontend', 'Status Aktif'),
            'created_by_pk' => Yii::t('frontend', 'Created By Pk'),
            'created_by_struktur' => Yii::t('frontend', 'Created By Struktur'),
            'lock' => Yii::t('frontend', 'Lock'),
        ];
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where([self::tableName() . '.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::class,
                'column' => 'id',
            ],
            'createdby' => [
                'class' => CreatedByBehavior::class,
                'values' => [
                    'byPegawai' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            return $userPegawai->pegawai->perjanjianKerja->id;
                        }
                        return false;
                    },
                    'byStruktur' => function () {
                        /** @var User $user */
                        $user = Yii::$app->user->identity;
                        if ($userPegawai = $user->userPegawai) {
                            if ($pegawaiStruktur = $userPegawai->pegawai->pegawaiStruktur) {
                                return $pegawaiStruktur->struktur_id;
                            }
                        }
                        return false;
                    },
                ],
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

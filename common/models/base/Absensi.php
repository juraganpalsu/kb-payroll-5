<?php

namespace common\models\base;

use frontend\models\Pegawai;
use frontend\models\TimeTable;
use frontend\modules\pegawai\models\CostCenter;
use frontend\modules\pegawai\models\PegawaiGolongan;
use frontend\modules\pegawai\models\PegawaiStruktur;
use frontend\modules\pegawai\models\PerjanjianKerja;
use mootensai\relation\RelationTrait;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the base model class for table "absensi".
 *
 * @property integer $id
 * @property string $masuk
 * @property string $istirahat
 * @property string $masuk_istirahat
 * @property string $pulang
 * @property string $tanggal
 * @property string $pegawai_id
 * @property string $perjanjian_kerja_id
 * @property string $pegawai_struktur_id
 * @property integer $pegawai_sistem_kerja_id
 * @property string $cost_center_id
 * @property string $pegawai_golongan_id
 * @property integer $time_table_id
 * @property integer $status_kehadiran
 * @property integer $spl_id
 * @property integer $spl_template_id
 * @property integer $telat
 * @property float $jumlah_jam_lembur
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property Pegawai $pegawai
 * @property PerjanjianKerja $perjanjianKerja
 * @property PegawaiStruktur $pegawaiStruktur
 * @property PegawaiGolongan $pegawaiGolongan
 * @property CostCenter $costCenter
 * @property TimeTable $timeTable
 * @property Spl $splRel
 * @property \common\models\SplTemplate $splTemplate
 * @property \common\models\PegawaiSistemKerja $pegawaiSistemKerja
 */
class Absensi extends ActiveRecord
{

    use RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['masuk', 'istirahat', 'masuk_istirahat', 'pulang', 'tanggal', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['tanggal', 'pegawai_id'], 'required'],
            [['time_table_id', 'spl_template_id', 'status_kehadiran', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['pegawai_id', 'perjanjian_kerja_id', 'pegawai_struktur_id'], 'string', 'max' => 32],
            [['jumlah_jam_lembur'], 'string'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'absensi';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock()
    {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'bisnis_unit' => Yii::t('frontend', 'Bisnis Unit'),
            'area_kerja' => Yii::t('frontend', 'Area Kerja'),
            'departemen' => Yii::t('frontend', 'Departemen'),
            'struktur' => Yii::t('frontend', 'Struktur'),
            'masuk' => Yii::t('app', 'Masuk'),
            'istirahat' => Yii::t('app', 'Istirahat'),
            'masuk_istirahat' => Yii::t('app', 'Masuk Istirahat'),
            'pulang' => Yii::t('app', 'Pulang'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'pegawai_id' => Yii::t('app', 'Pegawai'),
            'perjanjian_kerja_id' => Yii::t('frontend', 'Perjanjian Kerja ID'),
            'pegawai_struktur_id' => Yii::t('frontend', 'Pegawai Struktur ID'),
            'pegawai_sistem_kerja_id' => Yii::t('frontend', 'Sistem Kerja ID'),
            'pegawai_golongan_id' => Yii::t('frontend', 'Golongan ID'),
            'spl_id' => Yii::t('frontend', 'Spl Id'),
            'spl_template_id' => Yii::t('frontend', 'Spl Template ID'),
            'cost_center_id' => Yii::t('frontend', 'Const Center ID'),
            'time_table_id' => Yii::t('app', 'Time Table'),
            'status_kehadiran' => Yii::t('app', 'Status Kehadiran'),
            'jumlah_jam_lembur' => Yii::t('app', 'Jumlah Jam Lembur'),
            'telat' => Yii::t('app', 'Telat'),
            'lock' => Yii::t('app', 'Lock'),
        ];
    }

    /**
     * @return PerjanjianKerja
     */
    public static function modelPerjanjianKerja()
    {
        return new PerjanjianKerja();
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::class, ['id' => 'pegawai_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getPerjanjianKerja()
    {
        return $this->hasOne(PerjanjianKerja::class, ['id' => 'perjanjian_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiStruktur()
    {
        return $this->hasOne(PegawaiStruktur::class, ['id' => 'pegawai_struktur_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiSistemKerja()
    {
        return $this->hasOne(\common\models\PegawaiSistemKerja::class, ['id' => 'pegawai_sistem_kerja_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPegawaiGolongan()
    {
        return $this->hasOne(PegawaiGolongan::class, ['id' => 'pegawai_golongan_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCostCenter()
    {
        return $this->hasOne(CostCenter::class, ['id' => 'cost_center_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getTimeTable()
    {
        return $this->hasOne(TimeTable::class, ['id' => 'time_table_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplRel()
    {
        return $this->hasOne(Spl::class, ['id' => 'spl_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSplTemplate()
    {
        return $this->hasOne(\common\models\SplTemplate::class, ['id' => 'spl_template_id']);
    }

    /**
     *
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()->where(['absensi.deleted_by' => null])->orWhere(['absensi.deleted_by' => 0]);
    }

    /**
     *
     * @inheritdoc
     * @return bool
     */
    public function beforeSoftDelete()
    {
        $this->deleted_at = date('Y-m-d H:i:s');
        return true;
    }


    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_by' => function ($model) {
                        return Yii::$app->user->id;
                    }
                ],
                'replaceRegularDelete' => true
            ],
        ];
    }
}

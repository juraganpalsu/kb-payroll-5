<?php


namespace console\controllers;


use console\models\ProsesGaji;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: ProsesGajiController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 06/07/20
 * Time: 22.34
 */
class ProsesGajiController extends Controller
{

    public function actionSimpanGaji()
    {
        $model = new ProsesGaji();
        $model->simpanGaji();
        return ExitCode::OK;
    }

}
<?php


namespace console\controllers;


use console\models\CutiConf;
use console\models\CutiSaldo;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: CutiController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 13/01/20
 * Time: 23.34
 */
class CutiController extends Controller
{

    /**
     * Buat saldo cuti pertama
     */
    public function actionSetSaldoAwal()
    {
        $model = new CutiSaldo();
        $model->setSaldoAwal();
        ExitCode::OK;
    }

    /**
     * Pembaruan saldo cuti
     */
    public function actionPembaruanSaldo()
    {
        $model = new CutiSaldo();
        $model->setPembaruanSaldo();
        ExitCode::OK;
    }

    /**
     * Generate awal sistem
     */
    public function actionGenerateSaldo()
    {
        $model = new CutiSaldo();
        $model->generateSaldo();
        ExitCode::OK;
    }

    /**
     * Menambah raw pada cuti conf
     * Dilakukan satahun sekali susai dengan cron job
     */
    public function actionCreateCutiConf()
    {
        $model = new CutiConf();
        $model->createCutiConf();
        ExitCode::OK;
    }
}
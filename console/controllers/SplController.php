<?php


namespace console\controllers;


use console\models\Spl;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: SplController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 12/02/20
 * Time: 22.22
 */
class SplController extends Controller
{
    /**
     * @return Spl
     */
    public static function model()
    {
        return new Spl();
    }

    /**
     *
     */
    public function actionApproval()
    {
        $model = self::model();
        $model->autoApproval();
        ExitCode::OK;

    }

    /**
     * Mengirin notirikasi SPL ke email
     */
    public function actionKirimPemberitahuan()
    {
        $model = self::model();
        $model->kirimNotifikasi();
        ExitCode::OK;
    }


    /**
     * Testing untuk kirim email
     */
    public function actionTestSendMail()
    {
        $model = self::model();
        $model->testSendMail();
    }

}
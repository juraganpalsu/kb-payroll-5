<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 27/02/2019
 * Time: 22:03
 */

namespace console\controllers;


use common\models\Absensi;
use console\models\AntrianLaporanAbsensi;
use console\models\AntrianProsesAbsensi;
use DateInterval;
use DateTime;
use Exception;
use Throwable;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\StaleObjectException;

class AbsensiController extends Controller
{

    /**
     * Proses data absensi full dua hari, semua pegawai
     */
    public function actionProses()
    {
        $model = new Absensi();
        try {
            $date = new DateTime();
            $today = $date->format('Y-m-d');
            $date->add(DateInterval::createFromDateString('yesterday'));
            $yesterday = $date->format('Y-m-d');
            Yii::info('Memulai proses data absensi tanggal ' . $yesterday, 'process-attendance');
            $model->prosesData($yesterday, $today);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
            Yii::info($e->getMessage(), 'process-attendance');
        }
        ExitCode::OK;
    }


    /**
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionProsesPerGolongan()
    {
        $model = new AntrianProsesAbsensi();
        $model->prosesDataPerGolongan();
        ExitCode::OK;
    }

    /**
     */
    public function actionGenerateLaporan()
    {
        $model = new AntrianLaporanAbsensi();
        $model->generateLaporan();
        ExitCode::OK;
    }

    /**
     * Delete semua data dan semua file
     * Ini dilakukan pada tengah malam, jadi tidak akan ada sampah antrian dan files
     */
    public function actionDeleteQueueLaporan()
    {
        try {
            AntrianLaporanAbsensi::deleteAll('id != ""');
            $files = glob(Yii::getAlias('@frontend') . AntrianLaporanAbsensi::filePathLaporan . '*');
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage());
        }
        ExitCode::OK;
    }

    /**
     * Mengkonversi data wsc kedalam izin WSC
     */
    public function actionConvertFromWsc()
    {
        $model = new \console\models\Absensi();
        $model->convertFromWsc();
    }

}
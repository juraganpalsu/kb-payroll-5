<?php

namespace console\controllers;

use console\models\LiburPegawai;
use Exception;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 19/11/2018
 * Time: 17:32
 */
class LiburPegawaiController extends Controller
{

    public function actionGenerateLibur()
    {
        $model = new LiburPegawai();
        Yii::info('Generating libur pegawai ....', 'generate-libur');
        try {
            $model->generateLibur();
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
            ExitCode::UNSPECIFIED_ERROR;
        }
        ExitCode::OK;
    }
}
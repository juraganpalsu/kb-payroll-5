<?php


namespace console\controllers;


use console\models\DsAbsensi;
use console\models\Pegawai;
use console\models\PegawaiPtkp;
use PhpOffice\PhpSpreadsheet\Exception;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class PegawaiController extends Controller
{
    /**
     * @param int $limit
     * upload data attendance to https://serviceattendance.hc-kbu.com
     */
    public function actionUploadToAttendance(int $limit = 50)
    {
        $model = new Pegawai();
        $model->uploadToAttendance($limit);
        ExitCode::OK;
    }

    /**
     * Membuat User
     */
    public function actionCreateUser()
    {
        $model = new Pegawai();
        $model->createUser();
        ExitCode::OK;
    }

    /**
     * @throws Exception
     */
    public function actionGenerateDataHabisKontrak()
    {
        $model = new Pegawai();
        $model->generateDataHabisKontrak();
        ExitCode::OK;
    }

    /**
     *
     */
    public function actionKirimNotifikasiKeHr()
    {
        $model = new Pegawai();
        $model->kirimNotifikasiKeHr();
        ExitCode::OK;
    }

    /**
     *
     */
    public function actionSetResign()
    {
        $model = new Pegawai();
        $model->getPegawaiResignBerdasarTanggal();
        ExitCode::OK;
    }

    /**
     *
     */
    public function actionTes()
    {
//        $model = new PegawaiSearch();
//        $model->searchPegawaiAktifTanpaMasterData();
//        $p = ProsesGaji::findOne('bc7b3c04f80111ea89c2067f72d3402c');
//        $password = date('ymd', strtotime($p->payrollPeriode->tanggal_awal)) . date('d', strtotime($p->payrollPeriode->tanggal_awal)).'3';
//        print_r($password);
//        $p->createFileSlipGaji(true);
//        $p->buatFileRekap();

//        $qu = \frontend\modules\payroll\models\ProsesGaji::find()->asArray()->all();
//        foreach ($qu as $prosesGaji) {
//            $b = \console\models\ProsesGaji::findOne($prosesGaji['id']);
//            $b->buatFileRekap();
////            $b->kirimFileRekap();
//        }


        $ds = new DsAbsensi();
        print_r($ds->getIdPegawaiAktif());


        //tes email

//        $this->sendEmailtest();

        ExitCode::OK;
    }


    public function sendEmailtest()
    {
        try {
            $mail = Yii::$app->mailer_gmail;
            $compose = $mail->compose()
                ->setFrom('hc.system@kobe.co.id');
            $title = Yii::t('frontend', 'Berikut daftar SPKL yg disubmit pada hari ini.');
            $compose->setTo(['teti.roheti@kobe.co.id'])
                ->setSubject($title);
            $compose->send();
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * Generate Ptkp
     * @param bool $isRegenerate
     */
    public function actionGeneratePtkp(bool $isRegenerate = false)
    {
        $model = new PegawaiPtkp();
        $model->generatePtkp($isRegenerate);
    }

}
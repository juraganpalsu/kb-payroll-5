<?php


namespace console\controllers;


use console\models\DsAkhirKontrak;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: DsAkhirKontrakController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 12/12/19
 * Time: 21.44
 */
class DsAkhirKontrakController extends Controller
{

    public function actionRekapAkhirKontrak()
    {
        $model = new DsAkhirKontrak();
        $model->rekapAkhirKontrak();
        return ExitCode::OK;
    }

}
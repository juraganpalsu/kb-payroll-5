<?php


namespace console\controllers;


use console\models\DsAbsensi;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: DsPertanggalController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 23/10/20
 * Time: 05.55
 */
class DsPertanggalController extends Controller
{

    /**
     *
     */
    public function actionCreateDsAbsensi()
    {
        $model = new DsAbsensi();
        $model->simpanData();
        ExitCode::OK;
    }

}
<?php


namespace console\controllers;


use DateTime;
use frontend\modules\dashboard\models\DsTurnOver;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: DsTurnOverController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 07/12/20
 * Time: 04.13
 */
class DsTurnOverController extends Controller
{

    public function actionRekapData(): void
    {
        $dateTimeObj = new DateTime();
        $model = new DsTurnOver();
        $model->simpanData($dateTimeObj);
        ExitCode::OK;
    }

}
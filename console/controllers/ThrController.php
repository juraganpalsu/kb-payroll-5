<?php


namespace console\controllers;


use console\models\Thr;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: ThrController.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/5/2021
 * Time: 11:00 PM
 */
class ThrController extends Controller
{

    public function actionSimpanDetail()
    {
        $model = new Thr();
        $model->createDetail();
        ExitCode::OK;
    }

}
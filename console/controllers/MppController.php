<?php


namespace console\controllers;


use frontend\modules\personalia\models\MppAct;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: MppController.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 22/12/20
 * Time: 21.06
 */
class MppController extends Controller
{


    public function actionSimpanMpp()
    {
        $model = new MppAct();
        $model->saveMpp();
        ExitCode::OK;
    }

}
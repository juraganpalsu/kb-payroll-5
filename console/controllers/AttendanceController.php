<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 01/02/2019
 * Time: 18:49
 */

namespace console\controllers;


use console\models\Attendance;
use Exception;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class AttendanceController extends Controller
{
    /**
     * Download data attendance from ADMS
     * @param int $limit
     * @param int $status
     */
    public function actionDownload(int $limit = 50, int $status = Attendance::UNDOWNLOADED)
    {
        $model = new Attendance();
        Yii::info('Starting process download data attendance ....', 'download-attendance');
        try {
            $downloadAttendance = $model->downloadFromAdms($limit, $status);
            if (isset($downloadAttendance['status']) && $downloadAttendance['status']) {
//                print_r($downloadAttendance);
                $model->saveAttendances($downloadAttendance['data']);
                Yii::info('Processing data attendance is done!', 'download-attendance');
            } else {
                Yii::info('No new data found!', 'download-attendance');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
            ExitCode::UNSPECIFIED_ERROR;
        }
        ExitCode::OK;
    }

    /**
     * @param int $limit
     * upload data attendance to https://serviceattendance.hc-kbu.com
     */
    public function actionUploadToAttendance(int $limit = 50)
    {
        $model = new Attendance();
        $model->uploadToAttendance($limit);
        ExitCode::OK;
    }

    /**
     * Download from beat route
     */
    public function actionDownloadFromBeatroute()
    {
        $model = new Attendance();
        Yii::info('Starting process download data attendance ....', 'download-attendance-beatroute');
        try {
            $month = date('m');
            $year = date('Y');
            $model->downloadFromBeatRoute($month, $year);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'download-attendance-beatroute');
            Yii::info($e->getMessage(), 'exception');
            ExitCode::UNSPECIFIED_ERROR;
        }
        ExitCode::OK;
    }


    /**
     * Mengambil data dari SFA
     */
    public function actionGetDataSfa(int $min = 1)
    {
        $model = new Attendance();
        Yii::info('Starting process download data attendance ....', 'download-attendance-beatroute');
        try {
            $date = date('Y-m-d', strtotime("-$min day"));
            $model->getDataSfa($date);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'download-attendance-beatroute');
            Yii::info($e->getMessage(), 'exception');
        }
    }


}
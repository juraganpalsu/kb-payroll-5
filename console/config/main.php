<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
        ],
    ],
    'components' => [
        'urlManager' => [
            'scriptUrl' => '',
            'baseUrl' => 'https://hris.hc-kbu.com:8082/',
            'hostInfo' => 'hris.hc-kbu.com:8082',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['generate-libur'],
                    'logFile' => '@console/runtime/logs/generate-libur.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['exception'],
                    'logFile' => '@console/runtime/logs/exception.log',
                    'logVars' => ['_SERVER'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['download-attendance'],
                    'logFile' => '@console/runtime/logs/download-attendance.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['not-employee'],
                    'logFile' => '@console/runtime/logs/not-employee.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['process-attendance'],
                    'logFile' => '@console/runtime/logs/process-attendance.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['upload-attendance'],
                    'logFile' => '@console/runtime/logs/upload-attendance.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['upload-pegawai'],
                    'logFile' => '@console/runtime/logs/upload-pegawai.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['download-attendance-beatroute'],
                    'logFile' => '@console/runtime/logs/download-attendance-beatroute.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['create-user'],
                    'logFile' => '@console/runtime/logs/create-user.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['ds-akhir-kontrak'],
                    'logFile' => '@console/runtime/logs/ds-akhir-kontrak.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['saldo-cuti'],
                    'logFile' => '@console/runtime/logs/saldo-cuti.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['auto-spl-approval'],
                    'logFile' => '@console/runtime/logs/auto-spl-approval.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['update-komponen-pegawai'],
                    'logFile' => '@console/runtime/logs/update-komponen-pegawai.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['proses-gaji'],
                    'logFile' => '@console/runtime/logs/proses-gaji.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['generate-spl-pdf'],
                    'logFile' => '@console/runtime/logs/generate-spl-pdf.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['generate-ds-turn-over'],
                    'logFile' => '@console/runtime/logs/generate-ds-turn-over.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['download-attendance-sfa'],
                    'logFile' => '@console/runtime/logs/download-attendance-sfa.log',
                    'logVars' => [],
                ],
            ],
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableSession' => false,
            'enableAutoLogin' => false,
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'User' => 'frontend\models\User',
            ],
            'admins' => ['jiwandaru', 'bintang.famy', 'bobi.arip']
        ],
    ],
    'params' => $params,
];

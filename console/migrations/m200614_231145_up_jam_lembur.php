<?php

use yii\db\Migration;

/**
 * Class m200614_231145_up_jam_lembur
 */
class m200614_231145_up_jam_lembur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `gaji_bulanan` 
CHANGE COLUMN `jam_lembur` `jam_lembur` FLOAT(10,2) NOT NULL DEFAULT '0.00' ;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200614_231145_up_jam_lembur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200614_231145_up_jam_lembur cannot be reverted.\n";

        return false;
    }
    */
}
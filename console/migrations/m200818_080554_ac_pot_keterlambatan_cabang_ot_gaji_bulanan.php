<?php

use yii\db\Migration;

/**
 * Class m200818_080554_ac_pot_keterlambatan_cabang_ot_gaji_bulanan
 */
class m200818_080554_ac_pot_keterlambatan_cabang_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'cabang', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('tipe_gaji_pokok'));
        $this->addColumn('gaji_bulanan', 'pot_keterlambatan', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('tipe_gaji_pokok'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200818_080554_ac_pot_keterlambatan_cabang_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200818_080554_ac_pot_keterlambatan_cabang_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

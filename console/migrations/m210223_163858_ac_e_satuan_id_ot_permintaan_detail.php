<?php

use yii\db\Migration;

/**
 * Class m210223_163858_ac_e_satuan_id_ot_permintaan_detail
 */
class m210223_163858_ac_e_satuan_id_ot_permintaan_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey( 'fk_e_permintaan_e_satuan1', 'e_permintaan');
        $this->dropColumn('e_permintaan', 'e_satuan_id');
        $this->addColumn('e_permintaan_detail', 'e_satuan_id', 'VARCHAR(36) NOT NULL DEFAULT 0');
        $this->addForeignKey('satuan', 'e_permintaan_detail', 'e_satuan_id', 'e_satuan', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210223_163858_ac_e_satuan_id_ot_permintaan_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210223_163858_ac_e_satuan_id_ot_permintaan_detail cannot be reverted.\n";

        return false;
    }
    */
}

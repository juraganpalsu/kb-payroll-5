<?php

use yii\db\Migration;

/**
 * Class m191007_131301_ct_wilayah_indonesia
 */
class m191007_131301_ct_wilayah_indonesia extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `indonesia_desa` 
CHANGE COLUMN `kecamatan_id` `kecamatan_id` INT(11) NOT NULL AFTER `id`;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191007_131301_ct_wilayah_indonesia cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191007_131301_ct_wilayah_indonesia cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200510_075403_ct_thr
 */
class m200510_075403_ct_thr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `thr_setting` (
  `id` VARCHAR(32) NOT NULL,
  `cut_off_thr` DATE NOT NULL,
  `cut_off_resign` DATE NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `thr` (
  `id` VARCHAR(32) NOT NULL,
  `catatan` TEXT NULL,
  `thr_setting_id` VARCHAR(32) NOT NULL,
  `golongan_id` INT NOT NULL,
  `bisnis_unit` INT(2) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_thr_golongan1_idx` (`golongan_id` ASC),
  INDEX `fk_thr_thr_setting1_idx` (`thr_setting_id` ASC),
  CONSTRAINT `fk_thr_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_thr_setting1`
    FOREIGN KEY (`thr_setting_id`)
    REFERENCES `thr_setting` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `thr_detail` (
  `id` VARCHAR(32) NOT NULL,
  `nominal` INT(11) NOT NULL DEFAULT 0,
  `catatan` TEXT NULL,
  `thr_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `p_gaji_pokok_id` VARCHAR(32) NOT NULL,
  `p_gaji_pokok_nominal` VARCHAR(45) NOT NULL DEFAULT 0,
  `p_tunjangan_loyalitas_config_id` VARCHAR(32) NOT NULL,
  `p_tunjangan_loyalitas_config_nominal` VARCHAR(45) NOT NULL DEFAULT 0,
  `p_tunjangan_anak_config_id` VARCHAR(32) NOT NULL,
  `p_tunjangan_anak_config_nominal` VARCHAR(45) NOT NULL DEFAULT 0,
  `p_tunjangan_lain_tetap_id` VARCHAR(32) NOT NULL,
  `p_tunjangan_lain_tetap_nominal` VARCHAR(45) NOT NULL DEFAULT 0,
  `p_tunjangan_jabatan_id` VARCHAR(32) NOT NULL,
  `p_tunjangan_jabatan_nominal` VARCHAR(45) NOT NULL DEFAULT 0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_thr_detail_thr1_idx` (`thr_id` ASC),
  INDEX `fk_thr_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_thr_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_thr_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_thr_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_thr_detail_p_gaji_pokok1_idx` (`p_gaji_pokok_id` ASC),
  INDEX `fk_thr_detail_p_tunjangan_loyalitas_config1_idx` (`p_tunjangan_loyalitas_config_id` ASC),
  INDEX `fk_thr_detail_p_tunjangan_anak_config1_idx` (`p_tunjangan_anak_config_id` ASC),
  INDEX `fk_thr_detail_p_tunjagan_lain_tetap1_idx` (`p_tunjangan_lain_tetap_id` ASC),
  INDEX `fk_thr_detail_p_tunjangan_jabatan1_idx` (`p_tunjangan_jabatan_id` ASC),
  CONSTRAINT `fk_thr_detail_thr1`
    FOREIGN KEY (`thr_id`)
    REFERENCES `thr` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_p_gaji_pokok1`
    FOREIGN KEY (`p_gaji_pokok_id`)
    REFERENCES `p_gaji_pokok` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_p_tunjangan_loyalitas_config1`
    FOREIGN KEY (`p_tunjangan_loyalitas_config_id`)
    REFERENCES `p_tunjangan_loyalitas_config` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_p_tunjangan_anak_config1`
    FOREIGN KEY (`p_tunjangan_anak_config_id`)
    REFERENCES `p_tunjangan_anak_config` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_p_tunjagan_lain_tetap1`
    FOREIGN KEY (`p_tunjangan_lain_tetap_id`)
    REFERENCES `p_tunjagan_lain_tetap` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_thr_detail_p_tunjangan_jabatan1`
    FOREIGN KEY (`p_tunjangan_jabatan_id`)
    REFERENCES `p_tunjangan_jabatan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200510_075403_ct_thr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200510_075403_ct_thr cannot be reverted.\n";

        return false;
    }
    */
}

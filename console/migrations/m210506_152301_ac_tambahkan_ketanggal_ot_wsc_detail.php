<?php

use yii\db\Migration;

/**
 * Class m210506_152301_ac_tambahkan_ketanggal_ot_wsc_detail
 */
class m210506_152301_ac_tambahkan_ketanggal_ot_wsc_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('wsc_detail', 'tambahkan_ketanggal', $this->date()->null()->after('kategori'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210506_152301_ac_tambahkan_ketanggal_ot_wsc_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210506_152301_ac_tambahkan_ketanggal_ot_wsc_detail cannot be reverted.\n";

        return false;
    }
    */
}

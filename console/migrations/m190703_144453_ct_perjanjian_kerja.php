<?php

use yii\db\Migration;

/**
 * Class m190703_144453_ct_perjanjian_kerja
 */
class m190703_144453_ct_perjanjian_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL


-- -----------------------------------------------------
-- Table `jabatan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jabatan` (
  `id` VARCHAR(32) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `kode` VARCHAR(45) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_org_chart` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pegawai_struktur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pegawai_struktur` (
  `id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `struktur_id` INT NOT NULL,
  `mulai_berlaku` DATE NOT NULL,
  `akhir_berlaku` DATE NULL,
  `status` INT(2) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_org_chart` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_org_chart_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_pegawai_struktur_struktur1_idx` (`struktur_id` ASC),
  CONSTRAINT `fk_pegawai_org_chart_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_struktur_struktur1`
    FOREIGN KEY (`struktur_id`)
    REFERENCES `struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `perjanjian_kerja`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `perjanjian_kerja` (
  `id` VARCHAR(32) NOT NULL,
  `mulai_berlaku` DATE NOT NULL,
  `akhir_berlaku` DATE NULL,
  `status` INT(2) NOT NULL COMMENT '1.Magang\n2.Kontrak\n3.Tetap',
  `pegawai_id` VARCHAR(32) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_org_chart` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_perjanjian_kerja_pegawai2_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_perjanjian_kerja_pegawai2`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pk_poc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pk_poc` (
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  INDEX `fk_pk_poc_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_pk_poc_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_pk_poc_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pk_poc_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user_pegawai`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_pegawai` (
  `user_id` INT(11) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  INDEX `fk_user_pegawai_user1_idx` (`user_id` ASC),
  INDEX `fk_user_pegawai_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_user_pegawai_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_pegawai_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190703_144453_ct_perjanjian_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190703_144453_ct_perjanjian_kerja cannot be reverted.\n";

        return false;
    }
    */
}

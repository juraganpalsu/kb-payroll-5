<?php

use yii\db\Migration;

/**
 * Class m210709_081923_ac_gol_darah_ot_pegawai
 */
class m210709_081923_ac_gol_darah_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai', 'gol_darah', $this->integer(1)->notNull()->defaultValue(0)->after('jenis_kelamin'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210709_081923_ac_gol_darah_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210709_081923_ac_gol_darah_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

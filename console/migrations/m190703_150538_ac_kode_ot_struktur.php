<?php

use yii\db\Migration;

/**
 * Class m190703_150538_ac_kode_ot_struktur
 */
class m190703_150538_ac_kode_ot_struktur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('struktur', 'kode', $this->char('10')->defaultValue(0)->notNull()->after('child_allowed'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190703_150538_ac_kode_ot_struktur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190703_150538_ac_kode_ot_struktur cannot be reverted.\n";

        return false;
    }
    */
}

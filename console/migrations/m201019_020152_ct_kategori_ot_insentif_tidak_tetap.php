<?php

use yii\db\Migration;

/**
 * Class m201019_020152_ct_kategori_ot_insentif_tidak_tetap
 */
class m201019_020152_ct_kategori_ot_insentif_tidak_tetap extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('insentif_tidak_tetap', 'kategori', $this->tinyInteger(1)->notNull()->defaultValue(1)->after('id'));
        $this->update('insentif_tidak_tetap', ['kategori' => 1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201019_020152_ct_kategori_ot_insentif_tidak_tetap cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201019_020152_ct_kategori_ot_insentif_tidak_tetap cannot be reverted.\n";

        return false;
    }
    */
}

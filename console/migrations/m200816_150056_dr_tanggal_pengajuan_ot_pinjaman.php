<?php

use yii\db\Migration;

/**
 * Class m200816_150056_dr_tanggal_pengajuan_ot_pinjaman
 */
class m200816_150056_dr_tanggal_pengajuan_ot_pinjaman extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('pinjaman', 'tanggal_pengajuan');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200816_150056_dr_tanggal_pengajuan_ot_pinjaman cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200816_150056_dr_tanggal_pengajuan_ot_pinjaman cannot be reverted.\n";

        return false;
    }
    */
}

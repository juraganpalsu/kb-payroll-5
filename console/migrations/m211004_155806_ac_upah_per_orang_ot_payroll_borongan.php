<?php

use yii\db\Migration;

/**
 * Class m211004_155806_ac_upah_per_orang_ot_payroll_borongan
 */
class m211004_155806_ac_upah_per_orang_ot_payroll_borongan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payroll_borongan', 'upah_per_orang', $this->float(2)->notNull()->defaultValue(0)->after('hasil'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211004_155806_ac_upah_per_orang_ot_payroll_borongan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211004_155806_ac_upah_per_orang_ot_payroll_borongan cannot be reverted.\n";

        return false;
    }
    */
}

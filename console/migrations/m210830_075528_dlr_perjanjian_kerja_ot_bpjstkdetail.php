<?php

use yii\db\Migration;

/**
 * Class m210830_075528_dlr_perjanjian_kerja_ot_bpjstkdetail
 */
class m210830_075528_dlr_perjanjian_kerja_ot_bpjstkdetail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
ALTER TABLE `payroll_bpjs_tk_detail` 
DROP FOREIGN KEY `fk_payroll_bpjs_tk_detail_perjanjian_kerja1`,
DROP FOREIGN KEY `fk_payroll_bpjs_tk_detail_pegawai_struktur1`,
DROP FOREIGN KEY `fk_payroll_bpjs_tk_detail_pegawai_golongan1`;
ALTER TABLE `payroll_bpjs_tk_detail` 
DROP INDEX `fk_payroll_bpjs_tk_detail_pegawai_struktur1_idx` ,
DROP INDEX `fk_payroll_bpjs_tk_detail_pegawai_golongan1_idx` ,
DROP INDEX `fk_payroll_bpjs_tk_detail_perjanjian_kerja1_idx` ;
SQL;

        $this->execute($sql);
        $sql =<<<SQL
ALTER TABLE `payroll_bpjs_detail` 
DROP FOREIGN KEY `fk_bpjs_detail_perjanjian_kerja1`,
DROP FOREIGN KEY `fk_bpjs_detail_pegawai_golongan1`;
ALTER TABLE `payroll_bpjs_detail` 
DROP INDEX `fk_bpjs_detail_pegawai_golongan1_idx` ,
DROP INDEX `fk_bpjs_detail_perjanjian_kerja1_idx` ;
SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210830_075528_dlr_perjanjian_kerja_ot_bpjstkdetail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210830_075528_dlr_perjanjian_kerja_ot_bpjstkdetail cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m201012_043528_ct_surat_pegawai
 */
class m201012_043528_ct_surat_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `surat_pegawai` (
  `id` VARCHAR(32) NOT NULL,
  `jenis` INT(2) NULL DEFAULT '0',
  `tgl_mulai` DATE NULL,
  `tgl_akhir` DATE NULL,
  `tujuan_surat` VARCHAR(225) NULL,
  `struktur_id` INT NULL DEFAULT 0 COMMENT 'Jenis Surat\n1 =	PERJANJIAN KERJA WAKTU TERTENTU\r\n2 =	SURAT KEPUTUSAN - ACTING PROMOSI\r\n3 =	ADDENDUM\r\n4 =	SURAT KEPUTUSAN - PENGANGKATAN\r\n5 =	SURAT KEPUTUSAN - PROMOSI JABATAN\r\n6 =	SURAT KETERANGAN - DEMOSI\r\n7 =	SURAT KETERANGAN - MUTASI\r\n8 =	SURAT KETERAN' /* comment truncated */ /*GAN - Aktif*/,
  `golongan_id` INT NULL DEFAULT 0,
  `jenis_perjanjian` INT(2) NULL DEFAULT 0 COMMENT 'Jenis Kontrak',
  `kontrak` INT(2) NULL DEFAULT 0 COMMENT '1.KBU\n2.ADA\n\nBusines Unit',
  `catatan` TEXT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `oleh_pegawai_id` VARCHAR(32) NOT NULL DEFAULT '0',
  `oleh_perjanjian_kerja_id` VARCHAR(32) NOT NULL DEFAULT '0',
  `oleh_pegawai_struktur_id` VARCHAR(32) NOT NULL DEFAULT '0',
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  INDEX `fk_surat_pegawai_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_surat_pegawai_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_surat_pegawai_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_surat_pegawai_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_pegawai_struktur_struktur1_idx` (`struktur_id` ASC),
  INDEX `fk_surat_pegawai_golongan1_idx` (`golongan_id` ASC),
  CONSTRAINT `fk_surat_pegawai_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_surat_pegawai_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_surat_pegawai_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_surat_pegawai_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_surat_pegawai_struktur1`
    FOREIGN KEY (`struktur_id`)
    REFERENCES `struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_surat_pegawai_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201012_043528_ct_surat_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201012_043528_ct_surat_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

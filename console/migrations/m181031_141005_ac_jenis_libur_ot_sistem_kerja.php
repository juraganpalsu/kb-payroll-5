<?php

use yii\db\Migration;

/**
 * Class m181031_141005_ac_jenis_libur_ot_sistem_kerja
 */
class m181031_141005_ac_jenis_libur_ot_sistem_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sistem_kerja', 'jenis_libur', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('hari_keenam_setengah'));
        $this->addColumn('pola_jadwal_libur', 'hari', 'VARCHAR(45) NULL AFTER libur');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181031_141005_ac_jenis_libur_ot_sistem_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181031_141005_ac_jenis_libur_ot_sistem_kerja cannot be reverted.\n";

        return false;
    }
    */
}

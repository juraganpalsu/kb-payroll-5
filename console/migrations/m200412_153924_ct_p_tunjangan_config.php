<?php

use yii\db\Migration;

/**
 * Class m200412_153924_ct_p_tunjangan_config
 */
class m200412_153924_ct_p_tunjangan_config extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `p_tunjangan_anak_config` (
  `id` VARCHAR(32) NOT NULL,
  `jumlah_anak` INT NOT NULL DEFAULT 0,
  `nominal` INT(11) NOT NULL DEFAULT 0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `p_tunjangan_loyalitas_config` (
  `id` VARCHAR(32) NOT NULL,
  `masa_kerja` INT(2) NOT NULL DEFAULT 0,
  `nominal` INT(11) NOT NULL DEFAULT 0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SQL;

    $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200412_153924_ct_p_tunjangan_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200412_153924_ct_p_tunjangan_config cannot be reverted.\n";

        return false;
    }
    */
}

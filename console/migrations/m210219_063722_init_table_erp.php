<?php

use yii\db\Migration;

/**
 * Class m210219_063722_init_table_erp
 */
class m210219_063722_init_table_erp extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `e_satuan` (
  `id` VARCHAR(36) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_barang`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_barang` (
  `id` VARCHAR(36) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `harga_terakhir` DECIMAL(12,2) NOT NULL DEFAULT 0,
  `e_satuan_id` VARCHAR(36) NOT NULL,
  `kategori` INT(2) NOT NULL DEFAULT 0 COMMENT 'ATK\nGeneral',
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_barang_e_satuan1_idx` (`e_satuan_id` ASC),
  CONSTRAINT `fk_e_barang_e_satuan1`
    FOREIGN KEY (`e_satuan_id`)
    REFERENCES `e_satuan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_gudang`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_gudang` (
  `id` VARCHAR(36) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_stok`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_stok` (
  `id` VARCHAR(36) NOT NULL,
  `jumlah` DECIMAL(12,2) NOT NULL,
  `e_barang_id` VARCHAR(36) NOT NULL,
  `e_gudang_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_stok_e_barang1_idx` (`e_barang_id` ASC),
  INDEX `fk_e_stok_e_gudang1_idx` (`e_gudang_id` ASC),
  CONSTRAINT `fk_e_stok_e_barang1`
    FOREIGN KEY (`e_barang_id`)
    REFERENCES `e_barang` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_stok_e_gudang1`
    FOREIGN KEY (`e_gudang_id`)
    REFERENCES `e_gudang` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_harga`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_harga` (
  `id` VARCHAR(36) NOT NULL,
  `harga` DECIMAL(12,2) NOT NULL DEFAULT 0,
  `e_barang_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_harga_e_barang1_idx` (`e_barang_id` ASC),
  CONSTRAINT `fk_e_harga_e_barang1`
    FOREIGN KEY (`e_barang_id`)
    REFERENCES `e_barang` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_permintaan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_permintaan` (
  `id` VARCHAR(36) NOT NULL,
  `keterangan` TEXT NULL,
  `e_gudang_id` VARCHAR(36) NOT NULL,
  `e_satuan_id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `area_kerja_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_permintaan_e_gudang1_idx` (`e_gudang_id` ASC),
  INDEX `fk_e_permintaan_e_satuan1_idx` (`e_satuan_id` ASC),
  INDEX `fk_e_permintaan_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_e_permintaan_area_kerja1_idx` (`area_kerja_id` ASC),
  CONSTRAINT `fk_e_permintaan_e_gudang1`
    FOREIGN KEY (`e_gudang_id`)
    REFERENCES `e_gudang` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_permintaan_e_satuan1`
    FOREIGN KEY (`e_satuan_id`)
    REFERENCES `e_satuan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_permintaan_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_permintaan_area_kerja1`
    FOREIGN KEY (`area_kerja_id`)
    REFERENCES `area_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_permintaan_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_permintaan_detail` (
  `id` VARCHAR(36) NOT NULL,
  `jumlah` DECIMAL(12,2) NOT NULL DEFAULT 0,
  `e_permintaan_id` VARCHAR(36) NOT NULL,
  `e_stok_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_permintaan_detail_e_permintaan1_idx` (`e_permintaan_id` ASC),
  INDEX `fk_e_permintaan_detail_e_stok1_idx` (`e_stok_id` ASC),
  CONSTRAINT `fk_e_permintaan_detail_e_permintaan1`
    FOREIGN KEY (`e_permintaan_id`)
    REFERENCES `e_permintaan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_permintaan_detail_e_stok1`
    FOREIGN KEY (`e_stok_id`)
    REFERENCES `e_stok` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_barang_keluar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_barang_keluar` (
  `id` VARCHAR(36) NOT NULL,
  `keterangan` TEXT NULL,
  `e_permintaan_id` VARCHAR(36) NOT NULL,
  `e_gudang_id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_barang_keluar_e_permintaan1_idx` (`e_permintaan_id` ASC),
  INDEX `fk_e_barang_keluar_e_gudang1_idx` (`e_gudang_id` ASC),
  INDEX `fk_e_barang_keluar_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_e_barang_keluar_e_permintaan1`
    FOREIGN KEY (`e_permintaan_id`)
    REFERENCES `e_permintaan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_barang_keluar_e_gudang1`
    FOREIGN KEY (`e_gudang_id`)
    REFERENCES `e_gudang` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_barang_keluar_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_barang_masuk`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_barang_masuk` (
  `id` VARCHAR(36) NOT NULL,
  `po_number` VARCHAR(45) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `e_gudang_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_barang_masuk_e_gudang1_idx` (`e_gudang_id` ASC),
  CONSTRAINT `fk_e_barang_masuk_e_gudang1`
    FOREIGN KEY (`e_gudang_id`)
    REFERENCES `e_gudang` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_barang_masuk_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_barang_masuk_detail` (
  `id` VARCHAR(36) NOT NULL,
  `jumlah` DECIMAL(12,2) NOT NULL DEFAULT 0,
  `harga` DECIMAL(16,2) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `e_stok_id` VARCHAR(36) NOT NULL,
  `e_barang_masuk_id` VARCHAR(36) NOT NULL,
  `e_satuan_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_barang_masuk_detail_e_stok1_idx` (`e_stok_id` ASC),
  INDEX `fk_e_barang_masuk_detail_e_barang_masuk1_idx` (`e_barang_masuk_id` ASC),
  INDEX `fk_e_barang_masuk_detail_e_satuan1_idx` (`e_satuan_id` ASC),
  CONSTRAINT `fk_e_barang_masuk_detail_e_stok1`
    FOREIGN KEY (`e_stok_id`)
    REFERENCES `e_stok` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_barang_masuk_detail_e_barang_masuk1`
    FOREIGN KEY (`e_barang_masuk_id`)
    REFERENCES `e_barang_masuk` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_barang_masuk_detail_e_satuan1`
    FOREIGN KEY (`e_satuan_id`)
    REFERENCES `e_satuan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_barang_keluar_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_barang_keluar_detail` (
  `id` VARCHAR(36) NOT NULL,
  `jumlah` DECIMAL(12,2) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `e_barang_keluar_id` VARCHAR(36) NOT NULL,
  `e_satuan_id` VARCHAR(36) NOT NULL,
  `e_stok_id` VARCHAR(36) NOT NULL,
  `e_permintaan_detail_id` VARCHAR(36) NOT NULL,
  `e_barang_masuk_detail_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_barang_keluar_detail_e_barang_keluar1_idx` (`e_barang_keluar_id` ASC),
  INDEX `fk_e_barang_keluar_detail_e_satuan1_idx` (`e_satuan_id` ASC),
  INDEX `fk_e_barang_keluar_detail_e_stok1_idx` (`e_stok_id` ASC),
  INDEX `fk_e_barang_keluar_detail_e_permintaan_detail1_idx` (`e_permintaan_detail_id` ASC),
  INDEX `fk_e_barang_keluar_detail_e_barang_masuk_detail1_idx` (`e_barang_masuk_detail_id` ASC),
  CONSTRAINT `fk_e_barang_keluar_detail_e_barang_keluar1`
    FOREIGN KEY (`e_barang_keluar_id`)
    REFERENCES `e_barang_keluar` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_barang_keluar_detail_e_satuan1`
    FOREIGN KEY (`e_satuan_id`)
    REFERENCES `e_satuan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_barang_keluar_detail_e_stok1`
    FOREIGN KEY (`e_stok_id`)
    REFERENCES `e_stok` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_barang_keluar_detail_e_permintaan_detail1`
    FOREIGN KEY (`e_permintaan_detail_id`)
    REFERENCES `e_permintaan_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_barang_keluar_detail_e_barang_masuk_detail1`
    FOREIGN KEY (`e_barang_masuk_detail_id`)
    REFERENCES `e_barang_masuk_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_setting_approve`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_setting_approve` (
  `id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `atasan_pegawai_id` VARCHAR(32) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_setting_approve_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_e_setting_approve_pegawai2_idx` (`atasan_pegawai_id` ASC),
  CONSTRAINT `fk_e_setting_approve_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_setting_approve_pegawai2`
    FOREIGN KEY (`atasan_pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL DEFAULT 0,
  `table` VARCHAR(225) NOT NULL DEFAULT '-',
  `relasi_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_approval`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_approval` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipe` INT(2) NOT NULL DEFAULT 0,
  `tanggal` DATE NOT NULL,
  `komentar` TEXT NULL,
  `level` INT(2) NOT NULL DEFAULT 0,
  `table` VARCHAR(225) NOT NULL DEFAULT '-',
  `relasi_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `e_stok_pergerakan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e_stok_pergerakan` (
  `id` VARCHAR(36) NOT NULL,
  `jumlah` DECIMAL(12,2) NOT NULL DEFAULT 0,
  `e_stok_id` VARCHAR(36) NOT NULL,
  `e_satuan_id` VARCHAR(36) NOT NULL,
  `table` VARCHAR(225) NOT NULL DEFAULT '-',
  `relasi_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_stok_pergerakan_e_stok1_idx` (`e_stok_id` ASC),
  INDEX `fk_e_stok_pergerakan_e_satuan1_idx` (`e_satuan_id` ASC),
  CONSTRAINT `fk_e_stok_pergerakan_e_stok1`
    FOREIGN KEY (`e_stok_id`)
    REFERENCES `e_stok` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_stok_pergerakan_e_satuan1`
    FOREIGN KEY (`e_satuan_id`)
    REFERENCES `e_satuan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210219_063722_init_table_erp cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210219_063722_init_table_erp cannot be reverted.\n";

        return false;
    }
    */
}

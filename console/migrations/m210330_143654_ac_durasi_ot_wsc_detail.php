<?php

use yii\db\Migration;

/**
 * Class m210330_143654_ac_durasi_ot_wsc_detail
 */
class m210330_143654_ac_durasi_ot_wsc_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('wsc_detail', 'durasi', $this->integer(4)->notNull()->defaultValue(0)->after('nama_pekerjaan'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210330_143654_ac_durasi_ot_wsc_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210330_143654_ac_durasi_ot_wsc_detail cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m181119_101942_ac_sistem_kerja_id_ot_libur_pegawai
 */
class m181119_101942_ac_sistem_kerja_id_ot_libur_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('libur_pegawai', 'sistem_kerja_id', $this->integer(11)->notNull()->defaultValue(0)->after('pegawai_id'));
        $this->addForeignKey('libur_pegawai_sistem_kerja', 'libur_pegawai', 'sistem_kerja_id', 'sistem_kerja', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181119_101942_ac_sistem_kerja_id_ot_libur_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181119_101942_ac_sistem_kerja_id_ot_libur_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

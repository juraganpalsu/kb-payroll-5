<?php

use yii\db\Migration;

/**
 * Class m190426_081349_ac_has_uploaded_to_att_ot_pegawai
 */
class m190426_081349_ac_has_uploaded_to_att_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai', 'has_uploaded_to_att', $this->tinyInteger(1)->defaultValue(0)->notNull()->after('golongan_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190426_081349_ac_has_uploaded_to_att_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190426_081349_ac_has_uploaded_to_att_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191128_151253_uc_nama_ot_ref_peraturan_perusahaan
 */
class m191128_151253_uc_nama_ot_ref_peraturan_perusahaan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `ref_peraturan_perusahaan` 
CHANGE COLUMN `nama` `nama` TEXT NOT NULL ;
SQL;
        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191128_151253_uc_nama_ot_ref_peraturan_perusahaan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191128_151253_uc_nama_ot_ref_peraturan_perusahaan cannot be reverted.\n";

        return false;
    }
    */
}

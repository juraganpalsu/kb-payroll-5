<?php

use yii\db\Migration;

/**
 * Class m210828_141024_ac_tunjangan_pph_ot_gaji_pph
 */
class m210828_141024_ac_tunjangan_pph_ot_gaji_pph extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_pph21', 'tunjangan_pph', $this->decimal(11,2)->notNull()->defaultValue(0)->after('gaji'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210828_141024_ac_tunjangan_pph_ot_gaji_pph cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210828_141024_ac_tunjangan_pph_ot_gaji_pph cannot be reverted.\n";

        return false;
    }
    */
}

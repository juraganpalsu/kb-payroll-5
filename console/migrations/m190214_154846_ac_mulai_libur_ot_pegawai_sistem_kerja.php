<?php

use yii\db\Migration;

/**
 * Class m190214_154846_ac_mulai_libur_ot_pegawai_sistem_kerja
 */
class m190214_154846_ac_mulai_libur_ot_pegawai_sistem_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai_sistem_kerja', 'mulai_libur', $this->date()->null()->after('akhir_berlaku'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190214_154846_ac_mulai_libur_ot_pegawai_sistem_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_154846_ac_mulai_libur_ot_pegawai_sistem_kerja cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200803_091228_ct_payroll_bonus_detail
 */
class m200803_091228_ct_payroll_bonus_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `payroll_bonus_detail` (
  `id` VARCHAR(32) NOT NULL,
  `nominal` INT NOT NULL DEFAULT 0,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `payroll_bonus_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_bonus_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_bonus_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_bonus_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_bonus_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_payroll_bonus_detail_payroll_bonus1_idx` (`payroll_bonus_id` ASC),
  CONSTRAINT `fk_bonus_detail_pegawai10`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bonus_detail_perjanjian_kerja10`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bonus_detail_pegawai_golongan10`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bonus_detail_pegawai_struktur10`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_bonus_detail_payroll_bonus1`
    FOREIGN KEY (`payroll_bonus_id`)
    REFERENCES `payroll_bonus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200803_091228_ct_payroll_bonus_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200803_091228_ct_payroll_bonus_detail cannot be reverted.\n";

        return false;
    }
    */
}

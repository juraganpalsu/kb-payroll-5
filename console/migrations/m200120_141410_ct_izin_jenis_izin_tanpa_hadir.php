<?php

use yii\db\Migration;

/**
 * Class m200120_141410_ct_izin_jenis_izin_tanpa_hadir
 */
class m200120_141410_ct_izin_jenis_izin_tanpa_hadir extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `izin_jenis` (
  `id` VARCHAR(32) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `izin_tanpa_hadir` (
  `id` VARCHAR(32) NOT NULL,
  `tanggal_mulai` DATE NOT NULL,
  `tanggal_selesai` DATE NOT NULL,
  `keterangan` TEXT NULL,
  `batal` INT(1) NOT NULL DEFAULT 0,
  `izin_jenis_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_izin_tanpa_hadir_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_izin_tanpa_hadir_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_izin_tanpa_hadir_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_izin_tanpa_hadir_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_izin_tanpa_hadir_izin_jenis1_idx` (`izin_jenis_id` ASC),
  CONSTRAINT `fk_izin_tanpa_hadir_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_izin_tanpa_hadir_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_izin_tanpa_hadir_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_izin_tanpa_hadir_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_izin_tanpa_hadir_izin_jenis1`
    FOREIGN KEY (`izin_jenis_id`)
    REFERENCES `izin_jenis` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200120_141410_ct_izin_jenis_izin_tanpa_hadir cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200120_141410_ct_izin_jenis_izin_tanpa_hadir cannot be reverted.\n";

        return false;
    }
    */
}

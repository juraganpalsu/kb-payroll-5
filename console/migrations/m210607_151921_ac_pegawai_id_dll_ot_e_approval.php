<?php

use yii\db\Migration;

/**
 * Class m210607_151921_ac_pegawai_id_dll_ot_e_approval
 */
class m210607_151921_ac_pegawai_id_dll_ot_e_approval extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropTable('e_approval');
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `e_approval` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipe` INT(2) NULL,
  `tanggal` DATETIME NULL,
  `komentar` TEXT NULL,
  `level` INT(2) NOT NULL DEFAULT 0,
  `urutan` INT(2) NOT NULL DEFAULT 0,
  `table` VARCHAR(225) NOT NULL DEFAULT '-',
  `relasi_id` VARCHAR(36) NOT NULL,
  `e_approval_id` INT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_approval_e_approval1_idx` (`e_approval_id` ASC) ,
  INDEX `fk_e_approval_pegawai1_idx` (`pegawai_id` ASC) ,
  INDEX `fk_e_approval_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC) ,
  INDEX `fk_e_approval_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC) ,
  CONSTRAINT `fk_e_approval_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_approval_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_approval_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210607_151921_ac_pegawai_id_dll_ot_e_approval cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210607_151921_ac_pegawai_id_dll_ot_e_approval cannot be reverted.\n";

        return false;
    }
    */
}

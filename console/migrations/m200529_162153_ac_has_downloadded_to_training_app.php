<?php

use yii\db\Migration;

/**
 * Class m200529_162153_ac_has_downloadded_to_training_app
 */
class m200529_162153_ac_has_downloadded_to_training_app extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai', 'has_downloadded_to_training_app', $this->integer(1)->notNull()->defaultValue(0)->after('has_created_account'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200529_162153_ac_has_downloadded_to_training_app cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200529_162153_ac_has_downloadded_to_training_app cannot be reverted.\n";

        return false;
    }
    */
}

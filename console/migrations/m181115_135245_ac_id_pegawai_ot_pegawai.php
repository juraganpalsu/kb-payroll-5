<?php

use yii\db\Migration;

/**
 * Class m181115_135245_ac_id_pegawai_ot_pegawai
 */
class m181115_135245_ac_id_pegawai_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai', 'id_pegawai', $this->bigInteger(20)->notNull()->defaultValue(0)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181115_135245_ac_id_pegawai_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181115_135245_ac_id_pegawai_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

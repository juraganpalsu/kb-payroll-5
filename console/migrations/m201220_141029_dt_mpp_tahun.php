<?php

use yii\db\Migration;

/**
 * Class m201220_141029_dt_mpp_tahun
 */
class m201220_141029_dt_mpp_tahun extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
ALTER TABLE `mpp_jumlah` 
DROP FOREIGN KEY `fk_mpp_jumlah_mpp_tahun1`,
DROP FOREIGN KEY `fk_mpp_jumlah_mpp1`;
ALTER TABLE `mpp_jumlah` 
DROP COLUMN `mpp_tahun_id`,
DROP INDEX `fk_mpp_jumlah_mpp1_idx` ,
DROP INDEX `fk_mpp_jumlah_mpp_tahun1_idx` ;

SQL;
        $this->execute($sql);

        $this->dropTable('mpp_tahun');

        $this->addColumn('mpp_jumlah', 'tahun', $this->integer(4)->notNull()->defaultValue(0)->after('id'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201220_141029_dt_mpp_tahun cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201220_141029_dt_mpp_tahun cannot be reverted.\n";

        return false;
    }
    */
}

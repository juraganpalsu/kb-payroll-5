<?php

use yii\db\Migration;

/**
 * Class m190717_012345_ac_id_pegawai_ot_perjanjian_kerja
 */
class m190717_012345_ac_id_pegawai_ot_perjanjian_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perjanjian_kerja', 'id_pegawai', $this->char(20)->notNull()->defaultValue(0)->after('kontrak'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190717_012345_ac_id_pegawai_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190717_012345_ac_id_pegawai_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }
    */
}

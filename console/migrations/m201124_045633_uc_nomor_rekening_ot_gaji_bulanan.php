<?php

use yii\db\Migration;

/**
 * Class m201124_045633_uc_nomor_rekening_ot_gaji_bulanan
 */
class m201124_045633_uc_nomor_rekening_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('gaji_bulanan', 'pegawai_bank_nomor', $this->string(45)->null()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201124_045633_uc_nomor_rekening_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201124_045633_uc_nomor_rekening_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

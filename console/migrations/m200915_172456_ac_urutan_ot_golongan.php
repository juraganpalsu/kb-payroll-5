<?php

use yii\db\Migration;

/**
 * Class m200915_172456_ac_urutan_ot_golongan
 */
class m200915_172456_ac_urutan_ot_golongan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('golongan', 'urutan', $this->integer()->notNull()->defaultValue(0)->after('nama'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200915_172456_ac_urutan_ot_golongan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200915_172456_ac_urutan_ot_golongan cannot be reverted.\n";

        return false;
    }
    */
}

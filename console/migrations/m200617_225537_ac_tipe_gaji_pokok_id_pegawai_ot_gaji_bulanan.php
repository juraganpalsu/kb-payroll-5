<?php

use yii\db\Migration;

/**
 * Class m200617_225537_ac_tipe_gaji_pokok_id_pegawai_ot_gaji_bulanan
 */
class m200617_225537_ac_tipe_gaji_pokok_id_pegawai_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'id_pegawai', $this->integer(11)->notNull()->defaultValue(0)->after('id'));
        $this->addColumn('gaji_bulanan', 'tipe_gaji_pokok', $this->integer(2)->notNull()->defaultValue(0)->after('id_pegawai'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200617_225537_ac_tipe_gaji_pokok_id_pegawai_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200617_225537_ac_tipe_gaji_pokok_id_pegawai_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191212_163447_ac_kontrak_ot_ds_akhir_kontrak
 */
class m191212_163447_ac_kontrak_ot_ds_akhir_kontrak extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ds_akhir_kontrak_detail', 'kontrak', $this->integer(11)->notNull()->defaultValue(0)->after('id'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191212_163447_ac_kontrak_ot_ds_akhir_kontrak cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191212_163447_ac_kontrak_ot_ds_akhir_kontrak cannot be reverted.\n";

        return false;
    }
    */
}

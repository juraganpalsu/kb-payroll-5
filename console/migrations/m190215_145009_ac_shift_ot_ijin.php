<?php

use yii\db\Migration;

/**
 * Class m190215_145009_ac_shift_ot_ijin
 */
class m190215_145009_ac_shift_ot_ijin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ijin', 'shift', $this->tinyInteger(1)->defaultValue(0)->after('keterangan'));
        $this->addColumn('perubahan_status_absensi', 'shift', $this->tinyInteger(1)->defaultValue(0)->after('keterangan'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190215_145009_ac_shift_ot_ijin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190215_145009_ac_shift_ot_ijin cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190711_160111_ac_kontrak_ot_perjanjian_kerja
 */
class m190711_160111_ac_kontrak_ot_perjanjian_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perjanjian_kerja', 'kontrak', $this->integer(2)->after('jenis_perjanjian')->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190711_160111_ac_kontrak_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190711_160111_ac_kontrak_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }
    */
}

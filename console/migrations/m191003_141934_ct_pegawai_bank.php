<?php

use yii\db\Migration;

/**
 * Class m191003_141934_ct_pegawai_bank
 */
class m191003_141934_ct_pegawai_bank extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `pegawai_bank` (
  `id` VARCHAR(32) NOT NULL,
  `nama_bank` VARCHAR(225) NOT NULL,
  `nomer_rekening` BIGINT(16) NOT NULL,
  `catatan` TEXT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_bank_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_bank_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `pegawai_kontak_darurat` (
  `id` VARCHAR(32) NOT NULL,
  `hubungan` INT(2) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `pekerjaan` VARCHAR(45) NOT NULL,
  `alamat` TEXT NULL,
  `no_handphone` VARCHAR(13) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_kontak_darurat_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_kontak_darurat_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191003_141934_ct_pegawai_bank cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191003_141934_ct_pegawai_bank cannot be reverted.\n";

        return false;
    }
    */
}

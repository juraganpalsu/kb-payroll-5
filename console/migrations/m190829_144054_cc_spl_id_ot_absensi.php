<?php

use yii\db\Migration;

/**
 * Class m190829_144054_cc_spl_id_ot_absensi
 */
class m190829_144054_cc_spl_id_ot_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
ALTER TABLE `absensi` 
CHANGE COLUMN `spl_id` `spl_id` VARCHAR(32) NULL DEFAULT '0' ;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190829_144054_cc_spl_id_ot_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190829_144054_cc_spl_id_ot_absensi cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190819_151612_ac_urutan_ot_approval
 */
class m190819_151612_ac_urutan_ot_approval extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('spl_approval', 'urutan', $this->integer(2)->after('level')->notNull()->defaultValue(0));
        $this->addColumn('spl_detail_approval', 'urutan', $this->integer(2)->after('level')->notNull()->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190819_151612_ac_urutan_ot_approval cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190819_151612_ac_urutan_ot_approval cannot be reverted.\n";

        return false;
    }
    */
}

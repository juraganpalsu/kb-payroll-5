<?php

use yii\db\Migration;

/**
 * Class m200710_035128_ac_tukar_libur_ot_gaji_bulanan
 */
class m200710_035128_ac_tukar_libur_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'tl', $this->integer(2)->defaultValue(0)->after('dispensasi'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200710_035128_ac_tukar_libur_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200710_035128_ac_tukar_libur_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

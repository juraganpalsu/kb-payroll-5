<?php

use yii\db\Migration;

/**
 * Class m200809_133101_ac_finishing_status_ot_proses_gaji
 */
class m200809_133101_ac_finishing_status_ot_proses_gaji extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('proses_gaji', 'finishing_status', $this->integer(2)->notNull()->defaultValue(0)->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200809_133101_ac_finishing_status_ot_proses_gaji cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200809_133101_ac_finishing_status_ot_proses_gaji cannot be reverted.\n";

        return false;
    }
    */
}

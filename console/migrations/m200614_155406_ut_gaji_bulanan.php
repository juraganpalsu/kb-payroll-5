<?php

use yii\db\Migration;

/**
 * Class m200614_155406_ut_gaji_bulanan
 */
class m200614_155406_ut_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropTable('gaji_bulanan_detail');
        $this->dropTable('gaji_bulanan');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200614_155406_ut_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200614_155406_ut_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}
<?php

use yii\db\Migration;

/**
 * Class m210912_115140_ac_gaji_pph21_id_ot_gaji_pph21
 */
class m210912_115140_ac_gaji_pph21_id_ot_gaji_pph21 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_pph21', 'gaji_pph21_id', $this->string(32)->null()->after('pegawai_ptkp_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210912_115140_ac_gaji_pph21_id_ot_gaji_pph21 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210912_115140_ac_gaji_pph21_id_ot_gaji_pph21 cannot be reverted.\n";

        return false;
    }
    */
}

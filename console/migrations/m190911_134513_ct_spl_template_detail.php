<?php

use yii\db\Migration;

/**
 * Class m190911_134513_ct_spl_template_detail
 */
class m190911_134513_ct_spl_template_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `spl_template_detail` (
  `id` VARCHAR(45) NOT NULL,
  `spl_template_id` INT(11) NOT NULL,
  `spl_template_detail_id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  INDEX `fk_spl_template_detail_spl_template1_idx` (`spl_template_id` ASC),
  INDEX `fk_spl_template_detail_spl_template2_idx` (`spl_template_detail_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_spl_template_detail_spl_template1`
    FOREIGN KEY (`spl_template_id`)
    REFERENCES `spl_template` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_spl_template_detail_spl_template2`
    FOREIGN KEY (`spl_template_detail_id`)
    REFERENCES `spl_template` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190911_134513_ct_spl_template_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190911_134513_ct_spl_template_detail cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m201013_134015_ac_ot_gaji_borongan
 */
class m201013_134015_ac_ot_gaji_borongan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_borongan_header', 'jumlah_gaji', $this->decimal(11, 2)->defaultValue(0)->after('catatan'));
        $this->addColumn('gaji_borongan', 'shift', $this->integer(2)->defaultValue(0)->after('gaji'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201013_134015_ac_ot_gaji_borongan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201013_134015_ac_ot_gaji_borongan cannot be reverted.\n";

        return false;
    }
    */
}

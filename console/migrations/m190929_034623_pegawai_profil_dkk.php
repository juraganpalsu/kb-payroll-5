<?php

use yii\db\Migration;

/**
 * Class m190929_034623_pegawai_profil_dkk
 */
class m190929_034623_pegawai_profil_dkk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL

CREATE TABLE IF NOT EXISTS `pegawai_profile` (
  `id` VARCHAR(32) NOT NULL,
  `nama_lengkap` VARCHAR(225) NOT NULL,
  `nama_panggilan` VARCHAR(45) NOT NULL,
  `jenis_kelamin` TINYINT(1) NOT NULL,
  `tempat_lahir` VARCHAR(45) NOT NULL,
  `tanggal_lahir` DATE NOT NULL,
  `status_pernikahan` TINYINT(2) NOT NULL,
  `kewarganegaraan` VARCHAR(45) NOT NULL,
  `agama` TINYINT(2) NOT NULL,
  `nomor_ktp` VARCHAR(16) NOT NULL,
  `nomor_kk` VARCHAR(45) NULL,
  `alamat_sesuai_ktp` TEXT NOT NULL,
  `alamat_tempat_tinggal` TEXT NOT NULL,
  `no_telp` VARCHAR(13) NULL,
  `no_handphone` VARCHAR(13) NOT NULL,
  `alamat_email` VARCHAR(225) NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_profile_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_profile_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pegawai_pendidikan` (
  `id` VARCHAR(32) NOT NULL,
  `jenjang` INT(2) NOT NULL,
  `nama_sekolah` VARCHAR(225) NOT NULL,
  `lokasi` VARCHAR(225) NOT NULL,
  `jurusan` VARCHAR(225) NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_pendidikan_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_pendidikan_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `pegawai_keluarga` (
  `id` VARCHAR(32) NOT NULL,
  `hubungan` INT(2) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `pekerjaan` VARCHAR(45) NOT NULL,
  `alamat` TEXT NULL,
  `no_handphone` VARCHAR(13) NULL,
  `kelompok` INT(2) NULL COMMENT '1.orang tua\n2.istri\n3.anak\n4.saudara',
  `urutan_ke` INT(2) NOT NULL DEFAULT 0 COMMENT 'urutan anak \natau keluarga',
  `pegawai_id` VARCHAR(32) NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_keluarga_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_keluarga_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pegawai_npwp` (
  `id` VARCHAR(32) NOT NULL,
  `nomor` VARCHAR(45) NOT NULL,
  `catatan` TEXT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_npwp_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_npwp_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pegawai_bpjs` (
  `id` VARCHAR(32) NOT NULL,
  `jenis` INT(2) NOT NULL COMMENT '1.kesehatan\n2.ketenagakerjaan',
  `nomor` VARCHAR(15) NOT NULL,
  `tanggal_kepesertaan` DATE NOT NULL,
  `upah` VARCHAR(45) NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_bpjs_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_bpjs_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `pegawai_asuransi_lain` (
  `id` VARCHAR(32) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `nomor` VARCHAR(45) NOT NULL,
  `tanggal_kepesertaan` DATE NOT NULL,
  `catatan` TEXT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_asuransi_lain_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_asuransi_lain_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190929_034623_pegawai_profil_dkk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190929_034623_pegawai_profil_dkk cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200625_154938_ac_s2_nominal_ot_gaji_bulanan
 */
class m200625_154938_ac_s2_nominal_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 's2_nominal', $this->integer(11)->notNull()->defaultValue(0)->after('s2'));
        $this->addColumn('gaji_bulanan', 's3_nominal', $this->integer(11)->notNull()->defaultValue(0)->after('s3'));

        $this->addColumn('izin_jenis', 'uang_makan', $this->boolean()->defaultValue(true)->after('nama'));
        $this->addColumn('izin_jenis', 'uang_transport', $this->boolean()->defaultValue(true)->after('uang_makan'));
        $this->addColumn('izin_jenis', 'hitung_masuk', $this->boolean()->defaultValue(true)->after('uang_transport'));
        $this->addColumn('izin_jenis', 'potong_gaji', $this->boolean()->defaultValue(true)->after('hitung_masuk'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200625_154938_ac_s2_nominal_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200625_154938_ac_s2_nominal_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

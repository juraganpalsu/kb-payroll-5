<?php

use yii\db\Migration;

class m171203_155152_ac_istirahat_ot_spl_template extends Migration
{
    public function safeUp()
    {
        $this->addColumn('spl_template', 'tunjangan_shift', $this->integer(1)->defaultValue(0)->after('istirahat'));
        $this->addColumn('spl_template', 'jam_istirahat_minimum', $this->time()->null()->after('masuk_maksimum'));
        $this->addColumn('spl_template', 'jam_istirahat', $this->time()->null()->after('jam_istirahat_minimum'));
        $this->addColumn('spl_template', 'jam_istirahat_maksimum', $this->time()->null()->after('jam_istirahat'));
        $this->addColumn('spl_template', 'masuk_istirahat_minimum', $this->time()->null()->after('jam_istirahat_maksimum'));
        $this->addColumn('spl_template', 'masuk_istirahat', $this->time()->null()->after('masuk_istirahat_minimum'));
        $this->addColumn('spl_template', 'masuk_istirahat_maksimum', $this->time()->null()->after('masuk_istirahat'));
    }

    public function safeDown()
    {
        echo "m171203_155152_ac_istirahat_ot_spl_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171203_155152_ac_istirahat_ot_spl_template cannot be reverted.\n";

        return false;
    }
    */
}

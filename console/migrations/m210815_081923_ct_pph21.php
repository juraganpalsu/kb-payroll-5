<?php

use yii\db\Migration;

/**
 * Class m210815_081923_ct_pph21
 */
class m210815_081923_ct_pph21 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `pegawai_ptkp` (
  `id` VARCHAR(36) NOT NULL,
  `tahun` INT NOT NULL,
  `status` VARCHAR(5) NOT NULL,
  `ptkp` DECIMAL(11,2) NOT NULL COMMENT 'Noted: \n\n- Acuan administrasi untuk menentukan PTKP & Tanggungan sebaiknya menggunakan Kartu Keluarga sebagai rujukan awal tertulis\n- Daftar PTKP berlaku saat awal tahun.\n- Seluruh pegawai wanita kawin, PTKP sebesar untuk \"dirinya sendiri\" (54 juta).\n- Tahun 2021 TK/0 = 54 juta. Tambahan Tanggungan untuk WP Kawin 4.500.000,-\n- tambahan 4,5 juta untuk setiap anggota keluarga sedarah dan keluarga semenda dalam garis keturunan lurus serta anak angkat, yang menjadi tanggungan sepenuhnya, paling banyak 3 (tiga) orang untuk setiap keluarga (ada surat yang membuktikan)',
  `keterangan` TEXT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_ptkp_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_pegawai_ptkp_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_pegawai_ptkp_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_pegawai_ptkp_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_pegawai_ptkp_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_ptkp_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_ptkp_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_ptkp_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `gaji_pph21` (
  `id` VARCHAR(36) NOT NULL,
  `gaji` DECIMAL(11,2) NOT NULL DEFAULT 0 COMMENT 'Upah/Gaji Pokok (Rapel)',
  `tunjangan` DECIMAL(11,2) NOT NULL DEFAULT 0 COMMENT 'Seluruh Tunjangan yang diterima oleh pegawai\n\nInsentif',
  `premi_asuransi` DECIMAL(11,2) NOT NULL DEFAULT 0 COMMENT '\'-Premi JKK, JKM (BPJS TK)\n- BPJS Kes 4%\n- Asuransi Manulife',
  `bonus` DECIMAL(11,2) NOT NULL DEFAULT 0 COMMENT 'TANTIEM, BONUS, GRATIFIKASI, JASA PRODUKSI DAN THR',
  `penghasilan_bruto` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `biaya_jabatan` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `iuran_pensiun` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `pengurangan` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `penghasilan_netto` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `penghasilan_netto_setahun` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `ptkp_status` VARCHAR(5) NOT NULL DEFAULT 0,
  `ptkp_nominal` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `pkp` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `pph21_terhutang_setahun` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `pph21_terhutang` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `gaji_bulanan_id` VARCHAR(32) NOT NULL,
  `pegawai_ptkp_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  INDEX `fk_gaji_pph21_gaji_bulanan1_idx` (`gaji_bulanan_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_gaji_pph21_pegawai_ptkp1_idx` (`pegawai_ptkp_id` ASC),
  CONSTRAINT `fk_gaji_pph21_gaji_bulanan1`
    FOREIGN KEY (`gaji_bulanan_id`)
    REFERENCES `gaji_bulanan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_pph21_pegawai_ptkp1`
    FOREIGN KEY (`pegawai_ptkp_id`)
    REFERENCES `pegawai_ptkp` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `payroll_bpjs_tk_detail` (
  `id` VARCHAR(36) NOT NULL,
  `gaji_pokok` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `jkk_persen` DECIMAL(3,2) NOT NULL DEFAULT 0,
  `jkk_nominal` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `jkn_persen` DECIMAL(3,2) NOT NULL DEFAULT 0,
  `jkn_nominal` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `jht_persen` DECIMAL(3,2) NOT NULL DEFAULT 0,
  `jht_nominal` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `jht_persen_tk` DECIMAL(3,2) NOT NULL DEFAULT 0,
  `jht_nominal_tk` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `iuran_pensiun_persen` DECIMAL(3,2) NOT NULL DEFAULT 0,
  `iuran_pensiun_nominal` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `iuran_pensiun_tk_persen` DECIMAL(3,2) NOT NULL DEFAULT 0,
  `iuran_pensiun_tk_nominal` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `total_iuran` DECIMAL(11,2) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `pegawai_bpjs_id` VARCHAR(32) NOT NULL,
  `payroll_bpjs_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_payroll_bpjs_tk_detail_pegawai_bpjs1_idx` (`pegawai_bpjs_id` ASC),
  INDEX `fk_payroll_bpjs_tk_detail_payroll_bpjs1_idx` (`payroll_bpjs_id` ASC),
  INDEX `fk_payroll_bpjs_tk_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_payroll_bpjs_tk_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_payroll_bpjs_tk_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_payroll_bpjs_tk_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_payroll_bpjs_tk_detail_pegawai_bpjs1`
    FOREIGN KEY (`pegawai_bpjs_id`)
    REFERENCES `pegawai_bpjs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_bpjs_tk_detail_payroll_bpjs1`
    FOREIGN KEY (`payroll_bpjs_id`)
    REFERENCES `payroll_bpjs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_bpjs_tk_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_bpjs_tk_detail_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_bpjs_tk_detail_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_bpjs_tk_detail_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210815_081923_ct_pph21 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210815_081923_ct_pph21 cannot be reverted.\n";

        return false;
    }
    */
}

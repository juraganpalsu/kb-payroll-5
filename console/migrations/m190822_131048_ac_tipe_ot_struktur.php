<?php

use yii\db\Migration;

/**
 * Class m190822_131048_ac_tipe_ot_struktur
 */
class m190822_131048_ac_tipe_ot_struktur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('struktur', 'tipe', $this->integer(2)->notNull()->defaultValue(0)->after('jabatan_id'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190822_131048_ac_tipe_ot_struktur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190822_131048_ac_tipe_ot_struktur cannot be reverted.\n";

        return false;
    }
    */
}

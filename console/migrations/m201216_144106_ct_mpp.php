<?php

use yii\db\Migration;

/**
 * Class m201216_144106_ct_mpp
 */
class m201216_144106_ct_mpp extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `mpp_header` (
  `id` VARCHAR(36) NOT NULL,
  `tanggal` DATE NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mpp` (
  `id` VARCHAR(36) NOT NULL,
  `departemen_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  `mpp_header_id` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_mpp_departemen1_idx` (`departemen_id` ASC),
  INDEX `fk_mpp_mpp_header1_idx` (`mpp_header_id` ASC),
  CONSTRAINT `fk_mpp_departemen1`
    FOREIGN KEY (`departemen_id`)
    REFERENCES `departemen` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_mpp_header1`
    FOREIGN KEY (`mpp_header_id`)
    REFERENCES `mpp_header` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `mpp_detail` (
  `id` VARCHAR(36) NOT NULL,
  `keterangan` VARCHAR(45) NULL,
  `golongan_id` INT NOT NULL,
  `jabatan_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  `mpp_id` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_mpp_detail_golongan1_idx` (`golongan_id` ASC),
  INDEX `fk_mpp_detail_jabatan1_idx` (`jabatan_id` ASC),
  INDEX `fk_mpp_detail_mpp1_idx` (`mpp_id` ASC),
  CONSTRAINT `fk_mpp_detail_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_detail_jabatan1`
    FOREIGN KEY (`jabatan_id`)
    REFERENCES `jabatan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_detail_mpp1`
    FOREIGN KEY (`mpp_id`)
    REFERENCES `mpp` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `mpp_act` (
  `id` VARCHAR(36) NOT NULL,
  `tanggal` VARCHAR(45) NULL,
  `departemen_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  `mpp_header_id` VARCHAR(36) NOT NULL,
  `mpp_id` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_mpp_act_departemen1_idx` (`departemen_id` ASC),
  INDEX `fk_mpp_act_mpp_header1_idx` (`mpp_header_id` ASC),
  INDEX `fk_mpp_act_mpp1_idx` (`mpp_id` ASC),
  CONSTRAINT `fk_mpp_act_departemen1`
    FOREIGN KEY (`departemen_id`)
    REFERENCES `departemen` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_act_mpp_header1`
    FOREIGN KEY (`mpp_header_id`)
    REFERENCES `mpp_header` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_act_mpp1`
    FOREIGN KEY (`mpp_id`)
    REFERENCES `mpp` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `mpp_detail_act` (
  `id` VARCHAR(36) NOT NULL,
  `keterangan` VARCHAR(45) NULL,
  `golongan_id` INT NOT NULL,
  `jabatan_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  `mpp_act_id` VARCHAR(36) NOT NULL,
  `mpp_detail_id` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_mpp_detail_act_golongan1_idx` (`golongan_id` ASC),
  INDEX `fk_mpp_detail_act_jabatan1_idx` (`jabatan_id` ASC),
  INDEX `fk_mpp_detail_act_mpp_act1_idx` (`mpp_act_id` ASC),
  INDEX `fk_mpp_detail_act_mpp_detail1_idx` (`mpp_detail_id` ASC),
  CONSTRAINT `fk_mpp_detail_act_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_detail_act_jabatan1`
    FOREIGN KEY (`jabatan_id`)
    REFERENCES `jabatan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_detail_act_mpp_act1`
    FOREIGN KEY (`mpp_act_id`)
    REFERENCES `mpp_act` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_detail_act_mpp_detail1`
    FOREIGN KEY (`mpp_detail_id`)
    REFERENCES `mpp_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201216_144106_ct_mpp cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201216_144106_ct_mpp cannot be reverted.\n";

        return false;
    }
    */
}

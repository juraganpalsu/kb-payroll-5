<?php

use yii\db\Migration;

/**
 * Class m191209_150901_ct_penyelesaian_kasus_detail
 */
class m191209_150901_ct_penyelesaian_kasus_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `penyelesaian_kasus_detail` (
  `id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `penyelesaian_kasus_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_penyelesaian_kasus_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_penyelesaian_kasus_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_penyelesaian_kasus_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_penyelesaian_kasus_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_penyelesaian_kasus_detail_penyelesaian_kasus1_idx` (`penyelesaian_kasus_id` ASC),
  CONSTRAINT `fk_penyelesaian_kasus_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_penyelesaian_kasus_detail_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_penyelesaian_kasus_detail_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_penyelesaian_kasus_detail_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_penyelesaian_kasus_detail_penyelesaian_kasus1`
    FOREIGN KEY (`penyelesaian_kasus_id`)
    REFERENCES `penyelesaian_kasus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);

        $this->dropForeignKey('fk_penyelesaian_kasus_pegawai1', 'penyelesaian_kasus');
        $this->dropForeignKey('fk_penyelesaian_kasus_pegawai_golongan1', 'penyelesaian_kasus');
        $this->dropForeignKey('fk_penyelesaian_kasus_pegawai_struktur1', 'penyelesaian_kasus');
        $this->dropForeignKey('fk_penyelesaian_kasus_perjanjian_kerja1', 'penyelesaian_kasus');

        $this->dropColumn('penyelesaian_kasus', 'pegawai_id');
        $this->dropColumn('penyelesaian_kasus', 'perjanjian_kerja_id');
        $this->dropColumn('penyelesaian_kasus', 'pegawai_struktur_id');
        $this->dropColumn('penyelesaian_kasus', 'pegawai_golongan_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191209_150901_ct_penyelesaian_kasus_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191209_150901_ct_penyelesaian_kasus_detail cannot be reverted.\n";

        return false;
    }
    */
}

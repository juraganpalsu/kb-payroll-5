<?php

use yii\db\Migration;

/**
 * Class m200924_065924_ac_kategori_ot_spl
 */
class m200924_065924_ac_kategori_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('spl', 'kategori', $this->tinyInteger(1)->notNull()->defaultValue(1)->after('keterangan'));
        $this->update('spl', ['kategori' => 1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200924_065924_ac_kategori_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200924_065924_ac_kategori_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

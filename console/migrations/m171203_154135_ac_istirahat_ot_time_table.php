<?php

use yii\db\Migration;

class m171203_154135_ac_istirahat_ot_time_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('time_table', 'istirahat_minimum', $this->time()->null()->after('masuk_maksimum'));
        $this->addColumn('time_table', 'istirahat', $this->time()->null()->after('istirahat_minimum'));
        $this->addColumn('time_table', 'istirahat_maksimum', $this->time()->null()->after('istirahat'));
        $this->addColumn('time_table', 'masuk_istirahat_minimum', $this->time()->null()->after('istirahat_maksimum'));
        $this->addColumn('time_table', 'masuk_istirahat', $this->time()->null()->after('masuk_istirahat_minimum'));
        $this->addColumn('time_table', 'masuk_istirahat_maksimum', $this->time()->null()->after('masuk_istirahat'));
    }

    public function safeDown()
    {
        echo "m171203_154135_ac_istirahat_ot_time_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171203_154135_ac_istirahat_ot_time_table cannot be reverted.\n";

        return false;
    }
    */
}

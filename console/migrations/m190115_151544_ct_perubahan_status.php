<?php

use yii\db\Migration;

/**
 * Class m190115_151544_ct_perubahan_status
 */
class m190115_151544_ct_perubahan_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `perubahan_status_absensi` (
  `id` VARCHAR(32) NOT NULL,
  `urut` INT NOT NULL DEFAULT 0,
  `tanggal` DATE NOT NULL,
  `masuk` TIME NULL,
  `pulang` TIME NULL,
  `status_kehadiran` INT(2) NOT NULL,
  `jumlah_jam_lembur` VARCHAR(4) NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `perubahan_status_absensi_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `perubahan_status_absensi_detail` (
  `id` VARCHAR(32) NOT NULL,
  `urut` INT NOT NULL DEFAULT 0,
  `perubahan_status_absensi_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_perubahan_status_absensi_detail_perubahan_status_absensi_idx` (`perubahan_status_absensi_id` ASC),
  INDEX `fk_perubahan_status_absensi_detail_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_perubahan_status_absensi_detail_perubahan_status_absensi1`
    FOREIGN KEY (`perubahan_status_absensi_id`)
    REFERENCES `perubahan_status_absensi` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_perubahan_status_absensi_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190115_151544_ct_perubahan_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190115_151544_ct_perubahan_status cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190829_154747_rapi2_table_pegawai
 */
class m190829_154747_rapi2_table_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
ALTER TABLE `pegawai` 
DROP COLUMN `golongan_id`,
DROP COLUMN `jabatan`,
DROP COLUMN `divisi`,
DROP COLUMN `unit`,
DROP COLUMN `sistem_kerja`,
DROP COLUMN `idfp`;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190829_154747_rapi2_table_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190829_154747_rapi2_table_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200614_232744_dr_ot_gaji_bulanan
 */
class m200614_232744_dr_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `gaji_bulanan` 
CHANGE COLUMN `pegawai_bank_id` `pegawai_bank_id` VARCHAR(32) NULL ,
CHANGE COLUMN `pegawai_struktur_id` `pegawai_struktur_id` VARCHAR(32) NULL ,
CHANGE COLUMN `pegawai_sistem_kerja_id` `pegawai_sistem_kerja_id` INT(11) NULL DEFAULT 0 ;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200614_232744_dr_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200614_232744_dr_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

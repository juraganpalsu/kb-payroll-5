<?php

use yii\db\Migration;

/**
 * Class m191224_100130_cc_kode_ot_cost_center_template
 */
class m191224_100130_cc_kode_ot_cost_center_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `cost_center_template` 
CHANGE COLUMN `kode` `kode` VARCHAR(11) NOT NULL DEFAULT '0' ;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191224_100130_cc_kode_ot_cost_center_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191224_100130_cc_kode_ot_cost_center_template cannot be reverted.\n";

        return false;
    }
    */
}

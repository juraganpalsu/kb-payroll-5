<?php

use yii\db\Migration;

class m171204_152834_ac_spl_id_ot_absensi extends Migration
{
    public function safeUp()
    {
        $this->addColumn('absensi', 'spl_id', $this->integer(11)->defaultValue(0)->after('status_kehadiran'));
    }

    public function safeDown()
    {
        echo "m171204_152834_ac_spl_id_ot_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171204_152834_ac_spl_id_ot_absensi cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200111_020314_ac_terpakai_ot_cuti_saldo
 */
class m200111_020314_ac_terpakai_ot_cuti_saldo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cuti_saldo', 'terpakai', $this->integer(2)->notNull()->defaultValue(0)->after('kuota'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200111_020314_ac_terpakai_ot_cuti_saldo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200111_020314_ac_terpakai_ot_cuti_saldo cannot be reverted.\n";

        return false;
    }
    */
}

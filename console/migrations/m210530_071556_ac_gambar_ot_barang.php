<?php

use yii\db\Migration;

/**
 * Class m210530_071556_ac_gambar_ot_barang
 */
class m210530_071556_ac_gambar_ot_barang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('e_barang', 'gambar', $this->string('245')->null()->after('is_aktif'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210530_071556_ac_gambar_ot_barang cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210530_071556_ac_gambar_ot_barang cannot be reverted.\n";

        return false;
    }
    */
}

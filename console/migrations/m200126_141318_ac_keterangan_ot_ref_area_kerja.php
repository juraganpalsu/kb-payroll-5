<?php

use yii\db\Migration;

/**
 * Class m200126_141318_ac_keterangan_ot_ref_area_kerja
 */
class m200126_141318_ac_keterangan_ot_ref_area_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ref_area_kerja','keterangan', $this->string()->null()->after('name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200126_141318_ac_keterangan_ot_ref_area_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_141318_ac_keterangan_ot_ref_area_kerja cannot be reverted.\n";

        return false;
    }
    */
}

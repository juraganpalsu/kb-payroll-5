<?php

use yii\db\Migration;

/**
 * Class m210310_222622_uc_is_aktif_ot_e_barang
 */
class m210310_222622_uc_is_aktif_ot_e_barang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('e_barang', 'is_aktif', $this->integer(1)->notNull()->defaultValue(1));
        $this->alterColumn('cost_center_template', 'is_aktif', $this->integer(1)->notNull()->defaultValue(1));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210310_222622_uc_is_aktif_ot_e_barang cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210310_222622_uc_is_aktif_ot_e_barang cannot be reverted.\n";

        return false;
    }
    */
}

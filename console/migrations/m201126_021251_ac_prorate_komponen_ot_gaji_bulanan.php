<?php

use yii\db\Migration;

/**
 * Class m201126_021251_ac_prorate_komponen_ot_gaji_bulanan
 */
class m201126_021251_ac_prorate_komponen_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'prorate_komponen', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('cabang'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201126_021251_ac_prorate_komponen_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201126_021251_ac_prorate_komponen_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

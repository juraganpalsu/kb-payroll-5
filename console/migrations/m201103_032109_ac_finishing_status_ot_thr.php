<?php

use yii\db\Migration;

/**
 * Class m201103_032109_ac_finishing_status_ot_thr
 */
class m201103_032109_ac_finishing_status_ot_thr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('thr', 'finishing_status', $this->integer(2)->notNull()->defaultValue(0)->after('tahun'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201103_032109_ac_finishing_status_ot_thr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201103_032109_ac_finishing_status_ot_thr cannot be reverted.\n";

        return false;
    }
    */
}

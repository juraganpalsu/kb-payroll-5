<?php

use yii\db\Migration;

/**
 * Class m190808_151218_dr_spl_approval
 */
class m190808_151218_dr_spl_approval extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `spl_detail_approval` 
DROP FOREIGN KEY `fk_spl_detail_approval_spl_detail_approval1`;
ALTER TABLE `spl_detail_approval` 
DROP INDEX `fk_spl_detail_approval_spl_detail_approval1_idx` ;


SQL;

        $this->execute($sql);
        
        $sql = <<<SQL
ALTER TABLE `spl_approval` 
DROP FOREIGN KEY `fk_spl_approval_spl_approval1`;
ALTER TABLE `spl_approval` 
DROP INDEX `fk_spl_approval_spl_approval1_idx` ;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190808_151218_dr_spl_approval cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190808_151218_dr_spl_approval cannot be reverted.\n";

        return false;
    }
    */
}

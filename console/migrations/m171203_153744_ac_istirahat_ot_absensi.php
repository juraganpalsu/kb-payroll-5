<?php

use yii\db\Migration;

class m171203_153744_ac_istirahat_ot_absensi extends Migration
{
    public function safeUp()
    {
        $this->addColumn('absensi', 'istirahat', $this->dateTime()->null()->after('masuk'));
        $this->addColumn('absensi', 'masuk_istirahat', $this->dateTime()->null()->after('istirahat'));
    }

    public function safeDown()
    {
        echo "m171203_153744_ac_istirahat_ot_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171203_153744_ac_istirahat_ot_absensi cannot be reverted.\n";

        return false;
    }
    */
}

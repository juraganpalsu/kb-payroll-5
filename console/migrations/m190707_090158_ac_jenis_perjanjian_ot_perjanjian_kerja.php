<?php

use yii\db\Migration;

/**
 * Class m190707_090158_ac_jenis_perjanjian_ot_perjanjian_kerja
 */
class m190707_090158_ac_jenis_perjanjian_ot_perjanjian_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perjanjian_kerja', 'jenis_perjanjian', $this->integer(2)->after('id')->notNull()->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190707_090158_ac_jenis_perjanjian_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190707_090158_ac_jenis_perjanjian_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }
    */
}

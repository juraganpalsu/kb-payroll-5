<?php

use yii\db\Migration;

/**
 * Class m191120_120738_ct_training
 */
class m191120_120738_ct_training extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `training` (
  `id` VARCHAR(32) NOT NULL,
  `jenis_pelatihan` INT(2) NOT NULL DEFAULT 0 COMMENT 'INTERNAL/EKSTERNAL\n',
  `tanggal_pelaksanaan` DATE NOT NULL,
  `durasi` TIME NOT NULL,
  `nilai_tes` INT(11) ZEROFILL NOT NULL,
  `sertifikat` INT(2) NOT NULL DEFAULT 0 COMMENT 'YES/NO',
  `ref_nama_pelatihan_id` VARCHAR(32) NOT NULL,
  `ref_trainer_id` VARCHAR(32) NOT NULL,
  `ref_nama_institusi_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_training_ref_nama_pelatihan1_idx` (`ref_nama_pelatihan_id` ASC),
  INDEX `fk_training_ref_trainer1_idx` (`ref_trainer_id` ASC),
  INDEX `fk_training_ref_nama_institusi1_idx` (`ref_nama_institusi_id` ASC),
  CONSTRAINT `fk_training_ref_nama_pelatihan1`
    FOREIGN KEY (`ref_nama_pelatihan_id`)
    REFERENCES `ref_nama_pelatihan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_ref_trainer1`
    FOREIGN KEY (`ref_trainer_id`)
    REFERENCES `ref_trainer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_ref_nama_institusi1`
    FOREIGN KEY (`ref_nama_institusi_id`)
    REFERENCES `ref_nama_institusi` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `training_detail` (
  `id` VARCHAR(32) NOT NULL,
  `training_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_training_detail_training1_idx` (`training_id` ASC),
  INDEX `fk_training_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_training_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_training_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_training_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  CONSTRAINT `fk_training_detail_training1`
    FOREIGN KEY (`training_id`)
    REFERENCES `training` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_detail_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_detail_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_detail_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `training_trainer` (
  `id` VARCHAR(32) NOT NULL,
  `training_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_training_trainer_training1_idx` (`training_id` ASC),
  INDEX `fk_training_trainer_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_training_trainer_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_training_trainer_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_training_trainer_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  CONSTRAINT `fk_training_trainer_training1`
    FOREIGN KEY (`training_id`)
    REFERENCES `training` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_trainer_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_trainer_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_trainer_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_training_trainer_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191120_120738_ct_training cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191120_120738_ct_training cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191212_142827_ct_ds_akhir_kontrak
 */
class m191212_142827_ct_ds_akhir_kontrak extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `ds_akhir_kontrak` (
  `id` VARCHAR(32) NOT NULL,
  `tanggal` DATE NOT NULL,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `ds_akhir_kontrak_detail` (
  `id` VARCHAR(32) NOT NULL,
  `jumlah` INT NOT NULL DEFAULT 0,
  `perjanjian_kerja_ids` TEXT NULL,
  `ds_akhir_kontrak_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ds_akhir_kontrak_detail_ds_akhir_kontrak1_idx` (`ds_akhir_kontrak_id` ASC),
  CONSTRAINT `fk_ds_akhir_kontrak_detail_ds_akhir_kontrak1`
    FOREIGN KEY (`ds_akhir_kontrak_id`)
    REFERENCES `ds_akhir_kontrak` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191212_142827_ct_ds_akhir_kontrak cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191212_142827_ct_ds_akhir_kontrak cannot be reverted.\n";

        return false;
    }
    */
}

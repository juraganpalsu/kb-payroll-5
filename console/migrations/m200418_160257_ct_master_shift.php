<?php

use yii\db\Migration;

/**
 * Class m200418_160257_ct_master_shift
 */
class m200418_160257_ct_master_shift extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `master_uang_shift` (
  `id` VARCHAR(32) NOT NULL,
  `shift_1` INT(11) NOT NULL DEFAULT 0,
  `shift_2` INT(11) NOT NULL DEFAULT 0,
  `shift_3` INT(11) NOT NULL DEFAULT 0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200418_160257_ct_master_shift cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200418_160257_ct_master_shift cannot be reverted.\n";

        return false;
    }
    */
}

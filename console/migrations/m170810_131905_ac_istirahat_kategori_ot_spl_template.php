<?php

use yii\db\Migration;

class m170810_131905_ac_istirahat_kategori_ot_spl_template extends Migration
{
    public function safeUp()
    {
        $this->addColumn('spl_template', 'istirahat', $this->integer(2)->defaultValue(0)->after('kategori'));
    }

    public function safeDown()
    {
        echo "m170810_131905_ac_istirahat_kategori_ot_spl_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170810_131905_ac_istirahat_kategori_ot_spl_template cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191013_141414_ac_pegawai_keluarga_id_ot_pegawai_bpjs
 */
class m191013_141414_ac_pegawai_keluarga_id_ot_pegawai_bpjs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai_bpjs', 'pegawai_keluarga_id', $this->char(32)->null()->after('pegawai_id'));
        $this->addColumn('pegawai_asuransi_lain', 'pegawai_keluarga_id', $this->char(32)->null()->after('pegawai_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191013_141414_ac_pegawai_keluarga_id_ot_pegawai_bpjs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191013_141414_ac_pegawai_keluarga_id_ot_pegawai_bpjs cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210110_134113_auq_ot_surat_pegawai
 */
class m210110_134113_auq_ot_surat_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `surat_pegawai` 
ADD UNIQUE INDEX `seq_code_UNIQUE` (`seq_code` ASC);
SQL;
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210110_134113_auq_ot_surat_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210110_134113_auq_ot_surat_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

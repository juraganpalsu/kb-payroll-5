<?php

use yii\db\Migration;

/**
 * Class m201116_090942_ac_sakit_ot_appraisal
 */
class m201116_090942_ac_sakit_ot_appraisal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('appraisal', 'sakit', $this->integer(2)->after('cuti'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201116_090942_ac_sakit_ot_appraisal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201116_090942_ac_sakit_ot_appraisal cannot be reverted.\n";

        return false;
    }
    */
}

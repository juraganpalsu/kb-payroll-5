<?php

use yii\db\Migration;

/**
 * Class m190728_033812_ct_struktur_approval
 */
class m190728_033812_ct_struktur_approval extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql =<<<SQL

CREATE TABLE IF NOT EXISTS `modul_approval` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(45) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `struktur_approval` (
  `id` VARCHAR(32) NOT NULL,
  `struktur_id` INT NOT NULL,
  `struktur_atasan_id` INT NOT NULL,
  `modul_approval_id` INT NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_struktur_approval_struktur1_idx` (`struktur_id` ASC),
  INDEX `fk_struktur_approval_struktur2_idx` (`struktur_atasan_id` ASC),
  INDEX `fk_struktur_approval_modul_approval1_idx` (`modul_approval_id` ASC),
  CONSTRAINT `fk_struktur_approval_struktur1`
    FOREIGN KEY (`struktur_id`)
    REFERENCES `struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_struktur_approval_struktur2`
    FOREIGN KEY (`struktur_atasan_id`)
    REFERENCES `struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_struktur_approval_modul_approval1`
    FOREIGN KEY (`modul_approval_id`)
    REFERENCES `modul_approval` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190728_033812_ct_struktur_approval cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190728_033812_ct_struktur_approval cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200804_091403_ac_sh_sh2_sh3_ot_gaji_bulanan
 */
class m200804_091403_ac_sh_sh2_sh3_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'sh_s3', $this->integer(2)->defaultValue(0)->after('atl_s3'));
        $this->addColumn('gaji_bulanan', 'sh_s2', $this->integer(2)->defaultValue(0)->after('atl_s3'));
        $this->addColumn('gaji_bulanan', 'sh', $this->integer(2)->defaultValue(0)->after('atl'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200804_091403_ac_sh_sh2_sh3_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200804_091403_ac_sh_sh2_sh3_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

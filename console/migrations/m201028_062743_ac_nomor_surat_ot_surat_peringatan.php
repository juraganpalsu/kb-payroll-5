<?php

use yii\db\Migration;

/**
 * Class m201028_062743_ac_nomor_surat_ot_surat_peringatan
 */
class m201028_062743_ac_nomor_surat_ot_surat_peringatan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('surat_peringatan', 'seq_code', $this->integer(11)->notNull()->defaultValue(0)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201028_062743_ac_nomor_surat_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201028_062743_ac_nomor_surat_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }
    */
}

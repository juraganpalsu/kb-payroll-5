<?php

use yii\db\Migration;

/**
 * Class m201022_225011_ac_pegawais_ids_ot_ds_absensi
 */
class m201022_225011_ac_pegawais_ids_ot_ds_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ds_absensi', 'pegawai_ids', $this->text()->null()->after('jumlah'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201022_225011_ac_pegawais_ids_ot_ds_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201022_225011_ac_pegawais_ids_ot_ds_absensi cannot be reverted.\n";

        return false;
    }
    */
}

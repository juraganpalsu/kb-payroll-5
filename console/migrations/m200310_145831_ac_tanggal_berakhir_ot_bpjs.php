<?php

use yii\db\Migration;

/**
 * Class m200310_145831_ac_tanggal_berakhir_ot_bpjs
 */
class m200310_145831_ac_tanggal_berakhir_ot_bpjs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai_bpjs', 'tanggal_berakhir', $this->date()->null()->after('tanggal_kepesertaan'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200310_145831_ac_tanggal_berakhir_ot_bpjs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200310_145831_ac_tanggal_berakhir_ot_bpjs cannot be reverted.\n";

        return false;
    }
    */
}

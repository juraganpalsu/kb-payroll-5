<?php

use yii\db\Migration;

/**
 * Class m210309_005847_is_aktif_ot_cost_center_template
 */
class m210309_005847_is_aktif_ot_cost_center_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cost_center_template', 'is_aktif', $this->integer(1)->notNull()->defaultValue(0)->after('keterangan'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210309_005847_is_aktif_ot_cost_center_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210309_005847_is_aktif_ot_cost_center_template cannot be reverted.\n";

        return false;
    }
    */
}

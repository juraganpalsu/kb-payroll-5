<?php

use yii\db\Migration;

/**
 * Class m200709_161847_ac_libur_hadri_ot_gaji_bulanan
 */
class m200709_161847_ac_libur_hadri_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'atl_s3', $this->integer(2)->defaultValue(0)->after('jumlah_telat'));
        $this->addColumn('gaji_bulanan', 'atl_s2', $this->integer(2)->defaultValue(0)->after('jumlah_telat'));
        $this->addColumn('gaji_bulanan', 'atl_s1', $this->integer(2)->defaultValue(0)->after('jumlah_telat'));
        $this->addColumn('gaji_bulanan', 'libur_hadir', $this->integer(2)->defaultValue(0)->after('jumlah_telat'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200709_161847_ac_libur_hadri_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200709_161847_ac_libur_hadri_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

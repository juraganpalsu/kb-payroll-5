<?php

use yii\db\Migration;

class m170807_151548_ac_sistem_kerja_ot_pegawai extends Migration
{
    public function safeUp()
    {
        $this->addColumn('pegawai', 'sistem_kerja', $this->integer(1)->defaultValue(1)->after('nama'));
    }

    public function safeDown()
    {
        echo "m170807_151548_ac_sistem_kerja_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170807_151548_ac_sistem_kerja_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

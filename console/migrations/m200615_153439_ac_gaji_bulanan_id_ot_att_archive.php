<?php

use yii\db\Migration;

/**
 * Class m200615_153439_ac_gaji_bulanan_id_ot_att_archive
 */
class m200615_153439_ac_gaji_bulanan_id_ot_att_archive extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('attendance_archive', 'gaji_bulanan_id', $this->string(32)->notNull()->after('lock'));
        $this->addForeignKey('attendance_archive_to_gaji_bulanan', 'attendance_archive','gaji_bulanan_id', 'gaji_bulanan', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200615_153439_ac_gaji_bulanan_id_ot_att_archive cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200615_153439_ac_gaji_bulanan_id_ot_att_archive cannot be reverted.\n";

        return false;
    }
    */
}

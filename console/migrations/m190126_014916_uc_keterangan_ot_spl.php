<?php

use yii\db\Migration;

/**
 * Class m190126_014916_uc_keterangan_ot_spl
 */
class m190126_014916_uc_keterangan_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `spl` 
CHANGE COLUMN `keterangan` `keterangan` TEXT NULL DEFAULT NULL ;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190126_014916_uc_keterangan_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190126_014916_uc_keterangan_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

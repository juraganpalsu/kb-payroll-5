<?php

use yii\db\Migration;

/**
 * Class m200830_142808_ac_status_lunas_ot_pinjaman
 */
class m200830_142808_ac_status_lunas_ot_pinjaman extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pinjaman', 'status_lunas', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('angsuran'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200830_142808_ac_status_lunas_ot_pinjaman cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200830_142808_ac_status_lunas_ot_pinjaman cannot be reverted.\n";

        return false;
    }
    */
}

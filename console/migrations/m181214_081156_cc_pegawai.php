<?php

use yii\db\Migration;

/**
 * Class m181214_081156_cc_pegawai
 */
class m181214_081156_cc_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$sql = <<<SQL
ALTER TABLE `pegawai` 
CHANGE COLUMN `unit` `unit` INT(11) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `divisi` `divisi` INT(11) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `jabatan` `jabatan` INT(11) NOT NULL DEFAULT '0' ;

SQL;

$this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181214_081156_cc_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181214_081156_cc_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

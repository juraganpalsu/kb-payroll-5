<?php

use yii\db\Migration;

/**
 * Class m191113_161357_ac_gm_ot_surat_peringatan
 */
class m191113_161357_ac_gm_ot_surat_peringatan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('surat_peringatan', 'gmp', $this->integer(1)->notNull()->defaultValue(0)->after('akhir_berlaku'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191113_161357_ac_gm_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191113_161357_ac_gm_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210330_152701_cc_wsc_detail
 */
class m210330_152701_cc_wsc_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `wsc_detail` 
CHANGE COLUMN `jam_mulai` `jam_mulai` TIME NULL DEFAULT NULL ,
CHANGE COLUMN `jam_selesai` `jam_selesai` TIME NULL DEFAULT NULL ;
SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210330_152701_cc_wsc_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210330_152701_cc_wsc_detail cannot be reverted.\n";

        return false;
    }
    */
}

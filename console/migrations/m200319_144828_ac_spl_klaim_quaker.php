<?php

use yii\db\Migration;

/**
 * Class m200319_144828_ac_spl_klaim_quaker
 */
class m200319_144828_ac_spl_klaim_quaker extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('spl', 'klaim_quaker', $this->integer(1)->notNull()->defaultValue(0)->after('tanggal'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200319_144828_ac_spl_klaim_quaker cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200319_144828_ac_spl_klaim_quaker cannot be reverted.\n";

        return false;
    }
    */
}

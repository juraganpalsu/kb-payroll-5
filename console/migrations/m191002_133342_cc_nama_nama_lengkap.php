<?php

use yii\db\Migration;

/**
 * Class m191002_133342_cc_nama_nama_lengkap
 */
class m191002_133342_cc_nama_nama_lengkap extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $sql = <<<SQL
ALTER TABLE `pegawai` 
CHANGE COLUMN `nama` `nama_lengkap` VARCHAR(225) NOT NULL ;
SQL;
        $this->execute($sql);

        $sql0 = <<<SQL
ALTER TABLE `pegawai` 
DROP COLUMN `lock`,
DROP COLUMN `deleted_by`,
DROP COLUMN `deleted_at`,
DROP COLUMN `updated_by`,
DROP COLUMN `updated_at`,
DROP COLUMN `created_by`,
DROP COLUMN `created_at`;

SQL;
        $this->execute($sql0);


        $sql1 = <<<SQL
ALTER TABLE `pegawai` 
ADD COLUMN `nama_panggilan` VARCHAR(45) NOT NULL,
ADD COLUMN `jenis_kelamin` TINYINT(1) NOT NULL,
ADD COLUMN `tempat_lahir` VARCHAR(45) NOT NULL,
ADD COLUMN `tanggal_lahir` DATE NULL DEFAULT NULL,
ADD COLUMN `status_pernikahan` TINYINT(2) NOT NULL,
ADD COLUMN `kewarganegaraan` VARCHAR(45) NOT NULL,
ADD COLUMN `agama` TINYINT(2) NOT NULL,
ADD COLUMN `nomor_ktp` VARCHAR(16) NOT NULL,
ADD COLUMN `nomor_kk` VARCHAR(45) NULL,
ADD COLUMN `no_telp` VARCHAR(13) NULL,
ADD COLUMN `no_handphone` VARCHAR(13) NOT NULL,
ADD COLUMN `alamat_email` VARCHAR(225) NULL,
ADD COLUMN `catatan` TEXT NULL,
ADD COLUMN `created_at` DATETIME NULL DEFAULT NULL,
ADD COLUMN `created_by` INT(11) NULL DEFAULT '0',
ADD COLUMN `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
ADD COLUMN `created_by_struktur` INT(11) NULL DEFAULT 0,
ADD COLUMN `updated_at` DATETIME NULL DEFAULT NULL,
ADD COLUMN `updated_by` INT(11) NULL DEFAULT '0',
ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL,
ADD COLUMN `deleted_by` INT(11) NULL DEFAULT '0',
ADD COLUMN `lock` BIGINT(20) NULL DEFAULT '0';

SQL;

        $this->execute($sql1);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191002_133342_cc_nama_nama_lengkap cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_133342_cc_nama_nama_lengkap cannot be reverted.\n";

        return false;
    }
    */
}

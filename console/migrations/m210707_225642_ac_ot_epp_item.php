<?php

use yii\db\Migration;

/**
 * Class m210707_225642_ac_ot_epp_item
 */
class m210707_225642_ac_ot_epp_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `e_ppb_item` 
ADD COLUMN `id` VARCHAR(36) NOT NULL AFTER `lock`,
ADD PRIMARY KEY (`id`);
SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210707_225642_ac_ot_epp_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210707_225642_ac_ot_epp_item cannot be reverted.\n";

        return false;
    }
    */
}

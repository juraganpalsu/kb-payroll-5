<?php

use yii\db\Migration;

/**
 * Class m210311_144158_ac_seq_code_ot_permintaan
 */
class m210311_144158_ac_seq_code_ot_permintaan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('e_permintaan', 'seq_code', $this->integer(11)->notNull()->unique()->after('id')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210311_144158_ac_seq_code_ot_permintaan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210311_144158_ac_seq_code_ot_permintaan cannot be reverted.\n";

        return false;
    }
    */
}

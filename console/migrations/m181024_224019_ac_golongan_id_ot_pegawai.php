<?php

use yii\db\Migration;

/**
 * Class m181024_224019_ac_golongan_id_ot_pegawai
 */
class m181024_224019_ac_golongan_id_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('pegawai', 'golongan_id', $this->integer(11)->notNull()->defaultValue(0)->after('jabatan'));
//        $this->addForeignKey('fk_pegawai_golongan1', 'pegawai', 'pegawai_id', 'golongan', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181024_224019_ac_golongan_id_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_224019_ac_golongan_id_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

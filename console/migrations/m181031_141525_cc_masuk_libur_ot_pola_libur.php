<?php

use yii\db\Migration;

/**
 * Class m181031_141525_cc_masuk_libur_ot_pola_libur
 */
class m181031_141525_cc_masuk_libur_ot_pola_libur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL

ALTER TABLE `pola_jadwal_libur` 
CHANGE COLUMN `masuk` `masuk` TINYINT(1) NULL COMMENT 'Jumlah hari masuk kerja' ,
CHANGE COLUMN `libur` `libur` TINYINT(1) NULL DEFAULT 0 COMMENT 'Jumlah hari libur setelah masuk kerja' ;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181031_141525_cc_masuk_libur_ot_pola_libur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181031_141525_cc_masuk_libur_ot_pola_libur cannot be reverted.\n";

        return false;
    }
    */
}

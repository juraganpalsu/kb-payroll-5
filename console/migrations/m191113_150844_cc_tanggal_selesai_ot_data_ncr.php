<?php

use yii\db\Migration;

/**
 * Class m191113_150844_cc_tanggal_selesai_ot_data_ncr
 */
class m191113_150844_cc_tanggal_selesai_ot_data_ncr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
ALTER TABLE `data_ncr` 
CHANGE COLUMN `tanggal_selesai` `tanggal_selesai` DATE NULL DEFAULT NULL ,
CHANGE COLUMN `keterangan_penyelesaian` `keterangan_penyelesaian` TEXT NULL ;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191113_150844_cc_tanggal_selesai_ot_data_ncr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191113_150844_cc_tanggal_selesai_ot_data_ncr cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210627_150755_ac_notifikasi_ot_e_gudang_pegawai
 */
class m210627_150755_ac_notifikasi_ot_e_gudang_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('e_gudang_pegawai', 'notifikasi', $this->integer(2)->defaultValue(0)->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210627_150755_ac_notifikasi_ot_e_gudang_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210627_150755_ac_notifikasi_ot_e_gudang_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

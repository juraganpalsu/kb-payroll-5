<?php

use yii\db\Migration;

/**
 * Class m200818_031003_ac_pot_keterlambatan_cabang_ot_master_data_default
 */
class m200818_031003_ac_pot_keterlambatan_cabang_ot_master_data_default extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('master_data_default', 'pot_keterlambatan', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('tunjangan_anak'));
        $this->addColumn('master_data_default', 'cabang', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('tunjangan_anak'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200818_031003_ac_pot_keterlambatan_cabang_ot_master_data_default cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200818_031003_ac_pot_keterlambatan_cabang_ot_master_data_default cannot be reverted.\n";

        return false;
    }
    */
}

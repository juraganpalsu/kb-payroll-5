<?php

use yii\db\Migration;

/**
 * Class m210928_011803_ac_jumlah_bulan_ot_gaji_pph21
 */
class m210928_011803_ac_jumlah_bulan_ot_gaji_pph21 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_pph21', 'jumlah_bulan', $this->integer(2)->notNull()->defaultValue(0)->after('pkp'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210928_011803_ac_jumlah_bulan_ot_gaji_pph21 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210928_011803_ac_jumlah_bulan_ot_gaji_pph21 cannot be reverted.\n";

        return false;
    }
    */
}

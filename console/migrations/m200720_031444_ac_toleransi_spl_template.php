<?php

use yii\db\Migration;

/**
 * Class m200720_031444_ac_toleransi_spl_template
 */
class m200720_031444_ac_toleransi_spl_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('spl_template', 'toleransi_masuk', $this->integer(3)->null()->defaultValue(0)->after('masuk'));
        $this->addColumn('spl_template', 'toleransi_pulang', $this->integer(3)->null()->defaultValue(0)->after('pulang'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_031444_ac_toleransi_spl_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_031444_ac_toleransi_spl_template cannot be reverted.\n";

        return false;
    }
    */
}

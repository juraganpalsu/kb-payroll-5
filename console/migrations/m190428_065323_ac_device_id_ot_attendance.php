<?php

use yii\db\Migration;

/**
 * Class m190428_065323_ac_device_id_ot_attendance
 */
class m190428_065323_ac_device_id_ot_attendance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('attendance', 'device_id', $this->char(45)->notNull()->defaultValue(0)->after('uploaded'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190428_065323_ac_device_id_ot_attendance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190428_065323_ac_device_id_ot_attendance cannot be reverted.\n";

        return false;
    }
    */
}

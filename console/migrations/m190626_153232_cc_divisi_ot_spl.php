<?php

use yii\db\Migration;

/**
 * Class m190626_153232_cc_divisi_ot_spl
 */
class m190626_153232_cc_divisi_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `spl` 
CHANGE COLUMN `divisi` `divisi` INT(3) NOT NULL DEFAULT '0' ;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190626_153232_cc_divisi_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190626_153232_cc_divisi_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

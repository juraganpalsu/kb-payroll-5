<?php

use yii\db\Migration;

/**
 * Class m200621_150832_ct_ptkp_setting
 */
class m200621_150832_ct_ptkp_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `ptpk_setting` (
  `id` VARCHAR(32) NOT NULL,
  `tahun` INT(4) NOT NULL DEFAULT 0,
  `tk` INT(11) NOT NULL DEFAULT 0,
  `k0` INT(11) NOT NULL DEFAULT 0,
  `k1` INT(11) NOT NULL DEFAULT 0,
  `k2` INT(11) NOT NULL DEFAULT 0,
  `k3` INT(11) NOT NULL DEFAULT 0,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
SQL;
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200621_150832_ct_ptkp_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200621_150832_ct_ptkp_setting cannot be reverted.\n";

        return false;
    }
    */
}

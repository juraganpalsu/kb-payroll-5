<?php

use yii\db\Migration;

/**
 * Class m201019_143240_ac_nomor_ot_surat_pegawai
 */
class m201019_143240_ac_nomor_ot_surat_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('surat_pegawai', 'seq_code', $this->integer(11)->notNull()->defaultValue(0)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201019_143240_ac_nomor_ot_surat_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201019_143240_ac_nomor_ot_surat_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190714_155919_ct_attendance_beatroute
 */
class m190714_155919_ct_attendance_beatroute extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `attendance_beatroute` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `emp_role_id` INT(11) NULL DEFAULT 0,
  `user_br_id` INT(11) NOT NULL DEFAULT 0,
  `date` DATE NOT NULL,
  `duration` TIME NULL,
  `status` VARCHAR(5) NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190714_155919_ct_attendance_beatroute cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190714_155919_ct_attendance_beatroute cannot be reverted.\n";

        return false;
    }
    */
}

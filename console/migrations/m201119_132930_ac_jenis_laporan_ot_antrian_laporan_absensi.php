<?php

use yii\db\Migration;

/**
 * Class m201119_132930_ac_jenis_laporan_ot_antrian_laporan_absensi
 */
class m201119_132930_ac_jenis_laporan_ot_antrian_laporan_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('antrian_laporan_absensi', 'jenis_laporan', $this->integer(1)->notNull()->defaultValue(1)->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201119_132930_ac_jenis_laporan_ot_antrian_laporan_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201119_132930_ac_jenis_laporan_ot_antrian_laporan_absensi cannot be reverted.\n";

        return false;
    }
    */
}

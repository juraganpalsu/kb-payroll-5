<?php

use yii\db\Migration;

/**
 * Class m181014_114220_ac_unit_divisi_jabatan_ot_spl
 */
class m181014_114220_ac_unit_divisi_jabatan_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('spl', 'unit', $this->tinyInteger(1)->notNull()->after('spl_template_id')->defaultValue(0));
        $this->addColumn('spl', 'divisi', $this->tinyInteger(2)->notNull()->after('unit')->defaultValue(0));
        $this->addColumn('spl', 'jabatan', $this->tinyInteger(1)->notNull()->after('divisi')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181014_114220_ac_unit_divisi_jabatan_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181014_114220_ac_unit_divisi_jabatan_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m181213_130205_cc_idfp_ot_pegawai
 */
class m181213_130205_cc_idfp_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `pegawai` 
CHANGE COLUMN `idfp` `idfp` VARCHAR(11) NOT NULL ;
SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181213_130205_cc_idfp_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181213_130205_cc_idfp_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

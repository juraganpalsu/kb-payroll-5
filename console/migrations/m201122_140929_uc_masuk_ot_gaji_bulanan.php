<?php

use yii\db\Migration;

/**
 * Class m201122_140929_uc_masuk_ot_gaji_bulanan
 */
class m201122_140929_uc_masuk_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('gaji_bulanan', 'masuk', $this->float()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201122_140929_uc_masuk_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201122_140929_uc_masuk_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m211016_163856_ac_last_status_ot_payroll_borongan_detail
 */
class m211016_163856_ac_last_status_ot_payroll_borongan_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payroll_borongan_detail', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211016_163856_ac_last_status_ot_payroll_borongan_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211016_163856_ac_last_status_ot_payroll_borongan_detail cannot be reverted.\n";

        return false;
    }
    */
}

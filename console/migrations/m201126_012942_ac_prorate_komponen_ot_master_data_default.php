<?php

use yii\db\Migration;

/**
 * Class m201126_012942_ac_prorate_komponen_ot_master_data_default
 */
class m201126_012942_ac_prorate_komponen_ot_master_data_default extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('master_data_default', 'prorate_komponen', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('tunjangan_anak'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201126_012942_ac_prorate_komponen_ot_master_data_default cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201126_012942_ac_prorate_komponen_ot_master_data_default cannot be reverted.\n";

        return false;
    }
    */
}

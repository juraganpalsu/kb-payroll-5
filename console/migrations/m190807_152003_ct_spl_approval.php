<?php

use yii\db\Migration;

/**
 * Class m190807_152003_ct_spl_approval
 */
class m190807_152003_ct_spl_approval extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('spl_detail_approval');

        $sql =<<<SQL

CREATE TABLE IF NOT EXISTS `spl_detail_approval` (
  `id` VARCHAR(32) NOT NULL,
  `tipe` INT(2) NOT NULL DEFAULT 0,
  `tanggal` DATETIME NULL,
  `komentar` TEXT NULL,
  `level` INT NOT NULL DEFAULT 0,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `spl_detail_id` VARCHAR(32) NOT NULL,
  `spl_detail_approval_id` VARCHAR(32) NULL DEFAULT 0,
  `notifikasi` INT(1) NOT NULL DEFAULT 1,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_spl_detail_approval_spl_detail1_idx` (`spl_detail_id` ASC),
  INDEX `fk_spl_detail_approval_spl_detail_approval1_idx` (`spl_detail_approval_id` ASC),
  INDEX `fk_spl_detail_approval_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_spl_detail_approval_spl_detail1`
    FOREIGN KEY (`spl_detail_id`)
    REFERENCES `spl_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_spl_detail_approval_spl_detail_approval1`
    FOREIGN KEY (`spl_detail_approval_id`)
    REFERENCES `spl_detail_approval` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_spl_detail_approval_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `spl_approval` (
  `id` VARCHAR(32) NOT NULL,
  `tipe` INT(2) NOT NULL DEFAULT 0,
  `tanggal` DATETIME NULL,
  `komentar` TEXT NULL,
  `level` INT NOT NULL DEFAULT 0,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `spl_id` VARCHAR(32) NOT NULL,
  `spl_approval_id` VARCHAR(32) NULL DEFAULT 0,
  `notifikasi` INT(1) NOT NULL DEFAULT 1,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_spl_approval_spl1_idx` (`spl_id` ASC),
  INDEX `fk_spl_approval_spl_approval1_idx` (`spl_approval_id` ASC),
  CONSTRAINT `fk_spl_approval_spl1`
    FOREIGN KEY (`spl_id`)
    REFERENCES `spl` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_spl_approval_spl_approval1`
    FOREIGN KEY (`spl_approval_id`)
    REFERENCES `spl_approval` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190807_152003_ct_spl_approval cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190807_152003_ct_spl_approval cannot be reverted.\n";

        return false;
    }
    */
}

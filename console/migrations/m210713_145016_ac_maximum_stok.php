<?php

use yii\db\Migration;

/**
 * Class m210713_145016_ac_maximum_stok
 */
class m210713_145016_ac_maximum_stok extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('e_barang', 'maksimum_stok', $this->decimal(12, 2)->notNull()->defaultValue(0)->after('minimum_stok'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210713_145016_ac_maximum_stok cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210713_145016_ac_maximum_stok cannot be reverted.\n";

        return false;
    }
    */
}

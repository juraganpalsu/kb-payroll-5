<?php

use yii\db\Migration;

/**
 * Class m201122_142618_cc_komentar_ot_appraisal_detail
 */
class m201122_142618_cc_komentar_ot_appraisal_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `appraisal_detail` 
CHANGE COLUMN `komentar` `komentar` TEXT NULL ;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201122_142618_cc_komentar_ot_appraisal_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201122_142618_cc_komentar_ot_appraisal_detail cannot be reverted.\n";

        return false;
    }
    */
}

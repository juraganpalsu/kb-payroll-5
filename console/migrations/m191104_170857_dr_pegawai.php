<?php

use yii\db\Migration;

/**
 * Class m191104_170857_dr_pegawai
 */
class m191104_170857_dr_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `inventaris` 
DROP FOREIGN KEY `fk_inventaris_perjanjian_kerja1`,
DROP FOREIGN KEY `fk_inventaris_pegawai_struktur1`,
DROP FOREIGN KEY `fk_inventaris_pegawai_golongan1`;
ALTER TABLE `inventaris` 
DROP INDEX `fk_inventaris_pegawai_struktur1_idx` ,
DROP INDEX `fk_inventaris_pegawai_golongan1_idx` ,
DROP INDEX `fk_inventaris_perjanjian_kerja1_idx` ;


SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191104_170857_dr_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191104_170857_dr_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190916_160947_dr_struktur
 */
class m190916_160947_dr_struktur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `corporate` 
DROP FOREIGN KEY `fk_corporate_struktur1`;
ALTER TABLE `corporate` 
DROP COLUMN `struktur_id`,
DROP INDEX `fk_corporate_struktur1_idx` ;

ALTER TABLE `perusahaan` 
DROP FOREIGN KEY `fk_perusahaan_struktur1`;
ALTER TABLE `perusahaan` 
DROP COLUMN `struktur_id`,
DROP INDEX `fk_perusahaan_struktur1_idx` ;

ALTER TABLE `direktur` 
DROP FOREIGN KEY `fk_direktur_struktur1`;
ALTER TABLE `direktur` 
DROP COLUMN `struktur_id`,
DROP INDEX `fk_direktur_struktur1_idx` ;

ALTER TABLE `divisi` 
DROP FOREIGN KEY `fk_divisi_struktur1`;
ALTER TABLE `divisi` 
DROP COLUMN `struktur_id`,
DROP INDEX `fk_divisi_struktur1_idx` ;

ALTER TABLE `departemen` 
DROP FOREIGN KEY `fk_departemen_struktur1`;
ALTER TABLE `departemen` 
DROP COLUMN `struktur_id`,
DROP INDEX `fk_departemen_struktur1_idx` ;

ALTER TABLE `section` 
DROP FOREIGN KEY `fk_section_struktur1`;
ALTER TABLE `section` 
DROP COLUMN `struktur_id`,
DROP INDEX `fk_section_struktur1_idx` ;

SQL;

        $this->execute($sql);

        $this->addColumn('struktur', 'corporate_id', $this->char(32)->defaultValue(0)->notNull()->after('tipe'));
        $this->addColumn('struktur', 'perusahaan_id', $this->char(32)->defaultValue(0)->notNull()->after('corporate_id'));
        $this->addColumn('struktur', 'direktur_id', $this->char(32)->defaultValue(0)->notNull()->after('perusahaan_id'));
        $this->addColumn('struktur', 'divisi_id', $this->char(32)->defaultValue(0)->notNull()->after('direktur_id'));
        $this->addColumn('struktur', 'departemen_id', $this->char(32)->defaultValue(0)->notNull()->after('divisi_id'));
        $this->addColumn('struktur', 'section_id', $this->char(32)->defaultValue(0)->notNull()->after('departemen_id'));


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190916_160947_dr_struktur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190916_160947_dr_struktur cannot be reverted.\n";

        return false;
    }
    */
}

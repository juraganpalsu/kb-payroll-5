<?php

use yii\db\Migration;

/**
 * Class m200511_161629_dr_ot_thrdetail
 */
class m200511_161629_dr_ot_thrdetail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `thr_detail` 
DROP FOREIGN KEY `fk_thr_detail_perjanjian_kerja1`,
DROP FOREIGN KEY `fk_thr_detail_pegawai_struktur1`,
DROP FOREIGN KEY `fk_thr_detail_pegawai_golongan1`,
DROP FOREIGN KEY `fk_thr_detail_p_tunjangan_loyalitas_config1`,
DROP FOREIGN KEY `fk_thr_detail_p_tunjangan_jabatan1`,
DROP FOREIGN KEY `fk_thr_detail_p_tunjangan_anak_config1`,
DROP FOREIGN KEY `fk_thr_detail_p_tunjagan_lain_tetap1`;
ALTER TABLE `thr_detail` 
DROP INDEX `fk_thr_detail_p_tunjangan_jabatan1_idx` ,
DROP INDEX `fk_thr_detail_p_tunjagan_lain_tetap1_idx` ,
DROP INDEX `fk_thr_detail_p_tunjangan_anak_config1_idx` ,
DROP INDEX `fk_thr_detail_p_tunjangan_loyalitas_config1_idx` ,
DROP INDEX `fk_thr_detail_pegawai_struktur1_idx` ,
DROP INDEX `fk_thr_detail_pegawai_golongan1_idx` ,
DROP INDEX `fk_thr_detail_perjanjian_kerja1_idx` ;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200511_161629_dr_ot_thrdetail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200511_161629_dr_ot_thrdetail cannot be reverted.\n";

        return false;
    }
    */
}

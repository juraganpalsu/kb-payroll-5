<?php

use yii\db\Migration;

/**
 * Class m201001_095442_ac_keterangan_ot_pinjaman
 */
class m201001_095442_ac_keterangan_ot_pinjaman extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pinjaman', 'keterangan', $this->text()->after('status_lunas'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201001_095442_ac_keterangan_ot_pinjaman cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201001_095442_ac_keterangan_ot_pinjaman cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200630_043113_ac_izin_ot_gaji_bulanan
 */
class m200630_043113_ac_izin_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'wfh_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'sd_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'ith_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'idl_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'off_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'cm_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'is_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'tl_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'dispensasi_id', $this->char(32)->after('undifined'));
        $this->addColumn('gaji_bulanan', 'wfh', $this->integer(2)->after('wfh_id'));
        $this->addColumn('gaji_bulanan', 'sd', $this->integer(2)->after('sd_id'));
        $this->addColumn('gaji_bulanan', 'ith', $this->integer(2)->after('ith_id'));
        $this->addColumn('gaji_bulanan', 'idl', $this->integer(2)->after('idl_id'));
        $this->addColumn('gaji_bulanan', 'off', $this->integer(2)->after('off_id'));
        $this->addColumn('gaji_bulanan', 'cm', $this->integer(2)->after('cm_id'));
        $this->addColumn('gaji_bulanan', 'is', $this->integer(2)->after('is_id'));
        $this->addColumn('gaji_bulanan', 'dispensasi', $this->integer(2)->after('tl_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200630_043113_ac_izin_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200630_043113_ac_izin_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

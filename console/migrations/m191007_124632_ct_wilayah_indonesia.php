<?php

use yii\db\Migration;

/**
 * Class m191007_124632_ct_wilayah_indonesia
 */
class m191007_124632_ct_wilayah_indonesia extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `indonesia_provinsi` (
  `id` INT NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `indonesia_kabupaten_kota` (
  `id` INT NOT NULL,
  `provinsi_id` INT NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_kabupaten_kota_provinsi1_idx` (`provinsi_id` ASC),
  CONSTRAINT `fk_kabupaten_kota_provinsi1`
    FOREIGN KEY (`provinsi_id`)
    REFERENCES `indonesia_provinsi` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `indonesia_kecamatan` (
  `id` INT NOT NULL,
  `kabupaten_kota_id` INT NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_kecamatan_kabupaten_kota1_idx` (`kabupaten_kota_id` ASC),
  CONSTRAINT `fk_kecamatan_kabupaten_kota1`
    FOREIGN KEY (`kabupaten_kota_id`)
    REFERENCES `indonesia_kabupaten_kota` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `indonesia_desa` (
  `id` BIGINT(16) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `kecamatan_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_desa_kecamatan1_idx` (`kecamatan_id` ASC),
  CONSTRAINT `fk_desa_kecamatan1`
    FOREIGN KEY (`kecamatan_id`)
    REFERENCES `indonesia_kecamatan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191007_124632_ct_wilayah_indonesia cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191007_124632_ct_wilayah_indonesia cannot be reverted.\n";

        return false;
    }
    */
}

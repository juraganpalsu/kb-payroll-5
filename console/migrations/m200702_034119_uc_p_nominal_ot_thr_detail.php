<?php

use yii\db\Migration;

/**
 * Class m200702_034119_uc_p_nominal_ot_thr_detail
 */
class m200702_034119_uc_p_nominal_ot_thr_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('thr_detail', 'p_gaji_pokok_nominal', $this->integer(11)->notNull()->defaultValue(0));
        $this->alterColumn('thr_detail', 'p_tunjangan_loyalitas_config_nominal', $this->integer(11)->notNull()->defaultValue(0));
        $this->alterColumn('thr_detail', 'p_tunjangan_anak_config_nominal', $this->integer(11)->notNull()->defaultValue(0));
        $this->alterColumn('thr_detail', 'p_tunjangan_lain_tetap_nominal', $this->integer(11)->notNull()->defaultValue(0));
        $this->alterColumn('thr_detail', 'p_tunjangan_jabatan_nominal', $this->integer(11)->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200702_034119_uc_p_nominal_ot_thr_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200702_034119_uc_p_nominal_ot_thr_detail cannot be reverted.\n";

        return false;
    }
    */
}

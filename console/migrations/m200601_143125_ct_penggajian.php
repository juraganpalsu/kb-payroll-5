<?php

use yii\db\Migration;

/**
 * Class m200601_143125_ct_penggajian
 */
class m200601_143125_ct_penggajian extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `p_sewa_rumah` 
ADD PRIMARY KEY (`id`);

CREATE TABLE IF NOT EXISTS `proses_gaji` (
  `id` VARCHAR(32) NOT NULL,
  `payroll_periode_id` VARCHAR(32) NOT NULL,
  `golongan_id` INT NOT NULL,
  `bisnis_unit` INT(2) NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_proses_gaji_payroll_periode1_idx` (`payroll_periode_id` ASC),
  INDEX `fk_proses_gaji_golongan1_idx` (`golongan_id` ASC),
  CONSTRAINT `fk_proses_gaji_payroll_periode1`
    FOREIGN KEY (`payroll_periode_id`)
    REFERENCES `payroll_periode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_proses_gaji_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `gaji_bulanan` (
  `id` VARCHAR(32) NOT NULL,
  `nama_lengkap` VARCHAR(225) NOT NULL,
  `bisnis_unit_s` VARCHAR(225) NOT NULL,
  `golongan_s` VARCHAR(225) NOT NULL,
  `jenis_kontrak_s` VARCHAR(225) NOT NULL,
  `struktur_s` VARCHAR(225) NOT NULL,
  `masuk` INT(2) NOT NULL DEFAULT 0,
  `alfa` INT(2) NOT NULL DEFAULT 0,
  `ijin` INT(2) NOT NULL DEFAULT 0,
  `libur` INT(2) NOT NULL DEFAULT 0,
  `cuti` INT(2) NOT NULL DEFAULT 0,
  `undifined` INT(2) NOT NULL DEFAULT 0,
  `jam_lembur` FLOAT(3,2) NOT NULL DEFAULT 0,
  `s2` INT(2) NOT NULL DEFAULT 0,
  `s3` INT(2) NOT NULL DEFAULT 0,
  `uang_lembur` FLOAT(11,2) NOT NULL DEFAULT 0,
  `uml` FLOAT(11,2) NOT NULL DEFAULT 0,
  `p_gaji_pokok_id` VARCHAR(32) NULL,
  `p_gaji_pokok` INT(11) NOT NULL DEFAULT 0,
  `p_admin_bank_id` VARCHAR(32) NULL,
  `p_admin_bank` INT(11) NOT NULL DEFAULT 0,
  `pinjaman_angsuran_id` VARCHAR(32) NULL,
  `pinjaman_nominal` INT(11) NOT NULL DEFAULT 0,
  `angsuran_ke` INT(2) NOT NULL DEFAULT 0,
  `p_insentif_tetap_id` VARCHAR(32) NULL,
  `p_premi_hadir_id` VARCHAR(32) NULL,
  `p_sewa_mobil_id` VARCHAR(32) NULL,
  `p_sewa_motor_id` VARCHAR(32) NULL,
  `p_sewa_rumah_id` VARCHAR(32) NULL,
  `p_tunjagan_lain_tetap_id` VARCHAR(32) NULL,
  `p_tunjangan_anak_config_id` VARCHAR(32) NULL,
  `p_tunjangan_jabatan_id` VARCHAR(32) NULL,
  `p_tunjangan_kost_id` VARCHAR(32) NULL,
  `p_tunjangan_loyalitas_config_id` VARCHAR(32) NULL,
  `p_tunjangan_pulsa_id` VARCHAR(32) NULL,
  `p_uang_makan_id` VARCHAR(32) NULL,
  `p_uang_transport_id` VARCHAR(32) NULL,
  `p_insentif_tetap` INT(11) NOT NULL DEFAULT 0,
  `p_premi_hadir` INT(11) NOT NULL DEFAULT 0,
  `p_sewa_mobil` INT(11) NOT NULL DEFAULT 0,
  `p_sewa_motor` INT(11) NOT NULL DEFAULT 0,
  `p_sewa_rumah` INT(11) NOT NULL DEFAULT 0,
  `p_tunjagan_lain_tetap` INT(11) NOT NULL DEFAULT 0,
  `p_tunjangan_anak` INT(11) NOT NULL DEFAULT 0,
  `p_tunjangan_jabatan` INT(11) NOT NULL DEFAULT 0,
  `p_tunjangan_kost` INT(11) NOT NULL DEFAULT 0,
  `p_tunjangan_loyalitas` INT(11) NOT NULL DEFAULT 0,
  `p_tunjangan_pulsa` INT(11) NOT NULL DEFAULT 0,
  `p_uang_makan` INT(11) NOT NULL DEFAULT 0,
  `p_uang_transport` INT(11) NOT NULL DEFAULT 0,
  `hari_kerja_aktif` INT(2) NOT NULL DEFAULT 0,
  `pegawai_bpjs_id` VARCHAR(32) NULL,
  `pegawai_bpjs` INT(11) NOT NULL DEFAULT 0,
  `pegawai_bank_id` VARCHAR(32) NOT NULL,
  `pegawai_bank_nama` VARCHAR(225) NULL,
  `pegawai_bank_nomor` BIGINT(16) NULL DEFAULT 0,
  `proses_gaji_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_gaji_bulanan_p_gaji_pokok1_idx` (`p_gaji_pokok_id` ASC),
  INDEX `fk_gaji_bulanan_p_admin_bank1_idx` (`p_admin_bank_id` ASC),
  INDEX `fk_gaji_bulanan_pinjaman_angsuran1_idx` (`pinjaman_angsuran_id` ASC),
  INDEX `fk_gaji_bulanan_p_insentif_tetap1_idx` (`p_insentif_tetap_id` ASC),
  INDEX `fk_gaji_bulanan_p_premi_hadir1_idx` (`p_premi_hadir_id` ASC),
  INDEX `fk_gaji_bulanan_p_sewa_mobil1_idx` (`p_sewa_mobil_id` ASC),
  INDEX `fk_gaji_bulanan_p_sewa_motor1_idx` (`p_sewa_motor_id` ASC),
  INDEX `fk_gaji_bulanan_p_sewa_rumah1_idx` (`p_sewa_rumah_id` ASC),
  INDEX `fk_gaji_bulanan_p_tunjagan_lain_tetap1_idx` (`p_tunjagan_lain_tetap_id` ASC),
  INDEX `fk_gaji_bulanan_p_tunjangan_anak_config1_idx` (`p_tunjangan_anak_config_id` ASC),
  INDEX `fk_gaji_bulanan_p_tunjangan_jabatan1_idx` (`p_tunjangan_jabatan_id` ASC),
  INDEX `fk_gaji_bulanan_p_tunjangan_kost1_idx` (`p_tunjangan_kost_id` ASC),
  INDEX `fk_gaji_bulanan_p_tunjangan_loyalitas_config1_idx` (`p_tunjangan_loyalitas_config_id` ASC),
  INDEX `fk_gaji_bulanan_p_tunjangan_pulsa1_idx` (`p_tunjangan_pulsa_id` ASC),
  INDEX `fk_gaji_bulanan_p_uang_makan1_idx` (`p_uang_makan_id` ASC),
  INDEX `fk_gaji_bulanan_p_uang_transport1_idx` (`p_uang_transport_id` ASC),
  INDEX `fk_gaji_bulanan_pegawai_bpjs1_idx` (`pegawai_bpjs_id` ASC),
  INDEX `fk_gaji_bulanan_proses_gaji1_idx` (`proses_gaji_id` ASC),
  INDEX `fk_gaji_bulanan_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_gaji_bulanan_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_gaji_bulanan_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_gaji_bulanan_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_gaji_bulanan_pegawai_bank1_idx` (`pegawai_bank_id` ASC),
  CONSTRAINT `fk_gaji_bulanan_p_gaji_pokok1`
    FOREIGN KEY (`p_gaji_pokok_id`)
    REFERENCES `p_gaji_pokok` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_admin_bank1`
    FOREIGN KEY (`p_admin_bank_id`)
    REFERENCES `p_admin_bank` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_pinjaman_angsuran1`
    FOREIGN KEY (`pinjaman_angsuran_id`)
    REFERENCES `pinjaman_angsuran` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_insentif_tetap1`
    FOREIGN KEY (`p_insentif_tetap_id`)
    REFERENCES `p_insentif_tetap` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_premi_hadir1`
    FOREIGN KEY (`p_premi_hadir_id`)
    REFERENCES `p_premi_hadir` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_sewa_mobil1`
    FOREIGN KEY (`p_sewa_mobil_id`)
    REFERENCES `p_sewa_mobil` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_sewa_motor1`
    FOREIGN KEY (`p_sewa_motor_id`)
    REFERENCES `p_sewa_motor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_sewa_rumah1`
    FOREIGN KEY (`p_sewa_rumah_id`)
    REFERENCES `p_sewa_rumah` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_tunjagan_lain_tetap1`
    FOREIGN KEY (`p_tunjagan_lain_tetap_id`)
    REFERENCES `p_tunjagan_lain_tetap` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_tunjangan_anak_config1`
    FOREIGN KEY (`p_tunjangan_anak_config_id`)
    REFERENCES `p_tunjangan_anak_config` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_tunjangan_jabatan1`
    FOREIGN KEY (`p_tunjangan_jabatan_id`)
    REFERENCES `p_tunjangan_jabatan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_tunjangan_kost1`
    FOREIGN KEY (`p_tunjangan_kost_id`)
    REFERENCES `p_tunjangan_kost` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_tunjangan_loyalitas_config1`
    FOREIGN KEY (`p_tunjangan_loyalitas_config_id`)
    REFERENCES `p_tunjangan_loyalitas_config` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_tunjangan_pulsa1`
    FOREIGN KEY (`p_tunjangan_pulsa_id`)
    REFERENCES `p_tunjangan_pulsa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_uang_makan1`
    FOREIGN KEY (`p_uang_makan_id`)
    REFERENCES `p_uang_makan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_p_uang_transport1`
    FOREIGN KEY (`p_uang_transport_id`)
    REFERENCES `p_uang_transport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_pegawai_bpjs1`
    FOREIGN KEY (`pegawai_bpjs_id`)
    REFERENCES `pegawai_bpjs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_proses_gaji1`
    FOREIGN KEY (`proses_gaji_id`)
    REFERENCES `proses_gaji` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_pegawai_bank1`
    FOREIGN KEY (`pegawai_bank_id`)
    REFERENCES `pegawai_bank` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `gaji_bulanan_detail` (
  `id` VARCHAR(32) NOT NULL,
  `masuk` DATETIME NULL DEFAULT NULL,
  `istirahat` DATETIME NULL DEFAULT NULL,
  `masuk_istirahat` DATETIME NULL DEFAULT NULL,
  `pulang` DATETIME NULL DEFAULT NULL,
  `tanggal` DATE NOT NULL,
  `gaji_bulanan_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `pegawai_sistem_kerja_id` INT NOT NULL,
  `time_table_id` INT(11) NULL DEFAULT '0',
  `status_kehadiran` INT(2) NULL DEFAULT '0',
  `spl_id` VARCHAR(32) NULL DEFAULT '0',
  `spl_template_id` INT(11) NULL,
  `jumlah_jam_lembur` VARCHAR(4) NULL DEFAULT '0',
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT NULL,
  `lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_gaji_bulanan_detail_gaji_bulanan1_idx` (`gaji_bulanan_id` ASC),
  INDEX `fk_gaji_bulanan_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_gaji_bulanan_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_gaji_bulanan_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_gaji_bulanan_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_gaji_bulanan_detail_pegawai_sistem_kerja1_idx` (`pegawai_sistem_kerja_id` ASC),
  CONSTRAINT `fk_gaji_bulanan_detail_gaji_bulanan1`
    FOREIGN KEY (`gaji_bulanan_id`)
    REFERENCES `gaji_bulanan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_detail_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_detail_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_detail_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gaji_bulanan_detail_pegawai_sistem_kerja1`
    FOREIGN KEY (`pegawai_sistem_kerja_id`)
    REFERENCES `pegawai_sistem_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200601_143125_ct_penggajian cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200601_143125_ct_penggajian cannot be reverted.\n";

        return false;
    }
    */
}

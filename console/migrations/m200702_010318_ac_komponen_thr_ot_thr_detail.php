<?php

use yii\db\Migration;

/**
 * Class m200702_010318_ac_komponen_thr_ot_thr_detail
 */
class m200702_010318_ac_komponen_thr_ot_thr_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('thr_detail', 'id_pegawai', $this->char(20)->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'nama', $this->char(225)->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'bisnis_unit', $this->char(32)->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'jenis_perjanjian', $this->char(32)->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'golongan', $this->char(32)->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'gaji_pokok_thr', $this->integer(11)->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'tanggal_mulai', $this->date()->notNull()->defaultValue(null)->after('thr_id'));
        $this->addColumn('thr_detail', 'masa_kerja_tahun', $this->integer(2)->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'masa_kerja_bulan', $this->integer(2)->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'presentase_thr', $this->float()->notNull()->defaultValue(0)->after('thr_id'));
        $this->addColumn('thr_detail', 'cut_off', $this->date()->notNull()->defaultValue(null)->after('thr_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200702_010318_ac_komponen_thr_ot_thr_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200702_010318_ac_komponen_thr_ot_thr_detail cannot be reverted.\n";

        return false;
    }
    */
}

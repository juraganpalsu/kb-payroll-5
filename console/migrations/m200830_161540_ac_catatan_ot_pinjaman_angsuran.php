<?php

use yii\db\Migration;

/**
 * Class m200830_161540_ac_catatan_ot_pinjaman_angsuran
 */
class m200830_161540_ac_catatan_ot_pinjaman_angsuran extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pinjaman_angsuran', 'catatan', $this->text()->null()->after('status'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200830_161540_ac_catatan_ot_pinjaman_angsuran cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200830_161540_ac_catatan_ot_pinjaman_angsuran cannot be reverted.\n";

        return false;
    }
    */
}

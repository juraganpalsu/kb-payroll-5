<?php

use yii\db\Migration;

/**
 * Class m191120_151835_cc_training
 */
class m191120_151835_cc_training extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `training` 
DROP FOREIGN KEY `fk_training_ref_trainer1`;
ALTER TABLE `training` 
CHANGE COLUMN `ref_trainer_id` `ref_trainer_id` VARCHAR(32) NULL ;
ALTER TABLE `training` 
ADD CONSTRAINT `fk_training_ref_trainer1`
  FOREIGN KEY (`ref_trainer_id`)
  REFERENCES `ref_trainer` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

SQL;
        $this->execute($sql);
        $sql = <<<SQL
ALTER TABLE `training` 
DROP FOREIGN KEY `fk_training_ref_trainer1`;
ALTER TABLE `training` 
DROP INDEX `fk_training_ref_trainer1_idx` ;
SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191120_151835_cc_training cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191120_151835_cc_training cannot be reverted.\n";

        return false;
    }
    */
}

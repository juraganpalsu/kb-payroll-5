<?php

use yii\db\Migration;

/**
 * Class m210119_065814_ct_pegawai_files
 */
class m210119_065814_ct_pegawai_files extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `pegawai_files` (
  `id` VARCHAR(36) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `jenis` INT(2) NOT NULL DEFAULT 0 COMMENT 'KTP\nKK\ndll',
  `pegawai_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_files_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_files_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210119_065814_ct_pegawai_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210119_065814_ct_pegawai_files cannot be reverted.\n";

        return false;
    }
    */
}

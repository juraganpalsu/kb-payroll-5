<?php

use yii\db\Migration;

/**
 * Class m190707_153721_ac_jabatan_id_ot_struktur
 */
class m190707_153721_ac_jabatan_id_ot_struktur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('struktur', 'jabatan_id', $this->string(32)->defaultValue(0)->null()->after('kode'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190707_153721_ac_jabatan_id_ot_struktur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190707_153721_ac_jabatan_id_ot_struktur cannot be reverted.\n";

        return false;
    }
    */
}

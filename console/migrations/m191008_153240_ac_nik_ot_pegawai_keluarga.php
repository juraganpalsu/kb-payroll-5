<?php

use yii\db\Migration;

/**
 * Class m191008_153240_ac_nik_ot_pegawai_keluarga
 */
class m191008_153240_ac_nik_ot_pegawai_keluarga extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai_keluarga', 'nik', $this->char(16)->notNull()->defaultValue(0)->after('nama'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191008_153240_ac_nik_ot_pegawai_keluarga cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191008_153240_ac_nik_ot_pegawai_keluarga cannot be reverted.\n";

        return false;
    }
    */
}

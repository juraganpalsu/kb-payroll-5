<?php

use yii\db\Migration;

/**
 * Class m200628_152432_ac_pinjaman_ot_gaji_bulanan
 */
class m200628_152432_ac_pinjaman_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'angsuran_bank_ke', $this->integer(2)->after('pinjaman_lain_lain_nominal'));
        $this->addColumn('gaji_bulanan', 'angsuran_kasbon_ke', $this->integer(2)->after('pinjaman_lain_lain_nominal'));
        $this->addColumn('gaji_bulanan', 'angsuran_koperasi_ke', $this->integer(2)->after('pinjaman_lain_lain_nominal'));
        $this->addColumn('gaji_bulanan', 'angsuran_potongan_ke', $this->integer(2)->after('pinjaman_lain_lain_nominal'));
        $this->addColumn('gaji_bulanan', 'angsuran_lain_lain_ke', $this->integer(2)->after('pinjaman_lain_lain_nominal'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200628_152432_ac_pinjaman_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200628_152432_ac_pinjaman_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

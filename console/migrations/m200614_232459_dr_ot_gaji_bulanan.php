<?php

use yii\db\Migration;

/**
 * Class m200614_232459_dr_ot_gaji_bulanan
 */
class m200614_232459_dr_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `gaji_bulanan` 
DROP FOREIGN KEY `fk_gaji_bulanan_pinjaman_angsuran1`,
DROP FOREIGN KEY `fk_gaji_bulanan_pegawai_struktur1`,
DROP FOREIGN KEY `fk_gaji_bulanan_pegawai_sistem_kerja1`,
DROP FOREIGN KEY `fk_gaji_bulanan_pegawai_bpjs1`,
DROP FOREIGN KEY `fk_gaji_bulanan_pegawai_bank1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_uang_transport1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_uang_makan1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_tunjangan_pulsa1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_tunjangan_loyalitas_config1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_tunjangan_kost1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_tunjangan_jabatan1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_tunjangan_anak_config1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_tunjagan_lain_tetap1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_sewa_rumah1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_sewa_motor1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_sewa_mobil1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_premi_hadir1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_insentif_tetap1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_gaji_pokok1`,
DROP FOREIGN KEY `fk_gaji_bulanan_p_admin_bank1`;
ALTER TABLE `gaji_bulanan` 
DROP INDEX `fk_gaji_bulanan_pegawai_sistem_kerja1_idx` ,
DROP INDEX `fk_gaji_bulanan_pegawai_bank1_idx` ,
DROP INDEX `fk_gaji_bulanan_pegawai_struktur1_idx` ,
DROP INDEX `fk_gaji_bulanan_pegawai_bpjs1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_uang_transport1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_uang_makan1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_tunjangan_pulsa1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_tunjangan_loyalitas_config1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_tunjangan_kost1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_tunjangan_jabatan1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_tunjangan_anak_config1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_tunjagan_lain_tetap1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_sewa_rumah1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_sewa_motor1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_sewa_mobil1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_premi_hadir1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_insentif_tetap1_idx` ,
DROP INDEX `fk_gaji_bulanan_pinjaman_angsuran1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_admin_bank1_idx` ,
DROP INDEX `fk_gaji_bulanan_p_gaji_pokok1_idx` ;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200614_232459_dr_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200614_232459_dr_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}
<?php

use yii\db\Migration;

class m170901_143140_ac_untuk_hari_ot_time_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('time_table', 'untuk_hari', $this->string(45)->notNull()->after('nama'));
    }

    public function safeDown()
    {
        echo "m170901_143140_ac_untuk_hari_ot_time_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170901_143140_ac_untuk_hari_ot_time_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191015_232518_ac_nama_ibu_kandung_ot_pegawai
 */
class m191015_232518_ac_nama_ibu_kandung_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('pegawai', 'nama_ibu_kandung', $this->char(225)->notNull()->defaultValue(0)->after('nama_panggilan'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191015_232518_ac_nama_ibu_kandung_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191015_232518_ac_nama_ibu_kandung_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200701_032600_dr_id_izin_ot_gaji_bulanan
 */
class m200701_032600_dr_id_izin_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('gaji_bulanan', 'wfh_id');
        $this->dropColumn('gaji_bulanan', 'sd_id');
        $this->dropColumn('gaji_bulanan', 'ith_id');
        $this->dropColumn('gaji_bulanan', 'idl_id');
        $this->dropColumn('gaji_bulanan', 'off_id');
        $this->dropColumn('gaji_bulanan', 'cm_id');
        $this->dropColumn('gaji_bulanan', 'is_id');
        $this->dropColumn('gaji_bulanan', 'tl_id');
        $this->dropColumn('gaji_bulanan', 'dispensasi_id');
        $this->dropColumn('gaji_bulanan', 'atl_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200701_032600_dr_id_izin_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200701_032600_dr_id_izin_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

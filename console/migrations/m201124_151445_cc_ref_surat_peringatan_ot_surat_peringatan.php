<?php

use yii\db\Migration;

/**
 * Class m201124_151445_cc_ref_surat_peringatan_ot_surat_peringatan
 */
class m201124_151445_cc_ref_surat_peringatan_ot_surat_peringatan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `surat_peringatan` 
CHANGE COLUMN `ref_peraturan_perusahaan_id` `ref_peraturan_perusahaan_id` VARCHAR(255) NOT NULL DEFAULT '0' ;

SQL;
        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201124_151445_cc_ref_surat_peringatan_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201124_151445_cc_ref_surat_peringatan_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }
    */
}

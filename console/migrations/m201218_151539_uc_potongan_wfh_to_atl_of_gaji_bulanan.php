<?php

use yii\db\Migration;

/**
 * Class m201218_151539_uc_potongan_wfh_to_atl_of_gaji_bulanan
 */
class m201218_151539_uc_potongan_wfh_to_atl_of_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `gaji_bulanan` 
CHANGE COLUMN `pinjaman_potongan_wfh_id` `pinjaman_potongan_atl_id` CHAR(32) NULL DEFAULT NULL ,
CHANGE COLUMN `pinjaman_potongan_wfh_nominal` `pinjaman_potongan_atl_nominal` FLOAT NULL DEFAULT '0' ,
CHANGE COLUMN `angsuran_potongan_wfh_ke` `angsuran_potongan_atl_ke` INT(2) NULL DEFAULT NULL ;
SQL;
        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201218_151539_uc_potongan_wfh_to_atl_of_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201218_151539_uc_potongan_wfh_to_atl_of_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m201118_044619_ac_insentif_lembur_ot_gaji_bulanan
 */
class m201118_044619_ac_insentif_lembur_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'insentif_lembur', $this->decimal(11, 2)->defaultValue(0)->after('uang_lembur'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201118_044619_ac_insentif_lembur_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201118_044619_ac_insentif_lembur_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

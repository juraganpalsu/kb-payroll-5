<?php

use yii\db\Migration;

/**
 * Class m200910_162406_ct_pegawai_hub
 */
class m200910_162406_ct_pegawai_hub extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql =<<<SQL
-- -----------------------------------------------------
-- Table `pegawai_hub`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pegawai_hub` (
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_sistem_kerja_id` INT NOT NULL,
  `akun_beatroute_id` VARCHAR(32) NOT NULL,
  `area_kerja_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  INDEX `fk_pegawai_hub_pegawai1_idx` (`pegawai_id` ASC),
  PRIMARY KEY (`pegawai_id`),
  INDEX `fk_pegawai_hub_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_pegawai_hub_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_pegawai_hub_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_pegawai_hub_pegawai_sistem_kerja1_idx` (`pegawai_sistem_kerja_id` ASC),
  INDEX `fk_pegawai_hub_akun_beatroute1_idx` (`akun_beatroute_id` ASC),
  INDEX `fk_pegawai_hub_area_kerja1_idx` (`area_kerja_id` ASC),
  CONSTRAINT `fk_pegawai_hub_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_pegawai_sistem_kerja1`
    FOREIGN KEY (`pegawai_sistem_kerja_id`)
    REFERENCES `pegawai_sistem_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_akun_beatroute1`
    FOREIGN KEY (`akun_beatroute_id`)
    REFERENCES `akun_beatroute` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_area_kerja1`
    FOREIGN KEY (`area_kerja_id`)
    REFERENCES `area_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pegawai_hub_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pegawai_hub_log` (
  `id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_sistem_kerja_id` INT NOT NULL,
  `akun_beatroute_id` VARCHAR(32) NOT NULL,
  `area_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_hub_pegawai_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  INDEX `fk_pegawai_hub_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_pegawai_hub_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_pegawai_hub_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_pegawai_hub_pegawai_sistem_kerja1_idx` (`pegawai_sistem_kerja_id` ASC),
  INDEX `fk_pegawai_hub_akun_beatroute1_idx` (`akun_beatroute_id` ASC),
  INDEX `fk_pegawai_hub_area_kerja1_idx` (`area_kerja_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_hub_log_pegawai_hub1_idx` (`pegawai_hub_pegawai_id` ASC),
  CONSTRAINT `fk_pegawai_hub_perjanjian_kerja10`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_pegawai_struktur10`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_pegawai_golongan10`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_pegawai_sistem_kerja10`
    FOREIGN KEY (`pegawai_sistem_kerja_id`)
    REFERENCES `pegawai_sistem_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_akun_beatroute10`
    FOREIGN KEY (`akun_beatroute_id`)
    REFERENCES `akun_beatroute` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_area_kerja10`
    FOREIGN KEY (`area_kerja_id`)
    REFERENCES `area_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_hub_log_pegawai_hub1`
    FOREIGN KEY (`pegawai_hub_pegawai_id`)
    REFERENCES `pegawai_hub` (`pegawai_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200910_162406_ct_pegawai_hub cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200910_162406_ct_pegawai_hub cannot be reverted.\n";

        return false;
    }
    */
}

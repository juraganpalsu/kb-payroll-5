<?php

use yii\db\Migration;

/**
 * Class m190915_153607_dr_akun_beatroute
 */
class m190915_153607_dr_akun_beatroute extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `akun_beatroute` 
DROP FOREIGN KEY `fk_akun_beatroute_perjanjian_kerja1`;
ALTER TABLE `akun_beatroute` 
DROP COLUMN `pegawai_struktur_id`,
DROP COLUMN `perjanjian_kerja_id`,
DROP INDEX `fk_akun_beatroute_perjanjian_kerja1_idx` ;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190915_153607_dr_akun_beatroute cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190915_153607_dr_akun_beatroute cannot be reverted.\n";

        return false;
    }
    */
}

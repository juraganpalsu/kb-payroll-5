<?php

use yii\db\Migration;

/**
 * Class m210305_154416_finishing_status_ot_gaji_borongan_header
 */
class m210305_154416_finishing_status_ot_gaji_borongan_header extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_borongan_header', 'finishing_status', $this->integer(2)->notNull()->defaultValue(1)->after('jenis_borongan'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210305_154416_finishing_status_ot_gaji_borongan_header cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210305_154416_finishing_status_ot_gaji_borongan_header cannot be reverted.\n";

        return false;
    }
    */
}

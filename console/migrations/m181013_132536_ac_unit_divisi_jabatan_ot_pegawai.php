<?php

use yii\db\Migration;

/**
 * Class m181013_132536_ac_unit_divisi_jabatan_ot_pegawai
 */
class m181013_132536_ac_unit_divisi_jabatan_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('pegawai', 'unit', $this->tinyInteger(1)->notNull()->after('sistem_kerja')->defaultValue(0));
        $this->addColumn('pegawai', 'divisi', $this->tinyInteger(2)->notNull()->after('unit')->defaultValue(0));
        $this->addColumn('pegawai', 'jabatan', $this->tinyInteger(1)->notNull()->after('divisi')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181013_132536_ac_unit_divisi_jabatan_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181013_132536_ac_unit_divisi_jabatan_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

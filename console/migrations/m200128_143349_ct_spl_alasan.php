<?php

use yii\db\Migration;

/**
 * Class m200128_143349_ct_spl_alasan
 */
class m200128_143349_ct_spl_alasan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `spl_alasan` (
  `id` VARCHAR(32) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SQL;

        $this->execute($sql);

        $this->addColumn('spl', 'spl_alasan_id', $this->char(32)->notNull()->defaultValue(0)->after('keterangan'));
        $this->addColumn('spl', 'cost_center_id', $this->char(32)->notNull()->defaultValue(0)->after('spl_alasan_id'));


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200128_143349_ct_spl_alasan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200128_143349_ct_spl_alasan cannot be reverted.\n";

        return false;
    }
    */
}

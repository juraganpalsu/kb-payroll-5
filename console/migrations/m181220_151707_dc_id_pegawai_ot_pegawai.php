<?php

use yii\db\Migration;

/**
 * Class m181220_151707_dc_id_pegawai_ot_pegawai
 */
class m181220_151707_dc_id_pegawai_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('pegawai', 'id_pegawai');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181220_151707_dc_id_pegawai_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181220_151707_dc_id_pegawai_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

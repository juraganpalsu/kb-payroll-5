<?php

use yii\db\Migration;

/**
 * Class m191128_162854_ac_all_ot_kecelakaan_kerja
 */
class m191128_162854_ac_all_ot_kecelakaan_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('kecelakaan_kerja', 'klaim_bpjs', $this->integer(1)->defaultValue(0)->notNull()->after('preventif'));
        $this->addColumn('kecelakaan_kerja', 'berita_acara', $this->integer(1)->defaultValue(0)->notNull()->after('klaim_bpjs'));
        $this->addColumn('kecelakaan_kerja', 'tahap_satu', $this->integer(1)->defaultValue(0)->notNull()->after('berita_acara'));
        $this->addColumn('kecelakaan_kerja', 'tahap_dua', $this->integer(1)->defaultValue(0)->notNull()->after('tahap_satu'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191128_162854_ac_all_ot_kecelakaan_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191128_162854_ac_all_ot_kecelakaan_kerja cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200820_033205_ac_beatroute_ot_gaji_bulanan
 */
class m200820_033205_ac_beatroute_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'beatroute', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('cabang'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200820_033205_ac_beatroute_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200820_033205_ac_beatroute_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

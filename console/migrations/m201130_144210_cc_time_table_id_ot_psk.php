<?php

use yii\db\Migration;

/**
 * Class m201130_144210_cc_time_table_id_ot_psk
 */
class m201130_144210_cc_time_table_id_ot_psk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `perubahan_status_absensi` 
CHANGE COLUMN `time_table_id` `time_table_id` INT(11) NULL DEFAULT '0' ;
SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201130_144210_cc_time_table_id_ot_psk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201130_144210_cc_time_table_id_ot_psk cannot be reverted.\n";

        return false;
    }
    */
}

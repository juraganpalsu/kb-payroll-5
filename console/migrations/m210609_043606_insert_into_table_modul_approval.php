<?php

use yii\db\Migration;

/**
 * Class m210609_043606_insert_into_table_modul_approval
 */
class m210609_043606_insert_into_table_modul_approval extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
INSERT INTO `hris`.`modul_approval` (`id`, `nama`, `created_by`, `created_by_pk`, `created_by_struktur`, `updated_by`, `deleted_by`, `lock`) VALUES ('2', 'BARANG APPROVAL', '0', '0', '0', '0', '0', '0');
UPDATE `hris`.`modul_approval` SET `nama`='SPKL_APPROVAL' WHERE `id`='1';
SQL;
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210609_043606_insert_into_table_modul_approval cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210609_043606_insert_into_table_modul_approval cannot be reverted.\n";

        return false;
    }
    */
}

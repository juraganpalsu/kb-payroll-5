<?php

use yii\db\Migration;

/**
 * Class m201015_142931_ct_revisi_gaji
 */
class m201015_142931_ct_revisi_gaji extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `revisi_gaji` (
  `id` VARCHAR(36) NOT NULL,
  `proses_gaji_id` VARCHAR(32) NOT NULL,
  `revisi_ke` INT(2) NOT NULL DEFAULT  0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_revisi_gaji_proses_gaji1_idx` (`proses_gaji_id` ASC),
  CONSTRAINT `fk_revisi_gaji_proses_gaji1`
    FOREIGN KEY (`proses_gaji_id`)
    REFERENCES `proses_gaji` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201015_142931_ct_revisi_gaji cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201015_142931_ct_revisi_gaji cannot be reverted.\n";

        return false;
    }
    */
}

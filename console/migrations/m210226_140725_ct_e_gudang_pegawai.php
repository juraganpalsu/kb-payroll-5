<?php

use yii\db\Migration;

/**
 * Class m210226_140725_ct_e_gudang_pegawai
 */
class m210226_140725_ct_e_gudang_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `e_gudang_pegawai` (
  `id` VARCHAR(36) NOT NULL,
  `e_gudang_id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `status` INT(1) NOT NULL DEFAULT 0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_gudang_pegawai_e_gudang1_idx` (`e_gudang_id` ASC),
  INDEX `fk_e_gudang_pegawai_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_e_gudang_pegawai_e_gudang1`
    FOREIGN KEY (`e_gudang_id`)
    REFERENCES `e_gudang` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_gudang_pegawai_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210226_140725_ct_e_gudang_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210226_140725_ct_e_gudang_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

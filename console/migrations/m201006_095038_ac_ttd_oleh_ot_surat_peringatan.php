<?php

use yii\db\Migration;

/**
 * Class m201006_095038_ac_ttd_oleh_ot_surat_peringatan
 */
class m201006_095038_ac_ttd_oleh_ot_surat_peringatan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('surat_peringatan', 'oleh_pegawai_struktur_id', $this->string(32)->notNull()->defaultValue(0)->after('pegawai_struktur_id'));
        $this->addColumn('surat_peringatan', 'oleh_perjanjian_kerja_id', $this->string(32)->notNull()->defaultValue(0)->after('pegawai_struktur_id'));
        $this->addColumn('surat_peringatan', 'oleh_pegawai_id', $this->string(32)->notNull()->defaultValue(0)->after('pegawai_struktur_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201006_095038_ac_ttd_oleh_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201006_095038_ac_ttd_oleh_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }
    */
}

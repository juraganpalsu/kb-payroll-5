<?php

use yii\db\Migration;

/**
 * Class m181030_140327_ct_sistem_kerja
 */
class m181030_140327_ct_sistem_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `sistem_kerja` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(45) NOT NULL,
  `hari_keenam_setengah` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Apakah hari keenam setengah hari?',
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `sistem_kerja_time_table` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sistem_kerja_id` INT NOT NULL,
  `time_table_id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sistem_kerja_time_table_sistem_kerja1_idx` (`sistem_kerja_id` ASC),
  INDEX `fk_sistem_kerja_time_table_time_table1_idx` (`time_table_id` ASC),
  CONSTRAINT `fk_sistem_kerja_time_table_sistem_kerja1`
    FOREIGN KEY (`sistem_kerja_id`)
    REFERENCES `sistem_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sistem_kerja_time_table_time_table1`
    FOREIGN KEY (`time_table_id`)
    REFERENCES `time_table` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pola_jadwal_libur` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `masuk` TINYINT(1) NOT NULL COMMENT 'Jumlah hari masuk kerja',
  `libur` TINYINT(1) NOT NULL COMMENT 'Jumlah hari libur setelah masuk kerja',
  `sistem_kerja_id` INT NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pola_jadwal_libur_sistem_kerja1_idx` (`sistem_kerja_id` ASC),
  CONSTRAINT `fk_pola_jadwal_libur_sistem_kerja1`
    FOREIGN KEY (`sistem_kerja_id`)
    REFERENCES `sistem_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181030_140327_ct_sistem_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181030_140327_ct_sistem_kerja cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200414_143100_ac_status_pinjaman_angsuran
 */
class m200414_143100_ac_status_pinjaman_angsuran extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pinjaman_angsuran', 'payroll_periode_id', $this->char(32)->notNull()->after('pinjaman_id')->defaultValue('0'));
        $this->addColumn('pinjaman_angsuran', 'status', $this->boolean()->notNull()->after('payroll_periode_id')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200414_143100_ac_status_pinjaman_angsuran cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_143100_ac_status_pinjaman_angsuran cannot be reverted.\n";

        return false;
    }
    */
}

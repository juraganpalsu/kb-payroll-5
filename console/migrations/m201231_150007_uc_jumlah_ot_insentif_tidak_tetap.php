<?php

use yii\db\Migration;

/**
 * Class m201231_150007_uc_jumlah_ot_insentif_tidak_tetap
 */
class m201231_150007_uc_jumlah_ot_insentif_tidak_tetap extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `insentif_tidak_tetap` 
CHANGE COLUMN `jumlah` `jumlah` DECIMAL(11) NOT NULL DEFAULT '0' ;


SQL;
        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201231_150007_uc_jumlah_ot_insentif_tidak_tetap cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201231_150007_uc_jumlah_ot_insentif_tidak_tetap cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190104_162329_ac_approval_status_ot_spl
 */
class m190104_162329_ac_approval_status_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('spl', 'status_approval', $this->tinyInteger(2)->notNull()->defaultValue(0)->after('keterangan'));
        $this->addColumn('spl_detail', 'tipe_approval', $this->tinyInteger(2)->notNull()->defaultValue(0)->after('spl_id'));
        $this->addColumn('spl_detail', 'keterangan_approval', $this->text()->after('tipe_approval'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190104_162329_ac_approval_status_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190104_162329_ac_approval_status_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

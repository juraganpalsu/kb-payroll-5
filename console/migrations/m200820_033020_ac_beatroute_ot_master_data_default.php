<?php

use yii\db\Migration;

/**
 * Class m200820_033020_ac_beatroute_ot_master_data_default
 */
class m200820_033020_ac_beatroute_ot_master_data_default extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('master_data_default', 'beatroute', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('cabang'));
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200820_033020_ac_beatroute_ot_master_data_default cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200820_033020_ac_beatroute_ot_master_data_default cannot be reverted.\n";

        return false;
    }
    */
}

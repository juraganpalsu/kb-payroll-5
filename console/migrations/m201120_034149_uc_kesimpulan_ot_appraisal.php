<?php

use yii\db\Migration;

/**
 * Class m201120_034149_uc_kesimpulan_ot_appraisal
 */
class m201120_034149_uc_kesimpulan_ot_appraisal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('appraisal', 'kesimpulan', $this->string(255)->null());
        $this->alterColumn('appraisal', 'rekomendasi', $this->tinyInteger(1)->notNull()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201120_034149_uc_kesimpulan_ot_appraisal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201120_034149_uc_kesimpulan_ot_appraisal cannot be reverted.\n";

        return false;
    }
    */
}

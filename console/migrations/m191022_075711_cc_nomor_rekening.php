<?php

use yii\db\Migration;

/**
 * Class m191022_075711_cc_nomor_rekening
 */
class m191022_075711_cc_nomor_rekening extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `pegawai_bank` 
CHANGE COLUMN `nomer_rekening` `nomer_rekening` VARCHAR(45) NOT NULL ;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191022_075711_cc_nomor_rekening cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191022_075711_cc_nomor_rekening cannot be reverted.\n";

        return false;
    }
    */
}

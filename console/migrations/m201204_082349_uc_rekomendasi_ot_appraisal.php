<?php

use yii\db\Migration;

/**
 * Class m201204_082349_uc_rekomendasi_ot_appraisal
 */
class m201204_082349_uc_rekomendasi_ot_appraisal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `appraisal` 
CHANGE COLUMN `rekomendasi` `rekomendasi` TINYINT(1) NOT NULL DEFAULT 0 ;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201204_082349_uc_rekomendasi_ot_appraisal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201204_082349_uc_rekomendasi_ot_appraisal cannot be reverted.\n";

        return false;
    }
    */
}

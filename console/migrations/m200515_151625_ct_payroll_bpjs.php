<?php

use yii\db\Migration;

/**
 * Class m200515_151625_ct_payroll_bpjs
 */
class m200515_151625_ct_payroll_bpjs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `payroll_bpjs` (
  `id` VARCHAR(32) NOT NULL,
  `catatan` TEXT NULL,
  `payroll_periode_id` VARCHAR(32) NOT NULL,
  `golongan_id` INT NOT NULL,
  `bisnis_unit` INT(2) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_bpjs_golongan1_idx` (`golongan_id` ASC),
  INDEX `fk_bpjs_payroll_periode1_idx` (`payroll_periode_id` ASC),
  CONSTRAINT `fk_bpjs_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bpjs_payroll_periode1`
    FOREIGN KEY (`payroll_periode_id`)
    REFERENCES `payroll_periode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `payroll_bpjs_detail` (
  `id` VARCHAR(32) NOT NULL,
  `bpjs_k_nominal` INT NOT NULL DEFAULT 0,
  `bpjs_tk_nominal` INT NOT NULL DEFAULT 0,
  `bpjs_id` VARCHAR(32) NOT NULL,
  `pegawai_bpjs_t_id` VARCHAR(32) NOT NULL,
  `pegawai_bpjs_tk_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_bpjs_detail_pegawai_bpjs1_idx` (`pegawai_bpjs_t_id` ASC),
  INDEX `fk_bpjs_detail_pegawai_bpjs2_idx` (`pegawai_bpjs_tk_id` ASC),
  INDEX `fk_bpjs_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_bpjs_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_bpjs_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_bpjs_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_bpjs_detail_bpjs1_idx` (`bpjs_id` ASC),
  CONSTRAINT `fk_bpjs_detail_pegawai_bpjs1`
    FOREIGN KEY (`pegawai_bpjs_t_id`)
    REFERENCES `pegawai_bpjs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bpjs_detail_pegawai_bpjs2`
    FOREIGN KEY (`pegawai_bpjs_tk_id`)
    REFERENCES `pegawai_bpjs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bpjs_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bpjs_detail_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bpjs_detail_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bpjs_detail_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bpjs_detail_bpjs1`
    FOREIGN KEY (`bpjs_id`)
    REFERENCES `payroll_bpjs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;


        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200515_151625_ct_payroll_bpjs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200515_151625_ct_payroll_bpjs cannot be reverted.\n";

        return false;
    }
    */
}

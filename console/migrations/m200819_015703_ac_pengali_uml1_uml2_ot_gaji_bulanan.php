<?php

use yii\db\Migration;

/**
 * Class m200819_015703_ac_pengali_uml1_uml2_ot_gaji_bulanan
 */
class m200819_015703_ac_pengali_uml1_uml2_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'pengali_uml2', $this->integer(2)->notNull()->defaultValue(0)->after('uml'));
        $this->addColumn('gaji_bulanan', 'pengali_uml1', $this->integer(2)->notNull()->defaultValue(0)->after('uml'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200819_015703_ac_pengali_uml1_uml2_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200819_015703_ac_pengali_uml1_uml2_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

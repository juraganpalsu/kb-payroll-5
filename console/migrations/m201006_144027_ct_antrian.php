<?php

use yii\db\Migration;

/**
 * Class m201006_144027_ct_antrian
 */
class m201006_144027_ct_antrian extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `antrian_proses_absensi` (
  `id` VARCHAR(32) NOT NULL,
  `tanggal_awal` DATE NOT NULL,
  `tanggal_akhir` DATE NOT NULL,
  `golongan_id` VARCHAR(32) NOT NULL,
  `bisnis_unit` INT(2) NULL DEFAULT 0,
  `status` INT(2) NULL DEFAULT 0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_antrian_proses_absensi_1_idx` (`created_by` ASC),
  CONSTRAINT `fk_antrian_proses_absensi_1`
    FOREIGN KEY (`created_by`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `antrian_laporan_absensi` (
  `id` VARCHAR(32) NOT NULL,
  `tanggal_awal` DATE NOT NULL,
  `tanggal_akhir` DATE NOT NULL,
  `golongan_id` VARCHAR(32) NOT NULL,
  `bisnis_unit` INT(2) NULL DEFAULT 0,
  `jenis_perjanjian` INT(2) NULL DEFAULT 0,
  `status_kehadiran` INT(2) NULL DEFAULT 0,
  `status` INT(2) NULL DEFAULT 0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_antrian_laporan_absensi_1_idx` (`created_by` ASC),
  CONSTRAINT `fk_antrian_laporan_absensi_1`
    FOREIGN KEY (`created_by`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201006_144027_ct_antrian cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201006_144027_ct_antrian cannot be reverted.\n";

        return false;
    }
    */
}

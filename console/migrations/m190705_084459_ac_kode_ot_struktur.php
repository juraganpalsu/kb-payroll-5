<?php

use yii\db\Migration;

/**
 * Class m190705_084459_ac_kode_ot_struktur
 */
class m190705_084459_ac_kode_ot_struktur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `struktur` 
CHANGE COLUMN `kode` `kode` CHAR(20) NOT NULL DEFAULT '0' ;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190705_084459_ac_kode_ot_struktur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190705_084459_ac_kode_ot_struktur cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m201206_210849_ds_turn_over
 */
class m201206_210849_ds_turn_over extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `ds_turn_over` (
  `id` VARCHAR(36) NOT NULL,
  `tahun` INT(4) NOT NULL DEFAULT 0,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ds_turn_over_detail` (
  `id` VARCHAR(36) NOT NULL,
  `bulan` INT(2) NOT NULL DEFAULT 0,
  `bulan_string` VARCHAR(45) NOT NULL DEFAULT 0,
  `masuk` INT(4) NOT NULL DEFAULT 0,
  `resign` INT(4) NOT NULL DEFAULT 0,
  `total` INT(5) NOT NULL DEFAULT 0,
  `ds_turn_over_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ds_turn_over_detail_ds_turn_over1_idx` (`ds_turn_over_id` ASC),
  CONSTRAINT `fk_ds_turn_over_detail_ds_turn_over1`
    FOREIGN KEY (`ds_turn_over_id`)
    REFERENCES `ds_turn_over` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201206_210849_ds_turn_over cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201206_210849_ds_turn_over cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200211_094845_ac_durasi_auto_approve_min_ot_struktur
 */
class m200211_094845_ac_durasi_auto_approve_min_ot_struktur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('struktur', 'durasi_auto_approve_min', $this->integer(3)->notNull()->defaultValue(0)->after('auto_approve'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200211_094845_ac_durasi_auto_approve_min_ot_struktur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200211_094845_ac_durasi_auto_approve_min_ot_struktur cannot be reverted.\n";

        return false;
    }
    */
}

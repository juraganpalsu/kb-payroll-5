<?php

use yii\db\Migration;

/**
 * Class m191117_125600_ref_peraturan_perusahaan
 */
class m191117_125600_ref_peraturan_perusahaan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `ref_peraturan_perusahaan` (
  `id` VARCHAR(32) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SQL;
        $this->execute($sql);

        $this->addColumn('surat_peringatan', 'ref_peraturan_perusahaan_id', $this->char(32)->notNull()->defaultValue(0)->after('ref_surat_peringatan_id'));


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191117_125600_ref_peraturan_perusahaan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191117_125600_ref_peraturan_perusahaan cannot be reverted.\n";

        return false;
    }
    */
}

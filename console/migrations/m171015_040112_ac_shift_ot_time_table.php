<?php

use yii\db\Migration;

class m171015_040112_ac_shift_ot_time_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('time_table', 'shift', $this->integer(1)->defaultValue(0)->after('nama'));
        $sql = 'UPDATE `kb-payroll`.`time_table` SET `shift`=\'1\' WHERE `id`=\'2\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'2\' WHERE `id`=\'3\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'3\' WHERE `id`=\'4\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'1\' WHERE `id`=\'6\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'2\' WHERE `id`=\'7\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'3\' WHERE `id`=\'8\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'1\' WHERE `id`=\'10\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'2\' WHERE `id`=\'11\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'3\' WHERE `id`=\'12\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'1\' WHERE `id`=\'13\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'2\' WHERE `id`=\'14\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'3\' WHERE `id`=\'15\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'2\' WHERE `id`=\'16\';
UPDATE `kb-payroll`.`time_table` SET `shift`=\'3\' WHERE `id`=\'17\';
';
        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m171015_040112_ac_shift_ot_time_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171015_040112_ac_shift_ot_time_table cannot be reverted.\n";

        return false;
    }
    */
}

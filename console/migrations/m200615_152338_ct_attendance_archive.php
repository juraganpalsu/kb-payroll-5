<?php

use yii\db\Migration;

/**
 * Class m200615_152338_ct_attendance_archive
 */
class m200615_152338_ct_attendance_archive extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `attendance_archive` (
  `id` INT NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `check_time` DATETIME NOT NULL,
  `idfp` INT(11) NOT NULL,
  `uploaded` TINYINT NOT NULL DEFAULT 0,
  `device_id` VARCHAR(45) NULL DEFAULT 0,
  `attendance_beatroute_id` INT(11) UNSIGNED NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_arsip_kehadiran_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_arsip_kehadiran_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200615_152338_ct_attendance_archive cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200615_152338_ct_attendance_archive cannot be reverted.\n";

        return false;
    }
    */
}

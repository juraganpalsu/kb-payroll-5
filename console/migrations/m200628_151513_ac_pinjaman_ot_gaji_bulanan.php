<?php

use yii\db\Migration;

/**
 * Class m200628_151513_ac_pinjaman_ot_gaji_bulanan
 */
class m200628_151513_ac_pinjaman_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'pinjaman_bank_id', $this->char(32)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_bank_nominal', $this->float(2)->defaultValue(0)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_kasbon_id', $this->char(32)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_kasbon_nominal', $this->float(2)->defaultValue(0)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_koperasi_id', $this->char(32)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_koperasi_nominal', $this->float(2)->defaultValue(0)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_potongan_id', $this->char(32)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_potongan_nominal', $this->float(2)->defaultValue(0)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_lain_lain_id', $this->char(32)->after('pinjaman_angsuran_id'));
        $this->addColumn('gaji_bulanan', 'pinjaman_lain_lain_nominal', $this->float(2)->defaultValue(0)->after('pinjaman_angsuran_id'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200628_151513_ac_pinjaman_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200628_151513_ac_pinjaman_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

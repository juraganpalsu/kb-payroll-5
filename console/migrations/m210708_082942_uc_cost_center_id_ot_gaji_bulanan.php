<?php

use yii\db\Migration;

/**
 * Class m210708_082942_uc_cost_center_id_ot_gaji_bulanan
 */
class m210708_082942_uc_cost_center_id_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
ALTER TABLE `gaji_bulanan` 
ADD COLUMN `cost_center_id` VARCHAR(32) NULL DEFAULT NULL AFTER `pegawai_sistem_kerja_id`;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210708_082942_uc_cost_center_id_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210708_082942_uc_cost_center_id_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

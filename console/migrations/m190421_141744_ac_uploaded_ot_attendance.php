<?php

use yii\db\Migration;

/**
 * Class m190421_141744_ac_uploaded_ot_attendance
 */
class m190421_141744_ac_uploaded_ot_attendance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('attendance', 'uploaded', $this->tinyInteger(1)->defaultValue(0)->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190421_141744_ac_uploaded_ot_attendance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190421_141744_ac_uploaded_ot_attendance cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190902_140402_ac_perjanjian_kerja_id_ot_absensi
 */
class m190902_140402_ac_perjanjian_kerja_id_ot_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('absensi', 'perjanjian_kerja_id', $this->char(32)->null()->defaultValue(0)->after('pegawai_id'));
        $this->addColumn('absensi', 'pegawai_struktur_id', $this->char(32)->null()->defaultValue(0)->after('perjanjian_kerja_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190902_140402_ac_perjanjian_kerja_id_ot_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190902_140402_ac_perjanjian_kerja_id_ot_absensi cannot be reverted.\n";

        return false;
    }
    */
}

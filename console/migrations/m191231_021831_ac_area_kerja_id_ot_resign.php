<?php

use yii\db\Migration;

/**
 * Class m191231_021831_ac_area_kerja_id_ot_resign
 */
class m191231_021831_ac_area_kerja_id_ot_resign extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
ALTER TABLE `resign` 
ADD COLUMN `area_kerja_id` VARCHAR(32) NOT NULL AFTER `pegawai_golongan_id`;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191231_021831_ac_area_kerja_id_ot_resign cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191231_021831_ac_area_kerja_id_ot_resign cannot be reverted.\n";

        return false;
    }
    */
}

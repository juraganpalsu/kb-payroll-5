<?php

use yii\db\Migration;

/**
 * Class m200803_091023_ct_payroll_bonus
 */
class m200803_091023_ct_payroll_bonus extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `payroll_bonus` (
  `id` VARCHAR(32) NOT NULL,
  `tahun` INT(4) NOT NULL,
  `catatan` TEXT NULL,
  `golongan_id` INT NOT NULL,
  `bisnis_unit` INT(2) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_bonus_golongan1_idx` (`golongan_id` ASC),
  CONSTRAINT `fk_bonus_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200803_091023_ct_payroll_bonus cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200803_091023_ct_payroll_bonus cannot be reverted.\n";

        return false;
    }
    */
}

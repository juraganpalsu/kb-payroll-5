<?php

use yii\db\Migration;

/**
 * Class m200624_224141_ac_tanggal_pulang_ot_psk
 */
class m200624_224141_ac_tanggal_pulang_ot_psk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perubahan_status_absensi', 'tanggal_pulang', $this->date()->after('tanggal'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200624_224141_ac_tanggal_pulang_ot_psk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200624_224141_ac_tanggal_pulang_ot_psk cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190724_142443_ct_cost_center_template
 */
class m190724_142443_ct_cost_center_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `cost_center_template` (
  `id` VARCHAR(32) NOT NULL,
  `kode` INT NOT NULL DEFAULT 0,
  `area_kerja` VARCHAR(45) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SQL;
        $this->execute($sql);

        $this->dropColumn('cost_center', 'kode');
        $this->dropColumn('cost_center', 'area_kerja');

        $this->addColumn('cost_center', 'cost_center_template_id', $this->char(32)->notNull()->defaultValue(0)->after('keterangan'));


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190724_142443_ct_cost_center_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190724_142443_ct_cost_center_template cannot be reverted.\n";

        return false;
    }
    */
}

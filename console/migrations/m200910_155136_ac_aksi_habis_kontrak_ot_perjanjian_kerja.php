<?php

use yii\db\Migration;

/**
 * Class m200910_155136_ac_aksi_habis_kontrak_ot_perjanjian_kerja
 */
class m200910_155136_ac_aksi_habis_kontrak_ot_perjanjian_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perjanjian_kerja', 'keterangan_habis_kontrak', $this->text()->null()->after('keterangan'));
        $this->addColumn('perjanjian_kerja', 'aksi_habis_kontrak', $this->integer(2)->null()->defaultValue(0)->after('keterangan'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200910_155136_ac_aksi_habis_kontrak_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200910_155136_ac_aksi_habis_kontrak_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }
    */
}

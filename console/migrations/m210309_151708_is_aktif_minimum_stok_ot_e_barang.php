<?php

use yii\db\Migration;

/**
 * Class m210309_151708_is_aktif_minimum_stok_ot_e_barang
 */
class m210309_151708_is_aktif_minimum_stok_ot_e_barang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('e_barang', 'is_aktif', $this->integer(1)->notNull()->defaultValue(0)->after('kategori'));
        $this->addColumn('e_barang', 'minimum_stok', $this->integer(2)->notNull()->defaultValue(0)->after('kategori'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210309_151708_is_aktif_minimum_stok_ot_e_barang cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210309_151708_is_aktif_minimum_stok_ot_e_barang cannot be reverted.\n";

        return false;
    }
    */
}

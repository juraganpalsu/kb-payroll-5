<?php

use yii\db\Migration;

/**
 * Class m200806_153330_dr_struktur_ot_bpjs_detail
 */
class m200806_153330_dr_struktur_ot_bpjs_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `payroll_bpjs_detail` 
DROP FOREIGN KEY `fk_bpjs_detail_pegawai_struktur1`;
ALTER TABLE `payroll_bpjs_detail` 
DROP INDEX `fk_bpjs_detail_pegawai_struktur1_idx` ;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200806_153330_dr_struktur_ot_bpjs_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200806_153330_dr_struktur_ot_bpjs_detail cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210221_112012_ac_last_status_ot_barang_masuk
 */
class m210221_112012_ac_last_status_ot_barang_masuk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('e_barang_masuk', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('lock'));
        $this->addColumn('e_barang_masuk_detail', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('lock'));
        $this->addColumn('e_permintaan', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('lock'));
        $this->addColumn('e_permintaan_detail', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('lock'));
        $this->addColumn('e_barang_keluar', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('lock'));
        $this->addColumn('e_barang_keluar_detail', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('lock'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210221_112012_ac_last_status_ot_barang_masuk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210221_112012_ac_last_status_ot_barang_masuk cannot be reverted.\n";

        return false;
    }
    */
}

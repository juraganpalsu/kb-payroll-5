<?php

use yii\db\Migration;

/**
 * Class m200811_152347_cc_pegawai_struktur_ot_bonus_detail
 */
class m200811_152347_cc_pegawai_struktur_ot_bonus_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `payroll_bonus_detail` 
DROP FOREIGN KEY `fk_bonus_detail_pegawai_struktur10`;
ALTER TABLE `payroll_bonus_detail` 
CHANGE COLUMN `pegawai_struktur_id` `pegawai_struktur_id` VARCHAR(32) NULL DEFAULT NULL ,
DROP INDEX `fk_bonus_detail_pegawai_struktur1_idx` ;

SQL;
        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200811_152347_cc_pegawai_struktur_ot_bonus_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200811_152347_cc_pegawai_struktur_ot_bonus_detail cannot be reverted.\n";

        return false;
    }
    */
}

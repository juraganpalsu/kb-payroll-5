<?php

use yii\db\Migration;

/**
 * Class m201015_233901_cc_id_ot_ds_absensi
 */
class m201015_233901_cc_id_ot_ds_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `ds_absensi` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201015_233901_cc_id_ot_ds_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201015_233901_cc_id_ot_ds_absensi cannot be reverted.\n";

        return false;
    }
    */
}

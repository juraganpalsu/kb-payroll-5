<?php

use yii\db\Migration;

/**
 * Class m200714_145051_ac_libur_hadir_s123
 */
class m200714_145051_ac_libur_hadir_s123 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'libur_hadir_s3', $this->integer(2)->notNull()->defaultValue(0)->after('libur_hadir'));
        $this->addColumn('gaji_bulanan', 'libur_hadir_s2', $this->integer(2)->notNull()->defaultValue(0)->after('libur_hadir'));
        $this->addColumn('gaji_bulanan', 'libur_hadir_s1', $this->integer(2)->notNull()->defaultValue(0)->after('libur_hadir'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200714_145051_ac_libur_hadir_s123 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200714_145051_ac_libur_hadir_s123 cannot be reverted.\n";

        return false;
    }
    */
}

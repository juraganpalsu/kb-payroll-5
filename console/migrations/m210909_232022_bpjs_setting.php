<?php

use yii\db\Migration;

/**
 * Class m210909_232022_bpjs_setting
 */
class m210909_232022_bpjs_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `p_bpjs_tk_setting` (
  `id` VARCHAR(36) NOT NULL,
  `jkk` TINYINT(1) NOT NULL DEFAULT 0,
  `jkm` TINYINT(1) NOT NULL DEFAULT 0,
  `jht` TINYINT(1) NOT NULL DEFAULT 0,
  `ip` TINYINT(1) NOT NULL DEFAULT 0,
  `tanggal_mulai` DATE NOT NULL,
  `is_latest` TINYINT(1) NOT NULL DEFAULT 1,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_p_bpjs_tk_setting_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_p_bpjs_tk_setting_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_p_bpjs_tk_setting_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_p_bpjs_tk_setting_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_p_bpjs_tk_setting_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_p_bpjs_tk_setting_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_p_bpjs_tk_setting_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_p_bpjs_tk_setting_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;


        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210909_232022_bpjs_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_232022_bpjs_setting cannot be reverted.\n";

        return false;
    }
    */
}

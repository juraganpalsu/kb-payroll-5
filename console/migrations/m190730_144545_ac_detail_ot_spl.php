<?php

use yii\db\Migration;

/**
 * Class m190730_144545_ac_detail_ot_spl
 */
class m190730_144545_ac_detail_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('spl_detail', 'id', $this->char(32)->notNull());
        $this->dropForeignKey('fk_spl_detail_2', 'spl_detail');
        $this->alterColumn('spl_detail', 'spl_id', $this->char(32)->notNull());

        $this->addColumn('spl_detail', 'perjanjian_kerja_id', $this->char(32)->null()->after('spl_id'));
        $this->addColumn('spl_detail', 'pegawai_struktur_id', $this->char(32)->null()->after('perjanjian_kerja_id'));
        $this->addColumn('spl_detail', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('pegawai_struktur_id'));
        $this->addColumn('spl_detail', 'created_by_pk', $this->char(32)->null()->after('created_by'));
        $this->addColumn('spl_detail', 'created_by_struktur', $this->integer(11)->notNull()->defaultValue(0)->after('created_by_pk'));

        $this->alterColumn('spl', 'id', $this->char(32)->notNull());
        $this->addColumn('spl', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('keterangan'));
        $this->addColumn('spl', 'created_by_pk', $this->char(32)->null()->after('created_by'));
        $this->addColumn('spl', 'created_by_struktur', $this->integer(11)->notNull()->defaultValue(0)->after('created_by_pk'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190730_144545_ac_detail_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190730_144545_ac_detail_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

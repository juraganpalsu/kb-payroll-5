<?php

use yii\db\Migration;

/**
 * Class m190226_161409_ac_status_ot_attendace
 */
class m190226_161409_ac_status_ot_attendace extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('attendance', 'status', $this->tinyInteger(1)->defaultValue(0)->notNull()->after('idfp'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190226_161409_ac_status_ot_attendace cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190226_161409_ac_status_ot_attendace cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m201226_101647_ct_mpp_jumlah_history
 */
class m201226_101647_ct_mpp_jumlah_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `mpp_jumlah_history` (
  `id` VARCHAR(36) NOT NULL,
  `tahun` INT(4) NOT NULL DEFAULT 0,
  `jumlah` INT(4) NOT NULL DEFAULT 0,
  `mpp_id` VARCHAR(36) NOT NULL,
  `keterangan` TEXT NULL,
  `mpp_jumlah_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_mpp_jumlah_history_mpp_jumlah1_idx` (`mpp_jumlah_id` ASC),
  CONSTRAINT `fk_mpp_jumlah_history_mpp_jumlah1`
    FOREIGN KEY (`mpp_jumlah_id`)
    REFERENCES `mpp_jumlah` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201226_101647_ct_mpp_jumlah_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201226_101647_ct_mpp_jumlah_history cannot be reverted.\n";

        return false;
    }
    */
}

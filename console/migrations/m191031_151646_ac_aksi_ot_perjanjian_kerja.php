<?php

use yii\db\Migration;

/**
 * Class m191031_151646_ac_aksi_ot_perjanjian_kerja
 */
class m191031_151646_ac_aksi_ot_perjanjian_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perjanjian_kerja', 'aksi', $this->integer(2)->notNull()->defaultValue(0)->after('id'));
        $this->addColumn('pegawai_struktur', 'aksi', $this->integer(2)->notNull()->defaultValue(0)->after('id'));
        $this->addColumn('pegawai_golongan', 'aksi', $this->integer(2)->notNull()->defaultValue(0)->after('id'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191031_151646_ac_aksi_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191031_151646_ac_aksi_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }
    */
}

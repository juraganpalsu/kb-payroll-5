<?php

use yii\db\Migration;

/**
 * Class m200805_021148_ac_payroll_periode_ot_payroll_bonus
 */
class m200805_021148_ac_payroll_periode_ot_payroll_bonus extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payroll_bonus', 'payroll_periode_id', $this->string(32)->after('catatan'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_021148_ac_payroll_periode_ot_payroll_bonus cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200805_021148_ac_payroll_periode_ot_payroll_bonus cannot be reverted.\n";

        return false;
    }
    */
}

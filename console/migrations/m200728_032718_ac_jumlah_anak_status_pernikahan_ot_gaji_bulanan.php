<?php

use yii\db\Migration;

/**
 * Class m200728_032718_ac_jumlah_anak_status_pernikahan_ot_gaji_bulanan
 */
class m200728_032718_ac_jumlah_anak_status_pernikahan_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'jumlah_anak', $this->integer(2)->notNull()->defaultValue(0)->after('atl_s3'));
        $this->addColumn('gaji_bulanan', 'status_pernikahan', $this->integer(2)->notNull()->defaultValue(0)->after('atl_s3'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200728_032718_ac_jumlah_anak_status_pernikahan_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200728_032718_ac_jumlah_anak_status_pernikahan_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

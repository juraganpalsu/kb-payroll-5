<?php

use yii\db\Migration;

/**
 * Class m210404_072045_ct_wsc_detail_discus
 */
class m210404_072045_ct_wsc_detail_discus extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `wsc_detail_tag_pagawai` (
  `id` VARCHAR(36) NOT NULL,
  `wsc_detail_id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `permission` INT(2) NOT NULL DEFAULT 0,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_wsc_detail_tag_pagawai_wsc_detail1_idx` (`wsc_detail_id` ASC),
  INDEX `fk_wsc_detail_tag_pagawai_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_wsc_detail_tag_pagawai_wsc_detail1`
    FOREIGN KEY (`wsc_detail_id`)
    REFERENCES `wsc_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wsc_detail_tag_pagawai_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wsc_detail_diskusi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wsc_detail_diskusi` (
  `id` VARCHAR(36) NOT NULL,
  `wsc_detail_id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `komentar` TEXT NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  `wsc_detail_diskusi_id` VARCHAR(36) NULL,
  INDEX `fk_wsc_detail_diskusi_wsc_detail1_idx` (`wsc_detail_id` ASC),
  INDEX `fk_wsc_detail_diskusi_pegawai1_idx` (`pegawai_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_wsc_detail_diskusi_wsc_detail_diskusi1_idx` (`wsc_detail_diskusi_id` ASC),
  CONSTRAINT `fk_wsc_detail_diskusi_wsc_detail1`
    FOREIGN KEY (`wsc_detail_id`)
    REFERENCES `wsc_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wsc_detail_diskusi_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wsc_detail_diskusi_wsc_detail_diskusi1`
    FOREIGN KEY (`wsc_detail_diskusi_id`)
    REFERENCES `wsc_detail_diskusi` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wsc_detail_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wsc_detail_attachment` (
  `id` VARCHAR(36) NOT NULL,
  `nama_file` VARCHAR(225) NOT NULL,
  `wsc_detail_id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_wsc_detail_attachment_wsc_detail1_idx` (`wsc_detail_id` ASC),
  INDEX `fk_wsc_detail_attachment_pegawai1_idx` (`pegawai_id` ASC),
  CONSTRAINT `fk_wsc_detail_attachment_wsc_detail1`
    FOREIGN KEY (`wsc_detail_id`)
    REFERENCES `wsc_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wsc_detail_attachment_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210404_072045_ct_wsc_detail_discus cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210404_072045_ct_wsc_detail_discus cannot be reverted.\n";

        return false;
    }
    */
}

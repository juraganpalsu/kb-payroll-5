<?php

use yii\db\Migration;

/**
 * Class m201103_142111_cc_payroll_periode_id_ot_payroll_diluar_proses
 */
class m201103_142111_cc_payroll_periode_id_ot_payroll_diluar_proses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_payroll_diluar_proses_payroll_periode1', 'payroll_diluar_proses');
        $this->dropColumn('payroll_diluar_proses', 'payroll_periode_id');

        $sql = <<<SQL
ALTER TABLE `payroll_diluar_proses` 
ADD COLUMN `proses_gaji_id` VARCHAR(32) NOT NULL AFTER `keterangan`,
ADD INDEX `fk_payroll_diluar_proses_1_idx` (`proses_gaji_id` ASC);
ALTER TABLE `payroll_diluar_proses` 
ADD CONSTRAINT `fk_payroll_diluar_proses_1`
  FOREIGN KEY (`proses_gaji_id`)
  REFERENCES `proses_gaji` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201103_142111_cc_payroll_periode_id_ot_payroll_diluar_proses cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201103_142111_cc_payroll_periode_id_ot_payroll_diluar_proses cannot be reverted.\n";

        return false;
    }
    */
}

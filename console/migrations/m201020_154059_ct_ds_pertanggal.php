<?php

use yii\db\Migration;

/**
 * Class m201020_154059_ct_ds_pertanggal
 */
class m201020_154059_ct_ds_pertanggal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `ds_per_tanggal` (
  `id` VARCHAR(32) NOT NULL,
  `jenis` INT(2) NOT NULL DEFAULT 0,
  `tanggal` DATE NOT NULL,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ds_absensi` (
  `id` INT NOT NULL,
  `status_kehadiran` INT(3) NOT NULL DEFAULT 0,
  `departemen_id` VARCHAR(32) NOT NULL,
  `departemen_string` VARCHAR(225) NULL,
  `jumlah` INT(4) NULL DEFAULT 0,
  `ds_per_tanggal_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ds_absensi_departemen1_idx` (`departemen_id` ASC),
  INDEX `fk_ds_absensi_ds_per_tanggal1_idx` (`ds_per_tanggal_id` ASC),
  CONSTRAINT `fk_ds_absensi_departemen1`
    FOREIGN KEY (`departemen_id`)
    REFERENCES `departemen` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ds_absensi_ds_per_tanggal1`
    FOREIGN KEY (`ds_per_tanggal_id`)
    REFERENCES `ds_per_tanggal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201020_154059_ct_ds_pertanggal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201020_154059_ct_ds_pertanggal cannot be reverted.\n";

        return false;
    }
    */
}

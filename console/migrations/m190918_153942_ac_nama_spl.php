<?php

use yii\db\Migration;

/**
 * Class m190918_153942_ac_nama_spl
 */
class m190918_153942_ac_nama_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `spl_template` 
CHANGE COLUMN `nama` `nama` VARCHAR(225) NOT NULL ;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190918_153942_ac_nama_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190918_153942_ac_nama_spl cannot be reverted.\n";

        return false;
    }
    */
}

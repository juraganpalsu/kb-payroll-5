<?php

use yii\db\Migration;

/**
 * Class m181113_221828_truncate_table
 */
class m181113_221828_truncate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->truncateTable('ganti_hari_jumlah');
        $this->truncateTable('ganti_hari_detail');
        $sql = 'delete from ganti_hari where id > 0';
        $this->execute($sql);
        $sql = 'delete from spl where id > 0';
        $this->execute($sql);
        $sql = 'delete from pegawai where idfp > 0';
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181113_221828_truncate_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181113_221828_truncate_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210303_010506_insentif_gt_ot_gaji_bulanan
 */
class m210303_010506_insentif_gt_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'insentif_gt', $this->integer(11)->defaultValue(0)->after('insentif_pph'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210303_010506_insentif_gt_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210303_010506_insentif_gt_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

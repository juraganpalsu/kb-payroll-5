<?php

use yii\db\Migration;

/**
 * Class m190723_134814_it_perjanjian_kerja_jenis
 */
class m190723_134814_it_perjanjian_kerja_jenis extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL


INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('1', 'ADDENDUM1');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('2', 'ADDENDUM2');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('3', 'ADDENDUM3');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('4', 'ADDENDUM4');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('5', 'FREELANCE');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('6', 'MAGANG1');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('7', 'MAGANG2');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('8', 'KONTRAK1');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('9', 'KONTRAK2');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('10', 'PKL');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('11', 'TETAP');
INSERT INTO `perjanjian_kerja_jenis` (`id`, `nama`) VALUES ('12', 'OUTSOURCING');

SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190723_134814_it_perjanjian_kerja_jenis cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190723_134814_it_perjanjian_kerja_jenis cannot be reverted.\n";

        return false;
    }
    */
}

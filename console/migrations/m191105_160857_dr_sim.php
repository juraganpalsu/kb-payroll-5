<?php

use yii\db\Migration;

/**
 * Class m191105_160857_dr_sim
 */
class m191105_160857_dr_sim extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
ALTER TABLE `surat_izin_mengemudi` 
DROP FOREIGN KEY `fk_surat_izin_mengemudi_perjanjian_kerja1`,
DROP FOREIGN KEY `fk_surat_izin_mengemudi_pegawai_struktur1`,
DROP FOREIGN KEY `fk_surat_izin_mengemudi_pegawai_golongan1`;
ALTER TABLE `surat_izin_mengemudi` 
DROP INDEX `fk_surat_izin_mengemudi_pegawai_struktur1_idx` ,
DROP INDEX `fk_surat_izin_mengemudi_pegawai_golongan1_idx` ,
DROP INDEX `fk_surat_izin_mengemudi_perjanjian_kerja1_idx` ;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191105_160857_dr_sim cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191105_160857_dr_sim cannot be reverted.\n";

        return false;
    }
    */
}

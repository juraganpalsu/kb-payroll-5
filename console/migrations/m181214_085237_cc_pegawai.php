<?php

use yii\db\Migration;

/**
 * Class m181214_085237_cc_pegawai
 */
class m181214_085237_cc_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `pegawai` 
CHANGE COLUMN `id_pegawai` `id_pegawai` VARCHAR(11) NOT NULL ;
SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181214_085237_cc_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181214_085237_cc_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

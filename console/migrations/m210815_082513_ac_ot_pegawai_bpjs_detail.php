<?php

use yii\db\Migration;

/**
 * Class m210815_082513_ac_ot_pegawai_bpjs_detail
 */
class m210815_082513_ac_ot_pegawai_bpjs_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payroll_bpjs_detail', 'gaji_pokok', $this->decimal(11,2)->notNull()->defaultValue(0));
        $this->addColumn('payroll_bpjs_detail', 'iuran_persen', $this->decimal(3,2)->notNull()->defaultValue(0));
        $this->addColumn('payroll_bpjs_detail', 'iuran_nominal', $this->decimal(11,2)->notNull()->defaultValue(0));
        $this->addColumn('payroll_bpjs_detail', 'iuran_persen_tk', $this->decimal(3,2)->notNull()->defaultValue(0));
        $this->addColumn('payroll_bpjs_detail', 'iuran_nominal_tk', $this->decimal(11,2)->notNull()->defaultValue(0));
        $this->addColumn('payroll_bpjs_detail', 'total_iuran', $this->decimal(11,2)->notNull()->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210815_082513_ac_ot_pegawai_bpjs_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210815_082513_ac_ot_pegawai_bpjs_detail cannot be reverted.\n";

        return false;
    }
    */
}

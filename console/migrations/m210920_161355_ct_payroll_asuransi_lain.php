<?php

use yii\db\Migration;

/**
 * Class m210920_161355_ct_payroll_asuransi_lain
 */
class m210920_161355_ct_payroll_asuransi_lain extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `payroll_asuransi_lain` (
  `id` VARCHAR(36) NOT NULL,
  `catatan` TEXT NULL,
  `payroll_periode_id` VARCHAR(32) NOT NULL,
  `golongan_id` INT NOT NULL,
  `bisnis_unit` INT(2) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_payroll_asuransi_lain_payroll_periode1_idx` (`payroll_periode_id` ASC),
  INDEX `fk_payroll_asuransi_lain_golongan1_idx` (`golongan_id` ASC),
  CONSTRAINT `fk_payroll_asuransi_lain_payroll_periode1`
    FOREIGN KEY (`payroll_periode_id`)
    REFERENCES `payroll_periode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_asuransi_lain_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `payroll_asuransi_lain_detail` (
  `id` VARCHAR(36) NOT NULL,
  `potongan_perusahaan` FLOAT(11,2) NOT NULL DEFAULT 0,
  `potongan_karyawan` FLOAT(11,2) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `payroll_asuransi_lain_id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `pegawai_asuransi_lain_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_payroll_asuransi_lain_detail_payroll_asuransi_lain1_idx` (`payroll_asuransi_lain_id` ASC),
  INDEX `fk_payroll_asuransi_lain_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_payroll_asuransi_lain_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_payroll_asuransi_lain_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_payroll_asuransi_lain_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  INDEX `fk_payroll_asuransi_lain_detail_pegawai_asuransi_lain1_idx` (`pegawai_asuransi_lain_id` ASC),
  CONSTRAINT `fk_payroll_asuransi_lain_detail_payroll_asuransi_lain1`
    FOREIGN KEY (`payroll_asuransi_lain_id`)
    REFERENCES `payroll_asuransi_lain` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_asuransi_lain_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_asuransi_lain_detail_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_asuransi_lain_detail_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_asuransi_lain_detail_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_asuransi_lain_detail_pegawai_asuransi_lain1`
    FOREIGN KEY (`pegawai_asuransi_lain_id`)
    REFERENCES `pegawai_asuransi_lain` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210920_161355_ct_payroll_asuransi_lain cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210920_161355_ct_payroll_asuransi_lain cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200704_154629_ac_tunj_lain_lain_ot_gaji_bulanan
 */
class m200704_154629_ac_tunj_lain_lain_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'p_tunjagan_lain_lain_id', $this->char(32)->after('p_tunjagan_lain_tetap_id'));
        $this->addColumn('gaji_bulanan', 'p_tunjagan_lain_lain', $this->integer(11)->after('p_tunjagan_lain_tetap'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200704_154629_ac_tunj_lain_lain_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200704_154629_ac_tunj_lain_lain_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

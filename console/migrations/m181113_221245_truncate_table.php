<?php

use yii\db\Migration;

/**
 * Class m181113_221245_truncate_table
 */
class m181113_221245_truncate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->truncateTable('attendance');
        $this->truncateTable('absensi');
        $this->truncateTable('ijin');
        $this->truncateTable('libur');
        $this->truncateTable('libur_pegawai');
        $this->truncateTable('spl_detail');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181113_221245_truncate_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181113_221245_truncate_table cannot be reverted.\n";

        return false;
    }
    */
}

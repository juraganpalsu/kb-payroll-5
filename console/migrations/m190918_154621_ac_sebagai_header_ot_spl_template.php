<?php

use yii\db\Migration;

/**
 * Class m190918_154621_ac_sebagai_header_ot_spl_template
 */
class m190918_154621_ac_sebagai_header_ot_spl_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$this->addColumn('spl_template', 'sebagai_header', $this->integer(2)->notNull()->defaultValue(0)->after('sistem_kerja'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190918_154621_ac_sebagai_header_ot_spl_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190918_154621_ac_sebagai_header_ot_spl_template cannot be reverted.\n";

        return false;
    }
    */
}

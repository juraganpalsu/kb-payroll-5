<?php

use yii\db\Migration;

/**
 * Class m210921_144928_ac_kesehatan_ot_bpjs_setting
 */
class m210921_144928_ac_kesehatan_ot_bpjs_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('p_bpjs_tk_setting', 'kesehatan', $this->boolean()->notNull()->defaultValue(0)->after('ip'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210921_144928_ac_kesehatan_ot_bpjs_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210921_144928_ac_kesehatan_ot_bpjs_setting cannot be reverted.\n";

        return false;
    }
    */
}

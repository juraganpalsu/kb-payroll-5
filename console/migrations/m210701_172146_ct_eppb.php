<?php

use yii\db\Migration;

/**
 * Class m210701_172146_ct_eppb
 */
class m210701_172146_ct_eppb extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `e_ppb` (
  `id` VARCHAR(36) NOT NULL,
  `no` VARCHAR(45) NOT NULL,
  `tanggal` DATE NOT NULL,
  `e_gudang_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_ppb_e_gudang1_idx` (`e_gudang_id` ASC),
  CONSTRAINT `fk_e_ppb_e_gudang1`
    FOREIGN KEY (`e_gudang_id`)
    REFERENCES `e_gudang` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `e_ppb_item` (
  `id` VARCHAR(36) NOT NULL,
  `e_ppb_id` VARCHAR(36) NOT NULL,
  `jumlah` INT NOT NULL,
  `sisa` INT NOT NULL DEFAULT 0,
  `buffer_stock` INT NULL,
  `kebutuhan` INT NULL,
  `keterangan` TEXT NULL,
  `e_stok_id` VARCHAR(36) NOT NULL,
  `e_satuan_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_e_ppb_item_e_ppb1_idx` (`e_ppb_id` ASC),
  INDEX `fk_e_ppb_item_e_stok1_idx` (`e_stok_id` ASC),
  INDEX `fk_e_ppb_item_e_satuan1_idx` (`e_satuan_id` ASC),
  CONSTRAINT `fk_e_ppb_item_e_ppb1`
    FOREIGN KEY (`e_ppb_id`)
    REFERENCES `e_ppb` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_ppb_item_e_stok1`
    FOREIGN KEY (`e_stok_id`)
    REFERENCES `e_stok` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_ppb_item_e_satuan1`
    FOREIGN KEY (`e_satuan_id`)
    REFERENCES `e_satuan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210701_172146_ct_eppb cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210701_172146_ct_eppb cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210311_152418_ac_tanggal_terima_ot_barang_masuk
 */
class m210311_152418_ac_tanggal_terima_ot_barang_masuk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('e_barang_masuk', 'tanggal_terima', $this->date()->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210311_152418_ac_tanggal_terima_ot_barang_masuk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210311_152418_ac_tanggal_terima_ot_barang_masuk cannot be reverted.\n";

        return false;
    }
    */
}

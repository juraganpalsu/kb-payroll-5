<?php

use yii\db\Migration;

/**
 * Class m201014_065527_dt_fk_struktur_golongan_ot_surat_pegawai
 */
class m201014_065527_dt_fk_struktur_golongan_ot_surat_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `surat_pegawai` 
ADD PRIMARY KEY (`id`);
ALTER TABLE surat_pegawai`

SQL;
//        $sql = <<<SQL
//DROP FOREIGN KEY `fk_surat_pegawai_struktur1`,
//DROP FOREIGN KEY `fk_surat_pegawai_golongan1`;
//ALTER TABLE `surat_pegawai`
//DROP INDEX `fk_surat_pegawai_golongan1_idx` ,
//DROP INDEX `fk_pegawai_struktur_struktur1_idx` ;
//SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201014_065527_dt_fk_struktur_golongan_ot_surat_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201014_065527_dt_fk_struktur_golongan_ot_surat_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

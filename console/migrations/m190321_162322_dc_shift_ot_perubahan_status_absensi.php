<?php

use yii\db\Migration;

/**
 * Class m190321_162322_dc_shift_ot_perubahan_status_absensi
 */
class m190321_162322_dc_shift_ot_perubahan_status_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perubahan_status_absensi', 'time_table_id', $this->integer(11)->notNull()->defaultValue(0)->after('keterangan'));
        $this->dropColumn('perubahan_status_absensi', 'shift');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190321_162322_dc_shift_ot_perubahan_status_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190321_162322_dc_shift_ot_perubahan_status_absensi cannot be reverted.\n";

        return false;
    }
    */
}

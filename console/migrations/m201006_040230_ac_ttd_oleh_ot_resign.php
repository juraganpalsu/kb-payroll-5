<?php

use yii\db\Migration;

/**
 * Class m201006_040230_ac_ttd_oleh_ot_resign
 */
class m201006_040230_ac_ttd_oleh_ot_resign extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resign', 'oleh_pegawai_struktur_id', $this->string(32)->notNull()->defaultValue(0)->after('area_kerja_id'));
        $this->addColumn('resign', 'oleh_perjanjian_kerja_id', $this->string(32)->notNull()->defaultValue(0)->after('area_kerja_id'));
        $this->addColumn('resign', 'oleh_pegawai_id', $this->string(32)->notNull()->defaultValue(0)->after('area_kerja_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201006_040230_ac_ttd_oleh_ot_resign cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201006_040230_ac_ttd_oleh_ot_resign cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190915_143525_ct_pegawai_golongan
 */
class m190915_143525_ct_pegawai_golongan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `pegawai_golongan` (
  `id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `golongan_id` INT NOT NULL,
  `mulai_berlaku` DATE NOT NULL,
  `akhir_berlaku` DATE NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_org_chart` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_golongan_pegawai_golongan1_idx` (`pegawai_id` ASC),
  INDEX `fk_pegawai_golongan_golongan1_idx` (`golongan_id` ASC),
  CONSTRAINT `fk_pegawai_golongan_pegawai_golongan1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_golongan_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);

        $sql = <<<SQL
ALTER TABLE `cost_center` 
DROP FOREIGN KEY `fk_cost_center_perjanjian_kerja1`,
DROP FOREIGN KEY `fk_cost_center_pegawai_struktur1`;
ALTER TABLE `cost_center` 
DROP COLUMN `pegawai_struktur_id`,
DROP COLUMN `perjanjian_kerja_id`,
DROP INDEX `fk_cost_center_pegawai_struktur1_idx` ,
DROP INDEX `fk_cost_center_perjanjian_kerja1_idx` ;

SQL;

        $this->execute($sql);

        $this->addColumn('absensi', 'cost_center_id', $this->char(32)->defaultValue(0)->after('pegawai_struktur_id'));
        $this->addColumn('absensi', 'pegawai_golongan_id', $this->char(32)->defaultValue(0)->after('cost_center_id'));
        $this->addColumn('absensi', 'pegawai_sistem_kerja_id', $this->integer(11)->defaultValue(0)->after('pegawai_golongan_id'));


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190915_143525_ct_pegawai_golongan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190915_143525_ct_pegawai_golongan cannot be reverted.\n";

        return false;
    }
    */
}

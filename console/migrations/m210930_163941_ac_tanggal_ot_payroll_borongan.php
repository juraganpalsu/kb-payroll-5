<?php

use yii\db\Migration;

/**
 * Class m210930_163941_ac_tanggal_ot_payroll_borongan
 */
class m210930_163941_ac_tanggal_ot_payroll_borongan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payroll_borongan', 'tanggal', $this->date()->notNull()->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210930_163941_ac_tanggal_ot_payroll_borongan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210930_163941_ac_tanggal_ot_payroll_borongan cannot be reverted.\n";

        return false;
    }
    */
}

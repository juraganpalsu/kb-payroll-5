<?php

use yii\db\Migration;

/**
 * Class m201026_034935_ac_atasan_pegawai_ot_surat_peringatan
 */
class m201026_034935_ac_atasan_pegawai_ot_surat_peringatan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('surat_peringatan', 'atasan_pegawai_struktur_id', $this->string(32)->notNull()->defaultValue(0)->after('oleh_pegawai_struktur_id'));
        $this->addColumn('surat_peringatan', 'atasan_pegawai_id', $this->string(32)->notNull()->defaultValue(0)->after('oleh_pegawai_struktur_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201026_034935_ac_atasan_pegawai_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201026_034935_ac_atasan_pegawai_ot_surat_peringatan cannot be reverted.\n";

        return false;
    }
    */
}

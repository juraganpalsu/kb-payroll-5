<?php

use yii\db\Migration;

/**
 * Class m201019_022543_ct_insentif_pph_ot_gaji_bulanan
 */
class m201019_022543_ct_insentif_pph_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'insentif_pph', $this->integer(11)->defaultValue(0)->after('insentif_tidak_tetap'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201019_022543_ct_insentif_pph_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201019_022543_ct_insentif_pph_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

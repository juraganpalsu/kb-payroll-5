<?php

use yii\db\Migration;

/**
 * Class m201106_150521_ct_appraisal_pegawai
 */
class m201106_150521_ct_appraisal_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `ref_appraisal` (
  `id` VARCHAR(32) NOT NULL,
  `kategori` TINYINT(1) NOT NULL,
  `kriteria` VARCHAR(255) NOT NULL,
  `keterangan_kiri` TEXT NOT NULL,
  `keterangan_kanan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `appraisal` (
  `id` VARCHAR(32) NOT NULL,
  `kategori` TINYINT(1) NOT NULL DEFAULT 0,
  `jenis_evaluasi` TINYINT(1) NOT NULL DEFAULT 0,
  `tanggal_awal` DATE NOT NULL,
  `tanggal_akhir` DATE NOT NULL,
  `alpa` INT(2) NOT NULL DEFAULT 0,
  `izin` INT(2) NOT NULL DEFAULT 0,
  `terlambat` INT(2) NOT NULL DEFAULT 0,
  `cuti` INT(2) NOT NULL DEFAULT 0,
  `total_nilai` INT(3) NOT NULL DEFAULT 0,
  `rata_rata` FLOAT NOT NULL DEFAULT 0,
  `rekomendasi` TINYINT(1) NOT NULL DEFAULT 0,
  `komentar_tambahan` TEXT NULL,
  `kesimpulan` VARCHAR(255) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `oleh_pegawai_id` VARCHAR(32) NOT NULL DEFAULT '0',
  `oleh_perjanjian_kerja_id` VARCHAR(32) NOT NULL DEFAULT '0',
  `oleh_pegawai_struktur_id` VARCHAR(32) NOT NULL DEFAULT '0',
  `atasan_pegawai_id` VARCHAR(32) NOT NULL DEFAULT '0',
  `atasan_pegawai_struktur_id` VARCHAR(32) NOT NULL DEFAULT '0',
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_appraisal_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_appraisal_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_appraisal_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_appraisal_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_appraisal_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_appraisal_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_appraisal_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_appraisal_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `appraisal_detail` (
  `id` VARCHAR(32) NOT NULL,
  `komentar` TEXT NOT NULL,
  `nilai` INT(1) NOT NULL,
  `ref_appraisal_id` VARCHAR(32) NOT NULL,
  `appraisal_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_appraisal_detail_ref_appraisal1_idx` (`ref_appraisal_id` ASC),
  INDEX `fk_appraisal_detail_appraisal1_idx` (`appraisal_id` ASC),
  CONSTRAINT `fk_appraisal_detail_ref_appraisal1`
    FOREIGN KEY (`ref_appraisal_id`)
    REFERENCES `ref_appraisal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_appraisal_detail_appraisal1`
    FOREIGN KEY (`appraisal_id`)
    REFERENCES `appraisal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201106_150521_ct_appraisal_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201106_150521_ct_appraisal_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

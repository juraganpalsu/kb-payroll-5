<?php

use yii\db\Migration;

/**
 * Class m200514_150109_ac_persentase_pembayaran_ot_thr
 */
class m200514_150109_ac_persentase_pembayaran_ot_thr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('thr', 'persentase_pembayaran', $this->integer(3)->notNull()->defaultValue(0)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200514_150109_ac_persentase_pembayaran_ot_thr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200514_150109_ac_persentase_pembayaran_ot_thr cannot be reverted.\n";

        return false;
    }
    */
}

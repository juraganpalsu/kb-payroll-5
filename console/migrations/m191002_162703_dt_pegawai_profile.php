<?php

use yii\db\Migration;

/**
 * Class m191002_162703_dt_pegawai_profile
 */
class m191002_162703_dt_pegawai_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('pegawai_profile');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191002_162703_dt_pegawai_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_162703_dt_pegawai_profile cannot be reverted.\n";

        return false;
    }
    */
}

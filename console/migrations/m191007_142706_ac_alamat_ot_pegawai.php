<?php

use yii\db\Migration;

/**
 * Class m191007_142706_ac_alamat_ot_pegawai
 */
class m191007_142706_ac_alamat_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai', 'ktp_provinsi_id', $this->integer(11)->defaultValue(0)->after('alamat_email'));
        $this->addColumn('pegawai', 'ktp_kabupaten_kota_id', $this->integer(11)->defaultValue(0)->after('ktp_provinsi_id'));
        $this->addColumn('pegawai', 'ktp_kecamatan_id', $this->integer(11)->defaultValue(0)->after('ktp_kabupaten_kota_id'));
        $this->addColumn('pegawai', 'ktp_desa_id', $this->bigInteger(16)->defaultValue(0)->after('ktp_kecamatan_id'));
        $this->addColumn('pegawai', 'ktp_rw', $this->char(3)->defaultValue(0)->after('ktp_desa_id'));
        $this->addColumn('pegawai', 'ktp_rt', $this->char(3)->defaultValue(0)->after('ktp_rw'));
        $this->addColumn('pegawai', 'ktp_alamat', $this->text()->defaultValue(null)->after('ktp_rt'));

        $this->addColumn('pegawai', 'domisili_provinsi_id', $this->integer(11)->defaultValue(0)->after('ktp_rt'));
        $this->addColumn('pegawai', 'domisili_kabupaten_kota_id', $this->integer(11)->defaultValue(0)->after('domisili_provinsi_id'));
        $this->addColumn('pegawai', 'domisili_kecamatan_id', $this->integer(11)->defaultValue(0)->after('domisili_kabupaten_kota_id'));
        $this->addColumn('pegawai', 'domisili_desa_id', $this->bigInteger(16)->defaultValue(0)->after('domisili_kecamatan_id'));
        $this->addColumn('pegawai', 'domisili_rw', $this->char(3)->defaultValue(0)->after('domisili_desa_id'));
        $this->addColumn('pegawai', 'domisili_rt', $this->char(3)->defaultValue(0)->after('domisili_rw'));
        $this->addColumn('pegawai', 'domisili_alamat', $this->text()->defaultValue(null)->after('domisili_rt'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191007_142706_ac_alamat_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191007_142706_ac_alamat_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

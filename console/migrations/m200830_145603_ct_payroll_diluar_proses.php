<?php

use yii\db\Migration;

/**
 * Class m200830_145603_ct_payroll_diluar_proses
 */
class m200830_145603_ct_payroll_diluar_proses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL

CREATE TABLE `payroll_diluar_proses` (
  `id` varchar(32) NOT NULL,
  `jenis` int(2) NOT NULL DEFAULT '0',
  `jumlah` int(11) NOT NULL DEFAULT '0',
  `tanggal_bayar` date NOT NULL,
  `keterangan` TEXT NULL,
  `payroll_periode_id` varchar(32) NOT NULL,
  `pegawai_id` varchar(32) NOT NULL,
  `perjanjian_kerja_id` varchar(32) NOT NULL,
  `pegawai_golongan_id` varchar(32) NOT NULL,
  `pegawai_struktur_id` varchar(32) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `created_by_pk` varchar(32) DEFAULT '0',
  `created_by_struktur` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT '0',
  `lock` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_payroll_diluar_proses_payroll_periode1_idx` (`payroll_periode_id`),
  KEY `fk_payroll_diluar_proses_pegawai1_idx` (`pegawai_id`),
  KEY `fk_payroll_diluar_proses_perjanjian_kerja1_idx` (`perjanjian_kerja_id`),
  KEY `fk_payroll_diluar_proses_pegawai_golongan1_idx` (`pegawai_golongan_id`),
  KEY `fk_payroll_diluar_proses_pegawai_struktur1_idx` (`pegawai_struktur_id`),
  CONSTRAINT `fk_payroll_diluar_proses_payroll_periode1` FOREIGN KEY (`payroll_periode_id`) REFERENCES `payroll_periode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_diluar_proses_pegawai1` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_diluar_proses_pegawai_golongan1` FOREIGN KEY (`pegawai_golongan_id`) REFERENCES `pegawai_golongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_diluar_proses_pegawai_struktur1` FOREIGN KEY (`pegawai_struktur_id`) REFERENCES `pegawai_struktur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_diluar_proses_perjanjian_kerja1` FOREIGN KEY (`perjanjian_kerja_id`) REFERENCES `perjanjian_kerja` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200830_145603_ct_payroll_diluar_proses cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200830_145603_ct_payroll_diluar_proses cannot be reverted.\n";

        return false;
    }
    */
}

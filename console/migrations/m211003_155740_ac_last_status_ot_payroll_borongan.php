<?php

use yii\db\Migration;

/**
 * Class m211003_155740_ac_last_status_ot_payroll_borongan
 */
class m211003_155740_ac_last_status_ot_payroll_borongan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payroll_borongan', 'last_status', $this->integer(1)->after('jenis_borongan_id')->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211003_155740_ac_last_status_ot_payroll_borongan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211003_155740_ac_last_status_ot_payroll_borongan cannot be reverted.\n";

        return false;
    }
    */
}

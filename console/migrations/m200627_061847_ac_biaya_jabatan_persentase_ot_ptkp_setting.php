<?php

use yii\db\Migration;

/**
 * Class m200627_061847_ac_biaya_jabatan_persentase_ot_ptkp_setting
 */
class m200627_061847_ac_biaya_jabatan_persentase_ot_ptkp_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ptpk_setting', 'persen_biaya_jabatan', $this->float()->notNull()->defaultValue(0)->after('k3'));
        $this->addColumn('ptpk_setting', 'maks_nominal_jabatan', $this->integer(11)->notNull()->defaultValue(0)->after('persen_biaya_jabatan'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200627_061847_ac_biaya_jabatan_persentase_ot_ptkp_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200627_061847_ac_biaya_jabatan_persentase_ot_ptkp_setting cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200330_153233_ut_master_data_default
 */
class m200330_153233_ut_master_data_default extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    $sql = <<<SQL
ALTER TABLE `master_data_default` 
DROP FOREIGN KEY `fk_master_data_default_perjanjian_kerja1`,
DROP FOREIGN KEY `fk_master_data_default_pegawai_struktur1`,
DROP FOREIGN KEY `fk_master_data_default_pegawai_golongan1`;
ALTER TABLE `master_data_default` 
CHANGE COLUMN `perjanjian_kerja_id` `perjanjian_kerja_id` VARCHAR(32) NULL DEFAULT 0 ,
CHANGE COLUMN `pegawai_struktur_id` `pegawai_struktur_id` VARCHAR(32) NULL DEFAULT 0 ,
CHANGE COLUMN `pegawai_golongan_id` `pegawai_golongan_id` VARCHAR(32) NULL DEFAULT 0 ,
DROP INDEX `fk_master_data_default_pegawai_golongan1_idx` ,
DROP INDEX `fk_master_data_default_pegawai_struktur1_idx` ,
DROP INDEX `fk_master_data_default_perjanjian_kerja1_idx` ;

SQL;
    
    $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200330_153233_ut_master_data_default cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200330_153233_ut_master_data_default cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191030_143807_dt_ref_area_kerja
 */
class m191030_143807_dt_ref_area_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $sql = <<<SQL
ALTER TABLE `area_kerja` 
DROP FOREIGN KEY `fk_area_kerja_ref_area_kerja1`;
ALTER TABLE `area_kerja` 
DROP INDEX `fk_area_kerja_ref_area_kerja1_idx` ;
SQL;
        $this->execute($sql);

        $this->dropTable('ref_area_kerja');

        $sqlCreate = <<<SQL
CREATE TABLE ref_area_kerja (
    id            INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
    root          INT(11)               DEFAULT NULL,
    lft           INT(11)      NOT NULL,
    rgt           INT(11)      NOT NULL,
    lvl           SMALLINT(5)  NOT NULL,
    name          VARCHAR(60)  NOT NULL,
    icon          VARCHAR(255)          DEFAULT NULL,
    icon_type     TINYINT(1)   NOT NULL DEFAULT '1',
    active        TINYINT(1)   NOT NULL DEFAULT TRUE,
    selected      TINYINT(1)   NOT NULL DEFAULT FALSE,
    disabled      TINYINT(1)   NOT NULL DEFAULT FALSE,
    readonly      TINYINT(1)   NOT NULL DEFAULT FALSE,
    visible       TINYINT(1)   NOT NULL DEFAULT TRUE,
    collapsed     TINYINT(1)   NOT NULL DEFAULT FALSE,
    movable_u     TINYINT(1)   NOT NULL DEFAULT TRUE,
    movable_d     TINYINT(1)   NOT NULL DEFAULT TRUE,
    movable_l     TINYINT(1)   NOT NULL DEFAULT TRUE,
    movable_r     TINYINT(1)   NOT NULL DEFAULT TRUE,
    removable     TINYINT(1)   NOT NULL DEFAULT TRUE,
    removable_all TINYINT(1)   NOT NULL DEFAULT FALSE,
    child_allowed TINYINT(1)   NOT NULL DEFAULT FALSE,
    KEY tbl_product_NK1 (root),
    KEY tbl_product_NK2 (lft),
    KEY tbl_product_NK3 (rgt),
    KEY tbl_product_NK4 (lvl),
    KEY tbl_product_NK5 (active)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
SQL;

        $this->execute($sqlCreate);

        $sqlUpdate = <<<SQL
ALTER TABLE `area_kerja` 
CHANGE COLUMN `ref_area_kerja_id` `ref_area_kerja_id` INT(11) NOT NULL ;

SQL;
        $this->execute($sqlUpdate);


        $this->addForeignKey('fk_area_kerja_ref_area_kerja1', 'area_kerja', 'ref_area_kerja_id', 'ref_area_kerja', 'id');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191030_143807_dt_ref_area_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191030_143807_dt_ref_area_kerja cannot be reverted.\n";

        return false;
    }
    */
}

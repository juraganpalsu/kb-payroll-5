<?php

use yii\db\Migration;

/**
 * Class m181114_135039_ct_pegawai_sistem_kerja
 */
class m181114_135039_ct_pegawai_sistem_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL

CREATE TABLE IF NOT EXISTS `pegawai_sistem_kerja` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mulai_berlaku` DATE NOT NULL,
  `akhir_berlaku` DATE NULL,
  `keterangan` TEXT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `sistem_kerja_id` INT NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pegawai_sistem_kerja_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_pegawai_sistem_kerja_sistem_kerja1_idx` (`sistem_kerja_id` ASC),
  CONSTRAINT `fk_pegawai_sistem_kerja_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_sistem_kerja_sistem_kerja1`
    FOREIGN KEY (`sistem_kerja_id`)
    REFERENCES `sistem_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181114_135039_ct_pegawai_sistem_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181114_135039_ct_pegawai_sistem_kerja cannot be reverted.\n";

        return false;
    }
    */
}

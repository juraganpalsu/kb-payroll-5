<?php

use yii\db\Migration;

/**
 * Class m200219_150328_ac_is_revision_ot_spl
 */
class m200219_150328_ac_is_revision_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('spl', 'is_revision', $this->boolean()->defaultValue(0)->notNull()->after('is_planing'));
        $this->addColumn('spl', 'spl_id', $this->char(32)->defaultValue(0)->after('cost_center_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200219_150328_ac_is_revision_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200219_150328_ac_is_revision_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

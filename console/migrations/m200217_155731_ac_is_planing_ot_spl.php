<?php

use yii\db\Migration;

/**
 * Class m200217_155731_ac_is_planing_ot_spl
 */
class m200217_155731_ac_is_planing_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('spl', 'is_planing', $this->boolean()->notNull()->after('last_status')->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200217_155731_ac_is_planing_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200217_155731_ac_is_planing_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

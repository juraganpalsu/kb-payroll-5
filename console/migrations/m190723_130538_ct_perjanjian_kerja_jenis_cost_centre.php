<?php

use yii\db\Migration;

/**
 * Class m190723_130538_ct_perjanjian_kerja_jenis_cost_centre
 */
class m190723_130538_ct_perjanjian_kerja_jenis_cost_centre extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
        
CREATE TABLE IF NOT EXISTS `perjanjian_kerja_jenis` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(45) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `cost_center` (
  `id` VARCHAR(32) NOT NULL,
  `kode` INT(11) NOT NULL,
  `area_kerja` VARCHAR(225) NOT NULL,
  `mulai_berlaku` DATE NOT NULL,
  `akhir_berlaku` DATE NULL,
  `status` INT(2) NULL DEFAULT 0 COMMENT '1.Magang\n2.Kontrak\n3.Tetap',
  `keterangan` TEXT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_cost_center_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_cost_center_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_cost_center_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_cost_center_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cost_center_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cost_center_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




SQL;

        $this->execute($sql);

        $this->addColumn('akun_beatroute', 'pegawai_struktur_id', $this->string(32)->notNull()->after('perjanjian_kerja_id')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190723_130538_ct_perjanjian_kerja_jenis_cost_centre cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190723_130538_ct_perjanjian_kerja_jenis_cost_centre cannot be reverted.\n";

        return false;
    }
    */
}

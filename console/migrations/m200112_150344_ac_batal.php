<?php

use yii\db\Migration;

/**
 * Class m200112_150344_ac_batal
 */
class m200112_150344_ac_batal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cuti', 'batal', $this->integer(1)->notNull()->defaultValue(0)->after('keterangan'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200112_150344_ac_batal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200112_150344_ac_batal cannot be reverted.\n";

        return false;
    }
    */
}

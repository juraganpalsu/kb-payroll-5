<?php

use yii\db\Migration;

/**
 * Class m191209_154155_ct_penyelesaian_kasus_detail
 */
class m191209_154155_ct_penyelesaian_kasus_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `penyelesaian_kasus` 
CHANGE COLUMN `tanggal_selesai` `tanggal_selesai` DATE NULL ;

SQL;


        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191209_154155_ct_penyelesaian_kasus_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191209_154155_ct_penyelesaian_kasus_detail cannot be reverted.\n";

        return false;
    }
    */
}

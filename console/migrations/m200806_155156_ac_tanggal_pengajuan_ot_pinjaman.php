<?php

use yii\db\Migration;

/**
 * Class m200806_155156_ac_tanggal_pengajuan_ot_pinjaman
 */
class m200806_155156_ac_tanggal_pengajuan_ot_pinjaman extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pinjaman', 'tanggal_pengajuan', $this->date()->null()->after('payroll_periode_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200806_155156_ac_tanggal_pengajuan_ot_pinjaman cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200806_155156_ac_tanggal_pengajuan_ot_pinjaman cannot be reverted.\n";

        return false;
    }
    */
}

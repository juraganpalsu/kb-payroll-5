<?php

use yii\db\Migration;

/**
 * Class m190923_145633_ac_spl_template_id_ot_absensi
 */
class m190923_145633_ac_spl_template_id_ot_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('absensi', 'spl_template_id', $this->integer(11)->after('spl_id')->defaultValue(0));
        $this->addColumn('spl_template', 'jumlah_jam_lembur', $this->float(1)->after('kategori')->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190923_145633_ac_spl_template_id_ot_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190923_145633_ac_spl_template_id_ot_absensi cannot be reverted.\n";

        return false;
    }
    */
}

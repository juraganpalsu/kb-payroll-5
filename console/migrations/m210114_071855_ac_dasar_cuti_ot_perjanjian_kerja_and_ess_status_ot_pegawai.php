<?php

use yii\db\Migration;

/**
 * Class m210114_071855_ac_dasar_cuti_ot_perjanjian_kerja_and_ess_status_ot_pegawai
 */
class m210114_071855_ac_dasar_cuti_ot_perjanjian_kerja_and_ess_status_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai', 'ess_status', $this->integer(1)->notNull()->after('has_created_account')->defaultValue(0));
        $this->addColumn('perjanjian_kerja', 'dasar_cuti', $this->integer(1)->notNull()->defaultValue(0)->after('dasar_masa_kerja'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210114_071855_ac_dasar_cuti_ot_perjanjian_kerja_and_ess_status_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210114_071855_ac_dasar_cuti_ot_perjanjian_kerja_and_ess_status_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

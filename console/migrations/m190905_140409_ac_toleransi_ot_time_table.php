<?php

use yii\db\Migration;

/**
 * Class m190905_140409_ac_toleransi_ot_time_table
 */
class m190905_140409_ac_toleransi_ot_time_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('time_table', 'toleransi_masuk', $this->integer(3)->null()->defaultValue(0)->after('masuk'));
        $this->addColumn('time_table', 'toleransi_pulang', $this->integer(3)->null()->defaultValue(0)->after('pulang'));

        $this->addColumn('struktur', 'alias', $this->char(225)->null()->defaultValue(0)->after('tipe'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190905_140409_ac_toleransi_ot_time_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190905_140409_ac_toleransi_ot_time_table cannot be reverted.\n";

        return false;
    }
    */
}

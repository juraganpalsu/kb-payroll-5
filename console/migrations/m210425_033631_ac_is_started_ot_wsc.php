<?php

use yii\db\Migration;

/**
 * Class m210425_033631_ac_is_started_ot_wsc
 */
class m210425_033631_ac_is_started_ot_wsc extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('wsc', 'is_started', $this->boolean()->notNull()->defaultValue(0)->after('keterangan'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210425_033631_ac_is_started_ot_wsc cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210425_033631_ac_is_started_ot_wsc cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m201218_141358_delete_mpp_add_mpp
 */
class m201218_141358_delete_mpp_add_mpp extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->dropTable('mpp_detail_act');
        $this->dropTable('mpp_detail');
        $this->dropTable('mpp_act');
        $this->dropTable('mpp');
        $this->dropTable('mpp_header');

        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `mpp` (
  `id` VARCHAR(36) NOT NULL,
  `golongan_id` INT NOT NULL,
  `jabatan_id` VARCHAR(32) NOT NULL,
  `departemen_id` VARCHAR(32) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_mpp_detail_golongan1_idx` (`golongan_id` ASC),
  INDEX `fk_mpp_detail_jabatan1_idx` (`jabatan_id` ASC),
  INDEX `fk_mpp_detail_departemen1_idx` (`departemen_id` ASC),
  CONSTRAINT `fk_mpp_detail_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_detail_jabatan1`
    FOREIGN KEY (`jabatan_id`)
    REFERENCES `jabatan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_detail_departemen1`
    FOREIGN KEY (`departemen_id`)
    REFERENCES `departemen` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `mpp_tahun` (
  `id` VARCHAR(36) NOT NULL,
  `tahun` INT(4) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `mpp_jumlah` (
  `id` VARCHAR(36) NOT NULL,
  `jumlah` INT(4) NOT NULL DEFAULT 0,
  `mpp_tahun_id` VARCHAR(36) NOT NULL,
  `mpp_id` VARCHAR(36) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_mpp_jumlah_mpp_tahun1_idx` (`mpp_tahun_id` ASC),
  INDEX `fk_mpp_jumlah_mpp1_idx` (`mpp_id` ASC),
  CONSTRAINT `fk_mpp_jumlah_mpp_tahun1`
    FOREIGN KEY (`mpp_tahun_id`)
    REFERENCES `mpp_tahun` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_jumlah_mpp1`
    FOREIGN KEY (`mpp_id`)
    REFERENCES `mpp` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `mpp_act` (
  `id` VARCHAR(36) NOT NULL,
  `jumlah` INT(4) NOT NULL DEFAULT 0,
  `golongan_id` INT NOT NULL,
  `jabatan_id` VARCHAR(32) NOT NULL,
  `mpp_id` VARCHAR(36) NOT NULL,
  `mpp_jumlah_id` VARCHAR(36) NOT NULL,
  `keterangan` VARCHAR(45) NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_mpp_detail_act_golongan1_idx` (`golongan_id` ASC),
  INDEX `fk_mpp_detail_act_jabatan1_idx` (`jabatan_id` ASC),
  INDEX `fk_mpp_act_mpp2_idx` (`mpp_id` ASC),
  INDEX `fk_mpp_act_mpp_jumlah1_idx` (`mpp_jumlah_id` ASC),
  CONSTRAINT `fk_mpp_detail_act_golongan1`
    FOREIGN KEY (`golongan_id`)
    REFERENCES `golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_detail_act_jabatan1`
    FOREIGN KEY (`jabatan_id`)
    REFERENCES `jabatan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_act_mpp2`
    FOREIGN KEY (`mpp_id`)
    REFERENCES `mpp` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mpp_act_mpp_jumlah1`
    FOREIGN KEY (`mpp_jumlah_id`)
    REFERENCES `mpp_jumlah` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201218_141358_delete_mpp_add_mpp cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201218_141358_delete_mpp_add_mpp cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m201022_011548_uc_nominal_ot_insentif_tidak_tetap
 */
class m201022_011548_uc_nominal_ot_insentif_tidak_tetap extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('insentif_tidak_tetap', 'jumlah', $this->float()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201022_011548_uc_nominal_ot_insentif_tidak_tetap cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201022_011548_uc_nominal_ot_insentif_tidak_tetap cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200623_151342_ac_insentif_tidak_tetap_ot_gaji_bulanan
 */
class m200623_151342_ac_insentif_tidak_tetap_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'insentif_tidak_tetap_id', $this->char(32)->defaultValue('-')->after('tipe_gaji_pokok'));
        $this->addColumn('gaji_bulanan', 'insentif_tidak_tetap', $this->integer(11)->defaultValue(0)->after('insentif_tidak_tetap_id'));
        $this->addColumn('gaji_bulanan', 'rapel_id', $this->char(32)->defaultValue('-')->after('insentif_tidak_tetap'));
        $this->addColumn('gaji_bulanan', 'rapel', $this->integer(11)->defaultValue(0)->after('rapel_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200623_151342_ac_insentif_tidak_tetap_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200623_151342_ac_insentif_tidak_tetap_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

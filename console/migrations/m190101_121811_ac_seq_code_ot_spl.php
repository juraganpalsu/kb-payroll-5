<?php

use yii\db\Migration;

/**
 * Class m190101_121811_ac_seq_code_ot_spl
 */
class m190101_121811_ac_seq_code_ot_spl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('spl', 'seq_code', $this->integer(11)->defaultValue(0)->after('id')->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190101_121811_ac_seq_code_ot_spl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190101_121811_ac_seq_code_ot_spl cannot be reverted.\n";

        return false;
    }
    */
}

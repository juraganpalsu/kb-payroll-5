<?php

use yii\db\Migration;

/**
 * Class m191206_144822_ac_foto_ktp_bpjs_absensi
 */
class m191206_144822_ac_foto_ktp_bpjs_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('kecelakaan_kerja', 'foto_ktp', $this->integer(1)->defaultValue(0)->notNull()->after('tahap_dua'));
        $this->addColumn('kecelakaan_kerja', 'kartu_bpjs', $this->integer(1)->defaultValue(0)->notNull()->after('foto_ktp'));
        $this->addColumn('kecelakaan_kerja', 'absensi', $this->integer(1)->defaultValue(0)->notNull()->after('kartu_bpjs'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191206_144822_ac_foto_ktp_bpjs_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191206_144822_ac_foto_ktp_bpjs_absensi cannot be reverted.\n";

        return false;
    }
    */
}

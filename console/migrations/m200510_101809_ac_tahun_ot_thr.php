<?php

use yii\db\Migration;

/**
 * Class m200510_101809_ac_tahun_ot_thr
 */
class m200510_101809_ac_tahun_ot_thr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('thr', 'tahun', $this->integer(4)->notNull()->defaultValue(0)->after('id'));
        $this->addColumn('thr_setting', 'tahun', $this->integer(4)->notNull()->defaultValue(0)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200510_101809_ac_tahun_ot_thr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200510_101809_ac_tahun_ot_thr cannot be reverted.\n";

        return false;
    }
    */
}

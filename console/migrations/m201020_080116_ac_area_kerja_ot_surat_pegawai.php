<?php

use yii\db\Migration;

/**
 * Class m201020_080116_ac_area_kerja_ot_surat_pegawai
 */
class m201020_080116_ac_area_kerja_ot_surat_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `surat_pegawai` 
ADD COLUMN `area_kerja_id` VARCHAR(32) NOT NULL AFTER `pegawai_struktur_id`,
ADD COLUMN `ref_area_kerja_id` INT NULL DEFAULT 0 AFTER `struktur_id`;

SQL;
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201020_080116_ac_area_kerja_ot_surat_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201020_080116_ac_area_kerja_ot_surat_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

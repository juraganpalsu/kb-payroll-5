<?php

use yii\db\Migration;

/**
 * Class m200413_074506_ct_payroll_periode_and_other
 */
class m200413_074506_ct_payroll_periode_and_other extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL

CREATE TABLE IF NOT EXISTS `payroll_periode` (
  `id` VARCHAR(32) NOT NULL,
  `tipe` INT(1) NOT NULL DEFAULT 0 COMMENT '1.Bulanan\n2.Harian',
  `tanggal_awal` DATE NOT NULL,
  `tanggal_akhir` DATE NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pinjaman` (
  `id` VARCHAR(32) NOT NULL,
  `kategori` INT(2) NOT NULL DEFAULT 0 COMMENT '1.Kasbon\n2.Pijaman\n3.Potongan\n4.Lain2',
  `nama` VARCHAR(225) NOT NULL,
  `jumlah` INT(11) NOT NULL DEFAULT 0,
  `tenor` INT(2) NOT NULL DEFAULT 0,
  `angsuran` INT(11) NOT NULL DEFAULT 0,
  `payroll_periode_id` VARCHAR(32) NOT NULL COMMENT 'Mulai pemotongan',
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pinjaman_payroll_periode1_idx` (`payroll_periode_id` ASC),
  INDEX `fk_pinjaman_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_pinjaman_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_pinjaman_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_pinjaman_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_pinjaman_payroll_periode1`
    FOREIGN KEY (`payroll_periode_id`)
    REFERENCES `payroll_periode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pinjaman_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pinjaman_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pinjaman_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pinjaman_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pinjaman_angsuran` (
  `id` VARCHAR(32) NOT NULL,
  `angsuran_ke` INT(2) NOT NULL DEFAULT 0,
  `jumlah` INT(11) NOT NULL DEFAULT 0,
  `pinjaman_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_pinjaman_angsuran_pinjaman1_idx` (`pinjaman_id` ASC),
  CONSTRAINT `fk_pinjaman_angsuran_pinjaman1`
    FOREIGN KEY (`pinjaman_id`)
    REFERENCES `pinjaman` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `insentif_tidak_tetap` (
  `id` VARCHAR(32) NOT NULL,
  `jumlah` INT(11) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `payroll_periode_id` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_insentif_tidak_tetap_payroll_periode1_idx` (`payroll_periode_id` ASC),
  INDEX `fk_insentif_tidak_tetap_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_insentif_tidak_tetap_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_insentif_tidak_tetap_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_insentif_tidak_tetap_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_insentif_tidak_tetap_payroll_periode1`
    FOREIGN KEY (`payroll_periode_id`)
    REFERENCES `payroll_periode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_insentif_tidak_tetap_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_insentif_tidak_tetap_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_insentif_tidak_tetap_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_insentif_tidak_tetap_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `rapel` (
  `id` VARCHAR(32) NOT NULL,
  `jumlah` INT(11) NOT NULL,
  `keterangan` TEXT NULL,
  `periode_kurang` VARCHAR(32) NOT NULL,
  `periode_bayar` VARCHAR(32) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_rapel_payroll_periode1_idx` (`periode_kurang` ASC),
  INDEX `fk_rapel_payroll_periode2_idx` (`periode_bayar` ASC),
  INDEX `fk_rapel_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_rapel_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_rapel_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_rapel_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_rapel_payroll_periode1`
    FOREIGN KEY (`periode_kurang`)
    REFERENCES `payroll_periode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rapel_payroll_periode2`
    FOREIGN KEY (`periode_bayar`)
    REFERENCES `payroll_periode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rapel_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rapel_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rapel_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rapel_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200413_074506_ct_payroll_periode_and_other cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200413_074506_ct_payroll_periode_and_other cannot be reverted.\n";

        return false;
    }
    */
}

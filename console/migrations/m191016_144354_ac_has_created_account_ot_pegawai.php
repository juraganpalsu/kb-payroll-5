<?php

use yii\db\Migration;

/**
 * Class m191016_144354_ac_has_created_account_ot_pegawai
 */
class m191016_144354_ac_has_created_account_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pegawai', 'has_created_account', $this->integer(1)->notNull()->defaultValue(0)->after('has_uploaded_to_att'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191016_144354_ac_has_created_account_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191016_144354_ac_has_created_account_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210905_022519_ac_last_status_ot_payroll_bpjs
 */
class m210905_022519_ac_last_status_ot_payroll_bpjs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payroll_bpjs', 'last_status', $this->integer(2)->notNull()->defaultValue(0)->after('bisnis_unit'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210905_022519_ac_last_status_ot_payroll_bpjs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210905_022519_ac_last_status_ot_payroll_bpjs cannot be reverted.\n";

        return false;
    }
    */
}

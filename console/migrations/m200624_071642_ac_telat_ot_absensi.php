<?php

use yii\db\Migration;

/**
 * Class m200624_071642_ac_telat_ot_absensi
 */
class m200624_071642_ac_telat_ot_absensi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('absensi', 'telat', $this->integer(3)->notNull()->defaultValue(0)->after('jumlah_jam_lembur'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200624_071642_ac_telat_ot_absensi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200624_071642_ac_telat_ot_absensi cannot be reverted.\n";

        return false;
    }
    */
}

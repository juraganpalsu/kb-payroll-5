<?php

use yii\db\Migration;

/**
 * Class m210930_154538_ct_payroll_borongan
 */
class m210930_154538_ct_payroll_borongan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `jenis_borongan` (
  `id` VARCHAR(36) NOT NULL,
  `nama_produk` VARCHAR(225) NOT NULL,
  `harga` FLOAT(11,2) NOT NULL,
  `catatan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `payroll_borongan` (
  `id` VARCHAR(36) NOT NULL,
  `shift` INT(2) NOT NULL DEFAULT 0,
  `mulai` TIME NOT NULL,
  `selesai` TIME NOT NULL,
  `hasil` INT(6) NOT NULL,
  `catatan` TEXT NULL,
  `jenis_borongan_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_payroll_borongan_jenis_borongan1_idx` (`jenis_borongan_id` ASC),
  CONSTRAINT `fk_payroll_borongan_jenis_borongan1`
    FOREIGN KEY (`jenis_borongan_id`)
    REFERENCES `jenis_borongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `payroll_borongan_detail` (
  `id` VARCHAR(36) NOT NULL,
  `payroll_borongan_id` VARCHAR(36) NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT 0,
  `created_by_struktur` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_payroll_borongan_detail_payroll_borongan1_idx` (`payroll_borongan_id` ASC),
  INDEX `fk_payroll_borongan_detail_pegawai1_idx` (`pegawai_id` ASC),
  INDEX `fk_payroll_borongan_detail_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC),
  INDEX `fk_payroll_borongan_detail_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC),
  INDEX `fk_payroll_borongan_detail_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC),
  CONSTRAINT `fk_payroll_borongan_detail_payroll_borongan1`
    FOREIGN KEY (`payroll_borongan_id`)
    REFERENCES `payroll_borongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_borongan_detail_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_borongan_detail_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_borongan_detail_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payroll_borongan_detail_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210930_154538_ct_payroll_borongan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210930_154538_ct_payroll_borongan cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200630_064701_ac_atl_cuti_ot_gaji_bulanan
 */
class m200630_064701_ac_atl_cuti_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'atl_id', $this->char(32)->after('wfh'));
        $this->addColumn('gaji_bulanan', 'atl', $this->integer(2)->after('atl_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200630_064701_ac_atl_cuti_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200630_064701_ac_atl_cuti_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

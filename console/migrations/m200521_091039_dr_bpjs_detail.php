<?php

use yii\db\Migration;

/**
 * Class m200521_091039_dr_bpjs_detail
 */
class m200521_091039_dr_bpjs_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = <<<SQL
ALTER TABLE `payroll_bpjs_detail` 
DROP FOREIGN KEY `fk_bpjs_detail_pegawai_bpjs2`,
DROP FOREIGN KEY `fk_bpjs_detail_pegawai_bpjs1`;
ALTER TABLE `payroll_bpjs_detail` 
DROP INDEX `fk_bpjs_detail_pegawai_bpjs2_idx` ,
DROP INDEX `fk_bpjs_detail_pegawai_bpjs1_idx` ;
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200521_091039_dr_bpjs_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200521_091039_dr_bpjs_detail cannot be reverted.\n";

        return false;
    }
    */
}

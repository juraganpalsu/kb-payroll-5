<?php

use yii\db\Migration;

/**
 * Class m210324_152017_ct_wsl
 */
class m210324_152017_ct_wsl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `wsc` (
  `id` VARCHAR(36) NOT NULL,
  `tanggal` DATE NOT NULL,
  `pegawai_id` VARCHAR(32) NOT NULL,
  `pegawai_golongan_id` VARCHAR(32) NOT NULL,
  `pegawai_struktur_id` VARCHAR(32) NOT NULL,
  `perjanjian_kerja_id` VARCHAR(32) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_rencana_kerja_pegawai1_idx` (`pegawai_id` ASC) ,
  INDEX `fk_rencana_kerja_pegawai_golongan1_idx` (`pegawai_golongan_id` ASC) ,
  INDEX `fk_rencana_kerja_pegawai_struktur1_idx` (`pegawai_struktur_id` ASC) ,
  INDEX `fk_rencana_kerja_perjanjian_kerja1_idx` (`perjanjian_kerja_id` ASC) ,
  CONSTRAINT `fk_rencana_kerja_pegawai1`
    FOREIGN KEY (`pegawai_id`)
    REFERENCES `pegawai` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rencana_kerja_pegawai_golongan1`
    FOREIGN KEY (`pegawai_golongan_id`)
    REFERENCES `pegawai_golongan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rencana_kerja_pegawai_struktur1`
    FOREIGN KEY (`pegawai_struktur_id`)
    REFERENCES `pegawai_struktur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rencana_kerja_perjanjian_kerja1`
    FOREIGN KEY (`perjanjian_kerja_id`)
    REFERENCES `perjanjian_kerja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `wsc_detail` (
  `id` VARCHAR(36) NOT NULL,
  `nama_pekerjaan` VARCHAR(225) NOT NULL,
  `jam_mulai` TIME NOT NULL,
  `jam_selesai` TIME NOT NULL,
  `prioritas` INT(2) NOT NULL,
  `deskripsi` TEXT NULL,
  `status_finish` INT(2) NOT NULL DEFAULT 0,
  `keterangan` TEXT NULL,
  `kategori` INT(2) NOT NULL DEFAULT 0 COMMENT '1.DIRENCANAKAN\n2.TAMBAHAN\n3.PERUBAHAN',
  `wsc_id` VARCHAR(36) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_pk` VARCHAR(32) NULL DEFAULT '0',
  `created_by_struktur` INT(11) NULL DEFAULT '0',
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_wsc_detail_wsc1_idx` (`wsc_id` ASC),
  CONSTRAINT `fk_wsc_detail_wsc1`
    FOREIGN KEY (`wsc_id`)
    REFERENCES `wsc` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `wsc_location` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `latitude` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  `time` DATETIME NOT NULL,
  `wsc_id` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_wsc_location_wsc1_idx` (`wsc_id` ASC) ,
  CONSTRAINT `fk_wsc_location_wsc1`
    FOREIGN KEY (`wsc_id`)
    REFERENCES `wsc` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `wsc_picture_proof` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(225) NOT NULL,
  `time` DATETIME NOT NULL,
  `wsc_id` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_wsc_picture_proof_wsc1_idx` (`wsc_id` ASC) ,
  CONSTRAINT `fk_wsc_picture_proof_wsc1`
    FOREIGN KEY (`wsc_id`)
    REFERENCES `wsc` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SQL;

        $this->execute($sql);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210324_152017_ct_wsl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210324_152017_ct_wsl cannot be reverted.\n";

        return false;
    }
    */
}

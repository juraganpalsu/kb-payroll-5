<?php

use yii\db\Migration;

/**
 * Class m190827_132256_ct_fungsi_jabatan
 */
class m190827_132256_ct_fungsi_jabatan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `fungsi_jabatan` (
  `id` VARCHAR(32) NOT NULL,
  `nama` VARCHAR(225) NOT NULL,
  `kode` VARCHAR(45) NOT NULL,
  `keterangan` TEXT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT '0',
  `created_by_org_chart` INT(11) NULL DEFAULT 0,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT '0',
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `deleted_by` INT(11) NULL DEFAULT '0',
  `lock` BIGINT(20) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SQL;

        $this->execute($sql);

        $this->addColumn('struktur', 'fungsi_jabatan_id', $this->char(32)->notNull()->defaultValue(0)->after('jabatan_id'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190827_132256_ct_fungsi_jabatan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190827_132256_ct_fungsi_jabatan cannot be reverted.\n";

        return false;
    }
    */
}

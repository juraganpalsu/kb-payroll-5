<?php

use yii\db\Migration;

/**
 * Class m200204_143757_ac_auto_approve_ot_struktur
 */
class m200204_143757_ac_auto_approve_ot_struktur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('struktur', 'auto_approve', $this->integer(1)->notNull()->defaultValue(0)->after('section_id'));
        $this->addColumn('struktur', 'durasi_auto_approve', $this->integer(2)->notNull()->defaultValue(0)->after('auto_approve'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200204_143757_ac_auto_approve_ot_struktur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200204_143757_ac_auto_approve_ot_struktur cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191016_151824_uc_pegawai
 */
class m191016_151824_uc_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('pegawai', ['has_created_account' => 1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191016_151824_uc_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191016_151824_uc_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190723_131152_ac_keterangan_ot_akun_beatroute
 */
class m190723_131152_ac_keterangan_ot_akun_beatroute extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('akun_beatroute', 'keterangan', $this->text()->after('beatroute_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190723_131152_ac_keterangan_ot_akun_beatroute cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190723_131152_ac_keterangan_ot_akun_beatroute cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200526_153139_ac_hari_kerja_aktif_ot_sistem_kerja
 */
class m200526_153139_ac_hari_kerja_aktif_ot_sistem_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sistem_kerja', 'hari_kerja_aktif', $this->integer(2)->notNull()->defaultValue(0)->after('jenis_libur'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200526_153139_ac_hari_kerja_aktif_ot_sistem_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200526_153139_ac_hari_kerja_aktif_ot_sistem_kerja cannot be reverted.\n";

        return false;
    }
    */
}

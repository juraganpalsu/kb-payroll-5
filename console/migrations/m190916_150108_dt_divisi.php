<?php

use yii\db\Migration;

/**
 * Class m190916_150108_dt_divisi
 */
class m190916_150108_dt_divisi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('grup_kerja');
        $this->dropTable('divisi');
        $this->dropTable('unit');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190916_150108_dt_divisi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190916_150108_dt_divisi cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m201112_034148_ac_potongan_wfh_ot_gaji_bulanan
 */
class m201112_034148_ac_potongan_wfh_ot_gaji_bulanan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gaji_bulanan', 'angsuran_potongan_wfh_ke', $this->integer(2)->after('angsuran_ke'));
        $this->addColumn('gaji_bulanan', 'pinjaman_potongan_wfh_nominal', $this->float(2)->defaultValue(0)->after('angsuran_ke'));
        $this->addColumn('gaji_bulanan', 'pinjaman_potongan_wfh_id', $this->char(32)->after('angsuran_ke'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201112_034148_ac_potongan_wfh_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201112_034148_ac_potongan_wfh_ot_gaji_bulanan cannot be reverted.\n";

        return false;
    }
    */
}

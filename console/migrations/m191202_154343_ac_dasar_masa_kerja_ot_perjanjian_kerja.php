<?php

use yii\db\Migration;

/**
 * Class m191202_154343_ac_dasar_masa_kerja_ot_perjanjian_kerja
 */
class m191202_154343_ac_dasar_masa_kerja_ot_perjanjian_kerja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('perjanjian_kerja', 'dasar_masa_kerja', $this->integer(1)->notNull()->defaultValue(0)->after('status'));
        $this->addColumn('perjanjian_kerja', 'tanggal_resign', $this->date()->null()->after('dasar_masa_kerja'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191202_154343_ac_dasar_masa_kerja_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191202_154343_ac_dasar_masa_kerja_ot_perjanjian_kerja cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191126_160348_ac_is_resign_ot_pegawai
 */
class m191126_160348_ac_is_resign_ot_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('pegawai', 'is_resign', $this->integer(1)->notNull()->defaultValue(0)->after('catatan'));
        $this->addColumn('pegawai_keluarga', 'tanggal_lahir', $this->date()->null()->after('nik'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191126_160348_ac_is_resign_ot_pegawai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191126_160348_ac_is_resign_ot_pegawai cannot be reverted.\n";

        return false;
    }
    */
}

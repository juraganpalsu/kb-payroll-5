<?php

use yii\db\Migration;

/**
 * Class m200728_041008_uc_persen_jabatan_ot_ptkp
 */
class m200728_041008_uc_persen_jabatan_ot_ptkp extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('ptkp_setting', 'persen_biaya_jabatan', $this->integer(2)->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200728_041008_uc_persen_jabatan_ot_ptkp cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200728_041008_uc_persen_jabatan_ot_ptkp cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191002_161512_ac_tahun_masuk_ot_pegawai_pendidikan
 */
class m191002_161512_ac_tahun_masuk_ot_pegawai_pendidikan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('pegawai_pendidikan', 'tahun_masuk', $this->integer(4)->notNull()->defaultValue(0)->after('jurusan'));
        $this->addColumn('pegawai_pendidikan', 'tahun_lulus', $this->integer(4)->notNull()->defaultValue(0)->after('tahun_masuk'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191002_161512_ac_tahun_masuk_ot_pegawai_pendidikan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_161512_ac_tahun_masuk_ot_pegawai_pendidikan cannot be reverted.\n";

        return false;
    }
    */
}

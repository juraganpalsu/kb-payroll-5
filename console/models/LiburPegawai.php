<?php

namespace console\models;

use common\models\Pegawai;
use common\models\SistemKerja;
use DateInterval;
use DateTime;
use Exception;
use Yii;
use yii\db\ActiveRecord;

/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 19/11/2018
 * Time: 17:06
 */
class LiburPegawai extends \common\models\LiburPegawai
{

    /**
     * @throws Exception
     */
    public function generateLibur()
    {
        $pegawais = Pegawai::find()->all();
        /** @var Pegawai $pegawai */
        foreach ($pegawais as $pegawai) {

//            if ($pegawai->nama_lengkap != 'SAHRI BANI') {
//                continue;
//            }

            $pegawaiSistemKerja = $pegawai->pegawaiSistemKerja;
            //Proses ini hanya berlaku untuk pegawai yg sedang aktif bekerja
            if (is_null($pegawaiSistemKerja)) {
                continue;
            }

            if (!$pegawaiSistemKerja->sistem_kerja_id) {
                Yii::info('Nama pegawai ' . $pegawai->nama . ' continue di sistem kerja id', 'generate-libur');
                continue;
            }

            //Jika pola libur nya menggunakan hari, ini juga akan dilewati
            if ($pegawaiSistemKerja->sistemKerja->jenis_libur == SistemKerja::HARI) {
                continue;
            }

            //Jika tidak mempunyai pola, ini jg akan dilewati
            $polaJadwalLiburs = $pegawaiSistemKerja->sistemKerja->polaJadwalLiburs;
            if (empty($polaJadwalLiburs)) {
                continue;
            }

            Yii::info('Nama pegawai ' . $pegawai->nama, 'generate-libur');

            $initialStart = $pegawaiSistemKerja->mulai_libur ?? $pegawaiSistemKerja->mulai_berlaku;

            Yii::info('Mulai Berlaku : ' . $pegawaiSistemKerja->mulai_berlaku, 'generate-libur');
            Yii::info('Mulai Libur : ' . $pegawaiSistemKerja->mulai_libur, 'generate-libur');
            Yii::info('Generate Menggunakan : ' . $initialStart, 'generate-libur');


            $terakhirLibur = $this->getTerakhirLibur($pegawai);
            $start = $initialStart;
            if (!is_null($terakhirLibur)) {
                $start = new DateTime($terakhirLibur->tanggal);
                $today = new DateTime();
                $interval = $today->diff($start);
                //jika terakhir libur masih lebih dari 7 hari kedepan, maka libur tidak digenerate
                if ($interval->format('%r%a') > 7) {
                    Yii::info('Libur tidak digerate, karena libur terakhir pada ' . $terakhirLibur->tanggal, 'generate-libur');
                    continue;
                }
                $start->add(new DateInterval('P1D'));
                $start = $start->format('Y-m-d');
                Yii::info('Terakhir libur pada ' . $terakhirLibur->tanggal, 'generate-libur');
                Yii::info('Sistem kerja ' . $terakhirLibur->sistemKerja->nama, 'generate-libur');
                //antisipasi kejadian perubahan pola libur dari sistem hari ke sistem pola
                //karena data libur terakhir,terlalu lama untuk diambil
                if ($interval->d > 7) {
                    $start = new DateTime($initialStart);
                    $start = $start->format('Y-m-d');
                    Yii::info('Tetap menggunakan libur dari setting sistem kerja ' . $start, 'generate-libur');
                }
            }
            Yii::info('Mulai berlaku ' . $start, 'generate-libur');
            foreach ($polaJadwalLiburs as $polaJadwalLibur) {
                $masuk = $polaJadwalLibur->masuk - 1;
                $masukSampai = new DateTime($start);
                $masukSampai->add(new DateInterval('P' . $masuk . 'D'));
                $masukSampai = $masukSampai->format('Y-m-d');
                Yii::info('Masuk sampai ' . $masukSampai, 'generate-libur');
                $libur = '';
                for ($a = 1; $a <= $polaJadwalLibur->libur; $a++) {
                    $libur = new DateTime($masukSampai);
                    $libur->add(new DateInterval('P' . $a . 'D'));
                    $libur = $libur->format('Y-m-d');
                    //Procedural style
                    //$libur = date('Y-m-d', strtotime($masukSampai . "+$a days"));
                    Yii::info('Simpan libur pada : ' . $libur, 'generate-libur');
                    $this->simpanLibur($pegawai, $libur);
                }
                if (!empty($libur)) {
                    $start = new DateTime($libur);
                    $start->add(new DateInterval('P1D'));
                    $start = $start->format('Y-m-d');
                }
                //Procedural style
                //$start = date('Y-m-d', strtotime($libur . "+1 day"));
            }
        }
    }

    /**
     * Menyimpan data libur
     *
     * @param Pegawai $pegawai
     * @param string $tanggal
     */
    public function simpanLibur(Pegawai $pegawai, string $tanggal)
    {
        $sistemKerjaSekarang = $pegawai->getPegawaiSistemKerjaUntukTanggal($tanggal);
        if ($sistemKerjaSekarang) {
            $model = new self();
            $model->setScenario('create-libur');
            $model->pegawai_id = $pegawai->id;
            $model->sistem_kerja_id = $sistemKerjaSekarang->sistem_kerja_id;
            $model->tanggal = $tanggal;
            $model->save();
        }
    }

    /**
     * Mengambil terakhir libur pada seorang pegawai
     *
     * @param Pegawai $pegawai
     * @return array|null|ActiveRecord|self
     */
    public function getTerakhirLibur(Pegawai $pegawai)
    {
        return self::find()->andWhere(['pegawai_id' => $pegawai->id])->orderBy('tanggal DESC')->limit(1)->one();

    }
}
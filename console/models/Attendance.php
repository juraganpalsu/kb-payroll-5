<?php
/**
 * Created by PhpStorm.
 * User: JiwaNdaru
 * Date: 01/02/2019
 * Time: 18:51
 */

namespace console\models;


use common\components\ApiRequest;
use common\models\Absensi;
use common\models\AttendanceBeatroute;
use DateTime;
use Exception;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Query;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class Attendance
 * @package console\models
 *
 *
 */
class Attendance extends \common\models\Attendance
{

    const UNDOWNLOADED = 0;
    const DOWNLOADED = 1;
    const EXIST = 2;
    const NOIDFP = 3;

    const UNUPLOADED = 0;
    const UPLOADED = 1;

    const SFA_DEVICE_ID = 'SFA';


    /**
     * @param int $limit
     * @param int $status
     * @param string $start
     * @param string $end
     * @param array $idfp
     * @return array|mixed
     */
    public function downloadFromAdms(int $limit = 50, int $status = self::UNDOWNLOADED, string $start = '', string $end = '', array $idfp = [])
    {
        $data = [];
        $adms = Yii::$app->params['adms'];
//        if (Networking::ping($adms['host'])) {
        Yii::info('Succesfull pinging..', 'download-attendance');
        $apiRequest = new ApiRequest($adms['url']);
        $apiRequest->useCertificates = true;
        $apiRequest->ca_service = [
            'sslVerifyPeer' => false,
            'sslCafile' => '@common/certificates/serviceadms_hc_kbu_com.crt'
        ];
        $apiRequest->access_token = $adms['token'];
        $apiRequest->method = ApiRequest::GET_METHOD;
        try {
            Yii::info('Start downloading..', 'download-attendance');
            /** @var Response $responseApi */
            $responseApi = $apiRequest->createRequest()
                ->setData(['limit' => $limit, 'status' => $status, 'start' => $start, 'end' => $end, 'idfp' => $idfp, 'source' => 2])
                ->setUrl($adms['url'] . '/v1/attendance/get-data')
                ->send();

            if ($responseApi->isOk) {
                $data = $responseApi->data;
            } else {
                Yii::info('Gagal curl download from adms.', 'exception');
                Yii::info($responseApi->data, 'exception');
            }
        } catch (InvalidConfigException $e) {
            Yii::info($e->getMessage(), 'download-attendance');
        }
        Yii::info('Downloading finish..', 'download-attendance');
        return $data;
    }

    /**
     *
     * Menyimpan data attendance
     * --Upload id yg berhasil disimpan keserver adms, untuk update(flag downloaded)
     * ---Proses data attendance yg berhasil disimpan
     *
     * @param array $attendances
     *
     */
    public function saveAttendances(array $attendances)
    {
        $savedAttendanceId = [];
        Yii::info('Saving data attendance ....', 'download-attendance');
        foreach ($attendances as $attendance) {
            if ($this->validateDate($attendance['checktime'])) {
                /** @var PerjanjianKerja $perjanjianKerja */
                $perjanjianKerja = PerjanjianKerja::find()->andWhere(['id_pegawai' => $attendance['userid']])->orderBy('created_at DESC')->one();
                if ($perjanjianKerja) {
                    $cekAttendance = Attendance::find()->andWhere('pegawai_id =:pegawai_id AND check_time =:ct', [
                        ':pegawai_id' => $perjanjianKerja->id,
                        ':ct' => $attendance['checktime']
                    ])->one();
                    if ($cekAttendance) {
                        $savedAttendanceId['exist_ids'][] = $attendance['id'];
                        $savedAttendanceId['ids'][] = $attendance['id'];
                        //setelah realtime, dua baris berikut dihilangkan karena hanya untuk kebutuhan pertama kali sistem auto proses jalan
                        $savedAttendanceId['idfps'][] = $attendance['userid'];
                        $savedAttendanceId['dates'][] = date('Y-m-d', strtotime($attendance['checktime']));
                    } else {
                        $newAtt = new Attendance();
                        $newAtt->scenario = 'save-attendance';
                        $newAtt->pegawai_id = $perjanjianKerja->pegawai_id;
                        $newAtt->idfp = $perjanjianKerja->id_pegawai;
                        $newAtt->check_time = $attendance['checktime'];
                        $newAtt->device_id = $attendance['device_id'];
                        if ($newAtt->save()) {
                            $savedAttendanceId['ids'][] = $attendance['id'];
                            $savedAttendanceId['idfps'][] = $newAtt->idfp;
                            $savedAttendanceId['dates'][] = $newAtt->check_time;
                        }
                    }
                } else {
                    $savedAttendanceId['noidfp_ids'][] = $attendance['id'];
                    $savedAttendanceId['noidfps'][] = $attendance['userid'];
                }
            }
        }

        //catat idfp(id pegawai) yg tidak memiliki data pegawai pada sistem hris
        if (isset($savedAttendanceId['noidfps'])) {
            Yii::info('The following idfp(idpegawai) is not found in the employee\'s database : ' . Json::encode(array_unique($savedAttendanceId['noidfps'])), 'not-employee');
        }

        //upload to update data on adms
        if (isset($savedAttendanceId['ids'])) {
            $this->uploadToAdms($savedAttendanceId['ids'], self::DOWNLOADED);
        }

        //upload to update data on adms
        if (isset($savedAttendanceId['exist_ids'])) {
            $this->uploadToAdms($savedAttendanceId['exist_ids'], self::EXIST);
        }

        //upload to update data on adms
        if (isset($savedAttendanceId['noidfp_ids'])) {
            $this->uploadToAdms($savedAttendanceId['noidfp_ids'], self::NOIDFP);
        }


        //process data
        if (isset($savedAttendanceId['dates'])) {
            Yii::info('Process data attendance ....', 'download-attendance');
            $this->prosesData($savedAttendanceId['idfps'], $savedAttendanceId['dates']);
        }
    }


    /**
     * @param array $idfps
     * @param array $dates
     */
    public function prosesData(array $idfps, array $dates)
    {
        $idfps = array_unique($idfps);
        $queryPegawai = PerjanjianKerja::find()->select('pegawai_id')->andWhere(['IN', 'id_pegawai', $idfps])->asArray()->all();
        if (!empty($queryPegawai)) {
            $modelAbsensi = new Absensi();
            $queryPegawai = array_map(function ($x) {
                return $x['pegawai_id'];
            }, $queryPegawai);
            $start = date('Y-m-d', strtotime(min($dates)));
            $end = date('Y-m-d', strtotime(max($dates)));
            try {
                Yii::info('Amount of data that will process ' . count($queryPegawai), 'download-attendance');
                Yii::info('Processing data from ' . $start . ' to ' . $end, 'download-attendance');
                $modelAbsensi->prosesData($start, $end, null, $queryPegawai);
                Yii::info('Processing data finish ....', 'download-attendance');
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
    }

    /**
     * @param array $ids
     * @param int $status
     */
    public function uploadToAdms(array $ids, int $status)
    {
        $adms = Yii::$app->params['adms'];
//        if (Networking::ping($adms['host'])) {
        Yii::info('Uploading saved ids..', 'download-attendance');
        $apiRequest = new ApiRequest($adms['url']);
        $apiRequest->useCertificates = true;
        $apiRequest->ca_service = [
            'sslVerifyPeer' => false,
            'sslCafile' => '@common/certificates/serviceadms_hc_kbu_com.crt'
        ];
        $apiRequest->access_token = $adms['token'];
        $apiRequest->method = ApiRequest::POST_METHOD;
        try {
//            Yii::info('Ids : ' . Json::encode($ids), 'download-attendance');
            /** @var Response $responseApi */
            $responseApi = $apiRequest->createRequest()
                ->setData(['id' => $ids, 'downloaded_hristes' => $status])
                ->setUrl($adms['url'] . '/v1/attendance/update-data?source=2')
                ->send();

            if ($responseApi->isOk) {
                Yii::info('Response is OK..', 'download-attendance');
            } else {
                Yii::info('Gagal curl upload to adms.', 'exception');
                Yii::info($responseApi->data, 'exception');
            }
        } catch (InvalidConfigException $e) {
            Yii::info($e->getMessage(), 'exception');
        }
//        }
        Yii::info('Uploading finish..', 'download-attendance');
    }


    /**
     * @param int $limit
     */
    public function uploadToAttendance(int $limit = 50)
    {
        $query = Attendance::find()->select(['id', 'pegawai_id', 'idfp', 'check_time'])
            ->andWhere(['uploaded' => self::UNUPLOADED])
            ->limit($limit)->orderBy('check_time DESC')
            ->asArray()->all();

        if (!empty($query)) {
            $attendance = Yii::$app->params['attendance'];
//            if (Networking::ping($attendance['host'])) {
            Yii::info('Uploading ' . count($query) . ' attendance..', 'upload-attendance');
            $apiRequest = new ApiRequest($attendance['url']);
            $apiRequest->useCertificates = true;
            $apiRequest->ca_service = [
                'sslVerifyPeer' => false,
                'sslCafile' => '@common/certificates/serviceattendance_hc_kbu_com.crt'
            ];
            $apiRequest->access_token = $attendance['token'];
            $apiRequest->method = ApiRequest::POST_METHOD;
            try {
                /** @var Response $responseApi */
                $responseApi = $apiRequest->createRequest()
                    ->setData(['data' => $query])
                    ->setUrl($attendance['url'] . '/attendance/create-api')
                    ->send();

                //Yii::info($responseApi, 'upload-attendance');
                if ($responseApi->isOk) {
                    Yii::info('Response is OK..', 'upload-attendance');
                    //jika statusnya true maka update data yg berhasil disimpan
                    if ($responseApi->data['status']) {
                        $this->updateAttendanceHasBeenUploaded($responseApi->data['data']);
                    }
                } else {
                    Yii::info('Gagal curl upload to attendance.', 'exception');
                    Yii::info($responseApi->data, 'exception');
                }
            } catch (InvalidConfigException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
//            }
            Yii::info('Uploading finish..', 'upload-attendance');
        }
    }

    /**
     * @param array $ids id yg berhasil disimpan ke dalam aplikasi attendance
     * @return int
     */
    public function updateAttendanceHasBeenUploaded(array $ids)
    {
        Yii::info('Updating ' . count($ids) . ' attendance data.', 'upload-attendance');
        return Attendance::updateAll(['uploaded' => self::UPLOADED], ['IN', 'id', $ids]);
    }

    /**
     * Download from beatroute
     * @return array|mixed
     */
    public function downloadFromBeatRoute(int $month, int $year)
    {
        $data = [];
        $beatroute = Yii::$app->params['beatroute'];
//        if (Networking::ping($adms['host'])) {
        Yii::info('Succesfull pinging..', 'download-attendance-beatroute');
        $client = new Client([
            'baseUrl' => $beatroute['url'],
            'transport' => 'yii\httpclient\StreamTransport'
        ]);
//        Yii::info('Request URL : ' . $beatroute['url'] . '/v1/user/attendance?key=' . $beatroute['token'], 'download-attendance-beatroute');
        try {
            /** @var Response $response */
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setFormat(Client::FORMAT_XML)
                ->setUrl('/v1/user/attendance?key=' . $beatroute['token'])
                ->setData([
                    'search' => [
                        'month' => $month,
                        'year' => $year
                    ],
                ])
                ->send();
            if ($response->isOk) {
                Yii::info('Response is OK', 'download-attendance-beatroute');;
                Yii::info('Saving data attendances...', 'download-attendance-beatroute');
//                Yii::info($response->data, 'download-attendance-beatroute');
                $data = $response->data;
                if ($data['success']) {
                    Yii::info('Success response: ' . $data['success'], 'download-attendance-beatroute');
                    Yii::info('Status response: ' . $data['status'], 'download-attendance-beatroute');
                    $item = $data['data']['item'];
                    if (!empty($item)) {
                        $this->saveFromBeatroute($item);
                    }
                } else {
                    Yii::info('Beatroute response is fail', 'download-attendance-beatroute');;
                }
            }
            Yii::info('Downloading process has finished..', 'download-attendance-beatroute');
        } catch (InvalidConfigException $e) {
            Yii::info($e->getMessage(), 'download-attendance-beatroute');
            Yii::info('Downloading finish with error message.', 'download-attendance-beatroute');
            Yii::info($e->getMessage(), 'exception');
        }
        return $data;
    }

    /**
     * Menyimpan data dari beatroute ->>>masih butuh pengembangan
     *
     * @param array $datas
     */
    public function saveFromBeatroute(array $datas)
    {
        $counter = 0;
        foreach ($datas as $data) {
            if (empty($data['duration'])) {
                continue;
            }
            if (!self::isValidDate($data['duration'])) {
                continue;
            }
            $count = (new Query())
                ->from('attendance_beatroute')
                ->where(['user_br_id' => $data['user_br_id'], 'date' => $data['date'], 'duration' => $data['duration']])
                ->count();
            if ($count > 0) {
                continue;
            }

            $model = new AttendanceBeatroute();
            $model->emp_role_id = $data['emp_role_id'];
            $model->user_br_id = $data['user_br_id'];
            $model->date = $data['date'];
            $model->duration = $data['duration'];
            $model->status = $data['status'];
            if ($model->save()) {
                $counter++;
            }
        }
        Yii::info('Berhasil menyimpan ' . $counter . ' data', 'download - attendance - beatroute');
    }


    /**
     * @param string $time
     * @param string $format
     * @return bool
     */
    public static function isValidDate(string $time, string $format = 'H:i:s'): bool
    {
        $dateObj = DateTime::createFromFormat($format, $time);
        return $dateObj && $dateObj->format($format) == $time;
    }

    /**
     * @return array|mixed
     * @throws \yii\httpclient\Exception
     */
    public function getDataSfa(string $date)
    {
        $data = [];
        $sfa = Yii::$app->params['sfa'];
//        if (Networking::ping($adms['host'])) {
        Yii::info('Succesfull pinging..', 'download-attendance-sfa');
        $client = new Client([
            'baseUrl' => $sfa['url'],
            'transport' => '\yii\httpclient\CurlTransport'
        ]);
        try {
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setFormat(Client::FORMAT_JSON)
                ->setUrl('/v1/interface/get/absence')
                ->setData([
                    'INTF_USERID' => 'HRIS',
                    'INTF_PASSW' => '13A56',
                    'INTF_DOCDATE' => $date
                ])
                ->send();
            if ($response->isOk) {
                Yii::info('Response is OK', 'download-attendance-sfa');
                Yii::info('Saving data attendances...', 'download-attendance-sfa');
//                Yii::info($response->data, 'download-attendance-sfa');
                $data = $response->data;
                if ($data['STATUS'] == '200') {
                    $attendances = $data['DATA'];
                    Yii::info('Success response: ' . count($attendances), 'download-attendance-sfa');
                    if (!empty($attendances)) {
                        $this->saveDataSfa($date, $attendances);
                    }
                } else {
                    Yii::info('SFA response is fail', 'download-attendance-sfa');
                }
            }
            Yii::info('Downloading process has finished..', 'download-attendance-sfa');
        } catch (InvalidConfigException $e) {
            Yii::info($e->getMessage(), 'download-attendance-sfa');
            Yii::info('Downloading finish with error message.', 'download-attendance-sfa');
            Yii::info($e->getMessage(), 'exception');
        }
        return $data;
    }

    /**
     * @param string $date
     * @param array $attendances
     * @throws Exception
     */
    public function saveDataSfa(string $date, array $attendances)
    {
        $pegawaiIds = [];
        foreach ($attendances as $attendance) {
            if ($date == $attendance['DocDate']) {
                if ($in = $attendance['INTIME']) {
                    if ($modelAttendance = $this->saveAttendance((int)$attendance['NIK'], $date . ' ' . $in, self::SFA_DEVICE_ID)) {
                        $pegawaiIds[] = $modelAttendance->pegawai_id;
                    }
                }
                if ($out = $attendance['OUTTIME']) {
                    if ($modelAttendance = $this->saveAttendance((int)$attendance['NIK'], $date . ' ' . $out, self::SFA_DEVICE_ID)) {
                        $pegawaiIds[] = $modelAttendance->pegawai_id;
                    }
                }
            }
        }
        $modelAbsensi = new \console\models\Absensi();
        if (!empty($pegawaiIds)) {
            $modelAbsensi->prosesData($date, $date, null, $pegawaiIds);
        }
    }

}
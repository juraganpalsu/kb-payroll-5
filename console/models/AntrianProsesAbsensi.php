<?php


namespace console\models;


use Exception;
use frontend\models\Absensi;
use frontend\models\Kehadiran;
use frontend\models\Proyek;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Throwable;
use yii\console\ExitCode;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;

/**
 * Created by PhpStorm.
 * File Name: AntrianProsesAbsensi.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 07/10/20
 * Time: 17.49
 */
class AntrianProsesAbsensi extends \frontend\modules\queue\models\AntrianProsesAbsensi
{

    /**
     * @return int
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function prosesDataPerGolongan()
    {
        $getDataTemp = self::find()->andWhere(['status' => self::DIBUAT])->orderBy('created_at ASC')->limit(1)->one();
        if (is_null($getDataTemp)) {
            return ExitCode::UNSPECIFIED_ERROR;
        }

        //Mengambil jumlah data yg sedang diproses
        $dataSedangDiproses = self::find()->where(['status' => self::DIPROSES])->count();

        //Menandai sedang diproses
        $modelAntrianProsesAbsensi = self::findOne($getDataTemp['id']);
        //dapat melakukan hingga 8 proses secara bersamaan
        if ($modelAntrianProsesAbsensi && $dataSedangDiproses <= 2) {
            $modelAntrianProsesAbsensi->status = self::DIPROSES;
            if ($modelAntrianProsesAbsensi->save()) {
                $modelAntrianProsesAbsensi->prosesData();
            }
            //Setelah selesai proses langsung tandai
            $getModelTempProcessed = self::findOne($getDataTemp['id']);
            if ($getModelTempProcessed && $getModelTempProcessed->status == self::DIPROSES) {
                $getModelTempProcessed->delete();
            }

        }
        return ExitCode::OK;
    }

    /**
     * @throws Exception
     */
    private function prosesData()
    {
        $modelAbsensi = new Absensi();
        $queryPegawaiId = \frontend\modules\pegawai\models\Pegawai::find()->select(['pegawai.id'])
            ->joinWith(['pegawaiGolongans' => function (ActiveQuery $query) {
                $query->andWhere(['golongan_id' => $this->golongan_id]);
                $query->andWhere('\'' . $this->tanggal_awal . '\' BETWEEN pegawai_golongan.mulai_berlaku AND pegawai_golongan.akhir_berlaku OR \'' . $this->tanggal_akhir . '\' BETWEEN pegawai_golongan.mulai_berlaku AND pegawai_golongan.akhir_berlaku OR pegawai_golongan.mulai_berlaku BETWEEN \'' . $this->tanggal_awal . '\' AND \'' . $this->tanggal_akhir . '\' OR pegawai_golongan.akhir_berlaku BETWEEN \'' . $this->tanggal_awal . '\' AND \'' . $this->tanggal_akhir . '\' OR ( \'' . $this->tanggal_awal . '\' <= pegawai_golongan.mulai_berlaku AND \'' . $this->tanggal_akhir . '\' >= pegawai_golongan.akhir_berlaku) OR ( \'' . $this->tanggal_awal . '\' >= pegawai_golongan.mulai_berlaku AND \'' . $this->tanggal_akhir . '\' <= pegawai_golongan.akhir_berlaku) OR (pegawai_golongan.mulai_berlaku <=\'' . $this->tanggal_akhir . '\' AND pegawai_golongan.akhir_berlaku is null)');
            }])
            ->joinWith(['perjanjianKerjas' => function (ActiveQuery $query) {
                $query->andWhere(['kontrak' => $this->bisnis_unit]);
                $query->andWhere(PerjanjianKerja::queryPerjanjianKerjaAktifBerdasarRangeTanggal($this->tanggal_awal, $this->tanggal_akhir));
            }])
            ->column();
        try {
            if (!empty($queryPegawaiId)) {
                $modelAbsensi->prosesData($this->tanggal_awal, $this->tanggal_akhir, null, $queryPegawaiId);
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

}
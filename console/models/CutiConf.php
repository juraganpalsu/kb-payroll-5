<?php


namespace console\models;


/**
 * Created by PhpStorm.
 * File Name: CutiConf.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 11/02/21
 * Time: 21.30
 */
class CutiConf extends \frontend\modules\izin\models\CutiConf
{

    /**
     * Membuat row baru pada cuti conf
     */
    public function createCutiConf(): void
    {
        $model = new self();
        $model->detachBehavior('blameable');
        $model->detachBehavior('createdby');
        $model->tahun = date('Y', strtotime('+1 year'));
        $model->akhir_berlaku = $model->tahun + 1 . '-06-30';
        $model->save();
    }

    /**
     * @param int $tahun
     * @return CutiConf|null
     */
    public static function getAkhirBelakuBerdasarkanTahun(int $tahun): CutiConf
    {
        return self::findOne(['tahun' => $tahun]);
    }

}
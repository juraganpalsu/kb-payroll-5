<?php


namespace console\models;


use frontend\models\User;
use frontend\modules\izin\models\IzinTanpaHadir;
use frontend\modules\wsc\models\Wsc;
use Yii;

/**
 * Created by PhpStorm.
 * File Name: Absensi.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 5/26/2021
 * Time: 9:10 PM
 */
class Absensi extends \common\models\Absensi
{

    const idWfc = '5b3c6ed8723011ea9776067f72d3402c';


    /**
     * void
     */
    public function convertFromWsc()
    {
        $date = date('Y-m-d', strtotime('-1 day'));
        $queryWsc = Wsc::find()->andWhere(['tanggal' => $date])->all();
        /** @var Wsc $wsc */
        foreach ($queryWsc as $wsc) {
            Yii::$app->user->setIdentity(User::findOne($wsc->created_by));
            $modelIzin = new IzinTanpaHadir();
            $modelIzin->izin_jenis_id = self::idWfc;
            $modelIzin->tanggal_mulai = $date;
            $modelIzin->tanggal_selesai = $date;
            $modelIzin->pegawai_id = $wsc->pegawai_id;
            $modelIzin->keterangan = Yii::t('frontend', 'Generate from WSC');
            $modelIzin->save();
        }
    }
}
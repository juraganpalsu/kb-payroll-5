<?php


namespace console\models;


use common\models\Absensi;
use DateTime;
use frontend\modules\dashboard\models\DsPerTanggal;
use frontend\modules\pegawai\models\PerjanjianKerja;

/**
 * Created by PhpStorm.
 * File Name: DsAbsensi.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 20/10/20
 * Time: 22.57
 */
class DsAbsensi extends \frontend\modules\dashboard\models\DsAbsensi
{

    /**
     * @param string $tanggal
     * @return array
     */
    public function getIdPegawaiAktif(string $tanggal)
    {
        $modelPegawai = new PerjanjianKerja();
        $kerjaAktifBerdasarTanggal = $modelPegawai->perjanjianKerjaAktifBerdasarTanggal($tanggal);

        $data = [];

        /** @var PerjanjianKerja $perjanjianKerja */
        foreach ($kerjaAktifBerdasarTanggal as $perjanjianKerja) {
            if ($pegawai = $perjanjianKerja->pegawai) {
                $pegawaiStruktur = $pegawai->getPegawaiStrukturUntukTanggal($tanggal);
                if ($pegawaiStruktur) {
                    $data[$pegawaiStruktur->struktur->departemen_id]['departemen'] = ['id' => $pegawaiStruktur->struktur->departemen_id, 'nama' => $pegawaiStruktur->struktur->departemen->nama];
                    $data[$pegawaiStruktur->struktur->departemen_id]['pegawai_ids'][] = $pegawai->id;
                }
            }
        }
        return $data;
    }

    /**
     * @param string $tanggal
     * @param int $statusKehadiran
     * @return array
     */
    public function getStatusAbsensi(string $tanggal, int $statusKehadiran = Absensi::ALFA)
    {
        $data = [];
        foreach ($this->getIdPegawaiAktif($tanggal) as $key => $item) {
            $queryAbsensi = Absensi::find()->andWhere(['status_kehadiran' => $statusKehadiran, 'tanggal' => $tanggal])->andWhere(['IN', 'pegawai_id', $item['pegawai_ids']]);
            $data[$key]['status_kehadiran'] = $statusKehadiran;
            $data[$key]['departemen_id'] = $item['departemen']['id'];
            $data[$key]['departemen_string'] = $item['departemen']['nama'];
            $data[$key]['pegawai_ids'] = implode('*', $queryAbsensi->select('pegawai_id')->column());
            $data[$key]['jumlah'] = $queryAbsensi->count();
        }
        return $data;
    }

    /**
     * @param array $statusKehadirans
     */
    public function simpanData(array $statusKehadirans = [Absensi::ALFA])
    {
        $tanggalObj = new DateTime();
        $tanggalObj->modify('-1 day');
        $kemarin = $tanggalObj->format('Y-m-d');

        $modelDsPertanggal = new DsPerTanggal();
        $modelDsPertanggal->tanggal = $kemarin;
        $modelDsPertanggal->jenis = DsPerTanggal::ABSENSI_STATUS_KEHADIRAN;
        $cek = DsPerTanggal::findOne(['tanggal' => $kemarin, 'jenis' => DsPerTanggal::ABSENSI_STATUS_KEHADIRAN]);
        if ($cek) {
            $modelDsPertanggal = $cek;
        }

        if ($modelDsPertanggal->save()) {
            foreach ($statusKehadirans as $statusKehadiran) {
                $getDataDs = $this->getStatusAbsensi($kemarin, $statusKehadiran);
                if ($getDataDs) {
                    foreach ($getDataDs as $dataDs) {
                        $modelDsAbsensi = new DsAbsensi();
                        $modelDsAbsensi->ds_per_tanggal_id = $modelDsPertanggal->id;
                        $modelDsAbsensi->status_kehadiran = $dataDs['status_kehadiran'];
                        $modelDsAbsensi->departemen_id = $dataDs['departemen_id'];
                        $modelDsAbsensi->departemen_string = $dataDs['departemen_string'];
                        $cekModelDsAbsensi = DsAbsensi::findOne(['ds_per_tanggal_id' => $modelDsPertanggal->id, 'status_kehadiran' => $modelDsAbsensi->status_kehadiran, 'departemen_id' => $modelDsAbsensi->departemen_id]);
                        if ($cekModelDsAbsensi) {
                            $modelDsAbsensi = $cekModelDsAbsensi;
                        }
                        $modelDsAbsensi->jumlah = $dataDs['jumlah'];
                        $modelDsAbsensi->pegawai_ids = $dataDs['pegawai_ids'];
                        $modelDsAbsensi->save();
                    }
                }
            }
        }
    }

}
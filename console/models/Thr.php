<?php


namespace console\models;


use frontend\models\User;
use Yii;
use yii\console\ExitCode;

/**
 * Created by PhpStorm.
 * File Name: Thr.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 4/5/2021
 * Time: 11:03 PM
 */
class Thr extends \frontend\modules\payroll\models\Thr
{

    /**
     * @return int
     */
    public function createDetail()
    {
        $getDataTemp = self::find()->andWhere(['finishing_status' => self::created])->orderBy('created_at ASC')->limit(1)->one();
        if (is_null($getDataTemp)) {
            return ExitCode::UNSPECIFIED_ERROR;
        }

        //Mengambil jumlah data yg sedang diproses
        $dataSedangDiproses = self::find()->where(['finishing_status' => self::processing])->count();

        //Menandai sedang diproses
        $modelThr = self::findOne($getDataTemp['id']);
        //dapat melakukan hingga 8 proses secara bersamaan
        if ($modelThr && $dataSedangDiproses <= 2) {
            Yii::$app->user->setIdentity(User::findOne($modelThr->created_by));
            $modelThr->finishing_status = self::processing;
            if ($modelThr->save()) {
                $modelThr->simpanDataThr();
            }
            //Setelah selesai proses langsung tandai
            $getModelTempProcessed = self::findOne($getDataTemp['id']);
            if ($getModelTempProcessed) {
                $getModelTempProcessed->finishing_status = self::finished;
                $getModelTempProcessed->save();
            }

        }
        return ExitCode::OK;
    }

}
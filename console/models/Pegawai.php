<?php


namespace console\models;


use common\components\ApiRequest;
use common\components\Status;
use Exception;
use frontend\models\User;
use frontend\modules\pegawai\models\search\PegawaiSearch;
use frontend\modules\pegawai\models\UserPegawai;
use frontend\modules\personalia\models\Resign;
use Yii;
use yii\base\InvalidConfigException;
use yii\swiftmailer\Mailer;

class Pegawai extends \frontend\modules\pegawai\models\Pegawai
{

    const UNUPLOADED = 0;
    const UPLOADED = 1;


    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update_has_created_account'] = ['has_created_account'];
        return $scenarios;
    }

    /**
     * @param int $limit
     */
    public function uploadToAttendance(int $limit = 50)
    {
        $query = Pegawai::find()->select(['id', 'idfp', 'nama', 'sistem_kerja', 'unit', 'divisi', 'jabatan', 'golongan_id', 'has_uploaded_to_att', 'deleted_by'])
            ->andWhere(['has_uploaded_to_att' => self::UNUPLOADED])
            ->limit($limit)->orderBy('updated_at DESC')
            ->asArray()->all();

        if (!empty($query)) {
            $attendance = Yii::$app->params['attendance'];
//            if (Networking::ping($attendance['host'])) {
            Yii::info('Uploading ' . count($query) . ' pegawai..', 'upload-pegawai');
            $apiRequest = new ApiRequest($attendance['url']);
            $apiRequest->useCertificates = true;
            $apiRequest->ca_service = [
                'sslVerifyPeer' => false,
                'sslCafile' => '@common/certificates/serviceattendance_hc_kbu_com.crt'
            ];
            $apiRequest->access_token = $attendance['token'];
            $apiRequest->method = ApiRequest::POST_METHOD;
            try {
                $responseApi = $apiRequest->createRequest()
                    ->setData(['data' => $query])
                    ->setUrl($attendance['url'] . '/pegawai/create-from-api')
                    ->send();

                if ($responseApi->isOk) {
                    Yii::info('Response is OK..', 'upload-pegawai');
                    //jika statusnya true maka update data yg berhasil disimpan
                    if ($responseApi->data['status']) {
                        $this->updatePegawaiHasBeenUploaded($responseApi->data['data']);
                    }
                } else {
                    Yii::info('Gagal curl upload pegawai to attendance.', 'exception');
                    Yii::info($responseApi->data, 'exception');
                }
            } catch (\yii\httpclient\Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            } catch (InvalidConfigException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
//            }
            Yii::info('Uploading pegawai finish..', 'upload-pegawai');
        }
    }

    /**
     * @param array $ids id yg berhasil disimpan ke dalam aplikasi attendance
     * @return int
     */
    public function updatePegawaiHasBeenUploaded(array $ids)
    {
        Yii::info('Updating ' . count($ids) . ' pegawai data.', 'upload-pegawai');
        return Pegawai::updateAll(['has_uploaded_to_att' => self::UPLOADED], ['IN', 'id', $ids]);
    }

    /**
     *
     */
    public function createUser()
    {
        $query = Pegawai::find()
            ->andWhere(['has_created_account' => self::UNUPLOADED])
            ->all();
        foreach ($query as $pegawai) {
            try {
                $this->createUserIdentity($pegawai);
            } catch (InvalidConfigException $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
    }

    /**
     * @param Pegawai $model
     * @return bool
     * @throws InvalidConfigException
     */
    public function createUserIdentity(Pegawai $model)
    {
        $userPegawai = UserPegawai::findOne(['pegawai_id' => $model->id]);
        if ($userPegawai) {
            Yii::info(Yii::t('frontend', 'Telah dibuatkan user nya') . ' ' . $userPegawai->pegawai->nama_lengkap, 'create-user');
            $model->scenario = 'update_has_created_account';
            $model->has_created_account = 1;
            if ($model->save()) {
                Yii::info(Yii::t('frontend', 'Berhasil ditandai ') . ' ' . $userPegawai->pegawai->nama_lengkap, 'create-user');
                return false;
            }
        }

        /** @var User $user */
        $user = Yii::createObject(User::class);
        $user->setScenario('register');
        Yii::info(Yii::t('frontend', 'Nama Lengkap ') . ' ' . $model->nama_lengkap, 'create-user');
        $namaLengkap = str_replace('\'', '', $model->nama_lengkap);
        $namaLengkap = str_replace('`', '', $namaLengkap);
        $namaLengkap = str_replace('.', '', $namaLengkap);
        $namaLengkap = explode(' ', $namaLengkap);
        if (is_array($namaLengkap) && count($namaLengkap) >= 2) {
            $namaLengkap = [$namaLengkap[0], $namaLengkap[1]];
        }
        if (is_array($namaLengkap)) {
            $namaLengkap = implode('.', $namaLengkap);
        }
        $namaLengkap = strtolower($namaLengkap);
        $modelUser = $this->findUser($namaLengkap);
        if ($modelUser) {
            Yii::info(Yii::t('frontend', 'Username Telah Digunakan') . ' ' . $namaLengkap, 'create-user');
            $model->scenario = 'update_has_created_account';
            $model->has_created_account = 1;
            if ($model->save()) {
                Yii::info(Yii::t('frontend', 'Berhasil Ditandai') . ' ' . $namaLengkap, 'create-user');
                return false;
            }
        }
        Yii::info(Yii::t('frontend', 'Membuat username') . ' ' . $namaLengkap, 'create-user');
        $user->setAttributes(['email' => $namaLengkap . '@hc-kbu.com', 'username' => $namaLengkap, 'password' => '*' . $namaLengkap . '#']);
        try {
            if ($user->create()) {
                $modelUser = $modelUser = $this->findUser($namaLengkap);
                $modelUserPegawai = new UserPegawai();
                $modelUserPegawai->pegawai_id = $model->id;
                $modelUserPegawai->user_id = $modelUser['id'];
                if ($modelUserPegawai->save()) {
                    $this->createDefaultRole($modelUserPegawai->user_id);
                    $model->scenario = 'update_has_created_account';
                    $model->has_created_account = 1;
                    if ($model->save()) {
                        Yii::info(Yii::t('frontend', 'Berhasil membuat username') . ' ' . $namaLengkap, 'create-user');
                    }
                }
            } else {
                Yii::info(Yii::t('frontend', 'Gagal membuat username') . ' ' . $namaLengkap, 'create-user');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return false;
    }

    /**
     * @param int $userId
     * @throws Exception
     */
    public function createDefaultRole(int $userId)
    {
        $auth = Yii::$app->authManager;
        $authorRole = $auth->getRole('ESS');
        try {
            $auth->assign($authorRole, $userId);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * @param string $username
     * @return User|null
     */
    public function findUser(string $username)
    {
        return User::findOne(['username' => $username]);
    }

    /**
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function generateDataHabisKontrak()
    {
        $modelSearchPegawai = new PegawaiSearch();
        return $modelSearchPegawai->searchHabisKontrakExcel();
    }

    /**
     * Mengirim data habis kontrak Kepada user HR
     *
     *
     */
    public function kirimNotifikasiKeHr()
    {
        /** @var Mailer $mail */
        $mail = Yii::$app->mailer_gmail;
        $compose = $mail->compose()
            ->setFrom('hc.system@kobe.co.id');

        $title = Yii::t('frontend', 'Berikut List Pegawai Yang Akan Habis Kontrak Dalam 2 Bulan Kedepan.');

        $compose->setTo(['waskito@kobe.co.id', 'rama.dana@kobe.co.id', 'personalia@kobe.co.id', 'payroll@kobe.co.id', 'assessment@kobe.co.id'])
            ->setSubject($title);

        $filename = Yii::getAlias('@frontend') . '/web/habis-kontrak/data-pegawai-akan-habis-kontrak-' . date('m-Y', strtotime('+ 2 months')) . '.xls';
        $compose->attach($filename);

        $compose->send();
    }


    /**
     * Set komponen pegawai
     */
    public function getPegawaiResignBerdasarTanggal()
    {
        $hariIni = date('Y-m-d');
        $query = Resign::find()->andWhere(['tanggal' => $hariIni])->all();
        if ($query) {
            /** @var Resign $dataResign */
            foreach ($query as $dataResign) {
                $modelPegawai = \frontend\modules\pegawai\models\Pegawai::findOne($dataResign->pegawai_id);
                if ($modelPegawai) {
                    Yii::info(Yii::t('frontend', 'Pegawai a/n {nama} telah diset statusnya menjadi resign.', ['nama' => $modelPegawai->nama_lengkap]), 'update-komponen-pegawai');
                    $modelPegawai->is_resign = Status::YA;
                    if ($modelPegawai->save()) {
                        Yii::info(Yii::t('frontend', 'Berhasil diupdate'), 'update-komponen-pegawai');
                    }
                }
            }
        }
    }
}
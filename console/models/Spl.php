<?php


namespace console\models;


use common\components\Status;
use DateTime;
use Exception;
use frontend\models\form\SplExportExcelForm;
use frontend\models\status\SplApproval;
use kartik\mpdf\Pdf;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;

/**
 * Created by PhpStorm.
 * File Name: Spl.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 12/02/20
 * Time: 22.23
 * SMTP Username:AKIAQSULZ2RMRJNGC65A
 * SMTP Password:BPhT+FCFqCk4+MfMpvwje8IaOuvhZHArzXKxpiPNKqXM
 *
 * @property string $spl_folder
 *
 */
class Spl extends \common\models\Spl
{

    public $spl_folder = '@frontend/web/datas/spl/';

    /**
     *
     */
    public function autoApproval()
    {
        try {
            $query = Spl::find()->andWhere(['last_status' => Status::SUBMITED])->all();
            if ($query) {
                /** @var Spl $spl */
                foreach ($query as $spl) {
                    Yii::info('Spl Tanggal ' . $spl->tanggal, 'auto-spl-approval');
                    $queryApproval = SplApproval::find()->andWhere(['tipe' => Status::DEFLT, 'spl_id' => $spl->id])
                        ->orderBy('level DESC')->all();
                    if ($queryApproval) {
                        /** @var SplApproval $userApproval */
                        foreach ($queryApproval as $userApproval) {
                            if ($pegawaiStruktur = $userApproval->pegawai->pegawaiStruktur) {
                                Yii::$app->user->setIdentity($userApproval->pegawai->userPegawai->user);
                                Yii::info($userApproval->pegawai->nama, 'auto-spl-approval');
                                $tanggalSplObj = new DateTime($spl->tanggal);
                                if ($spl->is_planing) {
                                    $tanggalSpl = $tanggalSplObj->modify('-' . $pegawaiStruktur->struktur->durasi_auto_approve_min . ' days');
                                } else {
                                    $durasiAutoApprove = $pegawaiStruktur->struktur->durasi_auto_approve;
                                    if (in_array($tanggalSplObj->format('D'), ['Fri', 'Sat', 'Sun'])) {
                                        $durasiAutoApprove += 2;
                                    }
                                    $tanggalSpl = $tanggalSplObj->modify('+' . $durasiAutoApprove . ' days');
                                }
                                $hariIniObj = new DateTime();
                                Yii::info('Harusnya diapprove Tanggal ' . $tanggalSpl->format('Y-m-d'), 'auto-spl-approval');
                                if ($tanggalSpl->format('Ymd') == $hariIniObj->format('Ymd')) {
                                    Yii::info(Yii::t('frontend', 'Malakukan Auto Approve untuk tgl {tanggal}', ['tanggal' => $tanggalSpl->format('d-m-Y')]), 'auto-spl-approval');
                                    $model = self::findOne($spl->id);
                                    if ($model) {
                                        $model->approval($pegawaiStruktur->struktur->auto_approve, Yii::t('frontend', 'Auto'));
                                        $model = self::findOne($spl->id);
                                        $model->createApproval(Yii::t('frontend', 'Auto'));
                                        $model->prosesUlangAbsensi();
                                        Yii::info('Berhasil melakukan auto approve untuk id ' . $model->id, 'auto-spl-approval');
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                Yii::info('Tidak ada Spl', 'auto-spl-approval');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     *
     * Generate SPL ke file sebelum dikirim ke email
     *
     * @param Spl $model
     * @return string
     */
    public function generateSplToPdf(Spl $model)
    {
        try {
            $lokasiFile = Yii::getAlias($this->spl_folder);
            $namaFile = date('d-m-Y', strtotime($model->tanggal)) . '-' . $model->createdByStruktur->nameCostume . '-' . $model->id . '.pdf';

            if (!file_exists($lokasiFile . $namaFile)) {
                Yii::info('Spl Tanggal = ' . $model->tanggal . ' file = ' . $namaFile, 'generate-spl-pdf');

                $content = Yii::$app->view->renderAjax('@frontend/modules/spl/views/spl/_print', ['model' => $model]);
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_CORE,
                    'format' => Pdf::FORMAT_A4,
                    'orientation' => Pdf::ORIENT_PORTRAIT,
                    'destination' => Pdf::DEST_FILE,
                    'filename' => $lokasiFile . $namaFile,
                    'content' => $content,
                    'cssFile' => '@frontend/web/css/laporanHarian.css',
                    'cssInline' => 'table{font-size:10px}',
                    'options' => ['title' => Yii::$app->name],
                    'marginTop' => '5',
                    'marginBottom' => '5',
                    'marginLeft' => '2',
                    'marginRight' => '2',
                    'methods' => [
                        'SetFooter' => ['{PAGENO}'],
                    ]
                ]);
                $pdf->render();
                Yii::info(Yii::t('frontend', 'Generating files is Finish '), 'generate-spl-pdf');
                return $lokasiFile . $namaFile;
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return '';
    }

    /**
     * Mengirim notifikasi SPL ke email
     */
    public function kirimNotifikasi()
    {
        $modelFormSpl = new SplExportExcelForm();
        try {
            $query = Spl::find()
                ->joinWith(['splStatuses' => function (ActiveQuery $query) {
                    $query->andWhere(['date(spl_status.created_at)' => date('Y-m-d'), 'status' => Status::SUBMITED]);
                }])
                ->andWhere(['IN', 'spl.last_status', [Status::SUBMITED]])->all();

            if ($modelFormSpl->downloadExcel($query, true)) {
                Yii::info(Yii::t('frontend', 'Persiapan mengirim pemberitahuan {tanggal}', ['tanggal' => date('d-m-Y')]), 'generate-spl-pdf');
                /** @var Mailer $mail */
                $mail = Yii::$app->mailer_gmail;
                $compose = $mail->compose()
                    ->setFrom('hris@hc-kbu.com');

                $title = Yii::t('frontend', 'Berikut daftar SPKL yg disubmit pada hari ini.');

                $compose->setTo(['teti.roheti@kobe.co.id', 'anton.wijaya@kobe.co.id'])
                    ->setSubject($title);

                $message = '<html><body style="line-height: 1.5">';
                $message .= "<div style='font-weight:bold'>Dear Bapak/Ibu<br><br></div>";
                $message .= "<div> $title <br><br></div>";
                $message .= "<div style='font-weight:bold; color:red;'>Berikut link download SPKL hari ini.<br><br></div>";
                $message .= "<div>" . Html::a(Yii::t('frontend', 'Klik Disini'), Url::to(['/spl/spl/download-rekap-harian', 'fn' => Yii::$app->security->maskToken(date('d-m-Y'))], true)) . "<br><br></div>";
                $message .= "<div>Demikian, disampaikan.<br></div>";
                $message .= "<div>Terimakasih<br><br></div>";
                $message .= "<div style='font-weight:bold'>Regards<br><br></div>";
                $message .= "<div style='font-weight:bold'>HRIS - HC KBU</div>";
                $message .= '</body></html>';
                $compose->setHtmlBody($message);

                //Ganti dengan link file
                //$file = Yii::getAlias('@console/runtime/spl/') . 'data-spl-' . date('d-m-Y') . '.xls';
                //$compose->attach($file);

                $compose->send();
                Yii::info(Yii::t('frontend', 'Mengirim pemberitahuan telah selesai {tanggal}', ['tanggal' => date('d-m-Y')]), 'generate-spl-pdf');
            } else {
                Yii::info('Tidak ada Spl', 'auto-spl-approval');
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }


    /**
     * Testing untuk kirim email
     */
    public function testSendMail()
    {
        /** @var Mailer $mail */
        $mail = Yii::$app->mailer_gmail;
        $compose = $mail->compose()
            ->setFrom('hris@hc-kbu.com');

        $title = Yii::t('frontend', 'Berikut daftar SPKL yg disubmit pada hari ini dari local.');

        $compose->setTo(['jiwanndaru@gmail.com', 'andarum21@gmai.com', 'bobi.arip@gmail.com'])
            ->setSubject($title);
        $message = '<html><body style="line-height: 1.5">';
        $message .= "<div style='font-weight:bold'>Dear Bapak/Ibu<br><br></div>";
        $message .= "<div> $title <br><br></div>";
        $message .= "<div style='font-weight:bold; color:red;'>Berikut link download SPKL hari ini.<br><br></div>";
        $message .= "<div>" . Html::a(Yii::t('frontend', 'Klik Disini'), Url::to(['/spl/spl/download-rekap-harian', 'fn' => Yii::$app->security->maskToken(date('d-m-Y'))], true)) . "<br><br></div>";
        $message .= "<div>Demikian, disampaikan.<br></div>";
        $message .= "<div>Terimakasih<br><br></div>";
        $message .= "<div style='font-weight:bold'>Regards<br><br></div>";
        $message .= "<div style='font-weight:bold'>HRIS - HC KBU</div>";
        $message .= '</body></html>';
        $compose->setHtmlBody($message);

//        $file = Yii::getAlias('@console/runtime/spl/') . 'data-spl-08-04-2021.xls';
//        $compose->attach($file);

        $compose->send();
    }

}
<?php


namespace console\models;


use common\components\Status;
use DateTime;
use Exception;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;

/**
 * Created by PhpStorm.
 * File Name: CutiSaldo.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 13/01/20
 * Time: 23.08
 */
class CutiSaldo extends \frontend\modules\izin\models\CutiSaldo
{

    /**
     * Membuat saldo awal cuti
     *
     * @param int $jumlahHariKedepan
     */
    public function setSaldoAwal(int $jumlahHariKedepan = 10)
    {
        try {
            $dateTime = new DateTime('+ ' . $jumlahHariKedepan . ' day');
            $tanggalAwal = $dateTime->format('Y-m-d');
            $akhirBerlaku = CutiConf::getAkhirBelakuBerdasarkanTahun((int)$dateTime->format('Y'))->akhir_berlaku;
            $setahunLaluObj = $dateTime->modify('-1 year');
            $setahunLalu = $setahunLaluObj->format('Y-m-d');
            //$akhirTahun = new DateTime('Dec 31');
            $kuotaCuti = 12;
            $query = PerjanjianKerja::find()->andWhere(['dasar_cuti' => Status::YA, 'mulai_berlaku' => $setahunLalu])->all();
            Yii::info(Yii::t('frontend', 'Buat SALDO AWAL untuk tanggal awal masuk : {tanggal}', ['tanggal' => $setahunLalu]), 'saldo-cuti');
            if ($query) {
                /** @var PerjanjianKerja $perjanjianKerja */
                foreach ($query as $perjanjianKerja) {
                    $modelPegawai = Pegawai::findOne($perjanjianKerja->pegawai_id);
                    if (($perjanjianKerja->isAktif($tanggalAwal) || $perjanjianKerja->getDasarCuti()) && $modelPegawai->pegawaiGolongan && $modelPegawai->pegawaiStruktur) {
                        $modelCutiSaldo = new CutiSaldo();
                        $modelCutiSaldo->pegawai_id = $modelPegawai->id;
                        $modelCutiSaldo->mulai_berlaku = $tanggalAwal;
                        $modelCutiSaldo->akhir_berlaku = $akhirBerlaku;
                        $modelCutiSaldo->kuota = $kuotaCuti;
                        $modelCutiSaldo->saldo = $kuotaCuti;
                        $modelCutiSaldo->terpakai = 0;
                        $modelCutiSaldo->kadaluarsa = 0;
                        $cekCutiSaldo = CutiSaldo::find()->andWhere('\'' . $tanggalAwal . '\' BETWEEN mulai_berlaku AND akhir_berlaku')
                            ->andWhere(['pegawai_id' => $modelPegawai->id])->one();
                        if ($cekCutiSaldo) {
                            $modelCutiSaldo = CutiSaldo::findOne($cekCutiSaldo['id']);
                            $modelCutiSaldo->kuota = $kuotaCuti;
                        }
                        $modelCutiSaldo->detachBehavior('createdby');
                        $perjanjianKerja = $modelPegawai->getPerjanjianKerjaUntukTanggal($modelCutiSaldo->mulai_berlaku);
                        if ($perjanjianKerja && $modelPegawai->getPegawaiStrukturUntukTanggal($modelCutiSaldo->mulai_berlaku) && $modelPegawai->getPegawaiGolonganUntukTanggal($modelCutiSaldo->mulai_berlaku)) {
                            if ($perjanjianKerja->isAktif($modelCutiSaldo->mulai_berlaku)) {
                                $modelCutiSaldo->save();
                                Yii::info($modelPegawai->nama_lengkap, 'saldo-cuti');
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     * Pembaruan saldo cuti dilakukan pada tanggal2 akhir bulan desember
     * Ini akan membuat saldo cuti untuk tahun berikutnya
     */
    public function setPembaruanSaldo()
    {
        try {
            $dateTime = new DateTime('Jan 1');
            $tanggalAwalObj = $dateTime->modify('+1 year');
            $tanggalAwal = $tanggalAwalObj->format('Y-m-d');
            $akhirBerlaku = CutiConf::getAkhirBelakuBerdasarkanTahun((int)$tanggalAwalObj->format('Y'))->akhir_berlaku;
            Yii::info(Yii::t('frontend', 'Pembaruan saldo.'), 'saldo-cuti');
            $modelPerjanjianKerja = new PerjanjianKerja();
            $getPerjanjianKerjaAktif = $modelPerjanjianKerja->perjanjianKerjaAktifBerdasarTanggal($tanggalAwal);
            if ($getPerjanjianKerjaAktif) {
                /** @var PerjanjianKerja $perjanjianKerja */
                foreach ($getPerjanjianKerjaAktif as $perjanjianKerja) {
                    if (!$perjanjianKerja->pegawai) {
                        continue;
                    }
                    if (!$perjanjianKerja->pegawai->pegawaiStruktur || !$perjanjianKerja->pegawai->pegawaiGolongan) {
                        continue;
                    }
                    Yii::info(Yii::t('frontend', 'Buat saldo untuk tanggal awal : {tanggal}', ['tanggal' => $tanggalAwal]), 'saldo-cuti');
                    if ($modelPerjanjianKerja = PerjanjianKerja::findOne(['pegawai_id' => $perjanjianKerja->pegawai_id, 'dasar_cuti' => Status::YA])) {
                        $kuotaCuti = $modelPerjanjianKerja->hitungKuotaCuti('', $tanggalAwal);
                        $modelCutiSaldo = new CutiSaldo();
                        $modelCutiSaldo->pegawai_id = $modelPerjanjianKerja->pegawai_id;
                        $modelCutiSaldo->mulai_berlaku = $tanggalAwal;
                        $modelCutiSaldo->akhir_berlaku = $akhirBerlaku;
                        $modelCutiSaldo->kuota = $kuotaCuti;
                        $modelCutiSaldo->saldo = $kuotaCuti;
                        $modelCutiSaldo->terpakai = 0;
                        $modelCutiSaldo->kadaluarsa = 0;
                        $cekCutiSaldo = CutiSaldo::findOne(['mulai_berlaku' => $tanggalAwal, 'akhir_berlaku' => $akhirBerlaku, 'pegawai_id' => $perjanjianKerja->pegawai_id]);
                        if ($cekCutiSaldo) {
                            $modelCutiSaldo = $cekCutiSaldo;
                            $modelCutiSaldo->kuota = $kuotaCuti;
                        }
                        $modelCutiSaldo->detachBehavior('createdby');
                        if ($modelCutiSaldo->save()) {
                            Yii::info($modelPerjanjianKerja->pegawai->nama_lengkap, 'saldo-cuti');
                            Yii::info(Yii::t('frontend', 'Saldo baru berhasil ditambahkan untuk periode : {awal} s/d {akhir} sejumlah : {jumlah}',
                                ['awal' => $modelCutiSaldo->mulai_berlaku, 'akhir' => $modelCutiSaldo->akhir_berlaku, 'jumlah' => $modelCutiSaldo->kuota]), 'saldo-cuti');
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }

    /**
     *
     */
    public function generateSaldo()
    {
        try {
            $queryPerjanjianKerja = PerjanjianKerja::find()->andWhere(['dasar_cuti' => Status::YA])->all();
            if ($queryPerjanjianKerja) {
                /** @var PerjanjianKerja $perjanjianKerja */
                foreach ($queryPerjanjianKerja as $perjanjianKerja) {
                    $modelPegawai = \frontend\modules\pegawai\models\Pegawai::findOne($perjanjianKerja->pegawai_id);
                    Yii::info($perjanjianKerja->pegawai->nama_lengkap, 'saldo-cuti');
                    if (!empty($modelPegawai->perjanjianKerja) && !empty($modelPegawai->pegawaiStruktur) && isset($modelPegawai->pegawaiGolongan)) {
                        $masaKerja = $perjanjianKerja->hitungMasaKerja();
                        if ($masaKerja['tahun'] > 0) {
                            Yii::info($masaKerja, 'saldo-cuti');
                            $perjanjianKerja->setKuotaCuti(true);
                        } else {
                            Yii::info(Yii::t('frontend', 'Masih 0 tahun'), 'saldo-cuti');
                        }
                    } else {
                        Yii::info('perjanjian kerja', 'saldo-cuti');
                    }
                }
            }
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
    }
}
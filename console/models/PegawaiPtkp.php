<?php


namespace console\models;


use frontend\models\User;
use Yii;

/**
 * Created by PhpStorm.
 * File Name: PegawaiPtkp.php
 * User: Jiwa Ndaru -> jiwanndaru[at]gmail[.]com
 * Date: 2021-08-15
 * Time: 10:42 PM
 */
class PegawaiPtkp extends \frontend\modules\pegawai\models\PegawaiPtkp
{


    /**
     * Generate data ptkp perpegawai
     * digenerate setiap hari, jika sudah terdapat data pada tahun tersebut, maka tidak akan digenerate ulang
     * hanya untuk pegawai yang aktif perhari itu
     * @param bool $isRegenerate true secara manual hanya untuk perbaikan data
     *
     */
    public function generatePtkp(bool $isRegenerate = false)
    {
        $tahun = date('Y');
        $queryPtkp = PegawaiPtkp::find()->select('pegawai_id')->andWhere(['tahun' => $tahun]);
        $kondisi = $isRegenerate ? 'IN' : 'NOT IN';
        $queryPegawai = Pegawai::find()
            ->joinWith('perjanjianKerja')
            ->joinWith('pegawaiGolongan')
            ->joinWith('pegawaiStruktur')
            ->andWhere([$kondisi, 'pegawai.id', $queryPtkp])->all();
        /** @var Pegawai $pegawai */
        foreach ($queryPegawai as $pegawai) {
            //kalau belum dibuatkan user, user defaultnya bobi
            $user = $pegawai->userPegawai ? $pegawai->userPegawai->user_id : 7;
            Yii::$app->user->setIdentity(User::findOne($user));
            $modelPegawaiPtkp = new PegawaiPtkp();
            $modelPegawaiPtkp->pegawai_id = $pegawai->id;
            $modelPegawaiPtkp->tahun = $tahun;
            if ($isRegenerate) {
                $cek = PegawaiPtkp::findOne(['pegawai_id' => $modelPegawaiPtkp->pegawai_id, 'tahun' => $modelPegawaiPtkp->tahun]);
                if ($cek) {
                    $modelPegawaiPtkp = $cek;
                }
            }
            $modelPegawaiPtkp->status = $pegawai->getStatusPtkp();
            $modelPegawaiPtkp->ptkp = $this->getValueOfPtkp()[$modelPegawaiPtkp->status];
            $modelPegawaiPtkp->save();
        }
    }

}
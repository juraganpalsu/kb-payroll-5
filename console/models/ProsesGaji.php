<?php


namespace console\models;


use frontend\models\User;
use frontend\modules\payroll\models\PayrollBpjs;
use frontend\modules\payroll\models\RevisiGaji;
use Mpdf\MpdfException;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Created by PhpStorm.
 * File Name: ProsesGaji.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 06/07/20
 * Time: 22.34
 */
class ProsesGaji extends \frontend\modules\payroll\models\ProsesGaji
{

    /**
     * @throws CrossReferenceException
     * @throws InvalidConfigException
     * @throws MpdfException
     * @throws PdfParserException
     * @throws PdfTypeException
     */
    public function simpanGaji()
    {
        $query = self::find()->andWhere(['status' => self::dibuat])->orderBy('created_at')->limit(1)->one();
        if ($query) {
            $model = self::findOne($query['id']);
            Yii::$app->user->setIdentity(User::findOne($model->updated_by));
            if ($model) {
                $isNew = $model->lock == 0;
                $model->status = self::diproses;
                if ($model->save()) {
                    $newFileName = '';
                    if (!$isNew) {
                        $revisiGaji = new RevisiGaji();
                        $revisiGaji->proses_gaji_id = $model->id;
                        if ($revisiGaji->save()) {
                            $newFileName = $revisiGaji->id . '.pdf';
                        }
                    }
                    Yii::info($model->attributes, 'proses-gaji');
                    $model->simpanDataGajiBulanan();
                    $model = self::findOne($model->id);
                    $model->status = self::selesai;
                    if ($model->save()) {
                        $model->createFileSlipGaji(true, $newFileName);
                        $model->buatFileRekap();
//                        $model->createFileSlipGajiPerPegawai();
                        Yii::info($model->attributes, 'proses-gaji');
                    }
                }
            }
        }
    }

    /**
     *
     * Membuat file rekap
     *
     *
     */
    public function buatFileRekap(): void
    {
        $fileName = ProsesGaji::filePathGaji . $this->payrollPeriode->namaPeriode . '*' . $this->golongan->nama . '*' . PayrollBpjs::modelPerjanjianKerja()->_kontrak[$this->bisnis_unit] . '.xlsx';
        if (file_exists($fileName)) {
            rename($fileName, $fileName . '@' . date('Ymdhis'));
            $this->buatFileRekap();
        } else {
            $spreadsheet = new Spreadsheet();
            $headerStyle = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
                'fill' => [
                    'fillType' => Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => 'FFA0A0A0',
                    ],
                    'endColor' => [
                        'argb' => 'FFFFFFFF',
                    ],
                ],
            ];

            $center = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ]
            ];

            $outLineBottomBorder = [
                'borders' => array(
                    'bottom' => array(
                        'borderStyle' => Border::BORDER_THIN,
                    ),
                ),
            ];

            $borderInside = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_DOTTED,
                    ],
                ],
            ];

            $font = array(
                'font' => array(
                    'size' => 9,
                    'name' => 'Calibri'
                ));

            try {
                $spreadsheet->setActiveSheetIndex(0);
                $activeSheet = $spreadsheet->getActiveSheet();
                $activeSheet->getColumnDimension('A')->setWidth(5);
                $activeSheet->getColumnDimension('B')->setVisible(false);
                $activeSheet->getColumnDimension('C')->setWidth(15);
                $activeSheet->getColumnDimension('D')->setWidth(30);
                $activeSheet->getColumnDimension('E')->setWidth(15);
                $activeSheet->getColumnDimension('F')->setWidth(15);
                $activeSheet->getColumnDimension('G')->setWidth(15);
                $activeSheet->getColumnDimension('H')->setWidth(15);
                $activeSheet->getColumnDimension('I')->setWidth(5);
                $activeSheet->getColumnDimension('J')->setWidth(5);
                $activeSheet->getColumnDimension('K')->setWidth(5);
                $activeSheet->getColumnDimension('L')->setWidth(5);
                $activeSheet->getColumnDimension('M')->setWidth(5);
                $activeSheet->getColumnDimension('N')->setWidth(5);
                $activeSheet->getColumnDimension('O')->setWidth(5);
                $activeSheet->getColumnDimension('P')->setWidth(5);
                $activeSheet->getColumnDimension('Q')->setWidth(5);
                $activeSheet->getColumnDimension('R')->setWidth(5);
                $activeSheet->getColumnDimension('S')->setWidth(15);
                $activeSheet->getColumnDimension('T')->setWidth(15);
                $activeSheet->getColumnDimension('U')->setWidth(15);
                $activeSheet->getColumnDimension('V')->setWidth(15);
                $activeSheet->getColumnDimension('W')->setWidth(15);
                $activeSheet->getColumnDimension('X')->setWidth(15);
                $activeSheet->getColumnDimension('Y')->setWidth(15);
                $activeSheet->getColumnDimension('Z')->setWidth(15);

                $activeSheet->mergeCells('A1:C1')->setCellValue('A1', Yii::t('frontend', 'Payroll Periode'));
                $activeSheet->setCellValue('D1', $this->payrollPeriode->namaPeriode);
                $activeSheet->mergeCells('A2:C2')->setCellValue('A2', Yii::t('frontend', 'Golongan'));
                $activeSheet->setCellValue('D2', $this->golongan->nama);
                $activeSheet->mergeCells('A3:C3')->setCellValue('A3', Yii::t('frontend', 'Bisnis unit'));
                $activeSheet->setCellValue('D3', PayrollBpjs::modelPerjanjianKerja()->_kontrak[$this->bisnis_unit]);
                $activeSheet->mergeCells('A4:C4')->setCellValue('A4', Yii::t('frontend', 'Catatan'));
                $activeSheet->setCellValue('D4', $this->catatan);
                $activeSheet->mergeCells('A6:A7')->setCellValue('A6', Yii::t('frontend', 'NO'))->getStyle('A6:A7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('B6:B7')->setCellValue('B6', Yii::t('frontend', 'id'))->getStyle('B6:B7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('C6:C7')->setCellValue('C6', Yii::t('frontend', 'ID Pegawai'))->getStyle('C6:C7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('D6:D7')->setCellValue('D6', Yii::t('frontend', 'Nama'))->getStyle('D6:D7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('E6:E7')->setCellValue('E6', Yii::t('frontend', 'Bisnis Unit'))->getStyle('E6:E7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('F6:F7')->setCellValue('F6', Yii::t('frontend', 'Golongan'))->getStyle('F6:F7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('G6:G7')->setCellValue('G6', Yii::t('frontend', 'Jenis Kontrak'))->getStyle('G6:G7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('H6:H7')->setCellValue('H6', Yii::t('frontend', 'Struktur'))->getStyle('H6:H7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('I6:R6')->setCellValue('I6', Yii::t('frontend', 'Absensi'))->getStyle('I6:R7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('I7', Yii::t('frontend', 'Alpa'))->getStyle('I7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('J7', Yii::t('frontend', 'Cuti'))->getStyle('J7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('K7', Yii::t('frontend', 'UFN'))->getStyle('K7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('L7', Yii::t('frontend', 'Dispen'))->getStyle('L7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('M7', Yii::t('frontend', 'IS'))->getStyle('M7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('N7', Yii::t('frontend', 'CM'))->getStyle('N7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('O7', Yii::t('frontend', 'SD'))->getStyle('O7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('P7', Yii::t('frontend', 'WFH'))->getStyle('P7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('Q7', Yii::t('frontend', 'ATL'))->getStyle('Q7')->applyFromArray($headerStyle);
                $activeSheet->setCellValue('R7', Yii::t('frontend', 'LH'))->getStyle('R7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('S6:S7')->setCellValue('S6', Yii::t('frontend', 'HK Efektif'))->getStyle('S6:S7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('T6:T7')->setCellValue('T6', Yii::t('frontend', 'Gaji Pokok'))->getStyle('T6:T7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('U6:U7')->setCellValue('U6', Yii::t('frontend', 'Hari Kerja'))->getStyle('U6:U7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('V6:V7')->setCellValue('V6', Yii::t('frontend', 'Jumlah Hadir'))->getStyle('V6:V7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('W6:W7')->setCellValue('W6', Yii::t('frontend', 'Jumlah Absen'))->getStyle('W6:W7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('X6:X7')->setCellValue('X6', Yii::t('frontend', 'Jumlah Upah'))->getStyle('X6:X7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('Y6:Y7')->setCellValue('Y6', Yii::t('frontend', 'Jam Lembur'))->getStyle('Y6:Y7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('Z6:Z7')->setCellValue('Z6', Yii::t('frontend', 'Uang Lembur'))->getStyle('Z6:Z7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AA6:AA7')->setCellValue('AA6', Yii::t('frontend', 'Uang Makan Lembur'))->getStyle('AA6:AA7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AB6:AB7')->setCellValue('AB6', Yii::t('frontend', 'Uang Makan'))->getStyle('AB6:AB7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AC6:AC7')->setCellValue('AC6', Yii::t('frontend', 'Total Uang Makan'))->getStyle('AC6:AC7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AD6:AD7')->setCellValue('AD6', Yii::t('frontend', 'Uang Transport'))->getStyle('AD6:AD7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AE6:AE7')->setCellValue('AE6', Yii::t('frontend', 'Total Uang Transport'))->getStyle('AE6:AE7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AF6:AF7')->setCellValue('AF6', Yii::t('frontend', 'S2'))->getStyle('AF6:AF7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AG6:AG7')->setCellValue('AG6', Yii::t('frontend', 'S3'))->getStyle('AG6:AG7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AH6:AH7')->setCellValue('AH6', Yii::t('frontend', 'Uang Shift'))->getStyle('AH6:AH7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AI6:AI7')->setCellValue('AI6', Yii::t('frontend', 'Jumlah Telat'))->getStyle('AI6:AI7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AJ6:AJ7')->setCellValue('AJ6', Yii::t('frontend', 'Premi Hadir'))->getStyle('AJ6:AJ7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AK6:AK7')->setCellValue('AK6', Yii::t('frontend', 'Insentif Tidak Tetap'))->getStyle('AK6:AK7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AL6:AL7')->setCellValue('AL6', Yii::t('frontend', 'Rapel'))->getStyle('AL6:AL7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AM6:AM7')->setCellValue('AM6', Yii::t('frontend', 'Admin Bank'))->getStyle('AM6:AM7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AN6:AN7')->setCellValue('AN6', Yii::t('frontend', 'Sewa Motor'))->getStyle('AN6:AN7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AO6:AO7')->setCellValue('AO6', Yii::t('frontend', 'Sewa Mobil'))->getStyle('AO6:AO7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AP6:AP7')->setCellValue('AP6', Yii::t('frontend', 'Sewa Rumah'))->getStyle('AP6:AP7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AQ6:AQ7')->setCellValue('AQ6', Yii::t('frontend', 'Tunjagan Lain Tetap'))->getStyle('AQ6:AQ7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AR6:AR7')->setCellValue('AR6', Yii::t('frontend', 'Tunjangan Anak'))->getStyle('AR6:AR7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AS6:AS7')->setCellValue('AS6', Yii::t('frontend', 'Tunjangan Loyalitas'))->getStyle('AS6:AS7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AT6:AT7')->setCellValue('AT6', Yii::t('frontend', 'Tunjangan Kost'))->getStyle('AT6:AT7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AU6:AU7')->setCellValue('AU6', Yii::t('frontend', 'Tunjangan Pulsa'))->getStyle('AU6:AU7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AV6:AV7')->setCellValue('AV6', Yii::t('frontend', 'Pulsa WFH'))->getStyle('AU6:AU7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AW6:AW7')->setCellValue('AW6', Yii::t('frontend', 'Tunjangan Jabatan'))->getStyle('AV6:AV7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AX6:AX7')->setCellValue('AX6', Yii::t('frontend', 'Insentif Tetap'))->getStyle('AW6:AW7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AY6:AY7')->setCellValue('AY6', Yii::t('frontend', 'Tunjangan PPH'))->getStyle('AX6:AX7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('AZ6:AZ7')->setCellValue('AZ6', Yii::t('frontend', 'Potongan Gaji'))->getStyle('AY6:AY7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BA6:BA7')->setCellValue('BA6', Yii::t('frontend', 'Potongan ATL'))->getStyle('AY6:AY7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BB6:BB7')->setCellValue('BB6', Yii::t('frontend', 'Sub Total'))->getStyle('AZ6:AZ7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BC6:BC7')->setCellValue('BC6', Yii::t('frontend', 'Bpjs Kes'))->getStyle('BA6:BA7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BD6:BD7')->setCellValue('BD6', Yii::t('frontend', 'Bpjs TK'))->getStyle('BB6:BB7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BE6:BE7')->setCellValue('BE6', Yii::t('frontend', 'Potongan BPJS'))->getStyle('BC6:BC7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BF6:BF7')->setCellValue('BF6', Yii::t('frontend', 'Pinjaman'))->getStyle('BD6:BD7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BG6:BG7')->setCellValue('BG6', Yii::t('frontend', 'Bank'))->getStyle('BE6:BE7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BH6:BH7')->setCellValue('BH6', Yii::t('frontend', 'Koperasi'))->getStyle('BF6:BF7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BI6:BI7')->setCellValue('BI6', Yii::t('frontend', 'Kasbon'))->getStyle('BG6:BG7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BJ6:BJ7')->setCellValue('BJ6', Yii::t('frontend', 'Potongan Lain-lain'))->getStyle('BH6:BH7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BK6:BK7')->setCellValue('BK6', Yii::t('frontend', 'Potongan PPH'))->getStyle('BI6:BI7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BL6:BL7')->setCellValue('BL6', Yii::t('frontend', 'Sub Total2'))->getStyle('BJ6:BJ7')->applyFromArray($headerStyle);
                $activeSheet->mergeCells('BM6:BM7')->setCellValue('BM6', Yii::t('frontend', 'Total Diterima'))->getStyle('BK6:BK7')->applyFromArray($headerStyle);

                $no = 0;
                $row = 7;
                foreach ($this->gajiBulanans as $gajiBulanan) {
                    $no++;
                    $row++;
                    $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('B' . $row, $gajiBulanan->id)->getStyle('B' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('C' . $row, $gajiBulanan->id_pegawai)->getStyle('C' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('D' . $row, $gajiBulanan->nama_lengkap)->getStyle('D' . $row)->applyFromArray(array_merge($borderInside));
                    $activeSheet->setCellValue('E' . $row, $gajiBulanan->bisnis_unit_s)->getStyle('E' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('F' . $row, $gajiBulanan->golongan_s)->getStyle('F' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('G' . $row, $gajiBulanan->jenis_kontrak_s)->getStyle('G' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('H' . $row, $gajiBulanan->struktur_s)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('I' . $row, $gajiBulanan->alfa)->getStyle('I' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('J' . $row, $gajiBulanan->cuti)->getStyle('J' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('K' . $row, $gajiBulanan->undifined)->getStyle('K' . $row)->applyFromArray(array_merge($center, $borderInside));
                    $activeSheet->setCellValue('L' . $row, $gajiBulanan->dispensasi)->getStyle('L' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('M' . $row, $gajiBulanan->is)->getStyle('M' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('N' . $row, $gajiBulanan->cm)->getStyle('N' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('O' . $row, $gajiBulanan->sd)->getStyle('O' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('P' . $row, $gajiBulanan->wfh)->getStyle('P' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('Q' . $row, $gajiBulanan->atl)->getStyle('Q' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('R' . $row, $gajiBulanan->libur_hadir)->getStyle('R' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('S' . $row, $gajiBulanan->hari_kerja_aktif)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('T' . $row, $gajiBulanan->p_gaji_pokok)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('U' . $row, $gajiBulanan->jumlahHariKerja())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('V' . $row, $gajiBulanan->jumlahHadir())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('W' . $row, $gajiBulanan->jumlahAlfa())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('X' . $row, $gajiBulanan->totalGajiPokok())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('Y' . $row, $gajiBulanan->jam_lembur)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('Z' . $row, $gajiBulanan->uang_lembur)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AA' . $row, $gajiBulanan->totalUml())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AB' . $row, $gajiBulanan->p_uang_makan)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AC' . $row, $gajiBulanan->hitungTotalUangMakan())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AD' . $row, $gajiBulanan->p_uang_transport)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AE' . $row, $gajiBulanan->hitungTotalUangTransport())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AF' . $row, $gajiBulanan->shift2())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AG' . $row, $gajiBulanan->shift3())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AH' . $row, $gajiBulanan->hitungTotalUangShift())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AI' . $row, $gajiBulanan->jumlah_telat)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AJ' . $row, $gajiBulanan->premiHadir())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AK' . $row, $gajiBulanan->insentif_tidak_tetap)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AL' . $row, $gajiBulanan->rapel)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AM' . $row, $gajiBulanan->p_admin_bank)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AN' . $row, $gajiBulanan->sewaMotor())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AO' . $row, $gajiBulanan->sewaMobil())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AP' . $row, $gajiBulanan->sewaRumah())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AQ' . $row, $gajiBulanan->tunjLainTetap())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AR' . $row, $gajiBulanan->p_tunjangan_anak)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AS' . $row, $gajiBulanan->p_tunjangan_loyalitas)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AT' . $row, $gajiBulanan->tunjKost())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AU' . $row, $gajiBulanan->tunjPulsa())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AV' . $row, $gajiBulanan->tunjPulsaWfh())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AW' . $row, $gajiBulanan->tunjJabatan())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AX' . $row, $gajiBulanan->insetifTetap())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AY' . $row, $gajiBulanan->potonganPph())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('AZ' . $row, $gajiBulanan->pinjaman_potongan_nominal)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BA' . $row, $gajiBulanan->potonganATL())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BB' . $row, $gajiBulanan->subTotal1())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BC' . $row, $gajiBulanan->pegawai_bpjs_kes)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BD' . $row, $gajiBulanan->pegawai_bpjs_tk)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BE' . $row, $gajiBulanan->potonganBpjs())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BF' . $row, $gajiBulanan->pinjaman_nominal)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BG' . $row, $gajiBulanan->pinjaman_bank_nominal)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BH' . $row, $gajiBulanan->pinjaman_koperasi_nominal)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BI' . $row, $gajiBulanan->pinjaman_kasbon_nominal)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BJ' . $row, $gajiBulanan->pinjaman_lain_lain_nominal)->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BK' . $row, $gajiBulanan->potonganPph())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BL' . $row, $gajiBulanan->subTotal2())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                    $activeSheet->setCellValue('BM' . $row, $gajiBulanan->Total())->getStyle('H' . $row)->applyFromArray(array_merge($center, $borderInside))->getNumberFormat()->setFormatCode('#,##0');
                }

                $highestColumn = $activeSheet->getHighestColumn('6');

                $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($outLineBottomBorder);
                $activeSheet->getStyle('A1:' . $highestColumn . $row)->applyFromArray($font);

                //Setting password file
                $activeSheet->getProtection()->setSheet(true);
                $security = $spreadsheet->getSecurity();
                $security->setLockWindows(true);
                $security->setLockStructure(true);
                $password = date('ymd', strtotime($this->payrollPeriode->tanggal_awal)) . date('d', strtotime($this->payrollPeriode->tanggal_awal)) . '3';
                $security->setWorkbookPassword($password);

                $writer = new Xlsx($spreadsheet);
                $writer->save($fileName);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }
        }
    }

}
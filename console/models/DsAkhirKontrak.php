<?php


namespace console\models;


use Exception;
use frontend\modules\dashboard\models\DsAkhirKontrakDetail;
use frontend\modules\pegawai\models\PerjanjianKerja;
use Yii;

/**
 * Created by PhpStorm.
 * File Name: DsAkhirKontrak.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 12/12/19
 * Time: 21.43
 */
class DsAkhirKontrak extends \frontend\modules\dashboard\models\DsAkhirKontrak
{

    public function rekapAkhirKontrak()
    {
        $bulan = date('m', strtotime('+ 2 months'));
        $tahun = date('Y', strtotime('+ 2 months'));
        $query = PerjanjianKerja::find()->andWhere(['tanggal_resign' => null])->andWhere(['month(akhir_berlaku)' => $bulan, 'year(akhir_berlaku)' => $tahun])->orderBy('akhir_berlaku ASC')->all();
        $dataKontrak = [];
        Yii::info(Yii::t('frontend', 'Rekap pegawai yg berakhir kontraknya pada bulan {bulan}', ['bulan' => $bulan]), 'ds-akhir-kontrak');
        if ($query) {
            /** @var PerjanjianKerja $perjanjianKerja */
            foreach ($query as $perjanjianKerja) {
                $dataKontrak[$perjanjianKerja->akhir_berlaku]['kontrak'][$perjanjianKerja->kontrak][] = $perjanjianKerja->id;
            }
        }
        if (!empty($dataKontrak)) {
            $cekDs = DsAkhirKontrak::find()->select('id')->where(['month(tanggal)' => $bulan, 'year(tanggal)' => $tahun]);
            DsAkhirKontrakDetail::deleteAll(['IN', 'ds_akhir_kontrak_id', $cekDs]);
            try {
                DsAkhirKontrak::deleteAll(['month(tanggal)' => $bulan, 'year(tanggal)' => $tahun]);
            } catch (Exception $e) {
                Yii::info($e->getMessage(), 'exception');
            }

            Yii::info(Yii::t('frontend', 'Melakukan Rekap.'), 'ds-akhir-kontrak');
            foreach ($dataKontrak as $akhirBerlaku => $kontrak) {
                $modelDsAkhirKonrak = new DsAkhirKontrak();
                $modelDsAkhirKonrak->tanggal = $akhirBerlaku;
                if ($modelDsAkhirKonrak->save()) {
                    foreach ($kontrak['kontrak'] as $jenisKontrak => $dataAkhirKontrak) {
                        $modelDsAkhirKonrakDetail = new DsAkhirKontrakDetail();
                        $modelDsAkhirKonrakDetail->kontrak = $jenisKontrak;
                        $modelDsAkhirKonrakDetail->jumlah = count($dataAkhirKontrak);
                        $modelDsAkhirKonrakDetail->perjanjian_kerja_ids = implode(',', $dataAkhirKontrak);
                        $modelDsAkhirKonrakDetail->ds_akhir_kontrak_id = $modelDsAkhirKonrak->id;
                        $modelDsAkhirKonrakDetail->save();
                    }
                    Yii::info(Yii::t('frontend', 'Rekap selesai.'), 'ds-akhir-kontrak');
                }
            }
        } else {
            Yii::info(Yii::t('frontend', 'Tidak ada data di rekap untuk tanggal tsb.'), 'ds-akhir-kontrak');
        }

    }
}
<?php


namespace console\models;


use DateInterval;
use DateTime;
use frontend\models\Absensi;
use frontend\models\Attendance;
use frontend\models\form\AbsensiForm;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Created by PhpStorm.
 * File Name: AntrianLaporanAbsensi.php
 * User: jiwa -> jiwanndaru[at]gmail[.]com
 * Date: 08/10/20
 * Time: 22.30
 */
class AntrianLaporanAbsensi extends \frontend\modules\queue\models\AntrianLaporanAbsensi
{

    /**
     * @throws \Exception
     */
    public function generateLaporan()
    {
        $getDataTemp = self::find()->andWhere(['status' => AntrianProsesAbsensi::DIBUAT])->orderBy('created_at ASC')->limit(1)->one();

        //Mengambil jumlah data yg sedang diproses
        $dataSedangDiproses = self::find()->where(['status' => AntrianProsesAbsensi::DIPROSES])->count();

        //Menandai sedang diproses
        $modelAntrian = self::findOne($getDataTemp['id']);
        //dapat melakukan hingga 8 proses secara bersamaan
        if ($modelAntrian && $dataSedangDiproses <= 2) {
            $modelAntrian->status = AntrianProsesAbsensi::DIPROSES;
            if ($modelAntrian->save()) {
                if ($modelAntrian->jenis_laporan == self::laporan) {
                    $model = new AbsensiForm();
                    $model->setScenario('laporan');
                    $model->load($modelAntrian->attributes);
                    $model->jenis_kontrak_id = $modelAntrian->jenis_perjanjian;
                    $model->bisnis_unit_id = $modelAntrian->bisnis_unit;
                    $model->golongan_id = $modelAntrian->golongan_id;
                    $model->tanggal_awal = $modelAntrian->tanggal_awal;
                    $model->tanggal_akhir = $modelAntrian->tanggal_akhir;
                    if ($model->validate()) {
                        $model->downloadReport($modelAntrian);
                    }
                }
                if ($modelAntrian->jenis_laporan == self::rekap) {
                    $modelAntrian->generateRekap();
                }
            }
            //Setelah selesai proses langsung tandai
            $getModelTempProcessed = self::findOne($getDataTemp['id']);
            if ($getModelTempProcessed && $getModelTempProcessed->status == AntrianProsesAbsensi::DIPROSES) {
                $modelAntrian->status = AntrianProsesAbsensi::SELESAI;
                $modelAntrian->save();
            }
        }
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function queryAbsensi(): array
    {
        $query = Absensi::find();
        $query->andWhere(['BETWEEN', 'tanggal', $this->tanggal_awal, $this->tanggal_akhir]);
        $query->joinWith(['perjanjianKerja' => function (ActiveQuery $query) {
            if (!empty($this->jenis_perjanjian)) {
                $query->andWhere(['perjanjian_kerja.jenis_perjanjian' => $this->jenis_perjanjian]);
            }
            if (!empty($this->bisnis_unit)) {
                $query->andWhere(['perjanjian_kerja.kontrak' => $this->bisnis_unit]);
            }
        }]);
        $query->joinWith(['pegawaiGolongan' => function (ActiveQuery $query) {
            $query->andWhere(['pegawai_golongan.golongan_id' => $this->golongan_id]);
        }]);
        $query->orderBy('absensi.tanggal ASC');

        return $query->all();
    }

    /**
     * @param string $tanggal
     * @param string $pegawaiId
     * @return array|ActiveRecord[]
     */
    public function getAttandances(string $tanggal, string $pegawaiId): array
    {
        $query = Attendance::find();
        $query->where([
            'pegawai_id' => $pegawaiId,
        ]);
        $kemarin = date('Y-m-d', strtotime($tanggal . '-1 day'));
        $besok = date('Y-m-d', strtotime($tanggal . '+1 day'));

        $query->andWhere(['BETWEEN', 'date(check_time)', $kemarin, $besok]);

        $query->orderBy('check_time ASC');

        return $query->all();
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function formatingRekap(): array
    {
        $absensis = $this->queryAbsensi();
        $datas = [];
        /** @var Absensi $absensi */
        foreach ($absensis as $absensi) {
            $pegawaiStruktur = $absensi->pegawaiStruktur;
            $perjanjianKerja = $absensi->perjanjianKerja;
            $golongan = $absensi->pegawaiGolongan;

            $attendances = array_map(function (Attendance $x) {
                return date('d-m-Y H:i:s', strtotime($x->check_time));
            }, $this->getAttandances($absensi->tanggal, $absensi->pegawai_id));
            $pengaliUml = $absensi->hitungPengaliUangMakanLembur();

            $datas[$absensi->pegawai_id]['detail'] = [
                'id_pegawai' => $perjanjianKerja ? str_pad($absensi->perjanjianKerja->id_pegawai, 8, '0', STR_PAD_LEFT) : '',
                'pegawai_id' => $absensi->pegawai ? $absensi->pegawai->nama : '',
                'departemen' => $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->departemen ? $pegawaiStruktur->struktur->departemen->nama : '' : '' : '',
                'struktur' => $pegawaiStruktur ? $pegawaiStruktur->struktur ? $pegawaiStruktur->struktur->name : '' : '',
                'busines_unit' => $perjanjianKerja ? $perjanjianKerja->_kontrak[$perjanjianKerja->kontrak] : '',
                'golongan_id' => $golongan ? $golongan->golongan->nama : '',
                'sistem_kerja' => $absensi->pegawai->getPegawaiSistemKerjaUntukTanggal($absensi->tanggal) ? $absensi->pegawai->getPegawaiSistemKerjaUntukTanggal($absensi->tanggal)->sistemKerja->nama : '',
            ];
            $datas[$absensi->pegawai_id]['absensi'][$absensi->tanggal] = [
                'tanggal' => date('d-m-Y', strtotime($absensi->tanggal)),
                'masuk' => $absensi->masuk ? date('d-m-Y H:i:s', strtotime($absensi->masuk)) : '',
                'pulang' => $absensi->pulang ? date('d-m-Y H:i:s', strtotime($absensi->pulang)) : '',
                'shift_search' => $absensi->time_table_id ? $absensi->timeTable : '' ? $absensi->timeTable->shift_ : '',
                'telat' => $absensi->telat,
                'spl_id' => $absensi->splRel ? $absensi->splRel->splTemplate->nama : '',
                'time_table_id' => !$absensi->time_table_id ? '' : $absensi->timeTable ? $absensi->timeTable->nama : '',
                'status_kehadiran' => $absensi->statusKehadiran_,
                'attendance' => implode("\n", $attendances),
                'jumlah_jam_lembur' => $absensi->jamLembur(),
                'uml' => $pengaliUml ? 'UML' . $pengaliUml : '',
            ];
        }
        $reformatingDatas = [];
        foreach ($datas as $pegawai) {
            $tanggalAwal = new DateTime($this->tanggal_awal);
            $tanggalAkhir = new DateTime($this->tanggal_akhir);
            while ($tanggalAwal <= $tanggalAkhir) {
                $tanggal = $tanggalAwal->format('Y-m-d');
                if (isset($pegawai['absensi'][$tanggal])) {
                    $absensi = $pegawai['absensi'][$tanggal];
                    $reformatingDatas[] = array_merge($pegawai['detail'], $absensi);
                } else {
                    $absensi = [
                        'tanggal' => date('d-m-Y', strtotime($tanggal)),
                        'masuk' => '',
                        'pulang' => '',
                        'shift_search' => '',
                        'telat' => '',
                        'spl_id' => '',
                        'time_table_id' => '',
                        'status_kehadiran' => '',
                        'attendance' => '',
                        'jumlah_jam_lembur' => '',
                        'uml' => '',
                    ];
                    $reformatingDatas[] = array_merge($pegawai['detail'], $absensi);
                }
                $tanggalAwal->add(new DateInterval('P1D'));
            }
        }
        return $reformatingDatas;
    }


    /**
     * @return bool
     * @throws \Exception
     */
    public function generateRekap(): bool
    {
        try {
            $spreadsheet = new Spreadsheet();
            $headerStyle = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
                'fill' => [
                    'fillType' => Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => 'FFA0A0A0',
                    ],
                    'endColor' => [
                        'argb' => 'FFFFFFFF',
                    ],
                ],
            ];

            $font = array(
                'font' => array(
                    'size' => 9,
                    'name' => 'Calibri'
                ));

            $spreadsheet->setActiveSheetIndex(0);
            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->getColumnDimension('A')->setWidth(5);
            $activeSheet->getColumnDimension('B')->setWidth(15);
            $activeSheet->getColumnDimension('C')->setWidth(30);
            $activeSheet->getColumnDimension('D')->setWidth(12);
            $activeSheet->getColumnDimension('E')->setWidth(15);
            $activeSheet->getColumnDimension('F')->setWidth(15);
            $activeSheet->getColumnDimension('G')->setWidth(15);
            $activeSheet->getColumnDimension('H')->setWidth(15);
            $activeSheet->getColumnDimension('I')->setWidth(15);
            $activeSheet->getColumnDimension('J')->setWidth(15);
            $activeSheet->getColumnDimension('K')->setWidth(15);
            $activeSheet->getColumnDimension('L')->setWidth(15);
            $activeSheet->getColumnDimension('M')->setWidth(15);
            $activeSheet->getColumnDimension('N')->setWidth(15);
            $activeSheet->getColumnDimension('O')->setWidth(15);
            $activeSheet->getColumnDimension('P')->setWidth(15);
            $activeSheet->getColumnDimension('Q')->setWidth(15);
            $activeSheet->getColumnDimension('R')->setWidth(15);
            $activeSheet->getColumnDimension('S')->setWidth(15);


            $activeSheet->setCellValue('A1', 'NO')->getStyle('A1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('B1', 'Id Pegawai')->getStyle('B1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('C1', 'Pegawai ID')->getStyle('C1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('D1', 'Departemen')->getStyle('D1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('E1', 'Struktur')->getStyle('E1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('F1', 'Bisnis Unit')->getStyle('F1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('G1', 'Golongan')->getStyle('G1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('H1', 'Sistem Kerja')->getStyle('H1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('I1', 'Tangaal')->getStyle('I1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('J1', 'Masuk')->getStyle('J1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('K1', 'Pulang')->getStyle('K1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('L1', 'Shift')->getStyle('L1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('M1', 'Telat')->getStyle('M1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('N1', 'SPL')->getStyle('N1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('O1', 'Time Table')->getStyle('O1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('P1', 'Status Kehadiran')->getStyle('P1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('Q1', 'Attendance')->getStyle('Q1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('R1', 'Jam Lembur')->getStyle('R1')->applyFromArray($headerStyle);
            $activeSheet->setCellValue('S1', 'UML')->getStyle('S1')->applyFromArray($headerStyle);
            $row = 2;
            $no = 1;
            try {
                foreach ($this->formatingRekap() as $absensi) {
                    $activeSheet->setCellValue('A' . $row, $no)->getStyle('A' . $row);
                    $activeSheet->setCellValue('B' . $row, $absensi['id_pegawai'])->getStyle('B' . $row);
                    $activeSheet->setCellValue('C' . $row, $absensi['pegawai_id'])->getStyle('C' . $row);
                    $activeSheet->setCellValue('D' . $row, $absensi['departemen'])->getStyle('D' . $row);
                    $activeSheet->setCellValue('E' . $row, $absensi['struktur'])->getStyle('E' . $row);
                    $activeSheet->setCellValue('F' . $row, $absensi['busines_unit'])->getStyle('F' . $row);
                    $activeSheet->setCellValue('G' . $row, $absensi['golongan_id'])->getStyle('G' . $row);
                    $activeSheet->setCellValue('H' . $row, $absensi['sistem_kerja'])->getStyle('H' . $row);
                    $activeSheet->setCellValue('I' . $row, $absensi['tanggal'])->getStyle('I' . $row);
                    $activeSheet->setCellValue('J' . $row, $absensi['masuk'])->getStyle('J' . $row);
                    $activeSheet->setCellValue('K' . $row, $absensi['pulang'])->getStyle('K' . $row);
                    $activeSheet->setCellValue('L' . $row, $absensi['shift_search'])->getStyle('L' . $row);
                    $activeSheet->setCellValue('M' . $row, $absensi['telat'])->getStyle('M' . $row);
                    $activeSheet->setCellValue('N' . $row, $absensi['spl_id'])->getStyle('N' . $row);
                    $activeSheet->setCellValue('O' . $row, $absensi['time_table_id'])->getStyle('O' . $row);
                    $activeSheet->setCellValue('P' . $row, $absensi['status_kehadiran'])->getStyle('P' . $row);
                    $activeSheet->setCellValue('Q' . $row, $absensi['attendance'])->getStyle('Q' . $row);
                    $activeSheet->setCellValue('R' . $row, $absensi['jumlah_jam_lembur'])->getStyle('R' . $row);
                    $activeSheet->setCellValue('S' . $row, $absensi['uml'])->getStyle('S' . $row);
                    $row++;
                    $no++;
                }
            } catch (Exception $e) {
            }

            $activeSheet->getStyle('A1:' . 'A' . $row)->applyFromArray($font);

            $writer = new Xlsx($spreadsheet);
            $writer->save(Yii::getAlias('@frontend') . self::filePathLaporan . $this->id . '.xlsx');

        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'exception');
        }
        return true;
    }
}